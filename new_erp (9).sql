-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 03, 2012 at 03:13 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `new_erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `acad_change_requests`
--

CREATE TABLE IF NOT EXISTS `acad_change_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(70) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `instructional_hours` enum('0','1') DEFAULT '0',
  `classroom` enum('0','1') DEFAULT '0',
  `additional_class` enum('0','1') DEFAULT '0',
  `class_swapping` enum('0','1') DEFAULT '0',
  `course_extension` enum('0','1') DEFAULT '0',
  `others` enum('0','1') DEFAULT '0',
  `others_specify` varchar(70) DEFAULT NULL,
  `request_detail` varchar(400) NOT NULL,
  `acad_approval` int(11) DEFAULT '0',
  `sales_approval` int(11) DEFAULT '0',
  `acad_date_rcv` int(11) DEFAULT '0',
  `sales_date_rcv` int(11) DEFAULT '0',
  `date_added` int(11) DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `acad_change_requests`
--

INSERT INTO `acad_change_requests` (`request_id`, `student_name`, `student_id`, `instructional_hours`, `classroom`, `additional_class`, `class_swapping`, `course_extension`, `others`, `others_specify`, `request_detail`, `acad_approval`, `sales_approval`, `acad_date_rcv`, `sales_date_rcv`, `date_added`) VALUES
(1, 'Amadeo Pelaez', '11-011102', '1', '1', '1', '0', '0', '0', NULL, 'my reason here.', 1342403456, 1327902306, 1327917056, 1327902306, 1327569501),
(2, 'asd', '1', '1', '1', '1', '0', '0', '0', NULL, '', 1338448754, 0, 55154, 0, 1334892817);

-- --------------------------------------------------------

--
-- Table structure for table `acad_classes`
--

CREATE TABLE IF NOT EXISTS `acad_classes` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL COMMENT 'from books table',
  `course_id` int(11) NOT NULL COMMENT 'from course table',
  `level_id` int(11) DEFAULT NULL COMMENT 'from course_level table',
  `classroom_id` int(4) NOT NULL COMMENT 'from classrooms table',
  `class_type` enum('1:1','1:4','1:8') DEFAULT '1:1',
  `teacher_id` int(11) NOT NULL COMMENT 'from emp_user table(emp_id)',
  `time` enum('08:00-09:50','10:00-11:50','12:40-02:30','02:40-04:30','04:40-06:30','06:40-08:30') DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`class_id`),
  KEY `book_id` (`book_id`),
  KEY `course_id` (`course_id`),
  KEY `level_id` (`level_id`),
  KEY `classroom_id` (`classroom_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `acad_classes`
--

INSERT INTO `acad_classes` (`class_id`, `book_id`, `course_id`, `level_id`, `classroom_id`, `class_type`, `teacher_id`, `time`, `date_added`, `status`) VALUES
(1, 2, 2, 1, 2, '1:1', 11, '08:00-09:50', 1323237576, 0),
(2, 12, 15, 3, 2, '1:1', 11, '10:00-11:50', 1323312741, 1),
(5, 2, 8, 3, 11, '1:1', 12, '10:00-11:50', 1323396830, 1),
(6, 18, 15, 1, 2, '1:1', 12, '02:40-04:30', 1323410885, 1),
(7, 4, 8, 6, 15, '1:4', 13, '08:00-09:50', 1323411109, 1),
(8, 2, 8, 3, 15, '1:1', 13, '10:00-11:50', 1323411279, 1),
(10, 2, 8, 3, 21, '1:8', 12, '08:00-09:50', 1323764089, 1),
(11, 12, 15, 1, 1, '1:1', 11, '06:40-08:30', 1325053574, 1),
(12, 2, 8, 1, 5, '1:1', 14, '08:00-09:50', 1332985362, 1),
(13, 3, 8, 1, 6, '1:1', 12, '12:40-02:30', 1333012424, 1),
(14, 5, 8, 1, 2, '1:4', 12, '04:40-06:30', 1333075199, 0),
(15, 5, 8, 8, 6, '1:4', 11, '04:40-06:30', 1338458331, 1);

-- --------------------------------------------------------

--
-- Table structure for table `acad_lesson_plans`
--

CREATE TABLE IF NOT EXISTS `acad_lesson_plans` (
  `lesson_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `inclusive_date_from` int(11) DEFAULT NULL,
  `inclusive_date_to` int(11) DEFAULT NULL,
  `date_added` int(11) NOT NULL,
  `date_modified` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `date_approved` int(11) NOT NULL,
  PRIMARY KEY (`lesson_plan_id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `acad_lesson_plans`
--

INSERT INTO `acad_lesson_plans` (`lesson_plan_id`, `class_id`, `inclusive_date_from`, `inclusive_date_to`, `date_added`, `date_modified`, `teacher_id`, `date_approved`) VALUES
(5, 1, 1325503040, 1325848640, 1326694631, 1334805440, 11, 0),
(6, 2, 1326728806, 1327074406, 1326703606, 0, 11, 1339645479),
(7, 11, 1326729620, 1327075220, 1326704420, 0, 11, 0),
(8, 12, 1338890962, 1339409362, 1339643362, 0, 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acad_lesson_plan_details`
--

CREATE TABLE IF NOT EXISTS `acad_lesson_plan_details` (
  `lesson_plan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_plan_id` int(11) NOT NULL,
  `weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  `objectives` varchar(300) DEFAULT NULL,
  `topic` varchar(200) DEFAULT NULL,
  `class_starter` varchar(300) DEFAULT NULL,
  `introduction` varchar(300) DEFAULT NULL,
  `lesson_proper` varchar(300) DEFAULT NULL,
  `guided_practice` varchar(300) DEFAULT NULL,
  `independent_practice` varchar(300) DEFAULT NULL,
  `homework` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`lesson_plan_detail_id`),
  KEY `lesson_plan_id` (`lesson_plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `acad_lesson_plan_details`
--

INSERT INTO `acad_lesson_plan_details` (`lesson_plan_detail_id`, `lesson_plan_id`, `weekday`, `objectives`, `topic`, `class_starter`, `introduction`, `lesson_proper`, `guided_practice`, `independent_practice`, `homework`) VALUES
(5, 5, 'Monday', '', '', '', '', '', '', '', ''),
(6, 6, 'Monday', 'second', 'second', 'second', 'second', 'second', 'second', 'second', 'second'),
(7, 5, 'Tuesday', 'here\nhere\nhere\nhere\nhere\nhere\nhere\nhere', 'here', 'here\nhere\nhere\nhere\nhere', 'here\nhere\nhere\nhere\nhere\nhere\nhere\nhere\nhere\n', 'here\nhere\nhere\nhere\nhere\nhere\nhere\n', 'here\nhere\nhere\nhere\nhere\nhere\nhere', 'here\nhere\nhere\nhere\nhere\nhere\nhere', 'here\nhere\nhere\nhere\nhere\nhere\nhere'),
(8, 5, 'Wednesday', 'To enhance listening skill.\nTo build words with noun suffixes.\nTo learn world building.', 'Word Building (noun suffixes)', 'Greetings. What is a suffix?. Explain what a suffix is & give examples.', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `accommodations`
--

CREATE TABLE IF NOT EXISTS `accommodations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'number of persons',
  `type` enum('student','guest') NOT NULL DEFAULT 'student',
  `room_number` int(11) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `booking_type` varchar(35) NOT NULL,
  `check_in` int(11) NOT NULL DEFAULT '0',
  `check_out` int(11) NOT NULL DEFAULT '0',
  `pick_up_time` time NOT NULL DEFAULT '00:00:00',
  `send_off_time` time NOT NULL DEFAULT '00:00:00',
  `comment` text NOT NULL,
  `date_added` int(11) NOT NULL,
  `status` enum('reserved','checkin','checkout') DEFAULT 'checkin',
  PRIMARY KEY (`id`),
  KEY `room_number` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `accommodations`
--


-- --------------------------------------------------------

--
-- Table structure for table `accom_additional_payments`
--

CREATE TABLE IF NOT EXISTS `accom_additional_payments` (
  `aap_id` int(11) NOT NULL AUTO_INCREMENT,
  `accom_type` enum('guest','student') NOT NULL,
  `accom_type_id` int(11) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `amount` float(11,2) DEFAULT '0.00',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`aap_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `accom_additional_payments`
--

INSERT INTO `accom_additional_payments` (`aap_id`, `accom_type`, `accom_type_id`, `description`, `amount`, `date_added`) VALUES
(1, 'student', 1, 'extra towel', 521.00, '2012-02-14 15:07:38'),
(2, 'guest', 11, 'test101 towel', 100.00, '2012-02-14 15:31:20'),
(3, 'guest', 11, 'test101 towel', 100.00, '2012-02-14 15:31:31'),
(4, 'guest', 11, 'test101 towel', 100.00, '2012-02-14 15:33:21'),
(5, 'guest', 11, 'test', 1.00, '2012-02-14 15:33:41'),
(6, 'guest', 11, 'test', 1.00, '2012-02-14 15:38:46'),
(7, 'guest', 11, 'sdf', 324.00, '2012-02-14 15:39:03'),
(8, 'guest', 10, 'sdf', 1.00, '2012-02-14 15:48:46'),
(9, 'guest', 4, 'te', 2.00, '2012-02-14 15:51:27'),
(10, 'student', 5, 'asdf', 23.00, '2012-02-14 15:53:42'),
(11, 'guest', 3, 'test', 1.00, '2012-02-14 15:58:30'),
(12, 'guest', 6, 'testssss', 111.00, '2012-02-14 15:58:59'),
(13, 'guest', 5, 'foor food', 30.00, '2012-03-16 10:50:36'),
(14, 'guest', 18, 'dd', 34.00, '2012-05-22 09:30:15'),
(15, 'guest', 18, 'dd', 454.00, '2012-05-22 09:30:15'),
(16, 'guest', 18, '33', 33.00, '2012-05-22 09:30:15'),
(17, 'guest', 18, 'dds', 3233322.00, '2012-05-22 09:30:15'),
(18, 'student', 8, 'mini bar', 100.00, '2012-07-31 16:49:30');

-- --------------------------------------------------------

--
-- Table structure for table `acquisitions`
--

CREATE TABLE IF NOT EXISTS `acquisitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL COMMENT 'the quantity being used',
  `type` int(11) NOT NULL COMMENT 'link to the stock_type table',
  `requested_by` int(11) NOT NULL COMMENT 'emp_id of the employee who requested',
  `date_requested` int(11) NOT NULL COMMENT 'the date requested in unix timestamp',
  PRIMARY KEY (`id`),
  KEY `stock_id` (`stock_id`),
  KEY `type` (`type`),
  KEY `requested_by` (`requested_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `acquisitions`
--


-- --------------------------------------------------------

--
-- Table structure for table `admissions`
--

CREATE TABLE IF NOT EXISTS `admissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` int(11) NOT NULL COMMENT 'student id',
  `status` enum('stand by','cancelled','enrolled','graduated') DEFAULT NULL,
  `invoice_id` int(11) NOT NULL COMMENT 'link to the invoices table',
  `course_id` int(11) NOT NULL COMMENT 'link to the courses table',
  `course_level` int(11) NOT NULL COMMENT 'link to the course_level table',
  `class_type` enum('1:1','1:4','1:8') NOT NULL,
  `type` enum('individual','group','family') NOT NULL,
  `duration` tinyint(2) NOT NULL COMMENT 'number of weeks',
  `adm_date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stud_id` (`stud_id`),
  KEY `course_id` (`course_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `course_level` (`course_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `admissions`
--


-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `units` int(11) NOT NULL DEFAULT '9' COMMENT 'units of measure',
  `tag` varchar(50) NOT NULL COMMENT 'asset''s tag',
  `serial_num` varchar(120) NOT NULL,
  `item_code` varchar(120) NOT NULL COMMENT 'like (processor, memory, etc)',
  `brand` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(25) NOT NULL,
  `po_number` varchar(35) DEFAULT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`asset_id`),
  KEY `units` (`units`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`asset_id`, `units`, `tag`, `serial_num`, `item_code`, `brand`, `description`, `location`, `po_number`, `date_added`) VALUES
(38, 2, '2', '2', '22', 'jkljkl', 'iuiu', 'DAVAO', '', 0),
(44, 3, '3', '7', '7', 'asdasd', 'asd', 'DAVAOa', '', 0),
(49, 3, '1', '345', '432', 'HARDISK', 'dasd', 'CEBU CITYl', '', 0),
(50, 1, '1', '1234', '1232', 'lkmlkm', 'k', 'lkm', '', 0),
(51, 1, '32', '2', '21', 'ioj', 'oi', 'oij', '', 0),
(52, 1, '3', '2', '123', 'asdas', 'asdasd', 'asdasd', '', 0),
(53, 2, '3', '234', '345', 'sdfsdf', 'sfsf', 'DAVAO', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `asset_stats`
--

CREATE TABLE IF NOT EXISTS `asset_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `asset_stats`
--

INSERT INTO `asset_stats` (`id`, `status_name`) VALUES
(1, 'vacant'),
(2, 'assigned'),
(3, 'returned'),
(4, 'transferred');

-- --------------------------------------------------------

--
-- Table structure for table `asset_tracker`
--

CREATE TABLE IF NOT EXISTS `asset_tracker` (
  `tracker_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_tracker` int(11) NOT NULL DEFAULT '0',
  `asset_status` enum('vacant','assigned','returned','transfered') DEFAULT NULL,
  `from_emp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Default to zero if its the first time to be assigned',
  `to_emp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'employee id to be assigned',
  `asset_id` int(11) NOT NULL COMMENT 'the id of the asset in the assets table',
  `date` int(11) NOT NULL DEFAULT '0',
  `received` int(11) NOT NULL DEFAULT '0' COMMENT '0 if not yet received, the date value if received',
  `fin_appr` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  `for_log` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 or 1 if its one then this will be used for logs/history',
  PRIMARY KEY (`tracker_id`),
  KEY `asset_id` (`asset_id`),
  KEY `from_emp_id` (`from_emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `asset_tracker`
--

INSERT INTO `asset_tracker` (`tracker_id`, `parent_tracker`, `asset_status`, `from_emp_id`, `to_emp_id`, `asset_id`, `date`, `received`, `fin_appr`, `for_log`) VALUES
(1, 0, 'assigned', 5, 11, 38, 1324438918, 1324438918, 'approved', '1'),
(2, 0, 'assigned', 5, 11, 44, 1324438918, 1324438918, 'approved', '0'),
(3, 0, 'assigned', 5, 11, 49, 1324438918, 1324438918, 'approved', '0'),
(4, 0, 'assigned', 5, 11, 50, 1324438918, 1324438918, 'approved', '0'),
(5, 0, 'assigned', 5, 13, 51, 1324438918, 1324438918, 'approved', '1'),
(6, 0, 'assigned', 5, 13, 52, 1324438918, 1324438918, 'approved', '1'),
(7, 0, 'assigned', 5, 13, 53, 1324438918, 1324438918, 'approved', '0'),
(24, 5, 'returned', 13, 0, 51, 1324606765, 0, 'cancelled', '1'),
(26, 6, 'transfered', 13, 12, 52, 1324612372, 0, 'cancelled', '1'),
(27, 7, 'transfered', 13, 12, 53, 1324617933, 0, 'cancelled', '1'),
(28, 6, 'returned', 13, 0, 52, 1324628835, 0, 'cancelled', '1'),
(29, 6, 'returned', 13, 0, 52, 1324629348, 0, 'cancelled', '1'),
(30, 5, 'transfered', 13, 12, 51, 1324630881, 1324869187, 'approved', '1'),
(31, 30, 'transfered', 12, 13, 51, 1324870062, 0, 'cancelled', '1'),
(32, 30, 'returned', 12, 0, 51, 1324870080, 0, 'cancelled', '1'),
(33, 6, 'transfered', 13, 12, 52, 1324870195, 1324870429, 'approved', '0'),
(34, 33, 'transfered', 12, 13, 52, 1324889140, 0, 'cancelled', '1'),
(35, 33, 'returned', 12, 0, 52, 1324889153, 0, 'cancelled', '1'),
(36, 33, 'returned', 12, 0, 52, 1330305856, 0, 'cancelled', '1'),
(37, 30, 'returned', 12, 0, 51, 1330305856, 0, 'cancelled', '1'),
(38, 7, 'transfered', 13, 12, 53, 1330399350, 0, 'cancelled', '1'),
(39, 30, 'returned', 12, 0, 51, 1330405339, 0, 'cancelled', '1'),
(40, 33, 'returned', 12, 0, 52, 1330405340, 0, 'cancelled', '1'),
(41, 30, 'transfered', 12, 11, 51, 1330418070, 0, 'pending', '0'),
(42, 1, 'transfered', 11, 10, 38, 1341798374, 0, 'pending', '0');

-- --------------------------------------------------------

--
-- Table structure for table `as_account_categories`
--

CREATE TABLE IF NOT EXISTS `as_account_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(25) NOT NULL COMMENT 'ex. assets, liabilities, equity/capital revenue/income, expenses',
  `is_pl` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `as_account_categories`
--

INSERT INTO `as_account_categories` (`id`, `category_name`, `is_pl`) VALUES
(1, 'asset', '0'),
(2, 'liability', '0'),
(3, 'equity', '0'),
(4, 'income', '1'),
(5, 'expense', '1');

-- --------------------------------------------------------

--
-- Table structure for table `as_account_type`
--

CREATE TABLE IF NOT EXISTS `as_account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(35) NOT NULL COMMENT 'ex. bank, accounts, receivable, advances to suppliers, accounts payable, fixed asset, other current asset... etc. (note: this is based in quickbooks)',
  `category_id` int(11) NOT NULL COMMENT 'linked to the as_acctcategories table',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `as_account_type`
--

INSERT INTO `as_account_type` (`id`, `type_name`, `category_id`) VALUES
(1, 'bank', 1),
(2, 'ar', 1),
(3, 'ocasset', 1),
(4, 'fixasset', 1),
(5, 'oasset', 1),
(6, 'ap', 2),
(7, 'ocliab', 2),
(8, 'ltliab', 2),
(9, 'equity', 3),
(10, 'inc', 4),
(11, 'cogs', 4),
(12, 'exp', 5),
(13, 'exinc', 4),
(14, 'exexp', 5),
(15, 'nonposting', 5);

-- --------------------------------------------------------

--
-- Table structure for table `as_assets`
--

CREATE TABLE IF NOT EXISTS `as_assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `units` int(11) NOT NULL COMMENT 'units of measure | disregard this column since assets don''t have units of measure',
  `tag` varchar(50) NOT NULL COMMENT 'asset''s tag',
  `serial_num` varchar(120) NOT NULL,
  `item_code` varchar(120) NOT NULL COMMENT 'like (processor, memory, etc)',
  `brand` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(25) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'link to the asset_stats table',
  `owner` int(11) NOT NULL DEFAULT '0' COMMENT 'employee id',
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `as_assets`
--


-- --------------------------------------------------------

--
-- Table structure for table `as_chartdetails`
--

CREATE TABLE IF NOT EXISTS `as_chartdetails` (
  `account_code` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `banknum` varchar(25) NOT NULL COMMENT 'the bank number, if there is any',
  PRIMARY KEY (`account_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `as_chartdetails`
--


-- --------------------------------------------------------

--
-- Table structure for table `as_chartofaccounts`
--

CREATE TABLE IF NOT EXISTS `as_chartofaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_code` int(11) NOT NULL COMMENT 'the account code',
  `parent_code` int(11) NOT NULL DEFAULT '0' COMMENT 'the parent code and is refering to the account_code in this table',
  `account_name` varchar(65) NOT NULL,
  `account_type_id` int(11) NOT NULL COMMENT 'refering to the as_account_type table',
  PRIMARY KEY (`id`),
  KEY `account_type_id` (`account_type_id`),
  KEY `account_code` (`account_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=375 ;

--
-- Dumping data for table `as_chartofaccounts`
--

INSERT INTO `as_chartofaccounts` (`id`, `account_code`, `parent_code`, `account_name`, `account_type_id`) VALUES
(1, 1010100, 1010101, 'Cash on Hand', 1),
(2, 1010101, 1010100, 'Petty Cash Fund', 1),
(3, 1010102, 1010100, 'Revolving Fund', 1),
(4, 1010103, 1010100, 'Key Money Fund', 1),
(5, 1010104, 1010100, 'Marketing Fund', 1),
(6, 1010300, 0, 'Cash In Bank', 1),
(7, 1010301, 1010300, 'SCB Peso Checking - 688', 1),
(8, 1010302, 1010300, 'SCB Dollar Savings - 722', 1),
(9, 1010303, 1010300, 'RCBC Peso Checking - 495', 1),
(10, 1010304, 1010300, 'RCBC Dollar Savings - 922', 1),
(11, 1010305, 1010300, 'BDO Peso Checking -277', 1),
(12, 1010306, 1010300, 'BDO Dollar Account - 188', 1),
(13, 1010307, 1010300, 'Union Bank Peso Savings - 404', 1),
(14, 1010308, 1010300, 'Union Bank Dollar Savings - 835', 1),
(15, 1010309, 1010300, 'Union Bank Peso Savings - 118', 1),
(16, 1010310, 1010300, 'SCB Hnkg USD acct/CELI Ltd- 037', 1),
(17, 6014903, 0, 'Salary Adjustments', 1),
(18, 1020100, 0, 'Account Receivable', 2),
(19, 1020101, 1020100, 'Account Receivable-Trade', 2),
(20, 1020102, 1020100, 'Account Receivable-Others', 2),
(21, 1020103, 1020100, 'Accounts Receivable-NCLEX', 2),
(22, 1020104, 1020100, 'Accounts Receivable - Caregiver', 2),
(23, 1020108, 1020104, 'Acc. Receivable - Non Schollars', 2),
(24, 1020109, 1020104, 'Acc. Receivable - TESDA scholar', 2),
(25, 1020105, 1020100, 'Accounts Receivable-Call Center', 2),
(26, 1020106, 1020100, 'Accounts Receivable-Credit Card', 2),
(27, 1020107, 1020100, 'Accounts Receivable - Dormitory', 2),
(29, 1020300, 0, 'Advances to Officers & Employee', 3),
(30, 1020301, 1020300, 'Adv to Off/Employee-Kainos', 3),
(31, 1020302, 1020300, 'Adv to Off/Employee-SSS', 3),
(32, 1020303, 1020300, 'Adv to Off/Employees-Canteen', 3),
(33, 1020304, 1020300, 'Adv To Off/Employees-Telephone', 3),
(34, 1020305, 1020300, 'Adv To Off/Employees-Others', 3),
(35, 1020400, 0, 'Advances to/from Affiliates', 3),
(36, 1, 1020400, 'Adv. to/from CELI-Vietnam', 3),
(37, 1020401, 1020400, 'Adv to/frm CELI Ltd', 3),
(38, 1020402, 1020400, 'Adv to/frm IAFT-NCM', 3),
(39, 1020403, 1020400, 'Adv to/frm IAFT-CELI', 3),
(40, 1020404, 1020400, 'Adv to/from CELI-Vietnam', 3),
(41, 1020405, 1020400, 'Adv to/from Bigfoot Venture Ltd', 3),
(42, 1020406, 1020400, 'Adv to/from BF Capital Ventures', 3),
(43, 1020407, 1020400, 'Adv to/from Bigfoot Entertainme', 3),
(44, 1020408, 1020400, 'Adv to/from Bigfoot Properties', 3),
(45, 1020409, 1020400, 'Adv to/from Bigfoot Communicati', 3),
(46, 1020410, 1020400, 'Adv to/from CL BVI', 3),
(47, 1020411, 1020400, 'Adv to/from IAFT', 3),
(48, 1020412, 1020400, 'Adv to/from JL Mercado', 3),
(49, 1020413, 1020400, 'Adv to/from John Lawrence Merca', 3),
(50, 1020414, 1020400, 'Advances to/from Poddy O''Bryan', 3),
(51, 1020500, 0, 'Advances to Suppliers', 3),
(52, 1020600, 0, 'Other Receivables', 3),
(53, 1020900, 0, 'Undeposited Funds', 3),
(54, 1030100, 0, 'Prepaid Expenses', 3),
(55, 1030101, 1030100, 'Prepaid Rental', 3),
(56, 1030102, 1030100, 'Prepaid Supplies', 3),
(57, 1030103, 1030100, 'Prepaid Cards', 3),
(58, 1030200, 0, 'Inventory', 3),
(59, 1030201, 1030200, 'Office Supplies on Hand', 3),
(60, 1030202, 1030200, 'Mini Bar Supplies on Hand', 3),
(61, 1030203, 1030200, 'C-Spot Supplies on Hand', 3),
(62, 1030204, 1030200, 'Housekeeping Supplies on Hand', 3),
(63, 1030205, 1030200, 'Kitchen Supplies on Hand', 3),
(64, 1030206, 1030200, 'Amenities supplies on Hand', 3),
(65, 1030207, 1030200, 'ACAD Supplies on Hand', 3),
(66, 1030208, 0, 'Larry''s Place', 3),
(67, 1120, 0, 'Inventory Asset', 3),
(68, 1040000, 0, 'Fixed Assets', 4),
(69, 1, 1040000, 'Accumulated Depreciation', 4),
(70, 1040100, 1040000, 'Equipment', 4),
(71, 1040101, 1040100, 'Audio Equipment', 4),
(72, 1040102, 1040100, 'Camera Equipment', 4),
(73, 1040103, 1040100, 'Computer Hardware & Peripherals', 4),
(74, 1040104, 1040100, 'Computer Softwares', 4),
(75, 1040105, 1040100, 'Kitchen Eqt & Appliances', 4),
(76, 1040106, 1040100, 'Lighting Equipment', 4),
(77, 1040107, 1040100, 'Maintenance & Other Safety Eqpt', 4),
(78, 1040108, 1040100, 'Network Component & Peripherals', 4),
(79, 1040109, 1040100, 'Office Equipment', 4),
(80, 1040110, 1040100, 'Transpo & Other Service Vehicle', 4),
(81, 1040111, 1040100, 'Video Equipment', 4),
(82, 1040112, 1040100, 'Medical Equipment', 4),
(83, 1040113, 1040100, 'Laundry Equipment', 4),
(84, 1040200, 1040000, 'Dormitory Assets', 4),
(85, 1040201, 1040200, 'Dormitory Equipment', 4),
(86, 1040202, 1040200, 'Dormitory Kitchen Utensils', 4),
(87, 1040250, 1040000, 'Furniture and Fixtures', 4),
(88, 1040300, 1040000, 'Leasehold Improvements', 4),
(89, 1040400, 1040000, 'Library Asset', 4),
(90, 1040600, 1040000, 'Land', 4),
(91, 1040700, 1040000, 'Building', 4),
(92, 1040800, 1040000, 'Service Vehicle', 4),
(93, 1040899, 1040000, 'Fixed Assets-Others', 4),
(94, 1040500, 0, 'Construction In Progress', 4),
(95, 1040501, 1040500, 'CIP-Leasehold Improvements', 4),
(96, 1040502, 1040500, 'CIP-Land', 4),
(97, 1040503, 1040500, 'CIP-Building', 4),
(98, 1040504, 1040500, 'CIP-Safety & Maintenance Equipm', 4),
(99, 1040505, 1040500, 'CIP-Kitchen Equipment and App.', 4),
(100, 1040506, 1040500, 'CIP-Others', 4),
(101, 1040507, 1040500, 'CIP-Coffee Shop', 4),
(102, 1040900, 0, 'Accumulated Depreciation', 4),
(103, 1040901, 1040900, 'Acc.Dep.-Equipment', 4),
(104, 1040902, 1040901, 'Acc. Dep.-Network Components &', 4),
(105, 1040903, 1040901, 'Acc.Dep.-Audio Equipment', 4),
(106, 1040904, 1040901, 'Acc.Dep.-Camera Equipment', 4),
(107, 1040905, 1040901, 'Acc.Dep.-Computer Hardwares & P', 4),
(108, 1040906, 1040901, 'Acc.Dep.-Computer Softwares', 4),
(109, 1040907, 1040901, 'Acc.Dep.-Kitchen Eqt & Applianc', 4),
(110, 1040908, 1040901, 'Acc.Dep.-Lighting Equipment', 4),
(111, 1040909, 1040901, 'Acc.Dep.-Maintenance&OtherSafet', 4),
(112, 1040910, 1040901, 'Acc.Dep.-Office Equipment', 4),
(113, 1040911, 1040901, 'Acc.Dep.-Transpo&OtherVehicles', 4),
(114, 1040912, 1040901, 'Acc.Dep.-Video Equipment', 4),
(115, 1040918, 1040901, 'Acc.Dep.-Medical Equipment', 4),
(116, 1040919, 1040901, 'Acc.Dep.-Laundry Equipment', 4),
(117, 1040922, 1040901, 'Acc.Dep.-Hotel Equipment', 4),
(118, 1040923, 1040901, 'Acc.Dep.-Dormitory Equipment', 4),
(119, 1040913, 1040900, 'Acc.Dep.-Furniture and Fixtures', 4),
(120, 1040914, 1040900, 'Acc.Dep.-Leasehold Improvements', 4),
(121, 1040915, 1040900, 'Acc.Dep.-Library Asset', 4),
(122, 1040917, 1040900, 'Acc.Dep.-Building', 4),
(123, 1040920, 1040900, 'Acc.Dep.-Fixed Assets-Others', 4),
(124, 1040921, 1040900, 'Acc.Dep.-Service Vehicle', 4),
(125, 1050100, 0, 'Refundable Deposit', 5),
(126, 1050200, 0, 'Deferred Charges', 5),
(127, 1050201, 1050200, 'Deferred-CELI Ltd', 5),
(128, 1050202, 1050200, 'Deferred-CL BVI', 5),
(129, 1050203, 1050200, 'Deferred-BVI Ltd', 5),
(130, 1050204, 1050200, 'Deferred-BF Entertainment', 5),
(131, 1050205, 1050200, 'Deferred-NOLCO', 5),
(132, 1050300, 0, 'Creditable Withholding Tax', 5),
(133, 1050400, 0, 'Input VAT', 5),
(134, 1050500, 0, 'Other Assets', 5),
(135, 1050600, 0, 'Security Deposit-MECO', 5),
(136, 1050700, 0, 'Advance Rental', 5),
(137, 1050800, 0, 'Start Up Costs - BPO Fin&Acctg', 5),
(138, 101, 0, '+', 6),
(139, 2010100, 0, 'Accounts Payable', 6),
(140, 2010101, 2010100, 'Accounts Payable-Trade', 6),
(141, 2010102, 2010100, 'Accounts Payable-Others', 6),
(142, 2010103, 2010100, 'Accounts payable-L.M. Cred.Card', 6),
(143, 101, 0, 'Payroll Liabilities', 7),
(144, 2010200, 0, 'Advances from Affiliates', 7),
(145, 2010300, 0, 'Security Deposit on Rental', 7),
(146, 2010400, 0, 'Output VAT', 7),
(147, 2020100, 0, 'SSS Payable', 7),
(148, 2020101, 2020100, 'SSS Premium Payable', 7),
(149, 2020102, 2020100, 'SSS Salary Loans Payable', 7),
(150, 2020103, 2020100, 'SSS Payable - Others', 7),
(151, 2020200, 0, 'HDMF Payable', 7),
(152, 2020201, 2020200, 'HDMF Premium Payable', 7),
(153, 2020202, 2020200, 'HDMF Multi-Purpose Loans', 7),
(154, 2020203, 2020200, 'HDMF Housing Loans Payable', 7),
(155, 2020300, 0, 'PHIC Payable', 7),
(156, 2020301, 2020300, 'PHIC Premium Payable', 7),
(157, 2020400, 0, 'Withholding Taxes Payable', 7),
(158, 2020401, 2020400, 'WTP-Compensation', 7),
(159, 2020402, 2020400, 'WTP-Expanded', 7),
(160, 2020403, 2020400, 'WTP-Final', 7),
(161, 2020404, 2020400, 'Tax Refund', 7),
(162, 2020500, 0, 'Retention Payable', 7),
(163, 2020600, 0, 'Advances from Stockholders', 7),
(164, 2030100, 0, 'Accrued Expense Payable', 7),
(165, 2030101, 2030100, 'Accrued Rental', 7),
(166, 2030102, 2030100, 'Accrued Expense Payable-Others', 7),
(167, 2030103, 2030100, 'Accrued Salaries Payable', 7),
(168, 2030200, 0, 'Unearned Revenue', 7),
(169, 2030900, 0, 'Other Payable', 7),
(170, 2030901, 2030900, 'Other Payable-TOEIC', 7),
(171, 2030902, 2030900, 'Other Payable-Key Money', 7),
(172, 2030903, 2030900, 'Other Payable-Gym Usage', 7),
(173, 2030909, 2030900, 'Other Payable-Others', 7),
(174, 2030910, 2030900, 'Other Payable-Damages', 7),
(175, 2030911, 2030900, 'Other Payble-Temp Borwng-officr', 7),
(176, 2040100, 0, 'Deposit for Future Subscription', 8),
(177, 2050100, 0, 'Notes Payable', 8),
(178, 101, 0, 'Opening Bal Equity', 9),
(179, 3010100, 0, 'Capital Stock Subscribed', 9),
(180, 3010200, 0, 'CS Subscription Receivable', 9),
(181, 3020100, 0, 'Retained Earnings', 9),
(182, 4010000, 0, 'Gross Sales', 10),
(183, 4010100, 4010000, 'Sales', 10),
(184, 4010101, 4010100, 'General English', 10),
(185, 4010102, 4010100, 'Immersion Courses', 10),
(186, 4010103, 4010100, 'Sales-Others', 10),
(187, 4010104, 4010100, 'NCLEX Processing', 10),
(188, 4010105, 4010100, 'Caregiver Courses', 10),
(189, 4010106, 4010105, 'Caregiver - Scholars', 10),
(190, 4010107, 4010105, 'Caregiver - Non Scholars Tuitio', 10),
(191, 4010108, 4010100, 'Rental Income', 10),
(192, 4010109, 4010100, 'Call Center Sales', 10),
(193, 4010118, 4010100, 'Application Fees Income', 10),
(194, 4010119, 4010100, 'SSP Processing Income', 10),
(195, 4010110, 4010000, 'NCLEX Review Courses', 10),
(196, 4010111, 4010000, 'NCLEX Training Courses', 10),
(197, 4010113, 4010000, 'Call Center Tuition Fee', 10),
(198, 4010114, 4010000, 'Call Center - Scholarship', 10),
(199, 4010115, 4010000, 'English for Nurses Program(ENP)', 10),
(200, 4010116, 4010000, 'Int''l. Nurses Training Program', 10),
(201, 4010200, 4010000, 'Export of Services', 10),
(202, 4010300, 4010000, 'Sales Discounts', 10),
(203, 4010400, 4010000, 'Sales Returns', 10),
(204, 4010112, 0, 'Caregiving Review Courses', 10),
(205, 4010117, 0, 'Hotel Sales-Students Accomodati', 10),
(206, 5000, 0, 'Cost of Goods Sold', 11),
(207, 5010000, 0, 'Cost of Sales', 11),
(208, 5010101, 5010000, 'Student Activity Cost', 11),
(209, 5010102, 5010000, 'SSP Processing Expense', 11),
(210, 5010103, 5010000, 'Teaching Materials', 11),
(211, 5010104, 5010000, 'SAC-Immersion Courses', 11),
(212, 5010105, 5010000, 'Students'' Accommodation', 11),
(213, 5010106, 5010000, 'Rentals', 11),
(214, 5010107, 5010000, 'Food Accommodation', 11),
(215, 5010108, 5010000, 'Camps/Family Program Cost', 11),
(216, 5010109, 5010000, 'COS-Others', 11),
(217, 5010110, 5010000, 'Cspot & Waynes Place', 11),
(218, 5010111, 5010000, 'Hotel Amenities', 11),
(219, 5010112, 5010000, 'Annual Fee', 11),
(220, 501112, 0, 'JY Square Over-all Dept', 11),
(221, 5020000, 0, 'Cost of Sales-Immersion Courses', 11),
(222, 5020001, 0, 'Cost of Sales- NCLEX', 11),
(223, 5020002, 0, 'Cost of Sales-Caregiver', 11),
(224, 5020003, 0, 'Cost of Sales - Rental', 11),
(225, 5020004, 0, 'Cost of Sales- BPO Fin & Acctg.', 11),
(226, 5020005, 0, 'Marketing Dept. -JY', 11),
(227, 101, 0, 'BDO Peso Checking -277', 12),
(228, 101, 0, 'Makro Pilipinas', 12),
(229, 101, 0, 'Payroll Expenses', 12),
(230, 6010000, 0, 'Salaries and Wages Expense', 12),
(231, 6011000, 0, 'Overtime Pay', 12),
(232, 6012000, 0, 'Night Differential Pay', 12),
(233, 6013000, 0, '13th Month Pay Exp', 12),
(234, 6014000, 0, 'Employee Benefits', 12),
(235, 6014100, 6014000, 'Vacation Leave', 12),
(236, 6014200, 6014000, 'Sick Leave', 12),
(237, 6014300, 6014000, 'Bonus', 12),
(238, 6014400, 6014000, 'Rice Subsidy', 12),
(239, 6014500, 6014000, 'Separation Pay', 12),
(240, 6014600, 6014000, 'Other Incentives', 12),
(241, 6014700, 6014000, 'Transportation Allowance', 12),
(242, 6014800, 6014000, 'Food Allowance', 12),
(243, 6014900, 6014000, 'Performance Incentive', 12),
(244, 6014901, 6014000, 'Task Allowance', 12),
(245, 6014902, 6014000, 'Personal Allowance', 12),
(246, 6014001, 0, 'Other Employee Benefits', 12),
(247, 6014010, 6014001, 'Christmas Give aways', 12),
(248, 6014020, 6014001, 'Maternity Expenses - SSS', 12),
(249, 6014002, 0, 'Food & Subsistence - Admin', 12),
(250, 6014030, 0, 'Holiday Pay', 12),
(251, 6015000, 0, 'Allowances Expense', 12),
(252, 6015100, 6015000, 'Transportation Allowance', 12),
(253, 6015200, 6015000, 'Bereavement Assitance', 12),
(254, 6015300, 6015000, 'OJT/Intern', 12),
(255, 6015400, 6015000, 'Clothing Allowance', 12),
(256, 6015500, 6015000, 'Company Parties/Outings', 12),
(257, 6015600, 6015000, 'Meals', 12),
(258, 6015700, 6015000, 'Other Allowance Expense', 12),
(259, 6016000, 0, 'SSS Premium Expense', 12),
(260, 6016100, 6016000, 'SSS Employee Premium Expense', 12),
(261, 6016200, 6016000, 'SSS Employer Premium Expense', 12),
(262, 6017000, 0, 'HDMF Premium Expense', 12),
(263, 6017100, 6017000, 'HDMF Employee Premium Expense', 12),
(264, 6017200, 6017000, 'HDMF Employer Premium Expense', 12),
(265, 6018000, 0, 'PHIC Premium Expense', 12),
(266, 6018100, 6018000, 'PHIC Employee Premium Expense', 12),
(267, 6018200, 6018000, 'PHIC Employer Premium Expense', 12),
(268, 6019000, 0, 'Professional Fees', 12),
(269, 6019100, 6019000, 'Legal Fees', 12),
(270, 6019200, 6019000, 'Audit Fees', 12),
(271, 6019300, 6019000, 'Teaching/Mentoring Fees', 12),
(272, 6019900, 6019000, 'Other Professional Fees', 12),
(273, 6020000, 0, 'Services Fees', 12),
(274, 6020100, 6020000, 'Janitorial Services', 12),
(275, 6020200, 6020000, 'Security Services', 12),
(276, 6020300, 6020000, 'Pest Control Services', 12),
(277, 6020900, 6020000, 'Other Services Fees', 12),
(278, 6021000, 0, 'Rental Expense', 12),
(279, 6021100, 6021000, 'Equipment Rental', 12),
(280, 6021200, 6021000, 'Transpo/Vehicle Rental', 12),
(281, 6021300, 6021000, 'Office Rental', 12),
(282, 6021400, 6021000, 'Houses/Condo Rental', 12),
(283, 6021900, 6021000, 'Other Rental Expense', 12),
(284, 6022000, 0, 'Advertising Expense', 12),
(285, 6022100, 6022000, 'Newspaper/Magazine Ads', 12),
(286, 6022200, 6022000, 'Posters and Banners', 12),
(287, 6022300, 6022000, 'Manpower Recruitment', 12),
(288, 6022400, 6022000, 'Souvenirs (t-shirts,caps,etc.)', 12),
(289, 6022500, 6022000, 'TV/Radio Ads', 12),
(290, 6022600, 6022000, 'Website', 12),
(291, 6022900, 6022000, 'Other Advertising Expense', 12),
(292, 6023000, 0, 'Supplies Expense', 12),
(293, 6023100, 6023000, 'Office Supplies', 12),
(294, 6023200, 6023000, 'Non-consummable Supplies', 12),
(295, 6023300, 6023000, 'Cleaning Supplies and Materials', 12),
(296, 6023400, 6023000, 'Dues and Subscriptions', 12),
(297, 6023900, 6023000, 'Supplies Expense-Others', 12),
(298, 6023901, 6023000, 'Supplies Expense - Medicines', 12),
(299, 6024000, 0, 'Travel Expense', 12),
(300, 6024100, 6024000, 'Travel Expense-Local', 12),
(301, 6024101, 6024100, 'Airfare-Local', 12),
(302, 6024102, 6024100, 'Hotel Accommodation-Local', 12),
(303, 6024109, 6024100, 'Other Travel Expense-Local', 12),
(304, 6024200, 6024000, 'Travel Expense-Foreign', 12),
(305, 6024201, 6024200, 'Airfare-Foreign', 12),
(306, 6024202, 6024200, 'Hotel Accommodation-Foreign', 12),
(307, 6024203, 6024200, 'Visa Extension', 12),
(308, 6024209, 6024200, 'Other Travel Expense-Foreign', 12),
(309, 6025000, 0, 'Utilities Expense', 12),
(310, 6025100, 6025000, 'Electricity', 12),
(311, 6025200, 6025000, 'Water', 12),
(312, 6026000, 0, 'Communication Expense', 12),
(313, 6026100, 6026000, 'Mailing, Postage and Delivery', 12),
(314, 6026200, 6026000, 'Mobile Phones', 12),
(315, 6026300, 6026000, 'Telephone Expense', 12),
(316, 6026301, 6026300, 'Fixed Line Rental', 12),
(317, 6026302, 6026300, 'Call Charges', 12),
(318, 6026303, 6026300, 'Internet/DSL Connections', 12),
(319, 6026304, 6026300, 'Telephone Expense-Others', 12),
(320, 6026900, 6026000, 'Communication Expense-Others', 12),
(321, 6027000, 0, 'Insurance Expense', 12),
(322, 6028000, 0, 'Transportation Expense', 12),
(323, 6028100, 6028000, 'Fare', 12),
(324, 6028200, 6028000, 'Fuel Expenses', 12),
(325, 6028300, 6028000, 'Parking Fee', 12),
(326, 6028400, 6028000, 'Service Vehicle', 12),
(327, 6028900, 6028000, 'Other Transportation Expense', 12),
(328, 6029000, 0, 'Repair & Maintenance', 12),
(329, 6029100, 6029000, 'Computer Repairs', 12),
(330, 6029200, 6029000, 'Equipment Repairs', 12),
(331, 6029300, 6029000, 'Building Repairs', 12),
(332, 6029400, 6029000, 'Kitchen Repairs', 12),
(333, 6029500, 6029000, 'Furniture and Fixtures Repair', 12),
(334, 6029600, 6029000, 'Vehicle Repairs', 12),
(335, 6029700, 6029000, 'Fuel-Generator', 12),
(336, 6029800, 6029000, 'Pool Maintenance', 12),
(337, 6030000, 0, 'Training and Seminar Fees', 12),
(338, 6030100, 0, 'Nursing Examination Expense', 12),
(339, 6030500, 0, 'Referral fees expense', 12),
(340, 6031000, 0, 'Commission Expense', 12),
(341, 6031001, 6031000, 'Commision Incentive Expense', 12),
(342, 6032000, 0, 'Administrative Expense', 12),
(343, 6033000, 0, 'Taxes & Licenses', 12),
(344, 6034000, 0, 'Depreciation Expense', 12),
(345, 6035000, 0, 'Credit Card Charges', 12),
(346, 6036000, 0, 'Other Taxes', 12),
(347, 6037000, 0, 'Interest Expense', 12),
(348, 6037500, 0, 'Subscription Expense', 12),
(349, 6037501, 6037500, 'online banking', 12),
(350, 6038000, 0, 'Bank Service Charges', 12),
(351, 6038001, 6038000, 'Bank Charge on Check Returned', 12),
(352, 6038002, 6038000, 'Bank Charge on SPO', 12),
(353, 6038500, 0, 'Facilitation Fees', 12),
(354, 6039000, 0, 'Miscellaneous Expenses', 12),
(355, 6039001, 6039000, 'Penalty on Rental', 12),
(356, 6039002, 6039000, 'Penalty/surcharge on Utilities', 12),
(357, 6039999, 0, 'Uncategorized Expenses', 12),
(358, 6040000, 0, 'Freight and Handling', 12),
(359, 6041000, 0, 'Donations & charitable cont.', 12),
(360, 6041001, 6041000, 'Community Outreach Program', 12),
(361, 6051000, 0, 'Representation', 12),
(362, 606000, 0, 'Student Refund', 12),
(363, 7010000, 0, 'Other Income', 13),
(364, 7010001, 7010000, 'Other Income Employees Meal', 13),
(365, 7010002, 7010000, 'Realized Gain on Forex Exchange', 13),
(366, 7020000, 0, 'Interest Income', 13),
(367, 8010000, 0, 'Other Expenses', 14),
(368, 8010100, 8010000, 'Gain (Loss) on Forex Exchange', 14),
(369, 8010200, 8010000, 'Provision for Income Tax', 14),
(370, 8010300, 8010000, 'Realized Gain(Loss) on Forex Ex', 14),
(371, 2, 0, 'Purchase Orders', 15),
(372, 4, 0, 'Estimates', 15),
(373, 5, 0, 'Sales Orders', 15),
(374, 12345, 1010300, 'This is a test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `as_class`
--

CREATE TABLE IF NOT EXISTS `as_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `as_class`
--

INSERT INTO `as_class` (`id`, `name`) VALUES
(1, 'Academic Department'),
(2, 'BPO Finance & Acctg'),
(3, 'Call Center'),
(4, 'Caregiver'),
(5, 'CELI Hotel Mactan'),
(6, 'CELI Restobar'),
(7, 'Cofee Shop'),
(8, 'CSpot'),
(9, 'Customer Service'),
(10, 'ESL'),
(11, 'Finance Dept.-Mactan'),
(12, 'Human Resource Dept.'),
(13, 'Immersion'),
(14, 'IT'),
(15, 'JY Square'),
(16, 'JY Square:BPO Finance & Acctg'),
(17, 'Larry''s Place'),
(18, 'Mactan'),
(19, 'Marketing Dept'),
(20, 'NCLEX'),
(21, 'Ramos'),
(22, 'Rentals'),
(23, 'Right Way Program'),
(24, 'Sales/Marketing'),
(25, 'System Ad Dept.'),
(26, 'Tenants'),
(27, 'TESDA');

-- --------------------------------------------------------

--
-- Table structure for table `as_general_ledger`
--

CREATE TABLE IF NOT EXISTS `as_general_ledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_code` int(11) NOT NULL,
  `transaction_details` varchar(255) NOT NULL,
  `amount` float(11,2) NOT NULL DEFAULT '0.00' COMMENT 'The amount of the transaction. Negative value if it was credited and positive it it was debited',
  `transaction_date` int(11) NOT NULL DEFAULT '0' COMMENT 'default to 0 as of this moment, but remove this when this database goes live',
  `payment_terms` int(11) NOT NULL DEFAULT '5' COMMENT 'the payment terms of the transaction especially if its an account receivable',
  `je_type` varchar(30) DEFAULT NULL,
  `date_added` int(11) NOT NULL DEFAULT '0' COMMENT 'default to 0 as of this moment, but remove this when this database goes live',
  PRIMARY KEY (`id`),
  KEY `account_code` (`account_code`),
  KEY `payment_terms` (`payment_terms`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `as_general_ledger`
--

INSERT INTO `as_general_ledger` (`id`, `account_code`, `transaction_details`, `amount`, `transaction_date`, `payment_terms`, `je_type`, `date_added`) VALUES
(11, 1010300, 'Caregiving manual', 264.00, 1319493600, 5, 'sp-12', 1319521346),
(12, 7010000, 'Caregiving manual', -264.00, 1319493600, 5, 'sp-12', 1319521346),
(13, 1020100, 'Enrollment Fee', 6828.00, 1319522400, 1, 'sp-13', 1319521368),
(14, 4010105, 'Enrollment Fee', -6828.00, 1319522400, 1, 'sp-13', 1319521368),
(15, 1030200, 'An egg from the eggs', 60.00, 1319493600, 1, 'po-3', 1319521392),
(16, 2010100, 'An egg from the eggs', -60.00, 1319493600, 1, 'po-3', 1319521392),
(17, 1010307, 'Course Fee', 115.00, 1319580000, 5, 'sp-14', 1319609297),
(18, 4010105, 'Course Fee', -115.00, 1319580000, 5, 'sp-14', 1319609297),
(19, 1010307, 'Application Fee', 595.00, 1319580000, 5, 'sp-15', 1319609297),
(20, 4010101, 'Application Fee', -595.00, 1319580000, 5, 'sp-15', 1319609297),
(21, 1010300, 'Caregiving manual', 21.00, 1320361200, 5, 'sp-15', 1320369283),
(22, 7010000, 'Caregiving manual', -21.00, 1320361200, 5, 'sp-15', 1320369283),
(23, 1010300, '12', 2431.00, 1320361200, 5, 'sp-16', 1320369380),
(24, 4010102, '12', -2431.00, 1320361200, 5, 'sp-16', 1320369380);

-- --------------------------------------------------------

--
-- Table structure for table `as_invitems`
--

CREATE TABLE IF NOT EXISTS `as_invitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `description` varchar(125) NOT NULL,
  `account_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_code` (`account_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `as_invitems`
--

INSERT INTO `as_invitems` (`id`, `name`, `description`, `account_code`) VALUES
(1, '12% Value Added Tax', '12 % Value Added Tax', 2010400),
(2, 'Call Center', '', 4010102),
(3, 'Call Center:Call Center-Book', 'Call Center - Book', 4010102),
(4, 'Call Center:Call Center Training', 'Call Center Training', 4010102),
(5, 'Call Center:Referral Fee', 'Referral Fee', 7010000),
(6, 'Cancelled', 'Cancelled', 4010000),
(7, 'Caregiver', '', 4010100),
(8, 'Caregiver:Caregiving Books', 'Caregiving manual', 7010000),
(9, 'Caregiver:Course Discount', 'course fee discount', 4010300),
(10, 'Caregiver:Course Fee', 'Course Fee', 4010105),
(11, 'Caregiver:Enrollment Fee', 'Enrollment Fee', 4010105),
(12, 'CELI Dorm Income', '', 4010108),
(13, 'CELI Dorm Income:Rental Income', '', 4010108),
(14, 'CSPOT Sales', 'Various Sales', 7010000),
(15, 'Deferred Charges', '', 1050200),
(16, 'Deferred Charges:Deferred-BF Entertainment', '', 7010000),
(17, 'Deferred Charges:Deferred-BV Ltd.', '', 1020405),
(18, 'Deferred Charges:Deferred-Celi Ltd', '', 1050201),
(19, 'Deferred Charges:Deferred Local', '', 1050200),
(20, 'Dormitory Services', 'Dormitory Services for Students', 4010200),
(21, 'Electrical Charges', 'Electrical Charges', 7010000),
(22, 'English for Nursing Program', 'English for Nurses Program (ENP)', 4010000),
(23, 'General English', 'Course Duration:\\nAccommodation:\\n', 4010101),
(24, 'General English:1. GE- Application Fee', 'Application Fee', 4010101),
(25, 'General English:10. Agent Commissiion', ' Agent Commission', 6031000),
(26, 'General English:10. Others-GE', 'Others:', 4010101),
(27, 'General English:11. Diving Courses', 'Diving Courses', 4010101),
(28, 'General English:11. Gym Usage-GE', 'Gym Usage: Period', 4010101),
(29, 'General English:11.Gym Usage-SAC', 'Gym Usage: Period', 5010101),
(30, 'General English:12. Visa Extension', 'Visa Extension', 4010101),
(31, 'General English:13. Class swapping', 'Class Swapping', 4010101),
(32, 'General English:14.  Key Money', 'Key Money', 2030902),
(33, 'General English:15. Additional Class', ' Additional Class', 4010100),
(34, 'General English:2.   GE-Course Fee', 'Course Fee', 4010101),
(35, 'General English:3. GE-Accommodation', 'Accommodation:', 4010101),
(36, 'General English:4. Discounts', 'Discount', 4010300),
(37, 'General English:5. SSP', 'SSP', 4010101),
(38, 'General English:6. Room Key Deposit-OP', 'Room Key Deposit', 2030902),
(39, 'General English:7. GE-Books', 'GE-Books', 4010101),
(40, 'General English:8. Airport Pick-up', 'Airport Pick-Up:', 4010101),
(41, 'General English:9. ID', 'ID', 4010101),
(42, 'General English:9.I Card', 'I Card', 4010100),
(43, 'General English-CELI LTD', 'English Teaching Services', 4010200),
(44, 'Gym Usage-OP', 'Gym Usage Period:', 2030903),
(45, 'Int''l.Nurses Training Program', 'INTP for Nurses', 4010116),
(46, 'NCLEX', '', 4010102),
(47, 'NCLEX:Application Fee', 'NCLEX  Processing Fee', 4010104),
(48, 'NCLEX:Course Fee', 'NCLEX tuition fees', 4010104),
(49, 'NCLEX:Course Fee Discount', 'Course Discount', 4010300),
(50, 'NCLEX:Course Materials', 'various NCLEX Course Materials', 4010110),
(51, 'NCLEX:NCLEX Trainings', 'Various trainings conducted related to NCLEX', 4010111),
(52, 'Other Income', 'Other Income', 7010000),
(53, 'Other Income:1.Guest Meal', 'Guest Meal', 7010000),
(54, 'Other Income:10. Extra Blanket', '10. Extra Blanket', 4010000),
(55, 'Other Income:2. Van rental', 'Van rental', 7010000),
(56, 'Other Income:3.Employees Meal', 'Employees Meal', 7010001),
(57, 'Other Income:4.Realized Gain on Forex Ex', 'Realized Gain on Forex Ex', 7010000),
(58, 'Other Income:5.Mini Bar', 'Mini Bar', 7010000),
(59, 'Other Income:6.Extra Pillow', 'Extra Pillow', 7010000),
(60, 'Other Income:7. Uniform', 'Uniform', 7010000),
(61, 'Other Income:8. Extra Towel', 'Extra Towel', 4010000),
(62, 'Other Income:9. Dan''s Grill', 'Dan''s Grill', 7010000),
(63, 'Realized (Loss) on Forex Ex', 'Realized (Loss) on Forex Ex', 8010000),
(64, 'Security Deposit', '', 2010300),
(65, 'Unearned Revenue', 'Unearned Revenue', 2030200),
(66, 'Water Charges', 'Water Charges', 7010000),
(67, 'student payment accomodation', '', 1020404),
(68, 'student payment accomodation:Student Payment Accomodation', '', 1020404),
(69, 'temporary borrowing from Officr', '', 2030911),
(70, '1st downpayment', '', 1020900);

-- --------------------------------------------------------

--
-- Table structure for table `as_paymeth`
--

CREATE TABLE IF NOT EXISTS `as_paymeth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `as_paymeth`
--

INSERT INTO `as_paymeth` (`id`, `name`) VALUES
(1, 'cash'),
(2, 'check'),
(3, 'cash & check'),
(4, 'american express'),
(5, 'discover'),
(6, 'JCB'),
(7, 'fund transfer'),
(8, 'visa'),
(9, 'master card'),
(10, 'none');

-- --------------------------------------------------------

--
-- Table structure for table `as_payterms`
--

CREATE TABLE IF NOT EXISTS `as_payterms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(65) NOT NULL COMMENT 'ex current, 31-60days, 61-90days, over 90days',
  `percent` tinyint(5) NOT NULL DEFAULT '0' COMMENT 'extimated % uncollectible',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `as_payterms`
--

INSERT INTO `as_payterms` (`id`, `age`, `percent`) VALUES
(1, 'current', 1),
(2, '31-60 days', 5),
(3, '61-90 days', 15),
(4, 'over 90 days', 42),
(5, 'none', 0);

-- --------------------------------------------------------

--
-- Table structure for table `as_purchase_orders`
--

CREATE TABLE IF NOT EXISTS `as_purchase_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `po_number` varchar(25) NOT NULL COMMENT 'purchase order number',
  `supplier` int(11) NOT NULL COMMENT 'the supplier id',
  `item` varchar(155) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `rate` float(11,2) NOT NULL DEFAULT '0.00' COMMENT 'multiply the rate to the quantity to get the amount that is in the as_general_ledger table',
  `received` int(11) NOT NULL DEFAULT '0' COMMENT '0 if not received then the date in timestamp if received',
  `payment_method` int(11) NOT NULL DEFAULT '1' COMMENT 'the payment method. Ex. (cas, tseke, ug uban pa) referenced to the as_paymeth table',
  `class` int(11) DEFAULT NULL,
  `date_required` int(11) NOT NULL DEFAULT '0' COMMENT 'the date this purchase order will be required',
  PRIMARY KEY (`id`),
  KEY `payment_method` (`payment_method`),
  KEY `class` (`class`),
  KEY `supplier` (`supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `as_purchase_orders`
--

INSERT INTO `as_purchase_orders` (`id`, `po_number`, `supplier`, `item`, `qty`, `rate`, `received`, `payment_method`, `class`, `date_required`) VALUES
(3, 'PO-0000000001', 4, 'Eggs : each', 12, 5.00, 0, 1, 10, 1319580000);

-- --------------------------------------------------------

--
-- Table structure for table `as_sales_postings`
--

CREATE TABLE IF NOT EXISTS `as_sales_postings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invsn_no` varchar(25) NOT NULL COMMENT 'if its a sales no ''SN-000001'' or the official receipt, if its an invoice number ''INV-000001''',
  `customer` varchar(45) NOT NULL COMMENT 'sold to or the customer id',
  `item` varchar(155) NOT NULL COMMENT 'ex. NCLEX: Application Fee. the transaction_details field in the as_general_ledger table will be the description. thanks ^^',
  `qty` int(11) NOT NULL DEFAULT '0' COMMENT 'quantity',
  `rate` float(11,2) NOT NULL DEFAULT '0.00' COMMENT 'multiply the rate to the quantity to get the amount that is in the as_general_ledger table',
  `paymeth` int(11) NOT NULL DEFAULT '10' COMMENT 'the payment method. Ex. (cash, cheque, etc...). Referenced to the paymeth table',
  `cheque_num` varchar(65) NOT NULL DEFAULT '0' COMMENT 'the cheque number if the payment method is a tseke',
  `type` enum('invoice','sales') NOT NULL,
  `class` int(11) NOT NULL,
  `due_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `customer` (`customer`),
  KEY `paymeth` (`paymeth`),
  KEY `class` (`class`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `as_sales_postings`
--

INSERT INTO `as_sales_postings` (`id`, `invsn_no`, `customer`, `item`, `qty`, `rate`, `paymeth`, `cheque_num`, `type`, `class`, `due_date`) VALUES
(12, 'SN-0000000001', '3:customer', 'Caregiver:Caregiving Books', 12, 22.00, 1, '', 'sales', 10, 0),
(13, 'INV-0000000002', '5:customer', 'Caregiver:Enrollment Fee', 12, 569.00, 10, '0', 'invoice', 6, 1319666400),
(14, 'SN-0000000003', '2:customer', 'Caregiver:Course Fee', 1, 115.00, 2, '55555', 'sales', 11, 0),
(15, 'SN-0000000004', '1:guest', 'Caregiver:Caregiving Books', 1, 21.00, 1, '', 'sales', 2, 0),
(16, 'SN-0000000005', '1:guest', 'Call Center', 11, 221.00, 1, '', 'sales', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `class_type` enum('1:1','1:4','1:8') DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL COMMENT 'from course table',
  PRIMARY KEY (`book_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `title`, `class_type`, `course_id`) VALUES
(2, 'American Headway 3', '1:1', 8),
(3, 'American Headway 3', '1:1', 8),
(4, 'Know How Openera', '1:4', 8),
(5, 'Know How 1', '1:4', 8),
(6, 'Know How 2', '1:4', 8),
(7, 'Know How 3', '1:4', 8),
(8, 'New Interchange Intro', '1:8', 8),
(9, 'New Interchange 1', '1:8', 8),
(10, 'New Interchange 2', '1:8', 8),
(11, 'New Interchange 3', '1:8', 8),
(12, 'Real Writing 1', '1:1', 15),
(13, 'Real Writing 2', '1:1', 15),
(14, 'Real Writing 3', '1:1', 15),
(15, 'Real Reading 1', '1:1', 15),
(16, 'Real Reading 2', '1:1', 15),
(17, 'Real Reading 3', '1:1', 15),
(18, 'Real Listening and Speaking 1', '1:1', 15),
(19, 'Real Listening and Speaking 2', '1:1', 15),
(20, 'Real Listening and Speaking 3', '1:1', 15),
(27, 'Let''s Talk 1', '1:1', 8),
(68, 'Know How 2', '1:1', 2),
(69, 'bar', NULL, NULL),
(70, 'va', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cg_activities`
--

CREATE TABLE IF NOT EXISTS `cg_activities` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(250) NOT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cg_activities`
--

INSERT INTO `cg_activities` (`act_id`, `act_name`) VALUES
(1, 'Lecture Discussion'),
(2, 'Oral Recitation');

-- --------------------------------------------------------

--
-- Table structure for table `cg_class`
--

CREATE TABLE IF NOT EXISTS `cg_class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_id` int(11) NOT NULL,
  `days` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `duration` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `cg_class`
--

INSERT INTO `cg_class` (`class_id`, `mod_id`, `days`, `time`, `duration`, `semester`) VALUES
(21, 1, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20121),
(22, 3, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20121),
(23, 5, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20122),
(25, 3, 'monday-tuesday-wednesday', '06:00-10:00', 3, 20122),
(28, 2, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20122);

-- --------------------------------------------------------

--
-- Table structure for table `cg_enroll`
--

CREATE TABLE IF NOT EXISTS `cg_enroll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `cg_enroll`
--

INSERT INTO `cg_enroll` (`id`, `student_id`, `class_id`, `date_added`) VALUES
(52, 1, 21, '05/10/2012'),
(53, 2, 21, '05/14/2012'),
(54, 1, 22, '05/14/2012'),
(55, 3, 23, '05/14/2012'),
(56, 1, 23, '05/15/2012'),
(57, 3, 25, '05/15/2012'),
(58, 1, 28, '05/17/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cg_fees`
--

CREATE TABLE IF NOT EXISTS `cg_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `fees` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `cg_fees`
--

INSERT INTO `cg_fees` (`id`, `student_id`, `fees`, `semester`, `date_added`) VALUES
(8, 1, 1000, '20121', '05/14/2012'),
(9, 2, 500, '20121', '05/14/2012'),
(10, 3, 1000, '20122', '05/15/2012'),
(11, 1, 1000, '20122', '05/17/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cg_grades`
--

CREATE TABLE IF NOT EXISTS `cg_grades` (
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `mod_id` int(11) NOT NULL,
  `grade` decimal(5,1) NOT NULL,
  `date_added` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cg_grades`
--

INSERT INTO `cg_grades` (`student_id`, `class_id`, `mod_id`, `grade`, `date_added`) VALUES
(1, 21, 0, 99.0, '05/10/2012'),
(1, 23, 0, 88.0, '05/16/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cg_semester`
--

CREATE TABLE IF NOT EXISTS `cg_semester` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cg_semester`
--

INSERT INTO `cg_semester` (`id`, `semester`) VALUES
(2, 20121),
(3, 20122);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('ac1d8db526a4f0fb2740ab404f1925bb', '192.168.0.252', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:13.0) Gecko/20100101 Firefox/13.0.1', 1343999330, 'a:1:{s:8:"emp_info";a:7:{s:2:"id";s:2:"17";s:6:"emp_id";s:1:"0";s:6:"status";s:6:"active";s:10:"department";s:14:"Human Resource";s:7:"dept_id";s:2:"10";s:10:"date_added";s:1:"0";s:8:"fullname";s:14:"Hr Admin Admin";}}'),
('c7e7e76afc6c0f0572b527b70bce3e0c', '192.168.0.250', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.10 Chromium/17.0.963.65 Chrome/17.0.963.6', 1343999341, 'a:1:{s:8:"emp_info";a:5:{s:2:"id";s:2:"15";s:6:"status";s:6:"active";s:10:"department";s:22:"Hotel ( Front Office )";s:10:"date_added";s:1:"0";s:8:"fullname";s:16:"Super Hotel User";}}'),
('ee848f1e69d3137e2da5e1628f80d6d4', '192.168.0.252', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.19 (KHTML, like Gecko) Ubuntu/11.10 Chromium/18.0.1025.168 Chrome/18.0.102', 1343997432, 'a:1:{s:8:"emp_info";a:7:{s:2:"id";s:2:"17";s:6:"emp_id";s:1:"0";s:6:"status";s:6:"active";s:10:"department";s:14:"Human Resource";s:7:"dept_id";s:2:"10";s:10:"date_added";s:1:"0";s:8:"fullname";s:14:"Hr Admin Admin";}}');

-- --------------------------------------------------------

--
-- Table structure for table `classrooms`
--

CREATE TABLE IF NOT EXISTS `classrooms` (
  `classroom_id` int(4) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(30) NOT NULL,
  PRIMARY KEY (`classroom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `classrooms`
--

INSERT INTO `classrooms` (`classroom_id`, `room_name`) VALUES
(1, '113'),
(2, '114'),
(3, '115'),
(4, '116'),
(5, '117'),
(6, '118'),
(7, '119'),
(8, '120'),
(9, '121'),
(10, '122'),
(11, '123'),
(12, '124'),
(13, '125'),
(14, '126'),
(15, '127'),
(16, '128'),
(17, '129'),
(18, '130'),
(19, '131'),
(20, '132'),
(21, '133'),
(22, '134'),
(23, '135'),
(24, '136'),
(25, '137'),
(26, '138'),
(27, '139'),
(28, '140'),
(29, '141'),
(30, '142'),
(31, '003'),
(32, '004');

-- --------------------------------------------------------

--
-- Table structure for table `class_activities`
--

CREATE TABLE IF NOT EXISTS `class_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(250) NOT NULL,
  `class_id` int(11) NOT NULL,
  `start_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `class_activities`
--

INSERT INTO `class_activities` (`id`, `act_name`, `class_id`, `start_date`, `end_date`) VALUES
(1, 'Lecture', 21, '05/11/2012', '05/31/2012'),
(8, 'Lecture Discussion', 23, '05/01/2012', '05/16/2012'),
(11, 'Oral', 23, '05/17/2012', '05/18/2012');

-- --------------------------------------------------------

--
-- Table structure for table `class_students`
--

CREATE TABLE IF NOT EXISTS `class_students` (
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  KEY `class_id` (`class_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_students`
--

INSERT INTO `class_students` (`class_id`, `student_id`) VALUES
(12, 1),
(10, 3),
(10, 4),
(10, 5),
(10, 2),
(15, 4);

-- --------------------------------------------------------

--
-- Table structure for table `contrib_phic`
--

CREATE TABLE IF NOT EXISTS `contrib_phic` (
  `contrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `salary_bracket` varchar(4) NOT NULL,
  `range_min` decimal(8,2) NOT NULL,
  `range_max` decimal(8,2) NOT NULL,
  `salary_base` decimal(8,2) NOT NULL,
  `personal_share` decimal(8,2) NOT NULL,
  `employer_share` decimal(8,2) NOT NULL,
  `total_monthly` decimal(8,2) NOT NULL,
  PRIMARY KEY (`contrib_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `contrib_phic`
--

INSERT INTO `contrib_phic` (`contrib_id`, `salary_bracket`, `range_min`, `range_max`, `salary_base`, `personal_share`, `employer_share`, `total_monthly`) VALUES
(6, '4', 7000.00, 7999.99, 7000.00, 87.50, 87.50, 175.00),
(7, '5', 8000.00, 8999.99, 8000.00, 100.00, 100.00, 200.00),
(8, '6', 9000.00, 9999.99, 9000.00, 112.50, 112.50, 225.00),
(9, '7', 10000.00, 10999.99, 10000.00, 125.00, 125.00, 250.00),
(10, '8', 11000.00, 11999.99, 11000.00, 137.50, 137.50, 275.00),
(11, '9', 12000.00, 12999.99, 12000.00, 150.00, 150.00, 300.00),
(12, '10', 13000.00, 13999.99, 13000.00, 162.50, 162.50, 325.00),
(13, '11', 14000.00, 14999.99, 14000.00, 175.00, 175.00, 350.00),
(14, '12', 15000.00, 15999.99, 15000.00, 187.50, 187.50, 375.00),
(15, '13', 16000.00, 16999.99, 16000.00, 200.00, 200.00, 400.00),
(16, '14', 17000.00, 17999.99, 17000.00, 212.50, 212.50, 425.00),
(17, '15', 18000.00, 18999.99, 18000.00, 225.00, 225.00, 450.00),
(18, '16', 19000.00, 19999.99, 19000.00, 237.50, 237.50, 475.00),
(19, '17', 20000.00, 20999.99, 20000.00, 250.00, 250.00, 500.00),
(20, '18', 21000.00, 21999.99, 21000.00, 262.50, 262.50, 525.00),
(21, '19', 22000.00, 22999.99, 22000.00, 275.00, 275.00, 550.00),
(22, '20', 23000.00, 23999.99, 23000.00, 287.50, 287.50, 575.00),
(23, '21', 24000.00, 24999.99, 24000.00, 300.00, 300.00, 600.00),
(24, '22', 25000.00, 25999.99, 25000.00, 312.50, 312.50, 625.00),
(25, '23', 26000.00, 26999.99, 26000.00, 325.00, 325.00, 650.00),
(26, '24', 27000.00, 27999.99, 27000.00, 337.50, 337.50, 675.00),
(27, '25', 28000.00, 28999.99, 28000.00, 350.00, 350.00, 700.00),
(28, '26', 29000.00, 29999.99, 29000.00, 362.50, 362.50, 725.00),
(29, '27', 30000.00, 100000.00, 30000.00, 375.00, 375.00, 750.00),
(30, '2', 5000.00, 5999.99, 5000.00, 62.50, 62.50, 125.00),
(31, '3', 6000.00, 6999.99, 6000.00, 75.00, 75.00, 150.00);

-- --------------------------------------------------------

--
-- Table structure for table `contrib_sss`
--

CREATE TABLE IF NOT EXISTS `contrib_sss` (
  `contrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `range_min` decimal(8,2) NOT NULL,
  `range_max` decimal(8,2) NOT NULL,
  `employer` decimal(8,2) NOT NULL,
  `employee` decimal(8,2) NOT NULL,
  `ecer` decimal(8,2) NOT NULL,
  PRIMARY KEY (`contrib_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `contrib_sss`
--

INSERT INTO `contrib_sss` (`contrib_id`, `range_min`, `range_max`, `employer`, `employee`, `ecer`) VALUES
(5, 1750.00, 2249.99, 141.30, 66.70, 10.00),
(9, 4250.00, 4749.99, 318.00, 150.00, 10.00),
(10, 4750.00, 5249.99, 353.30, 166.70, 10.00),
(11, 5250.00, 5749.99, 388.70, 183.30, 10.00),
(12, 5750.00, 6249.99, 424.00, 200.00, 10.00),
(13, 6250.00, 6749.99, 459.30, 216.70, 10.00),
(14, 6750.00, 7249.99, 494.70, 233.30, 10.00),
(15, 7250.00, 7749.99, 530.00, 250.00, 10.00),
(16, 7750.00, 8249.99, 565.30, 266.70, 10.00),
(17, 8250.00, 8749.99, 600.70, 283.30, 10.00),
(18, 8750.00, 9249.99, 636.00, 300.00, 10.00),
(19, 9250.00, 9749.99, 671.30, 316.70, 10.00),
(20, 9750.00, 10249.99, 706.70, 333.30, 10.00),
(21, 10250.00, 10749.99, 742.00, 350.00, 10.00),
(22, 10750.00, 11249.99, 777.30, 366.70, 10.00),
(23, 11250.00, 11749.99, 812.70, 383.30, 10.00),
(24, 11750.00, 12249.99, 848.00, 400.00, 10.00),
(25, 12250.00, 12749.99, 883.30, 416.70, 10.00),
(26, 12750.00, 13249.99, 918.70, 433.30, 10.00),
(27, 13250.00, 13749.99, 918.70, 433.30, 10.00),
(31, 13750.00, 14249.99, 989.30, 466.70, 10.00),
(32, 14250.00, 14749.99, 1024.70, 483.30, 10.00),
(33, 14750.00, 999999.99, 1060.00, 500.00, 30.00);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `type` enum('elective','regular') NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `name`, `type`) VALUES
(1, 'Toefl IBT', 'elective'),
(2, 'TOEIC', 'elective'),
(3, 'lelts', 'elective'),
(4, 'CS', 'regular'),
(5, 'CK', 'regular'),
(6, 'Pre-Teen', 'elective'),
(7, 'CT', 'regular'),
(8, 'GE', 'regular'),
(9, 'SPK', 'regular'),
(10, 'AW', 'regular'),
(11, 'BE', 'regular'),
(12, 'Test Prep Intensive', 'elective'),
(13, 'IE 1hr', 'regular'),
(14, 'GE 1hr', 'regular'),
(15, 'IE', 'regular');

-- --------------------------------------------------------

--
-- Table structure for table `course_level`
--

CREATE TABLE IF NOT EXISTS `course_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `course_level`
--

INSERT INTO `course_level` (`id`, `name`) VALUES
(1, 'Beginner Low'),
(2, 'Beginner Med'),
(3, 'Beginner High'),
(4, 'Pre-Intermediate Low'),
(5, 'Pre-Intermediate Med'),
(6, 'Pre-Intermediate High'),
(7, 'Intermediate-Low'),
(8, 'Intermediate-Med'),
(9, 'Intermediate-High'),
(10, 'Advanced'),
(11, 'Upper Advanced');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `email_address`, `contact_no`, `address`, `status`, `date_added`) VALUES
(1, 'Rob', 'Smith', 'smith@localhost.com', '54545', 'Maine Street, California, USA', 'active', 0),
(2, 'first 123', 'sample customer', 'email_address@mail.com', '123456789', 'address 1', 'active', 1318303244),
(3, 'retrieve', 'cust test', '123@123.com', '1234123', '123', 'active', 1318386705),
(4, 'url incorrect test', 'here', 'here@here.com', '1234', 'here', 'active', 1318387607),
(5, 'sample sorting', 'added', 'sample@yahoo.com', '1234567', 'here', 'active', 1318399424);

-- --------------------------------------------------------

--
-- Table structure for table `daily_sales_report`
--

CREATE TABLE IF NOT EXISTS `daily_sales_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(65) NOT NULL,
  `contents` text NOT NULL,
  `sales_type` enum('hotel') NOT NULL,
  `filed_by` int(11) NOT NULL,
  `date_filed` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filed_by` (`filed_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `daily_sales_report`
--

INSERT INTO `daily_sales_report` (`id`, `subject`, `contents`, `sales_type`, `filed_by`, `date_filed`) VALUES
(1, 'Sample #1', '<p>\n	<span>:this is a test</span></p>\n', 'hotel', 12, 1326361133),
(2, 'Sample #2', '<p>\n	hello this is a test...</p>\n', 'hotel', 12, 1326362079),
(3, 'Sample #3', '<p>\n	This is a report from</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&lt;table style=&quot;width: 330px; height: 60px;&quot; 500px;&quot;=&quot;&quot; border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot;&gt;</p>\n<tbody>\n	<tr>\n		<td>\n			test</td>\n		<td>\n			test</td>\n	</tr>\n	<tr>\n		<td>\n			1</td>\n		<td>\n			sdf</td>\n	</tr>\n	<tr>\n		<td>\n			2</td>\n		<td>\n			&nbsp;</td>\n	</tr>\n	<tr>\n		<td>\n			&nbsp;</td>\n		<td>\n			&nbsp;</td>\n	</tr>\n</tbody>\n<p>\n	&nbsp;</p>\n', 'hotel', 12, 1326440454),
(4, 'Mao ni ang nakuha...', '<p>\n	We aproximately get this one to the button &#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;</p>\n', 'hotel', 12, 1330406553);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `deptd_id` int(11) NOT NULL,
  PRIMARY KEY (`dept_id`),
  KEY `emp_id` (`emp_id`),
  KEY `deptd_id` (`deptd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `emp_id`, `deptd_id`) VALUES
(1, 5, 5),
(2, 7, 5),
(3, 10, 7),
(4, 11, 7),
(5, 12, 7),
(8, 14, 7),
(9, 15, 8),
(11, 17, 10),
(12, 18, 2),
(16, 21, 11),
(17, 20, 2),
(18, 16, 11),
(19, 13, 2),
(21, 23, 2),
(22, 24, 2),
(23, 25, 2),
(26, 68, 5);

-- --------------------------------------------------------

--
-- Table structure for table `department_details`
--

CREATE TABLE IF NOT EXISTS `department_details` (
  `deptd_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(50) NOT NULL,
  `dept_code` varchar(30) NOT NULL,
  `dept_head` int(11) NOT NULL,
  `is_dept` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`deptd_id`),
  KEY `dept_head` (`dept_head`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `department_details`
--

INSERT INTO `department_details` (`deptd_id`, `dept_name`, `dept_code`, `dept_head`, `is_dept`) VALUES
(1, 'Vice President', 'VP', 1, '1'),
(2, 'Operations', 'Operations', 1, '1'),
(3, 'Food And Beverages', 'F&B', 1, '0'),
(4, 'House Keeping', '', 4, '0'),
(5, 'InfoTech', 'IT', 1, '1'),
(7, 'Academics', 'ACAD', 10, '1'),
(8, 'Hotel ( Front Office )', 'FO', 15, '1'),
(10, 'Human Resource', 'HRD', 17, '1'),
(11, 'Care Giver', 'CG', 21, '1');

-- --------------------------------------------------------

--
-- Table structure for table `dsr_collections`
--

CREATE TABLE IF NOT EXISTS `dsr_collections` (
  `dsr_collections_id` int(11) NOT NULL AUTO_INCREMENT,
  `dsr_report_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `particulars` varchar(65) NOT NULL,
  `or_num` varchar(65) NOT NULL,
  `yen` float(11,2) NOT NULL DEFAULT '0.00',
  `peso` float(11,2) NOT NULL DEFAULT '0.00',
  `us_dollar` float(11,2) NOT NULL DEFAULT '0.00',
  `k_won` float(11,2) NOT NULL DEFAULT '0.00',
  `check` float(11,2) NOT NULL DEFAULT '0.00',
  `check_num` varchar(115) NOT NULL,
  `card` float(11,2) NOT NULL DEFAULT '0.00',
  `card_num` varchar(115) NOT NULL,
  PRIMARY KEY (`dsr_collections_id`),
  KEY `dsr_report_id` (`dsr_report_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `dsr_collections`
--

INSERT INTO `dsr_collections` (`dsr_collections_id`, `dsr_report_id`, `name`, `particulars`, `or_num`, `yen`, `peso`, `us_dollar`, `k_won`, `check`, `check_num`, `card`, `card_num`) VALUES
(1, 1, 'JOhn Doe', 'Particulars', '565', 0.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(2, 2, 'asdf', 'asdf', 'asdf', 0.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(3, 3, 'sdf', 'sdf', '569', 0.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(4, 4, 'sdf', '34', 'saf', 0.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(5, 5, 'Name 101', 'Particular 101', '565', 100.00, 100.00, 100.00, 100.00, 100.00, '---', 100.00, '---'),
(6, 5, 'Name 102', 'Particular 102', '569', 200.00, 200.00, 200.00, 200.00, 200.00, '---', 200.00, '---'),
(7, 5, 'Name 103', 'Particular 103', '122', 300.00, 300.00, 300.00, 300.00, 300.00, '---', 300.00, '---'),
(8, 6, 'item 5', 'item 5', '8452', 100.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(9, 7, '---sdfsdf', '--sddfs', '---4456', 0.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(10, 8, 'sdsdf---', 'sdfddsf---', 'dss---', 0.00, 0.00, 0.00, 0.00, 0.00, 'sdsdf---', 0.00, '---'),
(11, 9, '---sddsfsdf', '---sdsdfsdf', '---dsffdsfds', 0.00, 0.00, 0.00, 0.00, 0.00, '---sdfdsf', 0.00, '---dsfsfd'),
(12, 10, 'sddfsdsf---', 'dfsdsf---', 'dssd---', 0.00, 0.00, 0.00, 0.00, 0.00, 'dsdsfds---', 0.00, 'dsds---'),
(13, 11, 'item4', 'item4', '123sd---', 0.00, 0.00, 0.00, 0.00, 0.00, '123---', 0.00, '45---'),
(14, 12, 'dfg---', 'dffg---', 'df---', 0.00, 0.00, 0.00, 0.00, 0.00, 'dfd---', 0.00, 'df---'),
(15, 13, 'Chicken Soup', 's---', '44---', 100.00, 0.00, 0.00, 0.00, 0.00, '---', 0.00, '---'),
(16, 14, 'sdfdsf---', 'sdffd---', 'ere---', 4500.00, 300.00, 300.00, 300.00, 300.00, '---', 300.00, '---'),
(17, 15, '---ds', '---d', '---4', 0.00, 0.00, 0.00, 0.00, 0.00, '---3', 400.04, '---3'),
(18, 16, 'Jake', 'howe', '123', 2.00, 4.00, 45.00, 100.00, 500.00, '---', 400.00, '---');

-- --------------------------------------------------------

--
-- Table structure for table `dsr_recap`
--

CREATE TABLE IF NOT EXISTS `dsr_recap` (
  `dsr_recap_id` int(11) NOT NULL AUTO_INCREMENT,
  `dsr_report_id` int(11) NOT NULL,
  `acct_name` varchar(65) DEFAULT NULL,
  `sales_acct` varchar(115) DEFAULT NULL,
  `sales_amt` float(11,2) NOT NULL DEFAULT '0.00',
  `ssp_acct` varchar(115) DEFAULT NULL,
  `ssp_amt` float(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`dsr_recap_id`),
  KEY `dsr_report_id` (`dsr_report_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `dsr_recap`
--

INSERT INTO `dsr_recap` (`dsr_recap_id`, `dsr_report_id`, `acct_name`, `sales_acct`, `sales_amt`, `ssp_acct`, `ssp_amt`) VALUES
(1, 1, 'US DOLLAR', 'ASD-6984', 10.00, 'SDF-2342', 20.00),
(2, 2, 'US DOLLAR', 'asdf', 10.00, 'sdf', 10.00),
(3, 3, 'US DOLLAR', 'asdf', 10.00, '123', 10.00),
(4, 4, 'US DOLLAR', 'asdf', 10.00, 'sfd3', 10.00),
(5, 5, 'US DOLLAR', '12346', 10.00, '654987', 20.00),
(6, 5, 'Yen', '987654', 10.00, '123456', 20.00),
(7, 6, 'US DOLLAR', '549', 0.00, '555', 0.00),
(8, 7, 'US DOLLAR', '---dsfdss', 0.00, '---dfssdf', 0.00),
(9, 8, 'US DOLLAR', 'sdfds---', 0.00, 'dsdsf---', 0.00),
(10, 9, 'US DOLLAR', '---dsffsd', 0.00, '---dfdfs', 0.00),
(11, 10, 'US DOLLAR', 'dsds---', 0.00, 'dsds---', 0.00),
(12, 11, 'US DOLLAR', 'sddsf---', 0.00, 'sdf---', 0.00),
(13, 12, 'US DOLLAR', 'df---', 0.00, 'df---', 0.00),
(14, 13, 'US DOLLAR', '147447---', 10.00, '471---', 10.00),
(15, 14, 'US DOLLAR', 'sdds---', 0.00, 'were---', 0.00),
(16, 15, 'US DOLLAR', '---ew32', 0.00, '---3', 0.00),
(17, 16, 'US DOLLAR', '123', 100.00, '124', 100.00);

-- --------------------------------------------------------

--
-- Table structure for table `dsr_report`
--

CREATE TABLE IF NOT EXISTS `dsr_report` (
  `dsr_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `prepared_by` int(11) NOT NULL,
  `checked_by` tinyint(1) NOT NULL DEFAULT '0',
  `received_by` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dsr_report_id`),
  KEY `prepared_by` (`prepared_by`),
  KEY `checked_by` (`checked_by`),
  KEY `received_by` (`received_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `dsr_report`
--

INSERT INTO `dsr_report` (`dsr_report_id`, `prepared_by`, `checked_by`, `received_by`, `date_added`) VALUES
(1, 12, 0, 0, '2012-02-21 11:18:49'),
(2, 12, 0, 0, '2012-02-21 11:21:25'),
(3, 12, 0, 0, '2012-02-21 11:23:23'),
(4, 12, 0, 0, '2012-02-21 11:26:10'),
(5, 12, 0, 0, '2012-02-21 17:14:41'),
(6, 12, 0, 0, '2012-04-02 11:47:59'),
(7, 20, 0, 0, '2012-04-02 15:20:25'),
(8, 20, 0, 0, '2012-04-03 13:35:34'),
(9, 12, 0, 0, '2012-04-03 13:36:24'),
(10, 20, 0, 0, '2012-04-03 13:37:04'),
(11, 20, 0, 0, '2012-04-03 13:39:33'),
(12, 20, 0, 0, '2012-04-03 13:40:23'),
(13, 20, 0, 0, '2012-04-03 14:39:48'),
(14, 23, 0, 0, '2012-06-19 17:01:22'),
(15, 23, 0, 0, '2012-06-21 15:57:47'),
(16, 15, 0, 0, '2012-07-31 14:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `employment_type`
--

CREATE TABLE IF NOT EXISTS `employment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employment_type`
--

INSERT INTO `employment_type` (`id`, `name`) VALUES
(1, 'probationary'),
(2, 'regular'),
(3, 'contractual');

-- --------------------------------------------------------

--
-- Table structure for table `emp_contact_details`
--

CREATE TABLE IF NOT EXISTS `emp_contact_details` (
  `emp_id` int(11) NOT NULL,
  `country` varchar(30) NOT NULL,
  `street` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `province` varchar(30) DEFAULT NULL,
  `zipcode` int(11) NOT NULL,
  `telno` varchar(25) DEFAULT NULL,
  `mobileno` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_contact_details`
--

INSERT INTO `emp_contact_details` (`emp_id`, `country`, `street`, `city`, `province`, `zipcode`, `telno`, `mobileno`, `email`) VALUES
(10, 'Philippines', 'Street123', 'Cebu', 'Cebu', 6010, '', '', ''),
(11, 'Philippines', 'Perrelos', 'Cebu', 'Cebu', 6018, '123', '', ''),
(12, '', '', 'Opon Lapu-Lapu City', '', 456, '', '', ''),
(13, '', '', '', '', 0, '', '', ''),
(16, 'Korea', 'North East, SOuth Korea', '0', '0', 0, '12345', '', 'kiera@localhost.com'),
(17, '', '', '', '', 0, '', '', ''),
(20, '', '', '', '', 0, '', '', ''),
(21, '', '', '', '', 0, '', '', ''),
(23, '', '', '', '', 745, '', '', ''),
(25, '', '', '', '', 0, '', '', ''),
(68, 'Philippines', 'saac', 'LLC', 'LLC', 6015, '1231234', '09331216391', '09331216391');

-- --------------------------------------------------------

--
-- Table structure for table `emp_contract`
--

CREATE TABLE IF NOT EXISTS `emp_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `start` int(11) NOT NULL DEFAULT '0' COMMENT 'Start date in timestmap',
  `end` int(11) NOT NULL DEFAULT '0' COMMENT 'End date in timestamp',
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `emp_contract`
--


-- --------------------------------------------------------

--
-- Table structure for table `emp_contrib_details`
--

CREATE TABLE IF NOT EXISTS `emp_contrib_details` (
  `emp_id` int(11) NOT NULL,
  `sss` varchar(100) DEFAULT NULL,
  `tin` varchar(100) DEFAULT NULL,
  `phic` varchar(100) DEFAULT NULL,
  `hdmf` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_contrib_details`
--

INSERT INTO `emp_contrib_details` (`emp_id`, `sss`, `tin`, `phic`, `hdmf`) VALUES
(10, '1123', '', '12345', ''),
(11, '1110 - 0101 - 1001 ', 'EF92 - D1C5 - 581A ', '11100-1002-110', '23-123-4232-AD23 '),
(12, '23', '232', '23', '2'),
(13, '', '', '', ''),
(16, '0', '0', '0', '0'),
(17, '', '', '', ''),
(20, '', '1024', '', ''),
(21, '', '', '', ''),
(23, '', '', '', ''),
(25, '', '', '', ''),
(68, '111', '222', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `emp_dependents`
--

CREATE TABLE IF NOT EXISTS `emp_dependents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL COMMENT 'can add mutiple data with duplicate emp_id',
  `name` varchar(50) NOT NULL COMMENT 'Dependents Name',
  `relationship` varchar(25) NOT NULL,
  `birthdate` int(11) NOT NULL DEFAULT '0' COMMENT 'In Unix timestamp',
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `emp_dependents`
--


-- --------------------------------------------------------

--
-- Table structure for table `emp_education`
--

CREATE TABLE IF NOT EXISTS `emp_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `school` varchar(65) NOT NULL,
  `course` varchar(65) NOT NULL,
  `year_start` int(11) NOT NULL,
  `year_end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_education`
--

INSERT INTO `emp_education` (`id`, `emp_id`, `school`, `course`, `year_start`, `year_end`) VALUES
(1, 11, 'Cebu Institute of Technology', 'Bachelor of Science in Computer Engineering', 1323990000, 1325199600);

-- --------------------------------------------------------

--
-- Table structure for table `emp_emergency_contact`
--

CREATE TABLE IF NOT EXISTS `emp_emergency_contact` (
  `emp_id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL COMMENT 'Name of the employee''s emergency contact',
  `relationship` varchar(25) NOT NULL COMMENT 'Relationship to the employee ex(Father, Mother, Sister, etc)',
  `telno` varchar(25) DEFAULT NULL,
  `mobileno` varchar(25) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_emergency_contact`
--

INSERT INTO `emp_emergency_contact` (`emp_id`, `name`, `relationship`, `telno`, `mobileno`, `address`) VALUES
(11, 'Jane Doe', 'Mother', '987456', '0123456798', ''),
(12, '', 'llk', '', 'sdsdfsdf', ''),
(13, '', '', '', '', ''),
(16, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(23, '', '', '', '', ''),
(25, 'dfgdfg', 'dfgdfg', 'dfgfdg', 'fdgfdg', ''),
(68, 'roland', 'brother', '', '', 'saac 1 llc');

-- --------------------------------------------------------

--
-- Table structure for table `emp_immigration`
--

CREATE TABLE IF NOT EXISTS `emp_immigration` (
  `emp_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL COMMENT 'visa/passport',
  `number` varchar(25) NOT NULL COMMENT 'The Immigration Number',
  `status` varchar(25) NOT NULL COMMENT 'Immigration Status',
  `review_date` int(11) NOT NULL DEFAULT '0' COMMENT 'Date in unix timestamp',
  `citizenship` varchar(25) NOT NULL,
  `issued_date` int(11) NOT NULL DEFAULT '0' COMMENT 'Date in unix timestamp',
  `expiry_date` int(11) NOT NULL DEFAULT '0' COMMENT 'Date in unix timestamp',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_immigration`
--

INSERT INTO `emp_immigration` (`emp_id`, `type`, `number`, `status`, `review_date`, `citizenship`, `issued_date`, `expiry_date`) VALUES
(10, '', '', '', 1324567615, '', 1324481215, 1324394815),
(11, 'Visa', '123456789 ', 'Fine', 1325060998, 'Filipino', 1324542598, 1325147398),
(12, '', 'weewr', '', 1334268000, 'sdfdsf', 1329865200, 1334268000),
(13, '', '', '', -3600, '''\\klk', -3600, -3600),
(16, '', '', '', 0, '', 0, 0),
(17, '', '', '', 1343618782, '', 1343618782, 1343618782),
(20, '', '', '', -3600, '', -3600, -3600),
(21, '', '', '', -3600, '', -3600, -3600),
(68, '', '', '', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `emp_job_details`
--

CREATE TABLE IF NOT EXISTS `emp_job_details` (
  `emp_id` int(11) NOT NULL,
  `job_title` int(11) NOT NULL COMMENT 'link to the job_title table',
  `job_salary` decimal(8,2) NOT NULL,
  `curr_empstatus` int(11) NOT NULL COMMENT 'the current employment status of the employee ex(regular, provisionary, resigned) and linked to the employment type table and somehow related to the emp_contract table',
  `job_status` int(11) NOT NULL DEFAULT '1' COMMENT 'Connected to the job status table',
  `join_date` int(11) NOT NULL COMMENT 'date in unix time',
  `end_date` int(11) NOT NULL DEFAULT '0',
  `job_leave` int(11) NOT NULL DEFAULT '7' COMMENT 'number of leave of the employee',
  `job_location` varchar(25) NOT NULL COMMENT 'ex. cebu, etc..',
  PRIMARY KEY (`emp_id`),
  KEY `job_title` (`job_title`),
  KEY `curr_empstatus` (`curr_empstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_job_details`
--

INSERT INTO `emp_job_details` (`emp_id`, `job_title`, `job_salary`, `curr_empstatus`, `job_status`, `join_date`, `end_date`, `job_leave`, `job_location`) VALUES
(10, 6, 20000.00, 2, 1, 996616800, 0, 7, 'mactan'),
(11, 7, 20000.00, 2, 1, 1128376800, 0, 7, 'mactan'),
(12, 7, 20000.00, 1, 1, 0, 0, 7, 'mactan'),
(13, 7, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(14, 7, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(15, 8, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(16, 7, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(17, 1, 20000.00, 2, 1, 1335909600, 0, 7, 'cebu'),
(18, 5, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(20, 8, 20000.00, 3, 1, 0, 0, 7, 'mactan'),
(21, 9, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(23, 8, 20000.00, 1, 1, 0, 0, 7, 'mactan'),
(24, 8, 20000.00, 3, 1, 0, 0, 7, 'mactan'),
(25, 8, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(68, 1, 20000.00, 3, 1, 1342044000, 1348956000, 7, 'mactan');

-- --------------------------------------------------------

--
-- Table structure for table `emp_language`
--

CREATE TABLE IF NOT EXISTS `emp_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `fluency` varchar(35) NOT NULL,
  `competency` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_language`
--

INSERT INTO `emp_language` (`id`, `emp_id`, `name`, `fluency`, `competency`) VALUES
(1, 11, 'Tagalog', 'Speaking', 'Good, Average');

-- --------------------------------------------------------

--
-- Table structure for table `emp_leaves`
--

CREATE TABLE IF NOT EXISTS `emp_leaves` (
  `leave_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_requested` int(11) NOT NULL COMMENT 'filing date',
  `leave_type` enum('sick leave','vacation leave','maternity leave','paternity leave','others') NOT NULL,
  `reason` varchar(300) NOT NULL,
  `time_requested` float(11,2) NOT NULL,
  `pay_type` enum('0','1') NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `return_date` int(11) NOT NULL,
  `imm_head_appvl` enum('pending','rejected','cancelled','approved') DEFAULT 'pending',
  `hr_appvl` enum('pending','rejected','cancelled','approved') DEFAULT 'pending',
  PRIMARY KEY (`leave_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `emp_leaves`
--

INSERT INTO `emp_leaves` (`leave_id`, `emp_id`, `date_requested`, `leave_type`, `reason`, `time_requested`, `pay_type`, `leave_start`, `leave_end`, `return_date`, `imm_head_appvl`, `hr_appvl`) VALUES
(4, 11, 1323298800, 'others', 'test', 0.50, '1', 1323903600, 1323990000, 1324249200, 'approved', 'rejected'),
(5, 11, 1323385200, 'vacation leave', 'teast', 0.50, '1', 1323644400, 1323730800, 1323817200, 'pending', 'pending'),
(6, 11, 1323212400, 'others', '', 0.50, '1', 1324335600, 1324335600, 1324854000, 'cancelled', 'pending'),
(7, 11, 1322694000, 'others', '', 0.50, '1', 1324940400, 1324940400, 1324940400, 'approved', 'approved'),
(8, 13, 1324508400, 'others', 'd', 0.50, '1', 1324594800, 1324508400, 1324594800, 'pending', 'pending'),
(17, 20, 1331766000, 'others', '', 0.50, '1', 1332799200, 1332198000, 1332111600, 'pending', 'pending'),
(19, 20, 1332111600, 'others', '', 10.00, '1', 1331161200, 1330902000, 1331593200, 'pending', 'pending'),
(24, 10, 1338501600, 'paternity leave', '', 1.00, '1', 1338501600, 1338588000, 1338588000, 'pending', 'pending'),
(25, 17, 1338847200, 'sick leave', '', 1.00, '1', 1338847200, 1338933600, 1339020000, 'cancelled', 'cancelled'),
(26, 17, 1338847200, 'vacation leave', '', 2.00, '1', 1338847200, 1338933600, 1339020000, 'cancelled', 'cancelled'),
(27, 12, 1340056800, 'sick leave', '', 1.00, '1', 1340229600, 1340229600, 1340316000, 'approved', 'approved'),
(28, 12, 1340056800, 'sick leave', '', 2.00, '1', 1340661600, 1340748000, 1340834400, 'approved', 'approved'),
(31, 12, 1340056800, 'sick leave', '', 1.00, '0', 1341784800, 1341784800, 1341871200, 'approved', 'approved'),
(32, 12, 1338415200, 'vacation leave', '', 3.00, '1', 1338760800, 1338933600, 1339020000, 'approved', 'approved'),
(33, 12, 1339711200, 'maternity leave', '', 2.00, '1', 1339970400, 1340056800, 1340143200, 'approved', 'approved'),
(34, 12, 1340575200, 'others', '', 5.00, '1', 1340575200, 1340920800, 1341180000, 'approved', 'approved'),
(36, 12, 1333663200, 'others', '', 5.00, '1', 1333922400, 1334268000, 1334527200, 'approved', 'approved'),
(37, 17, 1340316000, 'maternity leave', '', 3.00, '1', 1340748000, 1340920800, 1340920800, 'rejected', 'rejected');

-- --------------------------------------------------------

--
-- Table structure for table `emp_license`
--

CREATE TABLE IF NOT EXISTS `emp_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `type` varchar(25) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_license`
--

INSERT INTO `emp_license` (`id`, `emp_id`, `name`, `type`, `start`, `end`) VALUES
(1, 11, 'Drivers License', 'Platinum', 1325113200, 1325286000);

-- --------------------------------------------------------

--
-- Table structure for table `emp_ot_ut_detail`
--

CREATE TABLE IF NOT EXISTS `emp_ot_ut_detail` (
  `ot_ut_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_ot_ut_id` int(11) NOT NULL,
  `ot_ut_start` int(11) NOT NULL,
  `ot_ut_end` int(11) NOT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ot_ut_details_id`),
  KEY `emp_ot_ut_id` (`emp_ot_ut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `emp_ot_ut_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `emp_overtime`
--

CREATE TABLE IF NOT EXISTS `emp_overtime` (
  `otime_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_file` int(11) NOT NULL,
  `date_covered` int(11) NOT NULL,
  `reason` varchar(300) NOT NULL,
  `otime_date` int(11) NOT NULL,
  `otime_from` time NOT NULL,
  `otime_to` time NOT NULL,
  `total_hours` decimal(8,2) NOT NULL,
  `imm_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  `hr_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`otime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `emp_overtime`
--

INSERT INTO `emp_overtime` (`otime_id`, `emp_id`, `date_file`, `date_covered`, `reason`, `otime_date`, `otime_from`, `otime_to`, `total_hours`, `imm_head_appvl`, `hr_appvl`) VALUES
(1, 17, 1340661600, 1340661600, 'tyr thet', 1340661600, '12:00:00', '17:30:00', 5.30, 'approved', 'rejected'),
(2, 17, 1340661600, 1340661600, 'tyr thet', 1340661600, '18:00:00', '20:00:00', 2.00, 'approved', 'approved'),
(3, 17, 1340748000, 1340920800, 'going to bantayan', 1340920800, '13:00:00', '18:00:00', 5.00, 'approved', 'approved'),
(4, 10, 1340834400, 1340834400, 'sick', 1340920800, '13:00:00', '18:00:00', 5.00, 'approved', 'approved'),
(5, 10, 1340834400, 1340834400, 'asdasd', 1340920800, '13:05:00', '17:05:00', 4.00, 'approved', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `emp_overtime_undertime`
--

CREATE TABLE IF NOT EXISTS `emp_overtime_undertime` (
  `emp_ot_ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `type` enum('overtime','undertime') DEFAULT NULL,
  `pay_period_start` int(11) NOT NULL,
  `pay_period_end` int(11) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `imm_head_appvl` int(11) DEFAULT '0',
  `hr_appvl` int(11) DEFAULT '0',
  PRIMARY KEY (`emp_ot_ut_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `emp_overtime_undertime`
--


-- --------------------------------------------------------

--
-- Table structure for table `emp_personal_details`
--

CREATE TABLE IF NOT EXISTS `emp_personal_details` (
  `emp_id` int(11) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `nickname` varchar(25) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `birthdate` int(11) NOT NULL DEFAULT '0',
  `nationality` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL,
  `photo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_personal_details`
--

INSERT INTO `emp_personal_details` (`emp_id`, `id_number`, `firstname`, `middlename`, `lastname`, `nickname`, `gender`, `birthdate`, `nationality`, `status`, `photo`) VALUES
(10, '11-011102', 'Acad', 'First', 'Admin', 'acadmin', 'Male', 1970, 'Filipino', 'Widowed', ''),
(11, '0', 'Aidan', 'D', 'Alburo', 'aidan', 'Female', 1343858400, 'Filipino', 'Single', ''),
(12, '123-897', 'Joniefer', 'J', 'Ligas', 'Jonie', 'Female', 632358000, 'Filipino', 'Married', ''),
(13, '11-000003', 'Neriza', 'M', 'Trasmel', '', 'Male', -3600, '', 'Married', ''),
(14, '11-000004', 'Shirra', 'S', 'Augusto', 'Shir', 'Male', 1970, '', '', ''),
(15, '2323', 'super', 'hotel', 'user', '', '', 0, '', '', ''),
(16, '0', 'dummy', 'd', 'user', 'dummy', 'Male', 1337896800, 'korean', 'Single', ''),
(17, '0', 'hr', 'admin', 'admin', 'hr', 'Male', -3600, '', 'Widowed', ''),
(18, '11-000009', 'food', 'and', 'beverages', 'f and b', 'Male', 1970, '', '', ''),
(20, '12-022301', 'Mary', 'p', 'Jane', '', 'Female', 1334613600, '', 'Widowed', ''),
(21, '11-000005', 'Sahlee Pearl', 'V', 'Conson', 'Sahlee', 'Female', 0, 'filipino', 'Single', ''),
(23, '123455666', 'Mary Jane', '', 'Orbita', 'Jane', 'Male', 0, '', 'Married', ''),
(24, '125', 'On', '', 'Duty', '', '', 0, '', '', ''),
(25, '1223', 'Main', '', 'Tenance', '', 'Male', 0, '', 'Single', ''),
(68, '12345', 'reyjomer', 'B', 'fernandez', 'jomer', 'Male', 670629600, 'Filipino', 'Single', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emp_skills`
--

CREATE TABLE IF NOT EXISTS `emp_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `num_years_exp` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_skills`
--

INSERT INTO `emp_skills` (`id`, `emp_id`, `name`, `num_years_exp`) VALUES
(1, 11, 'Junior Programmer', '1 year experience');

-- --------------------------------------------------------

--
-- Table structure for table `emp_total_leaves`
--

CREATE TABLE IF NOT EXISTS `emp_total_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_leaves` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_total_leaves`
--

INSERT INTO `emp_total_leaves` (`id`, `total_leaves`) VALUES
(1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `emp_undertime`
--

CREATE TABLE IF NOT EXISTS `emp_undertime` (
  `utime_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_file` int(11) NOT NULL,
  `date_covered` int(11) NOT NULL,
  `reason` varchar(300) NOT NULL,
  `utime_date` int(11) NOT NULL,
  `utime_from` time NOT NULL,
  `utime_to` time NOT NULL,
  `total_hours` decimal(8,2) NOT NULL,
  `imm_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  `hr_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`utime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `emp_undertime`
--

INSERT INTO `emp_undertime` (`utime_id`, `emp_id`, `date_file`, `date_covered`, `reason`, `utime_date`, `utime_from`, `utime_to`, `total_hours`, `imm_head_appvl`, `hr_appvl`) VALUES
(17, 17, 1340661600, 1340748000, 'tututu', 1340661600, '09:00:00', '11:00:00', 2.00, 'approved', 'approved'),
(18, 17, 1340661600, 1340661600, '', 1340661600, '15:30:00', '18:30:00', 3.00, 'cancelled', 'cancelled'),
(19, 17, 1340661600, 1340661600, 'yujyujy', 1340748000, '21:45:00', '22:50:00', 1.05, 'cancelled', 'cancelled'),
(21, 17, 1340748000, 1340920800, 'Going to bantayan island', 1340920800, '13:00:00', '18:00:00', 5.00, 'pending', 'pending'),
(22, 21, 1340834400, 1340834400, 'sick', 1340834400, '13:00:00', '17:00:00', 4.00, 'approved', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `emp_user`
--

CREATE TABLE IF NOT EXISTS `emp_user` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_added` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `emp_user`
--

INSERT INTO `emp_user` (`emp_id`, `username`, `password`, `status`, `date_added`) VALUES
(1, 'VP Head', '1', 'active', 0),
(2, 'OpsHead', '1', 'active', 0),
(3, 'FandBHead', '1', 'active', 0),
(4, 'HouseKeepingHead', '1', 'active', 0),
(5, 'staff', '1', 'active', 0),
(6, 'ITHead', '1', 'active', 0),
(7, 'Programmer1', '1', 'active', 0),
(10, 'acad_admin1', 'cehj6aMWWul2qccSJ1oAi6k9oqclBKqqDrGUMrGik/L5jF6/VjO9JRa7nHqdk/DIqa35ifVjT/RV0ufdLgmQ+A==', 'active', 0),
(11, 'aidan.alburo', 'WJR1xiMUQdwcrjfS21q1YnLClOXwDZo5LNxi0IUF3vzgnx5ttJjodUVQe6ZFjJ7GQ3LxEAv9ad0VXtpKXEYBww==', 'active', 0),
(12, 'joniefer.ligas', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(13, 'hk.user', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(14, 'shirra.augusto', '4IFv2do657PjDxQDe+q+miyap77rAUc+5C+KsIeWf3IkbTe0WZrzGfWwMR983JjHkZM2SgLPDPIGc8Agxo2DPw==', 'active', 0),
(15, 'super.user.hotel', 'IldVpAvqNH75yb4kbGM/AhVNGholhZj2XgltfHqtk+l8lDDWdwh3clyPiRAmwiD6xeoeGCOZIlJAmGhUxdl6jg==', 'active', 0),
(16, 'student.dummy', 'gUu2CH1U/zUDDJ64h/caqnCELh0DynbeS/xu/H3SAeTETeKHlqpIZUif4e+HHV105UUYERmZ9E7oKYqOSAxZWg==', 'active', 0),
(17, 'hr_admin1', 'QgjRo9X8IKVoNG6Gx9NbfKciwH8T0p9EVL8vdaTyKApVXVN6yC8VQ3GtmjVJ5S9cjNPDCt4i/8GZMSl/c7EEBQ==', 'active', 0),
(18, 'fb_admin1', 'c6qg4YtmAWhopfEkECLCj59c1jG3ppsB2ppS7uOEI/yuLtNwVM3dkZ8aeOGCONNs40Ohyh5nNOfe2MUmBfFHkA==', 'active', 0),
(20, 'fb.user', 'xjTWrbBE8iv5NBOvJ/LFY7N8323M5xKSCkWOUUgmiIRaJeym9zuRlFCmOF0fS2jz7EEZ1iPF49MnOw622TwiUw==', 'active', 0),
(21, 'cg_admin', 'QoOT0G7fdohy5jksV8CNMGkOGJTE8RRACLJTvIwoFd1AlpQLgkrR3n4gEelHpQ2pZmpddIhPZhPzyPYJii48Zg==', 'active', 0),
(23, 'lp.user', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(24, 'lp.onduty', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(25, 'maintenance', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(68, 'jomer', '7t+qAS+iCpByTKQnId1F26a7IjkIIGYme6VmcxQE3FfkHq5Vh8YQpE3SGk/05swVmqy1cTQZFkzqYv2bPRKoVg==', 'active', 1342044000);

-- --------------------------------------------------------

--
-- Table structure for table `emp_work_experience`
--

CREATE TABLE IF NOT EXISTS `emp_work_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `employer` varchar(65) NOT NULL,
  `job_title` varchar(65) NOT NULL,
  `work_start` int(11) NOT NULL,
  `work_end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `emp_work_experience`
--

INSERT INTO `emp_work_experience` (`id`, `emp_id`, `employer`, `job_title`, `work_start`, `work_end`) VALUES
(1, 11, 'Cleverlearn English Language Institute', 'Teacher', 1323817200, 1324076400),
(2, 11, 'LexMark', 'Software Engineer', 1323990000, 1324076400),
(7, 11, 'dummy', 'dummy', 1329433200, 1330038000),
(10, 14, '', '', 1334322000, 1334322000);

-- --------------------------------------------------------

--
-- Table structure for table `fb_menu`
--

CREATE TABLE IF NOT EXISTS `fb_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name_id` int(11) DEFAULT NULL,
  `meal_type_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `ingredient_id` (`ingredient_id`),
  KEY `added_by` (`added_by`),
  KEY `menu_name_id` (`menu_name_id`),
  KEY `meal_type_id` (`meal_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `fb_menu`
--

INSERT INTO `fb_menu` (`menu_id`, `menu_name_id`, `meal_type_id`, `ingredient_id`, `status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 1, 1, 1, 'active', '0000-00-00 00:00:00', '2012-03-20 12:46:18', 20),
(2, 1, 2, 2, 'active', '0000-00-00 00:00:00', '2012-03-20 12:46:18', 20);

-- --------------------------------------------------------

--
-- Table structure for table `guests_1`
--

CREATE TABLE IF NOT EXISTS `guests_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_agent` varchar(30) DEFAULT NULL,
  `confirmation_no` int(11) DEFAULT NULL,
  `fo_staff` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `country` varchar(30) NOT NULL,
  `civil_status` enum('single','married','widowed') NOT NULL,
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `email_address` varchar(30) DEFAULT NULL,
  `company` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `passport_no` varchar(45) NOT NULL,
  `contact_no` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fo_staff` (`fo_staff`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `guests_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `guest_accom`
--

CREATE TABLE IF NOT EXISTS `guest_accom` (
  `guest_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(25) DEFAULT NULL,
  `booking_agent` varchar(35) DEFAULT NULL,
  `checkin_date` datetime DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`guest_accom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `guest_accom`
--

INSERT INTO `guest_accom` (`guest_accom_id`, `booking_id`, `booking_agent`, `checkin_date`, `checkout_date`, `comments`) VALUES
(1, '123456789', 'Corporate Rate', '2012-02-07 12:00:00', '2012-02-09 12:00:00', '[removed]alert&#40;''test''&#41;[removed]'),
(2, 'asdf', 'Asia Rooms', '2012-02-14 12:00:00', '2012-02-17 12:00:00', 'test'),
(3, '123', 'Late Rooms', '2012-02-08 00:00:00', '2012-02-16 00:00:00', ''),
(4, '123456789', 'Asia Rooms', '2012-02-08 12:00:00', '2012-02-08 12:00:00', ''),
(5, '123654', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(6, '', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(7, '56', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(8, '', 'Rates Go', '2012-07-17 14:52:00', '2012-07-28 00:00:00', ''),
(9, '12564', 'Bigfoot/IAFT', '2012-07-18 00:00:00', '2012-07-17 00:00:00', ''),
(10, '45', 'Rates Go', '2012-07-18 00:00:00', '2012-07-26 00:00:00', 'Na'),
(11, 'asdf', 'Agoda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(12, 'asdf', 'Agoda', '2012-02-16 23:00:00', '2012-02-13 00:00:00', ''),
(13, 'sdf', 'Booking.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(14, 'w3r', 'Agoda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(15, '22', 'Asia Rooms', '2012-02-24 10:29:00', '2012-02-20 00:00:00', 'sd'),
(16, '123-569', 'Bigfoot/IAFT', '2012-03-18 00:39:00', '0000-00-00 00:00:00', 'ds'),
(17, '123', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'comment rasad ni...'),
(18, 'sdfdsf', 'Walk-in', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'sS'),
(19, '123', 'Booking.com', '2012-02-29 07:25:00', '2012-03-01 12:31:00', ''),
(20, '145', 'Asia Rooms', '2012-02-29 12:00:00', '2012-02-29 12:00:00', 'this is comment test'),
(21, 'dfgdfg', 'Booking.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'comment'),
(22, '123', 'Room Rate', '2012-05-22 09:31:00', '0000-00-00 00:00:00', 'ssdfsdfsdfdfds'),
(23, '123', 'Late Rooms', '2012-03-15 11:29:00', '2012-03-20 19:07:00', 'sdsdfsdfdfd'),
(24, 'sdf', 'Asia Rooms', '2012-03-14 12:00:00', '0000-00-00 00:00:00', 'klkj'),
(25, 'dfds', 'Asia Rooms', '2012-03-13 12:00:00', '0000-00-00 00:00:00', 'f'),
(26, '58974', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(27, '23', 'Agoda', '2012-08-21 00:00:00', '2012-08-20 00:00:00', ''),
(28, '145', 'Agoda', '2012-06-18 15:52:00', '2012-06-14 15:52:00', 'fs'),
(29, 'x', 'Booking.com', '2012-07-25 00:00:00', '2012-07-16 00:00:00', ''),
(30, '122304444', 'Agoda', '2012-08-11 00:00:00', '2012-08-12 00:00:00', 'prepaid, need extra bed, mini bardsddds'),
(31, '36', 'Rates Go', '2012-08-15 00:00:00', '2012-08-21 00:00:00', 'wow! it works.'),
(32, '56', 'Asia Rooms', '2012-08-21 00:00:00', '2012-08-21 00:00:00', ''),
(33, '1456', 'Bigfoot/IAFT', '2012-08-13 00:00:00', '2012-08-30 00:00:00', 'it works aha..');

-- --------------------------------------------------------

--
-- Table structure for table `guest_accommodation_1`
--

CREATE TABLE IF NOT EXISTS `guest_accommodation_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_number` int(11) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `booking_type` varchar(35) NOT NULL,
  `check_in` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check_out` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment` text,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('reserved','checkin','checkout') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_number` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `guest_accommodation_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `guest_accomp`
--

CREATE TABLE IF NOT EXISTS `guest_accomp` (
  `guest_accompid` int(11) NOT NULL AUTO_INCREMENT,
  `guest_accom_id` int(11) DEFAULT NULL,
  `lastname` varchar(25) DEFAULT NULL,
  `firstname` varchar(25) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `nationality` varchar(25) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `company` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `passport_no` varchar(35) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`guest_accompid`),
  KEY `guest_accom_id` (`guest_accom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `guest_accomp`
--

INSERT INTO `guest_accomp` (`guest_accompid`, `guest_accom_id`, `lastname`, `firstname`, `gender`, `nationality`, `dob`, `email`, `company`, `address`, `passport_no`, `contact_no`) VALUES
(1, 1, 'Doe', 'John', 'male', 'Filipino', '2012-02-01', 'johndoe@localhost.com', NULL, 'Philippines', '123456789', '123456789'),
(2, 2, '101', 'Guest', 'female', 'filipino', '2012-02-15', 'andro@localhost.com', NULL, 'Philippines', '123456789', '123456789'),
(3, 3, 'sdafsdfsadf', 'asdfsadf', 'male', '', '0000-00-00', '', NULL, 'cherry', 'asdkj678666', ''),
(4, 4, 'Please', 'Test', 'male', '', '0000-00-00', '', NULL, 'Philippines', '', ''),
(5, 5, 'Test', 'This is', 'female', '', '0000-00-00', '', NULL, 's', 'asf', ''),
(6, 6, '', '', 'female', '', '0000-00-00', NULL, NULL, NULL, 'sd1', NULL),
(7, 7, 'f', 'd', 'female', '', '0000-00-00', '', NULL, '34', '14523', ''),
(8, 8, '', '', 'female', '', '0000-00-00', NULL, NULL, NULL, '5555', NULL),
(9, 9, 'r', 's', 'female', '', '0000-00-00', '', NULL, 'sd', '12', ''),
(10, 10, 'Uy', 'Caren', 'female', 'Chinese', '2012-07-20', NULL, NULL, NULL, '789', NULL),
(11, 11, 'asdf', 'asdf', 'female', '', '0000-00-00', '', NULL, 'asdf', '', ''),
(12, 13, 'asdf', 'asdfs', 'female', '', '0000-00-00', 's', NULL, 'asdf', '', 'history101s'),
(13, 13, 'Ka', 'Tiklo', 'female', '', '2012-02-17', '', NULL, 'asdf', '3432343', 'asd'),
(14, 14, 'Cleaverlearn', 'Celi', 'male', '', '0000-00-00', '', NULL, '2143', '', ''),
(15, 15, 'Go', 'Tina', 'male', '', '2012-02-24', 'vinzadz1987', NULL, 'Opon', 'dfsdf', ''),
(16, 16, 'Rosielda', 'Samuel', 'male', '', '2012-03-20', '', NULL, 'Opon, Lapu Lapu City', '', ''),
(17, 17, 'Lucena', 'Rico', 'male', '', '0000-00-00', '', NULL, 'Opon', '', ''),
(18, 18, 'Long', 'Wong chen', 'female', '', '0000-00-00', '', NULL, 'sdfsdf', '', ''),
(19, 19, 'Prinsesa', 'Puerto', 'male', '', '2012-02-24', 'vinzadz@yahoo.com', NULL, 'Opon', '124', ''),
(20, 20, 'Gwapo', 'Berting ', 'male', 'Fil', '2012-02-24', 'vinzadz1987@yahoo.com', NULL, 'Naval', '2563247', '9183139942'),
(21, 21, 'Pabling', 'Nong', 'female', '', '0000-00-00', '', NULL, 'dfg', '', 'dfgdfg'),
(22, 22, 'TestLAST', 'TestFIRST', 'male', '', '1987-03-13', '', NULL, 'TestADDRESS', '', ''),
(23, 23, 'Jose', 'Joselito', 'female', 'Fil', '2012-03-02', 'vinzadz1987@yahoo.com', NULL, 'Opon, Lapu Lapu', '1236fsd', '09183139942'),
(24, 24, 'leave', 'where', 'male', '', '0000-00-00', '', NULL, 'dsfsd', '', ''),
(25, 25, 'Ka', 'Tiklo', 'female', '', '0000-00-00', '', NULL, 'dsfsd', '', ''),
(26, 26, 'Fernandez', 'Reyjomer', 'female', '', '0000-00-00', '', NULL, 'SAAC II Lapu Lapu City', '', ''),
(27, 27, 'Montes', 'Paul', 'female', '', '2012-08-15', '', NULL, 'Larrazabal, Naval, Biliran', '', ''),
(28, 28, 'testname', 'test', 'female', '', '2012-04-03', '', NULL, 'testadress', '', ''),
(29, 29, 'Penduko', 'Pedro', 'female', '', '2012-07-02', 'ff', NULL, 'x', '', ''),
(30, 30, 'Pineda', 'Dave', 'male', '', '2012-08-08', '', NULL, 'Cebu City', '', ''),
(31, 31, 'Einstien', 'Albert', 'male', 'Canadian', '2012-08-21', 'sasfsdf@yahoo.com', NULL, 'Canada', '5694', '123'),
(32, 32, 'test', 'test', 'male', '', '2012-08-14', '', NULL, 'testadress', 'd', ''),
(33, 33, 'Muring', 'Emanuel', 'male', 'Filipino', '2012-08-14', 'eman@yahooo.com', NULL, 'Labangon', '4569', '45942236');

-- --------------------------------------------------------

--
-- Table structure for table `guest_room_tags_1`
--

CREATE TABLE IF NOT EXISTS `guest_room_tags_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_id` int(11) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accommodation_id` (`accommodation_id`),
  KEY `guest_id` (`guest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `guest_room_tags_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `guest_room_tag_1`
--

CREATE TABLE IF NOT EXISTS `guest_room_tag_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_id` int(11) NOT NULL,
  `guest_accommodation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `guest_id` (`guest_id`),
  KEY `guest_accommodation_id` (`guest_accommodation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `guest_room_tag_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `hk_attendant`
--

CREATE TABLE IF NOT EXISTS `hk_attendant` (
  `hk_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  PRIMARY KEY (`hk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `hk_attendant`
--

INSERT INTO `hk_attendant` (`hk_id`, `fname`, `lname`) VALUES
(7, 'Reyjomer', 'Fernandez'),
(8, 'Melvin', 'Evangelista'),
(9, 'Alvin', 'Ado');

-- --------------------------------------------------------

--
-- Table structure for table `hk_category`
--

CREATE TABLE IF NOT EXISTS `hk_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hk_category`
--

INSERT INTO `hk_category` (`cat_id`, `cat_name`) VALUES
(1, 'Mini Bar'),
(2, 'Linens'),
(3, 'Aminities');

-- --------------------------------------------------------

--
-- Table structure for table `hk_inv_amenities`
--

CREATE TABLE IF NOT EXISTS `hk_inv_amenities` (
  `amin_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `beg_bal` int(11) NOT NULL,
  `purchased` int(11) NOT NULL,
  `t_out` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `psy_count` int(11) NOT NULL,
  `remarks` varchar(120) NOT NULL,
  `reconcilation` varchar(120) NOT NULL,
  PRIMARY KEY (`amin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `hk_inv_amenities`
--

INSERT INTO `hk_inv_amenities` (`amin_id`, `inv_id`, `item_id`, `beg_bal`, `purchased`, `t_out`, `balance`, `psy_count`, `remarks`, `reconcilation`) VALUES
(34, 17, 46, 29, 72, 39, 62, 62, 'ok', 'na'),
(36, 17, 36, 20, 72, 0, 92, 52, 'na', 'na'),
(37, 17, 47, 57, 140, 0, 197, 65, 'none', 'none'),
(38, 17, 48, 0, 72, 0, 72, 54, 'none', 'none'),
(39, 17, 48, 5, 4, 1, 8, 3, '3', '2'),
(40, 18, 0, 0, 9, 2, 10, 10, 'none', 'ok'),
(42, 17, 36, 4, 4, 2, 6, 6, 'k', 'k'),
(44, 17, 48, 5, 2, 1, 6, 6, 'none', 'none'),
(46, 19, 47, 55, 12, 4, 63, 63, 'na', 'none'),
(51, 18, 36, 8, 8, 9, 7, 14, 'ok', 'na'),
(59, 19, 46, 8, 8, 8, 8, 8, '8', '8'),
(63, 19, 36, 8, 8, 8, 8, 8, '8', 'ko'),
(65, 19, 48, 4, 4, 4, 4, 4, '4', '4'),
(66, 18, 46, 5, 4, 4, 5, 4, '4', '4'),
(67, 27, 48, 44, 43, 5, 82, 4, 'none', 'none'),
(68, 30, 36, 4, 4, 4, 4, 4, '4', '4'),
(69, 18, 48, 7, 7, 7, 7, 7, 'ok', 'ok'),
(70, 27, 46, 1, 14, 2, 13, 1, 'ok', 'ok'),
(71, 18, 47, 4, 4, 4, 4, 4, '4', '4'),
(73, 27, 47, 9, 9, 2, 16, 16, 'ok', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `hk_inv_linen`
--

CREATE TABLE IF NOT EXISTS `hk_inv_linen` (
  `linen_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `beg_bal` int(11) NOT NULL,
  `purchased` int(11) NOT NULL,
  `stock_rooms` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `pick_up_laundry` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  `damaged` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`linen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `hk_inv_linen`
--

INSERT INTO `hk_inv_linen` (`linen_id`, `inv_id`, `item_id`, `beg_bal`, `purchased`, `stock_rooms`, `rooms`, `pick_up_laundry`, `sub_total`, `damaged`, `total`) VALUES
(3, 33, 37, 4, 4, 4, 4, 4, 16, 0, 0),
(6, 33, 58, 4, 4, 4, 4, 4, 16, 4, 16),
(7, 36, 55, 1, 78, 2, 1, 444, 524, 2, 524),
(8, 32, 57, 4, 4, 4, 4, 4, 18, 2, 18);

-- --------------------------------------------------------

--
-- Table structure for table `hk_inv_minibar`
--

CREATE TABLE IF NOT EXISTS `hk_inv_minibar` (
  `mini_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `beg_bal` int(11) NOT NULL,
  `purchased` int(11) NOT NULL,
  `t_stock` int(11) NOT NULL,
  `out` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `psy_count` int(11) NOT NULL,
  `hotel_room_from` int(11) NOT NULL,
  `hotel_room_to` int(11) NOT NULL,
  `remarks` varchar(120) NOT NULL,
  PRIMARY KEY (`mini_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hk_inv_minibar`
--

INSERT INTO `hk_inv_minibar` (`mini_id`, `inv_id`, `item_id`, `beg_bal`, `purchased`, `t_stock`, `out`, `balance`, `psy_count`, `hotel_room_from`, `hotel_room_to`, `remarks`) VALUES
(12, 31, 51, 8, 8, 14, 2, 14, 14, 104, 107, 'ok'),
(13, 29, 38, 44, 42, 61, 25, 61, 61, 103, 102, 'ok'),
(15, 29, 54, 5, 8, 8, 5, 8, 8, 102, 103, 'ok'),
(16, 31, 50, 25, 23, 46, 2, 46, 25, 102, 112, 'ok');

-- --------------------------------------------------------

--
-- Table structure for table `job_functions`
--

CREATE TABLE IF NOT EXISTS `job_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobtitle_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `jobtitle_id` (`jobtitle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `job_functions`
--


-- --------------------------------------------------------

--
-- Table structure for table `job_status`
--

CREATE TABLE IF NOT EXISTS `job_status` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(150) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `job_status`
--

INSERT INTO `job_status` (`job_id`, `status`) VALUES
(1, 'Active'),
(2, 'Resigned'),
(3, 'Terminated');

-- --------------------------------------------------------

--
-- Table structure for table `job_title`
--

CREATE TABLE IF NOT EXISTS `job_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `job_title`
--

INSERT INTO `job_title` (`id`, `name`) VALUES
(1, 'Web Programmer'),
(2, 'HR Head'),
(3, 'Web Designer'),
(4, 'Search Engine Optimizer'),
(5, 'IT Head'),
(6, 'Academics Supervisor'),
(7, 'Teacher'),
(8, 'Maintenance'),
(9, 'CG Head');

-- --------------------------------------------------------

--
-- Table structure for table `lp_dsales`
--

CREATE TABLE IF NOT EXISTS `lp_dsales` (
  `dsales_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(120) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` float(15,2) NOT NULL,
  `total_sales` float(15,2) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`dsales_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=486 ;

--
-- Dumping data for table `lp_dsales`
--

INSERT INTO `lp_dsales` (`dsales_id`, `description`, `quantity`, `price`, `total_sales`, `date_added`) VALUES
(354, 'Happy', 1, 1.00, 1.00, '07-20-2012'),
(356, 'Fit and Right', 1, 28.00, 28.00, '07-20-2012'),
(383, 'Lucky Me', 1, 15.00, 15.00, '07-20-2012'),
(399, 'Nagaray', 1, 2.00, 2.00, '07-20-2012'),
(400, 'Max', 1, 1.00, 1.00, '07-20-2012'),
(408, 'Palesin', 1, 15.00, 15.00, '07-20-2012'),
(439, 'Lucky Me', 1, 15.00, 15.00, '07-23-2012'),
(443, 'Fit and Right', 1, 28.00, 28.00, '07-24-2012'),
(444, 'Lucky Me', 1, 15.00, 15.00, '07-24-2012'),
(445, 'Choey Choco', 1, 1.00, 1.00, '07-24-2012'),
(446, 'Happy', 1, 1.00, 1.00, '07-24-2012'),
(447, 'Safegaurd', 1, 12.00, 12.00, '07-24-2012'),
(448, 'Slice Bread', 1, 20.00, 20.00, '07-24-2012'),
(449, 'Safari', 1, 1.00, 1.00, '07-24-2012'),
(450, 'Yari Ka', 1, 1.00, 1.00, '07-24-2012'),
(451, 'Slice Bread', 1, 20.00, 20.00, '07-24-2012'),
(452, 'Slice Bread', 1, 20.00, 20.00, '07-24-2012'),
(453, 'Safegaurd', 1, 12.00, 12.00, '07-24-2012'),
(454, 'Fit and Right', 1, 28.00, 28.00, '07-27-2012'),
(455, 'Happy', 1, 1.00, 1.00, '07-27-2012'),
(456, 'Max', 1, 1.00, 1.00, '07-27-2012'),
(457, 'Palesin', 1, 15.00, 15.00, '07-27-2012'),
(458, 'Choey Choco', 1, 1.00, 1.00, '07-30-2012'),
(459, 'Fit and Right', 1, 28.00, 28.00, '07-30-2012'),
(460, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(461, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(462, 'Max', 1, 1.00, 1.00, '07-31-2012'),
(463, 'Safegaurd', 1, 12.00, 12.00, '07-31-2012'),
(464, 'Max', 1, 1.00, 1.00, '07-31-2012'),
(465, 'Max', 1, 1.00, 1.00, '07-31-2012'),
(466, 'Safegaurd', 1, 12.00, 12.00, '07-31-2012'),
(467, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(468, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(469, 'Marlboro', 1, 2.00, 2.00, '07-31-2012'),
(470, 'SanMig Light', 1, 15.00, 15.00, '07-31-2012'),
(472, 'Safegaurd', 1, 12.00, 12.00, '07-31-2012'),
(473, 'SkyFlakes', 1, 5.00, 5.00, '07-31-2012'),
(474, 'SkyFlakes', 1, 5.00, 5.00, '07-31-2012'),
(475, 'SanMig Light', 1, 15.00, 15.00, '07-31-2012'),
(476, 'Yari Ka', 1, 1.00, 1.00, '07-31-2012'),
(477, 'SkyFlakes', 1, 5.00, 5.00, '07-31-2012'),
(478, 'Safari', 1, 1.00, 1.00, '07-31-2012'),
(479, 'Happy', 1, 1.00, 1.00, '08-01-2012'),
(480, 'Lucky Me', 1, 15.00, 15.00, '08-01-2012'),
(481, 'Nagaray', 1, 2.00, 2.00, '08-01-2012'),
(482, 'Safegaurd', 1, 12.00, 12.00, '08-01-2012'),
(483, 'SanMig Light', 1, 15.00, 15.00, '08-01-2012'),
(484, 'SkyFlakes', 1, 5.00, 5.00, '08-01-2012'),
(485, 'Slice Bread', 1, 20.00, 20.00, '08-01-2012');

-- --------------------------------------------------------

--
-- Table structure for table `lp_item_cat`
--

CREATE TABLE IF NOT EXISTS `lp_item_cat` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `lp_item_cat`
--

INSERT INTO `lp_item_cat` (`cat_id`, `name`) VALUES
(1, 'BISCUITS'),
(2, 'PEANUTS'),
(3, 'FROZEN / BREADS'),
(4, 'CHOCOLATES'),
(5, 'JUNKS'),
(6, 'DRINKS'),
(7, 'INS. PANCIT CANTON'),
(8, 'CIGARETTES'),
(9, 'CANDIES'),
(10, 'JUICES IN CAN/ BOTTLE'),
(11, 'OTHERS');

-- --------------------------------------------------------

--
-- Table structure for table `lp_price_list`
--

CREATE TABLE IF NOT EXISTS `lp_price_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(120) NOT NULL,
  `price` float(15,2) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `lp_price_list`
--

INSERT INTO `lp_price_list` (`id`, `description`, `price`, `cat_id`) VALUES
(68, 'Happy', 1.00, 2),
(71, 'Slice Bread', 20.00, 3),
(72, 'SanMig Light', 15.00, 6),
(74, 'Marlboro', 2.00, 8),
(75, 'Lucky Me', 15.00, 7),
(76, 'Choey Choco', 1.00, 4),
(77, 'SkyFlakes', 5.00, 1),
(79, 'Max', 1.00, 9),
(80, 'Fit and Right', 28.00, 10),
(81, 'Yari Ka', 1.00, 5),
(82, 'Safari', 1.00, 5),
(84, 'Palesin', 15.00, 6),
(85, 'Safegaurd', 12.00, 11),
(87, 'Nagaray', 2.00, 2);

-- --------------------------------------------------------

--
-- Table structure for table `lp_pys_inv`
--

CREATE TABLE IF NOT EXISTS `lp_pys_inv` (
  `lp_pys_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `description` varchar(120) NOT NULL,
  `price` float(15,2) NOT NULL,
  `sold_items` int(11) NOT NULL,
  `total_cost` float(15,2) NOT NULL,
  PRIMARY KEY (`lp_pys_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `lp_pys_inv`
--

INSERT INTO `lp_pys_inv` (`lp_pys_id`, `inv_id`, `cat_id`, `description`, `price`, `sold_items`, `total_cost`) VALUES
(13, 0, 2, 'Happy', 1.00, 14, 14.00),
(16, 0, 3, 'Slice Bread', 20.00, 9, 180.00),
(17, 0, 6, 'SanMig Light', 15.00, 6, 90.00),
(19, 0, 8, 'Marlboro', 2.00, 25, 50.00),
(20, 0, 7, 'Lucky Me', 15.00, 10, 150.00),
(21, 0, 4, 'Choey Choco', 1.00, 21, 21.00),
(22, 0, 1, 'SkyFlakes', 5.00, 23, 115.00),
(24, 0, 9, 'Max', 1.00, 4, 4.00),
(25, 0, 10, 'Fit and Right', 28.00, 10, 280.00),
(26, 0, 5, 'Yari Ka', 1.00, 40, 40.00),
(27, 0, 5, 'Safari', 1.00, 13, 13.00),
(29, 0, 6, 'Palesin', 15.00, 23, 345.00),
(30, 0, 11, 'Safegaurd', 12.00, 25, 300.00),
(32, 0, 2, 'Nagaray', 2.00, 0, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `mat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(120) NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`mat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`mat_id`, `name`, `description`, `added_by`, `date_added`) VALUES
(3, 'Tables', 'None', 21, '05/02/2012'),
(4, 'Chairs', 'None', 21, '05/15/2012');

-- --------------------------------------------------------

--
-- Table structure for table `menu_meal_type`
--

CREATE TABLE IF NOT EXISTS `menu_meal_type` (
  `meal_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`meal_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_meal_type`
--

INSERT INTO `menu_meal_type` (`meal_type_id`, `meal_name`) VALUES
(1, 'breakfast'),
(2, 'lunch'),
(3, 'dinner');

-- --------------------------------------------------------

--
-- Table structure for table `menu_name`
--

CREATE TABLE IF NOT EXISTS `menu_name` (
  `menu_name_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_name_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `menu_name`
--

INSERT INTO `menu_name` (`menu_name_id`, `menu_name`, `menu_status`, `date_added`, `last_update`, `added_by`) VALUES
(1, 'Chicken Soup', 'active', '0000-00-00 00:00:00', '2012-03-20 11:31:56', 20);

-- --------------------------------------------------------

--
-- Table structure for table `menu_status_id`
--

CREATE TABLE IF NOT EXISTS `menu_status_id` (
  `ingredient_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_status_name` varchar(65) NOT NULL,
  `menu_status_css` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`ingredient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_status_id`
--

INSERT INTO `menu_status_id` (`ingredient_id`, `menu_status_name`, `menu_status_css`) VALUES
(1, 'Ordered', 'hr-vc'),
(2, 'Reserved', 'hr-oc'),
(3, 'Out of stock', 'hr-wr');

-- --------------------------------------------------------

--
-- Table structure for table `ml_month`
--

CREATE TABLE IF NOT EXISTS `ml_month` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ml_month`
--


-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `mod_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`mod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`mod_id`, `name`, `description`) VALUES
(1, 'LCP', 'Live-in Caregiver Program'),
(2, 'PDEC', 'Personality Development and Effective Communication'),
(3, 'BHCP 1', 'Basic Healthcare and Procedures 1'),
(4, 'BHCP 2', 'Basic Healthcare and Procedures 2'),
(5, 'ICC', 'Infant and Child Care'),
(6, 'GC', 'Geriatric Care'),
(7, 'DSN', 'Disabilities and Special Needs'),
(8, 'BEC', 'Basic Emergency Care'),
(9, 'HMN', 'Home Management and Nutrition');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL COMMENT 'emp_id where the notification came from',
  `target` varchar(25) NOT NULL COMMENT 'all or by department or by employee id',
  `msg` text NOT NULL COMMENT 'the notification message',
  `date` int(11) NOT NULL COMMENT 'unix time where the notification was made/created',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `notifications`
--


-- --------------------------------------------------------

--
-- Table structure for table `ops_am`
--

CREATE TABLE IF NOT EXISTS `ops_am` (
  `am_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(6) NOT NULL,
  PRIMARY KEY (`am_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `ops_am`
--

INSERT INTO `ops_am` (`am_id`, `name`) VALUES
(1, '1:00'),
(2, '1:30'),
(3, '2:00'),
(4, '2:30'),
(5, '3:00'),
(6, '3:30'),
(7, '4:00'),
(8, '4:30'),
(9, '5:00'),
(10, '5:30'),
(11, '6:00'),
(12, '6:30'),
(13, '7:00'),
(14, '7:30'),
(15, '8:00'),
(16, '8:30'),
(17, '9:00'),
(18, '9:30'),
(19, '10:00'),
(20, '10:30'),
(21, '11:00'),
(22, '11:30'),
(23, '12:00'),
(24, '12:30');

-- --------------------------------------------------------

--
-- Table structure for table `ops_day`
--

CREATE TABLE IF NOT EXISTS `ops_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ops_day`
--

INSERT INTO `ops_day` (`id`, `name`) VALUES
(1, 'Monday'),
(2, 'Tuesday'),
(3, 'Wednesday'),
(4, 'Thursday'),
(5, 'Friday'),
(6, 'Saturday'),
(7, 'Sunday');

-- --------------------------------------------------------

--
-- Table structure for table `ops_delivery_schedule`
--

CREATE TABLE IF NOT EXISTS `ops_delivery_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `date_delivered` date NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ops_delivery_schedule`
--

INSERT INTO `ops_delivery_schedule` (`id`, `item_name`, `supplier`, `date_delivered`, `date_added`) VALUES
(3, 9, 11, '2012-05-30', '2012-05-18'),
(9, 16, 7, '2012-05-01', '2012-05-18'),
(10, 18, 12, '2012-05-16', '2012-05-18'),
(11, 8, 3, '2012-05-08', '2012-05-21'),
(12, 14, 7, '2012-05-11', '2012-05-21');

-- --------------------------------------------------------

--
-- Table structure for table `ops_fb_shortage_wastage`
--

CREATE TABLE IF NOT EXISTS `ops_fb_shortage_wastage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_of_students` int(11) NOT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `day` varchar(5) NOT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `ops_fb_shortage_wastage`
--

INSERT INTO `ops_fb_shortage_wastage` (`id`, `number_of_students`, `from`, `to`, `day`, `date_added`) VALUES
(25, 67, '2012-05-21', '2012-05-24', '', NULL),
(26, 23, '2012-05-28', '2012-05-31', '', NULL),
(29, 5, '2012-05-30', '2012-05-29', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ops_hk_inventory`
--

CREATE TABLE IF NOT EXISTS `ops_hk_inventory` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_from` varchar(20) NOT NULL,
  `inv_to` varchar(20) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `ops_hk_inventory`
--

INSERT INTO `ops_hk_inventory` (`inv_id`, `inv_from`, `inv_to`, `cat_id`) VALUES
(18, '06-01-2012', '06-30-2012', 3),
(27, '06-05-2012', '06-04-2012', 3),
(29, '06-05-2012', '06-11-2012', 1),
(31, '06-13-2012', '06-30-2012', 1),
(32, '06-13-2012', '06-10-2012', 2),
(36, '06-17-2012', '06-03-2012', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ops_hk_items`
--

CREATE TABLE IF NOT EXISTS `ops_hk_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_desc` varchar(120) NOT NULL,
  `price` float(11,2) NOT NULL,
  `item_cat` int(11) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `ops_hk_items`
--

INSERT INTO `ops_hk_items` (`item_id`, `item_desc`, `price`, `item_cat`) VALUES
(36, 'Colgate toothpaste', 12.55, 3),
(37, 'Pilow Case', 10.30, 2),
(38, 'Piattos', 5.00, 1),
(46, 'Cleene Economy Tootbrush', 12.00, 3),
(47, 'Sunsilk shampoo', 12.60, 0),
(48, 'Safeguard soap 25s', 21.26, 1),
(49, 'Nova Small', 6.00, 1),
(50, 'Choco Mucho', 10.00, 1),
(51, 'Cloud 9', 9.00, 1),
(52, 'Gatorade', 13.23, 1),
(53, 'Coke reg', 12.00, 1),
(54, '7 Up', 30.00, 1),
(55, 'Fitted', 0.00, 2),
(56, 'Flat', 0.00, 2),
(57, 'Pillow Case', 0.00, 2),
(58, 'Bed Pad', 0.00, 2),
(59, 'Blanket', 0.00, 2),
(60, 'Blanket', 0.00, 2),
(61, 'Bath Mat', 0.00, 2),
(62, 'Bath Towel', 0.00, 2),
(63, 'Bath Towel New', 0.00, 2),
(64, 'Pillow', 0.00, 2),
(65, 'Window Curtain', 0.00, 2),
(66, 'Shower Curtain', 0.00, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ops_ingredient`
--

CREATE TABLE IF NOT EXISTS `ops_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `day` int(11) NOT NULL,
  `dte` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `ops_ingredient`
--

INSERT INTO `ops_ingredient` (`id`, `name`, `desc`, `day`, `dte`) VALUES
(25, 'beverage powder', 'beverage powder', 6, '2012-05-14'),
(26, 'Food', 'food', 2, '2012-05-24'),
(27, 'soy sauce', 'sauce', 2, '2012-05-24'),
(28, 'Sugar', 'Sugar', 0, '2012-06-20'),
(29, 'Alvin', 'Ado', 1, '2012-07-19');

-- --------------------------------------------------------

--
-- Table structure for table `ops_lp_inv`
--

CREATE TABLE IF NOT EXISTS `ops_lp_inv` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_from` varchar(100) NOT NULL,
  `inv_to` varchar(100) NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_lp_inv`
--

INSERT INTO `ops_lp_inv` (`inv_id`, `inv_from`, `inv_to`) VALUES
(1, 'Jun 18, 2012', 'Jun 30, 2012'),
(2, 'Jul 03, 2012', 'Jul 02, 2012');

-- --------------------------------------------------------

--
-- Table structure for table `ops_main_tools`
--

CREATE TABLE IF NOT EXISTS `ops_main_tools` (
  `tools_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_num` int(11) NOT NULL,
  `description` varchar(120) NOT NULL,
  `quantity` int(11) NOT NULL,
  `uom` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  PRIMARY KEY (`tools_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ops_main_tools`
--

INSERT INTO `ops_main_tools` (`tools_id`, `item_num`, `description`, `quantity`, `uom`, `department`, `date`) VALUES
(8, 156, 'Wires', 4, 1, 2, '07-11-2012'),
(9, 18, 'Table', 18, 1, 7, '07-11-2012'),
(10, 12, 'toolstry', 21, 1, 3, '07-11-2012');

-- --------------------------------------------------------

--
-- Table structure for table `ops_pm`
--

CREATE TABLE IF NOT EXISTS `ops_pm` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `ops_pm`
--

INSERT INTO `ops_pm` (`pm_id`, `name`) VALUES
(1, '1:00'),
(2, '1:30'),
(3, '2:00'),
(4, '2:30'),
(5, '3:00'),
(6, '3:30'),
(7, '4:00'),
(8, '4:30'),
(9, '5:00'),
(10, '5:30'),
(11, '6:00'),
(12, '6:30'),
(13, '7:00'),
(14, '7:30'),
(15, '8:00'),
(16, '8:30'),
(17, '9:00'),
(18, '9:30'),
(19, '10:00'),
(20, '10:30'),
(21, '11:00'),
(22, '11:30'),
(23, '12:00'),
(24, '12:30');

-- --------------------------------------------------------

--
-- Table structure for table `ops_rm_attendant_dchecklist`
--

CREATE TABLE IF NOT EXISTS `ops_rm_attendant_dchecklist` (
  `ra_checklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_attendant_id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `time_in` varchar(30) NOT NULL,
  `time_out` varchar(30) NOT NULL,
  `room_status_id` int(11) NOT NULL,
  `guest_no` int(11) NOT NULL,
  `bt_in` int(11) NOT NULL,
  `bt_out` int(11) NOT NULL,
  `bm_in` int(11) NOT NULL,
  `bm_out` int(11) NOT NULL,
  `fit_in` int(11) NOT NULL,
  `fit_out` int(11) NOT NULL,
  `flat_in` int(11) NOT NULL,
  `flat_out` int(11) NOT NULL,
  `blanket_in` int(11) NOT NULL,
  `blanket_out` int(11) NOT NULL,
  `pilow_in` int(11) NOT NULL,
  `pilow_out` int(11) NOT NULL,
  `pilocs_in` int(11) NOT NULL,
  `pilocs_out` int(11) NOT NULL,
  `toilet_tissue_in` int(11) NOT NULL,
  `toilet_tissue_out` int(11) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `extra_work_done` varchar(100) NOT NULL,
  PRIMARY KEY (`ra_checklist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `ops_rm_attendant_dchecklist`
--

INSERT INTO `ops_rm_attendant_dchecklist` (`ra_checklist_id`, `room_attendant_id`, `room_no`, `time_in`, `time_out`, `room_status_id`, `guest_no`, `bt_in`, `bt_out`, `bm_in`, `bm_out`, `fit_in`, `fit_out`, `flat_in`, `flat_out`, `blanket_in`, `blanket_out`, `pilow_in`, `pilow_out`, `pilocs_in`, `pilocs_out`, `toilet_tissue_in`, `toilet_tissue_out`, `remarks`, `extra_work_done`) VALUES
(2, 48, 13, '10:05 AM', '11:05 AM', 3, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 'broken vase', 'wipe railings'),
(28, 48, 1, '10:00 AM', '10:00 AM', 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 0, 0, 1, 1, 'Broken Glass', 'Swipe Dust'),
(30, 53, 16, '11:00 AM', '11:03 AM', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 'Busted Ako', 'Everything is Done'),
(34, 54, 6, '11:00AM', '12:00AM', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Dark Color', 'Nothing Done'),
(50, 40, 11, '11:00 AM', '11:30 AM', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 'try', 'try'),
(52, 44, 4, '11:05AM', '11:06PM', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'try', 'try'),
(54, 45, 7, '5:00 AM ', '5:00 AM ', 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 'trythis', 'None'),
(55, 42, 10, '10:21 am', '5:00 AM ', 3, 1, 2, 2, 0, 2, 1, 0, 2, 0, 2, 2, 0, 2, 0, 0, 2, 1, 'try', 'wipe railing'),
(56, 42, 16, '10:00 am', '10:00 am', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 0, 0, 2, 2, 'ambot', 'nindot');

-- --------------------------------------------------------

--
-- Table structure for table `ops_rooms_attendant`
--

CREATE TABLE IF NOT EXISTS `ops_rooms_attendant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ra_date` date DEFAULT NULL,
  `am` int(11) NOT NULL,
  `pm` int(11) NOT NULL,
  `prepared_by` int(11) NOT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `ops_rooms_attendant`
--

INSERT INTO `ops_rooms_attendant` (`id`, `ra_date`, `am`, `pm`, `prepared_by`, `date_added`) VALUES
(40, '2012-05-23', 9, 7, 2, '2012-05-25'),
(41, '2012-05-01', 3, 4, 1, '2012-05-31'),
(42, '2012-05-02', 3, 5, 7, '2012-05-31'),
(44, '2012-05-09', 4, 3, 5, '2012-05-31'),
(45, '2012-05-14', 9, 7, 8, '2012-05-31'),
(48, '2012-07-09', 11, 1, 9, '2012-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `ops_shortage_wastage_tracker`
--

CREATE TABLE IF NOT EXISTS `ops_shortage_wastage_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortage_wastage_id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `b_shortage` int(11) NOT NULL,
  `b_wastage` int(11) NOT NULL,
  `b_comment` varchar(50) NOT NULL,
  `l_shortage` int(11) NOT NULL,
  `l_wastage` int(11) NOT NULL,
  `l_comment` varchar(50) NOT NULL,
  `d_shortage` int(11) NOT NULL,
  `d_wastage` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `ops_shortage_wastage_tracker`
--

INSERT INTO `ops_shortage_wastage_tracker` (`id`, `shortage_wastage_id`, `day`, `b_shortage`, `b_wastage`, `b_comment`, `l_shortage`, `l_wastage`, `l_comment`, `d_shortage`, `d_wastage`) VALUES
(20, 26, 0, 0, 0, '', 0, 0, '', 0, 0),
(21, 29, 0, 0, 0, '', 0, 0, '', 0, 0),
(22, 31, 1, 0, 0, '', 0, 0, '', 0, 0),
(27, 26, 1, 0, 0, '0', 1, 1, '1', 2, 2),
(34, 32, 2, 1, 1, '1', 1, 1, '1', 1, 1),
(40, 25, 1, 0, 0, '0', 0, 0, '0', 0, 0),
(41, 29, 2, 1, 1, '1', 1, 1, '1', 1, 1),
(51, 29, 6, 1, 1, '1', 1, 1, '1', 1, 1),
(52, 25, 1, 1, 1, '1', 1, 1, '1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ops_utensils`
--

CREATE TABLE IF NOT EXISTS `ops_utensils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(50) NOT NULL,
  `u_desc` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `added_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `ops_utensils`
--

INSERT INTO `ops_utensils` (`id`, `u_name`, `u_desc`, `quantity`, `added_date`) VALUES
(30, 'Pork', 'Porkie', 10, '2012-05-18');

-- --------------------------------------------------------

--
-- Table structure for table `ops_uten_status`
--

CREATE TABLE IF NOT EXISTS `ops_uten_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utensils_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ops_uten_status`
--

INSERT INTO `ops_uten_status` (`id`, `utensils_id`) VALUES
(1, 0),
(2, 0),
(3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_weekly_menu`
--

CREATE TABLE IF NOT EXISTS `ops_weekly_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(50) NOT NULL,
  `m_desc` varchar(100) NOT NULL,
  `ml_time` int(11) NOT NULL,
  `ml_month` int(11) NOT NULL,
  `ml_week` int(11) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `ops_weekly_menu`
--

INSERT INTO `ops_weekly_menu` (`id`, `m_name`, `m_desc`, `ml_time`, `ml_month`, `ml_week`, `date_added`) VALUES
(40, 'Fish Soup', 'Fish Soup', 1, 2, 1, '2012-05-17'),
(41, 'Meat Soup', 'Soup Meat', 1, 1, 1, '2012-05-18'),
(42, 'Salad', 'Salad', 3, 5, 2, '2012-05-21');

-- --------------------------------------------------------

--
-- Table structure for table `ops_wk_ml_month`
--

CREATE TABLE IF NOT EXISTS `ops_wk_ml_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ops_wk_ml_month`
--

INSERT INTO `ops_wk_ml_month` (`id`, `name`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `ops_wk_ml_time`
--

CREATE TABLE IF NOT EXISTS `ops_wk_ml_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ops_wk_ml_time`
--

INSERT INTO `ops_wk_ml_time` (`id`, `name`) VALUES
(1, 'Breakfast'),
(2, 'Lunch'),
(3, 'Dinner');

-- --------------------------------------------------------

--
-- Table structure for table `ops_wk_ml_week`
--

CREATE TABLE IF NOT EXISTS `ops_wk_ml_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ops_wk_ml_week`
--

INSERT INTO `ops_wk_ml_week` (`id`, `name`) VALUES
(1, '1st Week'),
(2, '2nd Week'),
(3, '3rd Week'),
(4, '4th Week');

-- --------------------------------------------------------

--
-- Table structure for table `payrolls`
--

CREATE TABLE IF NOT EXISTS `payrolls` (
  `payroll_id` int(11) NOT NULL AUTO_INCREMENT,
  `period_from` int(11) NOT NULL,
  `period_to` int(11) NOT NULL,
  `pay_date` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stat` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`payroll_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `payrolls`
--

INSERT INTO `payrolls` (`payroll_id`, `period_from`, `period_to`, `pay_date`, `date_added`, `stat`) VALUES
(13, 1337119200, 1338415200, 1338847200, '2012-06-05 18:17:43', 'active'),
(14, 1339711200, 1341007200, 1341007200, '2012-06-19 18:57:22', 'active'),
(15, 1342303200, 1343599200, 1343599200, '2012-06-21 11:26:49', 'active'),
(16, 1343253600, 1344549600, 1344981600, '2012-07-30 11:42:25', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_details`
--

CREATE TABLE IF NOT EXISTS `payroll_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `daily_rate` decimal(8,2) NOT NULL,
  `basic_pay` decimal(8,2) NOT NULL,
  `absences` decimal(8,2) NOT NULL,
  `late` decimal(8,2) NOT NULL,
  `reg_ot` decimal(8,2) NOT NULL,
  `reg_hol` decimal(8,2) NOT NULL,
  `sp_hol` decimal(8,2) NOT NULL,
  `leave` decimal(8,2) NOT NULL,
  `night_diff` decimal(8,2) NOT NULL,
  `allowance` decimal(8,2) NOT NULL,
  `adjustment` decimal(8,2) NOT NULL,
  `adj_non_tax` decimal(8,2) NOT NULL,
  `thirteenth_month` decimal(8,2) NOT NULL,
  `gross_income` decimal(8,2) NOT NULL,
  `sss` decimal(8,2) NOT NULL,
  `phic` decimal(8,2) NOT NULL,
  `hdmf` decimal(8,2) NOT NULL,
  `wtax` decimal(8,2) NOT NULL,
  `sss_loan` decimal(8,2) NOT NULL,
  `pibig_sloan` decimal(8,2) NOT NULL,
  `pibig_hloan` decimal(8,2) NOT NULL,
  `phone_bill` decimal(8,2) NOT NULL,
  `advances` decimal(8,2) NOT NULL,
  `uniform` decimal(8,2) NOT NULL,
  `others` decimal(8,2) NOT NULL,
  `canteen` decimal(8,2) NOT NULL,
  `total_deductions` decimal(8,2) NOT NULL,
  `total_net_pay` decimal(8,2) NOT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `payroll_details`
--

INSERT INTO `payroll_details` (`id`, `payroll_id`, `emp_id`, `daily_rate`, `basic_pay`, `absences`, `late`, `reg_ot`, `reg_hol`, `sp_hol`, `leave`, `night_diff`, `allowance`, `adjustment`, `adj_non_tax`, `thirteenth_month`, `gross_income`, `sss`, `phic`, `hdmf`, `wtax`, `sss_loan`, `pibig_sloan`, `pibig_hloan`, `phone_bill`, `advances`, `uniform`, `others`, `canteen`, `total_deductions`, `total_net_pay`, `date_added`) VALUES
(1, 14, 12, 305.75, 4650.00, 3057.53, 50.00, 0.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 1582.47, 316.70, 112.50, 100.00, 224.59, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 793.79, 788.68, 1341784800),
(2, 14, 11, 305.75, 4650.00, 5.00, 50.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 4640.00, 316.70, 112.50, 100.00, 225.34, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 794.54, 3845.46, 1341784800),
(3, 15, 11, 361.64, 5500.00, 11.00, 70.00, 10.00, 5.00, 2.00, 3.00, 1.00, 4.00, 5.00, 2.00, 40.00, 5491.00, 366.70, 137.50, 100.00, 337.13, 0.00, 0.00, 0.00, 5.00, 6.00, 8.00, 9.00, 4.00, 973.33, 4517.67, 1341784800);

-- --------------------------------------------------------

--
-- Table structure for table `pay_adjustments`
--

CREATE TABLE IF NOT EXISTS `pay_adjustments` (
  `adjustment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`adjustment_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pay_adjustments`
--


-- --------------------------------------------------------

--
-- Table structure for table `pay_allowances`
--

CREATE TABLE IF NOT EXISTS `pay_allowances` (
  `allowance_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `task` varchar(200) NOT NULL,
  `personal` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`allowance_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pay_allowances`
--


-- --------------------------------------------------------

--
-- Table structure for table `pay_canteen`
--

CREATE TABLE IF NOT EXISTS `pay_canteen` (
  `pay_canteen_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pay_canteen_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pay_canteen`
--


-- --------------------------------------------------------

--
-- Table structure for table `pay_holiday`
--

CREATE TABLE IF NOT EXISTS `pay_holiday` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `type` enum('special','legal') NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`holiday_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pay_holiday`
--


-- --------------------------------------------------------

--
-- Table structure for table `pay_night_diff`
--

CREATE TABLE IF NOT EXISTS `pay_night_diff` (
  `nightdiff_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`nightdiff_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pay_night_diff`
--


-- --------------------------------------------------------

--
-- Table structure for table `pay_uniform`
--

CREATE TABLE IF NOT EXISTS `pay_uniform` (
  `pay_uniform_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pay_uniform_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pay_uniform`
--


-- --------------------------------------------------------

--
-- Table structure for table `prs`
--

CREATE TABLE IF NOT EXISTS `prs` (
  `prs_id` int(11) NOT NULL AUTO_INCREMENT,
  `requested_by` int(11) NOT NULL COMMENT 'emp_id of the employee',
  `date_requested` int(11) NOT NULL DEFAULT '0',
  `date_required` int(11) NOT NULL DEFAULT '0',
  `supplier_name` varchar(50) NOT NULL,
  `type` enum('stock','asset') NOT NULL DEFAULT 'stock',
  `payment_type` enum('cash','check','credit card','others') NOT NULL DEFAULT 'others' COMMENT 'cc = credit card, c = cash, chq = cheque',
  `dept_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending' COMMENT 'department head approval, 0 means not yet approved',
  `fin_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending' COMMENT 'finance head approval, 0 means not yet approved',
  `date_received` int(11) NOT NULL DEFAULT '0' COMMENT 'date the item/items recieved in timestamp, 0 means the item is not yet recieved. If this has a value other than zero means the item/items were recieved and update the stock table if it is stock and the assets table if it is an asset of the employee',
  PRIMARY KEY (`prs_id`),
  KEY `requested_by` (`requested_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `prs`
--

INSERT INTO `prs` (`prs_id`, `requested_by`, `date_requested`, `date_required`, `supplier_name`, `type`, `payment_type`, `dept_head_appvl`, `fin_head_appvl`, `date_received`) VALUES
(5, 11, 1323834604, 1323730800, 'la nueva', 'stock', 'cash', 'approved', 'rejected', 0),
(6, 11, 1323834838, 1323730800, 'la nueva', 'stock', 'cash', 'cancelled', 'approved', 0),
(7, 11, 1323840386, 1323817200, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(8, 13, 1324541726, 1324508400, 'la nueva', 'stock', 'cash', 'cancelled', 'cancelled', 0),
(9, 12, 1325740667, 1325718000, 'la nueva', 'stock', 'check', 'cancelled', 'cancelled', 0),
(10, 12, 1325740816, 1326150000, 'macro', 'stock', 'credit card', 'cancelled', 'cancelled', 0),
(11, 12, 1325741535, 1326236400, 'macro', 'stock', 'cash', 'approved', 'approved', 0),
(12, 13, 1330398286, 1330470000, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(13, 20, 1331513989, 1331679600, 'la nueva', 'stock', 'cash', 'pending', 'pending', 0),
(14, 20, 1331703553, 1331593200, 'la nueva', 'stock', 'cash', 'pending', 'pending', 0),
(15, 20, 1331719031, 1332198000, 'macro', 'stock', 'cash', 'pending', 'pending', 0),
(16, 20, 1331719031, 1332198000, 'macro', 'stock', 'cash', 'cancelled', 'cancelled', 0),
(17, 10, 1333086484, 1330902000, 'la nueva', 'asset', 'check', 'approved', 'approved', 0),
(18, 20, 1333093888, 1331074800, 'la nueva', 'asset', 'check', 'cancelled', 'cancelled', 0),
(19, 12, 1333343774, 1333404000, 'macro', 'stock', 'others', 'approved', 'pending', 0),
(20, 20, 1334021143, 1334095200, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(21, 20, 1335855000, 1335909600, 'la nueva', 'stock', 'check', 'pending', 'pending', 0),
(22, 13, 1339460924, 1338760800, 'macro', 'stock', 'credit card', 'cancelled', 'cancelled', 0),
(23, 23, 1339666706, 1338933600, 'la nueva', 'asset', 'cash', 'approved', 'approved', 0),
(24, 23, 1339666706, 1338933600, 'la nueva', 'asset', 'cash', 'pending', 'pending', 0),
(25, 23, 1340159803, 1340056800, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(26, 23, 1340161042, 1338847200, 'nutech marketing', 'stock', 'cash', 'approved', 'approved', 0),
(27, 17, 1340268581, 1340229600, 'macro', 'stock', 'credit card', 'cancelled', 'cancelled', 0),
(28, 17, 1340335751, 1339538400, 'nutech marketing', 'stock', 'check', 'cancelled', 'cancelled', 0),
(29, 17, 1340335751, 1339538400, 'nutech marketing', 'stock', 'check', 'pending', 'pending', 0),
(30, 23, 1340755427, 1341007200, 'macro', 'stock', 'cash', 'pending', 'pending', 0),
(31, 25, 1341565326, 1341871200, 'la nueva', 'stock', 'cash', 'pending', 'pending', 0),
(32, 25, 1341799005, 1341957600, 'macro', 'asset', 'check', 'pending', 'pending', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prs_details`
--

CREATE TABLE IF NOT EXISTS `prs_details` (
  `prs_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `prs_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` float(11,2) NOT NULL DEFAULT '0.00',
  `units` varchar(50) NOT NULL COMMENT 'unit of measure',
  `unit_price` float(15,2) NOT NULL DEFAULT '0.00',
  `amount` float(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`prs_details_id`),
  KEY `prs_id` (`prs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `prs_details`
--

INSERT INTO `prs_details` (`prs_details_id`, `prs_id`, `description`, `quantity`, `units`, `unit_price`, `amount`) VALUES
(8, 12, 'Manok Palabok', 10.00, 'liters', 10.00, 100.00),
(9, 13, 'Chair', 1.00, 'each', 2.50, 2.50),
(10, 14, 'friend chicken', 5.00, 'each', 100.00, 500.00),
(11, 15, 'Enter the product namfdase or description', 13.00, 'liters', 30.00, 390.00),
(12, 16, 'sdfEnter the product name or description', 13.00, 'kilo', 30.00, 390.00),
(13, 17, 'try', 1.00, 'length', 20.00, 20.00),
(14, 18, 'Item1', 2.00, 'each', 20.00, 40.00),
(15, 19, 'Black Wire', 5.00, 'each', 170.52, 852.60),
(16, 20, 'Meat', 2.00, 'kilo', 200.00, 400.00),
(18, 21, 'Lechon Manok', 1.00, 'each', 150.00, 150.00),
(19, 22, 'Fitted', 5.00, 'each', 28.00, 140.00),
(20, 23, 'Luto', 5.00, 'meters', 40.00, 200.02),
(21, 24, 'Grails Meat', 8.00, 'bottle', 700.00, 5600.00),
(22, 25, 'Kitkat Chocolate', 1.00, 'bottle', 50.00, 50.00),
(23, 26, 'Marlboro Menthol', 3.00, 'pack', 300.00, 900.00),
(24, 27, 'try', 1.00, 'liters', 22.00, 22.00),
(25, 28, 'ASD', 1.00, 'length', 22.00, 22.00),
(26, 29, 'SDF', 2.00, 'liters', 2.00, 4.00),
(27, 30, 'Choey Choco', 1.00, 'pack', 40.00, 40.00),
(28, 31, 'This is for kitchen', 1.00, 'each', 25.00, 25.00),
(29, 32, 'Wires', 1.00, 'kilo', 40.00, 40.00);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_number_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `room_status_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `room_number_id` (`room_number_id`),
  KEY `room_type_id` (`room_type_id`),
  KEY `added_by` (`added_by`),
  KEY `room_status_id` (`room_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `room_number_id`, `room_type_id`, `room_status_id`, `status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 1, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 17:50:36', 7),
(2, 2, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 17:50:36', 7),
(3, 3, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:03:19', 7),
(4, 4, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:03:44', 7),
(5, 5, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:04:34', 7),
(6, 6, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:04:40', 7),
(7, 7, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:04:45', 7),
(8, 8, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:04:49', 7),
(9, 9, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:05:02', 7),
(10, 10, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:05:04', 7),
(11, 11, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:05:11', 7),
(12, 12, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:05:13', 7),
(13, 13, 2, 3, 'active', '0000-00-00 00:00:00', '2012-02-08 14:52:20', 7),
(14, 14, 2, 5, 'active', '0000-00-00 00:00:00', '2012-03-19 13:29:13', 7),
(15, 15, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:07:28', 7),
(16, 16, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:07:39', 7),
(17, 17, 2, 3, 'active', '0000-00-00 00:00:00', '2012-08-02 20:55:52', 7),
(18, 18, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:08:24', 7),
(19, 19, 2, 3, 'active', '0000-00-00 00:00:00', '2012-04-13 11:52:04', 7),
(20, 20, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:08:37', 7),
(21, 21, 3, 3, 'active', '0000-00-00 00:00:00', '2012-02-27 11:32:23', 7),
(22, 22, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:09:08', 7),
(23, 23, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:09:10', 7),
(24, 24, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:09:30', 7),
(25, 25, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:09:43', 7),
(26, 26, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:09:45', 7),
(27, 27, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:09:59', 7),
(28, 28, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:10:39', 7),
(29, 29, 2, 2, 'active', '0000-00-00 00:00:00', '2012-02-14 15:53:11', 7),
(30, 30, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 18:12:00', 7),
(31, 31, 2, 5, 'active', '0000-00-00 00:00:00', '2012-03-16 10:43:12', 7),
(32, 32, 2, 5, 'active', '0000-00-00 00:00:00', '2012-03-15 17:55:45', 7),
(33, 33, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 18:12:00', 7),
(34, 34, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:10:54', 7),
(35, 35, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:11:19', 7),
(36, 36, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:11:30', 7),
(37, 37, 1, 3, 'active', '0000-00-00 00:00:00', '2012-08-02 20:25:23', 7),
(38, 38, 1, 5, 'active', '0000-00-00 00:00:00', '2012-02-28 15:09:56', 7),
(39, 39, 1, 3, 'active', '0000-00-00 00:00:00', '2012-08-02 19:39:56', 7),
(40, 40, 1, 3, 'active', '0000-00-00 00:00:00', '2012-08-01 09:22:12', 7),
(41, 41, 1, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 18:12:00', 7),
(42, 42, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:12:58', 7),
(43, 43, 2, 5, 'active', '0000-00-00 00:00:00', '2012-02-08 15:02:53', 7),
(44, 44, 2, 3, 'active', '0000-00-00 00:00:00', '2012-02-10 13:55:07', 7),
(45, 45, 2, 3, 'active', '0000-00-00 00:00:00', '2012-08-03 08:55:45', 7),
(46, 46, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:13:07', 7),
(47, 47, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:13:09', 7),
(48, 48, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:13:12', 7),
(49, 49, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:13:14', 7),
(50, 50, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:13:58', 7),
(51, 51, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:00', 7),
(52, 52, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:02', 7),
(53, 53, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:04', 7),
(54, 54, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:07', 7),
(55, 55, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:09', 7),
(56, 56, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:12', 7),
(57, 57, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:14', 7),
(58, 58, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:16', 7),
(59, 59, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:19', 7),
(60, 60, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:22', 7),
(61, 61, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:14:25', 7),
(62, 62, 4, 3, 'active', '0000-00-00 00:00:00', '2012-06-12 10:08:32', 7),
(63, 63, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 18:12:00', 7),
(64, 64, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:07', 7),
(65, 65, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:10', 7),
(66, 66, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:12', 7),
(67, 67, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:14', 7),
(68, 68, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:16', 7),
(69, 69, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:19', 7),
(70, 70, 2, 2, 'active', '0000-00-00 00:00:00', '2012-02-14 15:59:00', 7),
(71, 71, 1, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:39', 7),
(72, 72, 1, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:15:42', 7),
(73, 73, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:01', 7),
(74, 74, 1, 5, 'active', '0000-00-00 00:00:00', '2012-02-08 18:10:59', 7),
(75, 75, 1, 3, 'active', '0000-00-00 00:00:00', '2012-02-23 18:36:46', 7),
(76, 76, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:36', 7),
(77, 77, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:38', 7),
(78, 78, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:41', 7),
(79, 79, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:43', 7),
(80, 80, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:46', 7),
(81, 81, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 11:16:49', 7),
(85, 82, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 13:40:46', 12),
(86, 84, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 13:47:01', 12),
(88, 85, 2, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 13:49:35', 12),
(89, 86, 3, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 13:52:25', 12),
(90, 87, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 13:55:35', 12),
(91, 88, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 13:56:16', 12),
(95, 86, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 14:06:13', 12),
(96, 87, 2, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 14:07:20', 12),
(97, 90, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 14:07:33', 12),
(127, 95, 1, 3, 'active', '2012-07-12 12:37:21', '2012-08-03 10:40:09', 12),
(128, 81, 1, 1, 'active', '2012-07-13 03:32:27', '2012-07-13 09:32:27', 12),
(129, 96, 1, 5, 'active', '2012-07-13 05:51:48', '2012-07-31 16:25:32', 12),
(130, 98, 1, 3, 'active', '2012-07-20 08:02:43', '2012-08-02 18:30:44', 15),
(133, 95, 5, 3, 'active', '2012-08-03 03:51:02', '2012-08-03 10:40:09', 15);

-- --------------------------------------------------------

--
-- Table structure for table `rooms_1`
--

CREATE TABLE IF NOT EXISTS `rooms_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `type` int(2) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `comment` text NOT NULL,
  `date_updated` int(11) NOT NULL DEFAULT '0' COMMENT 'this date here is auto generated by the php code please refer to the room_tags date table thanks',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `type` (`type`),
  KEY `number` (`number`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `rooms_1`
--

INSERT INTO `rooms_1` (`id`, `number`, `type`, `status`, `comment`, `date_updated`) VALUES
(1, 224, 3, 1, '', 0),
(2, 225, 1, 1, '', 0),
(3, 226, 1, 1, 'test ra ni nga comment oie, ayaw pa ilad', 0),
(4, 227, 1, 1, '', 0),
(5, 228, 1, 1, '', 0),
(6, 229, 1, 1, '', 0),
(7, 324, 1, 1, '', 0),
(8, 325, 1, 1, '', 0),
(9, 327, 1, 1, '', 0),
(10, 328, 1, 1, '', 0),
(11, 201, 2, 1, '', 0),
(12, 202, 2, 1, '', 0),
(13, 205, 2, 1, '', 0),
(14, 206, 2, 1, '', 0),
(15, 207, 2, 1, '', 0),
(16, 208, 2, 1, '', 0),
(17, 216, 2, 1, '', 0),
(18, 217, 2, 1, '', 0),
(19, 218, 2, 1, '', 0),
(20, 219, 2, 1, '', 0),
(21, 220, 2, 1, '', 0),
(22, 221, 2, 1, '', 0),
(23, 222, 2, 1, '', 0),
(24, 230, 2, 1, '', 0),
(25, 231, 2, 1, '', 0),
(26, 232, 2, 1, '', 0),
(27, 233, 2, 1, '', 0),
(28, 234, 2, 1, '', 0),
(29, 235, 2, 1, '', 0),
(30, 301, 2, 1, '', 0),
(31, 302, 2, 1, '', 0),
(32, 316, 2, 1, '', 0),
(33, 317, 2, 1, '', 0),
(34, 318, 2, 1, '', 0),
(35, 319, 2, 1, '', 0),
(36, 320, 2, 1, '', 0),
(37, 321, 2, 1, '', 0),
(38, 322, 2, 1, '', 0),
(39, 323, 2, 1, '', 0),
(40, 329, 2, 1, '', 0),
(41, 330, 2, 1, '', 0),
(42, 331, 2, 1, '', 0),
(43, 332, 2, 1, '', 0),
(44, 333, 2, 1, '', 0),
(45, 334, 2, 1, '', 0),
(46, 203, 3, 1, '', 0),
(47, 209, 3, 1, '', 0),
(48, 210, 3, 1, '', 0),
(49, 211, 3, 1, '', 0),
(50, 213, 3, 1, '', 0),
(51, 214, 3, 1, '', 0),
(52, 303, 3, 1, '', 0),
(53, 304, 3, 1, '', 0),
(54, 305, 3, 1, '', 0),
(55, 306, 3, 1, '', 0),
(56, 307, 3, 1, '', 0),
(57, 308, 3, 1, '', 0),
(58, 309, 3, 1, '', 0),
(59, 310, 3, 1, '', 0),
(60, 311, 3, 1, '', 0),
(61, 312, 3, 1, '', 0),
(62, 313, 3, 1, '', 0),
(63, 314, 3, 1, '', 0),
(64, 101, 4, 1, '', 0),
(65, 102, 4, 1, '', 0),
(66, 103, 4, 1, '', 0),
(67, 104, 4, 1, '', 0),
(68, 105, 4, 1, '', 0),
(69, 106, 4, 1, '', 0),
(70, 107, 4, 1, '', 0),
(71, 108, 4, 1, '', 0),
(72, 109, 4, 1, '', 0),
(73, 110, 4, 1, '', 0),
(74, 111, 4, 1, '', 0),
(75, 112, 4, 1, '', 0),
(76, 204, 4, 1, '', 0),
(77, 212, 4, 1, '', 0),
(78, 215, 4, 1, '', 0),
(79, 223, 4, 1, '', 0),
(80, 315, 4, 1, '', 0),
(81, 326, 4, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rooms_accom`
--

CREATE TABLE IF NOT EXISTS `rooms_accom` (
  `room_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `accom_type` enum('guest','student') NOT NULL,
  `accom_type_id` int(11) NOT NULL COMMENT 'the id of the guest or student from the guest_accom table or student_accom table',
  `room_number_id` int(11) DEFAULT NULL,
  `accom_status` enum('checkin','checkout','reserve') NOT NULL,
  `rooms_accom_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_accom_id`),
  KEY `room_number_id` (`room_number_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `rooms_accom`
--

INSERT INTO `rooms_accom` (`room_accom_id`, `accom_type`, `accom_type_id`, `room_number_id`, `accom_status`, `rooms_accom_status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 'guest', 1, 37, 'checkin', 'active', '2012-02-06 03:29:01', '2012-08-02 19:03:56', 12),
(2, 'student', 1, 38, 'checkout', 'active', '2012-02-06 03:32:15', '2012-02-14 15:58:14', 12),
(3, 'guest', 2, 38, 'reserve', 'active', '2012-02-07 09:53:37', '2012-02-07 16:53:37', 12),
(4, 'guest', 3, 38, 'checkin', 'active', '2012-02-07 09:54:17', '2012-02-24 14:33:33', 12),
(5, 'guest', 4, 13, 'checkin', 'active', '2012-02-08 07:52:20', '2012-02-23 16:05:10', 12),
(6, 'guest', 5, 37, 'checkin', 'active', '2012-02-08 07:55:14', '2012-08-02 19:40:43', 12),
(7, 'guest', 6, 70, 'checkout', 'active', '2012-02-08 08:01:52', '2012-02-14 15:58:59', 12),
(8, 'guest', 7, 43, 'reserve', 'active', '2012-02-08 08:02:53', '2012-02-08 15:02:53', 12),
(9, 'guest', 8, 44, 'checkout', 'active', '2012-02-08 08:05:32', '2012-02-17 09:48:40', 12),
(10, 'guest', 9, 44, 'checkin', 'active', '2012-02-08 08:06:10', '2012-02-08 15:06:10', 12),
(11, 'guest', 10, 75, 'checkin', 'active', '2012-02-08 08:08:25', '2012-02-23 18:36:46', 12),
(12, 'guest', 11, 29, 'checkout', 'active', '2012-02-08 08:10:13', '2012-02-14 15:38:46', 12),
(13, 'guest', 12, 31, 'reserve', 'active', '2012-02-08 08:20:40', '2012-02-08 16:34:17', 12),
(14, 'guest', 13, 40, 'checkin', 'active', '2012-02-08 09:50:04', '2012-08-01 09:22:12', 12),
(15, 'student', 2, 44, 'checkin', 'active', '2012-02-08 09:58:38', '2012-02-10 14:29:45', 12),
(16, 'student', 3, 74, 'reserve', 'active', '2012-02-08 10:53:10', '2012-02-08 18:10:59', 12),
(17, 'guest', 14, 75, 'checkin', 'active', '2012-02-14 08:49:24', '2012-04-19 18:41:01', 12),
(18, 'student', 4, 29, 'checkout', 'active', '2012-02-14 08:53:01', '2012-02-14 15:53:11', 12),
(19, 'student', 5, 13, 'checkin', 'active', '2012-02-14 08:53:28', '2012-02-23 16:18:49', 12),
(20, 'guest', 15, 39, 'reserve', 'active', '2012-02-23 10:28:34', '2012-02-24 08:58:53', 12),
(21, 'guest', 16, 37, 'checkin', 'active', '2012-02-27 02:52:47', '2012-08-01 15:14:43', 12),
(22, 'guest', 17, 21, 'checkin', 'active', '2012-02-27 04:32:23', '2012-02-27 11:32:23', 12),
(23, 'guest', 18, 37, 'checkin', 'active', '2012-02-28 07:12:11', '2012-06-14 15:53:33', 12),
(24, 'guest', 19, 38, 'reserve', 'active', '2012-02-28 08:09:56', '2012-02-28 15:09:56', 12),
(25, 'guest', 20, 14, 'reserve', 'active', '2012-02-29 02:34:38', '2012-03-19 13:29:13', 12),
(26, 'guest', 21, 62, 'checkin', 'active', '2012-03-07 02:29:48', '2012-06-12 10:08:32', 12),
(27, 'guest', 22, 37, 'checkin', 'active', '2012-03-12 08:01:07', '2012-03-12 15:01:07', 12),
(28, 'guest', 23, 37, 'reserve', 'active', '2012-03-12 08:03:06', '2012-05-09 11:37:44', 12),
(29, 'student', 6, 31, 'reserve', 'active', '2012-03-12 09:31:33', '2012-05-22 09:29:09', 12),
(30, 'guest', 24, 37, 'checkin', 'active', '2012-03-15 10:52:51', '2012-08-02 09:21:27', 12),
(31, 'guest', 25, 37, 'checkin', 'active', '2012-03-15 10:53:16', '2012-03-15 17:53:16', 12),
(32, 'guest', 26, 37, 'reserve', 'active', '2012-03-15 10:53:37', '2012-05-02 13:48:36', 12),
(33, 'student', 7, 32, 'reserve', 'active', '2012-03-15 10:55:45', '2012-03-15 17:55:45', 12),
(34, 'guest', 27, 39, 'checkin', 'active', '2012-03-16 02:40:12', '2012-03-16 09:40:12', 20),
(35, 'guest', 28, 19, 'checkin', 'active', '2012-04-10 07:52:51', '2012-04-13 11:52:04', 12),
(36, 'student', 8, 37, 'checkout', 'active', '2012-05-02 07:46:36', '2012-07-31 16:49:30', 12),
(37, 'guest', 29, 91, 'checkin', 'active', '2012-07-12 10:34:13', '2012-07-12 18:11:26', 12),
(38, 'student', 9, 21, 'checkout', 'active', '2012-07-17 08:01:36', '2012-08-03 10:02:09', 12),
(39, 'student', 10, 95, 'checkin', 'active', '2012-07-18 05:18:15', '2012-08-03 10:40:09', 12),
(40, 'guest', 30, 96, 'reserve', 'active', '2012-07-31 10:25:32', '2012-07-31 16:25:32', 15),
(41, 'guest', 31, 98, 'checkin', 'active', '2012-08-02 12:30:44', '2012-08-02 18:30:44', 15),
(42, 'guest', 32, 17, 'checkin', 'active', '2012-08-02 02:55:52', '2012-08-02 20:55:52', 15),
(43, 'guest', 33, 45, 'checkin', 'active', '2012-08-03 02:55:45', '2012-08-03 08:55:45', 15);

-- --------------------------------------------------------

--
-- Table structure for table `room_number_ids`
--

CREATE TABLE IF NOT EXISTS `room_number_ids` (
  `room_number_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(5) NOT NULL,
  `room_no_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_number_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Dumping data for table `room_number_ids`
--

INSERT INTO `room_number_ids` (`room_number_id`, `room_no`, `room_no_status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 101, 'active', '2012-07-12 23:00:00', '2012-07-11 17:05:48', 7),
(2, 102, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(3, 103, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(4, 104, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(5, 105, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(6, 106, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(7, 107, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(8, 108, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(9, 109, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(10, 110, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(11, 111, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(12, 112, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(13, 201, 'active', '0000-00-00 00:00:00', '2012-02-03 10:51:10', 7),
(14, 202, 'active', '0000-00-00 00:00:00', '2012-02-03 10:52:37', 7),
(15, 203, 'active', '0000-00-00 00:00:00', '2012-02-03 10:52:39', 7),
(16, 204, 'active', '0000-00-00 00:00:00', '2012-02-03 10:52:41', 7),
(17, 205, 'active', '0000-00-00 00:00:00', '2012-02-03 10:52:55', 7),
(18, 206, 'active', '0000-00-00 00:00:00', '2012-02-03 10:52:57', 7),
(19, 207, 'active', '0000-00-00 00:00:00', '2012-02-03 10:52:59', 7),
(20, 208, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:00', 7),
(21, 209, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:02', 7),
(22, 210, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:04', 7),
(23, 211, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:06', 7),
(24, 212, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:08', 7),
(25, 213, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:10', 7),
(26, 214, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:11', 7),
(27, 215, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:13', 7),
(28, 216, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:14', 7),
(29, 217, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:16', 7),
(30, 218, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:17', 7),
(31, 219, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:19', 7),
(32, 220, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:21', 7),
(33, 221, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:26', 7),
(34, 222, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:27', 7),
(35, 223, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:32', 7),
(36, 224, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:33', 7),
(37, 225, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:37', 7),
(38, 226, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:40', 7),
(39, 227, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:41', 7),
(40, 228, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:43', 7),
(41, 229, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:45', 7),
(42, 230, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:48', 7),
(43, 231, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:53', 7),
(44, 232, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:56', 7),
(45, 233, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:58', 7),
(46, 234, 'active', '0000-00-00 00:00:00', '2012-02-03 10:53:59', 7),
(47, 235, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:03', 7),
(48, 301, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:09', 7),
(49, 302, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:10', 7),
(50, 303, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:21', 7),
(51, 304, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:22', 7),
(52, 305, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:24', 7),
(53, 306, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:26', 7),
(54, 307, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:28', 7),
(55, 308, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:30', 7),
(56, 309, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:32', 7),
(57, 310, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:37', 7),
(58, 311, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:39', 7),
(59, 312, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:40', 7),
(60, 313, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:41', 7),
(61, 314, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:46', 7),
(62, 315, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:48', 7),
(63, 316, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:49', 7),
(64, 317, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:51', 7),
(65, 318, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:52', 7),
(66, 319, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:54', 7),
(67, 320, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:56', 7),
(68, 321, 'active', '0000-00-00 00:00:00', '2012-02-03 10:54:58', 7),
(69, 322, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:00', 7),
(70, 323, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:01', 7),
(71, 324, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:07', 7),
(72, 325, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:08', 7),
(73, 326, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:09', 7),
(74, 327, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:12', 7),
(75, 328, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:13', 7),
(76, 329, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:15', 7),
(77, 330, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:18', 7),
(78, 331, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:24', 7),
(79, 332, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:26', 7),
(80, 333, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:29', 7),
(81, 334, 'active', '0000-00-00 00:00:00', '2012-02-03 10:55:32', 7),
(82, 335, 'active', '0000-00-00 00:00:00', '2012-07-12 13:12:15', 12),
(83, 336, 'active', '0000-00-00 00:00:00', '2012-07-12 13:12:49', 12),
(84, 337, 'active', '0000-00-00 00:00:00', '2012-07-12 13:19:31', 12),
(85, 338, 'active', '2012-07-12 07:26:36', '2012-07-12 13:26:36', 12),
(86, 339, 'active', '2012-07-12 07:36:23', '2012-07-12 13:36:23', 12),
(87, 339, 'active', '2012-07-12 07:36:25', '2012-07-12 13:36:25', 12),
(88, 340, 'active', '2012-07-12 07:38:21', '2012-07-12 14:12:08', 12),
(89, 341, 'active', '2012-07-12 08:01:56', '2012-07-12 14:12:16', 12),
(90, 342, 'active', '2012-07-12 08:03:02', '2012-07-12 14:12:27', 12),
(91, 91, 'active', '2012-07-12 08:03:33', '2012-07-12 14:35:40', 12),
(92, 92, 'active', '2012-07-12 08:04:23', '2012-07-12 14:35:47', 12),
(93, 93, 'active', '2012-07-12 08:05:00', '2012-07-12 14:35:54', 12),
(94, 94, 'active', '2012-07-12 08:14:44', '2012-07-12 14:36:02', 12),
(95, 95, 'active', '2012-07-12 08:14:52', '2012-07-12 14:36:10', 12),
(96, 96, 'active', '0000-00-00 00:00:00', '2012-07-12 14:52:29', 12),
(97, 97, 'active', '0000-00-00 00:00:00', '2012-07-12 14:52:30', 12),
(98, 98, 'active', '0000-00-00 00:00:00', '2012-07-12 15:06:35', 12),
(99, 99, 'active', '0000-00-00 00:00:00', '2012-07-12 14:52:30', 12),
(100, 100, 'active', '0000-00-00 00:00:00', '2012-07-12 14:52:30', 12);

-- --------------------------------------------------------

--
-- Table structure for table `room_rates`
--

CREATE TABLE IF NOT EXISTS `room_rates` (
  `room_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_rate` float(11,2) DEFAULT NULL,
  `room_rate_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_rate_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `room_rates`
--

INSERT INTO `room_rates` (`room_rate_id`, `room_rate`, `room_rate_status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 0.00, 'active', '0000-00-00 00:00:00', '2012-02-03 10:42:19', 7);

-- --------------------------------------------------------

--
-- Table structure for table `room_status`
--

CREATE TABLE IF NOT EXISTS `room_status` (
  `room_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_status_name` varchar(65) DEFAULT NULL,
  `room_status_css` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`room_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `room_status`
--

INSERT INTO `room_status` (`room_status_id`, `room_status_name`, `room_status_css`) VALUES
(1, 'Vacant - Clean', 'hr-vc'),
(2, 'Vacant - Dirty', 'hr-vd'),
(3, 'Occupied', 'hr-oc'),
(4, 'Occupied with vacant bed', 'hr-ovb'),
(5, 'Reserved', 'hr-wr'),
(6, 'Under Maintenance', 'hr-um'),
(7, 'Corporate', 'hr-c');

-- --------------------------------------------------------

--
-- Table structure for table `room_status_1`
--

CREATE TABLE IF NOT EXISTS `room_status_1` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `css` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `room_status_1`
--

INSERT INTO `room_status_1` (`id`, `name`, `css`) VALUES
(1, 'Vacant - Clean', 'hr-vc'),
(2, 'Vacant - Dirty', 'hr-vd'),
(3, 'Occupied', 'hr-oc'),
(4, 'Occupied with vacant bed', 'hr-ovb'),
(5, 'Reserved', 'hr-wr'),
(6, 'Under Maintenance', 'hr-um'),
(7, 'Corporate', '');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `room_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_name` varchar(35) DEFAULT NULL,
  `room_rate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_type_id`),
  KEY `room_rate_id` (`room_rate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`room_type_id`, `room_type_name`, `room_rate_id`) VALUES
(1, 'Single', 1),
(2, 'Double', 1),
(3, 'Triple', 1),
(4, 'Quad', 1),
(5, 'Corporate', 1);

-- --------------------------------------------------------

--
-- Table structure for table `room_type_1`
--

CREATE TABLE IF NOT EXISTS `room_type_1` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `rate` float(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `room_type_1`
--

INSERT INTO `room_type_1` (`id`, `name`, `rate`) VALUES
(1, 'single', 0.00),
(2, 'double', 0.00),
(3, 'quad', 0.00),
(4, 'corporate', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `srs`
--

CREATE TABLE IF NOT EXISTS `srs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL COMMENT 'employee id',
  `head_appr` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending' COMMENT '0 or date, 0 means not approved and if approved the value will be the date in unixtime',
  `srs_released` int(11) NOT NULL DEFAULT '0' COMMENT '0 or date, released from the stock',
  `received` int(11) NOT NULL DEFAULT '0' COMMENT '0 or date',
  `purpose` varchar(255) NOT NULL,
  `date_required` int(11) NOT NULL DEFAULT '0',
  `date_requested` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_id` (`stock_id`),
  KEY `requested_by` (`requested_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `srs`
--

INSERT INTO `srs` (`id`, `stock_id`, `qty`, `requested_by`, `head_appr`, `srs_released`, `received`, `purpose`, `date_required`, `date_requested`) VALUES
(5, 1, 21, 11, 'approved', 0, 0, 'sf', 1323730800, 1323745042),
(6, 1, 1, 11, 'approved', 0, 0, 'asdf', 1323730800, 1323745230),
(7, 2, 1, 11, 'approved', 0, 0, 'sdaf', 1323730800, 1323745295),
(8, 1, 1, 11, 'pending', 0, 0, 'saf', 1323730800, 1323745328),
(9, 1, 1, 11, 'pending', 0, 0, 'as', 1323730800, 1323745406),
(10, 2, 1, 11, 'pending', 0, 0, 'as', 1323730800, 1323745406),
(11, 1, 1, 11, 'pending', 0, 0, 'rteasfsdf', 1323644400, 1323747222),
(12, 2, 1, 11, 'pending', 0, 0, 'rteasfsdf', 1323644400, 1323747222),
(13, 1, 1, 11, 'cancelled', 0, 0, 'dg', 1323817200, 1323856913),
(15, 1, 1, 11, 'pending', 0, 0, 'asdf', 1323212400, 1323857073),
(16, 1, 1, 11, 'pending', 0, 0, 'asdf', 1324249200, 1324281716),
(17, 2, 1, 11, 'pending', 0, 0, 'asdf', 1324249200, 1324281716),
(18, 1, 1, 13, 'pending', 0, 0, 'aasdf', 1324508400, 1324541701),
(19, 1, 1, 12, 'cancelled', 0, 0, 'asdf', 1326322800, 1325740932),
(20, 2, 1, 12, 'cancelled', 0, 0, 'asdf', 1326322800, 1325740932),
(23, 1, 1, 12, 'cancelled', 0, 0, 'For snack', 1330124400, 1330047666),
(25, 2, 4, 20, 'cancelled', 0, 0, 'Hi this is for yougart', 1331074800, 1331513880),
(28, 1, 1, 20, 'cancelled', 0, 0, 'for dugang sa pundo', 1332370800, 1331773623),
(29, 2, 1, 20, 'cancelled', 0, 0, 'for dugang sa pundo', 1332370800, 1331773623),
(31, 1, 1, 20, 'cancelled', 0, 0, 'jhkjkhh', 1331074800, 1333013480),
(32, 2, 1, 20, 'cancelled', 0, 0, 'jhkjkhh', 1331074800, 1333013480),
(35, 1, 1, 20, 'pending', 0, 0, 'xcxvcv', 1331074800, 1333071734),
(36, 2, 1, 20, 'cancelled', 0, 0, 'xcxvcv', 1331074800, 1333071734),
(37, 1, 1, 13, 'pending', 0, 0, '1', 1337119200, 1338458927),
(38, 1, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(39, 2, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(40, 9, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(41, 10, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(42, 11, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(43, 25, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(44, 26, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(45, 27, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(46, 30, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(47, 33, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(48, 34, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608611),
(49, 35, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608611),
(50, 36, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608611),
(51, 1, 1, 17, 'pending', 0, 0, 'try', 1340920800, 1340774283),
(52, 2, 1, 17, 'pending', 0, 0, 'try', 1340920800, 1340774283),
(53, 9, 1, 17, 'pending', 0, 0, 'try', 1340920800, 1340774283),
(54, 9, 1, 25, 'pending', 0, 0, 'for hotel', 1341871200, 1341565286),
(55, 36, 1, 25, 'pending', 0, 0, 'for housekeeping', 1341871200, 1341799053);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE IF NOT EXISTS `stocks` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_type` int(11) NOT NULL DEFAULT '0' COMMENT 'links to stock designation table',
  `meal_type_id` int(11) NOT NULL,
  `units` int(11) NOT NULL COMMENT 'reference to the unitsof_measure table',
  `name` varchar(45) NOT NULL,
  `description` varchar(120) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `qty_inhand` int(11) NOT NULL,
  `reorder_point` int(11) NOT NULL DEFAULT '0',
  `aqui_price` float(15,2) NOT NULL COMMENT 'aquisition price',
  `supplier` int(11) NOT NULL COMMENT 'reference to the suppliers table',
  `stock_image` varchar(255) NOT NULL COMMENT 'the image of the stock',
  `added_by` int(11) NOT NULL COMMENT 'if it is requested by the staff then the staff''s emp_id will be added',
  `date_added` int(11) NOT NULL COMMENT 'the date of stock added',
  PRIMARY KEY (`stock_id`),
  KEY `units` (`units`),
  KEY `supplier` (`supplier`),
  KEY `added_by` (`added_by`),
  KEY `stock_type` (`stock_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`stock_id`, `stock_type`, `meal_type_id`, `units`, `name`, `description`, `brand`, `qty_inhand`, `reorder_point`, `aqui_price`, `supplier`, `stock_image`, `added_by`, `date_added`) VALUES
(1, 2, 1, 2, 'breakfast_food', 'snack namo', 'jollibee', 50, 100, 99.00, 1232, 'default image here', 1, 1318846362),
(2, 1, 2, 1, 'jhubdie', 'Meat', 'brand 2', 20, 7, 3.50, 12, 'default image here', 1, 1318914295),
(9, 2, 0, 1, 'Notebook', 'this is for hotel', 'x', 25, 2, 5.00, 7, 'default image here', 1, 1334647360),
(10, 1, 0, 1, 'Meat', 'this is for kitchen', 'y', 23, 50, 35.00, 3, 'default image here', 1, 1334652286),
(11, 2, 0, 1, 'Ballpen', 'office supplies', 'Pilot', 30, 100, 56.00, 3, 'default image here', 1, 1334656492),
(25, 1, 0, 1, 'fbitem', 'ds', 'x', 57, 100, 21.00, 3, 'default image here', 1, 1335174033),
(26, 1, 0, 1, 'sd', 'Fried Chicken', 'sds', 40, 2, 2.00, 4, 'default image here', 1, 1335174064),
(27, 1, 0, 1, 'Onion', 'Onion', 'jy', 70, 122, 2.00, 3, 'default image here', 1, 1336030614),
(30, 1, 0, 1, 'tisue', 'tissue paper', 'nike', 20, 11, 6.00, 3, 'default image here', 1, 1336036666),
(33, 14, 0, 5, 'Bond Aid', 'Bond Aid', 'ad', 90, 2, 9.00, 3, 'default image here', 1, 1336122313),
(34, 3, 0, 7, 'Nescafe', 'Nescafe 3in1', 'coffemate', 80, 30, 6.00, 3, 'default image here', 1, 1336371347),
(35, 3, 0, 7, 'Rebisco', 'Rebisco Biscuits', 'rebisco', 15, 41, 6.00, 3, 'default image here', 1, 1336371779),
(36, 14, 0, 5, 'Thread', 'Sinulid', 'hope', 89, 41, 6.00, 8, 'default image here', 1, 1336373132);

-- --------------------------------------------------------

--
-- Table structure for table `stock_items`
--

CREATE TABLE IF NOT EXISTS `stock_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `unitsof_measure` int(11) NOT NULL,
  `account_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_code` (`account_code`),
  KEY `unitsof_measure` (`unitsof_measure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stock_items`
--

INSERT INTO `stock_items` (`id`, `name`, `description`, `price`, `unitsof_measure`, `account_code`) VALUES
(1, 'Eggs', 'An egg from the eggs', 5.00, 1, 1020305);

-- --------------------------------------------------------

--
-- Table structure for table `stock_type`
--

CREATE TABLE IF NOT EXISTS `stock_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `stock_type`
--

INSERT INTO `stock_type` (`id`, `name`) VALUES
(1, 'F and B'),
(2, 'Office Supplies'),
(3, 'Larry''s Place'),
(14, 'Hotel Supplies');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_no` varchar(25) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `english_name` varchar(25) NOT NULL,
  `birth_date` int(11) NOT NULL DEFAULT '0',
  `country` varchar(25) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `civil_status` enum('single','married','widowed') NOT NULL,
  `nationality` varchar(25) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_no` varchar(25) NOT NULL,
  `passport_no` varchar(35) NOT NULL,
  `visa_expiry` int(11) NOT NULL DEFAULT '0' COMMENT 'date in unix time',
  `accomodation` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 or 1 of the need accomodation',
  `date_reg` int(11) NOT NULL DEFAULT '0' COMMENT 'date registered or added',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_no`, `firstname`, `lastname`, `english_name`, `birth_date`, `country`, `gender`, `civil_status`, `nationality`, `address`, `email`, `contact_no`, `passport_no`, `visa_expiry`, `accomodation`, `date_reg`) VALUES
(1, 'AKL-002134', 'Kiera', 'Low', 'Kier', 1337810400, 'Korea', 'male', 'single', 'Korean', 'North East, South Korea', 'kiera@localhost.com', '12345678', '23SFD-GBG-23', 0, 0, 0),
(2, 'AKL-001111', 'amadeo', 'pelaez', 'johnny depp', 0, 'russia', 'male', 'single', 'russian', '', '', '', '', 0, 0, 0),
(3, 'AKL-002222', 'andro', 'misa', 'john doe', 0, 'south korea', 'male', 'single', 'korean', '', '', '', '', 0, 0, 0),
(4, 'AKL-003333', 'britta', 'oblan', 'alicia', 0, 'japan', 'female', 'married', 'japanese', '', '', '', '', 0, 0, 0),
(5, 'AKL-004444', 'lou', 'los banos', 'mariah', 0, 'vietnam', 'female', 'married', 'vietnamese', '', '', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_accommodation_1`
--

CREATE TABLE IF NOT EXISTS `students_accommodation_1` (
  `students_accommodation_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(11) DEFAULT NULL,
  `booking_id` varchar(65) DEFAULT NULL,
  `booking_agent` varchar(65) NOT NULL,
  `checkin_date` datetime DEFAULT '0000-00-00 00:00:00',
  `checkout_date` datetime DEFAULT '0000-00-00 00:00:00',
  `sendoff_time` time DEFAULT '00:00:00',
  `pickup_time` time DEFAULT '00:00:00',
  `comments` text,
  `status` enum('reserved','checkin','checkout') DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`students_accommodation_id`),
  KEY `added_by` (`added_by`),
  KEY `room_no` (`room_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `students_accommodation_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `students_accom_1`
--

CREATE TABLE IF NOT EXISTS `students_accom_1` (
  `students_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `english_name` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `numberof_weeks` float(5,2) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `mobile_no` varchar(25) DEFAULT NULL,
  `student_no` varchar(65) DEFAULT NULL,
  `course` varchar(75) DEFAULT NULL,
  `dateof_birth` date DEFAULT NULL,
  `nationality` varchar(65) DEFAULT NULL,
  `passport_no` varchar(65) DEFAULT NULL,
  `expiry_date` date DEFAULT '0000-00-00',
  `placeof_issue` varchar(255) DEFAULT NULL,
  `dateof_issue` date DEFAULT '0000-00-00',
  `visa_expiry` date DEFAULT '0000-00-00',
  `ssp_validity` date DEFAULT '0000-00-00',
  `address_abroad` varchar(300) DEFAULT NULL,
  `nameof_guardian` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`students_accom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `students_accom_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `student_accom`
--

CREATE TABLE IF NOT EXISTS `student_accom` (
  `student_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(25) DEFAULT NULL,
  `booking_agent` varchar(35) DEFAULT NULL,
  `checkin_date` datetime DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `sendoff_time` time DEFAULT NULL,
  `pickup_time` time DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`student_accom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `student_accom`
--

INSERT INTO `student_accom` (`student_accom_id`, `booking_id`, `booking_agent`, `checkin_date`, `checkout_date`, `sendoff_time`, `pickup_time`, `comments`) VALUES
(1, '123456789', 'Corporate Rate', '2012-02-07 12:00:00', '2012-02-09 12:00:00', '00:00:00', '00:00:00', '[removed]alert&#40;''test''&#41;[removed]'),
(2, '', 'Asia Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', 'sdf'),
(3, '134', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(4, '123456789', 'Asia Rooms', '2012-02-08 12:00:00', '2012-02-08 12:00:00', '00:00:00', '00:00:00', ''),
(5, '123654', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(6, '', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(7, '56', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(8, '', 'Rates Go', '2012-07-17 14:52:00', '2012-07-28 00:00:00', '00:00:00', '00:00:00', ''),
(9, '12564', 'Bigfoot/IAFT', '2012-07-18 00:00:00', '2012-07-17 00:00:00', '00:00:00', '00:00:00', ''),
(10, '45', 'Rates Go', '2012-07-18 00:00:00', '2012-07-26 00:00:00', '00:00:00', '00:00:00', 'Na');

-- --------------------------------------------------------

--
-- Table structure for table `student_accomp`
--

CREATE TABLE IF NOT EXISTS `student_accomp` (
  `student_accompid` int(11) NOT NULL AUTO_INCREMENT,
  `student_accom_id` int(11) DEFAULT NULL,
  `lastname` varchar(25) DEFAULT NULL,
  `firstname` varchar(25) DEFAULT NULL,
  `english_name` varchar(35) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `numberof_weeks` float(3,2) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `student_no` varchar(25) DEFAULT NULL COMMENT 'the student id of the student',
  `course` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(25) DEFAULT NULL,
  `passport_no` varchar(35) DEFAULT NULL,
  `p_expiry_date` date DEFAULT NULL,
  `p_placeof_issue` varchar(255) DEFAULT NULL,
  `p_dateof_issue` date DEFAULT NULL,
  `visa_expiry` date DEFAULT NULL,
  `ssp_validity` date DEFAULT NULL,
  `address_abroad` varchar(355) DEFAULT NULL,
  `nameof_guardian` varchar(35) DEFAULT NULL,
  `photo` longblob NOT NULL,
  PRIMARY KEY (`student_accompid`),
  KEY `student_accom_id` (`student_accom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `student_accomp`
--

INSERT INTO `student_accomp` (`student_accompid`, `student_accom_id`, `lastname`, `firstname`, `english_name`, `gender`, `age`, `numberof_weeks`, `email`, `mobile_no`, `student_no`, `course`, `dob`, `nationality`, `passport_no`, `p_expiry_date`, `p_placeof_issue`, `p_dateof_issue`, `visa_expiry`, `ssp_validity`, `address_abroad`, `nameof_guardian`, `photo`) VALUES
(1, 1, 'Doe', 'John', NULL, 'male', NULL, NULL, NULL, NULL, NULL, NULL, '2012-02-01', 'Filipino', '123456789', '0000-00-00', NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, 0xffd8ffe000104a46494600010200000100010000fffe00042a00ffe2021c4943435f50524f46494c450001010000020c6c636d73021000006d6e74725247422058595a2007dc00010019000300290039616373704150504c0000000000000000000000000000000000000000000000000000f6d6000100000000d32d6c636d7300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a64657363000000fc0000005e637072740000015c0000000b777470740000016800000014626b70740000017c000000147258595a00000190000000146758595a000001a4000000146258595a000001b80000001472545243000001cc0000004067545243000001cc0000004062545243000001cc0000004064657363000000000000000363320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000074657874000000004642000058595a20000000000000f6d6000100000000d32d58595a20000000000000031600000333000002a458595a200000000000006fa2000038f50000039058595a2000000000000062990000b785000018da58595a2000000000000024a000000f840000b6cf63757276000000000000001a000000cb01c903630592086b0bf6103f15511b3421f1299032183b92460551775ded6b707a0589b19a7cac69bf7dd3c3e930ffffffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc20011080210033003002200011101021101ffc4001a000002030101000000000000000000000001020003040506ffc4001a010003010101010000000000000000000000010203040506ffc4001a010003010101010000000000000000000000010203040506ffda000c03000001110211000001864f53c5841116040904462c4cc58054804063615940431b10c086114605165a2e2abd355525f5e5a15759f0687373d2f2ad5554dc541d5a6828b9002190a9682d7291e5627a606d5641a2bd632001a821054aa6287cf355e36cb9e952dc6344ba6ea91d19af6c0596e9a8a2cd0ed671b6c95ce9d3697ca1d5a99ce9a31d3b067b5a2b64667cdb511c7cdd3e7cd50aeb350866597d1aab3d3aaabb4c6d7577184a928c002c35309d5694ca675cf5d07294f75b82fb8be546a5c233088184a902ca419aac79e9d0ab349bd133322fb73e9a56588d5163d5624c0044acc2a5a0925a452aae4b45b2a6e185bad6cce9b85831590015eb4d4ac4cd0d9659c85f3d71b76f94aa966db52db8a6995dab9db2a75be67a8d0d8e237373525f59b97649d6bf8f7c9bf9faae0f399bb5c9ba5b725d46b3cfd0d1e67573238e9752a832b05baf26aacf65b9edbcb53d5638c5149711c050f334d5f9eb4cf460826ac95c1dcf9cb5a8e66a9d5750fa6764aaca44a911aacae6aa4cefcfd370acb1eca9dabafa2cb8bde9b1cdad539164409dad55c10984c6006cd582b55108673702aa4750aabb6b555c600aaca15d4f9259c6b465b6af53c4f483bf8fd6c371e737d7a2a0e3bf206cd345b537d74670d74e348bde9cf917b1f9e66bada38968bbbb3cf68a8f4fcecdd2acfcc2753845f5f18bae6e3cddad66c5d3c014c202cd19ae73aedcf6567b6dcd738c8563644518cd29ced54a67a9822660806080cf5335a8d3a34836a3699301185db54d7330face2e5a72dea396da1e9b6a74bd4fa676bd36d2b9eb043a36b9a4bc1730ab3503de9e15e8d49e52f494d001c0a151ada945952e70bb3e4c537ab1d6b9e8d46d64f176b95727ebf8394dcebdb928d719653a53beac9943565a5f2d4bb5d9dabb3a2b672152de029b6b0d6ce9f0f4eb9777cf75b3e91c6d985e6ce8a2b6b7d696d4e25b52e6595b93a2da2c6b5df96e79d504a254f4a6b9b4678d2b0467a4122708006000c50b2ebb35971ada8b34caebdbac4e5dc1126ccd91d7279ddfe761d396fccd17baccfab5c5ee175c25a9a516df4d64d8a8addb2a01a5b2c4f40cea3b56aad55c6844efaeaa517e7cb866f662a467a45664ebb2cb691b0d9719535d69d05a4baf4574335e5a44b702e9a178b25c73a02b6d960b8f1e857a2dc968adadc0918adcefd5ccdfae5c6af564556d365616bd0cd5d5b8d72530b4f6d2e2d56e7b1c42b2e48918b9b5552f125d563bac2b34440222002cacc7d14e9d73b3767eb565ace20a75534d4e8e639e757ddcebb3d69e0d9cfe7dfa9d0e469d33ed5dcfd3b63b1f2d8e6bb560ec891a69594de294ca80112509b253922b4e2a2a8d1d43cd2d8d6d24672e52d8ed4323424ac0e635cda25822846ba696e6742592d69ad5bc4ec280e7a66b237d06ea5c5a558901d5a7d396ed2139dd3e6d28204e32165ed55fae2918699c65325ef4b85b01d7330402a623367df9e34cab6a67a22bacb10c647175cb5f5d9a67b571e4cefa538e634ecb722c6bab567baa532db9a34cd46baf2d5a466a7438da1cf7b47374ed8dd15da32b20f14276058054654efcd9b1677a31accf584b00b19ea45b2da906c6154c6d65497d0033e9a93a55aa8b8af6c50b9ae0a99c80b1b539aafb15aa39faf831a65d9cddf1bf6736fc559332b12410d4656a4fcfd74dc506054a1832cd38f6ed880e34c9032a6ef59468826b934101a02115805146cae2f1a6aa73d2b662c0ead52ed5c0cb5337275c37dd9d601d25660b2ecd53a6acc2e6e697348af5b54ab2cde9e8f17539ecd982edb1ba6374f494017267c89e8c145396aca0c5960ed35aaf525d0b575b960b7b73233ab3954a7d04c6c9da02cd2596b4b5bdad102eed5565fa5aaef7a4530a7315a735e9cf74dd8f74e9dfc5d1c3a608c19409030b2334b18dcd79b4674eb0431f6e2dbbe2430db054b2b8b8444688269992a58590898a9008e89d79f467cf4220192b01d601d17d3a38faeeb6aba420540f8eec834a3400ce5aaa5a1a9b74855b101e9bab45bab9843a9a78f754f572e40a8d0566803111d5da7b33ab5a69ae26e1193319c759bdd19daf214db61423928962d8d3da86a6db3290d7664a03563ab1cd3e2a6c9d529d48aabdd8f7ad3b9935e5d39ea605440432323351c3d4e6cbb712a51052b7762d9be0d24df04aadcd9e964125df2894af34b356946a962a5cc5654ebcfa33c68201346281b2849a7be8bb9b7baca9e58a2da8aaa9b720f4c0446bb00b3db29b9d0b9d5ad55d6137ae5898b23b2c59152d76426b9616905ae19869440dd89e95d9ec64eb7b1a5d4d694eb7720a1c0091805e4398188e2d680f5d78155b4d066edab4e566acdae84d3afcceead74e4d1975e404195048c04163d953d4578b5e3548ac28bf5e5d3d3ccf00d7219aea72d2145cf4b6465692d224b14dcdc51b5c482ae13369cd1a2992692bb2a9a0009bbeea2dc75baca6c8a6aaead3c9cfddcd5a6fb306e70d246955c0671a9c5913a0030df61629b5c551bdc31b6d29e26d30741d649c63600c874c1d06f6452d60048400108296d4dd563d4e40ae90b52a54eca28c736d480adfa1cde9359aad750adcf72aabfbdcce93d2bcd767be4780a5208c924633a339ab2e9ce55308a5a2ece7a30beba91cda958cf47541357b50c3beccae1b6fe7da1a533d9a67682ba6299eeae6955ea4c566b8b29166efbb2e9ceed7a9f3bb964460e6753973bd7d0e76b73bdb35ef321a316c528b5ebb046390aec6764b03a44c00c044c800501039228305561020e41009546af4a8099204e0af32abb2055a1a74e754903b49bf16b158cacd564ee57b975f37486aada4c1ca944923209185d19aa6a744e82af6aca6ecba6642c068b15180264a1658d5322c7ad8776fe7e9737e5d54699e75bea7055852aa8d34c5d2ae99e87662d1357595b63a5a50a32733adcb9e8cfa72de4d9ab1b8b79aac72e559a67522b9e860d0f9e0b51ccc8d2b4907558368a4722a8582a565cb4c15b1230c0123220d8514a7a33282d1c5b3a246b05455b72b8a57b1cd65a4b85d56ba443b393a66d930917cd7536d2e1e032104301118d148a95b2a1d0cad73664d15dcd319066080c204c1106c54858f53277db9ec73aadc97eb92d375379b290d2d7624d674d14e7a576249bdcd53e1a3b214d797d8e647473dc046ecdab338e94ce53d0d43d4da4472cf480d47210d93240d83232340aac081d82a3630565e09645022ba87726751db432951e689ba9ae09d4fa2f1732bd989c8cef554f72836b586c1a9500bd255b316ee2dc290cf2b69b6a72c4196448c824614b6b019f46504743a4caa5749d628cc1106088050b7720805ea645cd53b2db73b567a2b417174aaca988e8c54759aa96d59a7b73e9c352c8d9dddcee9669df8d56aae5ea5af538e7d95395d91363cf936d6caad7cea1a8677737356e27642169a14354c6a1b53128f626783b119879d75d4aa3bda9d32d215693bc9c7574b139cdd5f39df0cfc6ece4a55e4d7a832efc1bd3cda1a354fa04c6565cb03cd9d59cbd5656e58a997208c05581d5832bcb7d009145a4482885488c100c1014ac1b4520ccb6846b15cbd897699caedaea4b48e423a82ab2aa45749a17e7b73bb8a9e7daeb28d8b4e1e7e9736351ab2bbcca69ad9ab6579097d357613c197b3cf1e61bdd57306929e59ac873db791e5b3430645de4786cb2e2334d119559ae873065ee239afd3adce536716a7d072fa1ce6b85d15a55f4f1eac82afa9c7eaaac9d4c7d1667d8db199f856e7223ab849238646561124b2246a10cc2a551566d39685465a5582289240301149021088330406b2960d3a3168b8b6daedd3315d95d4bc05a0aca3556596aac8a94a99ad0d559cfabecc3745e9e2fa4e54ebc8d79f6a592da9daebf23b1cd15bdff35e8daaf03e347a1a2ea85c3d581e74dfb27466b0733afe659dcb6aeab9e7d9b505e5bafc48d7a95b7438cbc5eff986f9fdaf3da274f6c9cfdb5967f39e83823e968e0f7199ceccc1bf1f4b96d73f7e7df1b65ebe3eb34fc37c0f310b27248e4105cc5200c053924683296156545793565a4ab757acd4ae135920e40408110b1e02468084c06b11da7d19eebcef42bac585cea52b7a254aba66d55966ab60d2e5f43c5dae8dcfaefb30750af3f575f8f1ae9636d6755b92e9ac9d5e7a3192223d10e7971cbbe9ea336f63ca7a042f94f4fe50ad9eabc3f552f6148aab3f259afc93bf67d3f88ea3cfbde53b1c50c8bd8c45af73cfe90f478fa0ba61c4ba511af732eacf59efe4f639a147428ea2d4726612634640308954868891c880a64481248d190320211456756d1567db9f6cf2a5d5e5a20655504810488d136ca5866b54634d15a659b454d56bb5c2335fb4e1c8334efbb5f0adb3b7426dbc70adb4f3b8d5d90d610cbdf3ddcfad9a723677d7e3f46c2fcdf42ac935bf3ee473562e8e44efc1d8e5b2d5d78c566dcdb9cf0ba7848fa7c1b6a9a9b1ec6adddc540440f37d1cfb2cbcb9496d31af5edc7d7bcb954f4b18fbbb795d079e2e4f7b9256f0585b29b80df9299023129c8c1210ab1610d410398444c8200c11a752ac8ac814eaa5bb39efa2c34b155ae8cb4ce2d4ceeb0e06a1823a6b5069aa7ad331485d767bea76595dfa44a363f5e7caa3a1cfbae753d7cf5ae0df9a8c2fd0e4cbd432e636ba795caf6e3862ea1d97b23f2eadd3e591f478fd905f23a1844bdb977bd463c7b0aa38b706b9fd8e3f64308d79c5869f45800cbdda5e67a1e78706c639edd4db9b7698f2f9bea788ab3f7b85dd4eee277f25463e9f335cbddc6ed73a95fa29a0ad5ccad4957692c18450820aa430021a02414860c4205240d3090254f4e92d04ebc1aca0a7751614f3a6948accb7a45d4b6aa6ed5943b238de4614be97da7a56737a1ddcf62db9b49e567db6c74f32cd54599736ca3a4c9af1cf1b6f43670fabd3868e7f5b9fe76b94c5db2d0f9efe6d5d95b3b9d1e700eb73356b2b9db33a4d3ea0959e7da3635e5fa99a88d7adcdebe273d163b1e78a8ebe70a6bd358f81476b8f3af43a7cdecb8ab93dacace0f730f5153d1ab38b0585c77a538dcdb9d982bb484456820c08004005218b0ab4242124012482008611222b0b76b0abd1a3a32c648698a9a560489caad58aa15973d159a4d121809581614d5a2a74fa3bf638f45e9d5cfd1e92bf97d79783dbe7d1e6f374307bd8614ba9f23a4e9c96f3bf47762bd4e2a1ebdb06bf3df9d5cedd1e3df9abd8e5216ea2cd27a2bcfd03365952bb6ee5b8bb1e73d264713a1c6ed072b655995778d76bcf3e4e9e74f8f87673e76ea7639bbc99565ceaedd9ca353ae8aea6b4e7424a39b8163224230004c6084005600aaead2ab2350c832a6000408484223601cb33d4cefe9e2f4f7c5f0f4a691ca96d7a4890044749aa832677190e7a5914a6c5191677b81df67a119b8456fc59757a7cdd81c4b7936e5251b71ea5c7657ecf063cbd0c5cdad562b79daf6b663e9f2d700a5bb459d5c9bb9afb3bb9fb7cee9e7713abc9efe658cbbe77a0bc28b5eb0b2b4055ba31929b773a076f373a49d33c946bb738b03b1cfa632d15c4c94b1c80f60a96bdd153bc15719402980b0c0809694900158022ba31412121000100042c02577228bd749c14eecf9689d0e6b35e8b4717a5b63a706f7d33e32eacf422b24d2d6d5e764019e8f16058d491dfbb96c1eafcf368d2e6dc7b7d6e4c59b562a52b89172035299b4d2eea4be469bfb7c3ee785af03a08dc950d41beeeee4af3f659cfd35f4e549077e35b522353536a055a60645d507906c50cb35c6641aa0651a82741d64595b63066b341150ee01630050ea08ac0155d41612d0320085402b282ab462b180a194141508b33eb2c107562f163965015574ea7e7d716ecf54577b5707a5b61d2e6eeb76cbce55d0e6e5b1ae08b428d34f041c82c401dac795e0b0274475fa5c8eb7b1cb9b074b9d6568eb4531ebce8d36d3371e8d16f575b916f95acc5653cd7668b7a45d2667f3bd8d5662ec35cd5ea64e9f2b34d14d62b6d645a1eab13656808c4098a30131804244a4c022409010121000808ac01159410481082c904080c045600a5a02c218aad580ac66da4a89d38982344481200330445811b2d33e86cd95f63779ddfa63d9e56ad3b65e607579996d9a099e964480cf594fb5e93c37578b6d1cbf63924f39d4cf67d1725dcee9f33af3cea2b07155b95ad37d5a192e4b38f6dc43f561977d1e9f2ded3932f85dfb3266917436ac33a75aaa75a694eb5d3973b34c3a8ba361db2abebacef50ddde12121cbb56c0e43250180b183043001850b0c055741aa3a3115c009001220412008c00020029a5933a67de1809d1918230c100c11120812483901433d71005a30d6de8712f71dee7cd5be3e6641cfd260806080d767755d5eff8fd995fb2e2d73699cc5a3d7e75cd6e4e4db65f5dbd5904b13aa28d346a8b95c5d0e8f6b99d3f9ff6694be8f1bbf3d37a6d929a6edb95ba983a1b7153d9f39dd8df9f935e5e0f68d88d95ba90e4909ea7cdb4467912a42c6ad81ca9022408640064440402865054756d15d0144810821215022302a8c9459892be8ccc075891c02c900891920804481248892402416a3433529b0f3e8bab0c739a48ae19001100c040bd65ad7b79366ababc9ba741cc77db9696061e9f30564da52daec5a536d8e6dd8f43cc3e074d984cf3bbb7e3cd3b3ccc45abe7efd5bb99bf7e1cbd1e75d1b5f97673f8fd5b4d6fcfd6d5be4d31df511e8fce42a08728c0e5581d9181e0212488120618b11162b22141c42a1008362088c9503d39b1eaafcca3a33301a459224cf9e45dca0d4c925282409210061004b215acb1895eacf9dd51a435ab4497cf220e490249024902110095227bf3beaba2f8e7a505aab74835d89aa1b73f63caf5afcbd1abc7f4eee8726f7cf662d3939376aa9cbd5cd052275d5b79f37e2d8fcfd9cfd7b2879cbd7cfdb932f4e3d2e7255d7c4f7611d7e676afe0db9bed1e7e9c9e864692c6a885d2b20e1421c286305036014221505465196a69a5b6be751a2db92a5d61964b524806082304040c33ab2ca2e011c5cacb2310b3856f75c8cd65d9c570a9a6ad5568a47683620d479f9267a4920492049001920492033d6c2b190e88db5ced8d129b7a22fedf1bafe57b17e4d99bc0f54d944b5a730a2f05cd766d156a1696aa1db4c17551661d9badcb7e1ace37678dd9c35a30f53c350d18a4802d5c1e8d3ce32faf6f10e6fbcfe7da5fa09c028ef4e0c1f797841aedd5c856752ae70a5b29a45ab156529200304030403010924080c408602b030ed7aec4f544d6e72dce805540c67b733525422b4e8c3a13d32b2cb42ab5c792454920484041204920e1044d1489ca4077a9ed596576f4677f4b957f3f6f729adbc3f7ea87274f35d9b665bc684b2f6b1be80e68dc31efcb6d946be6f4a594518eb76023d1f1d15c7570286002180b0c05840e49004207248124812481248124812021248120804488924086403246a490046880f2c9a1a1b5092322057657360456866bd032ad9548fa725c3d46866af5ae0f9b019a924030414920e48452481242008206caeca9b2daecd339655626d65472e9bea0ac2156f0b2dccf3af50664c7aba1cbd19b4e60c934c5c2c68a9950b0860570350d01630050c004906218024802180240330403044192049200848292414980098210c60308025d074aed1156ae914658a5aacaa2c2944c23234b45c88a9d0a773ab3091039f048a680b2404219052101248064225300decaedb8b099501922771a8b56c4668ab4048f014c099120422346481248d00c1b00c4c03180180a08010c05860d618024812480212810c0108090c092409201980034480f16031460b2fa368aeb545c15825c5026955c4d54184d548f5804b08b3cba0432a07353a7ffc40029100002020104010403000301010000000000010211031012202131042230321340412342503314ffda0008010000010502f99fc88b23de8e45e965f0b2f4be5e4dacda87e1be36597abd18d9390f4a2242246250a06d361f8cfc67e365328a656b5abec68921f14448a3f8be3b2cbd5fc9147b11f951f9073374ae2de88b2cb2cb7a5f15a6eda5d8f4ae57c2c948948f2514244311180a246224570a28da380d325713f22379d328a1a2711f144489fc5f0b66f379b8dc465f2f513f28e76f71b8bb1084cbe0cbecbe1e4fef17c9b10c721c89318968918f109088ad2f4b379bcfca91f918a6cf24a164e144f1e962c8cb4f49232469be0888b45c6cbd2c9b1b2cb2c4c522cb2fe094e86c459626217362172ad3ce8ca2dd6887c18fa1b218dcdcbd14946507166380bc4a746366e132cdc6f3f21bdb14921641641642390ea6b2e23222332acee24677a648da6b8210b45c59e0dc398e5c6cb371b85ce6e87255e78a6589e97c3fbc7ad3dac68aee9e8c650f8b64a44a4416e7e9b07e389eab0ee4a1dc464fca1166e1c8de58f2d9f945316414c5908651494966c74658d384cfb2fa494acb3247b7aa10b48f2632fe0b109966ee3256bc3b2f44f45aae162ecad2b5b55b8be1fd1e95ab1b2522532cf4b931c084d49599e7b62bdc224c8f728966e2531e4379b9be1629314c8e42193b525359f18fa7099955c6323ca7ee525c10b48f2b27f121695c68c989b5a5e885a2d1692f14d918d2fe69fda6c70146b4bd6ce86b8364a64b20e56791b2333d3677194b228c7366df2c23f1966638f563912c86fd12144a28a2b4b1488ccc79097be39954932321f4e133fb35c10b48f163637f1a62e2a2422d9f8faf518768c5a217043ec8c2c8c0f1aff45d0da652255a783ce9e06f4dc4a64b2929d97a4112c7167e2a23ed1e49488c1c8525053cb6420392259473d12285c9a2b584884cf530b44593117d794f82d171b18fe4522cbd231b23885d0d9333e2a6262d515a461da8dcae311bbe174290dd9659e74b3716390e64b2929d97aa44514768dc7e43f248b6c49449651ccbd12d68506cfc6c6dc4dc296af44c8487ee83f2798898a43e0b45c68711fc6b44848c78dc88e2dba324c9c8fe7e0736d6d1488b10b4aee2faddb558d96597a59659659658e43992ca4a65eb4242425a50e26c3f19b944722f54b4a121222baa3d42a20ae227c3fb164599954f55a7f38ae725f1a108c58f738a514d96363912d307d7d64b1964264642909899be87394845eb65eb6763658e44b212c837aa4515cdca872be2914509090b4bebd4b300d76b8c7cd99fc09d705c973921fc484415b4d456f372370ec977ae2c9eccb22fbc69cc4f6b8cacdc22be4639512c8396b425a514515a52d1f0f0791228ad121222bb24ccb2b9607dcd77c9327dc18b84792f824be1421117b53ca7e53f21bcdc5d92133274e5ee3f1a23ed1909d119d8997d7c165964a66498df04b44515a568fa3f8d9e1243a2848ad28a12b12e92a8b334e937de1f335ede7649763d57cee25735a22731ccdc6f16416414cde64546f4c7b4b10cf0639511997c2cbe0c94da2594732f8514508a28450ce9b9491543686db1212144a28a12230146dc89ca9669db31795de2e7664fb0f462fd0711ae15c672d28d86d28b685918b28d291b18a3a31e9099162658d97ad964f212c9637c12be0b5e88c50ff1a252832e28de3bd144485128a28da4719b49492539519720fb97f7110fafc124243d57e935cdf8628891b4da6c1e3251ad3711cb46e8bd18c5a4320a46e253217a592992c8395f151146f5b379f90fca7e443c88bb3a42a3714d8a250a3a5154460d918a422590964326424ec83f73f384892f3cff008dd0f542e0fe57cdf8fec50b8ca435ad97aad3c38e414cb4290e64b20e65f0f04522ec732cb2f851451b4a36ea9695a7b626eb2d253cae44a64e63f2d11f2cc2227f6e689f08f17f2be6f442d2cb1cb5a1c745ad68f4b148de6ed1f1439597ad328a28ad6b921166fe91bb68e56391398d9769fd60497785089fdb9a243d63c1fccc7f0ad1e937d27c1c4f02d5b1b1f05a3e55a428d90a7b5695f1a88de9e0dc58df53c846564a02e87f55d69863a4fedf031f9d23c18cb2cdc6e2cbe6c7a3d6f55abd327d62f8b5f35145148da6d28a36945146d28ae29595b4f250e5a58e44f21e4a716ddc4ff454dc577863d0fcf344c7ac75b1bd6ca2b5b13e2c7f02e393c5f717c5a28a28684848ad121246d36d1e4dbd6d369b4da6d28a3694515c6858eb4a3c1632c94e894edd886b747c691faff71ab945547e144c7ac74b2cb2f5b2cb28da50b831f38f85a2d1997c3212172a369b0d86d121228d86d361b28a66deb694514514572a3a2299ec439a1ccbd1c89641b1796231f70921c7a8fd7fb8224ba5f14c7a21165965f0b2cb1313d254b831f17a445c199490bcc18b8ae34242457c35f15a46f2f84a7439de8c431184990ee228fbb0c69647daf8a5e5ebfc2fe0b2c52379764756cbd5f05e78e5247f5f4465f0d14572be37f0597c5c872d122862f2ca31123192fb638ee7f484bcaf87f9fd7e34fe5fc37c1116490f8b1f05dc7865247f65e232232e15f35965f0bd2cbe4e64a65ea912428dbdb5a638dafa915ba0ba7b2cc38e9669e8be17e10f57e3e54c4cb24bb1f07c21c7212d13ea4a88b173b2cb65b37165eb5a59b8de6e2f9d8e63969425ae3954b2117529e3524e3461fb4fef8a8979c58ec9bd9193b7f0ff005e92d5beac7f2a659e50f831eb1f3c26ba98c893ee28c6ad3f6bd6cb2cb2cb2cdc8df13f223f21b99d94514515c2c721cc72d2848da6d369431cae288778e7122aa525dc0add3c30db1cd3b7f1cb49eb2f1f3a62631f17c23f5d5af6cd0c447dc998b26d1cf7491bb95f0a28a28a2b8d9b872376945090915a638d9b0947689fba4ab4f4efa9e925fe387461c76f2cb6c5befe247f592d65a5fcf65fc15a4387fae444b4c3e722a91130c6ccf0db2f06f3727ad14514570b2cdc6f379bcb2df0a2b451360fa314fdfe5661fd92de4a14f079ab9b8d4a5ff962c7be492c71cb3dcc5f17f090f57e7f6d708995124455a83a797b4c898254fd4fd4da6d3b37b379b8dc6e379bcde6f371b8b3b369b4a368e0c8c0da568a04237170322a8a752c72b8e5f32a31f5973408752c3dcf27d9fb8c38f6473e5f9e43fd3456b5f0af1a47ce456a688bdb2f128bb5285091f57965789223024bdb46c3f19b4a2b54851369b0d82456a91b68d86c360a25186433d43f6bf3e9f2d393f74c8f4f27d60bdd891951871519b26d8b76fe35ab1e8fe648da5085f24782ee3922490bb7f56bdd18f9cb1f66feb1f6420ab2af6edb1633f1976cda6c3f0d9b5228a2871377f9369b0da63547aa9758b350945ad8871246fdb954b74327b9cb123f1b46286e3245dff65da48c7d4238f74a6d638649ee7f22d1f07f35899f64bf4713ef2c0c91179c91ea3d3af7496ec061fb40ce45fbe103d47b61f415492442164a1b4524e5181f8e8a36991fbfd2e5de3859f88db47ab7deeefd36613193f127eef4f97d9b77295a31c6e6a1b639122bb8fb926638d8dac71cb977bfd07e1fe8a22c523cfcb1d53a7f68e680d18fdd0aed189dc271a70e9e2771cd3ff2dfbf13b8fa8eccd2f7619f71564152f51f5befd3e4534a28d8497597ed8e7b658a6b24467aa7791f984a8f4f9f712664face0d09b47a47b8c90b30ff00eb232097705d63c573b58d65cdb9fe8c86575f3ad62ff4118a4648dac90a31bdb29c7dce24254f2adc918f35629cada76fd34facb3ee6ede28f78a54d33d4fd25e70e4daf0e4538993eb93ca67a7cee0e32525925b633f7371d213a31e7dd1ab5285128c4c0f6e49097f95997cc1231c6cea0b2e5dcff4a4256da1fc55c56ab54b4a438fc0b54e9c5ee8e580d53c52528d5c484bdd35b6564a342314e8cb2f6256e0a8592b2629ee8faa97f8e6459833383c7914e3ea2558b2691bac1ea3f1bf559938297b9c148963ad31bef1f6a48cb1a22fb4ecf191f8cc8c189b3a8472e5ddfa92211e9a1a1fc145146d1ad16896b1572c9928fcac59d91c898e37c6b55ae29ed7256b3633eae196de48d4bc3c8b740c91ff00110f129d98d1feb7dfa7cdb65eaf25922286b61873bc6fd4e753c726220bd925b5b7a627717125885169fa77d678a271ea51daf17897dcfc7b9b6a0b2e5dcff4d8bca7a3435f06d450c44b442566d28a24bf1e390f45231e51a521a196263d170c53b271b32e3a174fff004c7f6847c38d4bed868c51ff001f87122ae1287b9c36a949bd30c0cabdfb5dcbad110face04a35a6162571c91da59e9df4df535d4cc3f598bb253505932eed2bf4dea9e8d0f9d9b86cb2f442644da463de5f71ff00cc4bd3a2589a7b58998f2d17bc9e371d10d6ab5f0426a6a70b3263a78a7b5aa538c76e4c98f6cb13ea6aa583eb9554e2cc7f49c7df28dc3f0fb147b82a5579a78ccb128441156b2631c68c7f68128ee5923b5fa765d92f135efc5d4689e4daa726ff0059f04c7a514515ad97c5088e9fc4919728e66edc3544a27831e5a232dca78d2289aff1e8b85ed709a92942c9e3dae321fba338ef8635efcd03d39ea227f71fd7321fd610ff001fe3a924417be8cb8c688f9c5e52b7b14a3342f38bc1923647d8f14b74d8e1ba49513cc6f77dbfdda28af86b48f98c51b74c9d465afda24d16432518e6a9ade57f85eab878219148946cc98a8c32a20e9ce3b26d7e4861f6e4cb0b8d54b0cba9fb918d75b3fc9930905dd128d9971d34a8c24175256b2c6caa787eb4327130fb4bb1b51253721897eb322ad573a1a2be252a2398864bd331b4ff00e79b258648aa6fccb4b233a31e531fb94e2e2f45cb1e5293278cdc759218dec964876bdd1f5106a716625be3921b6585928d48940ff53246d3b3078d271b1c518974fa1c90fb1744b250e4d9e44bf5e463293258c6b9597c6b928115441da9f72f4d8128d138a33e1a2631eb091e99d928a93c8b6cb45ca191c452521c1336b836d4d639ee58999712cb09429fa4951ea71266eda639ee6b4946c93da4b22272523008690d124ae2d4633cb6c7248736f48c6cafd793a1cbb84c8cb494370e35c9fc58e1b9e1f4d18ac986328a5b65fef1f0d99324623c91999176c9eb03d2bef3ba93d62848c7e9673337a78e28bd3c9da2394b4c711a69c66d4b1e4b5ea31985d494ae3960626d4a2fad2494966c0d1b4f4f1a529a4a594fcc6e7766e376bb48f51fd66e893b7e04cc732322c94772946be647a28f63665fbcba94325c7264db1cb95ca58dbb9fda489ad63e7d3fdbd4af6ee2d11ecc78518e1188a8f58fb7a23c9b74590b4ca137116774dadd0cc8dc9a9223928ffe8a7ffd3d2ce89678929c59f919b9bd6f4a28512bf65f63e87a29518f2119098e36a50ae2fe047a4975bfacfea9b78a5b9c88e5d8659efc6d5b8afc716f4921e91f380ccaf14918f1ee2b698e44189b3d5bf7eab4a36945b3717c3748767677ce8a368a257eeb80d68998f2119098e36a70ae0f95eb8726d9396e8493bc3d6932cdcc6dbe0fcd15de2fabee3b6e518f4cc6fbc6cdea066f7cb8216946d369b4da533b3b3be146d36b14448da6c2bf69b2f9380e22e886423222cdaa465c7b465e8f95962910cce23c919103f9939bd28c7f487716ab25e91fb29d2726f47c50b4ad769b4d86d361b0fc67e3361b4da6dfdd6e86f9d9d3250a3c10c842646435b965c543f8ec8cbbc62f1907c98b4c7f5c0ee1ea1d65fcac53910bbb2f4ab6d725cacbe55fbcdd0dfc4a44a367821321908cc74ccd846b9a31fa47931cf1388e3a62647c641f2627a43c63cbf8de59ef9a8598b10a04a2d6899e9a70bcdb375924ab442ff952917f2265290d511910c846675232e21eb7aa3d36778a4d63f510cde95c070206364c96bfedfd1e916ee24e02463c76f17a758e32cb147e58b278d31f429107f9202d68ad1a4cdce07f3fe1b2532fe7f24a342646643217b8cb8b9c6460f50f1b8658e58e5f4b1912c32c6e3d12f13d1b31bb97f46491897b9137ed8230463831cf23936c6c53711a591185d4f22a7188f1c9729ab8fa7ef1ff00c26c94ff004ac713c11911c85dfc0a462cae2f1fa9b3a6b2c764f7adb9245929598b8e25a5dbc0ae4ee4dad18c8ca9e45de2fbb571c193f1cf3a52849717e3d2f87ff01b252a252bfd56acf0d488cfe14c865a31e4664caa6a4d95b89c92d312f6f0c7f46c89e9d776791f8dad8e2d69e618749753c32df85f17e3d3f58dfefb64b20ddf0a2bf4286791aa14be2b233a239931c62d64eb48a2a970b4a240f4d1b238e308e54aebb7b31427b66a6a9c0c7e4cbf6f492327537c26d6d87583f76c94e8964b2f859658fe6fe1659638fc7658a6d12f72da6380f8a46c228f448f5729ee86ed9e25ea2327382db0ca7f13a9998f4afdf9ff00f4949216b925db9afdcb1c89651cb93d2fe4a144da574d1451b4af9a0c58e3272eb961c7bdfe28d4b0d1e99ed52a63247e425344a5659feb195c7278c49c5bec9c5496374c94b6abedcfb8e6688e54cbfd873489661cacbf897c1451b4dbad945148a46d5f3c67439d89f047a78d447213ea2c9318c6f58fd564dab7917ae646e1cac6f5b14da239c5962cbe17f33c890f30f2b66e2fe54f5a28a28511447d165965feba7ac4c2bfc6c90bc2631b431ea9fb5888b13d27f5e7658a6c59e42f502cd13f244dc8be166e47e488f2c4fce879c7958e6597fa2b48d0e2288a35a592196596265feb5888983e83d2c6ac7188e896be1421b8ad22f46ea3f1d966e371bcde6f379bcdc6e2cb2ff550842d2f590f5445feb2d1099825d8c7d1636365e9b5b2b68a2dbffc63e4a2b4cb3a8ff3fe6242889572631e888f0bfd25a2d232a6b3291633738bdf8e4a74509c46db361bf6a6ec8ba7ba23cb144b2b657fcd488c4aae6f46b54cb2ff005571df2afc921c8b2cb23e76ee51628a9ca5c2ff00e6a4462557c4f55faebe34c595a8d9f91ed7ff004122284ab8be2f8afd55c2cbe55ff551155fa0bf557fdf4417c6f5a36943e1ffc4002a11000202010402020202010500000000000001021110031220312130044113403251220523426171ffda0008010211013f01f750a256161e117e842458d8d96596597e863e291b4a1c4ae3181450fde84b0dd1763c51b4a2b0b142cbe2bc8a25628a1e63d95c1fa6b8244624b12621890a26c361b0711a17958acbc3ca5c9f084feb0f0f1d62b1622b3428091633a2ad8ffa140512b831a178634515c1e111e4caca441df86388f9515c1444b16596783b12cd97c1925858965e62c5c5b194242440af04a2515ce851c365f04b9212f1c191ccf9c645e5bc474cfc66c288ff0062659288d71a144ac5f142e1788a10f2c59d4f426265e11159da6dae8779944a2b0a228e5e28da6d28ae178488ac4bbe1f79d5f42165217a10e26d23a65570af4d14289544712ef9eae16369b4acac5090b9346d36e7771a12f4a895888ba1f3d4c4514514388d61616239486bf528512878489788fa26851122b849142c2c47311fe85e284851cbc44d497a6b931a2f0842ca195635c68a2b15ca851121ba2c4cfb3ece912e6fd0c62c213c2c2e862c35eca147364b1f6744579252bf43f4b286b845e21d12e146d2b851b59b4da56771b8bc743e8ec93fa587efb1b2f8ac418f3d962916b145158bc6e371bb82ec63e8fe2bf498c59598b11164b08622c8b1c8dc6e370cdc228e888d17e088fa3af277eabf4bc5718bc26779ab28aca24b09124450a4358fac22cefd5274262e165e5e121c06ab1588bc42449178587845f92bce2fce1af22635e489f675ec93b109f0acbc6ea629c85a8cf121ada27649511798cbe98e25962ec962f09e62c90bc0c89ff9ec9759427ca4372c4604a245f91ab1c5a76897588bcc65f4c71fe8658bc979a22310c48af748ae09f1a360a352c50fb23d144e36bc0d111458d56149a3c487017818fac262686acd878377bda1ae0b9cff0091176452ab64fb34de10e245211a9df05268b4fb3c1e0a452152370dbf7b746f13b1a1ae09714d2ec7d899bc640dc69bc234f4a5335fe3b879f5597ef72a1bc2744643564a27447cf1da4971892349f91b34fc9a538a548f912b833f04df9a1aac532bf5252ae4a4764a27988a5794458e26a469e286a888ea8b69f82316cd3f88dff21e84a1e624353710d5db3dacd68efd4a89a5f097fc88e8c1748d7d08ca24953fd294bd09d09d9289fc78266e275342879230a27d886c8ab67c5d38a5b99e07a91ba3556d96e46a4bfccba6a645dac4fa357bfd172f52c38f262f03912c28b6460d1393a50346ff0011ff00647fdcd3353bb179d33e3cbfc1588d796d8324fcfbec6f343e6bd4ec623e0e9a93353460c9fc55268515154897c68b7628a8c689e9daf0686924a994aa87f23f0f867caf97bd523717ebb2cbe75c68af5343451fe9efc8c639bfe8dcc94852f04047cee86f3659b8b2cb2cdc6e2fd945097b99f0f536c84d35e0d7d5fc7e45aca7d1293fa14652ecd5d4fa3e36ac65127af08fd9f2be47e47faed8bf418c4e88fc9944d4f9329aa66f21a94ec87cbdb3dc6b6aef958b51a1ea32ff005af0bf41e6b85feeae7fffc40029110002020201040202020203000000000000010211031020122130310413404105223251335061ffda0008010111013f01f2b6597f80c6ca121228ad597e05c996589f2722cb179d8d95424550b6f4d09d165963dae2fb0df0b16dfa2f6bccd8d91d450c436390e6759d4588f4597e06f92e138fed69737cac721b2847b2e969c8722f826263f4265f81f25a6f738fed098bc6e45ea8a28ee7a1c86f9a22f847d72687c52d569b26364645f82c72d5716f4df07c531ee1cda2b696a533ec3acbb19450989f16c722f55c58d8df17a7b43de3f034515a649eac523aac55b8c84cbd390e5b5ab3a8ea2cbe4d8df147e85ac5e17b7e8970b2c5a6291d43997bb2f765f1bd37a7cd6b169eba8be0f56364b9267517bae3639786f6c43e2b58b522cb2c5213d3d3d4b6f85f0bd5f8ecbd2dc7df820fb0e437c62cb1e9ea7b62fc2bd5f05a5e87c908be4b55a7a96deefcb65eaf68adad3e4bc0843db5a7a7c2fc75abe0b5621955a7c50bc28b16e89ad3f0d1451d8b4597ba28ae297ef487c578684b94976d3f059d45965f0af07be0f8a17810b9cd53d342e34342451456ab8ad3e6f8a12ede18ea38e4c941ad3d4d5afc1a17e04236343e15c11144b2b6c866687152571197a9aa7a6b92dd715ab1f9511548635c2f684461fd05f1e35dc8fc58b278e58994b2affd2506884ad1356b6d6eb4b9b171bf1c177db435cb147aa547d3117b279a31662ca99920a712fa58a51c8a9905526b538d6dabd5e9f0bd2ddf9e0c52e0d71c72e9958fe4bbec755c7a90ddb31cfa598a5713e4aa9b3a8c191472352fd8c9fa27351f6426a4ad6e8be54596532979d31313dbe7817f432e1712106dd187b2a3e643bd8e2ccdfd6699f6d232646d12302fe9c6b54f54514bf02306cfa871a13a1313d31be308393ec254a8ab3a122265f5665c2a71b3e746a8bb2bb14bf6639a7dbf2a10b12ad3564b1d09d11909d92e2b2a6639f72ed58b4fb3327a314ed1f3f139415187e34a48cd8963ec4a0cc71ee75afc8863bf625c650b3ba2323b486ab6ccd89fb463f91d2ea47c5caa7017a2e884ba9d9242528ce90e0a4a999b22c51327c8b767d8a5ec71a250b5647b47b93cdfe8791b31e469fe1c31ff00bf038d8e344645f5706896084fd9f1613f8f3efe879a2913cfd5d91f1d762bb09772bf67f2591b9f4a3bd8a0d11eea88aec57e892ad47d8bd7e02564215e26342972864711b535d8863b91054225d8725467ff0091c89d7d9a7fd64447fe4655df58e3722bcf1c628d699627cde9ae78e54ec84e15684c4cfe4f33847b187e76583f63f95ddb3a9b764733a1bb6464659bfd16cc787ecee61f8dd3ece843c438b5e25162c42825c684cbe2d965f861368864b2333f95ef14c5a5047489144868f87ef850e087891f51f51f51f51f51f5a145785eecb1f962e88e467ca7f6428e96998b1f59f538891d910819a0d3162933062e8fc6650ff010b4f14591c4a3e8a1c4786e3463874aa284bf1eb4ff00017fd13f0fffc4002d1000010303020504010403000000000000010011211020313050021240606122324151714262819180b1c1ffda0008010000063f02ecec2c6936d132a3533a11d7e2b8588ec16d832b0be2c8da986bcf50c17ddce6ff001a1894c7778bfcd235dcfba9cc33635feafeb49ed63b1326ea6755f8b2a0d5eaf779d2646ae28fb1bb7470a68fd139b1b7be6e1d28537e7a59a30a3f1746e2afb139515718d3c27ea71639dde29367ba11e51a31d6c951bc30b1aa57eedbdb541d85ab9b7887cec716bf4a76971b1bd91a0f73edf29c26348d89ff00dafbd2f02e3b8b8ebfdcbde530e65e5469794c3f92b947673fc5f36614468ffd511c3f69b871d9afc49ce3a360beca95eafe978d13d88e7a5618a79be6c3d8f2bdca3598526c84ddbb3952a6130d29ed7f514dc21bb93e3857deaba3dab3d0351b741b903ba0db48a1a3eeadd063a8f157161a0ec1cd32b35c7522afbec29ebcd5aad508a74ddc237a347b06c62d028c3786a85c3d34a8369e1b1ae268e530de88a7094dd0f2dbf94c6d74f6126e7dec715e2c7516356285729b00a35a455ace24cbc6f8786ad7040562c6f9b0a74f535639b79682df1bd358ff0006dccd591369a3d4d8e1136b1cd8280d676e958d57b390a20e4526a28fa4e11abaf080073a62be36e859ac6b85e0d39a9c3425350d2709aa281b4c15160a4edde6d95e74dad2ff0008d9c55347abd1ba11b83295056359d79b1fe0a3c346a146de6b1ec168d075e54edcf5636c95069c26f7168e3f909ea53dada441cfc5c5ed8debd217e3518ae54f6b580a71ae776f6952343894e8b1acd18ae7178a3ed91d3f31cd5c5e5494da90be8ae529979af304d6c8a8ba14ee4ca64ac26b24a8288d5fa1e509736cd8f4e6146be31bdbdc4d49b8206b0a540141a5098874e14d2166dc6f9062c2da23f28d6298e8735cefc7a06b32bd5c49c17df23a5769d60b8ad92fd8ae3abfc235c76145ce3479b8483e148e849ac0b3d4bd21d7b027e1c76138d175f635deafc6a29e9ab7ea1423eeff005484e3759b9c693f0c291aad4e7e2f71c053638cd0275cd9841c5fc5bbb8d5caf52f1a66a2ff00ca151c5f576171f62f85e83fc290c6ac2f3a5f8b1aecae3de675bd49c68b0b5c562ad5211b8765be930c29fba38c1b85d9402e57c75d1ba614526f1575346a4ae638eb63757fbd33738d09ecd1a66e3a98a6566ecacf6249b9bb6c8d2f34fdd6b03daa0db208fc28a7b57d53d21ab950a3b619efe2f13423e1bb908fba72fc7f825fffc4002a10000300020202020202020203010000000001111021314120516171308191a140b150f0c1d1f1e1ffda0008010000013f219fe5398e1411b0934b6751bbe4425473d09eca39791c6e9b3f8139a3ea362d7ec6d2e06d2546dae3b126da452703ff00ab1a69893d8dd1b5b837bc1b0dc4cb83da35c7a46a2558a0418f1234df42f5421f42bd32186b37728af643f637f6418a87789313c160834838fe2be02fe57707f276ec3544837fb1c5077995becd3b34f9c5d68fb705115bff00d0f87085a1f3a1bf9a2347d7ec7ae2243d1146fb37d9a0e3392e58c6d4f9f6348436a8eb0c6c674627b8ec525473dc23d62a89424381515131f50d7d0de85b90ce6f708127b13573061d1745d5ed0a3cac170638fe0a450dcaf65fb2fd95d146cbf8522ee7b6246f6f4761a384ba367bd94135a4686cd898aef78ab8487e10dba43bd09a0be50fd743d083f90d70cf86b5f2453884deff004367c8ef68706af07de21a1bd8d92a88db1106b654d29b22721054854512a34138f91fc05d4dfbd0cf2bf909ae841a283bb91b4e09fd8a69ed0b883de0fa0132b238de8e3e2e0545c347802c95eb82565f24253b7d8c6f644e47071346368a24b83bff00ce3ed7f63d1c909ba516c9a3ad1f25d106d345e3d92a8d6be06d445abaa43e0723f936eb2719471d515d0868db2a941b8221dcc44842d8c60b588667e03d393dd9ec71fcfb3e51147f6cd234462e064e7f246c084d723b5f42546e94b13c2c1c72e87d78ad2bc3a0a0d94a5c1141d7e0c699c072c4f06f784cfe41763a4a692144f47654c4ad08bf671ca38fa23e4e1c9c6fa347a085a9a2d3743a290f417d1ca8d05a47387ac8506a52d8981b7f435a12d468314b42c43686aa7542c1a21fe82467df97428e44cbb23d8b171c64fbd8d5b6c9a66c7a1d2f04d89a06dc45aece5d84abd8985838e5d0daf241e3186ca5294a5c1b7e017c29075cdca14ac53c1ac342636b939fa17a1b6972473d7b1f5f42ec2f9e4d3435e86fec96dc90f87489b45f91eb7d9a65a0e37a13d8d5ec6e34d1309653763ad9faf05f590c25bed8c9bd84215fa1ecc20e47926bfe9f0370acd89fb131a5b358cf6450b91c22867a623be4458de93da190c18f08718ba1b48be2c36c6cbe7461aa86e8822944eb2890f01a2e0b8c1b0c2c2b83a4bbdb24f815ca6838f4f93d1ff48ab41ef8e0a53646ee968db869c5156e6c96c7b3706fe70a11f38f118721c9884eebb2a0d0c77ae8deb7c0cbe851c0e86f834e05ad5d9d6b81b32b6e8d78209cf21c8d106fb1179d93f21d69065d4fe8db4404e6bc32db1e10c31743e8be0da188a31bf3b84f17b0bc248e1509935f46d7076bd1a3184c618ba1086e86e1d3983fd08416f485b769def049be1135529d333b5d3d28d5e209fc20dad082d3928b4cb0722cf9868758dd4dead3f4346e5ff04f1d9cc9b196f4be4f4106e89977fd4247a87f17ecb4a082410b1a34f038c2d2e4d1c8e6847ae1a226f0342365ca13fd22c784218a389e5b197583fc144c4f120822c1b570fa1128912d1a07ff0042c78184c42087c767b9a350b63e0532ca11594bec436cab5a2646c63ed0bb34e861f6c12ecf946f61d621efc0ed0ff51f47f052e12453b2b8778bf47a073c16f1a151331c0d2b91a462bc568c4575c6210d83510b19086e9f05c503749e50c51c4f2c78083fc64306d1fc414ba7bf6701a70508a834ded3e9d8f81c86b47a7892cdc511d8c72d917a11b1df67c987b11f25c2d336dd361fa17391942e592ef135f63ac416f958106283df46bb821a317f6339c92c290b26522d1a148a163daf19cba22e0b44fac367d33b16d3c34670d89df0228c5f166e1fe15828837e2ede0a7c8328e98c55864f6515267b637525f7445e4897dd34145b3f9072eb469ee8e2314bf221437109e8fa0dd688f269b13ce62b84ae14a88121684f14d08e038377e90d926d895c6bc00afd85a186d486dc251e4d25a17d04a87d0ca10c4c6f63ec9e2c5f1651083fc085147ad210d3823d9f331d20dff4c6e7da1bc2d74158eaef6587b1730e749a883895a63894827bf8294a5c73f66916ad1be877dace1ecb7c0af4b224f421303f862f6e0d5710f5a125b36fd0936c2b04c142d7e864567cf33d88589ce13efb11d1a0c128d10784f162f933bc687e682e1bbed9f360be62aec4feceeecf623f68677b925368d9cb1675a63e54d4290cbd158b6f099ca3691bf2381f61254a61211ec16c4f7926129a29b56e596d21ae4e46be35f2c64d7f262b22230410aa116fe072dc9fa175172da945c1654bbe05c89c15320e85c8d58f1d73328be6d6261af1484161f28d1b88fc87ca5386246a20c1c911bad3e211f2ce03d8aa16114fdf23e0d8a06a5d68560d364247d49a791cd8e9e2095710a39db130822106367261a2ac6fe1e0274a5028fd8e40af39649821fca747f2f07316b04dec7d0404178f2721397b12123cb98b8f062fc2d6288d10820961162a6c2b156168a42025c892ee93e3224c145c12d14d92c1840a3c6887243b3817c145028b490b62104908fb0da6dcafa20dff0018daadfb1afadfd8bbc13f83b9bdfb64acf66150967ce55fc894d7fea219d493f68df10ff40d75e05d89c3da1f3e0c451f25121285de1e2b85e0ff001b1ac0d793ec6ef32840d4f40d1c3287f23923686a39c87a751d79dea1996c561a093d63c52091064af81e9d070268946bd8e4b13383bf6411273ec6cfae05d946ff00ea5ae2be853b1096525b1056f6ce017ece03f6765fb1ec2e3d5fec48d2fd9253f4d8b1d0bb4714247fb179355b12dc707aee19cce0b2ff00218f07e5cc2572089865194e06a14411a3c2690c188ba15b12661b0f087234bb6c83abd10fc09c22bae174372caf65c462637fa3d05ed8a442425b10242f412df67fa131ab6e1d246c3f53da4e8b06d976cf7124d092ce485da38a3fbc2f0ecd3209a620eccf21618c6dfe4783f1a3685ce6430ca20f787433158872269217670c52450b51b33ef939782d7d8897fa073d653c6fd614e228a42222121329511176253d13d76c7e807d6b2786fdc7b578a4b43dba693ec4fd911b821af06c474c65e2b1c0539f816780c4f58a5c5f27917852ec5c8b0a38d9036585853e05b1b8d9570269c06ede1091097c8d3a722108704c21b27116f40e9ec36db11842090913c16c772f48afa449bd2d9173563d67086135b3a47347a0e446f5ed0cd19cc3722c67279210b8ce67782cf0c17802294bf8058528c53b388b26b06c82cb283a86dfb2f62d8c42424259e09e8be705b889af5953416145159130fd61ad1212fb3d06df245e90b5e8f44146236f630a4b631e9707397049b1d913d7ac0b87f24298dece7f2475870c293674114780f0c59646b0b0565e458631b28b8a3084343439d05178d07e1d6afa1045d10a8c726c3545391288f8223d913e4e1c1c463e98a2bc120e653b7a11b43e3b2eb45efb35f835f1ff00673e4657430b51a4c3ec42991d21f23ef96f6637b2efcd65e184f04c659618a362cc867c0b5e078721618f3b0e021b1aac0f09b8c6c3cc4c5ee4e0fa213a3d874416a24b73276a89f424fb6123c4b3a0cb89bc124f93d947b63e9afede91ed6bd21bd68629047a07bc1cf1e27ac555538049057706d25c2f15c6163c31c8e0f165e2a52e082c140a10684ef90c6f11d61f7304265d78261a551b2d6185884c618109a34459b89e0a37886915f44f61f0343f41bbec6f0d890e63934398bbc1b9426cd81372c1aa9ce369f885c0e380c4771bc294b9a5c16220ec23786f1476323c1e5e26131098f1e421a05909e6084c5e0209785cb58b85cd29b621866bcb6250f63d94668b1685eb10d1b37df468fec4dcfa506bf68d470fc46ede26f7043e63c2f8d2e14a539904772198eb261e47843c3085973c367124504f08f80842626213452946c6f31ecfd995e8aca53ec5c372bc5d4c36210d655110b36306cbb36704d882124cc9f7d32c69b4d13db28e239387e0ecea3b63d8b93947e17c6884c5af02404391d618a3cbee1d88585d0a3e4a27435cb687d6f0989899574c4c4c27f67cc7fda15eff00a3f72857ed1fb647afe713ec6a7c0a651b15f05b4381086b3918da41a1126b617924b3b8031b63711363422f42c3735111c0217077f80e84b431d8880db9127e24c4c4f022d83391d618820f0d119e8537bcd8a60fb162d1ab3b32e6f8134f81329f412090fbe2fb9394bd48b7c0b95fb2bdf8f4c5c108f48d62df852d8d0db42e9b3ef06d8ff50aee094b4c927466aff436d023796522c10b83bf1785c9e8747211c3182d8ff12626262783561cb2c620d104350b36c65398b45f024651196589df21755d895ec84c565159b2082710423c550d46b85b1b7e19af068e607a3510e466c3f0c7b311471689bf9378681c445c05b042e0efc19727bf094629c0be498927d8f584c4c4c4c4771bd8b2c63431068e782e46c145c8da961160db10fc61137c04fd885a645e2484c5c1a8d46a32c3763055f2242595283a2a3f98a935c8d50de98a07011ec274eda192668748f818d42683d1e8a12ae48e8efc68b043781ff1131bc2289898b92b289e5e18c668c964c23d7c19504448510e4483669054375b2a091c88a51292462cdfb1b8dcb66c2662c7b166867669825b3b99cc099a2e1b5f914b66f02075f644730feb1fd0167d8470890b3e81bc2163bf37c1d0d83651fe55e0d98d0f9127931887a2e84341b4c3c871d8f6215792266e37eab570ca7032536746e433e0596511c1da35e4fd06cba1be84610ab890fe09313a1639e89728e225bec47020d8243f029b3341e2fe4743946f0a35ecf990ab1989ac2fc1c87c8d9bb1b63c1fe4450f8b468fc07c8b8cb1e5e1a3c710421cdb24cd5a34c3e49ca107ead0e17e98824764f55c8ba46149536082c1b382d98a0b7e081a512f813be9a1c62ed92d8cd9f2c299718a53b3e2128d516e4f6294bd9b3781eda13150d46455e8b51890d2157d86f3fa2abbf431ade17e5f01f14793fc898b06fd884161f22e32c7863c2627553a133805110161a29df2302455d33f49b1a498da1c06da0a4c844425a7670bed9b1e43db3a5c8f4ad8c5d8435191b5d1bb9c04ab66cbe44277f4c4b4838e0503ea0850381dfd1645c1a39b06e6f8d8b6572232129b865454616c6b4e6fa2ea50fe61a31685e31cdd8c48e1f83f35c61e27d8863fca98c405fa454bac3171963c31e10dd08b19219150ed24c64f06c9915a3782ca6bb1e26c0df5d70287ca02d2b85a2482f709e01e3c1c9a1701d951284a63fb03d08f68495e7b26b24307b210941c4d0d5d763ee3114b6f5a25f9468a62686e4a0ddec7d9f6c0b9872c825e2ff1703960687f9c9c133794ec5963cbca2d54e70e76c258442289be01113f78a43dae651a318e6b474c6b9d1f53903ea0511ce87cbd17584e1ab4109a16d1a3be0e620c43f5da16d9a62bd214f9b63a8b431d466ca0d371cce8a38d94899bb12be50ba1216f68fec9b5c0cef0b4997e0fc9783e3b4083fc13084ca612172a486bd8af7062e87863f07e84224208b174de3289fa627f660769645f42aa87ab206f68698ea3fce3e62e3f9141281f66d105a7a14187ddf436cec44d1a1d07b7e51405c8958b2ff63fae89190510f9532bd704918a4fd0d7c0b83859b37a0d99edf07225843fc6b2c6ac97cde4907f8bec448209604b29541b791dcd8ab5cfa64153f434318952b2d765a2342f8620a0ec2ba0871adf0c5dc1a8348849cf6773a371f04d952743a53070e5d21c09c98299231ab1ad9527c142b8ed16bf3c94626ca221a0d13d9fa1340a7c1b52c3a8bc1ec5b96347fd22ff0028f8391c3096837589783fc6b8cf012d92d0d661a262782610ba252d0c8e64170ad60850f6376c465687a655a4ff00b11d4e07234f167d8b9c225be4886b8ecd707684b49b0bf47628268f43d8b0343d1a3ecc394605c8312aca31544bd10b725346317626a6b1ec25b3741084c637c0f62b23dc3c17ae0a177ed4fe03e70318e58bfc03cf11684cf60d6241a2109e062d843e44910b025582d57a15b24152af0f98fc928c357440e23122c697d9cf2d7b262e596a8584db4738c90a34b992227a596ce18c6a361cb6bacce3928214ab14c1b7ee1ee0886a0a6e56cdf705d9c41ac19d0ff43d8d5302a2c7fc231515564dc4125ae44c96c6d39339129e2ff1a1e28dbcd34ecd86861e441ac97434412105105c0b5f2625b10d21ac489fd85cd32bb5c89b619af46b343379614329f0e0f0d18842130135d8b51f44698f934ef9092bc896899aef685b06dd048719c2ce4429dbb453d4d9036dfc1ba353f44d8ba89469fa43d525db7b26da68d46d43e04a3f915503e9d1c4757a10bb3a0bd836d649e3d61e5fe26cb5df04c4cd0c30c41a1a1652222049212c1ed6ce75ed12b182db634cd8ff00621a3b50a07763d2339c3bbaed85cbf826dea1447c8952aa8afa996df87c0f42f02eeb9135bfa2d7c8da0c4ba8ba0dd98320a7b06a6b82130b7d39b391ef08daf62704d50c2e8a8b0cc6df4126cd67086e91dac91651d783cb17e17d0ed0365862c5131a3c2c318994a26279868ca27161b3eb624aafe239d5612c6380c5038691a7f6c9841e1e310b2aa7ac2b4483a8ad765d89fca1c1493f60aa5ecda9126b7b2faec67008d1fd98951b23935b3e7d0f815ab9151c905bf6704a0c551471b34471da36eb8117d842687856908f3fe3bf46a4bb4768878dc186f104b24528f68ee18fdac02e910175bfa2461702e97fa17bc1778a32e31f6d4a957098e21220f44bb5c8c23e047a67586d05e588b673f0477e50e2d71187a37a6225c2c5a58e55e0d091c46fd88f1475ae444dc213441c92549025570e0baf672eb09310c62d146190e89e2f2c5f85285041974262d3e473c7e679a27e0b63148b9129a20725035cc3558d550882cd48a690c8fa8c41363c36f0596f687c2194bfc9d57cc39fa882d3225f9368c5a095d172a915010869896e6690de3828aae4734c43086b43426316f61a3374cf978a5c32fec6fae0db9746886cf10e068af63cb7e6c799f8201c4f43661a08241afe2c6b145b385e0c21bbf427a1109968683148627d68f0dbd8f5ae8dfa94f41a87935fb304d1e05b89fc8f6de8ab406fb154c9160d0c281fa8aaf818b925186159a12d10e5944c90e19a62ed104704b23a3bd91d9221a55bfc0b8d51f78e7934b1ad8a858aa06bf8c3c4f2796317e06e1423ec3b66c70f614c0a2738d659c0b9be1cc42744aa1b943e03dadb37439a3e042cfb1a28f94a8d94426c9835af81f4cd9b0637c12885be4d43d418e5f487ce1a184e8d58e06207c9126990dae19f27f919fb3ec7c989f268d14d918b1ad30acdec6bf13c3c25e6d8f0e3e509e888d0e4741458d64d7f81a18c61b85294b8262672dc32693e50ff0001277c8f8342d7706a7f3f4728f2c52f4140795f225d7b432a1088d91e5810587c0e9cbfc3b1e78630c4a32f71b61582667ef0937cb13b2fb67a886d95e84e244ee13109f918f13c5e20317c1c7c970c43d62ae1680c6d703cc5b472f0a5f00efeaf42c816de4631e59d448d8e41a815f68d514782570dbe0616c3bd38fe05aaf7de1105a186c3a23444f94474594595477d1fb89be44c915ecfb895744cbcb18d6bf141e213c1e202acb8a5294434c7c8d09b6205098807d23424c32f941a984106dac299343504c671cb970aa843f68d7636c61689cc53a2646b9d0f4c842084c6ccc2c9c8b30498e7c990631fe1799963c7ef14fc34e87b425288d886491242a4833ac2f053e5b8ec3e698993128ce01ee05c3251a8c7c1c59bb1a8a29f0483b1cc55f0367fe03924d0e916594d1bd562237f60948675abdf5899166651084fc7fac318c7e6de213c1e86c8703afc8c4343e46b8d5967037b058f170787121ae3b425f2ff0068df25f2a25d1abc0a6ac6c627607c86711e98f22c69a7eca31ea496cd30be8d76df43d37afb379fc046c48533ff00d0762b7f1aff000412c540a1cc96a5452a3134f130bcbf59985c6fc58c7e0b0de678370621a43fceab40dd912ec99f2109e014a538202ab4f5da2c17ed157f40e321b8d8f31b34d28318621b295539904f62ac58550ad9b30e3e3d1d440d46341f0966a85c8924702a9217089f054ae1686a2d8fc1310f5f8de18fc517d79b709725b8e0bfe0afe02da0bfb0f4f1255df951324243502a16bf25de8d85352e3ac0ab362e8b83b9d8c7c0d1c8f4843db22ba1aa18744313d0e5315bae3617f4169136f1de44be188468e6817f80e65ca13f1ef2c597878786ff00109e4358bfe1a7047c58d3f026f92785dabdfa09ab55ec49397d8970536dc14f2bf78850867433479a21c8e7fa438e11765366a171936730a0d97ef09b6c5c1b7f65b7f507e87c8bec5f58e71bafd0dcf821328b8f098df9bc3fc1a5854b4b91ed94ae2ff3a61c8b33f90e10fc48b1aa70d0afef1851b31f2558a3421e3b3e40e47b1363d22ecae72f6d8bace3f43a91f6e6b91a9d9633947c31e304cd6f10b0fce0997e4d7c966e8a51adb74a5f1e04fc2f85cb6363637e5083811c98efa0fc1a67f87e48421ad0d84e8db93b171f8d04789626e43d8ef70871703f151641512b1a742417d03717f236598376763702ec407d329d870cd5be51ca31aad42fca2e11812701c9309e1085f82eca3651e18ca2c243442d2db98ce7d8df82399704ff001410632c5b9788aff2a645951c12b17086ebc31891cff086e81c9d91128c8d1339dfe0e428519ec16fc0b485d9eb44866e9ec82b8c6aad59105d72fd0aa40abf54336390da396d0919aca65294a5294a5f06c6c6f108539c6303f931f99ed65c9abe504105f01276287d1c06848897aff3284303b9e05941f8cf7104854d238f89b29b05a5dbe04cdf25fe7f627f03fa12925465fb1e91b1ad25d21b288f18c674a779048f879b85294a5294de1c18ca91d91e8477430ff1210581af44f008b5895c8c93432bc32584ff00321311c9c60b843b223ecb355c4dadb3dc1f16cbb260d5e32f453da17d9d85d1ec6fcc8a5c314ee9ec0df3509fd44fe847b45452a27da1afa0d1d4f6c7d0867486fb18fb1e2bf9d31cecb28d13fa2a20373061b2a36585297f321144ee99c0835c36e7c31ef8144f452e083ff29ec3fe488cba1b3b1e8fe435dfc2a29110f9134cb0e51f6c7f86f82b2bd97ecbf6cbf657b2fc5aff00869608289ad8e218b9b0d94634144cfd8dfe7a510c28c49895431bbc707f51a054aaccb2eb171113f2ff0043525453e575e85cd8bffa147c0be45728783ff8b4b05848631e29c32518e19bfe02e160b04c8c742bd38fe463642d58b7902af7d7c9c853ab7f66b387a427e5a1eb7de7c8f6ade321d2093b1a25691cab1ffc5412c17122418c83f035496112c50a5ff050b04513c24c353e918f6fc33e82d6f50b7a7c9d6a53f862462652e0ff00e2d2c17624418f0d8f0d8d8d8950d61842ff000d0b25e2c6bc0517ecf19c1cf80d7fe1213f21564831bf03787e07859d1ff82b059410be13098a5f09fe44213f35294a21690172d8d8e8f0c7863434242c546ca5fcef0b04899a51313ff8da5297c961d99a3fe8b968672c41e36c116f9294a7ffda000c03000001110211000010b4f9a92b14f85a80118f0ca3e63fe95154160fa0c1a2d428272e392a447e568bd82d9fd8778c2a483a419db2da5aa833b4f3b1226203ade25c6aa04631368680a926ecb5e33e293ab8d95b1582a3a42b5c305258a738e4a3c8a3e8f1ec42803d0a0693c904893ae781824d2061ae09697091bafe8f8c30434ea2dc4a158ec48142fc13395ef71716cd773e06dafc3e9997ace7aa895ee3182f0de810624d1c70d128001c18f42ade15286cdba912ccc2389e07dda4834bc5204aa722734a40ae30d5245430e08c1c59ab3f341e882d89933ff2b90fbb6cd88c20346a51a51ee1299b40c18dee4c2a025757bf63676664100f48c0959fe93a83ee92ce6954b79724e58abd9a424a4263c8cc91d4401154eae9ad8f39354f2c118db2c14d39b6deac52a54a54ca9531251f58ec3e04e351d0a07e8be811f102400644c1f79157b5424e841905b4e46049125a7f5fbd2140ac30035f7347b6cc9ba81072e61927471a013291462b95007d937f85206611029729a1361122a9098a05154054ac22a3f041f188b06cf3a053120edf2062fe58b840684f2f21289c2ff19f0a619e3a05c08107cb720660343cd73c61585f2407c12c468172560f25c1a6e393ae7cb6e13e319672fa0100a81f312c403d2ee4b631d8180785d8c24114254db260e317f48c8acc011cca20341ab1b42c4ea7db99c764636e81e553e7b6297f687a842762990e9b0804188bc65489b8341829fddddc0432abd26404b5303f88d35863b2791c1788816d88515c7834e3bbac9ea4d428731c673ba443a7ac2a0a8a5da44e305822864c5dbb129695c3bbad98e0da1a43d8d3e8e53100c5fb9bf85e86d5c3a49ea1026790c8829b47001c477dd654f78748c90af60c15fe58045bc1298c9fbde27bdeb07c1bb0bb0182b2e30cb7146ad5a4b985e4ca4a50bad91c41e492f0d19410b15412602506d25f1332a9e4c4708e32e996dd0788e8cda34248c7b78aba600e382b0cb28dcca9571b4de880db57350fb0e248f84362c72d5c5da0039b0c00ee04e08e065955d14dd1d6a09f42527adca290ab08d54d561460954cb46ce4c5a31b0a192311e80a2cfac02850ef9260f2428fd6d2846f09cb14323d1a5420d07199362043621acec5a90f020b0985b8596e4f05314c69098022179e0d12441318cdb3a7a3867c50493a8c9a193607eb8700ce0ae5f54b36588d69d1fe9a1a2f72ae6e74b078e588c4239574b6ff007efce0a16219763a222671b72c8796ef218c25f8cacec904f32a6e9be02a92e136cdcfd26e17c78cccf63ab8404ce7ea8de900162f3552268a4fb08a2ae28aec93b0bf68008320d3309804fac0545222c332c2d610704f25cb2620bf360e452b1f116b8c6411160874d889c1f3c0abc101caba514d4600e1a0028e267be7b2a8d05362ed241c643a09c1fc4c59a55e6c085f32408433575c2b67f8ae6b14c2c02383dbf6eb0c917d7f52837d13880b0efdd394052e6058364cc17f9a6be008d3bbcf41bfb5484d4a8580110d62e8eb086532dc505756c131dab007cb23c97df0f02263dccb4b75f3d14154b5e60d11809c5d0103129bc202285fb4fe6d2ff622430c2004a126f213e85f45f8124185df292433a2c9c81e86a03ccb1f1c06918bd90c36f8020021efa204dde287e105eafe1f2b0c29c39378a2a9a94840ee677ddb2a541c81132a2e112c3820b2aa28d0a161d855218108c122dd8a2d0519643455d155fe4e7adb9321a418425a38f4dfcf1f37e612c810a918de868b331e777fffc4002111010101000300020301010100000000000100111021312041305161408171ffda0008010211013f10599bbb2c78deade56356ceedadd33879042c37b970ed8eb839217dd608cf6de6eeddbe23b227039113396b2f7366d96cc0b37e8e09ce6dfb208f5c7a4c9112d8b0641bc1dd9c16991db3edb47cb14a517c921472b25b27ee7f4e032dde4ee7de0813289931a3b92c61f6bc7564930981932762c832dd8ed6728d87b6b32347a9163f725e158fdac4cc642c610638ce1bce049dc3be02ce0b2cb24bacabc6846f693830b35b48f7c3c85e5da3861111f612c0e484eac3b32b62e8624632073b01e79b4e899fc9921c637b911f124894255996cdc10c964cb2c8389438059d84e1af6d83f76dc7c744241c8ed67524c36fb23b36125d1e473832f83e7127eadf2c3b8817b27122eb3ab20e4361b01ef231792e025b65e1a4678378cbad923d9c1249cfd113de67599ef8ac84431c499d4f68134f819c1a9e31c316d9621f7060c978716db30eb83cbab277c7487e44fc0a921ad81c648780bef11e5a764a2f1b048c45d70b646d5a87f6f1382db33d9d6323d8e29e1e0f1e2188b2c6cf82f1dd78981824924b65dc2f92dbf6d8e59bec02c2c2cb2cb266fef86d14a27da7867d9e3c431db2dca9891ef1cb56179e02cd378cb58840cb6d8648bdd965929b1b3e3ef9747966da5808dfbbd643b32d7967de1bbc1c19b1301b3b38fdf10bc705d564f391d4d9010e7960f39f0cb272ed8e2188c3776cedf7e0fbc32aedfb3809965ffb0b30c1c0973e23ac99670fe0cb20942d3e5dbec457cb0e1dd86c9d5d8eed9cbeece7ef864d620596416420fbbb5bdcf90f38f25d2c441b36cb2ce042670eadbbb2084f97ed8388ead6ee1f5b0a7ac7c3eff10461dcfb2e01bb4711ee5f53d98d8f1ddadadb6db77c9969f2fd901745e65be479047ed6ba6ddd792c7cd9659f16048d8888878bee5b3c6d98cf96e39619fd251c6f1a7ea12dfee0fdc02ce1cbd4ebd9e90dba87a8dcd895fce07cb3e073b93c012caac444f18b0ea024feadea3ed75b48ac1b3cc1922d27b64b2757b1eda248e91149f8b7bbeed67a98f92f39f1316f53ec7038d09636a6431d9ecbaf57561d5adb0eeccead32c64c34b44f59e922ac597646a766217a1a955ac193313f174c7cd378877342225b278c99316fd3275b24584bd4b93d977e10ef2e8b5ec903627a6348718361d757b9c76caf6785998f8e6e05f396ed1ec6191a4abb83b985a1c76630fb18e9e1feeddea1ddfc8e4ef165d598462189d59444e037c93619efdbb7b7e0df7f16d174e01e499c09191a4eee05d122dfa805bac186c85e0843037250591ecbcee61b7a9273ae2d51eb62ecf5033e2c1cbc2c5c2752ce01f8e83617b1ab326ca1310471a3b85e34384db18ebd81bd6d8163527487ea15a4c8f7046ee4a9b203b957cf2deb0f93f0cb23d4c7ce078078cb275213bba162744322d889c7c24907674eb19eef2e1fe4ce7735630ef486cc66195e4cae86ce0b5985afdcfc57e610259cfc079c0cc106c83ac704bcefb2dccb6ee30ac66004b79e5e433e447110f0054ae378de77f01fa83b00ead0b39eb80e6261bf9d98a6421264b658c62d3665dc4f5d17efc4f3b6f1b0969c356db6dbf8c848b048bab5f621213ab660b390d66a6499d71f71d97a8637688c6d3fe4264441b05c749d63030d28f78db6db6dfcc1e25577e059fb38387b3488f1b2c61cc6dfc9f6884b64c97725a62122076b09abfe12bad831f48d9ba7c817ec85dbbcd254e7775d6ff870e89efe19c322805ad8ad39c9086792c9f72c0ecce2ba3989e38d25f67700fd9d33699c9ff20248218ef5ff0002e7b6dd13ce7cb5b0e9dc4fc7c80fb6d4012d62f36575962f336472fd489ebbb7dbec3fe91805948c8956c9fce92478cd60433e2f1dac873e4926cb58c2f5768217b3270dcc203e459a7566937f237ab60b03abc9ec3f513a60b6fe244996fc464d99964f57b14893a3f834be86e8c3a3f91dba5b3a837a64190097c5869c3019979143881b166cd898bb5fc0366c9ddda78071b27cf384bac06b6e96c98f433d0fd0d865f07d12ee3a223af6416c0e8f25e37fc670f6824b323e19c67e01615d3691844a10181d3f5121336f0267b69f7f06fe3de36717aca1df9e7e19ec933381b56cf090fc77f265964cbdc107024e3780bfffc40021110101010003010101010101010100000001001110213141205130614071a1ffda0008010111013f10cb2cfd659c6718176925ba4c16596673967e565c1a6778c8c78021211e1366c886c1d59cecb0976b8761fc7f396fb1f933860b722796fb2cb4e8b5ead200970a48936fe32fab1e91af220fb6410cb7ae524c565b61852321e3f9476c6d8bc470fe422db6db602dec7b2898d84fe43a80e00985373c975b76348d168f05b0f5c3c4f3b0dde0cfc00ca1865bdf78c9643f526595e1e0ab693608f70bd0819476eb093b2db79c38bb4ca30c3cec70efc8b3f82dc2c25ed80593c1ade035b35b26ead96d948096d9b14e92fe5974f523c1ed92477cbb49e9c96196c4e079d887e02ea9f3a8be702eaec948dc2c196dbcae49246f62041c2c9c7f0e02ce03dcf05dd7bdbe433e9926dfc7de6641c67509c42c813d3191bdc749c71fd18473a8226704b2f905a41641c3c87b877112bb10f5c7b9279ce1219c4086585df6c394bfb414c9dd874c17772db099e0216d8b123e716aec1659c61c44f9f86f27a92678de5e270e2ef8cb2211491ea53a83c6a782e929b5b7936d8b786f0eec227ce4be4a21db33cb6c4186d9e279caf5cef1b67c5dcbac5fd5e70b6c02d2db6de0bcbffb6ccd9bc5eb9117ce043a59653756e4b4e679c58f7cb877f0d84b786f0db6db6de482d098cde0cc3753c117c8f22cb1c8adb6db9e4b11cf21cfb943c3cedb6dbc6f390581693167f0f5b3ec739089f392c263cb6ca17e4e2cea330d278f712e46b9cfcef06f1a727ff6c7c963b84e2f97c86b1c889f2fbc0fda9f7788f24e048e3c7b8920ee20975616139c6db6dddab03d9c792db2771ce1e97697718472387906dfd126d64b66499f7e65b165b09c636add8fedd392a6de1e913d596691ecf565dadfc84f01fd84426420e19b4890d38cb27ab2ccbb9c7e535b60eb86659d459239764106a5e0fc0e435fdece64c70cd9f8495ff6f62786d76f70226e38785b1232931bc47bb76f52fc382391cf8b18fecbd702e85eb92702ef26cd2f186f1b66db2c923861966aeef49e9e1f20d97e12c444c4705b38872f0ce02396cebe16dae99ecbff00cb7758e48396c6c397b1b32677c326e85d8709a4f4cb4908e9c37a8365f84bc11f8261af1c378938662f2c7d97a3ebc106cdeed2cee1d4471bb2e113b86d7b21cbdb4773e70d9384d2fb0c78bd97d960ced9d4bc91c91c6c78deed38524fc114cbb0dbcace2cae8cc465f4de2964fe1bc6ce072f410e74c4cead167de58a7b7897487249d46b744b6efe089fce4ec0c3df090c9c6db62407f2bb9fc26d19352c678e6f2f1a4485d3cb0ced95f045b938fb28f227b6df6d906d918b6c2587b6ede7e8fde0db965f83ef0f1bb792e064dcf21e25ff0024e81f617b14efe58edb6c36d75b21c8ce3edff0c8b318c2b50568796af1bfb3f2cf52f3a91636cb7e65fb94121f240eef80ba3651fc5827b3efff002502f424dded118eb87f1adadadaf1d7fbbbd7c8061c08c66ec49018065a1eedde57092c1b2091845e2013b8794b5c5689ea11d5fdabff002d5dc81645c58766db78cff73826ede200c3f0c3d8b171e1d0c657c8d253e6cde0b64c7ce2c8d6db9d37c60d882de5abf4c0326cd2e856f78be4f69b11bd4766ff00e10dbef0eadb6de4f623dcce5200c7f00c06747fb6f7b3fb6849bf82c0b76e0721087ce47863b67a2bc3761a8b26f0bcbfc4fdbac21eefb16dbf93de329dd93d4c7e4bc8b1e319f280e1390a774b0abfb6d07e659e997592c9d9092602e867fb02caf6c430e1612ad0fc8cb39fd9b29c47d3cf14d65f503f8cce0f5d9dbc5c33ecc03ddbb62ceec070e5a7628637f2be359fe012bc26fb79179cb28b089b6db6e70b0ed069fe0ae908d38049d5836fb18078cbb3db2364dbd472891e9c066ad7f617f60fd8385e161fb4865a90b348a5bd821ff155a58bb9933dce024613ecfa21fb6a617d5bad9d33fe59bbff006db7fc23619c0dec916ff9ba13ddee935a261265d93e49a24bec259fe7b6ff00976ba086d9cbdf07f997ab61b784b39d87ff000edb6db7b075299961e3265bffc400291001000202020202020202030101000000010011213141511061718191a120b1c1d130e1f0f140ffda0008010000013f1022a5408102078a98f1512241895108844952a5408102043784a812d14d4c61f994b4aa9a00f994a1bc7cc0fafdc762f82e32d5c56e6465db1521a66aa0a68079bdc5ecaec3515d291e3981c85ea264e39cc7a587278a98056f04417297343154a064addc00227a26331cecdca434b5821aa435c2f328aaa2fd6e22dcd702dc6576df0cc2273dc10d37506386981cbb9cc8b3bc5706cdcbbc30dbbe2508233562e2f112f12a31886001f9847cbea364c46b123bbe259e5f51e0680315f9956299da469c87a9bc7e494b909f31a3842ba9bc3b7310d828de23ef7fe20cd0104a4894fd0c561293c091204104b19748f2418fc44c4e3c90841964af7292b3e5e0b163c31fe4420d88dc60796769ecd44280fa8eb13e50e637d42a6d2fb9ad6d77cc0374df70db247d1019e53d110ba31c26a00a50f198f4ca1dc8174ffeff00728375ea932c685db19a9873ff00bfea2bb8f6a7f70aa8185e48945e78666e2ce81a851578e53101646f7ffb715565e846831b6ec8dacc3fe628c9f07f8940bef874442abcb2a31bbffb80bbbc101ed2c642710626398e00ad9baa37d04a15552a4752bd2084408c5d244c9c10c2c95370a55821555fd43ea447bfa8077045d53e2133fda5d179dcee20b367cc5381255da520b200efe20e55058aa7848cf4ee94ac19f3b78662601e0d1f11d4bc4b972e0c1f0cd466b119bdcfcc13944c8f8e0f05f17fc48cb44aaa10b1003836c44447b316da9d809716e0c55c2a8d638894daf3156d0535168ecf58b9b1627751a88d37bba8bcaece7988d603bc402c0abacb048f46214508d19c7ea6cca4e6f3f99b8057532d2964ce3fea1b2536730d2d68e65a993dd40b589f1999b6c2d5a7fb8315887bf698a8af962acd94d273308c97cd4ab9fdca19663610ef12c9a5f3d44c8453a948f502b0ca9690ee368f817cd43e1515ea151c22504660ce863a989df011176f443bbf88acb525aa28f965199dd033f88cfef886abff002fc432902f4c0b21d0a0f692cb20f470cb723239250c2f188edbcf2400c6e5aaf24be432c92863e46d9884c4782f1f88ea5cb9700e587a309605a5e6658fb4f9782b659b603090b59a17c2e5cb97062b4236c9ca32e4432a411862815b6728df150176e2304b69dc09b0cf172a5323bbc431b715fe8c04acd2bd454765697fd884050e4bb482e86ebddc35d97f3a9d864cd73fee51417471000d8f76db1286d61fb621c85e288a69c7e236c75eb5157547643010b97bed94593f289706bde56296e7d2b54ca012fa6ea0df2fdcb976bf7119e19944c2444836e6a342617304a388db3171a0a94133ceac86e2b10472304a150568fcc31d12847d1322b9d04a3008ccb3131123b7ea54c28f72b4a0f770a34bb9b7e265314e5b6bf706d29f1b991cd9c2ca8b4e704bacdb282837c31d5bfaea15589d258c372446374c85b5f84ca77046532e18348c99254b8829950c7553578d13812f1e6e30c712ec0d42d18c9ccb79f164921c946e6084b5db065cb972e5c713bcc00b345b155cb2f3122d5398026ac85771450330ba53b38fc469aab7897092637dfdc2a53fd7f5308b1ae9c3f4c5bf432954fd42c835b4347b225038effc46a35ecc0b059d2f10176d74e234014375dc0c8e4f440a80bfe91735216d9165f72b54b182214a97b667bcbdc60881be221b16c771afb9a8e4e252c0c7a8a691ea112928820e66941c14b4040c6433e9d4168ced8401b258608e48204aea6dc4a3346e10837ccde77d4132e65ee5a405d39efb867361121628cddaf6ff00a8eabb7b97072ee0ab3e2c696bb7e2092c7df3301cb9171b00862af7302350120a5c9379539a398b9ff103d39a8e02d5d9287b3c90766c34f70186b2996344598a6a9a638be660cbc4b972e30925c044efc2ff00083da1278004940dc02d430964bf72e139cf334a618c80e65b98193f70d1bdf3c4a3ec85bafc61814a63a98395eb09054561721fd9155aac6cc9f70375c99bfc3122876530d0fd8d7fb9628e35735b4d9ab3fc904ad47cff00ec401cabe4ab84d72798dd30f67351615df12a205fbc4a9682d82a14e952deed9ea28e9c1fa98046398f6a8a354ee5ba14be596770ec18ea185d4a19e60873081ccc8f31c9710759e2e90249d87833b7011c8e598817994d1a045d0af7315fe20603085cd56dff001069a4f82582ae2ff3f134bbb58e3fed112aaeed60bcfc5cdbdc611c838866ee0854fcc52aadf7788c034ddac6c0402602ad7303452f269814691a8cfd9511265fd462bd403f587923313311c4c90423cf92d44be461065cb8b0a0be13172e5cb972e11966792dd3a8d65b157830c95a208145acbceb19f64c179b20ee9826ab71563cce23a9615a838ad5efdcc98cbe8cc41b6ef17009c6358db0a08e06f2367fb8b0676861ce8b7ff7030e5d57fb9aada5e25bcd4319d444daef8b616d116ccf698122bd5c5335f0c178e3862715d2ca546fbcd4a5ecd6bd450d4066bb626ca9ea5cb142e27a7c11330951871f5814de19bc616421cdee5c6b342cd21503659601740fa8f0253f313068e90371500b181cb02eef399a01b6662ad796638146de21d2b873195b7e3b98e2e35ea3b6c0370aabf128fb95a8907114ae5ae8066e88d4e8dfb87430aee1327d3dc4bb681897ee0bbf98982e885d24cc5fa57102a3f70505790b94a868fd41399b4c9e2d12830c3c5cb9c48c33610eaaa5e65cb97e2fc066694328b0a0f8abc42cbb66f060901153003fbc85f037e18dc7f73066e7157cfb9a1bf9388caae79f70eadafa6bea33a6e970cbd767bfb8dc215c35c4a674e07bf99b437b6a26bb5d5b06b004ae261304acb9861bfb3b9b516710dbd9dc564c1ea22b54bee61885fee351ae7dc4a377b2e35d47a58b7acfcc5dac9c9730410b7dc59cb1965b9718bcf53ed5bfd26505f3ff0078c1cadcb894184283a979f6981c87b43ae2c50c46c5cc16ac03822a53db44cc16bdbb97c9fa23bb61e2a50cb01dc04c7a8348598dc1788ca188a8517c764fae8415e34f3285645bec882f111e320c4b0bc94fd4242b2c36099996bcc5e9f889dcdbce309584c32e5ca215d5ca6b858a2cc5f17e2e5c5f0c9e3a0a602405a20b8e38b8cc08e10606896aee5b297667dc1672b287fe625a564c64c6542a7f4cd7fe274998ec3b72f119c872babb880d86e38e8aad310351128b84341c45041f9cc128095bdc1d5929ae203c2bea1d180966a1ee525d5bd316d9f445003bddca1107bb837871d12e68c33174c32ae5388879e11a1b965c4b5c2521e21e31be897166e6176c7c4712ff005000dabc12bd4e18670a6c565f2ae78968b1c930304e89aa958ba3b9894575c4da182fa95d184658d1cb1711e1af72f8c51ba946d75b38626d148e862b7153a81da564ed2cb3b233a1c4fd31892aa6de030261261832e66475894b7186a6d39972e5cb972e5c19b40d92846082fe42e89570699446ce18ddc0cde1e252b59c63f31d508641946cc0dd5c6c283a4876675342c1a71e1c4d42982ae88d1caf52a12d87a88af7e65c0a95319973793aee5970fc4750abfd41a51b3b8baa073bcc4aab2b9ad474a8f52db2a455e1efa9521b7c44ac2bea07a271e2a1a63ac3adcca5c2d2cb30eb897ce44d2026480a03480603f647943b5b63ad3966466656af72c71334a28ee02cb0ebdc025c1e5ea7029cc05977f641a6232f1121561475b8d950e07519d533dcad0d1ccfb78450c331bdc4e08fdee50d60f70af958a85519c44cca811530c0f99a218c19718904aac730c7c5cbf172e5f86f305d5cbab1099a3b3881b1066c993037ccc68e7fd47de07f51cd8265ddc6d183ba7f112b11bc07df73baaf50686a10150e0b95dc99c54701cc3740aef44c40a3954bc152dd1823b0aab0474acbbf71e65be650dc525014df7711db11014d8e6e657120ded98ea4560e6056f2466c18b7ccbbdc55a8cecb8b55dfb868af98637c4c96177098b4659a61af72f25ccbca17dcde2c4cd5858e662e4744757dca7894170c56cc41b2b3f32e38c770228a0b27716b580a200b25d6982f5a1ad738965c86a62c1c4351224198748c403229e21e75594dbe232bf3116f5344a30808f8cc62a7d4372f13010c25cbf2258ca5482279597e0f2311052defd4c7d8dfb8df859f714c7d970a8163505901f4430a35aa3d3fb208fd92d0d081de22d9379442c3ccbda6e2ddd456371a0ea1024a344b8a313065de88d0d3f3050022b4303a993a8f288bc3f5716e02a0d74d77172c3da2866c7b8a8168f282bbdf88e6f44cca3518dc5a80ac39b09cb83fb97ad0705cb7fe2003535394b70361c46f818a25b9e61c805bfa8939b97adf99dc12ba2c38398452269712c0de6185b2b8419ca8d4b87b969c2066e9af6cbb701fdcbcce0c4a8caead0b9b7893215982fe65465b2a39659b587153db0c9f51e7b62604430b9ea59cd8511506711364d2244f17a9821172e5cbf02e70bc622463e0212c6659842b6d7f44ab39c48b6b99661fce0e563e196ec0d71798156c40cf6e49a5bc150f38e7e9884549c90c8b3eaa0615861bcc54be62a338855460a6161dba2378c5ed661425055e25ba8228a871cc0a6fff00b2f1143dc761d4ae0235d350f9530b66fee0ba2ff3156e5d955f72929e60a9c9f842bff10cd202f1f88abb56fdc1ae0fa9868b965658f432f72ee5d07e5e08d507731db37ee09740ef510b456f7b9ff44676c0c2a6228d43acb1a9977a98968cb30439c420161658e8ac17b60115330bb67e7225569ab9598abdfa84660e5daa7ec8b8c271c09cc6ea1a96cb4ccdf285cda5b6462d1cf3030448c24e48e184197fc197152ab4f30912540963e3a8622c932d0a88ed2fe65fc6a21ea53b8b92e37c6c9f99220d6586eba8e44105a2fb89bc27be62989d4085b388b79dc006704bd152f8203465f396f9314f73533ee681ff00d8fa913ca826016c8f010623697dae5c8012d171ec582a55e202f1ae2e350aaf4476383fa9c8951bc3d447302b0110cb7ae667f17a9815454270077293b1cd4a818c688b6f9d2abf06d9604ebbc0446de0c0188aaff09931c4b1184d8d7a985919755d45c017a188ee265cf51069a0f6b961db5af987905bb985168f72df8b28f925191c1631cbc0f0c2070345cc0cc8e02e22d05cfb09619b8ae9970bdc38fc448912a6d14bf172e0f8f52bc40c7853c2de0aa0d45333a111db1706c6674a373117609ee452c580526ba95297114a1fd25eee9d9a94f386fa800aa20ea5d0ba6c8e84e4814157552b55e5dcb65fc4bd96abdc136dc017cca2f7a8e26a08dc2b54b5d6d1952e65a84044a3472caf4bdb3b2fd4178acc54ae7dcc6be3f53732e356cba5203d4428ceeac36115d026669ec12a64e6ca8808aa3915513ccb6dff00a86a67dcb1b12cd1286461055fa5436cc175338565a089464f3f0f7315d8e89c496d5dc10e6fb465265998ce57085c8cb83ee73dc10d284d6fc216c2a6d1c0a2ab1709445f72efcea1889949abd62377ab8b18ea3ce770fe2951224dc84b832e5cb832efc3920b850bc01e083062953c4b1846bd663bb211c4bcd46e6a3ee16c90fc10bc0682607326b2c5e207367903fca5ca545260a14c2e55c7ba5a960e706c68897b9673c45c8a38db16b28deef8992a15c773840f3dce08b4f706897bdfb82bb8733f700aff00b41d22ba66c02e4ac4d047779bd41b562b2caf819cbc235be199fccc8355ee665b865410c2d814088e037ea1029e848058872b011db62f2183e2514686fdc96a4b2aee2a34ca02ed6b3a8ab6ccaa896148ca5357632cf9a10b7a953e913685d7861b85540a581c972cf52308bfdc4c8dfea3ff00d9a15b8639dd4a8904d8f99717c5cb97060cb8ea6d0c3866f38f17060ccca0d47ee062e184948d465309485770622d4b1b8e3b8c94afe66b0d999413e2256ee5d8e98c96dc1ccc55da5b814671058d5c2a69cc6b65b8cb966566102c06d95e16e5d43587a769f18a79866850604c2c6e3f1409bda65cb3372c7506d96241acff00da5a80506823b4c172abee66181ea1345a3e2181fccb37820b5b63a8ed4be6a51f296145265e257151f4afe0955a8d1fe6194d2d1a98f0907340db397713703925c22ea66df5e291bf883f22207e6118ee28b4285d4b468a17532a982e2a606e64744ba325dc403dcd10621078853c2c1972cf03060f87c349bc1b3c2cb87b41b90dba9811cd6552a7708eda8a1c308c216171118ac5406a5135b99e1a208f426e7b8db91b924c46dee314b62f45c0cbfb271440dcbcc6d6dcb286ead7ea0768bc72674174711d8666ac2e37d28370cb199d90882cb2e02a883635ee1b880811f896382548d0cfd01751973fea27843ff2443ed9cb02d258e1d1f317dbefe9f11d2dbfa8b9a63bdb32da5b88eb14c7dcac0c0f11ae6020ad6463e1ee1002f1c4fdcf0231f0a1b832f8970c4597996116d80bb3b8b108ea62a2ccba17c9708183065c669349b334bee2cb97e0bda1c9f718e428ea282adcc31977121bd9b3c13986ae0cb50c5de105dc2e2e5ebf704eaab44055ee31c1a656752f198f56bb465b35f3099114b3e10803433ee2ddc12d4592dcc172713006f4c502b134ac44c2f306155e03e22cad8f4cfb99b9ceaa285536f52eb1b95af29cab98e50ae7ee3a12ba34473bd09a23347baa0e7825916268adcaad3ee5873deae152e1ea3a766a1401a9fb8209b7303e71a8f6d4b505456fee10bebc262e3e0e91dc6acf519a600f494eb308e33408475e2c195151e93de0ef32d2e2985a0c20f86691e5f9891a96e27bc629c1379f5f03a2f896b33993c35385678e258c90cd9b8970a459958d412e5980196306ae257ff6672d786a85fe2232be44abda62b18d5c3d660591586c84b5584555c1d5d627c3445d5c3d62622f5e00d6a02a18d84a7b841b4abefd410857a94ff00ff98ce60d1402df15b62527f7b16ee9b6e0fcd464a97a95058e0b6ae20d948ee001bacc73f3600daf4868e8e07a621799218838852ad7db0f0f877062fdd40a198b0c7568db5e236614ed533f845e004e1266b74450e651063c6ee0c2c8e152cdc5e2e69349bbf336b8cac5e132998e3a8955c751e23b855334aa90a4f4c134f078d43312ea5ce208e22bc4232588d32e4d4a30d9d303aaa8aeac1f72c0be0e25b423835a6712b110711c60a9cc334e5a864eda6572dfa96ab5f44c58f8f87243c07498efc0a71e29c557d4c623d32a56d68deca585ae5ced8b9045db9ffa46d9a0bcbccb5b29c5ce3b9b26782678e3a974c06f371ee6530c4bc1157de5a7c5c0501dc40bcf6cbe28bdcb2f71b27dc3c3e041c97067e3161944ea176d4fa999f7cc5acea62806a7733dbe17bbe0a21204dc416d7c9328e511359f1714589ba66475e0f2c60d4561d3280ddc7e0724a591c58995b0cb48411398b4c252e17ee586221d914c88332d5a9bdc20ba1bee188047dc7043eea06f07e6666a612fc24196defa8cb227481ea3640d5471d609671884d2a639948e5b8bee5b208a4ce239ca8e0047694ffb84ed8f7fa89a0fa0a88cb31fee3aeefe65f981998d93f38caadb324b3085988ef28066f30c06f70a036a1fdca51e36c4cb6b6577a40349be61fc4ea52cde6f50aadcd5f89c0b9f4472a635015ea61ea26252f94547da30c6f8c4f64ed9569978b234763b80cbcbc43d5cf8669065814450f4c42f644ea95dc599b6d91665807b973285982c9a9368860c7a967880f3372bc0594cb638172c4cb64a5f52e60837ea1027ea1a1e2042d10eb50acc4452e55ea52501a97ee38cf6fe22cb3e89ecb9c0204ece8dc46ff36599f0cf6e58e5a9963ccbce276b2cc1b664ce3af0394b348a460937dd4a29701d3388f2d9712846d504d4d12d298ee1b84bfbf2ad55c2131730087ed34d6ddcb5fc4309d93d91f68b9f2f0b8b0ef092928943b941b8f6c584238dcc10482b437cc38423009b8b3165cb474c70d9a62255e23f16b353e3c5afe62c09457b8144bc543533d4c1c3e270c90b423d128e184351bbb2152988a8a2dcb8e26d10731bf0b15dd8116fb650e88f49d12837b94f3f888b828f98b731f72e004c78dbea73744b88992233ccd8f6135d12aea2b5c04b0a996061547846288b57c3f30ba51116d17d3737621b8783388c6108b5798a4ad80cc4cda23a298abe28ccbf0b972e2dc20505cc2fcc19dc1259355c2000971f641152c4b0e6710620b20ccd22c598486b187b820e325c7ee297643fd4150607cc5112879944173fdcb27c45dcb398c30cbde3c4111e103d40ff00e67589c54b71fe25bd32dde0896ff195e0fa25baafcb3b13ea3dd62fafcb1ed15e2385d60e62f0cb336ea2b14e8abbd4b826d9ab6d9654d41b26e05dc485c6c0802a5099a608b51a2464c005ab256436e12508c827fefa869ba096db8e3dc2e325b2fd30608368e9fc41cbcc1c43c3e0960835c447cc4198d66584d3db1d91d42c712e5c22e32e0b08552ec46ab232e6a69cc055399a830c406a6ca210c3327830dc3537798948b8b1141a25c7e266466109b884b095ecd26a234fca1304ab52dc20fb8741f3177fd413ff0053803f10ea7e21dd882cff00052fd2fa96e81ed63ca5f04ab6eef8c4e56df2b960600f8221b1f9817198e58466b1db3d260ce225ee608b294e987b334998deea5396663f00a9a8e92949c2a9a85784a0f8656aa659ee54454a0ad22413a2e6686db661b9df50cf49b478951614898d59bcdf0dbc1e1f25bea1363a255779952c068bf50e466a1cc00a067997bf85c597065c1f27cd2b6377005ad78e0a10c12c2a50c1e3e589855743cccdd1af7060c414ea5f7d9066e50207569689d8641a9cc8cbaea136ae21a973981e63a5fb9ed901de10a4d5b8accb9d8ac4767d13fd819c2a228cb9672877cc3c602508a465cb30cab949c22bb5978ccb331ab24424368441ccbb460ee200e312a95c0a66ccbf0e4530ef0e6519198fac2e67d06538fa86016dd105bc0cac161825928e66f377c4e5f3087914b59077086e3a26a55aac10219e750a0878005b29ca99730dc6bcdcbf05f1e0651e0a885df84305dcda2ca0e2334825b1c8c60ca6ecc450619816564c4c8bf994291d18e8f0ea2fdd3a0197eb778222594210e276806c09ea2c3518c865881b87643b62be65d8bd446e29a2e3f3335d428ca9f72cee2505cc3730788ee654fde39c6bd4c0b2611031013d8a848337370a0e7f507e04f98006a62cf486659c750b5d3284d2d4cc2f1446056bd4b594d4565e619866cf120d7dce6538fb96d710789c983eaccc4965b8473d4d24f94be178f0b97e4664dd417194c8907cc6499f72bd4a4e57371154318ebc078818d5961c186186e1869e2213650a52c29a4301d389d65b22d6e3a7e60b28916faa12e3d924360d9d935892b546a94ea1ca01204f095801284a7701cc0f301af12bcc234b2a60a893286989866f2ac1e1997a097b80fa44e869b58cf30e664035af09f125389a89a51784571ea52a4f221a9a06557eea075346d969514623d4ee2c16ca581b86de23e2e5d6f3308a184f70ed80189762d3f12c772e55c4b972fc5c58b8830652cb1583143c1cd130d92d1865d971f060f01150c329198473355c9295c4dd8949e42e3b3a66722f7324750849f9be637711043f06a705668109d9e151dc3b653cc4785e84ed33a2fe631006d8a4a77024c155058da83061861516c172ac7005e5752e29c407bb9843a82445588d3217284d61f988bfba51338b4b5f5a4cb5ae3299ddea0e533b597cd86222db2af305511665e1f125c18f826009a8c1abb8e89c935cc56acb97fcae0cb9707c0843986612c335f50022501a95446318ebc348b3301d406159bdcda5230a91c4a9710fb4c7c21a0e18cdf7a9431bb1f9866b01ee0412379a6265a4a179bd13018f31b2e503dc50e912c2532ad2c3b588e0b889d15fcced2769295a8da19b2186d29dcc61eb1090af0104d149b5351a0212ec966d7f119d2fee13bb7a851d9ee271a5b3dc1140892d2c1e5ab97335a8c146d6720a06e313d92ec1082538ac4a906d89501cce7825665a711d959b8007ee64f170437e1638e7c556e0b85718a2fa2654b2cc0fe4e3c2f8197062973287cc31c8f571742410d7d466e9abc336f07519a41ae2568e9c468d6ee6424c3a8f30bdc11f0225b1a0cab1129a46679a431cf7002192c62dd522330034cbe528ee1d602ab1045e0984987ccdc237dd3589578817825175296348d0d1adc051550e253877019cdf0453203f0ac4473f4655cdeba962a8b8d57e16933e6c2ccc4ea3d4e1e6588a0991dc5704a2841c792540d2e0002c4b2c12dc5d4f82a4ad983552e5ed99935c05cac065dbd4135cee200dd69132a59887c36cbcc0adf7e1bab86e393ea1bcf877186e2ca0dba22e6207bb869a3131dcbcc62d41fc2ffe0a237718d32b6bf27805118a97b9a3c336f075e1a46e5528ff0028374ea7141b3d305908a92a51fcc309815f294c9c4027b084f5b2467707d37508e241f44b62834b4cb65199f5a8a8592e68088c74364c4094787c254b505c7c62d87b96c2994ac91d448645ea3f60508b939b4b4a24c9cfa9c68b87dc711b275a8753b226d8f786632bccb93ccb83c11ecf700a956970d98e5f31c22be6113bd90a8987072c5256ce6521b30968cad5c4aa6d54c75000454c5d60ee342ab88adcc458506304bb62db08b10975e36c233085b99408af4cd0208737044cff001c5783f85cc932cd8b80c590b0c2b88c797f81b783af0d25b7e1cae66d1a0cad786218e4945d226b8c6e6acf5110646982c961b31aa4f531d148974bc5c000ec8741c0bcde30b2fd7c47d8e61d970a569750c39f292a2d2f73e268d78d37085344c2a4e65739a771cab431531da55292c8ee9850e98d943883478c4521485132241784fdc50d05a671122a28723b232829786333cb005c7530b88372f7584c92eb387491c1bafab8a0ae5cd46414369b5a00fcc081c6a2b632cb6c41aa95352b30cb16370f065a8cb86e2f1362dea5a47ac17056f5b9c50544b8ff000597fc5dc22638a8f2886d172f73595182691d466b2a0454dc28769548d0a789b5fc407c5dc45c4557174929850b5dc31caa59f3137fa49a0505fd44903e143b06306cadac089ccb2e6480adc16a2236ac34ae8963b979216edc7498d4ade625148fe62819ac9d45043da13fa8edfdc78b30dbc7642ec8464ba2cbfe45b00844615c4a413986548387b980ac65f6970ab50f485ca70dc9a73091ce5e25af4ca0043811592d9c2f5165d144400e38229dc30f99590ea182e2cca5671e15b0d4f8f18bcc264cb2db8c660a8c271b66854ad61a6318ff0005f1f84657127319959f129986a396186df06e50737705c79b4f15336d8ee0a8f868c0cf9cf6e66d5e0ac323040dd6656046d7115077d4cdb954945af24e3125a032378596aa286371b74359c3349baa42350dba8bf6432c6a25be18234da427b16cca63027b920ee1646f67501d748a7dcb54064359ef062a8ffb2050a347895cb574cc4d94c0892e5d883832fa6511ca4ae9618f66d84e3a7109d74882bd1338b00dea219ffb66bc00602310615731d44efea74940976ce212f11df9b86bc57319cc7bae660605efb3c5bbc63312279633e32fd4bf505a119d24328fc45ea66f003180ab0cb151a91cbee216c9f308554bdee02c15b875bb618c349e475122eb11834dc107e504ef962ac4406e89603364441002308cace0c9d93606e18ec81c5f300358284041b2968f65263941c26b70a1204113198a90cac6b7dcdb030fa96102af72e21b032c7b4b50b0ea5ad8e18322d9472788060d42c48e42eb8634a563b2062e70888b3092d1c5ea12c1ee19b1557057ca292946125ab65cba7e1294ec41fa4019e1a3b9c5c0304480e38892ae0531dc0ccd688e30796738f1cf92a2ab7b976d737e4cb344bdd19717364cde36123e4c12cabea5ca44cd0253dcc9d43299e56a4cf0c1b69f883ea2ad0404f1e5ebd4b221197210cd4b64383fda6985d391fb8ea26a2819135c414f8a69c31fc25e62fd6a655c305c1712384310d7ce6539c788c0b1f38cd47794987a9411b2ec65546c625535dec972a6473281a12f150e01798c216f994f734517d45b19911ad46356d0215e25a98dada8ad96098d37505aa61d3d46624421877c2401375b8db17c194e8cdfb23a841ae8c4bc81909f6b8c3c732830bcf2c8bc0fdcba29d08ff3896ca881128c40ee31772a9f2c0f3ccbb61302bd4bcc2eee3fca0051e014a52f9b9f2ec30c244833923f3107a8a1882844e6e71999b10095ccabb065b7aca046ab9a227d34114347a0ab8c98337ea2b8ced6b8792175d4c5e98c5a5b4228606e3a0f431834ccb7316210cad080052910891821100d3925c812a0f7004e1acf271081abb21fe3e5f864e2110e16562ab36451db5310f530261843fb97c72144f5dc260e618026f8d5306e20cca293d4729893355aa940f73325f295348f275752884eaf045a1788f807a4542f589bc73a86d7c4cdcd66e2ad65733953fa4df9341125de0180a8225590d4e257783c52773798cdc3f8ed15b2aa3d25c0972e6399652315939ff3e718546688fbcc8896916f12fd4589154941f11a5998a7b184a42d6e15e6b66705f8e44731a8629ae92813206d7106f2971a95840f1c93784120ce5fa869f38acb34cca28513e4962352c8acca27280881d46653869d402450ccb1a2eebe65d59da7a85d90b260bc3572a35dcfcc205dac4320b2b324095b6a008c01332dabc222849a232fd4671351dc1ccddf04cc0c19f9950c0b1fa4cfb53ccc3eafa869a682a080c4b04cfd06b1b9aa282ea223848f32e6a825225adb148b9956c5f9813447f502d958812f2b8b0637e2e28471e38f232e3adca8b85bfc232e2ab5304c34c44784df0beb3b3c1476c088de27743a897711cd4ae016c198b76820fe0235f151ea3a8d6f0f06f51e57febf0168f98ce2f8c1d929940777c4bc55add35704f08bc3ff00bdca19cc1961061e0fbd5434b55af999e11296226929207cb44c24a6cf528a18e220855e443c0c0c7cc4589187101e21a52b157ee006ef7ccbc15c4aa9e46134eb91dc300898d4d23112c4849ee70aea0c9d198e5a52ae1a70f09c47127ce08eee2d8ea3e8cdc568f7334fc32f965689bdc748e546388f57e0971c07ee3afe894c108979833e6ab379ebc1df8dbc98f7e1f171654a2845bd4c025448a2c418520ae6681236c3e66071066aa08071066982598f24e897062372d41518482c2944986a2b4597131d2807f2641d09870ca674c16f10d4651c29fa98f49f12e1ac9fb221a890d32e7da4cfc089324f24043f24bd948c5a1a7d431b1d7da5014a1146b586579d3fb96166988894f4f703280623f9793ea585a5b93a890ad5938822512260fc1fed0c9401aed089462800f08ed2a3a94e2555f49a418a9fde522bd92b72cfea2dd5a209c910b63ee68f36e6cd3b45066cb1ac7f79f707286820ac4fc407a8199808b1dd4631d466d84a8efc31847712a670458fb8e047dc40721d4bcd950310972e5bb970ab85c9335cbfb842f04bc0a658f67a9720216b23390c38d74d9b943d9b951c02e1a1cb2cd224688e8423d90aa20ca252fb19830ca991dc18c20a62a2b96986340d19d41005349e4bc2e08ae88e10aac8749fda65c3c8615727b84690b2f1d421f2294198467265a82494dfcc6625232e3f607a9a5af21c312a1c711aa440a2f88b105219aa78611b0f4d46f03ce789683b6de61021467db30c78333de08498b714016aca6215cb2d8b3414a7246604e620d3889db303d0433a8c19dc70c40bda61f12b989dce7e63348ebe62d45fc0f86397c07733a99e2288b884198492f863b45a14caa7f83af0d186a006bf701aff0011388b8ba837925e713b8ccb282a8257dced307c4428358426f8cc7cc123c95fb230ed05417b4fc8921ca09a85d901285195dfbf0b944d15ecff0050c871265b73ee5c0aca0a23d405744a5529e88ec27e7a0844b4ff00c820ef27b250643b9930f8861a96891ee13455ddcc89398a039d95a964ddcc83912cca38d18691884c426d0d2b8753b21be37700777b8d1515a231f833a8db56fa4a576fd42eb65dac1f76c768689bdb706e88d1af52d419ca61b956cc106b504e097ea2e26dcc7398dcca6971639614254f7388c607fdcab612af11756a0b7b8a6f1441710c0df8b3433c31a89f0c4a8f87c6a2e16f3043bb86dfea51825c2bbc4c1194310622f021d27310e059dc607ee586ca4723a63e62cf5307b36863cf64a12dc6cf4c1e1d08ecf74fd4b85c2914c393a60f9a7f318c07b5a8e89f8e084cf6016fe618e61f17227dbff5379692a610a630dfe313e1540a8d91c6243d99f180530214d621a965c8c1b47cc4c4c7610770b253617b6679c616df6455362c9af17ec957816ee29611f3374c94e497e583a27a66313ed1565b960ea4bd07e516f55f532f7515bc11b3ad4ab9544a6b89528e67e11311289a4d3e60b626271e586732a8f704c2ed8bd118a0431b88188199c6a1029bf1579233b31c328f3a4ca31c05c3dc1ad30ee433a82ea1493b25ea2ee29a680b62d6368152fcc7a0d76ca42ef2cd8c69757994639798d972b23dc77f7192589d914ac54c0c5691a1645225e5a9c1e6144057a98176fa311e90cc00fb984d6263f3fe7c44a1b23f7087b82ea3ac67d4a5e461a6d90f425b1957860ed0f867094fca6516b04b631ec653ff00221d940ed2c7105c116b7a83bcc57730d171384c16c01bea68a23f1072d7539252ca89c4c5cdcc5ee18e2397339f0751473304a8918ea396a7f44af1cc56dc5f708d03f31997f4cbf551475198e5261053381e627c44188527e51fa6719e022a809b403cd43394fb99777716f5ae6392b8999025c2a38a4b74917605c45757412e9ae23a897a6582cf645fe65a28fcb167135865bbcc072221ca1e859f01bf98b85ce61908012e05e4980c0fdc0429436f0594b5f825a712f8003a85b8625592b56a5752c948bb64e0353a911f0c7818873e23d63e92ce72e823ac0ac5b01348e20cec0e217e1476cb56da472368ae2660b67c654256622eaf2c61c112e2623888ea556f702216e23d544e23bf0fde3d7e61152a3122e26b1f9803fa963bf0b971dc07444ef99b59136e4585482c22524ca5b895fecd4749b9459dc55c40bccd914ac5b2ef9f3bddc10cb7710e60a193ca6181691fee5f6e581cf30658f30c5504ee5f8eea5964aac90d074cf7407f50139309aa3136bdc7c35b8b8d848b141d2f1ea2b4503954c3aa20ebfb227836f89926196cb2b3006a6e57885fb546fc54c1927aa216fd41cd94d6bf28bc692b40150bc286616295f51acda04d012a60cce1512e00ee7a1281ee594624bbe13d47f70dcf8267c68c4bb8e16fd4c23bbfc42303c318e88e33cba8191ffb89ba3172fdf99941b197490ed18b0b3c09982c1266693923c406f6472c391d474dcba65d6ce618c186a32fdc2d659e22652e62322f996c2b4a2ee63710d32b1e34c896951e273fb8ad1ee053160eee1d1cd888d69c7f44b35fd6760fa8b170f880e010a94bcba87cb796560932075dfd45426a0ce3c1874d3354c10f6d44b8cd3130550f48d70981544cf4129eebea3ed085a331c4b3d25246237ebc5c166e98db1709c41e0b98c3735985259cc66b994af513a8fea577ff00c852247c15114db0436e515dacb972e5cbf37e14e1f073613b0b84d5b3099c447cd93690e48f454aaca5cd206232ed962424ef81a1f982d96c488b51e1853f7309ea0c312dc79b65445ae1f22d460d971527a628456e48045ad0971601ae06e0aaafc47ec509547d92a8874d01fee6783795a12c8b032b6e4e88a4ed526cba65461234cb5cd2586da951ca3dc1c41697b847c20833980566e6d8b9aa961399bc6595fa82dc05e2e530d5b47dc7ba995d51d5c56782bd4bc54282625d0b06cba88ddcc11cfa26d7fa8728998ee31458c1c763f5195adbfcee5f8b96132232f1aebe2ae96b3002c1b11311240cd43a1f691504830a43e9167738bb94b129b78e24879049b20cb70d17f715c06b88812d182c9922862382180c305d25bd84b53b6b5303e1896500368f4546c6a6802076c36a707ccc78d3a6277ab83006a2f3d18a4444e233dc42602c7a751c512e6884a274ff00d2e39cb60dd1f70865bf894707db04c2eb5532a869e4fb89140b11b88547b430ee32e0cb75d7ee51cca1d4aa9d1a77e1a1dddf44a5df2f313a81ff00444ea3f11c712c6eb3c41348272be38888d3a94151f7f8f1d98c59b84253f30acb9e588b2e5cb972e5cbfe2c182929fe0629b0e2a266e5d474665a09449936affb86d867af27a4da186e5ec444666466c26997c46ccfc46122736d4cb717be1fb8e822c93050c77086b44b03a9be644c52da08aaa68ccc7f699c739b2a7104e0f270771db13838233ca4ee5fb2630df35a8a6be43fc469d42eda610032e4e98c51340151719c476db2a0c6fd7e67b7ee16ff00dcf97e20bb7db0a9202f10af6c83577052f8a834cc8457102f7f0432c71d3144c128ad4d7a655e663926669c469db2958898839f0732b371125c5c3823c04f7cf737a815b8b16543188255545b8abfb46ecb87f2b97fc1843c57873d25e9d6c80554fce0056ae2085ccf5b95e2e5cb85228c64662306691d43a937474cc58ed3242729b7f089217350d698a573a01039c72e0c436e5d3376b1d36e8dc575f505ee56fc731d3d683a38217965175351d964a731d68c27641718a4b7ec4277931e98cadad0f502d56aace9ff00c43a53f511e8f987d2fb60c65095eeced0d730076dd6a2066bc07399cfa8c25906cc41c6095d0dcd4cf4a8fb25838226aebe0269f32bee303656b92346e6710388398e1bb966a5f8e256260d4ba9b9545b2b2885aacf533a71d46dfc6e5cbfe172fc35f70f07875632cabfdb3322aba8a2171841712fc2ff0085c2228ee632bb6bfc4f6be4c96101b1f7104567a828d03958f5acdc16b2c572c195f70e255a20cca734d12b63986e15e212d8a2a58d114e6150232fd21310ebd92a6611c9fa94fac115e71513100a6db3ff00114553ef7dcf6fa111c27db2df444df41a6215c2a5af71156e0ae7883897717b850751252ee0a66737701bc469cefdc319864dfd437194dadd9f11d5c636bb3dcd331d51308faf04d6e2c580b1a3e26ecd7b96ab7db822856d8be1151066e519bbfe372e5ff13c041ba25ba98c6ad7d522b7c4d4fe777e05e230c08a472304097ac71f7182035c3632805065dca21621f5a660c7dcd67310848b1728bab985150e504402a9e94fd781403aee11b0285376cb443f33e1dd441c66a5e5bdca43aa983d819851e99ebc537e5f44a0e081e5fd4be8bf30002c98c730535517d455c04bc41b65c16c87ca6285d9983786257330a1e660e65fb97598c375d45f72835730627ba60ea2e7b8b3e65f509649437007b85de6e097e0d748eb2fc1b986882bdc03ba94de8f8bff0083355c4084923c8351d4621bb9a262b036460dff00f10631cc28ee6a76710dcb271178ce0ba3dbe21063295a25b2d5a8e38848cc97530531a1c3dcc8ea6c1f8cc7ea60d2a44acd46c5c55d32c17230d11b333d2e3f4c30ae8997c152a3d899ce87ff007e22555bd40070fcc1744b748ab51146e2452506299643d1968e6cf08c4d6ee6d6c1d4bccc12f3cca3b8a577f11732cb9b22937502b71c3044f316a2df310663d49936c33a95b6e6012e463e66c31f6c5554ac47f82cc469e243bfcea5409af801c100ebf318412b58a8260ba946bfe2b97062cab1d30a59ec4d3c050782ea0b8663b8f62accc1d4946c8ee406a84a65fc03565c42980050ea22c91aef0699516b353a6a55f042fd8304c39aa661ce5a872806199e3d8b12a0be1bd465c57170b6995b50e25c83b36e58876ab6e570be698757f7a80d889ea52602730b389d9082295b9ecdca771f667b0c7a62317dbfb8b17ee7b274e63fc4582600dcac3f0fc11d60af6c5ad17e623165ff0017f2118399431b51f713c54a9508496c470282db3d41add116980b44b8fca57dc19d4b9a95b30d5767fc8302b1d660d8635c2648ae3a896cbc96cd568454cd9282809ea1234626cacc926e9b63ea32e59c73307d22470882dea56572c7e509d3f315999cad8cd1010447dcbfe43dc523c0112e0c887315b53e183a03db5316dbee1762fc3060c3da1ed3e72f2d1f69f289f996e08be91bee2113c417b63b6936c7e0cc33fb99885874467988cbff00818c3463a9dd0aaf485e0d35e243338c94fb4640256ee0770f725182b8dc24ca134ca25f3ff21154e1629ad5e7b85aa7c744dc0cc06275abfcc20c652c682a3a3d12961419905c31e7c6cae3dd135b17632c156860cfd912980c5656e582375e8c4b8ab12578b482229cc66d4f860b481ec87fea66d1f44d4fd989a0fcd07d37dcf712bd93dc7e623b0fb9b01fb9befa182d27ea1763f311a3ed9d47e22794fcb11e62a5a5ff001bff0082aa552dc3a8018666087e25eb32a22c13645b074dc7de2dccb1abb828ebb896c0562a5206aa327fc679298411f8188fc23730a06489a1c100ea3a50a3b856d68db2ba75157774cd81f507e4fb40783f2b32ad11616e1315defe5040e484555987b3ecd9107499c1a6337805c46af6373789e6a3e6d9783ee0fb83ee1d900e5f9877ff0033ff00bd3dbfcc5397e63d917dc5f714cb3ccf94b978ff009aa54abc47b8315123595b96d7f188313dd1e12f8b9da59f332f9b4d33035dc1e66cb213cdff00c24327c41878162298d9068c445646372e1012c239ac3b4a251a7ee3227139e0e6356a30ea3ba2bc04291f827253d3a81422588050e7b669ee6e32de66bbc75fed2c64b766988aa1f71006ca41e22e04113c279a89fced96cb65cb65ff00c77ff0d7bf216cb3153247782138dc4dd45645533b8fb44287ea2ab2a26e6d324430272c1f71a98636dc0b771f3c5ffc24f69717136f3295d44caad692ade8a01aad110e76318a8f6258cf9e6d59fb833e7d99802ee4eae0a57b84cc5c1382a5e109c843cc695ba10a9572b2a175cca6af60b9b03d5213696e5b26d8e544892bca78af15e6bffcb52a04a952a543c1be2b3fb9521f72878298731731f896b86732e44a63cf8c16032b718b972e12e3ff0021f86b05777b8a42b628f8b005577af51968b7752d1f682ee6d9e65c96a0f5d901b308cb41adc42c6bb8a32a99a66be0ae3e12244952a544f352a57f0aff009ea54a812b12a54a952a649731cb1882808b305c70df8ddcba7c76732a9929999af0065a1f98b897fc6ff89e6bc1e1bc104086d3e6542172e6510d44482c41953e0f465f57057865ed93600bf9ee5895e2fc04af292a54a951224afe15ff00354af257f30f016cb2380106922f7326217cb1cdc46665d461ea266e1f7f52d30ea1b8999bc78fea176d9881e48af5a97fcaa57f13c93683c5ee0d56627800f8a951f15882c1772dfc55e12279ab952a31f092a54a8ff1a95fc054a952a54afe15e312c94f1208ce5e2a7ce4708b77ea5e6e6494171a6e0be224a6a7ce3b8465c4cb9f0641f51bb8654b34a84b972e5c3cf17e2fc1960136843349511c215f64ba841197c364a951224afe2314af172ae547c32e5c6279a951254aff008ebf89f22e5cbf17e1b90805be208188befc2c65873dc5b239c47d9a94bf889e8b9430e614b9dca611b6653d4aca2d16730570c27fffd9);
INSERT INTO `student_accomp` (`student_accompid`, `student_accom_id`, `lastname`, `firstname`, `english_name`, `gender`, `age`, `numberof_weeks`, `email`, `mobile_no`, `student_no`, `course`, `dob`, `nationality`, `passport_no`, `p_expiry_date`, `p_placeof_issue`, `p_dateof_issue`, `visa_expiry`, `ssp_validity`, `address_abroad`, `nameof_guardian`, `photo`) VALUES
(2, 2, 'sadf', 'sdf', 'asdf', 'female', 127, 0.00, '', '', '', '', '0000-00-00', '', 'asdf', '2012-02-15', 'Test place issue', '0000-00-00', '2012-02-09', '2012-02-01', '', '', ''),
(3, 3, 'test', 'test2', 'test3', 'female', 0, 0.00, '', '', '', '', '0000-00-00', '', 'asdf', '2012-02-01', '', '0000-00-00', '2012-02-03', '2012-02-11', '', '', ''),
(4, 4, 'Please', 'Test', NULL, 'male', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', '0000-00-00', NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, ''),
(5, 5, 'Test', 'This is', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', 'asf', '0000-00-00', NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, ''),
(6, 6, '', '', 's', 'female', 0, 0.00, '', '', '', '', '0000-00-00', '', 'sd1', '2012-03-07', '', '0000-00-00', '2012-03-09', '2012-03-06', '', '', ''),
(7, 7, 'f', 'd', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '14523', '0000-00-00', NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, ''),
(8, 8, '', '', 'Rombert', 'female', 0, 0.00, '', '', '', '', '0000-00-00', '', '5555', '2012-05-22', '', '0000-00-00', '2012-05-24', '2012-05-09', '', '', ''),
(9, 9, 'r', 's', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '12', '0000-00-00', NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, ''),
(10, 10, 'Uy', 'Caren', 'Caren', 'female', 21, 4.00, 'carenuy@gmail.com', '123', '124', 'BSCS', '2012-07-20', 'Chinese', '789', '2012-07-23', 'Mactan', '0000-00-00', '2012-07-19', '2012-07-16', 'China', 'Megan Uy', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_room_tags_1`
--

CREATE TABLE IF NOT EXISTS `student_room_tags_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` int(11) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  `date_occupied` int(11) NOT NULL COMMENT 'date in unix time',
  PRIMARY KEY (`id`),
  KEY `accommodation_id` (`accommodation_id`),
  KEY `stud_guest_id` (`stud_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `student_room_tags_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `student_room_tag_1`
--

CREATE TABLE IF NOT EXISTS `student_room_tag_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `students_accom_id` int(11) DEFAULT NULL,
  `students_accommodation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `students_accom_id` (`students_accom_id`),
  KEY `students_accommodation_id` (`students_accommodation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `student_room_tag_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(25) DEFAULT NULL,
  `mobile_no` varchar(25) NOT NULL,
  `faxnum` varchar(35) DEFAULT NULL,
  `note` varchar(25) DEFAULT NULL,
  `terms` varchar(35) DEFAULT NULL,
  `company_name` varchar(35) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_added` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1234 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `name`, `address`, `phone_no`, `mobile_no`, `faxnum`, `note`, `terms`, `company_name`, `status`, `date_added`) VALUES
(3, '8 DEES Trading', 'Pajo, Lapu-lapu City TIN#195-684-046-000  ', '2964', '', '', '', '', '', 'active', 0),
(4, 'A. OCLEASA SIGN PRINTERS', '   ', '', '', '', '', '', '', 'active', 0),
(7, 'Abayon,Meliza R.', '   ', '', '', '', '', '', '', 'active', 0),
(8, 'Abel Mangalo', '   ', '', '', '', '', '', '', 'active', 0),
(9, 'Able Plan Promotions', '   ', '', '', '', '', '', '', 'active', 0),
(10, 'Abraham Arellano', '   ', '', '', '', '', '', '', 'active', 0),
(11, 'Ace Penzionne', '   ', '', '', '', '', '', '', 'active', 0),
(12, 'Ace Rivera', '   ', '', '', '', '', '', '', 'active', 0),
(13, 'Acecolors Outdoor Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(14, 'Adela Eumague', '   ', '', '', '', '', '', '', 'active', 0),
(15, 'Adelaida Ocampo', 'n/a', '', '', '', '', '', '', 'active', 0),
(16, 'Adelina Velayo', '   ', '', '', '', '', '', '', 'active', 0),
(18, 'Aeschylus Rivera', '   ', '', '', '', '', '', '', 'active', 0),
(19, 'Agnes Dianne Dacera', '   ', '', '', '', '', '', '', 'active', 0),
(20, 'Agrifina Breviescas', '   ', '', '', '', '', '', '', 'active', 0),
(21, 'Ahn Kwan Su v', '   ', '', '', '', '', '', '', 'active', 0),
(22, 'Aidan Alburo', '   ', '', '', '', '', '', '', 'active', 0),
(23, 'Aiden Iky Chopdat', '   ', '', '', '', '', '', '', 'active', 0),
(24, 'Aikka Iñigo', '   ', '', '', '', '', '', '', 'active', 0),
(25, 'Aileen R. Secuya', '   ', '', '', '', '', '', '', 'active', 0),
(26, 'Ailen Hazel Oñes', '   ', '', '', '', '', '', '', 'active', 0),
(27, 'Ailen Moralde', '   ', '', '', '', '', '', '', 'active', 0),
(28, 'Ailyn Becalas v', '   ', '', '', '', '', '', '', 'active', 0),
(29, 'Aime Medillo', 'Mabolo, Cebu City   ', '788-500', '9183456689', '963-8900', 'Cool supplier', 'Urgent', 'Bigfoot', 'active', 0),
(30, 'Aimeh Ruiz', '   ', '', '', '', '', '', '', 'active', 0),
(31, 'Aisah Ronaville Ubanan', '   ', '', '', '', '', '', '', 'active', 0),
(32, 'Aivy Saliente', '   ', '', '', '', '', '', '', 'active', 0),
(33, 'Akina Suga v', '   ', '', '', '', '', '', '', 'active', 0),
(34, 'Alain Ralph Arabe', '   ', '', '', '', '', '', '', 'active', 0),
(35, 'Alan Clegg', '   ', '', '', '', '', '', '', 'active', 0),
(36, 'Alan Granada', '   ', '', '', '', '', '', '', 'active', 0),
(37, 'Alan T. Alvez', '   ', '', '', '', '', '', '', 'active', 0),
(38, 'Albatech Engineering', '   ', '', '', '', '', '', '', 'active', 0),
(39, 'Alel Mae Pacres', '   ', '', '', '', '', '', '', 'active', 0),
(40, 'Alfie Bryan M. Cañon', '   ', '', '', '', '', '', '', 'active', 0),
(41, 'Alfredo Plana', '   ', '', '', '', '', '', '', 'active', 0),
(42, 'Alipio Dompor', '   ', '', '', '', '', '', '', 'active', 0),
(43, 'Alisha Tiu', '   ', '', '', '', '', '', '', 'active', 0),
(44, 'Allan Comighud', '   ', '', '', '', '', '', '', 'active', 0),
(45, 'Allen Bercel Cabreros', '   ', '', '', '', '', '', '', 'active', 0),
(46, 'Alpha Sunshine Arias', '   ', '', '', '', '', '', '', 'active', 0),
(47, 'Alsme Chemicals Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(48, 'Alvarez Nuez Galang Espina Lopez', '   ', '', '', '', '', '', '', 'active', 0),
(49, 'Alvie Colegado', '   ', '', '', '', '', '', '', 'active', 0),
(50, 'Amelia Joy Buo', '   ', '', '', '', '', '', '', 'active', 0),
(51, 'Amella Joy Buo', '   ', '', '', '', '', '', '', 'active', 0),
(52, 'An-An and Bebie Boy Meat Store', '   ', '', '', '', '', '', '', 'active', 0),
(53, 'Ana Theresa Singcol', '   ', '', '', '', '', '', '', 'active', 0),
(54, 'Anabelle Nabong', '   ', '', '', '', '', '', '', 'active', 0),
(55, 'Anadel Sabas', '   ', '', '', '', '', '', '', 'active', 0),
(56, 'Analyn Villa', '   ', '', '', '', '', '', '', 'active', 0),
(57, 'Andons Merchandising', '   ', '', '', '', '', '15 days', '', 'active', 0),
(58, 'Andrea Dunlop', '   ', '', '', '', '', '', '', 'active', 0),
(59, 'Andrea Ernestine Gierza', '   ', '', '', '', '', '', '', 'active', 0),
(60, 'Andrew Imbong', '   ', '', '', '', '', '', '', 'active', 0),
(61, 'Andy Kim', '   ', '', '', '', '', '', '', 'active', 0),
(62, 'Anecita Gorre', '   ', '', '', '', '', '', '', 'active', 0),
(63, 'Angelyn Ceniza', '   ', '', '', '', '', '', '', 'active', 0),
(64, 'Anita Yrogirog', '   ', '', '', '', '', '', '', 'active', 0),
(65, 'Anjanette Guibijar', '   ', '', '', '', '', '', '', 'active', 0),
(66, 'Ann Driane Tampus', '   ', '', '', '', '', '', '', 'active', 0),
(67, 'Anna Lou Alvarico', '   ', '', '', '', '', '', '', 'active', 0),
(68, 'Anna Marie Lauron', '   ', '', '', '', '', '', '', 'active', 0),
(69, 'Annadelle Alajar', '   ', '', '', '', '', '', '', 'active', 0),
(70, 'Annaliza Merin', '   ', '', '', '', '', '', '', 'active', 0),
(71, 'Annie Loren Soquilon', '   ', '', '', '', '', '', '', 'active', 0),
(72, 'Annie Lou Laurel', '   ', '', '', '', '', '', '', 'active', 0),
(73, 'Annie Mae Nazareta', '   ', '', '', '', '', '', '', 'active', 0),
(74, 'Antero Montague Fairbanks L. Melgar', '   ', '', '', '', '', '', '', 'active', 0),
(75, 'Anthony Delmo', '   ', '', '', '', '', '', '', 'active', 0),
(76, 'Anthony Hall', '   ', '', '', '', '', '', '', 'active', 0),
(77, 'Anthony Villalvito', '   ', '', '', '', '', '', '', 'active', 0),
(78, 'Antonio A. Teves', '   ', '', '', '', '', '', '', 'active', 0),
(79, 'Apple Cuizon', '   ', '', '', '', '', '', '', 'active', 0),
(80, 'April Balbuena', '   ', '', '', '', '', '', '', 'active', 0),
(81, 'April Barong', '   ', '', '', '', '', '', '', 'active', 0),
(82, 'April Centino', '   ', '', '', '', '', '', '', 'active', 0),
(83, 'April Louise Ayaton', '   ', '', '', '', '', '', '', 'active', 0),
(84, 'April Love Baguio', '   ', '', '', '', '', '', '', 'active', 0),
(85, 'April Sabesaje', '   ', '', '', '', '', '', '', 'active', 0),
(86, 'Arcason & Sons Construction & Dev''t. Corp', '   ', '', '', '', '', '', '', 'active', 0),
(87, 'Archie Sungahid', '   ', '', '', '', '', '', '', 'active', 0),
(88, 'Argeline Orbiso', '   ', '', '', '', '', '', '', 'active', 0),
(89, 'Ariel Baga', '   ', '', '', '', '', '', '', 'active', 0),
(90, 'Aries Distributor', '   ', '', '', '', '', '', '', 'active', 0),
(91, 'Arlene Gallenero', '   ', '', '', '', '', '', '', 'active', 0),
(92, 'Arlene Lascoña', '   ', '', '', '', '', '', '', 'active', 0),
(93, 'Armie Labalan', '   ', '', '', '', '', '', '', 'active', 0),
(94, 'Armie Lenin Labalan', '   ', '', '', '', '', '', '', 'active', 0),
(95, 'Arminue Po', '   ', '', '', '', '', '', '', 'active', 0),
(96, 'Arnel Frias', '   ', '', '', '', '', '', '', 'active', 0),
(97, 'Arnel Ponce', '   ', '', '', '', '', '', '', 'active', 0),
(98, 'Arnel S. Frias', '   ', '', '', '', '', '', '', 'active', 0),
(99, 'Arnulfo B. Oplado', '   ', '', '', '', '', '', '', 'active', 0),
(100, 'Art Crone', '   ', '', '', '', '', '', '', 'active', 0),
(101, 'Artemia Degamo Meat Vendor', '   ', '', '', '', '', '', '', 'active', 0),
(102, 'Arthur Rosalem', '   ', '', '', '', '', '', '', 'active', 0),
(103, 'Arzenith Degollacion', '   ', '', '', '', '', '', '', 'active', 0),
(104, 'Asia Pacific Ads Unlimited', '   ', '', '', '', '', '', '', 'active', 0),
(105, 'Asia Pacific Marketing', 'Jose L. Briones Ave., North Reclamation,   ', '232-0402', '', '232-1022', '', '', 'Asia Pacific Marketing', 'active', 0),
(106, 'Assi Mart', '   ', '', '', '', '', '7 days', '', 'active', 0),
(107, 'Astara Mae Daga-as', '   ', '', '', '', '', '', '', 'active', 0),
(108, 'Athena Mae Ibon', '   ', '', '', '', '', '', '', 'active', 0),
(109, 'Athena Rocero', '   ', '', '', '', '', '', '', 'active', 0),
(110, 'Aubrey Angelique Quirante', '   ', '', '', '', '', '', '', 'active', 0),
(111, 'Audrey Rose Ymbong', '   ', '', '', '', '', '', '', 'active', 0),
(112, 'Aurora Cachapero', '   ', '', '', '', '', '', '', 'active', 0),
(113, 'Austria Parane', '   ', '', '', '', '', '', '', 'active', 0),
(114, 'B.E. Benitez Architects', '   ', '', '', '', '', '', '', 'active', 0),
(115, 'B.P. Deliarte Electrical Works', '   ', '', '', '', '', '', '', 'active', 0),
(116, 'Baby Lyn Alesquio', '   ', '', '', '', '', '', '', 'active', 0),
(117, 'Banco de Oro Unibank', '   ', '', '', '', '', '', '', 'active', 0),
(118, 'Barangay Mactan Treasurer', '   ', '', '', '', '', '', '', 'active', 0),
(119, 'BBC''s Printshop', '   ', '', '', '', '', '', '', 'active', 0),
(120, 'BBC''s Printstop', '   ', '', '', '', '', '', '', 'active', 0),
(121, 'Belen Hermosa', '   ', '', '', '', '', '', '', 'active', 0),
(122, 'Benjamin Dabalos', '   ', '', '', '', '', '', '', 'active', 0),
(123, 'Benjie P. Yu', '   ', '', '', '', '', '', '', 'active', 0),
(124, 'Bernadeth Calderon', '   ', '', '', '', '', '', '', 'active', 0),
(125, 'Berovan Marketing, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(126, 'Beverly Ann Segismar', '   ', '', '', '', '', '', '', 'active', 0),
(127, 'Beverly Lyn Yurong', '   ', '', '', '', '', '', '', 'active', 0),
(128, 'Bianca A. Hernandez', '   ', '', '', '', '', '', '', 'active', 0),
(129, 'Bienvenida Tacumba', '   ', '', '', '', '', '', '', 'active', 0),
(130, 'Bigbucks Coffee Shop, Inc.', 'F. Ramos St., Cogon Central Ceby City  ', '', '', '', '', '', '', 'active', 0),
(131, 'Bigfoot Capital Venture Funds, Inc. v', 'Bigfoot Center F. Ramos Street Cebu City 228-524-321-000 ', '', '', '', '', '', '', 'active', 0),
(132, 'Bigfoot Coffee Shop, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(133, 'Bigfoot Communications Phils.,Inc.', 'F. Ramos St. Cogon Central Cebu City 220-885-777-000 ', '', '', '', '', '', '', 'active', 0),
(134, 'Bigfoot Customer Care Systems, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(135, 'Bigfoot Entertainment', '   ', '', '', '', '', '', '', 'active', 0),
(136, 'Bigfoot Entertainment Phils., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(137, 'Bigfoot Global Solutions, Inc.', 'F. Ramos Center Cogon Street Cebu City 221-721-479-000 ', '', '', '', '', '', '', 'active', 0),
(138, 'Bigfoot Properties, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(139, 'Bigfoot Studios, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(140, 'BIR FAO CELI INC.', '   ', '', '', '', '', '', '', 'active', 0),
(141, 'Bisda Security Agency', '   ', '', '', '', '', '', '', 'active', 0),
(142, 'Blonde Anne Dy', '   ', '', '', '', '', '', '', 'active', 0),
(143, 'Blondelle Belongilot', '   ', '', '', '', '', '', '', 'active', 0),
(144, 'Blue-Eye Security & Investigation Agency', '   ', '', '', '', '', '', '', 'active', 0),
(145, 'Blue Bubbles Express Laundry Services', '   ', '', '', '', '', '', '', 'active', 0),
(146, 'Bob Yu', '   ', '', '', '', '', '', '', 'active', 0),
(147, 'Bon Heber Puracan', '   ', '', '', '', '', '', '', 'active', 0),
(148, 'Bonnie Gallardo', '   ', '', '', '', '', '', '', 'active', 0),
(149, 'Booking.com', '   ', '', '', '', '', '', '', 'active', 0),
(150, 'Boyla Dive Center', '   ', '', '', '', '', '', '', 'active', 0),
(151, 'Brenda Abucay', '   ', '', '', '', '', '', '', 'active', 0),
(152, 'Brian Daly', '   ', '', '', '', '', '', '', 'active', 0),
(153, 'Brian Dominic Padilla v', '   ', '', '', '', '', '', '', 'active', 0),
(154, 'Brianne Mae Belga', '   ', '', '', '', '', '', '', 'active', 0),
(155, 'Brinda Demeterio', '   ', '', '', '', '', '', '', 'active', 0),
(156, 'Britta Mary Oblan', '   ', '', '', '', '', '', '', 'active', 0),
(157, 'Bruce Cunningham', '   ', '', '', '', '', '', '', 'active', 0),
(158, 'Bryan Roy Salvador', '   ', '', '', '', '', '', '', 'active', 0),
(159, 'Brylle Castro', '   ', '', '', '', '', '', '', 'active', 0),
(160, 'Bui Thi Hong Nhung', '   ', '', '', '', '', '', '', 'active', 0),
(161, 'Business Processing Association Phils.', '   ', '', '', '', '', '', '', 'active', 0),
(162, 'Byun Hyun Ju', '   ', '', '', '', '', '', '', 'active', 0),
(163, 'Caesar Francis Lee', '   ', '', '', '', '', '', '', 'active', 0),
(164, 'Carleen Carampatan', '   ', '', '', '', '', '', '', 'active', 0),
(165, 'Carlife Booc', '   ', '', '', '', '', '', '', 'active', 0),
(166, 'Carlito Aguirre Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(167, 'Carlo Emmanuel Redoble', '   ', '', '', '', '', '', '', 'active', 0),
(168, 'Carmel Saluta', '   ', '', '', '', '', '', '', 'active', 0),
(169, 'Carmelo Mialda', '   ', '', '', '', '', '', '', 'active', 0),
(170, 'Carmelyn Grace Mellijor', '   ', '', '', '', '', '', '', 'active', 0),
(171, 'Carolyn Leizlee Lanawan', '   ', '', '', '', '', '', '', 'active', 0),
(172, 'Cassandra Leigh', '   ', '', '', '', '', '', '', 'active', 0),
(173, 'Catalina Rentals', '   ', '', '', '', '', '', '', 'active', 0),
(174, 'Cathay Hardware Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(175, 'Catherine Homecillo', '   ', '', '', '', '', '', '', 'active', 0),
(176, 'Catherine R. Sasam', '   ', '', '', '', '', '', '', 'active', 0),
(177, 'Catron Computer Technology', '   ', '', '', '', '', '', '', 'active', 0),
(178, 'Causeway Printers, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(179, 'Cebu A.I. Logic Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(180, 'Cebu AIDORG Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(181, 'Cebu Ass''n. of Pvt./Public TVET Ins.,Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(182, 'Cebu Beach Club', 'Mactan Island Lapu-lapu City  ', '', '', '', '', '', '', 'active', 0),
(183, 'Cebu City Treasurer Office', '   ', '', '', '', '', '', '', 'active', 0),
(184, 'Cebu Cube Ice', '   ', '', '', '', '', '', '', 'active', 0),
(185, 'Cebu Diamond Iron Works', '   ', '', '', '', '', '7 days', '', 'active', 0),
(186, 'Cebu Educational Supply', '   ', '', '', '', '', '', '', 'active', 0),
(187, 'Cebu General Services,Inc.', 'G/F Heng Bldg. Manuel L. Quezon Avenue, Casuntingan Mandaue City', '', '', '', '', '', '', 'active', 0),
(188, 'Cebu Holdings, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(189, 'Cebu Home and Builders Centre', '   ', '', '', '', '', '', '', 'active', 0),
(190, 'Cebu Island Tour Services', '   ', '', '', '', '', '', '', 'active', 0),
(191, 'Cebu Oversea Hardware Co., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(192, 'Cebu Pacific Air', '   ', '', '', '', '', '', '', 'active', 0),
(193, 'Cebu Progress Commercial', '   ', '', '', '', '', '', '', 'active', 0),
(194, 'Cebu Progress Marketing', 'Cor. Borromeo & P. Lopez STs. Cebu City 004-271-250-000V  ', '', '', '', '', '', '', 'active', 0),
(195, 'Cebu Tristar Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(196, 'Cebu Vascom Trading Corp.', 'Banilad Road Cebu City 081-004-269-624 VAT 3450727/416-2084 ', '', '', '', '', '', '', 'active', 0),
(197, 'Cecilia Guadalupe Corominas', '   ', '', '', '', '', '', '', 'active', 0),
(198, 'CELI Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(199, 'CELI Ltd.', '   ', '', '', '', '', '', '', 'active', 0),
(200, 'Celina Tan', '   ', '', '', '', '', '', '', 'active', 0),
(201, 'Cesar Evan N. Tiu', '   ', '', '', '', '', '', '', 'active', 0),
(202, 'Charina Sarte', '   ', '', '', '', '', '', '', 'active', 0),
(203, 'Charisma Construction Services/A.Lenaming', '   ', '', '', '', '', '', '', 'active', 0),
(204, 'Charissa Keith Padilla', '   ', '', '', '', '', '', '', 'active', 0),
(205, 'Charisse Guenever Sison', '   ', '', '', '', '', '', '', 'active', 0),
(206, 'Charlene Tito', '   ', '', '', '', '', '', '', 'active', 0),
(207, 'Charles Vailoces', '   ', '', '', '', '', '', '', 'active', 0),
(208, 'Charmine Catuburan', '   ', '', '', '', '', '', '', 'active', 0),
(209, 'Chateau de Carmen Hotel', '21 Juana Osmeña Street Cebu City  ', '255-63-66', '', '', '', '', '', 'active', 0),
(210, 'Cherry Ann Arrojado', '   ', '', '', '', '', '', '', 'active', 0),
(211, 'Cherry Mae A. Genesiran', '   ', '', '', '', '', '', '', 'active', 0),
(212, 'Cherry Mae Pabulayan', '   ', '', '', '', '', '', '', 'active', 0),
(213, 'Cherry Mae Sotto', '   ', '', '', '', '', '', '', 'active', 0),
(214, 'Cherry May Agbay', '   ', '', '', '', '', '', '', 'active', 0),
(215, 'Cherryl Gecera', '   ', '', '', '', '', '', '', 'active', 0),
(216, 'Cheryl Ortega', '   ', '', '', '', '', '', '', 'active', 0),
(217, 'Cheryl Rose Carayo', '   ', '', '', '', '', '', '', 'active', 0),
(218, 'Cheryll Rose A. Carayo', '   ', '', '', '', '', '', '', 'active', 0),
(219, 'Chi-Wen Hwang', '   ', '', '', '', '', '', '', 'active', 0),
(220, 'Chinebeth P. Borja', '   ', '', '', '', '', '', '', 'active', 0),
(221, 'Ching-Ching Luningning Tan', '   ', '', '', '', '', '', '', 'active', 0),
(222, 'Chiwen Huan', '   ', '', '', '', '', '', '', 'active', 0),
(223, 'Chloe Leulhati Barcelona', '   ', '', '', '', '', '', '', 'active', 0),
(224, 'Chong Moon Water Corp.', 'Nasipit Talamban Cebu City 236-999-896NV ', '344-1663', '', '', '', '', '', 'active', 0),
(225, 'Chony', '   ', '', '', '', '', '', '', 'active', 0),
(226, 'Chony Adamos', '   ', '', '', '', '', '', '', 'active', 0),
(227, 'Chrisitine Garcia', '   ', '', '', '', '', '', '', 'active', 0),
(228, 'Chrislann Catuburan', '   ', '', '', '', '', '', '', 'active', 0),
(229, 'Christabel S. Apas', '   ', '', '', '', '', '', '', 'active', 0),
(230, 'Christian James Cypres', '   ', '', '', '', '', '', '', 'active', 0),
(231, 'Christine Aura Ceniza', '   ', '', '', '', '', '', '', 'active', 0),
(232, 'Christine Joyce Gabrinez', '   ', '', '', '', '', '', '', 'active', 0),
(233, 'Christine Labata', '   ', '', '', '', '', '', '', 'active', 0),
(234, 'Christine Mae Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(235, 'Christine Marie Labata', '   ', '', '', '', '', '', '', 'active', 0),
(236, 'Christine Michelle Redoña', '   ', '', '', '', '', '', '', 'active', 0),
(237, 'Christine Rabe', '   ', '', '', '', '', '', '', 'active', 0),
(238, 'Christine Tajanlangit', '   ', '', '', '', '', '', '', 'active', 0),
(239, 'Christopher Delfino', '   ', '', '', '', '', '', '', 'active', 0),
(240, 'Christopher John Orongan', '   ', '', '', '', '', '', '', 'active', 0),
(241, 'Chrys Ocampo', '   ', '', '', '', '', '', '', 'active', 0),
(242, 'Ciara Baclayon', '   ', '', '', '', '', '', '', 'active', 0),
(243, 'Cindy Pamela E. Baron', '   ', '', '', '', '', '', '', 'active', 0),
(244, 'Citibank', '   ', '', '', '', '', '', '', 'active', 0),
(245, 'CITIBANK VISA FAO 248-000-0322013', '   ', '', '', '', '', '', '', 'active', 0),
(246, 'CITIBANK VISA FAO 4532-4800-0632-2013', '   ', '', '', '', '', '', '', 'active', 0),
(247, 'CITIBANK VISA FAO 4532-4872-0000-9270', '   ', '', '', '', '', '', '', 'active', 0),
(248, 'Citibank Visa FAO 4532480000322013', '   ', '', '', '', '', '', '', 'active', 0),
(249, 'City Treasure of Cebu', '   ', '', '', '', '', '', '', 'active', 0),
(250, 'City Treasurer of Lapu-Lapu', '   ', '', '', '', '', '', '', 'active', 0),
(251, 'Claire Dipay', '   ', '', '', '', '', '', '', 'active', 0),
(252, 'Clara Villaronza', '   ', '', '', '', '', '', '', 'active', 0),
(253, 'Clarice Armian', '   ', '', '', '', '', '', '', 'active', 0),
(254, 'Clariffel Marie Dinglasa', '   ', '', '', '', '', '', '', 'active', 0),
(255, 'Clarissa Joy Beton', '   ', '', '', '', '', '', '', 'active', 0),
(256, 'Clarita Villacote', '   ', '', '', '', '', '', '', 'active', 0),
(257, 'Classicware Eximport Co., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(258, 'Claudette Blanca Bascon', '   ', '', '', '', '', '', '', 'active', 0),
(259, 'Cleverlearn English Language Ltd.', '   ', '', '', '', '', '', '', 'active', 0),
(260, 'Clifford Kyle Polancos', '   ', '', '', '', '', '', '', 'active', 0),
(261, 'Color Acuity International, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(262, 'Connielyn Martus', '   ', '', '', '', '', '', '', 'active', 0),
(263, 'Country Fresh Trading', 'Suite 210, Crown Bldg. Road North 6 Reclamation Area Cebu City ', '233-0195', '', '', '121-756-119-000', '', '', 'active', 0),
(264, 'Crispen Dela Cruz', '   ', '', '', '', '', '', '', 'active', 0),
(265, 'Crispin Dela Cruz', '   ', '', '', '', '', '', '', 'active', 0),
(266, 'Cristopher Ofril', '   ', '', '', '', '', '', '', 'active', 0),
(267, 'Crown Regency Suites', 'Maximo Patalinghug Jr. Avenue Lapu-lapu Cebu  ', '341-4586-94', '', '', '', '', '', 'active', 0),
(268, 'Cycyl Birog', '   ', '', '', '', '', '', '', 'active', 0),
(269, 'Cynde Rosario Malayo', '   ', '', '', '', '', '', '', 'active', 0),
(270, 'Cynthia Vasquez', '   ', '', '', '', '', '', '', 'active', 0),
(271, 'Cyril John Remonde', '   ', '', '', '', '', '', '', 'active', 0),
(272, 'Daisuke Hotta v', '   ', '', '', '', '', '', '', 'active', 0),
(273, 'Daisy Yu', '   ', '', '', '', '', '', '', 'active', 0),
(274, 'Dan Ryan Gasataya', '   ', '', '', '', '', '', '', 'active', 0),
(275, 'Daniel Malaki', '   ', '', '', '', '', '', '', 'active', 0),
(276, 'Daniella Ma. Milagros Escoto', '   ', '', '', '', '', '', '', 'active', 0),
(277, 'Danielle Ma. Milagros Escoto', '   ', '', '', '', '', '', '', 'active', 0),
(278, 'Dao Duy Thang', '   ', '', '', '', '', '', '', 'active', 0),
(279, 'Darl Carmel Lee Lopez', '   ', '', '', '', '', '', '', 'active', 0),
(280, 'David Aaron Jenkins', '   ', '', '', '', '', '', '', 'active', 0),
(281, 'David Garrison', '   ', '', '', '', '', '', '', 'active', 0),
(282, 'David Page', '   ', '', '', '', '', '', '', 'active', 0),
(283, 'David Wiiliams', '   ', '', '', '', '', '', '', 'active', 0),
(284, 'David Williams', '   ', '', '', '', '', '', '', 'active', 0),
(285, 'Dax Jordan A. Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(286, 'Days Hotel', '   ', '', '', '', '', '', '', 'active', 0),
(287, 'Days Hotel Philippines, Inc', 'Airport Road Matumbo Pusok Hills Lapu-lapu City  ', '', '', '', '', '', '', 'active', 0),
(288, 'Delahne Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(289, 'Demnah Dalde', '   ', '', '', '', '', '', '', 'active', 0),
(290, 'Den Stevie Amodia', '   ', '', '', '', '', '', '', 'active', 0),
(291, 'Dennis Dalanon', '   ', '', '', '', '', '', '', 'active', 0),
(292, 'Dennis Dominquez', '   ', '', '', '', '', '', '', 'active', 0),
(293, 'Dennis Grimalt', '   ', '', '', '', '', '', '', 'active', 0),
(294, 'Dept. of Trade & Industry Securities & Ex', '   ', '', '', '', '', '', '', 'active', 0),
(295, 'Dervis Senica', '   ', '', '', '', '', '', '', 'active', 0),
(296, 'Desert Spring Trading', '   ', '', '', '', '', '', '', 'active', 0),
(297, 'Desiree Ann Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(298, 'Desiree May Ancheta', '   ', '', '', '', '', '', '', 'active', 0),
(299, 'Dheb Muit', '   ', '', '', '', '', '', '', 'active', 0),
(300, 'DIACOMM Communications Equipment', '   ', '', '', '', '', '', '', 'active', 0),
(301, 'Diacomm Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(302, 'Diamond Trading & Services', 'F.U. Bldg A.S. Fortuna St. Bakilid Mandaue City TIN 181-109-883-000 ', '', '', '', '', '', '', 'active', 0),
(303, 'Digitel Mobile Phils., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(304, 'Dina Gorgonia', '   ', '', '', '', '', '', '', 'active', 0),
(305, 'Dina Joanna Lao', '   ', '', '', '', '', '', '', 'active', 0),
(306, 'Dinah Laguerta', '   ', '', '', '', '', '', '', 'active', 0),
(307, 'Diona Lorraine Malaque', '   ', '', '', '', '', '', '', 'active', 0),
(308, 'Dionaly Fajardo', '   ', '', '', '', '', '', '', 'active', 0),
(309, 'Diplomat Hotel', '   ', '', '', '', '', '', '', 'active', 0),
(310, 'Divina Allanic', '   ', '', '', '', '', '', '', 'active', 0),
(311, 'Do Young Kim', '   ', '', '', '', '', '', '', 'active', 0),
(312, 'Dodong Belen Meat Shop', '   ', '', '', '', '', '', '', 'active', 0),
(313, 'Dominador Mimis', '   ', '', '', '', '', '', '', 'active', 0),
(314, 'Donabelle Diwatin', '   ', '', '', '', '', '', '', 'active', 0),
(315, 'dotPH Domains, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(316, 'Dr. Ignacio M. Cortes General Hospital', '   ', '', '', '', '', '', '', 'active', 0),
(317, 'DuPRINTER PHILS., INC.', '   ', '', '', '', '', '', '', 'active', 0),
(318, 'Earl Mullen', '   ', '', '', '', '', '', '', 'active', 0),
(319, 'East Capitol Pensionne', 'Capitol, Cebu City   ', '', '', '', '', '', '', 'active', 0),
(320, 'Easy Gas Convenience Store', '   ', '', '', '', '', '', '', 'active', 0),
(321, 'Echo Electrical Supply Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(322, 'ECO-TECH Appliance Circle Inc', '   ', '', '', '', '', '', '', 'active', 0),
(323, 'Edcon Glass and Aluminum,Inc.', 'H.Cortes St. Mandaue City 220-111-091-000  ', '', '', '', '', '', '', 'active', 0),
(324, 'Eddisio Pepito', '   ', '', '', '', '', '', '', 'active', 0),
(325, 'Edgar Anuada', '   ', '', '', '', '', '', '', 'active', 0),
(326, 'Edgardo Amoin', '   ', '', '', '', '', '', '', 'active', 0),
(327, 'edna Pacaña', '   ', '', '', '', '', '', '', 'active', 0),
(328, 'Edna Remulta', '   ', '', '', '', '', '', '', 'active', 0),
(329, 'Edna Tiongson', '   ', '', '', '', '', '', '', 'active', 0),
(330, 'Edsel Muzares', '   ', '', '', '', '', '', '', 'active', 0),
(331, 'eduardo Piañar', '   ', '', '', '', '', '', '', 'active', 0),
(332, 'Eduardo T. Cabigas', '   ', '', '', '', '', '', '', 'active', 0),
(333, 'Edward Mangaron', '   ', '', '', '', '', '', '', 'active', 0),
(334, 'Eileen Joy Cola', '   ', '', '', '', '', '', '', 'active', 0),
(335, 'Eiress Lugod', '   ', '', '', '', '', '', '', 'active', 0),
(336, 'Elaine Alix', '   ', '', '', '', '', '', '', 'active', 0),
(337, 'Elbe Ackman', '   ', '', '', '', '', '', '', 'active', 0),
(338, 'Elbe Akman', '   ', '', '', '', '', '', '', 'active', 0),
(339, 'Elbert Villariza', '   ', '', '', '', '', '', '', 'active', 0),
(340, 'Eleanor Ouano', '   ', '', '', '', '', '', '', 'active', 0),
(341, 'Electroworld Ayala', '   ', '', '', '', '', '', '', 'active', 0),
(342, 'Elena Freire', '   ', '', '', '', '', '', '', 'active', 0),
(343, 'Elenita Ayangco', '   ', '', '', '', '', '', '', 'active', 0),
(344, 'Elibeth Cozo', '   ', '', '', '', '', '', '', 'active', 0),
(345, 'Eliseo Damalerio', '   ', '', '', '', '', '', '', 'active', 0),
(346, 'Elite Machines', 'Espiritu Bldg. 33 Gen Maxilom Ave. Cebu City 005-679-485-001 ', '', '', '', '', '', '', 'active', 0),
(347, 'Elizabeth Gasco', '   ', '', '', '', '', '', '', 'active', 0),
(348, 'Elizabeth Mendoza', '   ', '', '', '', '', '', '', 'active', 0),
(349, 'Elizabeth Telen', '   ', '', '', '', '', '', '', 'active', 0),
(350, 'Elnie Abrigondo', '   ', '', '', '', '', '', '', 'active', 0),
(351, 'Elnora Jacaban', '   ', '', '', '', '', '', '', 'active', 0),
(352, 'Elorie Jo-anah Montilla', '   ', '', '', '', '', '', '', 'active', 0),
(353, 'Elsa Lim', '   ', '', '', '', '', '', '', 'active', 0),
(354, 'Elsie Dalaguan', '   ', '', '', '', '', '', '', 'active', 0),
(355, 'Emcor Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(356, 'Emergency Rescue Unit Foundation', '   ', '', '', '', '', '', '', 'active', 0),
(357, 'Emily Lawas', '   ', '', '', '', '', '', '', 'active', 0),
(358, 'Emily Tatoy', '   ', '', '', '', '', '', '', 'active', 0),
(359, 'Emma Saldua', '   ', '', '', '', '', '', '', 'active', 0),
(360, 'Emmanuel Pateña', '   ', '', '', '', '', '', '', 'active', 0),
(361, 'Emmylou Laurito', '   ', '', '', '', '', '', '', 'active', 0),
(362, 'Emo Road Runner Enterpises', '   ', '', '', '', '', '', '', 'active', 0),
(363, 'Erika Michell Entrampas', '   ', '', '', '', '', '', '', 'active', 0),
(364, 'Erma Juaton', '   ', '', '', '', '', '', '', 'active', 0),
(365, 'Ernest Villariza', '   ', '', '', '', '', '', '', 'active', 0),
(366, 'Ernesto Caballes Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(367, 'Ernie Jun Actub', '   ', '', '', '', '', '', '', 'active', 0),
(368, 'Erwin Diaz', '   ', '', '', '', '', '', '', 'active', 0),
(369, 'Esacar Agbay, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(370, 'Esmeraldo Camilo', '   ', '', '', '', '', '', '', 'active', 0),
(371, 'Esmeraldo Radam', '   ', '', '', '', '', '', '', 'active', 0),
(372, 'Estala Vianney Tiongco', '   ', '', '', '', '', '', '', 'active', 0),
(373, 'Estrella Igot', '   ', '', '', '', '', '', '', 'active', 0),
(374, 'Eulalia Sagnoy', '   ', '', '', '', '', '', '', 'active', 0),
(375, 'Eve Abalos', '   ', '', '', '', '', '', '', 'active', 0),
(376, 'Evelyn Juaneza', '   ', '', '', '', '', '', '', 'active', 0),
(377, 'Faida Bojos', '   ', '', '', '', '', '', '', 'active', 0),
(378, 'Faith Evangeline Jamandre', '   ', '', '', '', '', '', '', 'active', 0),
(379, 'Faithfully Travel    v', '   ', '', '', '', '', '', '', 'active', 0),
(380, 'Farrah Faye Fariola', '   ', '', '', '', '', '', '', 'active', 0),
(381, 'Fast Net Development Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(382, 'FASTTRACK SOLUTIONS , INC.', '   ', '', '', '', '', '', 'FASTTRACK SOLUTIONS , INC.', 'active', 0),
(383, 'Fatimah Ah-Dawia Apalla', '   ', '', '', '', '', '', '', 'active', 0),
(384, 'Federico Albacite, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(385, 'Fedie Painting Contractor/Fedelito Mata', '   ', '', '', '', '', '', '', 'active', 0),
(386, 'Fel Louise Alingasa', '   ', '', '', '', '', '', '', 'active', 0),
(387, 'Felipe Jose Sarmiento', '   ', '', '', '', '', '', '', 'active', 0),
(388, 'Flerida Ho', '   ', '', '', '', '', '', '', 'active', 0),
(389, 'Flora Mae D. Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(390, 'Floricor Toledo v', '   ', '', '', '', '', '', '', 'active', 0),
(391, 'Floro Jacaban', '   ', '', '', '', '', '', '', 'active', 0),
(392, 'Flory Mae Salintao', '   ', '', '', '', '', '', '', 'active', 0),
(393, 'Fontini Christi Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(394, 'Fooda Saversmart Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(395, 'foods', '   ', '', '', '', '', '', '', 'active', 0),
(396, 'Fotoline Express Incorporated', '   ', '', '', '', '', '', '', 'active', 0),
(397, 'Frameworkx Incorporated', '   ', '', '', '', '', '', '', 'active', 0),
(398, 'Frances Gayle Esimos', '   ', '', '', '', '', '', '', 'active', 0),
(399, 'Francis Caesar Lee', '   ', '', '', '', '', '', '', 'active', 0),
(400, 'Francis Te', '   ', '', '', '', '', '', '', 'active', 0),
(401, 'Francisco Mari Flores', '   ', '', '', '', '', '', '', 'active', 0),
(402, 'Franzina Sol-Gae Jose', '   ', '', '', '', '', '', '', 'active', 0),
(403, 'Fredie Auditor', '   ', '', '', '', '', '', '', 'active', 0),
(404, 'Freya Lynn Mari', '   ', '', '', '', '', '', '', 'active', 0),
(405, 'Freya Lynn Mari Bitas', '   ', '', '', '', '', '', '', 'active', 0),
(406, 'Fujimart Cebu, Inc.', '   ', '', '', '', '', 'Due on receipt', '', 'active', 0),
(407, 'Fukumi Tam', '   ', '', '', '', '', '', '', 'active', 0),
(408, 'Fulvio Calamba', '   ', '', '', '', '', '', '', 'active', 0),
(409, 'Fun & Sun', '   ', '', '', '', '', '', '', 'active', 0),
(410, 'Gacil Larino', '   ', '', '', '', '', '', '', 'active', 0),
(411, 'Gaisano Capital Mactan', '   ', '', '', '', '', '', '', 'active', 0),
(412, 'GAKKEN (Philippines). Inc.', '   ', '', '', '', '', '30 days', '', 'active', 0),
(413, 'Gardens r'' Us, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(414, 'Garry Lou Tenchavez', '   ', '', '', '', '', '', '', 'active', 0),
(415, 'Gary Maxwell', '   ', '', '', '', '', '', '', 'active', 0),
(416, 'Gasera Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(417, 'Gemmarie Dinglasa', '   ', '', '', '', '', '', '', 'active', 0),
(418, 'GENDIESEL Philippines, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(419, 'Genelyn Elveña', '   ', '', '', '', '', '', '', 'active', 0),
(420, 'Genevieve Fuentes', '   ', '', '', '', '', '', '', 'active', 0),
(421, 'Gennet Chiong', '   ', '', '', '', '', '', '', 'active', 0),
(422, 'Genson Surveying &Engineering Consultants', '   ', '', '', '', '', '', '', 'active', 0),
(423, 'Geraldine Olita', '   ', '', '', '', '', '', '', 'active', 0),
(424, 'Geramie Lagumbay', '   ', '', '', '', '', '', '', 'active', 0),
(425, 'Gerard Paul Niño Vergara', '   ', '', '', '', '', '', '', 'active', 0),
(426, 'Gina Francisco', '   ', '', '', '', '', '', '', 'active', 0),
(427, 'Ginebra San Miguel, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(428, 'Girlie Bacay', '   ', '', '', '', '', '', '', 'active', 0),
(429, 'Giselle Ann Lagarnia', '   ', '', '', '', '', '', '', 'active', 0),
(430, 'Glacierview Realty, Inc.', 'Tuplips Center A.S. Fortuna Bakilid Mandaue City  ', '344-7726/344-7727', '', '', '', '', '', 'active', 0),
(431, 'Glenda Garcia', '   ', '', '', '', '', '', '', 'active', 0),
(432, 'Gliceria Alinsonorin/A.G. Copier Ctr', 'F. Ramos St. Cogon Cebu City TIN: 142-898-318-003', '', '', '', '', '', '', 'active', 0),
(433, 'Gloria Olitres', '   ', '', '', '', '', '', '', 'active', 0),
(434, 'Golda Dazo', '   ', '', '', '', '', '', '', 'active', 0),
(435, 'Goldberg Printing Services', '   ', '', '', '', '', '', '', 'active', 0),
(436, 'Golden Cypress Water Co.,Ltd.', '   ', '', '', '', '', '', '', 'active', 0),
(437, 'Gomer Bongcac', '   ', '', '', '', '', '', '', 'active', 0),
(438, 'Good Earth Water Refilling Station', '   ', '', '', '', '', '', '', 'active', 0),
(439, 'Gotta Wash It Studio', '   ', '', '', '', '', '', '', 'active', 0),
(440, 'Grace Augusto', '   ', '', '', '', '', '', '', 'active', 0),
(441, 'Grace Cantero', '   ', '', '', '', '', '', '', 'active', 0),
(442, 'Grace Maglines', '   ', '', '', '', '', '', '', 'active', 0),
(443, 'Grace Marie Albio', '   ', '', '', '', '', '', '', 'active', 0),
(444, 'Grace Tango-an', '   ', '', '', '', '', '', '', 'active', 0),
(445, 'Grand Holidays, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(446, 'Graphic Star', '   ', '', '', '', '', '', '', 'active', 0),
(447, 'Great Eastern Commercial', '   ', '', '', '', '', '', '', 'active', 0),
(448, 'Green Leaf Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(449, 'Gretchen Tongco', '   ', '', '', '', '', '', '', 'active', 0),
(450, 'Gritzie Fulguirinas', '   ', '', '', '', '', '', '', 'active', 0),
(451, 'Guada Corominas', '   ', '', '', '', '', '', '', 'active', 0),
(452, 'Guendolyn Templado', '   ', '', '', '', '', '', '', 'active', 0),
(453, 'Gullem Metal Shop/Richard Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(454, 'Gywn Jomuad', '   ', '', '', '', '', '', '', 'active', 0),
(455, 'Haney Lynn Siclot', '   ', '', '', '', '', '', '', 'active', 0),
(456, 'Hardinado Patnugot III', '   ', '', '', '', '', '', '', 'active', 0),
(457, 'Hardrock General Contractors', '   ', '', '', '', '', '', '', 'active', 0),
(458, 'Hazel Catherine Pagunsan', '   ', '', '', '', '', '', '', 'active', 0),
(459, 'Hazel Daño', '   ', '', '', '', '', '', '', 'active', 0),
(460, 'Hazel Kiamco', '   ', '', '', '', '', '', '', 'active', 0),
(461, 'Hazel Tan', '   ', '', '', '', '', '', '', 'active', 0),
(462, 'Heart Pedroza', '   ', '', '', '', '', '', '', 'active', 0),
(463, 'Helen May Mullet', '   ', '', '', '', '', '', '', 'active', 0),
(464, 'Henjel Bardoquillo', '   ', '', '', '', '', '', '', 'active', 0),
(465, 'Henny Fer Marie Cañete', '   ', '', '', '', '', '', '', 'active', 0),
(466, 'Henry Gerard Reynes', '   ', '', '', '', '', '', '', 'active', 0),
(467, 'Henry Peterson', '   ', '', '', '', '', '', '', 'active', 0),
(468, 'HITECH Supplies & Packaging', '   ', '', '', '', '', '30 days', '', 'active', 0),
(469, 'Holiday Plaza Hotel', 'F Ramos St. Cebu City   ', '', '', '', '', '', '', 'active', 0),
(470, 'Home Development Mutual Fund', '   ', '', '', '', '', '', '', 'active', 0),
(471, 'Home Options, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(472, 'Home Plan Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(473, 'Hope Taboada', '   ', '', '', '', '', '', '', 'active', 0),
(474, 'Hwang Ho In v', '   ', '', '', '', '', '', '', 'active', 0),
(475, 'Hye Kim Kyoung (Tessie)', '   ', '', '', '', '', '', '', 'active', 0),
(476, 'Hygeia Valerie Abellanosa', '   ', '', '', '', '', '', '', 'active', 0),
(477, 'IAFT-CELI', '   ', '', '', '', '', '', '', 'active', 0),
(478, 'Ian Carlos Go', '   ', '', '', '', '', '', '', 'active', 0),
(479, 'Ian Graciano Villacera', '   ', '', '', '', '', '', '', 'active', 0),
(480, 'Ian Lambojon', '   ', '', '', '', '', '', '', 'active', 0),
(481, 'Ilene Angelitud', '   ', '', '', '', '', '', '', 'active', 0),
(482, 'Inca Plastics Philippines, Inc.', 'Unit 2 NGS Bldg GoSotto Compound M.J. Cuenco St Cebu City 000-12-172-001V', '', '', '', '', '', '', 'active', 0),
(483, 'Indiana Aerospace Univ. (IAU) Foundation', '   ', '', '', '', '', '', '', 'active', 0),
(484, 'Industrial Inspection (International),Inc', '3-D Gill Tudtud St. Mabolo Cebu City   ', '', '', '', '', '', '', 'active', 0),
(485, 'Innove Communications, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(486, 'Int''l. Academy of Film & Television, Inc.', 'Bigfoot IT & Media Park, Lapu-lapu city   ', '', '', '', '', '', '', 'active', 0),
(487, 'International Pharmaceuticals, Inc.', '   ', '', '', '', '', '30 days', '', 'active', 0),
(488, 'Invincible Armada Secuirity Agency', '   ', '', '', '', '', '', '', 'active', 0),
(489, 'Ionie Guazon', '   ', '', '', '', '', '', '', 'active', 0),
(490, 'Ionne Llenes', '   ', '', '', '', '', '', '', 'active', 0),
(491, 'Iorena Nabe Vergara', '   ', '', '', '', '', '', '', 'active', 0),
(492, 'Iori Yoshida', '   ', '', '', '', '', '', '', 'active', 0),
(493, 'Irah Miraflor', '   ', '', '', '', '', '', '', 'active', 0),
(494, 'Irene Angelitud', '   ', '', '', '', '', '', '', 'active', 0),
(495, 'Irene Jane Geniston', '   ', '', '', '', '', '', '', 'active', 0),
(496, 'Irene Palacio', '   ', '', '', '', '', '', '', 'active', 0),
(497, 'Iris Hope De Los Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(498, 'Irish Gonzales', '   ', '', '', '', '', '', '', 'active', 0),
(499, 'Irish Jao', '   ', '', '', '', '', '', '', 'active', 0),
(500, 'Irish Lorraine Miraflor', '   ', '', '', '', '', '', '', 'active', 0),
(501, 'Irish Mae Toring', '   ', '', '', '', '', '', '', 'active', 0),
(502, 'Islands Souvenirs, Inc', '   ', '', '', '', '', '', '', 'active', 0),
(503, 'Israel Ken Zacal', '   ', '', '', '', '', '', '', 'active', 0),
(504, 'Ivy Catagcatag', '   ', '', '', '', '', '', '', 'active', 0),
(505, 'Iwojima Arcipe', '   ', '', '', '', '', '', '', 'active', 0),
(506, 'J I G Fish Dealer', '   ', '', '', '', '', '', '', 'active', 0),
(507, 'J. King & Sons Company, Inc.', 'Holy Name Street Mabolo Cebu city   ', '', '', '', '', '', '', 'active', 0),
(508, 'Jack Zhang', '   ', '', '', '', '', '', '', 'active', 0),
(509, 'Jackielou Rocamura', '   ', '', '', '', '', '', '', 'active', 0),
(510, 'Jacob''s Basic First Ventures', '   ', '', '', '', '', '', '', 'active', 0),
(511, 'Jacqueline Canoy', '   ', '', '', '', '', '', '', 'active', 0),
(512, 'Jake Daro', '   ', '', '', '', '', '', '', 'active', 0),
(513, 'James Debellis', '   ', '', '', '', '', '', '', 'active', 0),
(514, 'Jan- Michael Niño Buhay', '   ', '', '', '', '', '', '', 'active', 0),
(515, 'Jancy Paz Basubas', '   ', '', '', '', '', '', '', 'active', 0),
(516, 'Jane Rachel Riconalla', '   ', '', '', '', '', '', '', 'active', 0),
(517, 'Jane Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(518, 'Janice Lucille Villamor', '   ', '', '', '', '', '', '', 'active', 0),
(519, 'Janice Lucille Virtucio', '   ', '', '', '', '', '', '', 'active', 0),
(520, 'Jay''s Square Bakeshop', '   ', '', '', '', '', '', '', 'active', 0),
(521, 'Jay C. Designs, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(522, 'Jay Yrogirog', '   ', '', '', '', '', '', '', 'active', 0),
(523, 'Jayme Electronics & Refrigeration Service', '   ', '', '', '', '', '', '', 'active', 0),
(524, 'Jayziel Palopalo', '   ', '', '', '', '', '', '', 'active', 0),
(525, 'JCAMP General Merchandise', '   ', '', '', '', '', '', '', 'active', 0),
(526, 'Jeane Evangelista', '   ', '', '', '', '', '', '', 'active', 0),
(527, 'Jecyl Malazarte', '   ', '', '', '', '', '', '', 'active', 0),
(528, 'Jed Libby Tadlas', '   ', '', '', '', '', '', '', 'active', 0),
(529, 'Jefferson Murillo', '   ', '', '', '', '', '', '', 'active', 0),
(530, 'Jegger Rasonable', '   ', '', '', '', '', '', '', 'active', 0),
(531, 'Jell-Ann Noynay', '   ', '', '', '', '', '', '', 'active', 0),
(532, 'Jelsan Peñada', '   ', '', '', '', '', '', '', 'active', 0),
(533, 'Jemary Rose Gualiza', '   ', '', '', '', '', '', '', 'active', 0),
(534, 'Jennefer Tumulak', '   ', '', '', '', '', '', '', 'active', 0),
(535, 'Jennifer Andres', '   ', '', '', '', '', '', '', 'active', 0),
(536, 'Jennifer Basan', '   ', '', '', '', '', '', '', 'active', 0),
(537, 'Jennifer Blase', '   ', '', '', '', '', '', '', 'active', 0),
(538, 'Jennifer Tahinay', '   ', '', '', '', '', '', '', 'active', 0),
(539, 'Jeramel Mingueto', '   ', '', '', '', '', '', '', 'active', 0),
(540, 'Jerelyn Wagwag', '   ', '', '', '', '', '', '', 'active', 0),
(541, 'Jerill Galleon', '   ', '', '', '', '', '', '', 'active', 0),
(542, 'Jermaine Gara', '   ', '', '', '', '', '', '', 'active', 0),
(543, 'Jermeine Abapo', '   ', '', '', '', '', '', '', 'active', 0),
(544, 'Jerome Ramirez', '   ', '', '', '', '', '', '', 'active', 0),
(545, 'Jerome Sabac', '   ', '', '', '', '', '', '', 'active', 0),
(546, 'Jesa A. Gonido', '   ', '', '', '', '', '', '', 'active', 0),
(547, 'Jeser Plotinia', '   ', '', '', '', '', '', '', 'active', 0),
(548, 'Jesha Tejero', '   ', '', '', '', '', '', '', 'active', 0),
(549, 'Jessa Capadiso', '   ', '', '', '', '', '', '', 'active', 0),
(550, 'Jessica Fuentes', '   ', '', '', '', '', '', '', 'active', 0),
(551, 'Jessicca Gulane', '   ', '', '', '', '', '', '', 'active', 0),
(552, 'Jessie Carangue', '   ', '', '', '', '', '', '', 'active', 0),
(553, 'Jesster Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(554, 'Jessterads', '   ', '', '', '', '', '', '', 'active', 0),
(555, 'Jestoni Empas', '   ', '', '', '', '', '', '', 'active', 0),
(556, 'JGL Cable and Electrical Services', '   ', '', '', '', '', '', '', 'active', 0),
(557, 'JH Bread Baker Bakeshop', '   ', '', '', '', '', '', '', 'active', 0),
(558, 'Jhonrey Carambias', '   ', '', '', '', '', '', '', 'active', 0),
(559, 'Jimbo Ramirez', '   ', '', '', '', '', '', '', 'active', 0),
(560, 'JJED Philippines Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(561, 'JKY Food Ventures', '   ', '', '', '', '', '7 days', '', 'active', 0),
(562, 'Joan Clavete', '   ', '', '', '', '', '', '', 'active', 0),
(563, 'Joan Eve Lomoljo', '   ', '', '', '', '', '', '', 'active', 0),
(564, 'Joan Marie Quimno', '   ', '', '', '', '', '', '', 'active', 0),
(565, 'Joan Tampus', '   ', '', '', '', '', '', '', 'active', 0),
(566, 'Joan Torreon', '   ', '', '', '', '', '', '', 'active', 0),
(567, 'Joanna Fye Trajico', '   ', '', '', '', '', '', '', 'active', 0),
(568, 'Joanne Cabarles', '   ', '', '', '', '', '', '', 'active', 0),
(569, 'Joanne Castillote', '   ', '', '', '', '', '', '', 'active', 0),
(570, 'Joanne Espina', '   ', '', '', '', '', '', '', 'active', 0),
(571, 'Joanne Rose Bionat', '   ', '', '', '', '', '', '', 'active', 0),
(572, 'Job Paul Cereño', '   ', '', '', '', '', '', '', 'active', 0),
(573, 'Jobs DB Philippines, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(574, 'Jobstreet.com Philippines Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(575, 'Jocel Arong', '   ', '', '', '', '', '', '', 'active', 0),
(576, 'Jocel Bajao', '   ', '', '', '', '', '', '', 'active', 0),
(577, 'Joe Keehan', '   ', '', '', '', '', '', '', 'active', 0),
(578, 'Joebert Apiladas', '   ', '', '', '', '', '', '', 'active', 0),
(579, 'Joevanne Te', '   ', '', '', '', '', '', '', 'active', 0),
(580, 'Joey Tecson', '   ', '', '', '', '', '', '', 'active', 0),
(581, 'Johannah Lunor', '   ', '', '', '', '', '', '', 'active', 0),
(582, 'Johannah Tupas', '   ', '', '', '', '', '', '', 'active', 0),
(583, 'John Alexander Macdonald', '   ', '', '', '', '', '', '', 'active', 0),
(584, 'John Carlo Velasco', '   ', '', '', '', '', '', '', 'active', 0),
(585, 'John Cawley', '   ', '', '', '', '', '', '', 'active', 0),
(586, 'John Derstine', '   ', '', '', '', '', '', '', 'active', 0),
(587, 'John Henry Montebon', '   ', '', '', '', '', '', '', 'active', 0),
(588, 'John Joseph Mercado v', '   ', '', '', '', '', '', '', 'active', 0),
(589, 'John Lawrence Mercado-AP-others', '   ', '', '', '', '', '', '', 'active', 0),
(590, 'John Lawrence Mercado v', '   ', '', '', '', '', '', '', 'active', 0),
(591, 'John Marc Salantandre', '   ', '', '', '', '', '', '', 'active', 0),
(592, 'John Mark De Los Santos', '   ', '', '', '', '', '', '', 'active', 0),
(593, 'John Paul Espejon', '   ', '', '', '', '', '', '', 'active', 0),
(594, 'John Ray Gador', '   ', '', '', '', '', '', '', 'active', 0),
(595, 'Joice Maissa Fernandez', '   ', '', '', '', '', '', '', 'active', 0),
(596, 'Jolie Ann Cenas', '   ', '', '', '', '', '', '', 'active', 0),
(597, 'Jollibee Foods Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(598, 'Joma Valor Echavez', '   ', '', '', '', '', '', '', 'active', 0),
(599, 'Jon-Paul Raymond', '   ', '', '', '', '', '', '', 'active', 0),
(600, 'Jona Ville Dugaduga', '   ', '', '', '', '', '', '', 'active', 0),
(601, 'Jonabeth Minoza', '   ', '', '', '', '', '', '', 'active', 0),
(602, 'Jonah Marie Alegre', '   ', '', '', '', '', '', '', 'active', 0),
(603, 'Jonathan Cabilao', '   ', '', '', '', '', '', '', 'active', 0),
(604, 'Jonathan Casio', '   ', '', '', '', '', '', '', 'active', 0),
(605, 'Joniefer Ligas', '   ', '', '', '', '', '', '', 'active', 0),
(606, 'Jonnabel Ibanez', '   ', '', '', '', '', '', '', 'active', 0),
(607, 'Jonny Ong', '   ', '', '', '', '', '', '', 'active', 0),
(608, 'Joselito Baena', '   ', '', '', '', '', '', '', 'active', 0),
(609, 'Joselito Tabon', '   ', '', '', '', '', '', '', 'active', 0),
(610, 'Joselo Atup', '   ', '', '', '', '', '', '', 'active', 0),
(611, 'Joseph Cabahug', '   ', '', '', '', '', '', '', 'active', 0),
(612, 'Joseph Marchello', '   ', '', '', '', '', '', '', 'active', 0),
(613, 'Joseph Roa', '   ', '', '', '', '', '', '', 'active', 0),
(614, 'Joseph Satumcacal', '   ', '', '', '', '', '', '', 'active', 0),
(615, 'Joshua Margaja', '   ', '', '', '', '', '', '', 'active', 0),
(616, 'Joshua Pritchard', '   ', '', '', '', '', '', '', 'active', 0),
(617, 'Jovima Management & Dev''t. Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(618, 'Joyce Ann Rosima', '   ', '', '', '', '', '', '', 'active', 0),
(619, 'Jude Mathew Dela Cruz', '   ', '', '', '', '', '', '', 'active', 0),
(620, 'Judilyn A. Basubas', '   ', '', '', '', '', '', '', 'active', 0),
(621, 'Julie-Ann Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(622, 'Julie Kornerup', '   ', '', '', '', '', '', '', 'active', 0),
(623, 'Julius Señara', '   ', '', '', '', '', '', '', 'active', 0),
(624, 'Julymarie Demotor', '   ', '', '', '', '', '', '', 'active', 0),
(625, 'June Keith Ramirez', '   ', '', '', '', '', '', '', 'active', 0),
(626, 'June Tesorero', '   ', '', '', '', '', '', '', 'active', 0),
(627, 'Junelyn Sandigan', '   ', '', '', '', '', '', '', 'active', 0),
(628, 'Junrick Jake Laude', '   ', '', '', '', '', '', '', 'active', 0),
(629, 'Jupiter Apa', '   ', '', '', '', '', '', '', 'active', 0),
(630, 'Juveryn Undap', '   ', '', '', '', '', '', '', 'active', 0),
(631, 'Juvy Ann Mendez', '   ', '', '', '', '', '', '', 'active', 0),
(632, 'Juvy Limpangog', '   ', '', '', '', '', '', '', 'active', 0),
(633, 'JV Shop ''N Shop Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(634, 'KAINOShealth Management, Inc.', '10th flr Metrobank Plaza Jones Ave. Cebu City  ', '032-2556300', '', '', '', '', '', 'active', 0),
(635, 'Kamayo Express', '   ', '', '', '', '', '', '', 'active', 0),
(636, 'Kamayo Express Grocery', '   ', '', '', '', '', '', '', 'active', 0),
(637, 'Kareen Ann Surmillon', '   ', '', '', '', '', '', '', 'active', 0),
(638, 'Karen Marie B. Pogoy', '   ', '', '', '', '', '', '', 'active', 0),
(639, 'Karen May Ferrer', '   ', '', '', '', '', '', '', 'active', 0),
(640, 'Karl David Escoto', '   ', '', '', '', '', '', '', 'active', 0),
(641, 'KARTZONE', 'f. Cabahug St,Ayala Access Road, Mabolo Cebu City  ', '', '', '', '', '', '', 'active', 0),
(642, 'Karyl Bolences', '   ', '', '', '', '', '', '', 'active', 0),
(643, 'Kate Grace Anusuriya', '   ', '', '', '', '', '', '', 'active', 0),
(644, 'Katherine Digamon', '   ', '', '', '', '', '', '', 'active', 0),
(645, 'Katherine Homecillo', '   ', '', '', '', '', '', '', 'active', 0),
(646, 'Kathleen Joy R. Suarez', '   ', '', '', '', '', '', '', 'active', 0),
(647, 'Kay Cadilig', '   ', '', '', '', '', '', '', 'active', 0),
(648, 'KC Pelaez', '   ', '', '', '', '', '', '', 'active', 0),
(649, 'KCT International Travel Services', '   ', '', '', '', '', '', '', 'active', 0),
(650, 'Kein Enterprises', '1-85 Osmena ext. Gochan Hills Camputhaw Cebu City TIN#200-191-535V ', '', '', '', '', '', '', 'active', 0),
(651, 'Keith Everette', '   ', '', '', '', '', '', '', 'active', 0),
(652, 'Ken Ryan B. Gimpayan', '   ', '', '', '', '', '', '', 'active', 0),
(653, 'Kenneth Brian Aninipok', '   ', '', '', '', '', '', '', 'active', 0),
(654, 'Kenneth Res Arong', '   ', '', '', '', '', '', '', 'active', 0),
(655, 'Kenneth Ross Standen', '   ', '', '', '', '', '', '', 'active', 0),
(656, 'Kenneth Subaria', '   ', '', '', '', '', '', '', 'active', 0),
(657, 'Kim Cordle', '   ', '', '', '', '', '', '', 'active', 0),
(658, 'Kim Soo Jung', '   ', '', '', '', '', '', '', 'active', 0),
(659, 'Kim Tae Ho', '   ', '', '', '', '', '', '', 'active', 0),
(660, 'Kim Taeho', '   ', '', '', '', '', '', '', 'active', 0),
(661, 'Kim Yuji', '   ', '', '', '', '', '', '', 'active', 0),
(662, 'King''s Quality Foods (CEBU), Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(663, 'King Godfrey Estardo', '   ', '', '', '', '', '', '', 'active', 0),
(664, 'KP LABELS', '   ', '', '', '', '', '', '', 'active', 0),
(665, 'Kristine Caress Morales', '   ', '', '', '', '', '', '', 'active', 0),
(666, 'Kristine Joy Anusuriya', '   ', '', '', '', '', '', '', 'active', 0),
(667, 'Kristine Simplicio', '   ', '', '', '', '', '', '', 'active', 0),
(668, 'Kristoffer Serrano', '   ', '', '', '', '', '', '', 'active', 0),
(669, 'Krustinna Alvarez', '   ', '', '', '', '', '', '', 'active', 0),
(670, 'KTR Marketing/Celso Rivera, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(671, 'KTR Marketing/James Jonas Rivera', '   ', '', '', '', '', '15 days', '', 'active', 0),
(672, 'Kyung Song Nyel', '   ', '', '', '', '', '', '', 'active', 0),
(673, 'La Nueva Supermart', '   ', '', '', '', '', '7 days', '', 'active', 0),
(674, 'Laarni Venus Marie Giango', '   ', '', '', '', '', '', '', 'active', 0),
(675, 'Lady Grace Sarcena', '   ', '', '', '', '', '', '', 'active', 0),
(676, 'Lahug Glass Supply', '   ', '', '', '', '', '', '', 'active', 0),
(677, 'Laine Bresette Alusin', '   ', '', '', '', '', '', '', 'active', 0),
(678, 'Lalie Ledesma', '   ', '', '', '', '', '', '', 'active', 0),
(679, 'Lani Bersales', '   ', '', '', '', '', '', '', 'active', 0),
(680, 'Lanie Rose Colongan', '   ', '', '', '', '', '', '', 'active', 0),
(681, 'Lannil Bering', '   ', '', '', '', '', '', '', 'active', 0),
(682, 'Lapu-Lapu City Treasurer''s Office', '   ', '', '', '', '', '', '', 'active', 0),
(683, 'Lapu-Lapu Electronics', '   ', '', '', '', '', '', '', 'active', 0),
(684, 'Larbeth Wong', '   ', '', '', '', '', '', '', 'active', 0);
INSERT INTO `suppliers` (`supplier_id`, `name`, `address`, `phone_no`, `mobile_no`, `faxnum`, `note`, `terms`, `company_name`, `status`, `date_added`) VALUES
(685, 'Late Rooms Ltd', '   ', '', '', '', '', '', '', 'active', 0),
(686, 'Lavinea Hopkirk', '   ', '', '', '', '', '', '', 'active', 0),
(687, 'Le Tat Thang', '   ', '', '', '', '', '', '', 'active', 0),
(688, 'Leah Marie Inopia', '   ', '', '', '', '', '', '', 'active', 0),
(689, 'Lee Dong Hee v', '   ', '', '', '', '', '', '', 'active', 0),
(690, 'Lee Hoseog', '   ', '', '', '', '', '', '', 'active', 0),
(691, 'Leilane Villahermosa', '   ', '', '', '', '', '', '', 'active', 0),
(692, 'Leizel Lim', '   ', '', '', '', '', '', '', 'active', 0),
(693, 'Lendon Macatonog', '   ', '', '', '', '', '', '', 'active', 0),
(694, 'Leofe Paquibot', '   ', '', '', '', '', '', '', 'active', 0),
(695, 'Leon John Hill', '   ', '', '', '', '', '', '', 'active', 0),
(696, 'Leonila C. Solon/CEC Agent', '   ', '', '', '', '', '', '', 'active', 0),
(697, 'Leopoldo Daño', '   ', '', '', '', '', '', '', 'active', 0),
(698, 'Lera Lumactod', '   ', '', '', '', '', '', '', 'active', 0),
(699, 'Lesley Joy Secusana', '   ', '', '', '', '', '', '', 'active', 0),
(700, 'Leslie Natividad', '   ', '', '', '', '', '', '', 'active', 0),
(701, 'Lexia Cel Sabuga', '   ', '', '', '', '', '', '', 'active', 0),
(702, 'Lexro Engineering Services', '   ', '', '', '', '', '', '', 'active', 0),
(703, 'Liezel E. Abretil', '   ', '', '', '', '', '', '', 'active', 0),
(704, 'Liezel Elcamel', '   ', '', '', '', '', '', '', 'active', 0),
(705, 'Liezel Sonon', '   ', '', '', '', '', '', '', 'active', 0),
(706, 'Liezl Sacnanas', '   ', '', '', '', '', '', '', 'active', 0),
(707, 'Liezl Tampon', '   ', '', '', '', '', '', '', 'active', 0),
(708, 'Light  Bulb Production', '   ', '', '', '', '', '', '', 'active', 0),
(709, 'Lilane Herana', '   ', '', '', '', '', '', '', 'active', 0),
(710, 'Lilibeth Conado', '   ', '', '', '', '', '', '', 'active', 0),
(711, 'Lilibeth Grace Bernabat', '   ', '', '', '', '', '', '', 'active', 0),
(712, 'Lindsay Nea Belocura', '   ', '', '', '', '', '', '', 'active', 0),
(713, 'Lindsey Masiglat', '   ', '', '', '', '', '', '', 'active', 0),
(714, 'Living Water Filtration & Refilling Stati', '   ', '', '', '', '', '15 days', '', 'active', 0),
(715, 'Liza F. Gutladera', '   ', '', '', '', '', '', '', 'active', 0),
(716, 'Liza Francisco', '   ', '', '', '', '', '', '', 'active', 0),
(717, 'Lloyd Aliviado', '   ', '', '', '', '', '', '', 'active', 0),
(718, 'Lloyd Raymond Wells', '   ', '', '', '', '', '', '', 'active', 0),
(719, 'Loida Salve', '   ', '', '', '', '', '', '', 'active', 0),
(720, 'Lordboy Cabrera', '   ', '', '', '', '', '', '', 'active', 0),
(721, 'Lorelie Salve', '   ', '', '', '', '', '', '', 'active', 0),
(722, 'Lorena Nabe Vergara', '   ', '', '', '', '', '', '', 'active', 0),
(723, 'Lorilen Capendit', '   ', '', '', '', '', '', '', 'active', 0),
(724, 'Lorraine Cuñado', '   ', '', '', '', '', '', '', 'active', 0),
(725, 'Lorraine Michelle Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(726, 'Lorrie Fay Chu', '   ', '', '', '', '', '', '', 'active', 0),
(727, 'Loth Orozco', '   ', '', '', '', '', '', '', 'active', 0),
(728, 'Louigie Malubay', '   ', '', '', '', '', '', '', 'active', 0),
(729, 'Lovely Fuentes', '   ', '', '', '', '', '', '', 'active', 0),
(730, 'Lovely Labra', '   ', '', '', '', '', '', '', 'active', 0),
(731, 'LPN Manna Advertising Co.', '   ', '', '', '', '', '', '', 'active', 0),
(732, 'Luanna Pit Magdales', '   ', '', '', '', '', '', '', 'active', 0),
(733, 'Luchie-Lee Longinos', '   ', '', '', '', '', '', '', 'active', 0),
(734, 'Lucille Alinsug', '   ', '', '', '', '', '', '', 'active', 0),
(735, 'Lucille Belacho', '   ', '', '', '', '', '', '', 'active', 0),
(736, 'Lucille Ceniza', '   ', '', '', '', '', '', '', 'active', 0),
(737, 'Luzviminda Cervantes', '   ', '', '', '', '', '', '', 'active', 0),
(738, 'Lydia Marikit Aworuwa', '   ', '', '', '', '', '', '', 'active', 0),
(739, 'Lydia Nacua', '   ', '', '', '', '', '7 days', '', 'active', 0),
(740, 'Lyndel Inot', '   ', '', '', '', '', '', '', 'active', 0),
(741, 'Lynholly Alforque', '   ', '', '', '', '', '', '', 'active', 0),
(742, 'MA-AY Enterprises', '   ', '', '', '', '', '30 days', '', 'active', 0),
(743, 'Ma. Concetta Cabañero', '   ', '', '', '', '', '', '', 'active', 0),
(744, 'Ma. Cristy D. Angchangco', '   ', '', '', '', '', '', '', 'active', 0),
(745, 'Ma. Gypsy Desquitado', '   ', '', '', '', '', '', '', 'active', 0),
(746, 'Ma. Jennylyn Atizon', '   ', '', '', '', '', '', '', 'active', 0),
(747, 'Ma. Linda T. Ompad', '   ', '', '', '', '', '', '', 'active', 0),
(748, 'Ma. Luisa Pepito', '   ', '', '', '', '', '', '', 'active', 0),
(749, 'Ma. Rodelia Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(750, 'Ma. Theresa Adolfo', '   ', '', '', '', '', '', '', 'active', 0),
(751, 'Ma. Theresa Rom', '   ', '', '', '', '', '', '', 'active', 0),
(752, 'Ma. Veronica Arca', '   ', '', '', '', '', '', '', 'active', 0),
(753, 'Mabel Baguio', '   ', '', '', '', '', '', '', 'active', 0),
(754, 'Macrolite Electrical', '   ', '', '', '', '', '', '', 'active', 0),
(755, 'Macrolite Hardware', '307 Magallanes St. Cebu City  ', '253-3755', '', '', '', '', '', 'active', 0),
(756, 'Mactan Electric Company, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(757, 'Mactan Gas Center', '   ', '', '', '', '', '', '', 'active', 0),
(758, 'Mae Joy Doroy', '   ', '', '', '', '', '', '', 'active', 0),
(759, 'Maebeth Mullero', '   ', '', '', '', '', '', '', 'active', 0),
(760, 'Main Systems Incorporated', '   ', '', '', '', '', '', '', 'active', 0),
(761, 'Mandaue Foam Industries, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(762, 'Mangelie Verde', '   ', '', '', '', '', '', '', 'active', 0),
(763, 'Mansmith and Fielders, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(764, 'Marc and Mattheus', '   ', '', '', '', '', '', '', 'active', 0),
(765, 'Marc Andre Ginete', '   ', '', '', '', '', '', '', 'active', 0),
(766, 'Marcelina Gesalago', '   ', '', '', '', '', '', '', 'active', 0),
(767, 'Marciano Agbay', '   ', '', '', '', '', '', '', 'active', 0),
(768, 'Marcos Pio Alarin', '   ', '', '', '', '', '', '', 'active', 0),
(769, 'Marcy Bansag', '   ', '', '', '', '', '', '', 'active', 0),
(770, 'Margallaine Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(771, 'Margarette Grace Barz', '   ', '', '', '', '', '', '', 'active', 0),
(772, 'Marguad Trading and Services Co.', '   ', '', '', '', '', '', '', 'active', 0),
(773, 'Maria Christine Go', '   ', '', '', '', '', '', '', 'active', 0),
(774, 'Maria Dolores Tedor', '   ', '', '', '', '', '', '', 'active', 0),
(775, 'Maria Eloisa Lasan', '   ', '', '', '', '', '', '', 'active', 0),
(776, 'Maria Joanna Angelica Lacia', '   ', '', '', '', '', '', '', 'active', 0),
(777, 'Maria Kenvi Suan', '   ', '', '', '', '', '', '', 'active', 0),
(778, 'Maria Kristina Quisido', '   ', '', '', '', '', '', '', 'active', 0),
(779, 'Maria Linda Danes', '   ', '', '', '', '', '', '', 'active', 0),
(780, 'Maria Linen Baguio', '   ', '', '', '', '', '', '', 'active', 0),
(781, 'Maria Lourdes Calma', '   ', '', '', '', '', '', '', 'active', 0),
(782, 'Maria Lourdes Noval', '   ', '', '', '', '', '', '', 'active', 0),
(783, 'Maria Reima Potot', '   ', '', '', '', '', '', '', 'active', 0),
(784, 'Maria Theresa Pama', '   ', '', '', '', '', '', '', 'active', 0),
(785, 'Maria Veronica Arca', '   ', '', '', '', '', '', '', 'active', 0),
(786, 'Maria Victoria Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(787, 'Marian D. Canama', '   ', '', '', '', '', '', '', 'active', 0),
(788, 'Marianne Katrina Velasco', '   ', '', '', '', '', '', '', 'active', 0),
(789, 'Maribeth Sembrano', '   ', '', '', '', '', '', '', 'active', 0),
(790, 'Maricar Tito', '   ', '', '', '', '', '', '', 'active', 0),
(791, 'Maricel Cañete', '   ', '', '', '', '', '', '', 'active', 0),
(792, 'Maricel Gemilgo', '   ', '', '', '', '', '', '', 'active', 0),
(793, 'Maricor Carmela Amoguis', '   ', '', '', '', '', '', '', 'active', 0),
(794, 'Maridel Marketing', '2 Sycamore St. Pacific Grand Villas Marigondon Lapu-lapu City 496-8412 171-984-561-000', '', '', '', '', '', '', 'active', 0),
(795, 'Marie Angelique Villamor', '   ', '', '', '', '', '', '', 'active', 0),
(796, 'Marie Joy Madeja', '   ', '', '', '', '', '', '', 'active', 0),
(797, 'Marie Joy Martinez', '   ', '', '', '', '', '', '', 'active', 0),
(798, 'Marie Rose Escabas', '   ', '', '', '', '', '', '', 'active', 0),
(799, 'Mariedel Ardina', '   ', '', '', '', '', '', '', 'active', 0),
(800, 'Mariefe Barata', '   ', '', '', '', '', '', '', 'active', 0),
(801, 'Mariejoy Martinez', '   ', '', '', '', '', '', '', 'active', 0),
(802, 'Mariel Cortes', '   ', '', '', '', '', '', '', 'active', 0),
(803, 'marigold Paclipan', '   ', '', '', '', '', '', '', 'active', 0),
(804, 'Marigold Zaballero/Elsa Lim', '   ', '', '', '', '', '', '', 'active', 0),
(805, 'Marilyn Torres', '   ', '', '', '', '', '', '', 'active', 0),
(806, 'Marissa Noraida Dalupang', '   ', '', '', '', '', '', '', 'active', 0),
(807, 'Marivel Bihag', '   ', '', '', '', '', '', '', 'active', 0),
(808, 'Marivic Calidguid', '   ', '', '', '', '', '', '', 'active', 0),
(809, 'Marivic Dalugdog', '   ', '', '', '', '', '', '', 'active', 0),
(810, 'Mariza Duhaylungsod', '   ', '', '', '', '', '', '', 'active', 0),
(811, 'Mariza Garcia', '   ', '', '', '', '', '', '', 'active', 0),
(812, 'Marjorie Miralles', '   ', '', '', '', '', '', '', 'active', 0),
(813, 'Mark Anthony Tan', '   ', '', '', '', '', '', '', 'active', 0),
(814, 'Mark Carmelo Ganar', '   ', '', '', '', '', '', '', 'active', 0),
(815, 'Mark Machacon', '   ', '', '', '', '', '', '', 'active', 0),
(816, 'Marlowe Toledo Villagonzalo', '   ', '', '', '', '', '', '', 'active', 0),
(817, 'Marquel Andrico', '   ', '', '', '', '', '', '', 'active', 0),
(818, 'Marquel Andrino', '   ', '', '', '', '', '', '', 'active', 0),
(819, 'Marrycar Aquino', '   ', '', '', '', '', '', '', 'active', 0),
(820, 'Marsha Valencia', '   ', '', '', '', '', '', '', 'active', 0),
(821, 'Marteniana H. Tulomia', '   ', '', '', '', '', '', '', 'active', 0),
(822, 'Marvin Ascura', '   ', '', '', '', '', '', '', 'active', 0),
(823, 'Marvin Bryan Gayotin', '   ', '', '', '', '', '', '', 'active', 0),
(824, 'Mary-Ann Ylanan', '   ', '', '', '', '', '', '', 'active', 0),
(825, 'Mary Angelique Villamor', '   ', '', '', '', '', '', '', 'active', 0),
(826, 'Mary Catherine Ariosa', '   ', '', '', '', '', '', '', 'active', 0),
(827, 'Mary Claire Jurolan', '   ', '', '', '', '', '', '', 'active', 0),
(828, 'Mary Emily Ymbong', '   ', '', '', '', '', '', '', 'active', 0),
(829, 'Mary Faith Bate', '   ', '', '', '', '', '', '', 'active', 0),
(830, 'Mary Grace Auman', '   ', '', '', '', '', '', '', 'active', 0),
(831, 'Mary Grace Cantero', '   ', '', '', '', '', '', '', 'active', 0),
(832, 'Mary Grace Labiste', '   ', '', '', '', '', '', '', 'active', 0),
(833, 'Mary Grace N. Torbeso', '   ', '', '', '', '', '', '', 'active', 0),
(834, 'Mary Grace Torbeso', '   ', '', '', '', '', '', '', 'active', 0),
(835, 'Mary Jane Gutib', '   ', '', '', '', '', '', '', 'active', 0),
(836, 'Mary Jane Orbeta', '   ', '', '', '', '', '', '', 'active', 0),
(837, 'Mary Jane S. Orbeta', '   ', '', '', '', '', '', '', 'active', 0),
(838, 'Mary Jonafe Alcover', '   ', '', '', '', '', '', '', 'active', 0),
(839, 'Mary Joy Debalucos', '   ', '', '', '', '', '', '', 'active', 0),
(840, 'Mary Joy Maglahus', '   ', '', '', '', '', '', '', 'active', 0),
(841, 'Mary Joy Manglahus', '   ', '', '', '', '', '', '', 'active', 0),
(842, 'Mary June Tesorero', '   ', '', '', '', '', '', '', 'active', 0),
(843, 'Mary Lad Caliao', '   ', '', '', '', '', '', '', 'active', 0),
(844, 'Mary Maystall Labus', '   ', '', '', '', '', '', '', 'active', 0),
(845, 'Mary Melany Ralloma', '   ', '', '', '', '', '', '', 'active', 0),
(846, 'Marygrace Cantero', '   ', '', '', '', '', '', '', 'active', 0),
(847, 'Masashi Nagata', '   ', '', '', '', '', '', '', 'active', 0),
(848, 'Matt Gershom Talisic', '   ', '', '', '', '', '', '', 'active', 0),
(849, 'Maureen Bayo-on', '   ', '', '', '', '', '', '', 'active', 0),
(850, 'Maureen Bayon-on', '   ', '', '', '', '', '', '', 'active', 0),
(851, 'Maureen Michelle Tan', '   ', '', '', '', '', '', '', 'active', 0),
(852, 'Maximax Systems, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(853, 'May De Vera', '   ', '', '', '', '', '', '', 'active', 0),
(854, 'May Miot', '   ', '', '', '', '', '', '', 'active', 0),
(855, 'Mayfifth Patalinjug', '   ', '', '', '', '', '', '', 'active', 0),
(856, 'Mayflor Montejo', '   ', '', '', '', '', '', '', 'active', 0),
(857, 'Maynard Sian', '   ', '', '', '', '', '', '', 'active', 0),
(858, 'MCM Water Refilling Station', 'TDB Bldg. National Hiway Brgy.Pajo, Lapu-lapu City  ', '342-8714', '', '', '', '', '', 'active', 0),
(859, 'Medelyn Igot', '   ', '', '', '', '', '', '', 'active', 0),
(860, 'Mega Modern Gasul Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(861, 'Melanio Zaraga', '   ', '', '', '', '', '', '', 'active', 0),
(862, 'Melissa Longakit', '   ', '', '', '', '', '', '', 'active', 0),
(863, 'Meliza R. Abayon', '   ', '', '', '', '', '', '', 'active', 0),
(864, 'MELJOY TALINGTING', '   ', '', '', '', '', '', '', 'active', 0),
(865, 'Mellibeth Go', '   ', '', '', '', '', '', '', 'active', 0),
(866, 'Mellicent Ponce', '   ', '', '', '', '', '', '', 'active', 0),
(867, 'Melnie Chavez', '   ', '', '', '', '', '', '', 'active', 0),
(868, 'Merlin T. Manalon', '   ', '', '', '', '', '', '', 'active', 0),
(869, 'Mervin Capili', '   ', '', '', '', '', '', '', 'active', 0),
(870, 'Meryll Jayne Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(871, 'Mesias Alinsunod', '   ', '', '', '', '', '', '', 'active', 0),
(872, 'Mesraem King Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(873, 'Metaluxe Cebu City', 'Ground Flr Glory Bldg Mabini St Cebu City 102-716-363-003 ', '', '', '', '', '', '', 'active', 0),
(874, 'Metro Sports', '   ', '', '', '', '', '', '', 'active', 0),
(875, 'Metro Sports Badminton Club', '   ', '', '', '', '', '', '', 'active', 0),
(876, 'Metropolitan Cebu Water District', '   ', '', '', '', '', '', '', 'active', 0),
(877, 'Mi Suk Song', '   ', '', '', '', '', '', '', 'active', 0),
(878, 'Mia Pearl Geron', '   ', '', '', '', '', '', '', 'active', 0),
(879, 'Michael B. Berry', '   ', '', '', '', '', '', '', 'active', 0),
(880, 'Michael Birdzell', '   ', '', '', '', '', '', '', 'active', 0),
(881, 'Michael Gerard Fults', '   ', '', '', '', '', '', '', 'active', 0),
(882, 'Michael Short', '   ', '', '', '', '', '', '', 'active', 0),
(883, 'Michael Sulit', '   ', '', '', '', '', '', '', 'active', 0),
(884, 'Michelle A. Cosep', '   ', '', '', '', '', '', '', 'active', 0),
(885, 'Michelle Dianne Arche', '   ', '', '', '', '', '', '', 'active', 0),
(886, 'Michelle Florenda', '   ', '', '', '', '', '', '', 'active', 0),
(887, 'Michelle Go', '   ', '', '', '', '', '', '', 'active', 0),
(888, 'Michelle Ramayla', '   ', '', '', '', '', '', '', 'active', 0),
(889, 'Miki Iwamoto and/or CASH', '   ', '', '', '', '', '', '', 'active', 0),
(890, 'Mildred Baay', '   ', '', '', '', '', '', '', 'active', 0),
(891, 'Milton McGlynn Worthington', '   ', '', '', '', '', '', '', 'active', 0),
(892, 'Minerva Adem', '   ', '', '', '', '', '', '', 'active', 0),
(893, 'Minnie Burlaos', '   ', '', '', '', '', '', '', 'active', 0),
(894, 'Minnielou Chavez', '   ', '', '', '', '', '', '', 'active', 0),
(895, 'Miraflor Maghinay', '   ', '', '', '', '', '', '', 'active', 0),
(896, 'Miraflor Orio', '   ', '', '', '', '', '', '', 'active', 0),
(897, 'Mirasol Bontilao', '   ', '', '', '', '', '', '', 'active', 0),
(898, 'Miren Go', '   ', '', '', '', '', '', '', 'active', 0),
(899, 'MJ Auto ID', 'Escano Bldg. Door 4 P. del Rosario St. Cebu City TIN#181-176-786-000 ', '', '', '', '', '', '', 'active', 0),
(900, 'MJ Auto ID Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(901, 'MK Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(902, 'Mom''s Classy Decors & Dress Shop', '   ', '', '', '', '', '', '', 'active', 0),
(903, 'Monica Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(904, 'Monna Zelda Ursal', '   ', '', '', '', '', '', '', 'active', 0),
(905, 'Mozcom, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(906, 'MUFARRIJ ALSUBAIE', '   ', '', '', '', '', '', '', 'active', 0),
(907, 'Mygill Rago', '   ', '', '', '', '', '', '', 'active', 0),
(908, 'Mykha Dela Pena', '   ', '', '', '', '', '', '', 'active', 0),
(909, 'Mylene Bonganciso', '   ', '', '', '', '', '', '', 'active', 0),
(910, 'Mylene Chavez', '   ', '', '', '', '', '', '', 'active', 0),
(911, 'Mynimo Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(912, 'Myra Monteclar', '   ', '', '', '', '', '', '', 'active', 0),
(913, 'Myrla Bantilan', '   ', '', '', '', '', '', '', 'active', 0),
(914, 'Myrnali Gustilo v', '   ', '', '', '', '', '', '', 'active', 0),
(915, 'Nadine Rorie Jumao-as', '   ', '', '', '', '', '', '', 'active', 0),
(916, 'Naomi Villagonzalo', '   ', '', '', '', '', '', '', 'active', 0),
(917, 'Narumol Jewyoungphol', '   ', '', '', '', '', '', '', 'active', 0),
(918, 'Natasha Juaniza', '   ', '', '', '', '', '', '', 'active', 0),
(919, 'National Bookstore', '   ', '', '', '', '', '30 days', '', 'active', 0),
(920, 'Need Ink', '   ', '', '', '', '', '30 days', '', 'active', 0),
(921, 'Needs and Solutions', '   ', '', '', '', '', '', '', 'active', 0),
(922, 'Neff Grace Tan', '   ', '', '', '', '', '', '', 'active', 0),
(923, 'Neil Braw', '   ', '', '', '', '', '', '', 'active', 0),
(924, 'Neil Nazareno', '   ', '', '', '', '', '', '', 'active', 0),
(925, 'Nelsa Traveno', '   ', '', '', '', '', '', '', 'active', 0),
(926, 'Nelsa Travero', '   ', '', '', '', '', '', '', 'active', 0),
(927, 'NEMPEX Pest Control', '201 A Hiway Tagunol Cogon Pardo Cebu City  ', '', '', '', '', '15 days', '', 'active', 0),
(928, 'Neo-Konsult', '   ', '', '', '', '', '', '', 'active', 0),
(929, 'Neriza Trasmel', '   ', '', '', '', '', '', '', 'active', 0),
(930, 'Newgen Products, Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(931, 'Nguyen Quang Giang', '   ', '', '', '', '', '', '', 'active', 0),
(932, 'Nieva Joy Sevillejo', '   ', '', '', '', '', '', '', 'active', 0),
(933, 'Niño Adonis Litgio', '   ', '', '', '', '', '', '', 'active', 0),
(934, 'Niño Janolino', '   ', '', '', '', '', '', '', 'active', 0),
(935, 'Noe Silario', '   ', '', '', '', '', '', '', 'active', 0),
(936, 'Noe Silorio', '   ', '', '', '', '', '', '', 'active', 0),
(937, 'Noellie Lamis', '   ', '', '', '', '', '', '', 'active', 0),
(938, 'Norris Pat Ruiz', '   ', '', '', '', '', '', '', 'active', 0),
(939, 'NUTECH Marketing', 'Sanciangko cor Borromeo Sts. Cebu City  ', '254-0397', '', '', '204-928-530-000', '', '', 'active', 0),
(940, 'Octagon Computer Superstore', '   ', '', '', '', '', '', '', 'active', 0),
(941, 'Odessa Faye Kong', '   ', '', '', '', '', '', '', 'active', 0),
(942, 'Office of the Building Officials', '   ', '', '', '', '', '', '', 'active', 0),
(943, 'Office of the City Treasurer', '   ', '', '', '', '', '', '', 'active', 0),
(944, 'Ophelia Puson', '   ', '', '', '', '', '', '', 'active', 0),
(945, 'Optima Typographics', '   ', '', '', '', '', '', '', 'active', 0),
(946, 'Orient Star Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(947, 'Osmundo Trinidad, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(948, 'Otelia Nutcha Dacaldacal', '   ', '', '', '', '', '', '', 'active', 0),
(949, 'Othelea Yap', '   ', '', '', '', '', '', '', 'active', 0),
(950, 'Pamela Jane Almaden', '   ', '', '', '', '', '', '', 'active', 0),
(951, 'Pamela Luz Osabel', '   ', '', '', '', '', '', '', 'active', 0),
(952, 'Pan Pacific Travel Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(953, 'Papertech, Inc.', '835 Felipe Pike Street, Bagong Ilog, Pasig City ', '631-5524', '', '', '000-165-824-000', '', '', 'active', 0),
(954, 'Paul Bourassa', '   ', '', '', '', '', '', '', 'active', 0),
(955, 'Paul Fellingham', '   ', '', '', '', '', '', '', 'active', 0),
(956, 'Paul Noel Rendon', '   ', '', '', '', '', '', '', 'active', 0),
(957, 'Paul Sanders', '   ', '', '', '', '', '', '', 'active', 0),
(958, 'Paula Lau', '   ', '', '', '', '', '', '', 'active', 0),
(959, 'Payroll - Part Time Teachers', '   ', '', '', '', '', '', '', 'active', 0),
(960, 'Pearl Gumaroy', '   ', '', '', '', '', '', '', 'active', 0),
(961, 'Perlita Regidor', '   ', '', '', '', '', '', '', 'active', 0),
(962, 'Perri Carpets Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(963, 'Pete Onni', '   ', '', '', '', '', '', '', 'active', 0),
(964, 'Peter Hutchinson', '   ', '', '', '', '', '', '', 'active', 0),
(965, 'Peter Teejay Salgado', '   ', '', '', '', '', '', '', 'active', 0),
(966, 'Petronio B. Cotcot, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(967, 'Philip John Señoren', '   ', '', '', '', '', '', '', 'active', 0),
(968, 'Philip Morris Sarmiento', '   ', '', '', '', '', '', '', 'active', 0),
(969, 'Philipp Tampus', '   ', '', '', '', '', '', '', 'active', 0),
(970, 'Philippine Health Insurance Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(971, 'Philippine Long Distance Telephone Co.', '   ', '', '', '', '', '', '', 'active', 0),
(972, 'Philippine Spring Water Resources, Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(973, 'Philippines Spring Water Resources, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(974, 'Phoebe Marie Luzano', '   ', '', '', '', '', '', '', 'active', 0),
(975, 'Pjlou Abelgas', '   ', '', '', '', '', '', '', 'active', 0),
(976, 'Porferio Alvarez,Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(977, 'Prime Care Cebu', '   ', '', '', '', '', '', '', 'active', 0),
(978, 'Prince Warehouse Club, Inc.', '3rd Ave. North Reclamation Area Cebu City 000-313-880-000 ', '', '', '', '', '', '', 'active', 0),
(979, 'Princess Irene Dy', '   ', '', '', '', '', '', '', 'active', 0),
(980, 'Princess Rea Minguito', '   ', '', '', '', '', '', '', 'active', 0),
(981, 'PTA Software Solution', '   ', '', '', '', '', '', '', 'active', 0),
(982, 'QA Causeway Printers, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(983, 'Queenie D. Valdehueza', '   ', '', '', '', '', '', '', 'active', 0),
(984, 'Queenie Jessale Silvosa', '   ', '', '', '', '', '', '', 'active', 0),
(985, 'R&E Technical Services', 'Fatima Village, Pagsabungan Mandaue City TIN#080-168-287-772 ', '', '', '', '', '', '', 'active', 0),
(986, 'Rachel Cañete', '   ', '', '', '', '', '', '', 'active', 0),
(987, 'Rachel Jane  Riconalla', '   ', '', '', '', '', '', '', 'active', 0),
(988, 'Radical Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(989, 'Rafaella Rowena Niere', '   ', '', '', '', '', '', '', 'active', 0),
(990, 'Rafunzel Bero', '   ', '', '', '', '', '', '', 'active', 0),
(991, 'Rainbow Tours & Travel, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(992, 'Rainbow Tours & Travels, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(993, 'Rajah Park Hotel', 'Fuente Osmeña Cebu City 241-159-332-000  ', '412-3337', '', '', '', '', '', 'active', 0),
(994, 'Ramer Duhino', '   ', '', '', '', '', '', '', 'active', 0),
(995, 'Ramil Madera', '   ', '', '', '', '', '', '', 'active', 0),
(996, 'Ramir Carbonilla', '   ', '', '', '', '', '', '', 'active', 0),
(997, 'Ramir N. Martinez, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(998, 'Ramir Pielago', '   ', '', '', '', '', '', '', 'active', 0),
(999, 'Ramon De Castro', '   ', '', '', '', '', '', '', 'active', 0),
(1000, 'Rana Maria Melgar', '   ', '', '', '', '', '', '', 'active', 0),
(1001, 'Randy Tangag', '   ', '', '', '', '', '', '', 'active', 0),
(1002, 'Raquel Ebarita', '   ', '', '', '', '', '', '', 'active', 0),
(1003, 'Rasmus Frederiksen', '   ', '', '', '', '', '', '', 'active', 0),
(1004, 'Raul Arnibal', '   ', '', '', '', '', '', '', 'active', 0),
(1005, 'RC Goldline Saleshop', 'Door 2 F Ramos St Cebu City 203-918-627-000 ', '', '', '', '', '', '', 'active', 0),
(1006, 'Rebecca Rebaño', '   ', '', '', '', '', '', '', 'active', 0),
(1007, 'Redjing Calma', '   ', '', '', '', '', '', '', 'active', 0),
(1008, 'Redtail Ventures Fund', 'TIN 220-404-819-000   ', '', '', '', '', '', '', 'active', 0),
(1009, 'Reggell Magalso', '   ', '', '', '', '', '', '', 'active', 0),
(1010, 'Regine Clyde Songcoya', '   ', '', '', '', '', '', '', 'active', 0),
(1011, 'Register of Deeds-Lapu-Lapu City', '   ', '', '', '', '', '', '', 'active', 0),
(1012, 'Rei Tanaka c', '   ', '', '', '', '', '', '', 'active', 0),
(1013, 'Rei Tanaka v', '   ', '', '', '', '', '', '', 'active', 0),
(1014, 'Renan Tumulak', '   ', '', '', '', '', '', '', 'active', 0),
(1015, 'Renie Aro', '   ', '', '', '', '', '', '', 'active', 0),
(1016, 'Rennie Aro', '   ', '', '', '', '', '', '', 'active', 0),
(1017, 'Res Kenneth Arong', '   ', '', '', '', '', '', '', 'active', 0),
(1018, 'Restilyn Segovia', '   ', '', '', '', '', '', '', 'active', 0),
(1019, 'Retchie Mae Sotilleza', '   ', '', '', '', '', '', '', 'active', 0),
(1020, 'Rex Alexander I. Woo Aquino', '   ', '', '', '', '', '', '', 'active', 0),
(1021, 'Rex Camay', '   ', '', '', '', '', '', '', 'active', 0),
(1022, 'Rhea Jagmoc', '   ', '', '', '', '', '', '', 'active', 0),
(1023, 'Rhea Papas', '   ', '', '', '', '', '', '', 'active', 0),
(1024, 'Rhea Paula Lau', '   ', '', '', '', '', '', '', 'active', 0),
(1025, 'Rhoda Mae Herrera', '   ', '', '', '', '', '', '', 'active', 0),
(1026, 'Richard Aying', '   ', '', '', '', '', '', '', 'active', 0),
(1027, 'Ricio Salibay', '   ', '', '', '', '', '', '', 'active', 0),
(1028, 'Rico Lanurias', '   ', '', '', '', '', '', '', 'active', 0),
(1029, 'Rinchen Namgay', '   ', '', '', '', '', '', '', 'active', 0),
(1030, 'Rinnah Flores', '   ', '', '', '', '', '', '', 'active', 0),
(1031, 'Risha Mae Regalado', '   ', '', '', '', '', '', '', 'active', 0),
(1032, 'Rita Fe Jayme', '   ', '', '', '', '', '', '', 'active', 0),
(1033, 'Rita Jayme', '   ', '', '', '', '', '', '', 'active', 0),
(1034, 'Rizal Commercial Banking Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1035, 'Rizalene Carcedo', '   ', '', '', '', '', '', '', 'active', 0),
(1036, 'Roan Hurst', '   ', '', '', '', '', '', '', 'active', 0),
(1037, 'Robert Bailey', '   ', '', '', '', '', '', '', 'active', 0),
(1038, 'Robert Whitfield', '   ', '', '', '', '', '', '', 'active', 0),
(1039, 'Robin Nguyen', '   ', '', '', '', '', '', '', 'active', 0),
(1040, 'Rochelle Arante', '   ', '', '', '', '', '', '', 'active', 0),
(1041, 'Rod Hill', '   ', '', '', '', '', '', '', 'active', 0),
(1042, 'Rodmar Colipano', '   ', '', '', '', '', '', '', 'active', 0),
(1043, 'Rodolfo L. Yap', '   ', '', '', '', '', '', '', 'active', 0),
(1044, 'Rodrigo Ellescas', '   ', '', '', '', '', '', '', 'active', 0),
(1045, 'Rodrigo V. Calonia', '   ', '', '', '', '', '', '', 'active', 0),
(1046, 'Roel Molijon', '   ', '', '', '', '', '', '', 'active', 0),
(1047, 'Roel Tiongson', '   ', '', '', '', '', '', '', 'active', 0),
(1048, 'Rogeno Icoy', '   ', '', '', '', '', '', '', 'active', 0),
(1049, 'Rogeno Mar Icoy', '   ', '', '', '', '', '', '', 'active', 0),
(1050, 'Roi Dana Berador', '   ', '', '', '', '', '', '', 'active', 0),
(1051, 'Roland Caranzo', '   ', '', '', '', '', '', '', 'active', 0),
(1052, 'Rolando Rosales', '   ', '', '', '', '', '', '', 'active', 0),
(1053, 'Roldan Aguirre', '   ', '', '', '', '', '', '', 'active', 0),
(1054, 'Rolher Marble Supply', '   ', '', '', '', '', '', '', 'active', 0),
(1055, 'Roli Lendio', '   ', '', '', '', '', '', '', 'active', 0),
(1056, 'Rombert Dakay', '   ', '', '', '', '', '', '', 'active', 0),
(1057, 'Rommel Amante', '   ', '', '', '', '', '', '', 'active', 0),
(1058, 'Ronald Payao', '   ', '', '', '', '', '', '', 'active', 0),
(1059, 'Ronald Restauro', '   ', '', '', '', '', '', '', 'active', 0),
(1060, 'Ronaldo Goliat', '   ', '', '', '', '', '', '', 'active', 0),
(1061, 'Ronit Remulta', '   ', '', '', '', '', '', '', 'active', 0),
(1062, 'Ronnie Pitogo', '   ', '', '', '', '', '', '', 'active', 0),
(1063, 'Ronnie Pitogo{2}', '   ', '', '', '', '', '', '', 'active', 0),
(1064, 'Rosa Lima Ewican', '   ', '', '', '', '', '', '', 'active', 0),
(1065, 'Rosa Ria Son', '   ', '', '', '', '', '', '', 'active', 0),
(1066, 'Rosalyn P. Lobo', '   ', '', '', '', '', '', '', 'active', 0),
(1067, 'Rosario Carpina', '   ', '', '', '', '', '', '', 'active', 0),
(1068, 'Rose Cañedo', '   ', '', '', '', '', '', '', 'active', 0),
(1069, 'Rosemarie Booc', '   ', '', '', '', '', '', '', 'active', 0),
(1070, 'Rosendo Ajeas', '   ', '', '', '', '', '', '', 'active', 0),
(1071, 'Rosmin Gallego', '   ', '', '', '', '', '', '', 'active', 0),
(1072, 'Rosseanne Jade Gaco', '   ', '', '', '', '', '', '', 'active', 0),
(1073, 'Rowena Camocamo', '   ', '', '', '', '', '', '', 'active', 0),
(1074, 'Rowena Degamo', '   ', '', '', '', '', '', '', 'active', 0),
(1075, 'Roxanne Doronila', '   ', '', '', '', '', '', '', 'active', 0),
(1076, 'RTM Traders', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1077, 'Rubelyn Inot', '   ', '', '', '', '', '', '', 'active', 0),
(1078, 'Rubicon Trading', '   ', '', '', '', '', '', '', 'active', 0),
(1079, 'Ruel S. Molijon', '   ', '', '', '', '', '', '', 'active', 0),
(1080, 'Ruth Montebon', '   ', '', '', '', '', '', '', 'active', 0),
(1081, 'Ryan Esclamado', '   ', '', '', '', '', '', '', 'active', 0),
(1082, 'Ryan Jude Espenido', '   ', '', '', '', '', '', '', 'active', 0),
(1083, 'Ryan Sucaldito', '   ', '', '', '', '', '', '', 'active', 0),
(1084, 'S.T. Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(1085, 'Sabrina Theresa Concepcion', '   ', '', '', '', '', '', '', 'active', 0),
(1086, 'Sahlee B. Opone', '   ', '', '', '', '', '', '', 'active', 0),
(1087, 'Salome Espra', '   ', '', '', '', '', '', '', 'active', 0),
(1088, 'Sanitary Care Products Asia, Inc.-Cebu', '   ', '', '', '', '', '30 days', '', 'active', 0),
(1089, 'Sarah Laborte', '   ', '', '', '', '', '', '', 'active', 0),
(1090, 'Sarah Macabantog', '   ', '', '', '', '', '', '', 'active', 0),
(1091, 'Sarah Mae Ando', '   ', '', '', '', '', '', '', 'active', 0),
(1092, 'Sarah Mae Tapec', '   ', '', '', '', '', '', '', 'active', 0),
(1093, 'Sarinah Soliano', '   ', '', '', '', '', '', '', 'active', 0),
(1094, 'Savemore Mactan', '   ', '', '', '', '', '', '', 'active', 0),
(1095, 'Sayoko Konishi v', '   ', '', '', '', '', '', '', 'active', 0),
(1096, 'Sean Millerick', '   ', '', '', '', '', '', '', 'active', 0),
(1097, 'Security Systems Monitoring,Inc.', '51-A MT Bldg. Subangdako Mandaue City 004-755-903-000 ', '', '', '', '', '', '', 'active', 0),
(1098, 'Senta Kreger', '   ', '', '', '', '', '', '', 'active', 0),
(1099, 'Shaeg Criste Albuladora', '   ', '', '', '', '', '', '', 'active', 0),
(1100, 'Shangrila Mactan Island Resort', '   ', '', '', '', '', '', '', 'active', 0),
(1101, 'Sharon Ia Casayan', '   ', '', '', '', '', '', '', 'active', 0),
(1102, 'Sharon Perla Suico', '   ', '', '', '', '', '', '', 'active', 0),
(1103, 'Sharp (Phils.) Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1104, 'Sheila Bactol', '   ', '', '', '', '', '', '', 'active', 0),
(1105, 'Sheila Mae Tero', '   ', '', '', '', '', '', '', 'active', 0),
(1106, 'Sheila Tabuno', '   ', '', '', '', '', '', '', 'active', 0),
(1107, 'Sheila Umbay', '   ', '', '', '', '', '', '', 'active', 0),
(1108, 'Sheila Veyra', '   ', '', '', '', '', '', '', 'active', 0),
(1109, 'Shelley Kei Sato', '   ', '', '', '', '', '', '', 'active', 0),
(1110, 'Shellou Gamus', '   ', '', '', '', '', '', '', 'active', 0),
(1111, 'Shendelen Lumanog', '   ', '', '', '', '', '', '', 'active', 0),
(1112, 'Shene Dablo', '   ', '', '', '', '', '', '', 'active', 0),
(1113, 'Shene Dablo Ragaza', '   ', '', '', '', '', '', '', 'active', 0),
(1114, 'Shene Ragaza', '   ', '', '', '', '', '', '', 'active', 0),
(1115, 'Sherly Garciano', '   ', '', '', '', '', '', '', 'active', 0),
(1116, 'Sherry Mae Beleno', '   ', '', '', '', '', '', '', 'active', 0),
(1117, 'Sherwin Clark Pasana', '   ', '', '', '', '', '', '', 'active', 0),
(1118, 'Sherylyn Abing', '   ', '', '', '', '', '', '', 'active', 0),
(1119, 'Shewalk''s Engravables', '   ', '', '', '', '', '', '', 'active', 0),
(1120, 'shiela veyra', '   ', '', '', '', '', '', '', 'active', 0),
(1121, 'Shiella Bactol', '   ', '', '', '', '', '', '', 'active', 0),
(1122, 'Shiena Joy Tisoy', '   ', '', '', '', '', '', '', 'active', 0),
(1123, 'Shirley Nacua', '   ', '', '', '', '', '', '', 'active', 0),
(1124, 'Shirly Kwen Sabal', '   ', '', '', '', '', '', '', 'active', 0),
(1125, 'Shirra Mae Augusto', '   ', '', '', '', '', '', '', 'active', 0),
(1126, 'Simon Timmins', '   ', '', '', '', '', '', '', 'active', 0),
(1127, 'Skyline Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(1128, 'SM Prime Holdings, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1129, 'SNESS Merchandising and Service', '   ', '', '', '', '', '', '', 'active', 0),
(1130, 'Social Security System', '   ', '', '', '', '', '', '', 'active', 0),
(1131, 'SOFO Construction Services', '73 Echavez st. Cebu City   ', '', '', '', '', '', '', 'active', 0),
(1132, 'Softwash Laundromat', '   ', '', '', '', '', '', '', 'active', 0),
(1133, 'Solid Construction (CEBU) Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1134, 'Solid Electronics Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1135, 'SONAMCO', '   ', '', '', '', '', '', '', 'active', 0),
(1136, 'Sophia Belle Tan', '   ', '', '', '', '', '', '', 'active', 0),
(1137, 'Sophia Godin', '   ', '', '', '', '', '', '', 'active', 0),
(1138, 'Sta. Ana, Rivera & Co.', '   ', '', '', '', '', '', '', 'active', 0),
(1139, 'Stefan Andrei Flores', '   ', '', '', '', '', '', '', 'active', 0),
(1140, 'Stefanie Guitguit', '   ', '', '', '', '', '', '', 'active', 0),
(1141, 'Stellaly Romares', '   ', '', '', '', '', '', '', 'active', 0),
(1142, 'Stephanie Noynay', '   ', '', '', '', '', '', '', 'active', 0),
(1143, 'Stephany Cuico', '   ', '', '', '', '', '', '', 'active', 0),
(1144, 'Stephen Low v', '   ', '', '', '', '', '', '', 'active', 0),
(1145, 'Stephen Rosales', '   ', '', '', '', '', '', '', 'active', 0),
(1146, 'Stronghold Glass and Aluminum Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(1147, 'Sugar Ganancial', '   ', '', '', '', '', '', '', 'active', 0),
(1148, 'Sun.Star Publishing, Inc.', 'P.del Rosario St. Cebu City 000-565-060-000  ', '', '', '', '', '', '', 'active', 0),
(1149, 'Sunshine Pardiñan', '   ', '', '', '', '', '', '', 'active', 0),
(1150, 'Super Hankuk Mart & General Merchandise', '   ', '', '', '', '', '15 days', '', 'active', 0),
(1151, 'Susana Tero', '   ', '', '', '', '', '', '', 'active', 0),
(1152, 'Synchrotek Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1153, 'Tambuli Development Corp.', 'Mactan Island Cebu Philippines 004-488-112-000V ', '', '', '', '', '', '', 'active', 0),
(1154, 'Tanya Francia Yap v', '   ', '', '', '', '', '', '', 'active', 0),
(1155, 'Tanya Lozano', '   ', '', '', '', '', '', '', 'active', 0),
(1156, 'Temmy Meat Shop/Artemia C. Degamo', '   ', '', '', '', '', '', '', 'active', 0),
(1157, 'Teresa Avila', '   ', '', '', '', '', '', '', 'active', 0),
(1158, 'Teresita Susana Omanito', '   ', '', '', '', '', '', '', 'active', 0),
(1159, 'TESDA/Dervis Cenica', '   ', '', '', '', '', '', '', 'active', 0),
(1160, 'Tharsis Marine Sports', '   ', '', '', '', '', '', '', 'active', 0),
(1161, 'The Hive-All Visuals & Lights System', '   ', '', '', '', '', '', '', 'active', 0),
(1162, 'The Hongkong and Shanghai Banking Corpora', '   ', '', '', '', '', '', '', 'active', 0),
(1163, 'The Only Dewfoam Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1164, 'The Tile Shop', '   ', '', '', '', '', '', '', 'active', 0),
(1165, 'The Tile Shpo', '   ', '', '', '', '', '', '', 'active', 0),
(1166, 'Therese Wenneth Cabus', '   ', '', '', '', '', '', '', 'active', 0),
(1167, 'Thinking Tools, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1168, 'Thomas Joseph Keehan', '   ', '', '', '', '', '', '', 'active', 0),
(1169, 'Tiffany Kristine Tan', '   ', '', '', '', '', '', '', 'active', 0),
(1170, 'Tim Transport', '7-A Maria Cristina St., Cebu City  ', '', '', '', '', '', '', 'active', 0),
(1171, 'Tito Aldecoa III', '   ', '', '', '', '', '', '', 'active', 0),
(1172, 'Toni Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(1173, 'Toni Enterprisese', 'Saac II Mactan Lapu Lapu City 184-219-143-000NV  ', '340-4399', '', '', '', '', '', 'active', 0),
(1174, 'Toni Rose Tan', '   ', '', '', '', '', '', '', 'active', 0),
(1175, 'Tower Medical Distributing Co., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1176, 'Tracy Lao', '   ', '', '', '', '', '', '', 'active', 0),
(1177, 'Treasure Island', '   ', '', '', '', '', 'Due on receipt', '', 'active', 0),
(1178, 'Trevor Trinidad', '   ', '', '', '', '', '', '', 'active', 0),
(1179, 'Tricom Systems (Philippines), Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1180, 'Tuzca Camille Cortes', '   ', '', '', '', '', '', '', 'active', 0),
(1181, 'Tzu Hsuan Tsai v', '   ', '', '', '', '', '', '', 'active', 0),
(1182, 'U-BIX Corporation', 'Dr.2 YMCA Arcade, Osmena Boulevard, Cebu City ', '416-5123', '', '', '', '', '', 'active', 0),
(1183, 'UHM SONS CORPORATION', '   ', '', '', '', '', '', '', 'active', 0),
(1184, 'Unicable TV, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1185, 'Union Bank of the Philippines', '   ', '', '', '', '', '', '', 'active', 0),
(1186, 'Universal Telecommunications Services,Inc', '4/F JRDC Bldg. Osmeña Blvd. Cebu City 000-830-195-000V ', '2545956', '', '', '', '', '', 'active', 0),
(1187, 'Van Joyce Sarcos', '   ', '', '', '', '', '', '', 'active', 0),
(1188, 'Vance Cedric Sy', '   ', '', '', '', '', '', '', 'active', 0),
(1189, 'Vanessa Marie Bonjoc', '   ', '', '', '', '', '', '', 'active', 0),
(1190, 'Vantage Point Global Trading', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1191, 'Various Accounts', '   ', '', '', '', '', '', '', 'active', 0),
(1192, 'Venice Mae Gilig', '   ', '', '', '', '', '', '', 'active', 0),
(1193, 'Vennette', '   ', '', '', '', '', '', '', 'active', 0),
(1194, 'Vennette Claire Silvosa', '   ', '', '', '', '', '', '', 'active', 0),
(1195, 'Venus Liquit', '   ', '', '', '', '', '', '', 'active', 0),
(1196, 'Veolia Water Solutions & Technologies', '   ', '', '', '', '', '', '', 'active', 0),
(1197, 'Verah Marie S. Almodal', '   ', '', '', '', '', '', '', 'active', 0),
(1198, 'Veronica Rich', '   ', '', '', '', '', '', '', 'active', 0),
(1199, 'Veya Faye Yu', '   ', '', '', '', '', '', '', 'active', 0),
(1200, 'Vicente Cabrera III', '   ', '', '', '', '', '', '', 'active', 0),
(1201, 'Victor Ma. Liza Mercader', '   ', '', '', '', '', '', '', 'active', 0),
(1202, 'Viena Rose Jumarito', '   ', '', '', '', '', '', '', 'active', 0),
(1203, 'Villahermosa Bus Liner', '   ', '', '', '', '', '', '', 'active', 0),
(1204, 'Vincent Guerra', '   ', '', '', '', '', '', '', 'active', 0),
(1205, 'Viner Pingol', '   ', '', '', '', '', '', '', 'active', 0),
(1206, 'Virgilio Lumapas', '   ', '', '', '', '', '', '', 'active', 0),
(1207, 'Virgilio V. Calonia', '   ', '', '', '', '', '', '', 'active', 0),
(1208, 'Virginia Food, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1209, 'Virginia Monares', '   ', '', '', '', '', '', '', 'active', 0),
(1210, 'Virginia Roble', '   ', '', '', '', '', '', '', 'active', 0),
(1211, 'Visayan Educational Supply', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1212, 'VR & W COLLECIONE/ROMOFF', '3/F Krizia Building, Gorordo Avenue Lahug Cebu City  ', '', '', '', '', '', '', 'active', 0),
(1213, 'Wakaba Ito', '   ', '', '', '', '', '', '', 'active', 0),
(1214, 'Waterfront Cebu City Hotel & Casino', '   ', '', '', '', '', '', '', 'active', 0),
(1215, 'Welmer Lo-oc', '   ', '', '', '', '', '', '', 'active', 0),
(1216, 'WESTLINK Travel and Tours', '   ', '', '', '', '', '', '', 'active', 0),
(1217, 'Wilfredo Conejos, Jr.', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1218, 'Willy Beth Barillo', '   ', '', '', '', '', '', '', 'active', 0),
(1219, 'Wilmer S. Maquilan', '   ', '', '', '', '', '', '', 'active', 0),
(1220, 'Wilter Cabarrubias', '   ', '', '', '', '', '', '', 'active', 0),
(1221, 'Windell Magale', '   ', '', '', '', '', '', '', 'active', 0),
(1222, 'Winnie Tugahan', '   ', '', '', '', '', '', '', 'active', 0),
(1223, 'Woo Soo Youn', '   ', '', '', '', '', '', '', 'active', 0),
(1224, 'Wrenleys Motor Plaza', '   ', '', '', '', '', '', '', 'active', 0),
(1225, 'WS Pacific Publications, Inc.(Cebu)', 'G/F Lyden Bldg. F Ramos St. Cogon Cebu City  ', '', '', '', '004-526-852-001', '', '', 'active', 0),
(1226, 'Y101', '   ', '', '', '', '', '', '', 'active', 0),
(1227, 'Yale Hardware', '   ', '', '', '', '', '', '', 'active', 0),
(1228, 'YN Realty Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1229, 'Yoko Povadora', '   ', '', '', '', '', '', '', 'active', 0),
(1230, 'Yuko Kishi', '   ', '', '', '', '', '', '', 'active', 0),
(1231, 'Zefren Suan', '   ', '', '', '', '', '', '', 'active', 0),
(1232, 'testing 123 purposes only', 'here', '134134', '419240912374', '120481209', 'here', 'here', 'here', 'active', 1318559747),
(1233, 'sfsd', 'sdsd', '125', 'fsdfsfdsfdsfsdsfsdfsdf', '', '', '', '', 'active', 1333353569);

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE IF NOT EXISTS `supplies` (
  `supply_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`supply_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `supplies`
--

INSERT INTO `supplies` (`supply_id`, `name`, `supplier_id`, `quantity`) VALUES
(1, 'cottons', 1103, 10);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(65) NOT NULL,
  `task_link` varchar(120) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `task`
--


-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_id`, `emp_id`) VALUES
(1, 1, 10),
(2, 14, 11),
(3, 3, 11),
(4, 4, 10),
(5, 5, 10),
(6, 6, 10),
(7, 7, 10),
(9, 9, 12),
(10, 10, 10),
(11, 11, 12),
(12, 3, 14),
(14, 13, 12),
(15, 16, 12),
(16, 39, 10),
(17, 2, 10),
(18, 16, 10),
(19, 2, 16),
(20, 18, 12),
(21, 15, 17),
(22, 19, 17),
(23, 48, 17),
(24, 20, 17),
(31, 26, 20),
(33, 28, 20),
(37, 30, 21),
(38, 29, 21),
(39, 25, 21),
(40, 27, 21),
(45, 33, 16),
(46, 30, 16),
(47, 31, 16),
(48, 32, 20),
(50, 37, 13),
(51, 38, 13),
(52, 36, 16),
(53, 34, 13),
(54, 35, 13),
(55, 18, 10),
(56, 40, 23),
(57, 41, 23),
(58, 42, 23),
(59, 43, 23),
(60, 44, 24),
(61, 45, 11),
(62, 46, 25),
(63, 47, 25),
(64, 9, 15),
(65, 34, 12),
(66, 21, 17),
(67, 11, 15),
(68, 13, 15),
(69, 34, 15),
(70, 49, 15),
(71, 50, 11),
(72, 12, 15);

-- --------------------------------------------------------

--
-- Table structure for table `task_master_list`
--

CREATE TABLE IF NOT EXISTS `task_master_list` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(45) NOT NULL,
  `link` varchar(120) NOT NULL,
  `class_icon` varchar(30) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `task_master_list`
--

INSERT INTO `task_master_list` (`task_id`, `task_name`, `link`, `class_icon`) VALUES
(1, 'Class Schedules', '#/task/class-schedule', 'sprite-create'),
(2, 'Change Form', '#/task/change-form', 'sprite-create'),
(3, 'Lesson Plan', '#/task/lesson-plan-create', 'sprite-create'),
(4, 'Books', '#/task/books-list', 'sprite-list'),
(5, 'Courses', '#/task/courses-list', 'sprite-list'),
(6, 'Course Levels', '#/task/course-levels-list', 'sprite-list'),
(7, 'Classrooms', '#/task/classrooms-list', 'sprite-list'),
(8, 'Create Form', '#/task/hotel-create/guest', 'sprite-create'),
(9, 'Accommodations', '#/task/hotel-list/guest', 'sprite-create'),
(10, 'Teachers', '#/task/teachers-list', 'sprite-list'),
(11, 'Rooms', '#/task/hotel-rooms', 'sprite-list'),
(12, 'Manage Rooms', '#/task/manage-hotel-rooms', 'sprite-create'),
(13, 'Daily Sales Rep', '#/task/hotel-daily-sales-report', 'sprite-create'),
(14, 'Class Schedule', '#/task/teacher-class-schedule', 'sprite-list'),
(15, 'Forms', '#/task/hr-forms', 'sprite-create'),
(16, 'Lesson Plans', '#/task/lesson-plans-list', 'sprite-list'),
(17, 'Asset Master List', '#/task/asset-master-list', 'sprite-list'),
(18, 'Change Requests', '#/task/change-requests', 'sprite-list'),
(19, 'Employees', '#/task/employees-list', 'sprite-list'),
(20, 'Contributions', '#/task/contributions-list', 'sprite-list'),
(21, 'Payroll', '#/task/libro_paga', 'sprite-create'),
(22, 'Stocks', '#/task/ops-stock', 'sprite-create'),
(24, 'Inventory', '#/task/ops-inventory', 'sprite-list'),
(25, 'Materials', '#/task/materials-list', 'sprite-list'),
(26, 'F and B', '#/task/ops-menu', 'sprite-create'),
(27, 'Clinic supplies', '#/task/clinic-supplies', 'sprite-list'),
(28, 'Forms', '#/task/ops-forms', 'sprite-create'),
(29, 'Students', '#/task/student-list', 'sprite-create'),
(30, 'Class Schedules', '#/task/class-list', 'sprite-create'),
(31, 'Grades', '#/task/grades', 'sprite-create'),
(32, 'Reports', '#/task/ops-reports', 'sprite-create'),
(33, 'Profile', '#/task/profile', 'sprite-create'),
(34, 'Room Status', '#/task/ops-room', 'sprite-list'),
(35, 'Forms', '#/task/ops-checklist', 'sprite-create'),
(36, 'Fees', '#/task/fees', 'sprite-list'),
(37, 'Stocks', '#/task/ops-hkstocks', 'sprite-create'),
(38, 'Inventory', '#/task/ops-hkinventory', 'sprite-create'),
(39, 'Students', '#/task/student-sched', 'sprite-list'),
(40, 'Price List', '#/task/ops-pricelist', 'sprite-list'),
(41, 'Inventory', '#/task/ops-lpinventory', 'sprite-create'),
(42, 'Incoming Stocks', '#/task/ops-lpinstocks', 'sprite-list'),
(43, 'Daily Sales', '#/task/ops-lpdreports', 'sprite-create'),
(44, 'Daily Sales', '#/task/ops-lpondutysales', 'sprite-create'),
(45, 'Payslip', '#/task/payslip', 'sprite-view1'),
(46, 'Utilities', '#/task/ops-mainutil', 'sprite-list'),
(47, 'Supplies', '#/task/ops-mainsupplies', 'sprite-create'),
(48, 'Department', '#/task/department-list', 'sprite-list'),
(49, 'Lp Day Sales', '#/task/fo-dsales-or', 'sprite-list'),
(50, 'Loans', '#/task/loans', 'sprite-list');

-- --------------------------------------------------------

--
-- Table structure for table `tax_base_over`
--

CREATE TABLE IF NOT EXISTS `tax_base_over` (
  `base_id` int(11) NOT NULL AUTO_INCREMENT,
  `over` decimal(8,2) NOT NULL,
  `base` decimal(8,2) NOT NULL,
  PRIMARY KEY (`base_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tax_base_over`
--

INSERT INTO `tax_base_over` (`base_id`, `over`, `base`) VALUES
(1, 0.00, 0.00),
(2, 0.05, 0.00),
(3, 0.10, 20.83),
(4, 0.15, 104.17),
(5, 0.20, 354.17),
(6, 0.25, 937.50),
(7, 0.30, 2083.33),
(8, 0.32, 5208.33);

-- --------------------------------------------------------

--
-- Table structure for table `tax_table`
--

CREATE TABLE IF NOT EXISTS `tax_table` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `exemption` int(11) NOT NULL,
  `base_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `tax_table`
--

INSERT INTO `tax_table` (`tax_id`, `status`, `exemption`, `base_id`) VALUES
(1, 'Zero', 1, 1),
(2, 'SME', 1, 1),
(3, 'SME', 2083, 2),
(4, 'SME', 2500, 3),
(5, 'SME', 3333, 4),
(6, 'SME', 5000, 5),
(7, 'SME', 7917, 6),
(8, 'SME', 12500, 7),
(9, 'SME', 22917, 8),
(10, 'SME1', 1, 1),
(11, 'SME1', 3125, 2),
(12, 'SME1', 3542, 3),
(13, 'SME1', 4375, 4),
(14, 'SME1', 6042, 5),
(15, 'SME1', 8958, 6),
(16, 'SME1', 13542, 7),
(17, 'SME1', 23958, 8),
(18, 'SME2', 1, 1),
(19, 'SME2', 4167, 2),
(20, 'SME2', 4583, 3),
(21, 'SME2', 5417, 4),
(22, 'SME2', 7083, 5),
(23, 'SME2', 10000, 6),
(24, 'SME2', 14583, 7),
(25, 'SME2', 25000, 8),
(26, 'SME3', 1, 1),
(27, 'SME3', 5208, 2),
(28, 'SME3', 5625, 3),
(29, 'SME3', 6458, 4),
(30, 'SME3', 8125, 5),
(31, 'SME3', 11042, 6),
(32, 'SME3', 15625, 7),
(33, 'SME3', 26042, 8),
(34, 'SME4', 1, 1),
(35, 'SME4', 6250, 2),
(36, 'SME4', 6667, 3),
(37, 'SME4', 7500, 4),
(38, 'SME4', 9167, 5),
(39, 'SME4', 12083, 6),
(40, 'SME4', 16667, 7),
(41, 'SME4', 27083, 8),
(42, 'Zero', 0, 2),
(43, 'Zero', 417, 3),
(44, 'Zero', 1250, 4),
(45, 'Zero', 2917, 5),
(46, 'Zero', 5833, 6),
(47, 'Zero', 10417, 7),
(48, 'Zero', 20833, 8);

-- --------------------------------------------------------

--
-- Table structure for table `unitsof_measure`
--

CREATE TABLE IF NOT EXISTS `unitsof_measure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `unitsof_measure`
--

INSERT INTO `unitsof_measure` (`id`, `name`) VALUES
(1, 'each'),
(2, 'kilo'),
(3, 'length'),
(4, 'liters'),
(5, 'meters'),
(6, 'bottle'),
(7, 'pack'),
(8, 'pcs'),
(11, 'others');

-- --------------------------------------------------------

--
-- Table structure for table `with_stock_tracker`
--

CREATE TABLE IF NOT EXISTS `with_stock_tracker` (
  `ws_tracker_id` int(11) NOT NULL AUTO_INCREMENT,
  `ws_tracker_type` int(11) NOT NULL,
  `ws_tracker_stock_id` int(11) NOT NULL,
  `ws_tracker_item_ctr` int(11) NOT NULL,
  `ws_binder_item_bal` int(11) NOT NULL,
  `ws_tracker_date` date NOT NULL,
  `ws_recipient` int(11) NOT NULL,
  PRIMARY KEY (`ws_tracker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `with_stock_tracker`
--

INSERT INTO `with_stock_tracker` (`ws_tracker_id`, `ws_tracker_type`, `ws_tracker_stock_id`, `ws_tracker_item_ctr`, `ws_binder_item_bal`, `ws_tracker_date`, `ws_recipient`) VALUES
(1, 1, 2, 20, 20, '2012-05-08', 12),
(3, 1, 10, 23, 23, '2012-05-08', 3),
(4, 1, 25, 57, 57, '2012-05-05', 3),
(5, 1, 26, 40, 40, '2012-05-07', 4),
(7, 1, 27, 70, 70, '2012-05-08', 3),
(135, 1, 2, 6, 26, '2012-05-11', 12),
(136, 1, 2, 5, 25, '2012-05-11', 12),
(137, 1, 27, 8, 78, '2012-05-11', 3),
(138, 1, 2, 9, 29, '2012-05-11', 12),
(139, 1, 2, 9, 29, '2012-05-15', 12),
(140, 1, 2, 45, 65, '2012-05-22', 12),
(141, 1, 2, 6, 26, '2012-05-22', 12),
(142, 1, 2, 6, 26, '2012-05-22', 12),
(143, 1, 2, 6, 26, '2012-05-22', 12),
(144, 1, 10, 6, 29, '2012-05-22', 3),
(145, 1, 10, 9, 32, '2012-05-22', 3),
(146, 1, 10, 0, 23, '2012-05-22', 3),
(147, 1, 26, 6, 46, '2012-05-22', 4);

-- --------------------------------------------------------

--
-- Table structure for table `with_stock_tracker_type`
--

CREATE TABLE IF NOT EXISTS `with_stock_tracker_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `with_stock_tracker_type`
--

INSERT INTO `with_stock_tracker_type` (`id`, `name`) VALUES
(1, 'IN'),
(2, 'OUT');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acad_classes`
--
ALTER TABLE `acad_classes`
  ADD CONSTRAINT `acad_classes_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `acad_classes_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  ADD CONSTRAINT `acad_classes_ibfk_3` FOREIGN KEY (`level_id`) REFERENCES `course_level` (`id`),
  ADD CONSTRAINT `acad_classes_ibfk_4` FOREIGN KEY (`classroom_id`) REFERENCES `classrooms` (`classroom_id`),
  ADD CONSTRAINT `acad_classes_ibfk_5` FOREIGN KEY (`teacher_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `acad_lesson_plans`
--
ALTER TABLE `acad_lesson_plans`
  ADD CONSTRAINT `acad_lesson_plans_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `acad_classes` (`class_id`),
  ADD CONSTRAINT `acad_lesson_plans_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `acad_lesson_plan_details`
--
ALTER TABLE `acad_lesson_plan_details`
  ADD CONSTRAINT `acad_lesson_plan_details_ibfk_1` FOREIGN KEY (`lesson_plan_id`) REFERENCES `acad_lesson_plans` (`lesson_plan_id`);

--
-- Constraints for table `accommodations`
--
ALTER TABLE `accommodations`
  ADD CONSTRAINT `accommodations_ibfk_1` FOREIGN KEY (`room_number`) REFERENCES `rooms_1` (`number`);

--
-- Constraints for table `acquisitions`
--
ALTER TABLE `acquisitions`
  ADD CONSTRAINT `acquisitions_ibfk_1` FOREIGN KEY (`requested_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `acquisitions_ibfk_2` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`stock_id`),
  ADD CONSTRAINT `acquisitions_ibfk_3` FOREIGN KEY (`type`) REFERENCES `stock_type` (`id`);

--
-- Constraints for table `admissions`
--
ALTER TABLE `admissions`
  ADD CONSTRAINT `admissions_ibfk_1` FOREIGN KEY (`stud_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `admissions_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  ADD CONSTRAINT `admissions_ibfk_3` FOREIGN KEY (`course_level`) REFERENCES `course_level` (`id`);

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_ibfk_1` FOREIGN KEY (`units`) REFERENCES `unitsof_measure` (`id`);

--
-- Constraints for table `asset_tracker`
--
ALTER TABLE `asset_tracker`
  ADD CONSTRAINT `asset_tracker_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`asset_id`),
  ADD CONSTRAINT `asset_tracker_ibfk_4` FOREIGN KEY (`from_emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `as_account_type`
--
ALTER TABLE `as_account_type`
  ADD CONSTRAINT `as_account_type_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `as_account_categories` (`id`);

--
-- Constraints for table `as_chartdetails`
--
ALTER TABLE `as_chartdetails`
  ADD CONSTRAINT `as_chartdetails_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`);

--
-- Constraints for table `as_chartofaccounts`
--
ALTER TABLE `as_chartofaccounts`
  ADD CONSTRAINT `as_chartofaccounts_ibfk_1` FOREIGN KEY (`account_type_id`) REFERENCES `as_account_type` (`id`);

--
-- Constraints for table `as_general_ledger`
--
ALTER TABLE `as_general_ledger`
  ADD CONSTRAINT `as_general_ledger_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`),
  ADD CONSTRAINT `as_general_ledger_ibfk_2` FOREIGN KEY (`payment_terms`) REFERENCES `as_payterms` (`id`);

--
-- Constraints for table `as_invitems`
--
ALTER TABLE `as_invitems`
  ADD CONSTRAINT `as_invitems_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`);

--
-- Constraints for table `as_purchase_orders`
--
ALTER TABLE `as_purchase_orders`
  ADD CONSTRAINT `as_purchase_orders_ibfk_1` FOREIGN KEY (`payment_method`) REFERENCES `as_paymeth` (`id`),
  ADD CONSTRAINT `as_purchase_orders_ibfk_3` FOREIGN KEY (`class`) REFERENCES `as_class` (`id`),
  ADD CONSTRAINT `as_purchase_orders_ibfk_5` FOREIGN KEY (`supplier`) REFERENCES `suppliers` (`supplier_id`);

--
-- Constraints for table `as_sales_postings`
--
ALTER TABLE `as_sales_postings`
  ADD CONSTRAINT `as_sales_postings_ibfk_2` FOREIGN KEY (`paymeth`) REFERENCES `as_paymeth` (`id`),
  ADD CONSTRAINT `as_sales_postings_ibfk_3` FOREIGN KEY (`class`) REFERENCES `as_class` (`id`);

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`);

--
-- Constraints for table `class_students`
--
ALTER TABLE `class_students`
  ADD CONSTRAINT `class_students_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `acad_classes` (`class_id`),
  ADD CONSTRAINT `class_students_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `daily_sales_report`
--
ALTER TABLE `daily_sales_report`
  ADD CONSTRAINT `daily_sales_report_ibfk_1` FOREIGN KEY (`filed_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `department_ibfk_2` FOREIGN KEY (`deptd_id`) REFERENCES `department_details` (`deptd_id`);

--
-- Constraints for table `department_details`
--
ALTER TABLE `department_details`
  ADD CONSTRAINT `department_details_ibfk_1` FOREIGN KEY (`dept_head`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `department_details_ibfk_2` FOREIGN KEY (`dept_head`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `dsr_collections`
--
ALTER TABLE `dsr_collections`
  ADD CONSTRAINT `dsr_collections_ibfk_1` FOREIGN KEY (`dsr_report_id`) REFERENCES `dsr_report` (`dsr_report_id`);

--
-- Constraints for table `dsr_recap`
--
ALTER TABLE `dsr_recap`
  ADD CONSTRAINT `dsr_recap_ibfk_1` FOREIGN KEY (`dsr_report_id`) REFERENCES `dsr_report` (`dsr_report_id`);

--
-- Constraints for table `dsr_report`
--
ALTER TABLE `dsr_report`
  ADD CONSTRAINT `dsr_report_ibfk_1` FOREIGN KEY (`prepared_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_contact_details`
--
ALTER TABLE `emp_contact_details`
  ADD CONSTRAINT `emp_contact_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_contract`
--
ALTER TABLE `emp_contract`
  ADD CONSTRAINT `emp_contract_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_contrib_details`
--
ALTER TABLE `emp_contrib_details`
  ADD CONSTRAINT `emp_contrib_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_dependents`
--
ALTER TABLE `emp_dependents`
  ADD CONSTRAINT `emp_dependents_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_education`
--
ALTER TABLE `emp_education`
  ADD CONSTRAINT `emp_education_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_emergency_contact`
--
ALTER TABLE `emp_emergency_contact`
  ADD CONSTRAINT `emp_emergency_contact_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_immigration`
--
ALTER TABLE `emp_immigration`
  ADD CONSTRAINT `emp_immigration_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_job_details`
--
ALTER TABLE `emp_job_details`
  ADD CONSTRAINT `emp_job_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `emp_job_details_ibfk_3` FOREIGN KEY (`job_title`) REFERENCES `job_title` (`id`),
  ADD CONSTRAINT `emp_job_details_ibfk_4` FOREIGN KEY (`curr_empstatus`) REFERENCES `employment_type` (`id`);

--
-- Constraints for table `emp_language`
--
ALTER TABLE `emp_language`
  ADD CONSTRAINT `emp_language_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_leaves`
--
ALTER TABLE `emp_leaves`
  ADD CONSTRAINT `emp_leaves_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_license`
--
ALTER TABLE `emp_license`
  ADD CONSTRAINT `emp_license_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_ot_ut_detail`
--
ALTER TABLE `emp_ot_ut_detail`
  ADD CONSTRAINT `emp_ot_ut_detail_ibfk_1` FOREIGN KEY (`emp_ot_ut_id`) REFERENCES `emp_overtime_undertime` (`emp_ot_ut_id`);

--
-- Constraints for table `emp_overtime_undertime`
--
ALTER TABLE `emp_overtime_undertime`
  ADD CONSTRAINT `emp_overtime_undertime_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_personal_details`
--
ALTER TABLE `emp_personal_details`
  ADD CONSTRAINT `emp_personal_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_skills`
--
ALTER TABLE `emp_skills`
  ADD CONSTRAINT `emp_skills_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_work_experience`
--
ALTER TABLE `emp_work_experience`
  ADD CONSTRAINT `emp_work_experience_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `fb_menu`
--
ALTER TABLE `fb_menu`
  ADD CONSTRAINT `fb_menu_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `fb_menu_ibfk_2` FOREIGN KEY (`menu_name_id`) REFERENCES `menu_name` (`menu_name_id`),
  ADD CONSTRAINT `fb_menu_ibfk_3` FOREIGN KEY (`meal_type_id`) REFERENCES `menu_meal_type` (`meal_type_id`),
  ADD CONSTRAINT `fb_menu_ibfk_4` FOREIGN KEY (`ingredient_id`) REFERENCES `menu_status_id` (`ingredient_id`);

--
-- Constraints for table `guests_1`
--
ALTER TABLE `guests_1`
  ADD CONSTRAINT `guests_1_ibfk_1` FOREIGN KEY (`fo_staff`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `guest_accommodation_1`
--
ALTER TABLE `guest_accommodation_1`
  ADD CONSTRAINT `guest_accommodation_1_ibfk_1` FOREIGN KEY (`room_number`) REFERENCES `rooms_1` (`number`);

--
-- Constraints for table `guest_accomp`
--
ALTER TABLE `guest_accomp`
  ADD CONSTRAINT `guest_accomp_ibfk_1` FOREIGN KEY (`guest_accom_id`) REFERENCES `guest_accom` (`guest_accom_id`);

--
-- Constraints for table `guest_room_tags_1`
--
ALTER TABLE `guest_room_tags_1`
  ADD CONSTRAINT `guest_room_tags_1_ibfk_2` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodations` (`id`),
  ADD CONSTRAINT `guest_room_tags_1_ibfk_3` FOREIGN KEY (`guest_id`) REFERENCES `guests_1` (`id`);

--
-- Constraints for table `guest_room_tag_1`
--
ALTER TABLE `guest_room_tag_1`
  ADD CONSTRAINT `guest_room_tag_1_ibfk_1` FOREIGN KEY (`guest_id`) REFERENCES `guests_1` (`id`),
  ADD CONSTRAINT `guest_room_tag_1_ibfk_2` FOREIGN KEY (`guest_accommodation_id`) REFERENCES `guest_accommodation_1` (`id`);

--
-- Constraints for table `job_functions`
--
ALTER TABLE `job_functions`
  ADD CONSTRAINT `job_functions_ibfk_1` FOREIGN KEY (`jobtitle_id`) REFERENCES `job_title` (`id`),
  ADD CONSTRAINT `job_functions_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`);

--
-- Constraints for table `menu_name`
--
ALTER TABLE `menu_name`
  ADD CONSTRAINT `menu_name_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `fb_menu` (`added_by`),
  ADD CONSTRAINT `menu_name_ibfk_2` FOREIGN KEY (`menu_name_id`) REFERENCES `fb_menu` (`menu_name_id`);

--
-- Constraints for table `payroll_details`
--
ALTER TABLE `payroll_details`
  ADD CONSTRAINT `payroll_details_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `payroll_details_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_adjustments`
--
ALTER TABLE `pay_adjustments`
  ADD CONSTRAINT `pay_adjustments_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_adjustments_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_allowances`
--
ALTER TABLE `pay_allowances`
  ADD CONSTRAINT `pay_allowances_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_allowances_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_canteen`
--
ALTER TABLE `pay_canteen`
  ADD CONSTRAINT `pay_canteen_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_canteen_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_holiday`
--
ALTER TABLE `pay_holiday`
  ADD CONSTRAINT `pay_holiday_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_holiday_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_night_diff`
--
ALTER TABLE `pay_night_diff`
  ADD CONSTRAINT `pay_night_diff_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_night_diff_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_uniform`
--
ALTER TABLE `pay_uniform`
  ADD CONSTRAINT `pay_uniform_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_uniform_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `prs`
--
ALTER TABLE `prs`
  ADD CONSTRAINT `prs_ibfk_1` FOREIGN KEY (`requested_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `prs_details`
--
ALTER TABLE `prs_details`
  ADD CONSTRAINT `prs_details_ibfk_1` FOREIGN KEY (`prs_id`) REFERENCES `prs` (`prs_id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`room_type_id`),
  ADD CONSTRAINT `rooms_ibfk_2` FOREIGN KEY (`room_number_id`) REFERENCES `room_number_ids` (`room_number_id`),
  ADD CONSTRAINT `rooms_ibfk_3` FOREIGN KEY (`room_status_id`) REFERENCES `room_status` (`room_status_id`),
  ADD CONSTRAINT `rooms_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `rooms_1`
--
ALTER TABLE `rooms_1`
  ADD CONSTRAINT `rooms_1_ibfk_1` FOREIGN KEY (`status`) REFERENCES `room_status_1` (`id`),
  ADD CONSTRAINT `rooms_1_ibfk_2` FOREIGN KEY (`type`) REFERENCES `room_type_1` (`id`);

--
-- Constraints for table `rooms_accom`
--
ALTER TABLE `rooms_accom`
  ADD CONSTRAINT `rooms_accom_ibfk_1` FOREIGN KEY (`room_number_id`) REFERENCES `room_number_ids` (`room_number_id`),
  ADD CONSTRAINT `rooms_accom_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `room_number_ids`
--
ALTER TABLE `room_number_ids`
  ADD CONSTRAINT `room_number_ids_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `room_rates`
--
ALTER TABLE `room_rates`
  ADD CONSTRAINT `room_rates_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `room_type`
--
ALTER TABLE `room_type`
  ADD CONSTRAINT `room_type_ibfk_1` FOREIGN KEY (`room_rate_id`) REFERENCES `room_rates` (`room_rate_id`);

--
-- Constraints for table `srs`
--
ALTER TABLE `srs`
  ADD CONSTRAINT `srs_ibfk_1` FOREIGN KEY (`requested_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `srs_ibfk_2` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`stock_id`);

--
-- Constraints for table `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_ibfk_2` FOREIGN KEY (`units`) REFERENCES `unitsof_measure` (`id`),
  ADD CONSTRAINT `stocks_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `stocks_ibfk_5` FOREIGN KEY (`stock_type`) REFERENCES `stock_type` (`id`);

--
-- Constraints for table `stock_items`
--
ALTER TABLE `stock_items`
  ADD CONSTRAINT `stock_items_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`),
  ADD CONSTRAINT `stock_items_ibfk_2` FOREIGN KEY (`unitsof_measure`) REFERENCES `unitsof_measure` (`id`);

--
-- Constraints for table `students_accommodation_1`
--
ALTER TABLE `students_accommodation_1`
  ADD CONSTRAINT `students_accommodation_1_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `students_accommodation_1_ibfk_2` FOREIGN KEY (`room_no`) REFERENCES `rooms_1` (`number`);

--
-- Constraints for table `student_accomp`
--
ALTER TABLE `student_accomp`
  ADD CONSTRAINT `student_accomp_ibfk_1` FOREIGN KEY (`student_accom_id`) REFERENCES `student_accom` (`student_accom_id`);

--
-- Constraints for table `student_room_tags_1`
--
ALTER TABLE `student_room_tags_1`
  ADD CONSTRAINT `student_room_tags_1_ibfk_1` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodations` (`id`),
  ADD CONSTRAINT `student_room_tags_1_ibfk_2` FOREIGN KEY (`stud_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `student_room_tag_1`
--
ALTER TABLE `student_room_tag_1`
  ADD CONSTRAINT `student_room_tag_1_ibfk_1` FOREIGN KEY (`students_accom_id`) REFERENCES `students_accom_1` (`students_accom_id`),
  ADD CONSTRAINT `student_room_tag_1_ibfk_2` FOREIGN KEY (`students_accommodation_id`) REFERENCES `students_accommodation_1` (`students_accommodation_id`);

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task_master_list` (`task_id`);
