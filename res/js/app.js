// Filename: app.js
define([
  'jquery',
  'underscore',
  'backbone',
  'router', // Request router.js
  'baseURL',
  'jQ-plugins/jquery.is_active'
], function($, _, Backbone, Router, baseURL, is_active){
	
  var active = Backbone.Model.extend({});
  var initialize = function(){
    // Pass in our Router module and call it's initialize function
    Router.initialize();
    
    // check if the session is true or set
    var is_active = function() {
  	  //console.log("test");
      active.prototype.url = baseURL+"user/active/check?s="+Math.floor(Math.random() * 1000000000000);
      var activ = new active;
      
      activ.fetch({
    	  success: function() {
    		  if( !activ.attributes.active ) {
    			  clearInterval(clear); //clear interval
        		  $.is_active({
        			  title: "Session Expired",
        			  content: "Your session has already expired. Please login to continue."
        		  }, function(data) {
        			  if(data) window.location.href = baseURL;
        		  });
    		  }
    	  }
      });
    };
    var clear = setInterval(is_active, 4000);
  };
  return {
    initialize: initialize
  };
});