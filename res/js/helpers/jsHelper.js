define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	// added by: celi-andro
	var page_pagination = Backbone.Model.extend({});
	
	var jsHelper = {
		trim_text: function( text ) {
			return text.replace(/^\s*|\s*$/g, "");
		},
		word_displayed_limiter: function( limit, text ) {
			//console.log(this);
			text = jsHelper.trim_text(text); //remove extra space
			var words = text.split(" "), num_words = words.length, w = "";
			
			if( num_words > limit ) {
				for( var i = 0; i < limit; i++ ) w += ( i === (limit-1) ) ? words[i] : words[i]+" ";
				w += " ...."; //add dots
			} else w = text;
			
			return w;
		},
		page_pagination: function(element, template, callback) {
			$(".page-pagination a").live("click", function(e) {
				e.preventDefault();
				var url = $(this).attr("href");
				page_pagination.prototype.url = url;
				var pagePagination = new page_pagination;
				pagePagination.fetch({
					success: function() {
						var searched_data = _.template(template, $.extend({}, pagePagination.attributes, {_:_}));
						$(element).html(searched_data);
						// if there is any callback function
						if( typeof callback === 'function' ) {
							callback.call(this, pagePagination.attributes);
						}
					}
				});
			});
		},
		editing_items: function( $elements, $select_box ) {
			$($elements).live("click", function(evt) {
				evt.preventDefault(); evt.stopPropagation();
				
				var $item = $(this);
				var $width = $item.width();
				var $textbox = $("<input>").attr("type", "text").css({
					"border": "none", "height": "18px", 
					"width": $width-2, "text-align": "center", 
					"border-bottom": "1px solid #CCC",
					"background": "#FFF"
				});
				var $text = $item.html();
				
				if( $item.hasClass("item-select") ) {
					var $select = $("<select>").attr($select_box.attr).css({width: "120px"});
					//var $options = $("<option>");
					$.each($select_box.options, function(index, element){
						var $options = $("<option>").attr("value", element).text(element.charAt(0).toUpperCase() + element.slice(1).toLowerCase());
						$options.appendTo($select);
					});
					$item.html("");
					$select.appendTo($item);
					
					$select.click(function(e){e.stopPropagation();})
					$select.focus();
					
					$select.blur(function(e){
						e.stopPropagation();
						$item.html($select.val());
					});
					
				} else {
					$item.html("");
					// append textbox to the div element
					$textbox.appendTo($item);
					$textbox.val($text);
					$textbox.click(function(e){e.stopPropagation();});
					$textbox.focus();
				}
				
				if( $item.hasClass("item-qty") || $item.hasClass("item-price") ) {
		            // prevent users from typing letters
		            $item.keypress(function(e){
		                if( e.which!=8 && e.which!=0 && (e.which<46 || e.which>57 || e.which == 47 ) )
		                    e.preventDefault();
		            });
		            // calculate the amount
		            $textbox.change(function(){
		            	var amount = 0;
		            	if( $item.hasClass("item-qty") ) {
		            		amount = parseFloat( $(this).val() ) * parseFloat( $item.next().html() );
		            	} else if( $item.hasClass("item-price") ) {
		            		amount = parseFloat( $(this).val() ) * parseFloat( $item.prev().html() );
		            	}
		            	$item.parent().find(".item-amt").html(amount.toFixed(2));
		            });
				}
				
				$textbox.blur(function(e){
					e.stopPropagation();
					$text = ( $textbox.val() === "" || $textbox.val === null ) ? $text : $textbox.val();
					$item.html($text);
				});
			});
		},
		digit_only: function( $element ) {
			//$element.keypress(function(e){
             //   if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57 ) )
              //      e.preventDefault();
			//});
			// you can also use delegate function if you like
			$element.live('keypress', function(e){
				 if( e.which!=8 && e.which!=0 && (e.which<46 || e.which>57 || e.which == 47 ) )
					e.preventDefault();
			});
		},
		hovered_items: function( $element ) {
			// when hovering over
			$($element).live("mouseover mouseout", function(e) {
				var $checkbox = $(this).find("input[type='checkbox']");
				if(e.type === 'mouseover' && !$(this).hasClass("item-disabled") || $checkbox.attr("checked") ) 
					$(this).css({"background": "#f2f2f2"});
				else if(e.type === 'mouseout' && !$(this).hasClass("item-disabled") || $checkbox.attr("checked") )
					$(this).css({"background": "#fff"});
			});
		},
		inArray: function( needle, haystack ) {
			var length = haystack.length;
			for( var x = 0; x < length; x++ ) {
				if( needle == haystack[x] ) 
					return true;
			}
			return false;
		},
		dateRange: function( $elementId1, $elementId2) {
			var dates = $( "#"+$elementId1+", #"+$elementId2 ).datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) {
					var option = this.id == $elementId1 ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		},
		validate: function( $fields, $element ) {
			var errorReturn = false;
			var $data = {};
			$($fields, $element).each(function(index, element) {
				var e = $(element);
				var ePos = e.position();
				var errorLog = "";
				var error = false;
				var $div = $("<div>");
				
				/*	--------------
				 *	|Validation
				 *	--------------
				 */
				if(e.hasClass("validation-required") && e.val() === "") {
					errorLog += "Required";
					errorReturn = error = true;
				}
				
				else if( e.hasClass("validation-numbers-only") && (e.val() !== "") && !(/^[\d]+$/.test(e.val())) ) {
					errorLog += "Numbers Only";
					errorReturn = error = true;
				}
				
				else if(e.hasClass("validation-valid-email") && e.val() !== "" && !(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(e.val())) ) {
					errorLog += ( errorLog !== "" ) ? " ,invalid email" : "Invalid email";
					errorReturn = error = true;
				}
				/*	--------------
				 *	|End Validation
				 *	--------------
				 */
				
				// add error message
				if( error && !e.next().hasClass("validation-warning")) {
					$div.addClass("validation-warning").append(errorLog).css({
						"top": ePos.top-2, 
						"left": ePos.left + (e.width() - 5)
					}).insertAfter(e);
				}
				// remove error messages that needs to be removed
				//if( e.hasClass("validation-required") && e.val() !== "" && e.next().hasClass("validation-warning") ) {
					//e.next().fadeOut(500, function() {
					//	$(this).remove();
					//});
				//}
				// assign values and return it if this passes the validation
				$data[e.attr("name")] = e.val();
				
				// remove error messages when the change event was fired
				e.change(function(){
					if( e.hasClass("validation-required") && e.val() !== "" && e.next().hasClass("validation-warning") ) {
						e.next().fadeOut(500, function() {
							$(this).remove();
						});
					}
				});
			});
			
			$(".validation-warning").hover(function(){
				$(this).fadeOut(500, function() {
					$(this).remove();
				});
			});
			// return true if there are errors or required fields that needs to be filled up
			return errorReturn ? errorReturn : $data;
		}
	};
	return jsHelper;
});