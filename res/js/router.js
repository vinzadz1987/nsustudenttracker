// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jQ-plugins/hover',
  'jQ-plugins/jquery.facebox',
  'jQ-plugins/jquery.litebox', 
  'jQ-plugins/jquery.formvalidate',
  'jQ-plugins/jquery.custom_prompts',
  'jQ-plugins/jquery.decimalFormat',
  'jqueryUi'
], function($, _, Backbone){
  var AppRouter = Backbone.Router.extend({
    routes: {
    	'task/:type': 'taskType',
    	'task/:type/:extra' : 'taskTypeExtra',
    	'task/:type/:extra/*actions': 'taskTypeExtraActions',
    	'profile': 'profile',
    	'profile/:type': 'profileType',
    	'forms': 'form',
    	'forms/:type': 'formType',
    	'forms/:type/:extra': 'formTypeExtra',
    	'assets': 'asset',
    	'assets/:type': 'assetType',
    	'assets/:type/:extra': 'assetTypeExtra',
    	'notifications': 'notifications',
    	'*actions': 'defaultAction'
    },
    /* default actions */
    defaultAction: function(actions){
    	require(['views/task/task'], function(taskView) {
    		taskView.render();
    	});
    },
    /* Task Type */
    taskType: function(type) {
    	var dependency = "";
    	switch(type) {
    		case "change-form":
    			dependency = 'views/task/acad/changeForm';
    			break;
    		case "lesson-plan-create":
    			dependency = 'views/task/acad/lessonPlanCreate';
    			break;
            case "class-schedule":
                dependency = 'views/task/acad/classSchedules';
			break;
            case "books-list":
                dependency = 'views/task/acad/booksList';
            break;
            case "courses-list":
                dependency = 'views/task/acad/courseList';
            break;
            case "course-levels-list":
                dependency = 'views/task/acad/courseLevelsList';
            break;
            case "classrooms-list":
            	dependency = 'views/task/acad/classroomsList';
            break;
            case "teachers-list":
            	dependency = 'views/task/acad/teachersList';
            break;
            case "teacher-class-schedule":
                dependency = 'views/task/acad/teacherClassSchedule';
            break;
            case "lesson-plans-list":
                dependency = 'views/task/acad/lessonPlanList';
            break;
            case "change-requests":
                dependency = 'views/task/acad/changeRequests';
            break;
            //HOTEL
			case "hotel-create":
				dependency = "views/task/hotel/create-guest";
			break;
			case "hotel-list":
				dependency = "views/task/hotel/guest-list";
			break;
			case "hotel-rooms":
				dependency = "views/task/hotel/rooms";
			break;
			case "manage-hotel-rooms":
				dependency = "views/task/hotel/manage_rooms";
			break;
			case "hotel-daily-sales-report":
				dependency = "views/task/hotel/daily_sales_report";
			break;
                        case "fo-dsales-or":
                                dependency = "views/task/hotel/dsales_or";
                        break;
			//END HOTEL
                        
             
			
			//ASSET LIST
			case "asset-master-list":
				dependency = "views/task/assets/master-list";
			break;
			//END ASSET LIST
			//HR
			case "hr-forms":
				dependency = "views/task/hr/forms";
			break;
			case "employees-list":
				dependency = "views/task/hr/employees_list";
			break;
                        case "contributions-list":
				dependency = "views/task/hr/contributions_list";
			break;
                        
                     //OPS
                     
                        case "ops-stock":
                             dependency = "views/task/ops/stocks";
                        break;
                        case "ops-inventory":
                             dependency = "views/task/ops/inventory";
                        break;
                        case "ops-menu":
                                dependency="views/task/ops/fb";
                        break;  
                        case "ops-forms":
                             dependency="views/task/hr/forms";
                        break; 
                        case "ops-daily-sales-report":
                             dependency = "views/task/ops/daily_sales_report";
                        break;
                        case "ops-reports":
                                dependency="views/task/ops/reports";
                        break;
                        
                        //ops-houesekeeping
                        
                        case "ops-hkstocks":
                                dependency="views/task/ops/hk_stocks";
                        break;
                        case "ops-hkinventory":
                                dependency="views/task/ops/hk_inventory";
                        break;
                        case "ops-room":
                                dependency="views/task/ops/room_status";
                        break;
                        case "ops-checklist":
                                dependency="views/task/ops/checklist_form";
                        break;
                        
                        
                        //end - housekeeping
                        
                        // ops-larrys place
                         
                        case "ops-pricelist":
                                dependency="views/task/ops/lp_price_list";
                        break;
                        
                        case "ops-lpinventory":
                                dependency="views/task/ops/lp_inventory";
                        break;
                        
                        case "ops-lpinstocks":
                                dependency="views/task/ops/lp_instocks";
                        break;
                        
                        case "ops-lpdreports":
                                dependency="views/task/ops/lp_daily_sales";
                        break;
                        
                        case "ops-lpondutysales":
                                dependency="views/task/ops/lp_onduty_dsales";
                        break;
                        
                        // End of Larry place
                        
                        //ops - maintenance
                        
                        case "ops-mainutil":
                                dependency="views/task/ops/util";
                        break;
                        
                        case "ops-mainsupplies":
                                dependency="views/task/ops/main_supplies";
                        break;
                        
                        //end of maintenance
						
						//real state
						
						/*case "forSale":
								dependency = "views/task/rs/forSale";
						break;
						*/
						case "allreal":
								dependency = "views/task/rs/allreal";
						break;
						
						case "forSale":
								dependency = "views/task/rs/forSale";
						break;
						
						case "forRent":
								dependency = "views/task/rs/forRent";
						break;
						
						case "lookingFor":
								dependency = "views/task/rs/lookingFor";
						break;
						
						case "joinUs":
								dependency = "views/task/rs/joinUs";
						break;
						
						case "advertise":
								dependency = "views/task/rs/advertise";
						break;
						
						case "myadvert":
								dependency = "views/task/rs/myadvert";
						break;

                                               case "messages":
								dependency = "views/task/rs/messages";
						break;
                                            
                                               case "location":
								dependency = "views/task/rs/location";
						break;

						
						//end of real state
                        
                    //endofOPS
			
			default:
            	dependency = 'views/task/acad/classSchedules';
			break;
    	}
    	require([dependency], function(View) {
    		View.render();
    	});
    },
    taskTypeExtra: function( type, extra ) {
    	var dependency = "";
    	switch( type ) {
    		case "hotel-list":
    			//dependency = "views/task/hotel/guest-list";
    			dependency = (extra === 'guest') ? 
    				"views/task/hotel/guest-list":
    					"views/task/hotel/student-list";
    		break;
    		case "hotel-rooms":
    			dependency = "views/task/hotel/rooms";
    		break;
    	}
    	require([dependency], function(View) {
    		View.render();
    	});
    },
    taskTypeExtraActions: function(type, extra, actions) {
    	var dependency = "", item1 = "", item2 = "", item3 = ""; item4 = "";
    	switch(type) {
             /*case "hotel-create":
    			if( extra === "guest" ) {
    				dependency = "views/task/hotel/create-guest";
    				//console.log('oh yeah baby');
    			} else if( extra === "student" ) {
    				dependency = "views/task/hotel/create-student";
    			}
    		break;
    		case "hotel-list":
    			if( extra === "guest" ) {
    				dependency = "views/task/hotel/guest-list";
    				//console.log('oh yeah baby');
    			} else if( extra === "student" ) {
    				dependency = "views/task/hotel/student-list";
    			}
    		break;
    		*/
    		case "hotel-rooms":
    			var a = actions.split('/');
    			item1 = a[1]; item2 = a[2];
    			if(extra === 'view') {
    				switch( a[0] ) {
    					case "logs":
    	    				dependency = "views/task/hotel/view-logs";
    					break;
    				}
    			} else if ( extra === "add" ) {
    				switch( a[0] ) {
    					case "guest":
    						dependency = "views/task/hotel/add-guest";
    					break;
    					case "student":
    						dependency = "views/task/hotel/add-student";
    					break;
    				}
    			} else if ( extra === "checkout" ) {
    				// we will change the values of the item1, item2, ... variables;
    				item1 = a[0]; item2 = a[1]; item3 = a[2]; item4 = a[3];
    				
    				dependency = "views/task/hotel/checkout";
    			}
    		break;
    	}
    	require([dependency], function(View){
    		View.render(item1, item2, item3, item4);
    	});
    },
    /* Profile */
    profile: function() {
    	require(['views/profile/profile'], function(profileView) {
    		profileView.render();
    	});
    },
    /* Profile Type */
    profileType: function(type) {
    	var dependency = "";
    	switch(type) {
    		case "information":
    			dependency = 'views/profile/information';
    			break;
    	}
    	require([dependency], function(View) {
    		View.render();
    	});
    },
    /* Form */
    form: function(id) {
    	require(['views/forms/leave/form'], function(formView) {
    		formView.render();
    	});
    },
    /* Form Type */
    formType: function(type) {
    	//console.log(type);
    },
    formTypeExtra: function(type, extra) {
    	var dependency = "";
    	switch(type) {
    		case "leave":
    			if(extra === 'form') {
    				dependency = "views/forms/leave/form";
    			} else if ( extra === 'list' ) {
    				dependency = "views/forms/leave/list";
    			} else if ( extra === 'summary' ) {
    				dependency = "views/forms/leave/summary";
    			}
    		break;
    		case "srs":
    			if(extra === 'form') {
    				dependency = "views/forms/srs/form";
    			} else if (extra === 'list') {
    				dependency = "views/forms/srs/list";
    			}
    		break;
    		case "prs":
    			if(extra === 'form') {
    				dependency = "views/forms/prs/form";
    			} else if (extra === 'list') {
    				dependency = "views/forms/prs/list";
    			}
    		break;
    		case "pty":
    			if(extra === 'form') {
    				dependency = "views/forms/petty_cash/form";
    			} else if (extra === 'pending') {
    				dependency = "views/forms/petty_cash/pending";
    			} else if (extra === 'approved') {
    				dependency = "views/forms/petty_cash/approved";
    			}
    		break;
    	}
    	require([dependency], function(View) {
    		View.render();
    	});
    },
    /* Asset */
    asset: function() {
    	require(['views/assets/asset_list'], function(assetView) {
    		assetView.render();
    	});
    },
    /* Asset Type */
    assetType: function(type) {
        var dependency = "";
    	switch(type) {
    		case "asset-list":
    			dependency = "views/assets/asset_list";
            break;
            case "transfered":
    			dependency = "views/assets/transfered";
            break;
            case "returned":
    			dependency = "views/assets/returned";
            break;
            default:
            	dependency = "views/assets/accountable_asset/transfered";
            break;
    	}
    	require([dependency], function(View) {
    		View.render();
    	});
    },
    /* Asset Type Extra */
    assetTypeExtra: function(type, extra) {
        var dependency = "";
    	switch(type) {
         case "accountable-asset":
            if(extra === 'transfered'){
                dependency = "views/assets/accountable_asset/transfered";
            }
            else if(extra === 'assigned'){
                dependency = "views/assets/accountable_asset/assigned";    
            }
          break;
        }
    	require([dependency], function(View) {
    		View.render();
    	});
    }
  });
  var initialize = function(){
    var app_router = new AppRouter;
    Backbone.history.start();
  };
  return {
    initialize: initialize
  };
});
