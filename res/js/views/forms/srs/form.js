define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/forms/srs/form.html',
    'text!tpl/submenu.html',
    'collections/forms_menu/submenuFormsCollection',
    'baseURL',
    'jQ-plugins/lbox',
    'text!tpl/forms/srs/srs-display-items.html',
    'text!tpl/forms/srs/srs-display-stock-items.html',
    'jsHelper'
], function($, _, Backbone, srsF, submenuTemplate, submenuFormsCollection, baseURL, lbox, srsDisplayItems, stockItems, jsHelper){
	var retrieve_form_info = Backbone.Model.extend({});
	var stock_items = Backbone.Model.extend({});
	var save_srs = Backbone.Model.extend({});
	
	// add the model urls
	stock_items.prototype.url = baseURL+"forms/retrieve/stock-items";
	retrieve_form_info.prototype.url = baseURL+"forms/retrieve/srs-form-info";
	
	var si = new stock_items;
	var rfi = new retrieve_form_info;
	
	
	// when hovering the list of stock items 
	jsHelper.hovered_items(".srs-stock-lists");
	
	// click events when editing the srs items
	jsHelper.editing_items('.srs-form-list-contents-description, .srs-form-list-contents-qty, .srs-form-list-contents-uprice');
	
	$(".srs-select-items a").live("click", function(evt) {
		evt.preventDefault();
		var content = _.template(stockItems, {srs_ids: $.displayed_items.ids, si_data: si.attributes, in_array: jsHelper.inArray});
		//console.log($.displayed_items.ids);
		// show stock items in the lightbox
		$.lbox({
			title: "Select Stocks", 
			content: content,
			buttons: { cancel: true, submit: true, close: false }
		}, function(data){
			$.each(data, function(index, element){
				// push the id to the $.displayed-items variable
				// so that we will know which id's were selected
				$.displayed_items.ids.push(element);
			});
			var d = {
				srs_ids: $.displayed_items.ids,
				si_data: si.attributes,
				_: _
			};
			var srsflist = _.template(srsDisplayItems, d);
			$(".srs-form-lists").html(srsflist);
		});
	});
	
	
	var srsForm = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {},
		events: {
			"click .srs-form-list-contents-delete a": "doDelete",
			"click .srs-submit-form input[name='submit_srs']": "doSave"
		},
		doSave: function(evt) {
			//console.log(evt.currentTarget);
			var purpose = $("input[name='srs_purpose']").val();
			var date_required = $("input[name='srs_date_required']").val();
			var $e = $(".srs-form-lists");
			
			var save_srs_request = {};
			validation = jsHelper.validate("input[type='text']", $(".srs-form-header"));
			
			if( !_.isEmpty($.displayed_items.ids)  && validation !== true ) {
				for(var x in $.displayed_items.ids)
					save_srs_request[$.displayed_items.ids[x]] = $(".srs-content-"+$.displayed_items.ids[x]).find(".item-qty").html();
				
				save_srs.prototype.url = baseURL+"forms/submit/srs-form";
				var srs_save = new save_srs;
				var d = $.extend({}, validation, {ajax_request: "true", srs_items: JSON.stringify(save_srs_request)});
				srs_save.set(d);
				Backbone.emulateJSON = true;
				srs_save.save({}, {
					success: function() {
						$.displayed_items = {ids: []}; // unset the id's
						Backbone.history.navigate("#/forms/srs/list");
					}
				}); //save data to the database

			}

		},
		doDelete: function(evt) {
			//console.log(evt.currentTarget);
			var id = $(evt.currentTarget).attr("id");
			$("."+id).fadeOut(500, function() {
				$(this).remove();
				id = id.split("-");
				id = id[2];
				// remove the ids in the $.displayed_items variable
				for( var x in $.displayed_items.ids ) {
					if( $.displayed_items.ids[x] === id ) {
						$.displayed_items.ids.splice(x, 1);
					}
				}
				// set the value to empty if there is no id displayed
				if(_.isEmpty($.displayed_items.ids)) $(".srs-form-lists").html("");
				
			});
		},
		render: function() {
			$.displayed_items = {ids: []}; // ids of the stock items that will be added to the list..
			var disp_data = {};
			var data = {
				subs: submenuFormsCollection.models,
				_: _,
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			rfi.fetch({
				success: function() {
					si.fetch({
						success: function() {
							var si_data = {si_data: si.attributes};
							var d = _.template(srsF, $.extend({},rfi.attributes, si_data));
							$('#container').html(d);
							$("#srs_date_required").datepicker();
						}
					});
				}
			});
		}
	});
	return new srsForm;
});