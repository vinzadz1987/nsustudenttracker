define([
    'jquery',
    'underscore',
    'backbone',
    'collections/forms_menu/submenuFormsCollection',
    'text!tpl/submenu.html',
    'text!tpl/forms/srs/list.html',
    'text!tpl/forms/srs/list-template.html',
    'jsHelper',
    'baseURL'
], function($, _, Backbone, submenuFormsCollection, submenuTemplate, listView, listTemplate, jsHelper, baseURL) {
	var srs_list = Backbone.Model.extend({});
	var cancel_srs = Backbone.Model.extend({});
	
	jsHelper.page_pagination(".forms-list", listTemplate);
	jsHelper.hovered_items(".srs-pendapvd-list");
	
	var View = Backbone.View.extend({
		el: $('#container'),
		events: {
			"click .srs-list-search": "doSearchSrs",
			"click .cancel-srs":"doCancel"
		},
		doCancel: function(evt) {
			var id = $(evt.currentTarget).attr("class");
			id = id.split(" "); id = id[1];
			cancel_srs.prototype.url = baseURL+"forms/cancel/srs";
			var cancelSrs = new cancel_srs;
			cancelSrs.set({"ajax_request": "true", "id": id});
			
			Backbone.emulateHTTP = true;
			Backbone.emulateJSON = true;
			
			cancelSrs.save({}, {
				success: function(){
					//console.log("test");
					Backbone.history.loadUrl();
				}
			});
			//console.log(cancelSrs.attributes);
			//console.log(id);
		},
		doSearchSrs: function(evt) {
			delete srsList;
			var $period_from = $("#period_from").val();
			var $period_to = $("#period_to").val();
			var $status = $("#forms_list_status").val();
			
			if( $period_from && $period_to && $status ) {
				$.ajax({
					type: "POST",
					url: baseURL+"forms/search/srs-list/",
					data: {
						ajax_request: "true",
						period_from: $period_from,
						period_to: $period_to,
						status: $status
					},
					success: function(data) {
						data = JSON.parse(data);
						var d = _.template(listTemplate, $.extend({}, data, {_:_}));
						$('.forms-list').html(d);
					}
				});	
			}
		},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			srs_list.prototype.url = baseURL+"forms/retrieve/srs-list";
			var srsList = new srs_list;
			
			srsList.fetch({
				success: function() {
					var data = _.template(listView, $.extend({}, srsList.attributes, {_:_}));
					$('#container').html(data);
					
					$("#period_from").datepicker();
					$("#period_to").datepicker();
				}
			});
		}
	});
	return new View;
});