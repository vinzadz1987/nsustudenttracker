define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/forms/petty_cash/form.html',
    'text!tpl/submenu.html',
    'collections/forms_menu/submenuFormsCollection'
], function($, _, Backbone, srsF, submenuTemplate, submenuFormsCollection){
	var rView = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			this.el.html(srsF);
		}
	});
	return new rView;
});