define([
	'jquery',
	'underscore',
	'backbone',
	'text!tpl/forms/form_view.html',
	'text!tpl/submenu.html',
	'collections/forms_menu/submenuFormsCollection'
], function($, _, Backbone, formView, submenuTemplate, submenuFormsCollection){
	var form_view = Backbone.View.extend({
		el: $('#container'),
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			this.el.html(formView);
		}
	});
	return new form_view;
});