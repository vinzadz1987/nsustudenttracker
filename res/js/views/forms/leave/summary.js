define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/forms/leave/summary.html',
    'text!tpl/submenu.html',
    'collections/forms_menu/submenuFormsCollection',
    'baseURL'
], function($, _, Backbone, leaveSummary, submenuTemplate, submenuFormsCollection, baseURL){
	var summary_of_leaves = Backbone.Model.extend({});
	var leaveSummry = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {
			
		},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			summary_of_leaves.prototype.url = baseURL+"forms/retrieve/leave-summary";
			var s = new summary_of_leaves;
			s.fetch({success: function() {
				var summary_data = _.template(leaveSummary, s.attributes);
				$('#container').html(summary_data);
			}});
		}
	});
	return new leaveSummry;
});