define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/forms/leave/form.html',
    'text!tpl/submenu.html',
    'collections/forms_menu/submenuFormsCollection',
    'baseURL',
    'jsHelper'
], function($, _, Backbone, leaveF, submenuTemplate, submenuFormsCollection, baseURL, jsHelper){
	var leaveForm = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {},
		events: {
			'click .submit-leave': "doLeaveSubmit"
		},
		doLeaveSubmit: function(evt) {
			var $form_data = $(evt.currentTarget).parent().parent();
			// validate fields
			var validation = jsHelper.validate('input[type="radio"]:checked, input[type="text"], textarea, select', $form_data);
			// create an ajax request if the returned value is a json object
			if( validation !== true ) {
				var $data = {};
				$data = validation;
				$data["ajax_request"] = "true";
				
				$ajax_request = {
					type: "POST",
					url: baseURL+'forms/submit/leave-form',
					data: $data,
					success: function() {
						Backbone.history.navigate("#/forms/leave/list");
					}
				};
				$.ajax($ajax_request);
			}
		},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			var $a_jax = {
				type: "POST",
				data: {ajax_request: "true"},
				url: baseURL+"forms/retrieve/leave-form-info",
				success: function(data) {
					data = JSON.parse(data);
					data["_"] = _;
					var d = _.template(leaveF, data);
					$('#container').html(d);
					$("#date_requested, #firstday_leave, #return_date, #lastday_leave").datepicker();
				}
			};
			$.ajax($a_jax);
		}
	});
	return new leaveForm;
});