define([
	'jquery',
	'underscore',
	'backbone',
	'text!tpl/forms/leave/list.html',
	'text!tpl/submenu.html',
	'collections/forms_menu/submenuFormsCollection',
	'baseURL',
	'text!tpl/forms/leave/leave-list-search-template.html',
	'text!tpl/forms/leave/leave-list-contents.html',
	'jsHelper'
], function($, _, Backbone, leaveL, submenuTemplate, submenuFormsCollection, baseURL, searchTemplate, leaveListContents, jsHelper) {
	var page_pagination = Backbone.Model.extend({});
	var cancel_leave = Backbone.Model.extend({});
	
	
	$(".page-pagination a").live("click", function(e) {
		e.preventDefault();
		var url = $(this).attr("href");
		//console.log(url);
		page_pagination.prototype.url = url;
		var pagePagination = new page_pagination;
		pagePagination.fetch({
			success: function() {
				var searched_data = _.template(leaveListContents, $.extend({}, pagePagination.attributes, {_:_}));
				$('.leave-list-contents').html(searched_data);
				//$('.page-pagination').html(pagePagination.get("pagination"));
			}
		});
	});
	
	jsHelper.hovered_items(".leave-contents-list");
	
	// click events for pagination
	$(".search-pagination a").live("click",function(e) {
		e.preventDefault();
		
		var $period_from = $("#period_from").val();
		var $period_to = $("#period_to").val();
		var $status = $("#leave_list_status").val();
		var $url = $(this).attr('href');
		
		if( $period_from !== "" && $period_to !== "" )
			search.results( $period_from, $period_to, $status, $url );
	});
	// search function for pagination, if there is
	var search = {
		results: function( $period_from, $period_to, $status, $url ) {
			$a_jax = {
				type: "POST",
				url: $url,
				data: {
					ajax_request: "true",
					period_from: $period_from,
					period_to: $period_to,
					status: $status
				},
				success: function(data) {
					var $d = $.extend({}, JSON.parse(data), { _:_ });
					var searched_data = _.template(searchTemplate, $d);
					$('.leave-list-contents').html(searched_data);
				}
			};
			$.ajax($a_jax);
		}
	};
	// backbone view
	var leaveList = Backbone.View.extend({
		el: $('#container'),
		events: {
			"click #leave_list_search": "doSearch",
			"click .cancel-leave": "doCancel"
		},
		doCancel: function(evt) {
			//evt.currentTarget;
			var id = $(evt.currentTarget).attr("class");
			id = id.split(" ");
			id = id["1"];
			cancel_leave.prototype.url = baseURL+"forms/cancel/leave";
			var c = new cancel_leave;
			c.set({ajax_request: "true", id: id});
			
			Backbone.emulateHTTP = true;
			Backbone.emulateJSON = true;
			
			c.save();
			Backbone.history.loadUrl();
		},
		doSearch: function() {
			var $period_from = $("#period_from").val();
			var $period_to = $("#period_to").val();
			var $leave_list_status = $("#leave_list_status").val();
			
			if( $period_from !== "" && $period_to !== "" ) {
				$a_jax = {
					type: "POST",
					url: baseURL+"forms/search/leave-list-status",
					data: {
						ajax_request: "true",
						period_from: $period_from,
						period_to: $period_to,
						status: $leave_list_status
					},
					success: function(data) {
						var $d = $.extend({}, JSON.parse(data), { _:_ });
						var searched_data = _.template(searchTemplate, $d);
						$('.leave-list-contents').html(searched_data);
					}
				};
				$.ajax($a_jax);
			}
		},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			$a_jax = {
				type: "POST",
				url: baseURL+"forms/retrieve/leave-list",
				data: {ajax_request: "true"},
				success: function(data) {
					var $d = $.extend({}, JSON.parse(data), { _:_ });
					$('#container').html(leaveL);
					var $data = _.template(leaveListContents, $d);
					$(".leave-list-contents").html($data);
					$("#period_from, #period_to").datepicker();
				}
			};
			$.ajax($a_jax);
		}
	});
	return new leaveList;
});