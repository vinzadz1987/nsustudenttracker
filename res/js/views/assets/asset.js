define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/submenu.html',
    'text!tpl/assets/asset_view.html',
	'collections/forms_menu/submenuAssetsCollection'
],function($, _, Backbone, subMenu, assetView, submenuAssetsCollection){
	var asset = Backbone.View.extend({
		el: $("#container"),
		render: function() {
			var data = {
				subs: submenuAssetsCollection.models,
				_: _
			};
			var sub = _.template(subMenu, data);
			$('#sub-menu').html(sub);
			this.el.html(assetView);
		}
	});
	return new asset;
});