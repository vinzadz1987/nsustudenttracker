define([
    'jquery',
    'underscore',
    'backbone',
    'jsHelper',
    'baseURL',
    'text!tpl/submenu.html',
    'text!tpl/assets/returned.html',
    'text!tpl/assets/returned-pagination-template.html',
	'collections/forms_menu/submenuAssetsCollection'
],function($, _, Backbone, jsHelper, baseURL, subMenu, assetView, returnedPaginationTemplate, submenuAssetsCollection) {
	var returned_assets = Backbone.Model.extend({});
	var cancel_action = Backbone.Model.extend({});
	
	returned_assets.prototype.url = baseURL+"assets/retrieve/returned";
	var returnedAssets = new returned_assets;
	
	jsHelper.hovered_items(".asset-return-list-field");
	jsHelper.page_pagination(".asset-returned-list", returnedPaginationTemplate);
	
	var aReturn = Backbone.View.extend({
		el: $("#container"),
		events: {
			"click .cancel-asset-return" : "doCancel"
		},
		doCancel: function(evt) {
			var id = $(evt.currentTarget).attr("class"); id = id.split(" "); id = id[1];
			
			cancel_action.prototype.url = baseURL+"assets/cancel/return";
			var cancelAction = new cancel_action;
			cancelAction.set({id: id});
			cancelAction.save({}, {
				success: function() {
					Backbone.history.loadUrl();
				}
			});
			
		},
		render: function() {
			var sub = _.template(subMenu, { subs: submenuAssetsCollection.models, _: _});
			$('#sub-menu').html(sub);
			returnedAssets.fetch({
				success: function() {
					var data = _.template(assetView, $.extend({}, returnedAssets.attributes, {_:_}));
					$("#container").html(data);
				}
			});
		}
	});
	return new aReturn;
});