define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'jsHelper',
    'jQ-plugins/lbox',
    'jQ-plugins/jquery.box_step',
    'jQ-plugins/jquery.lionbars.0.3',
    'text!tpl/submenu.html',
    'text!tpl/assets/asset_list.html',
    'text!tpl/assets/asset-list-view.html',
    'text!tpl/assets/asset_list_template.html',
    'text!tpl/assets/asset-transfer-template/search-results.html',
	'collections/forms_menu/submenuAssetsCollection'
],function($, _, Backbone, baseURL, jsHelper, lbox, box_step, lionbars, subMenu, assetView, assetListView, assetListTemplate, searchResults, submenuAssetsCollection){
	var asset_list = Backbone.Model.extend({});
	var get_asset = Backbone.Model.extend({});
	var assets_withoutpagination = Backbone.Model.extend({});
	var return_asset = Backbone.Model.extend({});
	var transfer_to = Backbone.Model.extend({});
	var save_transfer = Backbone.Model.extend({});
	
	// list of assets
	asset_list.prototype.url = baseURL+"assets/retrieve/asset-list";
	var assetList = new asset_list;
	
	
	transfer_to.prototype.url = baseURL+"assets/retrieve/name-to-transfer";
	var nametoTransfer = new transfer_to;
	nametoTransfer.fetch();
	
	// return the assets that is not paginated
	assets_withoutpagination.prototype.url = baseURL+"assets/retrieve/all-assets";
	var awp = new assets_withoutpagination;
	
	
	jsHelper.page_pagination(".container-contents", assetListTemplate);
	jsHelper.hovered_items(".asset-list-content, .asset-transfer-search-contents");
	
	var aView = Backbone.View.extend({
		el: $("#container"),
		events: {
			"click #asset-list-content" : "showDetails",
			"click .asset-transfer" : "doTransfer",
			"click .asset-return" : "doReturn"
		},
		doTransfer: function(evt) {
			awp.fetch({
				success: function() {
					$.box_step({
						title: ["Please select assets to transfer", "List of assets to be transfered"],
						contents: ["transfer-assets-1.html", "transfer-assets-2.html"],
						data: $.extend({}, awp.attributes, nametoTransfer.attributes),
						selector: ".select-asset-list-content",
						baseURL: baseURL+"res/tpl/assets/asset-transfer-template/",
						lionbars: [".disp-assets-transret", ".asset-transfer-search-results"],
						events: {
							"keyup #search-transfer-name": "doKeyup",
							"click .asset-transfer-search-contents": "doClick"
						},
						// click event when the list of names on the search list will be clicked
						doClick: function(evt) {
							var $this = $(this);
							var transferTo = $this.attr("id");

							$(".asset-transfer-search-contents").removeClass("item-disabled")
							.removeClass("item-italic").css({background: "#fff"});

							$this.addClass("item-disabled item-italic").css({background: "#f1f1f1"});
							$.dataResponse["transferto"] = transferTo; // add the transfer to id to the data
						},
						// this function will be fired when the user searches a name on where to transfer the assets
						doKeyup: function(evt) {
							var value = $(evt.target).val();
							$.ajax({
								type: "POST",
								data: {name: value},
								url: baseURL+"assets/retrieve/name-to-transfer",
								success: function(data) {
									var returned_data = JSON.parse(data);
									var dta = _.template(searchResults, $.extend({}, returned_data, {_:_}));
									$(".asset-transfer-search-results").html(dta);
								}
							});
						}
					}, function(data){ // callback function
						
						var ids = [];
						var transfer = $.dataResponse.transferto.split("-"); transfer = transfer[1];
						$.each(data, function(key, val){
							var id = val.split("-"); id = id[1];
							ids.push(id);
						});
						
						save_transfer.prototype.url = baseURL+"assets/action/transfer";
						var saveTransfer = new save_transfer;
						saveTransfer.set($.extend({ids: ids}, {transfer: transfer}));
						
						saveTransfer.save({},{
							success: function() {
								//Backbone.history.loadUrl();
								Backbone.history.navigate("#/assets/transfered");
								//window.history.go(0);
							}
						});
					});
				}
			});

		},
		doReturn: function(evt) {
			awp.fetch({
				success: function(){
					$.box_step({
						//title: "Return Assets", 
						title: ["Please select assets to be returned","List of assets to be returned"],
						contents: ["return-assets-1.html", "return-assets-2.html"],
						data: awp.attributes,
						selector: ".select-asset-list-content",
						baseURL: baseURL+"res/tpl/assets/asset-return-template/",
						lionbars: [".disp-assets-transret"]
					}, function(data){ // callback function of the $.box_step plugin
						var ids = [];
						// remove the "asset" word and store the id in the ids variable
						$.each(data, function(i,e){
							var id = e.split("-"); id = id[1];
							ids.push(id);
							//console.log(id);
						});
						
						return_asset.prototype.url = baseURL+"assets/action/return";
						var returnAsset = new return_asset;
						
						returnAsset.set({ids:ids});
						returnAsset.save({},{
							success: function() {
								//Backbone.history.loadUrl();
								Backbone.history.navigate("#/assets/returned");
								//window.history.go(0);
							}
						});
					});
				}
			});
		},
		showDetails: function(evt) {
			var $id = $(evt.currentTarget).attr("class");
			$id = $id.split(" "); $id = $id[1];

			get_asset.prototype.url = baseURL+"assets/asset_info/"+$id;
			var getAsset = new get_asset;
			getAsset.fetch({
				success: function() {
					var assets = getAsset.attributes;
					var asset_data = _.template(assetListView, $.extend({}, assets, {_:_}));
					$.lbox({
						title: "Asset Information - "+assets.brand,
						content: asset_data
					});
				}
			});
		},
		render: function() {
			
			var data = { subs: submenuAssetsCollection.models, _: _ };
			var sub = _.template(subMenu, data);
			$('#sub-menu').html(sub);
			
			assetList.fetch({
				success: function() {
					var data = _.template(assetView, $.extend({}, assetList.attributes, {_:_}));
					$("#container").html(data);
				}
			});
		}
	});
	return new aView;
});