define([
    'jquery',
    'underscore',
    'backbone',
    'jsHelper',
    'baseURL',
    'text!tpl/submenu.html',
    'text!tpl/assets/accountable_asset/transfered.html',
	'collections/forms_menu/submenuAssetsCollection'
],function($, _, Backbone, jsHelper, baseURL, subMenu, assetView, submenuAssetsCollection){
	var assets_transfered = Backbone.Model.extend({});
	var receive_transfer = Backbone.Model.extend({});
	
	assets_transfered.prototype.url = baseURL+"assets/retrieve/transfered-assets";
	var assetsTransfered = new assets_transfered;
	
	var aTransfer = Backbone.View.extend({
		el: $("#container"),
		events: {
			"click .receive-asset-transfer" : "receiveTransfer"
		},
		receiveTransfer: function(evt) {
			// recieve the transfered assets
			var id = $(evt.currentTarget).attr("class").split(" "); id = id[1];
			receive_transfer.prototype.url = baseURL+"assets/action/receive-transfer";
			
			var receiveTransfer = new receive_transfer;
			receiveTransfer.set({id:id});
			
			receiveTransfer.save({}, {
				success: function() {
					Backbone.history.navigate("#/assets/asset-list");
				}
			});
		},
		render: function() {
			var sub = _.template(subMenu, {subs: submenuAssetsCollection.models, _: _});
			$('#sub-menu').html(sub);
			assetsTransfered.fetch({
				success: function() {
					var data = _.template(assetView, $.extend({}, assetsTransfered.attributes, {_:_}));
					$("#container").html(data);
				}
			});
		}
	});
	return new aTransfer;
});