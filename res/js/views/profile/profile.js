define([
	'jquery',
	'underscore',
	'backbone',
	'text!tpl/profile/profile_view.html',
        'baseURL',
        'jsHelper'
], function($, _, Backbone, profileView, baseURL, jsHelper){
        $("body").on("hover", "div.profile-photo", function(e){
            if(e.type=="mouseenter") {
                $("div.option").removeClass("hide");
            } else if(e.type=="mouseleave") {
                $("div.option").addClass("hide");
            }
        });
        $("body").on("click", "div.edit_box", function(){
            var options = {};
            var  callback = {};
            $("div.popup_window_style").removeClass("hide");
            $("div.popup_window_style").show('fold', options, 500, null);
            
        });
        $("body").on("click", "div.popup_window_exit", function(){
            var options = {};
            var  callback = {};
            $("div.popup_window_style").addClass("hide");
            $("div.popup_window_style").hide('fold', options, 1000, null);
            
        });
        
        
     /*  $("body").on("submit", "#upload-pic", function(e){
            e.preventDefault();
           
             var $a_jax = {
                    type: "POST",
                    data: {ajax_request: "true"},
                    url: baseURL+"profile/do_upload/",
                    data: {
                        "title": $("#filename").val(),
                        "userfile": $("#userfile").val()
                    },
                    success: function(data) {
                            data = JSON.parse(data);
                            $.customAlert(data.res)
                    }

            };
            $.ajax($a_jax);
        });*/
	var profile_view = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {},
		render: function() {
			$('#sub-menu').html("");
			this.el.html(profileView);
                        $.post(baseURL+"profile",{
                        dir: "get_id"
                        }, function(data){
                            var eid = data.res;
                            $("input[name$=filename]").val(eid);
                            $("#prof").attr("src",data.img);
                        }, 'json');  
		}
               
	});
	return new profile_view;
});