define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/profile/profile_view.html',
    'text!tpl/profile/information_view.html',
    'baseURL'
], function($, _, Backbone, profileView, informationView, baseURL){
	var informationModel = Backbone.Model.extend({
		defaults: {},
		validate: function(attributes) {
			
		},
		initialize: function() {
			//this.bind(error, function(model, error) {
				
			//});
		}
	});
	var iModel = new informationModel;

	var information = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {
			
		},
		events: {
			"click .personal-details-edit": "doEdit",
			"click .personal-details-save": "doSave"
		},
		doSave: function(evt) {
			var user_id = iModel.get("user_id");
			var d_tab = $(evt.currentTarget).attr("rel");
			var $element = $(evt.currentTarget).parent().next();
			
			var $post_data = {};
			$post_data['user_id'] = user_id;
			$post_data['ajax_request'] = "true";
			
			$("input[type='text'], select", $element).each(function(index, element) {
				// assign values to the elements attribute name
				$post_data[$(element).attr("name")] = $(element).val();
			});
			$a_jax = {
				type: "POST",
				url: baseURL+"profile/update/"+d_tab,
				data: $post_data,
				success: function(data) {
					Backbone.history.loadUrl();
				}
			};
			$.ajax($a_jax);
		},
		doEdit: function(evt) {
			
			// add active class
			$(evt.currentTarget).parent().parent().addClass("active-save");
			
			var type = $(evt.currentTarget).attr("rel");
			switch(type) {
				case "personal-details":
					$(".personal-details").html($("#pd_save").html());
					$('#pd_bdate').datepicker({changeYear: true, changeMonth: true, yearRange: '1940:+10'});
				break;
				case "contact-details":
					$(".contact-details").html($("#cd_save").html());
				break;
				
				case "immigration":
					$(".immigration").html($("#im_save").html());
					$("#im_issued_date, #im_date_expiry, #im_review_date").datepicker();
				break;
			}
		},
		render: function() {
			$a_jax = {
				type: "POST",
				data: {ajax_request: "true"},
				url: baseURL+'profile/retrieve/information',
				success: function(data) {
					iModel.set(JSON.parse(data));
					var data = _.template(informationView, iModel.attributes);
					$('.contents').html(data);
					$('.personal-details').html($("#pd_details").html());
					$('.contact-details').html($("#cd_details").html());
					$('.emergency-contact').html($("#ec_details").html());
					$('.immigration').html($("#im_details").html());
                                        
                                        $.post(baseURL+"profile",{
                                        dir: "get_id"
                                        }, function(data){
                                            var eid = data.res;
                                            $("input[name$=filename]").val(eid);
                                            $("#prof").attr("src",data.img);
                                        }, 'json');  
				}
			};
			$.ajax($a_jax);
		}
	});
	
	return new information;
});