define([
	'jquery',
	'underscore',
	'backbone',
        'baseURL',
	'text!tpl/task/task_view.html',
	'text!tpl/task/acad/change_requests.html',
], function($, _, Backbone,base_url, taskView, changeRequests) {
    var change_requests_cnv = {
        request_no: null,
        retrieve_requests_list: function(container,param) {
            $.post(base_url+"acad/change_request_list",{
                dir: "retrieve_change_request_list",
                data: param
            },function(data){
                container.html(data.res).append(data.pagination);
            },"json");
        }
    };
    //initialize date picker
    $("body").on("click","input[name$=date_param_from], input[name$=date_param_to], input[name$=acad_date_rcv]",function(){
        $(this).datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends});
        $(this).datepicker({showOn: 'focus'}).focus();
    });
    //view change request details
    $("body").on("click","div.view-request-detail-btn",function(){
        var today = new Date();
        $("input[name$=acad_date_rcv]").val($.datepicker.formatDate('yy-mm-dd', new Date(today.getFullYear(), today.getMonth(), today.getDate())));
        var elem = $(this);
        change_requests_cnv.request_no = $(this).parent().attr("id").split("_",2)[1];
        $.post(base_url+"acad/change_request_list",{
            dir: "retrieve_details",
            rid: change_requests_cnv.request_no
        },function(data){
            $("div.change_request_details label#student_name").html(data.student_name);
            $("div.change_request_details label#student_id").html(data.student_id);
            $("div.change_request_details textarea[name$=request_detail]").val(data.request_detail);
            $("div.change_request_details label#others_specify").val(data.others_specify);
            $.each($("div.change_request_details input[name$=request_type]"),function(i,elem){
                if($(elem).val()=="instructional_hours" && data.instructional_hours == 1) {
                    $(elem).attr("checked","checked");
                }
                else if($(elem).val()=="classroom" && data.classroom == 1) {
                    $(elem).attr("checked","checked");
                }
                else if($(elem).val()=="additional_class" && data.additional_class == 1) {
                    $(elem).attr("checked","checked");
                }
                else if($(elem).val()=="class_swapping" && data.class_swapping == 1) {
                    $(elem).attr("checked","checked");
                } 
                else if($(elem).val()=="course_extension" && data.course_extension == 1) {
                    $(elem).attr("checked","checked");
                } 
                else if($(elem).val()=="others" && data.others == 1) {
                    $(elem).attr("checked","checked");
                } else {
                    $(elem).removeAttr("checked");
                }
            });
            if(data.acad_approval != "0") {
                $("input[name$=acad_approval]").attr("checked","checked");
            } else {
                $("input[name$=acad_approval]").removeAttr("checked");
            }
            if(data.sales_approval != "0") {
                $("input[name$=sales_approval]").attr("checked","checked");
            } else {
                $("input[name$=sales_approval]").removeAttr("checked");
            }
            $("input[name$=acad_date_rcv]").val(data.acad_date_rcv);
            $("input[name$=sales_date_rcv]").val(data.sales_date_rcv);
            elem.litebox("div.change_request_details",{show:true});
        },"json");
    });
    //approve change request
    $("body").on("click","input[name$=acad_approval]",function(){
        var approval = $(this).attr("checked") ? true:false;
        $(this).litebox("div.change_request_details",{close:true});
        $.post(base_url+"acad/change_request",{
            dir: "approve_change_request",
            data: {
                approval: approval,
                acad_date_rcv: $("input[name$=acad_date_rcv]").val(),
                rid: change_requests_cnv.request_no
            }
        },function(data){
            if(data.result===true) {
                $.customAlert("Academics approval updated.");
            } else {
                $.customAlert("An error occurred. Please refresh this page then try again.");
            }
        },"json");
    });
    //hover for each lesson plan row
    $("body").on("hover","div.change_request_container div.lp_row",function(e){
        if(e.type=="mouseenter") {
            var elem = $(this);
            $("div.lp_row").children("div.view-lesson-plan-btn").remove();
            elem.append($('<div class="sprite-inspect view-request-detail-btn to-right"></div>'));
        }
        else if(e.type=="mouseleave") {
            $("div.view-request-detail-btn").stop(true,true).remove();
        }
    });
/*search mod*/
    $("body").on("change","div.change_requests input[name$=date_param_from]",function(){
        console.log("here");
        if($(this).val()!="" && $("input[name$=date_param_to]").val()!="" && $("select[name$=category_date]").val()!="") {
            var params = {
                date_from : $("input[name$=date_param_from]").val(),
                date_to : $("input[name$=date_param_to]").val(),
                category : $("select[name$=category_date]").val()
            }
            change_requests_cnv.retrieve_requests_list($('div.dyn_content'),params);
        }
    });
    $("body").on("change","div.change_requests input[name$=date_param_to]",function(){
        if($(this).val()!="" && $("input[name$=date_param_from]").val()!="" && $("select[name$=category_date]").val()!="") {
            var params = {
                date_from : $("input[name$=date_param_from]").val(),
                date_to : $("input[name$=date_param_to]").val(),
                category : $("select[name$=category_date]").val()
            }
            change_requests_cnv.retrieve_requests_list($('div.dyn_content'),params);
        }
    });
    $("body").on("change","div.change_requests select[name$=category_date]",function(){
        if($(this).val()!="" && $("input[name$=date_param_from]").val()!="" && $("input[name$=date_param_to]").val()!="") {
            var params = {
                date_from : $("input[name$=date_param_from]").val(),
                date_to : $("input[name$=date_param_to]").val(),
                category : $("select[name$=category_date]").val()
            }
            change_requests_cnv.retrieve_requests_list($('div.dyn_content'),params);
        }
    });
/*end of search mod*/
    var action_form = Backbone.View.extend({
        el: $('#container'),
        initialize: function() {
        },
        render: function() {
            $('.contents').html(changeRequests);
            change_requests_cnv.retrieve_requests_list($('div.dyn_content'));
        }
    });
    return new action_form;
});