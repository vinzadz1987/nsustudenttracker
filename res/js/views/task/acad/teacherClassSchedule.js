define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/teacher_class_schedule.html'
],function($,_,Backbone,base_url,teacherClassSchedule){
    var teacher_class_schedule_cnv = {
        getMySchedule: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/teacher_class_schedule",{
                dir: "retrieve_my_schedule"
            },function(data){
                container.html(data);
            });
        }
    };
    //view class student(s)
    $("body").on("click","table#my_class_schedule div.view_students_btn",function(){
        var e = $(this);
        $.post(base_url+"acad/teacher_class_schedule",{
            dir: "fetch_my_students",
            cid: e.parent().parent().attr("id")
        },function(data){
            if(data!="") {
                $('div.my_class_students').html(data);
            } else {
                $('div.my_class_students').html("No student yet.");
            }
            e.litebox('div.my_class_students',{show:true});
        });
    });
    //print page
    $("body").on("click","button[name$=print_page]",function(){
        window.print();
    });
    var teacher_class_schedule = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('.contents').html(teacherClassSchedule);
            teacher_class_schedule_cnv.getMySchedule($('div.dyn_content'));
        }
    });
    return new teacher_class_schedule;
});