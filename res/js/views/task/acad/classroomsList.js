define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/classrooms_list.html'
],function($,_,Backbone,base_url,classroomsList){
    var classrooms_cnv = {
        getClassrooms: function(param,container,status,time) {
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/classrooms",{
                dir: "retrieve_classrooms",
                like: param, stat: status, sched:time
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        updateClassroom: function(update_info) {
            $.post(base_url+"acad/classrooms",{
                dir: "edit_classroom",
                data: {
                    rid: update_info.classroom_no.val(),
                    name: update_info.name.val()
                }
            },function(data){
                if(data.result===true) {
                    classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'));
                    $.customAlert("Classroom Updated");
                }
            },"json");
        },
        rmClassroom: function(classroom) {
            $.post(base_url+"acad/classrooms",{
                dir: "remove_classroom",
                rid: classroom
            },function(data){
                if(data.result===true) {
                    classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            },"json");
        }
    };
    $("body").ajaxError(function(e,jqxhr,settings,exception){
        if(settings.data.search("remove_classroom")>0 && jqxhr.status === 500) {
            $.customAlert('Unable to delete classroom. Classroom has scheduled class.<br />');
        }
    });
    //add classroom
    $("body").on("submit","#addclassroom",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message).litebox("div.err_msg",{show:true});
            } else {
                $.post(base_url+"acad/classrooms",{
                    dir: "classroom_add",
                    data: {
                        "room": $("input[name$=classroom-name]").val()
                    }
                },function(data){
                    if(data.result===true) {
                        $("input[name$=srch_classroom_name]").val("")
                        classrooms_cnv.getClassrooms("",$('div.dyn_content'));
                        $("#addclassroom")[0].reset();
                    }
                },"json");
            }
        });
    });
    //search
    $("body").on("keyup","div.search-box input[name$=srch_classroom_name]",function(){
        $(this).siblings("button").removeClass("gray-gradient selected");
        classrooms_cnv.getClassrooms($(this).val(),$('div.dyn_content'),"",$("select[name$=srch_time]").val());
    });
    //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        var status = "";
        if($("div.search-box button[name$=classroom_occupied]").hasClass("selected")) {
            status = "occupied";
        }
        else if($("div.search-box button[name$=classroom_vacant]").hasClass("selected")) {
            status = "vacant";
        }
        $.post($(this).attr("href"),{
            like: $("input[name$=srch_classroom_name]").val(), stat: status
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    //edit classroom initialize form
    $("body").on("click","div.edit-classroom-btn",function(){
        var classroom = {
            no: $(this).parent().attr("id").split("_",2)[1],
            name: $(this).siblings('.classroom-name').html()
        };
        $("#edit-classroom input[name$=edit-id]").val(classroom.no);
        $("#edit-classroom input[name$=edit-classroom-name]").val(classroom.name);
        $(this).litebox("div.classroom-edit",{show:true});
    });
    //edit classroom submit form
    $("body").on("submit","#edit-classroom",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    classroom_no: $("#edit-classroom input[name$=edit-id]"),
                    name: $("#edit-classroom input[name$=edit-classroom-name]")
                };
                classrooms_cnv.updateClassroom(update_info);
            }
        });
    });
    //delete classroom
    $("body").on("click","div.delete-classroom-btn",function(){
        var rid = $(this).parent().attr("id").split("_",2)[1];
        $.customConfirm("Are you sure you want to delete this classroom?",function(response){
            if(response===true) {
                classrooms_cnv.rmClassroom(rid);
            }
        });
    });
    //display occupied classrooms
    $("body").on("click","div.search-box button[name$=classroom_occupied]",function(){
        if($(this).hasClass("gray-gradient selected")) {
            $(this).removeClass("gray-gradient selected");
            classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'));
        } else {
            $(this).siblings("button").removeClass("gray-gradient selected");
            $(this).addClass("gray-gradient selected");
            classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'),"occupied");
        }
    });
    //display vacant classrooms
    $("body").on("click","div.search-box button[name$=classroom_vacant]",function(){
        if($(this).hasClass("gray-gradient selected")) {
            $(this).removeClass("gray-gradient selected");
            classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'));
        } else {
            $(this).siblings("button").removeClass("gray-gradient selected");
            $(this).addClass("gray-gradient selected");
            classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'),"vacant");
        }
    });
    //by-time search
    $("body").on("change","div.search-box select[name$=srch_time]",function(){
        $(this).siblings("button").removeClass("gray-gradient selected");
        classrooms_cnv.getClassrooms($("div.search-box input[name$=srch_classroom_name]").val(),$('div.dyn_content'),"",$(this).val());
    });
    //inspect classroom occupant
    $("body").on("click","div.classrooms_container div.inspect-classroom-btn",function(){
        var classroom = $(this).parent().attr("id").split("_",2)[1];
        var class_scheduled = $(this).siblings(".classes-scheduled").html();
        $('div.occupants').html($('<div class="loading-style1"></div>'));
        $(this).litebox("div.occupants",{show:true});
        if(class_scheduled==='0') {
            $('div.occupants').html('No class scheduled.');
        } else {
            $.post(base_url+"acad/classrooms",{
                dir: "classroom_occupants",
                rid: classroom
            },function(data){
                if(data.result===true) {
                    $('div.occupants').html(data.occupants);
                }
            },"json");
        }
    });
    var classrooms_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(classroomsList);
            classrooms_cnv.getClassrooms("",$('div.dyn_content'));
        }
    });
    return new classrooms_list;
});