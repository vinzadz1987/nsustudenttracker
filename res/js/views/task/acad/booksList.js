define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/books_list.html'
],function($,_,Backbone,base_url,booksList){
    var books_cnv = {
        getBooks: function(param,container) {
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/books",{
                dir: "retrieve_books",
                like: param
            },function(data){
                var rcv = $.parseJSON(data);
                container.html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
            });
        },
        updateBook: function(update_info) {
            $.post(base_url+"acad/books",{
                dir: "edit_book",
                data: {
                    book_id: update_info.id.val(),
                    title: update_info.title.val(),
                    class_type: update_info.class_type.val(),
                    course: update_info.course.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {
                    $("div.search-box input[name$=book_title]").val("");
                    books_cnv.getBooks("",$('div.dyn_content'));
                    $.customAlert("Book Updated.");
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        rmBook: function(book) {
            $.post(base_url+"acad/books",{
                dir: "remove_book",
                bid: book
            },function(data){
                if(data.result===true) {
                    $("div.search-box input[name$=book_title]").val("");
                    books_cnv.getBooks("",$('div.dyn_content'));
                    $.customAlert("Book Deleted.");
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            },"json");
        }
    }
    $("body").ajaxError(function(e,jqxhr,settings,exception){
        if(settings.data.search("remove_book")>0 && jqxhr.status === 500) {
            $.customAlert('Unable to delete book. Book is used by a class.<br />');
        } else {
            $.customAlert('An error occurred. Please refresh the page and try again.\n\
                <br />If the problem persist, send IT an email explaining the nature of the\n\
                problem. <br />'+exception);
        }
    });
    //add book
    $("body").on("submit","#addbook",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .litebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    title : $("input[name$=title]").val(),
                    class_type: $("select[name$=book-class-type]").val(),
                    course: $("select[name$=book-course]").val()
                }
                $.post(base_url+"acad/books",{
                    dir: "book_add",
                    data: {
                        "title": infos.title,
                        "class_type": infos.class_type,
                        "course": infos.course
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input[name$=book_title]").val("");
                        books_cnv.getBooks("",$('div.dyn_content'));
                        $("#addbook")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    //search
    $("body").on("keyup","div.search-box input[name$=book_title]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        books_cnv.getBooks($(this).val(),$('div.dyn_content'));
    });
    //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
            like: $("input[name$=book_title]").val()
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    //edit book initialize form
    $("body").on("click","div.edit-book-btn",function(){
        $.post(base_url+"acad/books",{
            dir: "retrieve_details",
            bid: $(this).parent().attr("id").split("_",2)[1]
        },function(data){
            var rcv = $.parseJSON(data);
            $("#edit-book input[name$=edit-id]").val(rcv.book.book_id);
            $("#edit-book input[name$=edit-book-title]").val(rcv.book.title).data('original',rcv.book.title);
            $("#edit-book select[name$=edit-book-class-type]").val(rcv.book.class_type).data('original',rcv.book.class_type);
            $("#edit-book select[name$=edit-book-course]").val(rcv.book.course_id).data('original',rcv.book.course_id);
            $(this).litebox("div.book-edit",{show:true});
        });
    });
    //delete book
    $("body").on("click","div.delete-book-btn",function(){
        var bid = $(this).parent().attr("id").split("_",[2])[1];
        $.customConfirm("Are you sure you want to delete this book?",function(response){
                if(response===true) {
                    books_cnv.rmBook(bid);
                }
            });
    });
    //edit book submit form
    $("body").on("submit","#edit-book",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit-book input[name$=edit-id]"),
                    title: $("#edit-book input[name$=edit-book-title]"),
                    class_type: $("#edit-book select[name$=edit-book-class-type]"),
                    course: $("#edit-book select[name$=edit-book-course]")
                };
                if(update_info.class_type.val() != update_info.class_type.data('original')) {
                    $.customConfirm("Editing this book's class type may create conflicts if the book is being used by a class.\n\
                                    <br /> Are you sure you want to continue?",function(response){
                                        if(response===true) {
                                            books_cnv.updateBook(update_info);
                                        }
                                    });
                }
                else if(update_info.course.val() != update_info.course.data('original')) {
                    $.customConfirm("Editing this book's course may create conflicts if the book is being used by a class.\n\
                                    <br /> Are you sure you want to continue?",function(response){
                                        if(response===true) {
                                            books_cnv.updateBook(update_info);
                                        }
                                    });
                }
                else {
                    books_cnv.updateBook(update_info);
                }
            }
        });
    });
    var books_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(booksList);
            books_cnv.getBooks("",$('div.dyn_content'));
            $.post(base_url+"acad/class_schedule",{
                dir: "initialization", only: "courses"
            },function(data){
                var rcv = $.parseJSON(data);
                $(".course").empty()
                .append($('<option></option>').val("").html(""));
                $.each(rcv.courses,function(i,val){
                    $(".course").append($('<option></option>')
                    .val(val.course_id).html(val.name));
                });
            });
        }
    });
    return new books_list;
});