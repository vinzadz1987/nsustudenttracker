define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/task/acad/class_schedule.html',
],function($,_,Backbone,base_url,taskView,classSched) {
    var classes_cnv = {
        updatee: null,
        resetForm: function(form) {
            form.not(':button, :submit, :reset, :hidden').val('')
            .removeAttr('checked').removeAttr('selected');
        },
        getClass: function(teacher,container) {
            $.post(base_url+"acad/class_schedule",{
                dir: 'teacher_class',
                tid: teacher
            },function(data){
                container.html(data);
            });
        },
        rmClass: function(id, tid, container) {
            $.post(base_url+"acad/class_schedule",{
                dir: 'rm_class',
                cid: id
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {
                    classes_cnv.getClass(tid,container);
                }
            });
        },
        chkRoomStat: function(room, time) {
            $.post(base_url+"acad/class_schedule",{
                dir: 'room_chk',
                data: {
                    room_id: room,
                    sched: time
                }
            },function(data){
                var rcv = $.parseJSON(data);
                return rcv;
            });
        },
        bookChange: function() {
            $.post(base_url+"acad/class_schedule",{
                dir: "class_type",
                type: $("select[name$=class-type]").val(),
                course: $("select[name$=course]").val()
            },function(data){
                var rcv = $.parseJSON(data);
                $("select[name$=book], select[name$=edit_book]").empty()
                .append($('<option></option>').val("").html(""));
                $.each(rcv.books,function(i,val){
                    $("select[name$=book], select[name$=edit_book]").append($('<option></option>')
                    .val(val.book_id).html(val.title));
                });

            });
        },
        getClassStudents: function(class_no,container) {
            $.post(base_url+"acad/class_schedule",{
                dir: "fetch_class_students",
                cid: class_no
            },function(data){
                container.html(data);
            });
        },
        update_class_sched: function(values) {
            $.post(base_url+"acad/class_schedule",{
                dir: "class_sched_change",
                cid: this.updatee,
                data: values
            },function(data){
                if(data.result==true) {
                    classes_cnv.getClass(data.teacher,$('div.dyn_content'));
                    $.customAlert("Class schedule updated.");
                } else {
                    $.customAlert("An error occurred. Please refresh this page and try again.");
                }
            },"json");
        }
    }
    //open class
    $("#openclass").live("submit",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .litebox("div.err_msg",{show:true});
            } else {
                //populate data;
                var infos = {
                    teacher : $("select[name$=teacher]").val(),
                    classroom: $("select[name$=classroom]").val(),
                    course: $("select[name$=course]").val(),
                    course_level: $("select[name$=course-level]").val(),
                    book: $("select[name$=book]").val(),
                    time: $("select[name$=sched-time]").val(),
                    class_type: $("select[name$=class-type]").val()
                }
                $("div.err_msg").html("Checking for conflicts... Please wait...")
                .litebox("div.err_msg",{show:true});
                $.post(base_url+"acad/class_schedule",{
                    dir: 'reality_chk',
                    data: {
                        room_id: infos.classroom,
                        sched: infos.time,
                        teacher: infos.teacher
                    }
                },function(data){
                    var rcv = $.parseJSON(data);
                    if(rcv.data.occupied===true) {
                        $("div.err_msg").html("Room occupied by "+rcv.data.teacher+" for "+rcv.data.class_type+" class ("+$("select[name$=sched-time]").val()+"). <br/>"
                        +"Please select another room or class time.");
                    }
                    else if(rcv.data.scheduled===true) {
                        $("div.err_msg").html(rcv.data.teacher+" already has a class at room "+rcv.data.room+"<br />"
                        +"Please select another time.");
                    }
                    else {
                        $.post(base_url+"acad/class_schedule",{
                            dir: 'class_add',
                            data: {
                                "teacher": infos.teacher,
                                "classroom": infos.classroom,
                                "course": infos.course,
                                "course_level": infos.course_level,
                                "book": infos.book,
                                "time": infos.time,
                                "class_type": infos.class_type
                            }
                        },function(data){
                            if(data==='added'){
                                classes_cnv.getClass(infos.teacher,$("div.dyn_content"));
                                $("#openclass")[0].reset();
                                $("select[name$=teacher]").val(infos.teacher);
                                $("div.err_msg").html("Class successfully added.");
                            } else {
                                $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                                .litebox("div.err_msg",{show:true});
                            }
                        });
                    }
                });
            }
        });
    });
    //set class to inactive
    $("body").on("click","table#teacher_class div.delete_btn",function(){
        var id = $(this).parent().parent().attr('id');
        $.customConfirm("Confirm class delete.",function(response){
            if(response===true) {
                classes_cnv.rmClass(id,$("select[name$=teacher]").val(),$('div.dyn_content'));
            }
        });
    });
    //initialize and open add student form
    $("body").on("click","table#teacher_class div.add_btn",function(){
        var e = $(this);
        var class_info = {
            id: $(this).parent().parent().attr("id"),
            type: $(this).parent().siblings(".sched_type").html(),
            time: $(this).parent().siblings(".sched_time").html()
        }
        $("div.teacher_students label#ctype").html(class_info.type);
        $("div.teacher_students label#ctime").html(class_info.time);
        $("input[name$=class]").val(class_info.id);
        classes_cnv.getClassStudents(class_info.id,$('div.class_students'));
        
        $.post(base_url+"acad/class_schedule",{
            dir: "fetch_students"
        },function(data){
            var rcv = $.parseJSON(data);
            $("select[name$=students_list]").empty()
            .append($('<option></option>').val("").html(""));
            $.each(rcv.students,function(i,val){
                $("select[name$=students_list]").append($('<option></option>')
                .val(val.id).html(val.student_name));
            });
            e.litebox("div.teacher_students",{show:true});
        });
    });
    //add a student to class
    $("body").on("submit","form#addstudent",function(e){
        e.preventDefault();
        var student = $("select[name$=students_list]").val(); var class_no = $("input[name$=class]").val();
        var cur_studs = $("div.class_students div.class_student").length;
        var type = $("div.teacher_students label#ctype").html(); var time = $("div.teacher_students label#ctime").html();
        if(type == '1:1' && cur_studs >= 1) {
            $.customAlert("Uh-ah! Not a chance. 1:1 Class is full. Try deleting the current student first.");            
        }
        else if(type == '1:4' && cur_studs >= 4) {
            $.customAlert("Uh-ah! Not a chance. 1:4 Class is full. Try deleting a student on the list first.");
        }
        else if(type == '1:8' && cur_studs >= 8) {
            $.customAlert("Uh-ah! Not a chance. 1:8 Class is full. Try deleting a student on the list first.");
        }
        else if(student=='') {
            $.customAlert("Please select a student.");
        } else {
            $('div.class_students').html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/class_schedule",{
                dir: "chk_student_class",
                sid: student, sched: time
            },function(data){
                $('div.class_students').empty();
                var rcv = $.parseJSON(data);
                if(rcv.scheduled===false) {
                    $.post(base_url+"acad/class_schedule",{
                        dir: "add_student_to_class",
                        sid: student, cid: class_no
                    },function(data){
                        var rcv = $.parseJSON(data);
                        if(rcv.result===true) {
                            $("form#addstudent")[0].reset();
                            classes_cnv.getClassStudents(class_no,$('div.class_students'));
                        } else {
                            $.customAlert('An error occurred. Please refresh the page and try again.');
                        }
                    });
                } else {
                    $.customAlert('Student already has '+rcv.details.class_type+" class with "+rcv.details.teacher+" at "+rcv.details.room_name+".");
                    classes_cnv.getClassStudents(class_no,$('div.class_students'));
                }
            });
        }
    });
    //remove student from class
    $("body").on("click","div.class_student div.del_student_btn",function(){
        var student = $(this).attr("id").split("_",2)[1]; var class_no = $("input[name$=class]").val();
        $.post(base_url+"acad/class_schedule",{
            dir: "rm_class_student",
            cid: class_no,
            sid: student
        },function(data){
            var rcv = $.parseJSON(data);
            if(rcv.result===true) {
                classes_cnv.getClassStudents(class_no,$('div.class_students'));
            } else {
                $.customAlert('An error occurred. Please refresh the page and try again.');
            }
        });
    });
    //get classes of selected teacher
    $("body").on("change","select[name$=teacher]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        classes_cnv.getClass($(this).val(),$('div.dyn_content'));
    });
    //book changes depending on class type and course
    $("body").on("change","select[name$=class-type], select[name$=course], select[name$=edit_class-type], select[name$=edit_course]",function(){
        classes_cnv.bookChange();
    });
    $("body").on("change","select[name$=edit_class-type], select[name$=edit_course]",function(){
        $.post(base_url+"acad/class_schedule",{
            dir: "class_type",
            type: $("select[name$=edit_class-type]").val(),
            course: $("select[name$=edit_course]").val()
        },function(data){
            var rcv = $.parseJSON(data);
            $("select[name$=book], select[name$=edit_book]").empty()
            .append($('<option></option>').val("").html(""));
            $.each(rcv.books,function(i,val){
                $("select[name$=book], select[name$=edit_book]").append($('<option></option>')
                .val(val.book_id).html(val.title));
            });

        });
    });
    //edit class schedule initialize
    $("body").on("click","table#teacher_class div.edit_class_btn",function(){
        var id = $(this).parent().parent().attr('id');
        classes_cnv.updatee = id;
        $.post(base_url+"acad/class_schedule",{
            dir: "retrieve_class_sched_values",
            cid: id
        },function(data){
            $("select[name$=edit_course]").val(data.course);
            $("select[name$=edit_course-level]").val(data.level);
            $("select[name$=edit_class-type]").val(data.class_type);
            $("select[name$=edit_sched-time]").val(data.time);
            $("select[name$=edit_classroom]").val(data.classroom).data({"original_value":data.classroom});
            $("select[name$=edit_book]").val(data.book);
            $(this).litebox("div.edit_class_sched",{show:true});
        },"json");
    });
    //edit class form submit (validate and update)
    $("body").on("submit","#edit-class",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var infos = {
                    classroom: $("select[name$=edit_classroom]").val(),
                    course: $("select[name$=edit_course]").val(),
                    course_level: $("select[name$=edit_course-level]").val(),
                    book: $("select[name$=edit_book]").val(),
                    time: $("select[name$=edit_sched-time]").val(),
                    class_type: $("select[name$=edit_class-type]").val()
                };
                if(infos.classroom != $("select[name$=edit_classroom]").data().original_value) {
                    $.post(base_url+"acad/class_schedule",{
                        dir: 'reality_chk',
                        data: {
                            room_id: infos.classroom,
                            sched: infos.time,
                            teacher: 0
                        }
                    },function(data){
                        var rcv = $.parseJSON(data);
                        if(rcv.data.occupied===true) {
                            $.customAlert("Room occupied by "+rcv.data.teacher+" for "+rcv.data.class_type+" class ("+$("select[name$=sched-time]").val()+"). <br/>"
                            +"Please select another room or class time.");
                        }
                        else {
                            classes_cnv.update_class_sched(infos);
                        }
                    });
                } else {
                    classes_cnv.update_class_sched(infos);
                }
            }
        });
    });
    var class_sched = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
        },
        events: {
//            "click #teacher-sched": "teacher_sched"
        },
        render: function() {
            $('div.contents').html(classSched);
            $.post(base_url+"acad/class_schedule",{
                dir: 'initialization'
            },function(data){
                var rcv = $.parseJSON(data);
                $("select[name$=teacher]").empty()
                .append($('<option></option>').val("").html(""));
                $("select[name$=classroom], select[name$=edit_classroom]").empty()
                .append($('<option></option>').val("").html(""));
                $("select[name$=course], select[name$=edit_course]").empty()
                .append($('<option></option>').val("").html(""));
                $("select[name$=course-level], select[name$=edit_course-level]").empty()
                .append($('<option></option>').val("").html(""));
                $("select[name$=book], select[name$=edit_book]").empty()
                .append($('<option></option>').val("").html(""));
                $.each(rcv.teachers,function(i,val){
                    $("select[name$=teacher]").append($('<option></option>')
                    .val(val.emp_id).html(val.firstname+" "+val.lastname));
                });
                $.each(rcv.classrooms,function(i,val){
                    $("select[name$=classroom], select[name$=edit_classroom]").append($('<option></option>')
                    .val(val.classroom_id).html(val.room_name));
                });
                $.each(rcv.courses,function(i,val){
                    $("select[name$=course], select[name$=edit_course]").append($('<option></option>')
                    .val(val.course_id).html(val.name));
                });
                $.each(rcv.course_level,function(i,val){
                    $("select[name$=course-level], select[name$=edit_course-level]").append($('<option></option>')
                    .val(val.id).html(val.name));
                });
                $.each(rcv.books,function(i,val){
                    $("select[name$=book], select[name$=edit_book]").append($('<option></option>')
                    .val(val.book_id).html(val.title));
                });
            });
        }
//        teacher_sched: function() {
//            console.log("here");
//        }
    });
    return new class_sched;
});