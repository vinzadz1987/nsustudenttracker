define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/teachers_list.html'
],function($,_,Backbone,base_url,teachersList){
    var teachers_cnv = {
        getTeachers: function(param,container,status) {
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/teachers",{
                dir: "retrieve_teachers",
                like: param, type: status
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getTeachersInfo: function(container,teacher) {
            $(this).litebox("div.teacher_info",{show:true});
            $.post(base_url+"acad/teachers",{
                dir: "retrieve_teacher_info",
                tid: teacher
            },function(data){
                //data.photo is also available though not used yet
                container.children("label.firstname").html(data.firstname);
                container.children("label.middlename").html(data.middlename);
                container.children("label.lastname").html(data.lastname);
                var street = (data.street=="") ? "":data.street+", ";
                var city = (data.city=="") ? "":data.city+" City, ";
                var province = (data.province=="") ? "":data.province+", ";
                container.children("label.address").html(street+city+province+data.country);
                container.children("label.gender").html(data.gender);
                container.children("label.email").html(data.email);
                container.children("label.nationality").html(data.nationality);
                container.children("label.zipcode").html(data.zipcode);
                container.children("label.telephone").html(data.telephone);
                container.children("label.mobile").html(data.mobile);
            },"json");
        }
    };
    //employment type change
    $("body").on("change","div.search-box input[name$=contract]",function(){
        teachers_cnv.getTeachers($("div.search-box input[name$=srch_teacher_name]").val(),$('div.dyn_content'),$(this).val());
    });
    //search box change
    $("body").on("keyup","div.search-box input[name$=srch_teacher_name]",function(){
        teachers_cnv.getTeachers($(this).val(),$('div.dyn_content'),$("div.search-box input[name$=contract]:checked").val());
    });
    //view teacher info
    $("body").on("click","div.teachers_container div.info-teacher-btn",function(){
        var teacher = $(this).parent().attr("id").split("_",2)[1];
        teachers_cnv.getTeachersInfo($('div.teacher_infos_cont'),teacher);
    });
    //view teacher class
    $("body").on("click","div.class-teacher-btn",function(){
        var teacher = $(this).parent().attr("id").split("_",2)[1];
        var class_scheduled = $(this).siblings(".classes-scheduled").html();
        $('div.teacher_scheds').html($('<div class="loading-style1"></div>'));
        $(this).litebox("div.teacher_scheds",{show:true});
        if(class_scheduled==0) {
            $('div.teacher_scheds').html('No class scheduled.');
        } else {
           $.post(base_url+"acad/class_schedule",{
                dir: 'teacher_class',
                tid: teacher, access: "read"
            },function(data){
                $('div.teacher_scheds').html(data);
            });
        }
    });
    var teachers_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(teachersList);
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            teachers_cnv.getTeachers("",$('div.dyn_content'),4);
        }
    });
    return new teachers_list;
});