define([
        'jquery',
	'underscore',
	'backbone',
        'baseURL',
	'text!tpl/task/task_view.html',
	'text!tpl/task/acad/lessonPlanCreate.html',
], function($, _, Backbone,base_url,taskView, lessonPlanCreate) {
    var lesson_plan_cnv = {
        teacher: "",
        class_sched: null,
        lesson_plan: null,
        lesson_plan_detail: null
    };
    //date range that the lesson plan covers
    $("body").on("click","#inclusive-date-from, #inclusive-date-to",function(){
        $(this).datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends });
        $(this).datepicker({showOn: 'focus'}).focus();
    });
    //select class to make lesson plan
    $("body").on("click","div#lp_class_list div.select_btn",function(){
        lesson_plan_cnv.class_sched = $(this).parent().parent().attr("id");
        $("div#loader").addClass("loading-style1").show();
        if($(this).hasClass("sprite-orange-square-right")) {
            lesson_plan_cnv.class_sched = null;
            $(this).removeClass("sprite-orange-square-right");
            $("div#lesson_plan_form").toggle("blind");
            $("div#talker").empty().html("Choose the class to create lesson plan for:");
            $("div#loader").removeClass("loading-style1").hide();
        } else {
            $("div#lp_class_list div.select_btn").removeClass("sprite-orange-square-right");
            $("div#talker").empty().html("Lesson plan for "+$(this).parent().siblings(".sched_time").html()+" class");
            $(this).addClass("sprite-orange-square-right");
            $("div#lesson_plan_form input[name$=class_teacher]").val(lesson_plan_cnv.teacher);
            $("div#lesson_plan_form input[name$=class_book]").val($(this).parent().siblings(".sched_book").html());
            $("div#lesson_plan_form input[name$=class_time]").val($(this).parent().siblings(".sched_time").html());
            $("div#lesson_plan_form input[name$=class_course]").val($(this).parent().siblings(".sched_course").html());
            $("div#lesson_plan_form input[name$=class_type]").val($(this).parent().siblings(".sched_type").html());
            $("div.lp_date_approved").empty();
            $("div#save_lp_frm select[name$=weekday]").val("").change();
            $("#inclusive-date-from").val(""); $("#inclusive-date-to").val("");
            $.post(base_url+"acad/lesson_plan",{
                dir: "retrieve_class_students_and_lp",
                cid: $(this).parent().parent().attr("id")
            },function(data){
                $("div.class_students_list").html(data.res);
                lesson_plan_cnv.lesson_plan = data.lp;
                if(data.lp != null) {
                    if(data.approved != 0 && typeof data.approved == "string") {
                        $("div.lp_date_approved").html("Checked: "+data.approved);
                    }
                    $('div#init_lesson_plan button[name$=create_lesson_plan]').hide();
                    $("#inclusive-date-from").val(data.idf);
                    $("#inclusive-date-to").val(data.idt);
                    $("div#lesson_plan_form").stop(true,true).effect("slide",function(){
                        $("div#save_lp_frm").stop(true,true).slideDown();
                    });
                } else {
                    $('div#init_lesson_plan button[name$=create_lesson_plan]').show();
                    $("div#save_lp_frm").hide();
                    $("div#lesson_plan_form").stop(true,true).effect("slide");
                }
                $("div#loader").removeClass("loading-style1").hide();
            },"json");
        }
    });
    //create lesson plan
    $("body").on("click","div#init_lesson_plan button[name$=create_lesson_plan]",function(){
        $("div#loader").addClass("loading-style1").show();
        if($("#inclusive-date-from").val()==="" || $("#inclusive-date-to").val()==="") {
            $(this).litebox($("div.err_msg").html("Inclusive dates required."),{show:true});
            $("div#loader").removeClass("loading-style1").hide();
        } else {
            $.post(base_url+"acad/lesson_plan",{
                dir: "add_lesson_plan",
                data: {
                    cid: lesson_plan_cnv.class_sched,
                    inc_date_from: $("#inclusive-date-from").val(),
                    inc_date_to: $("#inclusive-date-to").val()
                }
            },function(data){
                lesson_plan_cnv.lesson_plan = data.lp;
                $('div#init_lesson_plan button[name$=create_lesson_plan]').hide();
                $("#inclusive-date-from").val(data.idf);
                $("#inclusive-date-to").val(data.idt);
                $("div#save_lp_frm").stop(true,true).slideDown();
                $("div#loader").removeClass("loading-style1").hide();
            },"json");
        }
    });
    //assign day for lesson
    $("body").on("change","select[name$=weekday]",function(){
        $("div#loader").addClass("loading-style1").show();
        $.post(base_url+"acad/lesson_plan",{
            dir: "retrieve_lp_day_details",
            data: {
                lp: lesson_plan_cnv.lesson_plan,
                day: $("div#save_lp_frm select[name$=weekday]").val()
            }
        },function(data){
            lesson_plan_cnv.lesson_plan_detail = data.lpd;
            $("#save_lesson_plan textarea[name$=lp_objectives]").val(data.objectives);
            $("#save_lesson_plan textarea[name$=lp_topic]").val(data.topic);
            $("#save_lesson_plan textarea[name$=lp_class_starter]").val(data.class_starter);
            $("#save_lesson_plan textarea[name$=lp_introduction]").val(data.introduction);
            $("#save_lesson_plan textarea[name$=lp_lesson_proper]").val(data.lesson_proper);
            $("#save_lesson_plan textarea[name$=lp_guided_practice]").val(data.guided_practice);
            $("#save_lesson_plan textarea[name$=lp_independent_practice]").val(data.independent_practice);
            $("#save_lesson_plan textarea[name$=lp_homework]").val(data.homework);
            $("div#loader").removeClass("loading-style1").hide();
        },"json");
    });
    //save lesson plan
    $("body").on("submit","#save_lesson_plan",function(e){
        e.preventDefault();
        var direction = (lesson_plan_cnv.lesson_plan_detail!=null) ? "edit_lesson_plan_detail":"add_lesson_plan_detail";
        $("div#loader").addClass("loading-style1").show();
        if($("div#save_lp_frm select[name$=weekday]").val()=="" || $("#inclusive-date-from").val()==="" || $("#inclusive-date-to").val()==="") {
            $('div.err_msg').html("Please enter a value for required fields.");
            $(this).litebox("div.err_msg",{show:true});
            $("div#loader").removeClass("loading-style1").hide();
        } else {
            $.post(base_url+"acad/lesson_plan",{
                dir: direction,
                data: {
                    lp: lesson_plan_cnv.lesson_plan,
                    lpd: lesson_plan_cnv.lesson_plan_detail,
                    idf: $("#inclusive-date-from").val(),
                    idt: $("#inclusive-date-to").val(),
                    weekday: $("div#save_lp_frm select[name$=weekday]").val(),
                    objectives: $("#save_lesson_plan textarea[name$=lp_objectives]").val(),
                    topic: $("#save_lesson_plan textarea[name$=lp_topic]").val(),
                    class_starter: $("#save_lesson_plan textarea[name$=lp_class_starter]").val(),
                    introduction: $("#save_lesson_plan textarea[name$=lp_introduction]").val(),
                    lesson_proper: $("#save_lesson_plan textarea[name$=lp_lesson_proper]").val(),
                    guided_practice: $("#save_lesson_plan textarea[name$=lp_guided_practice]").val(),
                    independent_practice: $("#save_lesson_plan textarea[name$=lp_independent_practice]").val(),
                    homework: $("#save_lesson_plan textarea[name$=lp_homework]").val()
                }
            },function(data){
                lesson_plan_cnv.lesson_plan_detail = data.lpd;
                if(data.result == true && data.result_lp == true) {
                    $.customAlert("Lesson plan successfully saved.");
                } else {
                    $.customAlert("Lesson plan not saved. Try refreshing the page.");
                }
                $("div#loader").removeClass("loading-style1").hide();
            },"json");
        }
    });
    var lesson_plan = Backbone.View.extend({
        el: $('#container'),
        initialize: function() {
        },
        render: function() {
            $('.contents').html(lessonPlanCreate);
            $("div#lp_class_list").html($('<div class="loading-style1"></div>')).show();
            $.post(base_url+"acad/lesson_plan",{
                dir: "retrieve_class_list"
            },function(data){
                $("div#lp_class_list").html(data.res).show();
                lesson_plan_cnv.teacher = data.teacher;
            },"json");
        }
    });
    return new lesson_plan;
});