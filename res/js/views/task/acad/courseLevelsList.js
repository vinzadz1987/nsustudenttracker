define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/course_levels_list.html'
],function($,_,Backbone,base_url,courseLevelsList){
    var course_levels_cnv = {
        getCourseLevels: function(container) {
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/course_levels",{
                dir: "retrieve_course_levels"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        updateCourseLevel: function(update_info) {
            $.post(base_url+"acad/course_levels",{
                dir: "edit_course_level",
                data: {
                    clid: update_info.id.val(),
                    name: update_info.name.val()
                }
            },function(data){
                if(data.result===true) {
                    course_levels_cnv.getCourseLevels($('div.dyn_content'));
                    $.customAlert("Course Level Updated");
                }
            },"json");
        },
        rmCourseLevel: function(courselevel) {
            $.post(base_url+"acad/course_levels",{
                dir: "remove_course_level",
                clid: courselevel
            },function(data){
                if(data.result===true) {
                    course_levels_cnv.getCourseLevels($('div.dyn_content'));
                }
            },"json");
        }
    };
    $("body").ajaxError(function(e,jqxhr,settings,exception){
        if(settings.data.search("remove_course_level")>0 && jqxhr.status === 500) {
            $.customAlert('Unable to delete course level. Course Level is added to a class.<br />');
        }
    });
    //add course level
    $("body").on("submit","#addcourselevel",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .litebox("div.err_msg",{show:true});
            } else {
                $.post(base_url+"acad/course_levels",{
                    dir: "add_course_level",
                    data: {
                        "name": $("input[name$=course-level-name]").val()
                    }
                },function(data){
                    if(data.result===true) {
                        $("input[name$=srch_course_name]").val("");
                        course_levels_cnv.getCourseLevels($('div.dyn_content'));
                        $("#addcourselevel")[0].reset();
                    }
                },"json");
            }
        });
    });
    //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
            
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    //edit course level initialize form
    $("body").on("click","div.edit-course-level-btn",function(){
        var course_level = {
            id: $(this).parent().attr("id").split("_",2)[1],
            name: $(this).siblings('.course-level-name').html()
        };
        $("#edit-course-level input[name$=edit-id]").val(course_level.id);
        $("#edit-course-level input[name$=edit-course-level-name]").val(course_level.name);
        $(this).litebox("div.course-level-edit",{show:true});
    });
    //edit course level submit
    $("body").on("submit","#edit-course-level",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $.customeAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit-course-level input[name$=edit-id]"),
                    name: $("#edit-course-level input[name$=edit-course-level-name]")
                };
                course_levels_cnv.updateCourseLevel(update_info);
            }
        });
    });
    //delete course level
    $("body").on("click","div.delete-course-level-btn",function(){
        var clid = $(this).parent().attr("id").split("_",2)[1];
        $.customConfirm("Are you sure you want to delete this course level?",function(response){
            if(response===true) {
                course_levels_cnv.rmCourseLevel(clid);
            }
        });
    });
    var course_levels_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(courseLevelsList);
            course_levels_cnv.getCourseLevels($('div.dyn_content'));
        }
    });
    return new course_levels_list;
});