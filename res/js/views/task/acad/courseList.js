define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/courses_list.html'
],function($,_,Backbone,base_url,coursesList){
    var courses_cnv = {
        getCourses: function(param,container) {
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            $.post(base_url+"acad/courses",{
                dir: "retrieve_courses",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        updateCourse: function(update_info) {
            $.post(base_url+"acad/courses",{
                dir: "edit_course",
                data: {
                    cid: update_info.course_id.val(),
                    name: update_info.course_name.val(),
                    type: update_info.course_type.val()
                }
            },function(data){
                if(data.result===true) {
                    courses_cnv.getCourses($("div.search-box input[name$=srch_course_name]").val(),$('div.dyn_content'));
                    $.customAlert("Course Updated");
                }
            },"json");
        },
        rmCourse: function(course) {
            $.post(base_url+"acad/courses",{
                dir: "remove_course",
                cid: course
            },function(data){
                if(data.result===true) {
                    $("div.search-box input[name$=srch_course_name]").val("");
                    courses_cnv.getCourses("",$('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            },"json");
        }
    };
    $("body").ajaxError(function(e,jqxhr,settings,exception){
        if(settings.data.search("remove_course")>0 && jqxhr.status === 500) {
            $.customAlert('Unable to delete course. Course is used by a class.<br />');
        }
    });
    //add course
    $("body").on("submit","#addcourse",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .litebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    course_name: $("input[name$=course-name]").val(),
                    course_type: $("select[name$=course-type]").val()
                }
                $.post(base_url+"acad/courses",{
                    dir: "course_add",
                    data: {
                        "name": infos.course_name,
                        "type": infos.course_type
                    }
                },function(data){
                    if(data.result===true) {
                        $("input[name$=srch_course_name]").val("");
                        courses_cnv.getCourses($("input[name$=srch_course_name]").val(),$('div.dyn_content'));
                        $('#addcourse')[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                },"json");
            }
        });
    });
    //search
    $("body").on("keyup","div.search-box input[name$=srch_course_name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        courses_cnv.getCourses($(this).val(),$('div.dyn_content'));
    });
    //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
            like: $("input[name$=srch_course_name]").val()
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    //edit course initialize form
    $("body").on("click","div.edit-course-btn",function(){
        var course = {
            no: $(this).parent().attr("id").split("_",2)[1],
            name: $(this).siblings('.course-name').html(),
            type: $(this).siblings('.course-type').html()
        };
        $("#edit-course input[name$=edit-id]").val(course.no);
        $("#edit-course input[name$=edit-course-name]").val(course.name);
        $("#edit-course select[name$=edit-course-type]").val(course.type);
        $(this).litebox("div.course-edit",{show:true});
    });
    //edit course submit form
    $("body").on("submit","#edit-course",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    course_id: $("#edit-course input[name$=edit-id]"),
                    course_name: $("#edit-course input[name$=edit-course-name]"),
                    course_type: $("#edit-course select[name$=edit-course-type]")
                };
                courses_cnv.updateCourse(update_info);
            }
        });
    });
    //delete course
    $("body").on("click","div.delete-course-btn",function(){
        var cid = $(this).parent().attr("id").split("_",2)[1];
        $.customConfirm("Are you sure you want to delete this course?",function(response){
            if(response===true) {
                courses_cnv.rmCourse(cid);
            }
        });
    });
    var courses_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(coursesList);
            courses_cnv.getCourses("",$('div.dyn_content'));
        }
    });
    return new courses_list;
});