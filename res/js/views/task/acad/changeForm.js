define([
	'jquery',
	'underscore',
	'backbone',
        'baseURL',
	'text!tpl/task/task_view.html',
	'text!tpl/task/acad/changeForm.html',
], function($, _, Backbone,base_url, taskView, changeForm) {
    //add change request
    $("body").on("submit","form#change_request_frm",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .litebox("div.err_msg",{show:true});
            }
            else if($("input[name$=request_type]:checked").length == 0) {
                $("div.err_msg").html("Please check atleast one request type.")
                .litebox("div.err_msg",{show:true});
            } else {
                var requests = [];
                $.each($("input[name$=request_type]:checked"),function(i,elem){
                    requests.push($(elem).val());
                });
                $.post(base_url+"acad/change_request",{
                    dir: "send_change_request",
                    data: {
                        student_name: $("form#change_request_frm input[name$=student_name]").val(),
                        student_id: $("form#change_request_frm input[name$=student_id]").val(),
                        request_set: requests,
                        request_detail: $("form#change_request_frm textarea[name$=request_detail]").val(),
                        others_specify: $("form#change_request_frm input[name$=others_specify]").val()
                    }
                },function(data){
                    if(data.result===true) {
                        $.customAlert("Your request has been sent.");
                        $("form#change_request_frm")[0].reset();
                    } else {
                        $.customAlert("An error occurred. Please refresh this page and try again.");
                    }
                },"json");            
            }
        });
    });
    //toggle others specify
    $("body").on("click","form#change_request_frm input[name$=request_type]",function(){
        if($(this).val()=="others") {
            $("form#change_request_frm input[name$=others_specify]").val("");
            if($(this).is(":checked")) {
                $("form#change_request_frm input[name$=others_specify]").parent().show();
            } else {
                $("form#change_request_frm input[name$=others_specify]").parent().hide();
            }
        }
    });
    var action_form = Backbone.View.extend({
            el: $('#container'),
            initialize: function() {
            },
            render: function() {
                    $('.contents').html(changeForm);
            }
    });
    return new action_form;
});