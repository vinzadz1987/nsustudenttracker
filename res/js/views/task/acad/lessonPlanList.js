define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/acad/lesson_plan_list.html'
],function($,_,Backbone,base_url,lessonPlanList){
    var lp_list_cnv = {
        lesson_plan: null,
        retrieve_lp_list: function(container,param) {
            $.post(base_url+"acad/lesson_plan_list",{
                dir: "retrieve_lesson_plans",
                data: param
            },function(data){
                container.html(data.res).append(data.pagination);
            },"json");
        },
        lp_approval: function(approval) {
            $.post(base_url+"acad/lesson_plan_list",{
                dir: "update_lp_approval",
                data: {
                    apprv: approval,
                    lpid: this.lesson_plan
                }
            },function(data){
//                console.log(data);
            },"json");
        }
    };
//    $("body").ajaxError(function(e,jqxhr,settings,exception){
//        console.log(exception);
//    });
    //initialize date picker
    $("body").on("click","input[name$=date_param_from], input[name$=date_param_to]",function(){
        $(this).datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends });
        $(this).datepicker({showOn: 'focus'}).focus();
    });
    //hover for each lesson plan row
    $("body").on("hover","div.lesson_plans_container div.lp_row",function(e){
        if(e.type=="mouseenter") {
            var elem = $(this);
            $("div.lp_row").children("div.view-lesson-plan-btn").remove();
            elem.append($('<div class="sprite-inspect view-lesson-plan-btn to-right"></div>')
            .css({"margin-top":"-20px"}));
        }
        else if(e.type=="mouseleave") {
            $("div.view-lesson-plan-btn").stop(true,true).remove();
        }
    });
    //view lesson plan opens lesson plan viewport
    $("body").on("click","div.view-lesson-plan-btn",function(){
        var elem = $(this);
        var mark_approved = $('<div class="to-right clr lp_date_approved"><input type="checkbox" name="mark_lp_approved"/><label>Approve</label></div>');
        lp_list_cnv.lesson_plan = $(this).parent().attr("id").split("_",2)[1];
        $("div.lp_viewport").html($('<div class="loading-style1"></div>'));
        $.post(base_url+"acad/lesson_plan_list",{
            dir: "retrieve_lesson_plan_details",
            lpid: lp_list_cnv.lesson_plan
        },function(data){
            $("div.lp_viewport").empty().html(data.res).append(mark_approved);
            if(data.approved != 0 && typeof data.approved == "string") {
                $("div.lp_date_approved input[name$=mark_lp_approved]").attr("checked","checked");
                $("div.lp_date_approved label").html(data.approved);
            } else {
                $("div.lp_date_approved input[name$=mark_lp_approved]").removeAttr("checked");
                $("div.lp_date_approved label").html("Approve");
            }
            elem.litebox("div.lp_viewport",{show:true});
        },"json");
    });
    //approve lesson plan
    $("body").on("change","input[name$=mark_lp_approved]",function(){
        if($(this).attr("checked")=="checked") {
            lp_list_cnv.lp_approval(true);
        } else {
            lp_list_cnv.lp_approval(false);
        }
        $(this).litebox('div.lp_viewport',{close:true});
    });
/*search mod*/
    $("body").on("change","input[name$=date_param_from]",function(){
        if($(this).val()!="" && $("input[name$=date_param_to]").val()!="" && $("select[name$=category_date]").val()!="") {
            var params = {
                date_from : $("input[name$=date_param_from]").val(),
                date_to : $("input[name$=date_param_to]").val(),
                category : $("select[name$=category_date]").val()
            }
            lp_list_cnv.retrieve_lp_list($('div.dyn_content'),params);
        }
    });
    $("body").on("change","input[name$=date_param_to]",function(){
        if($(this).val()!="" && $("input[name$=date_param_from]").val()!="" && $("select[name$=category_date]").val()!="") {
            var params = {
                date_from : $("input[name$=date_param_from]").val(),
                date_to : $("input[name$=date_param_to]").val(),
                category : $("select[name$=category_date]").val()
            }
            lp_list_cnv.retrieve_lp_list($('div.dyn_content'),params);
        }
    });
    $("body").on("change","select[name$=category_date]",function(){
        if($(this).val()!="" && $("input[name$=date_param_from]").val()!="" && $("input[name$=date_param_to]").val()!="") {
            var params = {
                date_from : $("input[name$=date_param_from]").val(),
                date_to : $("input[name$=date_param_to]").val(),
                category : $("select[name$=category_date]").val()
            }
            lp_list_cnv.retrieve_lp_list($('div.dyn_content'),params);
        }
    });
/*end of search mod*/
    var lesson_plan_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('.contents').html(lessonPlanList);
            $('div.dyn_content').html($('<div class="loading-style1"></div>'));
            lp_list_cnv.retrieve_lp_list($('div.dyn_content'));
        }
    });
    return new lesson_plan_list;
});