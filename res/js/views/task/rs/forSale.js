define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'jQ-plugins/timepicker',
    'jQ-plugins/jquery.numeric',
    'text!tpl/task/rs/forSale.html'
],function($,_,Backbone,base_url, timepicker, numeric, stocksList){
    var allreal_cnv = {
         getAdvertiser: function(param, container) {
            $.post(base_url+"rs/forSale",{
                dir: "all_advertiser",
		like: param
            },function(data){
                container.html(data.res_advertiser).append($('<div class="pagination to-left"></div>').html(data.pagination));
            },"json");
         },
	getAllRealAllforSale: function(container, params) {
            $.post(base_url+"rs/forSale",{
		dir:"all_realAllrealforSale",
		data: params
		}, function(data){
                    container.html(data.res).append($('<div class="pagination to-left"></div>').html(data.pagination));
		},"json");
	},
        updateRealContentsforSale: function(update_contentsallreal) {
            $.post(base_url+"rs/forSale",{
                dir: "update_contentsAllreal",
                data: {
                    real_id:    update_contentsallreal.real_id.val(),
                    title:      update_contentsallreal.title.val(),
                    location:   update_contentsallreal.location.val(),
                    price:      update_contentsallreal.price.val(),
                    date:       update_contentsallreal.date.val(),
                    time:       update_contentsallreal.time.val(),
                    desc:       update_contentsallreal.desc.val(),
                    bedrms:     update_contentsallreal.bedrms.val(),
                    cat:        update_contentsallreal.cat.val(),
                    stype:      update_contentsallreal.stype.val(),
                    prop:       update_contentsallreal.prop.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {
                    allreal_cnv.getAllRealAllforSale($('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
    };

    $("body").on("click","a.title_real_Allreal", function(){
        var ti_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/forSale", {
            dir: "get_details",
            bid: ti_no
        }, function(data) {
	    $("label.infoId").val(data.real.real_id).html(data.real.real_id);
            $("#advertPhoto").html(data.res_advertiser);
           $("div.realInfo_Allreal").show();
             $('div.upload_Real_Photos').hide();
             $("div.under").show();
        },"json");
    });
	//initialise upload photo
	 $("body").on("click","button.UploadRealPhotos", function(){
		var urp_no = $(this).attr("id").split("_",2)[1];
		$.post(base_url+"rs/forSale", {
			dir: "get_details",
			urp: urp_no
		}, function(data){
			$("div#upload-Real-Photos input[name$=real_id]").val(data.realURP.real_id).data('original', data.realURP.real_id);
			$("div#upload-Real-Photos input[name$=added_by]").val(data.realURP.added_by).data('original', data.realURP.added_by);
			$("div.upload_Real_Photos").show();
                        $('div.update_Contents').fadeOut();
		},"json");
	 });

   //initialize update contents
     $("body").on("mouseover","button.updateAllreal", function(){
         var upc_no = $(this).attr("id").split("_",2)[1];
         $.post(base_url+"rs/forSale", {
             dir: "get_details",
             upc: upc_no
         }, function(data) {
             $("div#contents-Update input[name$=real_id]").val(data.realContents.real_id).data('original', data.realContents.real_id);
             $("div#contents-Update select[name$=edit-location]").val(data.realContents.loc_id).data('original',data.realContents.loc_id);
             $("div#contents-Update select[name$=edit-realhold]").val(data.realContents.real_hold_id).data('original', data.realContents.real_hold_id);
             $("div#contents-Update select[name$=edit-stype]").val(data.realContents.cat_id).data('original', data.realContents.cat_id);
             $("div#contents-Update select[name$=edit-prop]").val(data.realContents.property).data('original', data.realContents.property);
             $("div#contents-Update input[name$=added_by]").val(data.realContents.added_by).data('original', data.realContents.added_by);
             $("div#contents-Update input[name$=edit-title]").val(data.realContents.title).data('original', data.realContents.title);
             $("div#contents-Update input[name$=edit-price]").val(data.realContents.price).data('original', data.realContents.price);
             $("div#contents-Update input[name$=edit-date]").val(data.realContents.date).data('original', data.realContents.date);
             $("div#contents-Update input[name$=edit-time]").val(data.realContents.time).data('original', data.realContents.time);
             $("div#contents-Update textarea[name$=edit-desc]").val(data.realContents.description).data('original', data.realContents.description);
             $("div#contents-Update input[name$=edit-bedrooms]").val(data.realContents.bedrooms).data('original', data.realContents.bedrooms);
             $("div.update_ContentsforSale").show();
             $('div.upload_Real_Photos').hide();
         },"json");
     });

    

    //edit contents submit form
    $("body").on("submit","#frm-update-contentsforSale",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_contentsallreal = {
                    real_id:        $("#contents-Update input[name$=real_id]"),
                    title:          $("#frm-update-contentsforSale input[name$=edit-title]"),
                    location:       $("#frm-update-contentsforSale select[name$=edit-location]"),
                    price:          $("#frm-update-contentsforSale input[name$=edit-price]"),
                    date:           $("#frm-update-contentsforSale input[name$=edit-date]"),
                    time:           $("#frm-update-contentsforSale input[name$=edit-time]"),
                    desc:           $("#frm-update-contentsforSale textarea[name$=edit-desc]"),
                    bedrms:         $("#frm-update-contentsforSale input[name$=edit-bedrooms]"),
                    cat:            $("#frm-update-contentsforSale select[name$=edit-realhold]"),
                    stype:          $("#frm-update-contentsforSale select[name$=edit-stype]"),
                    prop:           $("#frm-update-contentsforSale select[name$=edit-prop]")
                };

                allreal_cnv.updateRealContentsforSale(update_contentsallreal);
                $.customAlert("Succesfully Updated");
                $('div.realInfo_Allreal').hide();
                $('div.update_ContentsforSale').hide();
                $("div.under").hide();

            }
        });
    });

     //delete my advert
     $("body").on("click","button.deleteAdvert",function(){
        var da_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/forSale",{
            dir: "delete_advert",
            pid: da_no
        },function(data){
            $('div.dyn_content2').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });

     //initialize date picker
    $("body").on("click","input[name$=edit-date]", function(){
       $(this).datepicker({dateFormat: 'MM dd, yy',changeYear: true, changeMonth: true, yearRange: '1940:+10'});
       $(this).datepicker({showOn: 'focus'}).focus();
     });

     //format numeric
     $("body").on("click",".formatdec", function(){
         $(this).numeric();
     });
     
     
      //format numeric
     $("body").on("mouseover","button.sendMessageInfoAllreal", function(){
         var upc_no = $(this).attr("id").split("_",2)[1];
         $.post(base_url+"rs/forSale", {
             dir: "get_details",
             sm: upc_no
         }, function(data) {
             $("label.whoka").html(data.sm);
             $("label.recname").html(data.rc);
             $("label.recemail").html(data.email);
             $("label.recntact").html(data.ctc);
             $("label.recntact2").html(data.ctc2);
             $("div#send-Message input[name$=real_id]").val(data.sendMessage.real_id).data('original', data.sendMessage.real_id);
             $("div.send_MessageContentforSale").show();
             $('div.upload_Real_Photos').hide();
         },"json");
     });

//send Message
    $("body").on("submit","#frm-send-messageforSale",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $.customAlert("A required field is empty.");
            } else {
                var infos = {

                    real_id: 		$("#send-Message input[name$=real_id]").val(),
                    sendby: 		$("#frm-send-messageforSale input[name$=sendby]").val(),
                    sendto: 		$("#frm-send-messageforSale input[name$=sendto]").val(),
                    mobileno: 		$("#frm-send-messageforSale input[name$=rmobno]").val(),
                    telno: 		$("#frm-send-messageforSale input[name$=rtelno]").val(),
                    text: 		$("#frm-send-messageforSale textarea[name$=yourmess]").val()

                }
                $.post(base_url+"rs/forSale",{
                    dir: "sendmessage",
                    data: {

                    "real_id": 	infos.real_id,
                    "sendby":   infos.sendby,
                    "sendto":   infos.sendto,
                    "mobno":    infos.mobileno,
                    "telno":    infos.telno,
                    "text":     infos.text

                    }
                },function(data){
                    if(data==="added") {
                        $("#frm-send-messageforSale")[0].reset();
                        $.customAlert("Message sent successfullly.");
                    } else {
                         $.customAlert("A required field is empty.");
                    }
                });
            }
        });
    });

    /* $("body").on("click", "input[name$=edit-time]", function(){
         $(this).timepicker({ampm: true});
     });*/

     //close update pyd
     $("body").on("click","div.closeAdvertContent",function(){
             $('div.realInfo_Allreal').slideUp();
             $('div.upload_Real_Photos').hide();
             $('div.update_ContentsforSale').hide();
             $('div.send_MessageContentforSale').hide();
             $("div.under").hide();
    });

    //under hide
    $("body").on("click","div.under", function(){
             $('div.realInfo_Allreal').slideUp();
             $('div.upload_Real_Photos').hide();
             $('div.update_ContentsAllreal').hide();
             $('div.send_MessageContentforSale').hide();
             $("div.under").hide();
    });

    $("body").on("click","button.closeURP", function(){
	     $('div.upload_Real_Photos').fadeOut();
             $('div.update_ContentsforSale').fadeOut();
    });

    $("body").on("mouseover","button.sendMessageNotlogSale", function(){
        $.customAlert("Not Member? Create account.<br>Member Already? Login");
    });

    //update contents close
    $("body").on("click","button.closeUpdate", function(){
        $('div.update_ContentsforSale').fadeOut();
    });
    
    //update contents close
    $("body").on("click","button.closeSM", function(){
        $('div.send_MessageContentforSale').fadeOut();
    });

	 //close update pyd
     $("body").on("click","button.advance_search",function(){
             $('div.advanceSearch1').slideDown();
     });

	 //close update pyd
     $("body").on("click","div.close_adsearch",function(){
             $('div.advanceSearch1').slideUp();
     });

	  //view larger
     $("body").on("click","img.1",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideDown(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideUp(1000);
    });

	 //view larger
     $("body").on("click","img.2",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideDown(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideUp(1000);
    });

	  //view larger
     $("body").on("click","img.3",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideDown(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideUp(1000);
    });

	  //view larger
     $("body").on("click","img.4",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideDown(1000);
			 $('img.0-5').slideUp(1000);
    });

	 //view larger
     $("body").on("click","img.5",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideDown(1000);
    });


	//format dec input
	/*$("body").on("keydown",".formatdec", function(){
		$(this).numeric();
	});*/

	//display the title of state
	$("body").on("keyup","div.search-box1 input[name$=titleAllreal]",function(){
		$('div.dyn_content2').html('<div class="loading-style1"></div>');
		var params = {
			srch:	$(this).val(),
			loc:    $("select[name$=location1Allreal]").val(),
			cat:	$("input[name$=real_catAllreal]:checked").val(),
			prop:	$("select[name$=real_propertyAllreal]").val()
		};
		allreal_cnv.getAllRealAllforSale($('div.dyn_content2'),params);
	});
	//display location
	$("body").on("change","select[name$=location1Allreal]",function(){
		$('div.dyn_content2').html('<div class="loading-style1"></div>');
		var params = {
			srch:	$("div.search-box1 input[name$=titleAllreal]").val(),
			loc:	$(this).val(),
			cat: 	$("input[name$=real_catAllreal]:checked").val(),
			prop:	$("select[name$=real_propertyAllreal]").val()
		};
		allreal_cnv.getAllRealAllforSale($('div.dyn_content2'),params);
	 });
	 //display by category
	 $("body").on("change","input[name$=real_catAllreal]",function(){
		$('div.dyn_content2').html('<div class="loading-style1"></div>');
		var params = {
			cat:	$(this).val(),
			loc:	$("select[name$=location1Allreal]").val(),
			srch:	$("div.search-box1 input[name$=titleAllreal]").val(),
			prop:	$("select[name$=real_propertyAllreal]").val()
		};
		allreal_cnv.getAllRealAllforSale($('div.dyn_content2'),params);
	  });

	 //display by category
	 $("body").on("change","select[name$=real_propertyAllreal]",function(){
		$('div.dyn_content2').html('<div class="loading-style1"></div>');
		var params = {
			prop:	$(this).val(),
			loc:	$("select[name$=location1Allreal]").val(),
			srch:	$("div.search-box1 input[name$=titleAllreal]").val(),
			cat: 	$("input[name$=real_catAllreal]:checked").val()
		};
		allreal_cnv.getAllRealAllforSale($('div.dyn_content2'),params);
	  });

	 //search all date
    $("body").on("keyup","div.search1 input[name$=useradvert]",function(){
        $('div.advertisement').html($('<div class="loading-style1"></div>'));
        allreal_cnv.getAdvertiser($(this).val(),$('div.advertisement'));
    });


  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content2').html(rcv.res).append($('<div class="pagination to-left"></div>').html(rcv.pagination));
        });
    });

	//pagination
  /*  $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.advertisement').html(rcv.res_advertiser).append($('<div class="pagination to-left"></div>').html(rcv.pagination));
        });
    });
  */
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {

        },
        events: {

        },
        render: function() {

            $('div.contents').html(stocksList);
            allreal_cnv.getAllRealAllforSale($('div.dyn_content2'));
            allreal_cnv.getAdvertiser("",$('div.advertisement'));
			$.post(base_url+"rs/forSale", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=location],select[name$=edit-location]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.loc,function(i,val){
               $("select[name$=location], select[name$=edit-location]")
               .append($('<option></option>').val(val.loc_id).html(val.name));
               });

              $("select[name$=real_propertyAllreal], select[name$=edit-prop]").empty()
	      .append($('<option></option>').val("").html(""));
	      $.each(rcv.classif,function(i,val){
	      $("select[name$=real_propertyAllreal], select[name$=edit-prop]")
	      .append($("<option></option>").val(val.property_id).html(val.name))
	      });

               $("select[name$=edit-realhold]").empty()
	      .append($('<option></option>').val("").html(""));
	      $.each(rcv.realhold,function(i,val){
	      $("select[name$=edit-realhold]")
	      .append($("<option></option>").val(val.real_hold_id).html(val.name))
	      });

              $("select[name$=stype]").empty().append($('<option></option>').val("").html(""));
              $.each(rcv.seltype, function(i,val){
              $("select[name$=stype]").append($('<option></option>').val(val.cat_id).html(val.name));
              })

            });

             $('input[name$=edit-time]').timepicker({
		ampm: true
	     });
        }

    });
    return new stock_list;
}); 