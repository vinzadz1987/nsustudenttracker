define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'jQ-plugins/timepicker',
    'jQ-plugins/jquery.numeric',
    'text!tpl/task/rs/myadvert.html'
],function($,_,Backbone,base_url, timepicker, numeric, stocksList){
    var checklist_cnv = {
         getAdvertiser: function(param, container) {
            $.post(base_url+"rs/myadvert",{
                dir: "all_advertiser",
		like: param
            },function(data){
                container.html(data.res_advertiser).append($('<div class="pagination to-left"></div>').html(data.pagination));
            },"json");
         },
	getAllRealmyAll: function(container, params) {
            $.post(base_url+"rs/myadvert",{
		dir:"all_real",
		data: params
		}, function(data){
                    container.html(data.res).append($('<div class="pagination to-left"></div>').html(data.pagination));
		},"json");
	},
        updateRealContents: function(update_contents) {
            $.post(base_url+"rs/myadvert",{
                dir: "update_contents",
                data: {
                    real_id:    update_contents.real_id.val(),
                    title:      update_contents.title.val(),
                    location:   update_contents.location.val(),
                    price:      update_contents.price.val(),
                    date:       update_contents.date.val(),
                    time:       update_contents.time.val(),
                    desc:       update_contents.desc.val(),
                    bedrms:     update_contents.bedrms.val(),
                    cat:        update_contents.cat.val(),
                    stype:      update_contents.stype.val(),
                    prop:       update_contents.prop.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {
                    checklist_cnv.getAllRealmyAll($('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
    };



    $("body").on("click","a.title_real_myAdvert", function(){
        var ti_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/myadvert", {
            dir: "get_details",
            bid: ti_no
        }, function(data) {
	    $("label.infoId").val(data.real.real_id).html(data.real.real_id);
            $("#advertPhoto").html(data.res_advertiser);
            $("div.realInfo_myAdvert").show();
             $('div.upload_Real_Photos').hide();
             $("div.under").show();
        },"json");
    });
	//initialise upload photo
	 $("body").on("click","button.UploadRealPhotos", function(){
		var urp_no = $(this).attr("id").split("_",2)[1];
		$.post(base_url+"rs/myadvert", {
			dir: "get_details",
			urp: urp_no
		}, function(data){
			$("div#upload-Real-Photos input[name$=real_id]").val(data.realURP.real_id).data('original', data.realURP.real_id);
			$("div#upload-Real-Photos input[name$=added_by]").val(data.realURP.added_by).data('original', data.realURP.added_by);
			$("div.upload_Real_Photos").show();
                        $('div.update_Contents').fadeOut();
		},"json"); 
	 });
  
   //initialize update photo
     $("body").on("mouseover","button.updateContentsInfo", function(){
         var upc_no = $(this).attr("id").split("_",2)[1];
         $.post(base_url+"rs/myadvert", {
             dir: "get_details",
             upc: upc_no
         }, function(data) {
             $("div#contents-Update input[name$=real_id]").val(data.realContents.real_id).data('original', data.realContents.real_id);
             $("div#contents-Update select[name$=edit-location]").val(data.realContents.loc_id).data('original',data.realContents.loc_id);
             $("div#contents-Update select[name$=edit-realhold]").val(data.realContents.real_hold_id).data('original', data.realContents.real_hold_id);
             $("div#contents-Update select[name$=edit-stype]").val(data.realContents.cat_id).data('original', data.realContents.cat_id);
             $("div#contents-Update select[name$=edit-prop]").val(data.realContents.property).data('original', data.realContents.property);
             $("div#contents-Update input[name$=added_by]").val(data.realContents.added_by).data('original', data.realContents.added_by);
             $("div#contents-Update input[name$=edit-title]").val(data.realContents.title).data('original', data.realContents.title);
             $("div#contents-Update input[name$=edit-price]").val(data.realContents.price).data('original', data.realContents.price);
             $("div#contents-Update input[name$=edit-date]").val(data.realContents.date).data('original', data.realContents.date);
             $("div#contents-Update input[name$=edit-time]").val(data.realContents.time).data('original', data.realContents.time);
             $("div#contents-Update textarea[name$=edit-desc]").val(data.realContents.description).data('original', data.realContents.description);
             $("div#contents-Update input[name$=edit-bedrooms]").val(data.realContents.bedrooms).data('original', data.realContents.bedrooms);
             $("div.update_Contents").show();
             $('div.upload_Real_Photos').hide();
         },"json");
     });

    //edit contents submit form
    $("body").on("submit","#frm-update-contents",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_contents = {
                    real_id:        $("#contents-Update input[name$=real_id]"),
                    title:          $("#frm-update-contents input[name$=edit-title]"),
                    location:       $("#frm-update-contents select[name$=edit-location]"),
                    price:          $("#frm-update-contents input[name$=edit-price]"),
                    date:           $("#frm-update-contents input[name$=edit-date]"),
                    time:           $("#frm-update-contents input[name$=edit-time]"),
                    desc:           $("#frm-update-contents textarea[name$=edit-desc]"),
                    bedrms:         $("#frm-update-contents input[name$=edit-bedrooms]"),
                    cat:            $("#frm-update-contents select[name$=edit-realhold]"),
                    stype:          $("#frm-update-contents select[name$=edit-stype]"),
                    prop:           $("#frm-update-contents select[name$=edit-prop]")
                };
                
                checklist_cnv.updateRealContents(update_contents);
                $.customAlert("Succesfully Updated");
                $('div.realInfo_myAdvert').hide();
                $('div.update_Contents').hide();
                $("div.under").hide();
                  
            }
        });
    });

     //delete my advert
     $("body").on("click","button.deleteAdvert",function(){
        var da_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/myadvert",{
            dir: "delete_advert",
            pid: da_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });

     //initialize date picker
    $("body").on("click","input[name$=edit-date]", function(){
       $(this).datepicker({dateFormat: 'MM dd, yy',changeYear: true, changeMonth: true, yearRange: '1940:+10'});
       $(this).datepicker({showOn: 'focus'}).focus();
     });

     //format numeric
     $("body").on("click",".formatdec", function(){
         $(this).numeric();
     });

    /* $("body").on("click", "input[name$=edit-time]", function(){
         $(this).timepicker({ampm: true});
     });*/

     //close update pyd
     $("body").on("click","div.closeAdvertContent",function(){
             $('div.realInfo_myAdvert').slideUp();
             $('div.upload_Real_Photos').hide();
             $('div.update_Contents').hide();
             $("div.under").hide();
    });

    //close under
     $("body").on("click","div.under",function(){
             $('div.realInfo_myAdvert').slideUp();
             $('div.upload_Real_Photos').hide();
             $('div.update_Contents').hide();
             $("div.under").hide();
    });
	 
    $("body").on("click","button.closeURP", function(){
	     $('div.upload_Real_Photos').fadeOut();
             $('div.update_Contents').fadeOut();
    });
    
    //update contents close
    $("body").on("click","button.closeUpdate", function(){
        $('div.update_Contents').fadeOut();
    });
	 
	 //close update pyd
     $("body").on("click","button.advance_search",function(){
             $('div.advanceSearch1').slideDown();
     });
	 
	 //close update pyd
     $("body").on("click","div.close_adsearch",function(){
             $('div.advanceSearch1').slideUp();
     });
	 
	  //view larger
     $("body").on("click","img.1",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideDown(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideUp(1000);
    });
	 
	 //view larger
     $("body").on("click","img.2",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideDown(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideUp(1000);
    });
	 
	  //view larger
     $("body").on("click","img.3",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideDown(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideUp(1000);
    });
	 
	  //view larger
     $("body").on("click","img.4",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideDown(1000);
			 $('img.0-5').slideUp(1000);
    });
	 
	 //view larger
     $("body").on("click","img.5",function(){
             $('img.0').slideUp(1000);
			 $('img.0-1').slideUp(1000);
			 $('img.0-2').slideUp(1000);
			 $('img.0-3').slideUp(1000);
			 $('img.0-4').slideUp(1000);
			 $('img.0-5').slideDown(1000);
    });
	
	
	//format dec input
	/*$("body").on("keydown",".formatdec", function(){
		$(this).numeric();
	});*/
	
	//display the title of state
	$("body").on("keyup","div.search-box1 input[name$=title]",function(){
		$('div.dyn_content').html('<div class="loading-style1"></div>');
		var params = {
			srch:	$(this).val(),
			loc:    $("select[name$=location1]").val(),
			cat:	$("input[name$=real_cat]:checked").val(),
			prop:	$("select[name$=real_property]").val()
		};
		checklist_cnv.getAllRealmyAll($('div.dyn_content'),params);
	});
	//display location
	$("body").on("change","select[name$=location1]",function(){
		$('div.dyn_content').html('<div class="loading-style1"></div>');
		var params = {
			srch:	$("div.search-box1 input[name$=title]").val(),
			loc:	$(this).val(),
			cat: 	$("input[name$=real_cat]:checked").val(),
			prop:	$("select[name$=real_property]").val()
		};
		checklist_cnv.getAllRealmyAll($('div.dyn_content'),params);
	 });
	 //display by category
	 $("body").on("change","input[name$=real_cat]",function(){
		$('div.dyn_content').html('<div class="loading-style1"></div>');
		var params = {
			cat:	$(this).val(),
			loc:	$("select[name$=location1]").val(),
			srch:	$("div.search-box1 input[name$=title]").val(),
			prop:	$("select[name$=real_property]").val()
		};
		checklist_cnv.getAllRealmyAll($('div.dyn_content'),params);
	  });
	 
	 //display by category
	 $("body").on("change","select[name$=real_property]",function(){
		$('div.dyn_content').html('<div class="loading-style1"></div>');
		var params = {
			prop:	$(this).val(),
			loc:	$("select[name$=location1]").val(),
			srch:	$("div.search-box1 input[name$=title]").val(),
			cat: 	$("input[name$=real_cat]:checked").val()
		};
		checklist_cnv.getAllRealmyAll($('div.dyn_content'),params);
	  });
	
	 //search all date
    $("body").on("keyup","div.search1 input[name$=useradvert]",function(){
        $('div.advertisement').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getAdvertiser($(this).val(),$('div.advertisement'));
    });
   
    
  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-left"></div>').html(rcv.pagination));
        });
    });
	
	//pagination
  /*  $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.advertisement').html(rcv.res_advertiser).append($('<div class="pagination to-left"></div>').html(rcv.pagination));
        });
    });
  */
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            checklist_cnv.getAllRealmyAll($('div.dyn_content'));
            checklist_cnv.getAdvertiser("",$('div.advertisement'));
			$.post(base_url+"rs/myadvert", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=location],select[name$=edit-location]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.loc,function(i,val){
               $("select[name$=location], select[name$=edit-location]")
               .append($('<option></option>').val(val.loc_id).html(val.name));
               });
			   
              $("select[name$=real_property], select[name$=edit-prop]").empty()
	      .append($('<option></option>').val("").html(""));
	      $.each(rcv.classif,function(i,val){
	      $("select[name$=real_property], select[name$=edit-prop]")
	      .append($("<option></option>").val(val.property_id).html(val.name))
	      });

               $("select[name$=edit-realhold]").empty()
	      .append($('<option></option>').val("").html(""));
	      $.each(rcv.realhold,function(i,val){
	      $("select[name$=edit-realhold]")
	      .append($("<option></option>").val(val.real_hold_id).html(val.name))
	      });

              $("select[name$=stype]").empty().append($('<option></option>').val("").html(""));
              $.each(rcv.seltype, function(i,val){
              $("select[name$=stype]").append($('<option></option>').val(val.cat_id).html(val.name));
              })
         
            });

             $('input[name$=edit-time]').timepicker({
		ampm: true
	     });
        }

    });
    return new stock_list;
}); 