define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/rs/location.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getallitem: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"rs/location",{
                dir: "all_item",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
        
         updateitem: function(update_info) {
            $.post(base_url+"rs/location",{
                dir: "update_items",
                data: {
                    loc_id: update_info.loc_id.val(),
                    locname: update_info.locname.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallitem("",$('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
          
    };
    
    
    
    //format dec
    $("body").on("keydown",".formatdec",function(e){
       $.decimalFormat($(this),e,2); 
    });
    
    
    //delete items
     $("body").on("click","li.delete-items",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/location",{
            dir: "delete_item",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
   
   //toggle update checklist item
   $("body").on("click","div.item ul li", function(){
       $("div.hk-item").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","ch");
       }else {
           $("div.item ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Item":$("div#itemupdate").slideDown();
                   checklist_cnv.getallitem("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
   });
    //add item
    $("body").on("submit","#frm-hk-item",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("label.error").html("A required field is empty.").fadeIn();
                $("label.error").html("A required field is empty.").fadeOut();
            } else {
                var infos = {
                    loc: $("input[name$=locname]").val()
                }
                $.post(base_url+"rs/location",{
                    dir: "add_loc",
                    data: {
                        "loc": infos.loc
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=item-name]").val("");
                        checklist_cnv.getallitem("",$('div.dyn_content'));           
                        $("#frm-hk-item")[0].reset();
                         $('div#showsuccess').fadeIn(1200);
                         $('div#showsuccess').fadeOut(1200);
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    
   
    //initialize update items
    $("body").on("click","li.update-items",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/location",{
            dir: "get_item_details",
            bid: st_no 
        },function(data){   
            
            $('div.dyn_content').html(data.res); 
            $("#update-item input[name$=item-id]").val(data.item.loc_id).data('original',data.item.loc_id); 
            $("#update-item input[name$=edit-loc]").val(data.item.name).data('original',data.item.name); 
            
        },"json");
    });
    
    //edit checklist submit form
    $("body").on("submit","#update-item",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    
                    loc_id: $("#update-item input[name$=item-id]"),
                    locname: $("#update-item input[name$=edit-loc]")
                    
                };
                
              
               checklist_cnv.updateitem(update_info);
               checklist_cnv.getallitem($('div.dyn_content'));
                                          
               
            }
        });
    });
    
    //search all
    $("body").on("keyup","div.search-box1 input[name$=locname]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallitem($(this).val(),$('div.dyn_content'));
    });
    
    $("body").on("click","div.close", function(){
       $("div#itemupdate").hide(); 
    });
    
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
            checklist_cnv.getallitem("",$('div.dyn_content'));
            
            $.post(base_url+"rs/location", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=item-cat]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.category,function(i,val){
                   $("select[name$=item-cat]")
                   .append($('<option></option>').val(val.cat_id).html(val.cat_name));
               });
               
            });
        }
    });
    return new stock_list;
}); 