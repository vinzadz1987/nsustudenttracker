define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
	'jQ-plugins/jquery.numeric',
    'text!tpl/task/rs/joinUs.html'
],function($,_,Backbone,base_url,numeric, stocksList){
    var checklist_cnv = {          
    };
  
	//add item
    $("body").on("submit","#frm-add-advert",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var infos = {
                    
                    uname: 		$("input[name$=uname]").val(),
                    pw: 		$("input[name$=pw]").val(),
					fn:			$("input[name$=firstname]").val(),
					mn:			$("input[name$=middlename]").val(),
					ln:			$("input[name$=lastname]").val(),
					address:	$("input[name$=address]").val(),
					mob_no:		$("input[name$=mobileno]").val(),
					tel_no:		$("input[name$=telno]").val(),
					email:		$("input[name$=email]").val()
                    
                }
                $.post(base_url+"rs/joinUs",{
                    dir: "add_user",
                    data: {
                        
                    "uname": 	infos.uname,
                    "pw": 		infos.pw,
					"fname":	infos.fn,
					"mname":	infos.mn,
					"lname":	infos.ln,
					"address":	infos.address,
					"mob_no":	infos.mob_no,
					"tel_no":	infos.tel_no,
					"email":	infos.email
					
                    }
                },function(data){
                    if(data==="added") { 
                        $("#frm-add-advert")[0].reset();
                         $('div.successBox').fadeIn(1200);
                    } else {                       
                         $.customAlert("An error occured.");
                    }
                });
            }
        });
    });
    
	
  
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);            
        }
    });
    return new stock_list;
}); 