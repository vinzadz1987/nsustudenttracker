define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'jQ-plugins/timepicker',
    'jQ-plugins/jquery.numeric',
    'text!tpl/task/rs/messages.html'
],function($,_,Backbone,base_url, timepicker, numeric, stocksList){
    var checklist_cnv = {
         getAdvertiser: function(param, container) {
            $.post(base_url+"rs/messages",{
                dir: "all_advertiser",
		like: param
            },function(data){
                container.html(data.res_advertiser).append($('<div class="pagination to-left"></div>').html(data.pagination));
            },"json");
         },
	getAllRealmyAllmessage: function(param, container) {
            $.post(base_url+"rs/messages",{
		dir:"all_messages",
                like: param
		}, function(data){
                    container.html(data.res).append($('<div class="pagination to-left"></div>').html(data.pagination));
		},"json");
	},
        updateRealmessContents: function(update_contents) {
            $.post(base_url+"rs/messages",{
                dir: "update_contents",
                data: {
                    real_message_id:    update_contents.real_message_id.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {
                    checklist_cnv.getAllRealmyAllmessage("",$('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
    };



    $("body").on("click","div.showMessage", function(){
        var mess_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/messages", {
            dir: "get_details",
            mess: mess_no
        }, function(data) {
	    $("input[name$=mess_id]").val(data.mess.real_message_id);
            $("label.messto").val(data.mess.lastname).html(data.mess.lastname);
            $("label.yourtext").val(data.mess.text).html(data.mess.text);
            $("input[name$=sendby]").val(data.mess.sendto).html(data.mess.sendto);
            $("input[name$=sendto]").val(data.mess.sendby).html(data.mess.sendby);
            $("input[name$=real_id]").val(data.mess.real_id).html(data.mess.real_id);
            $("label.recntact").html(data.ctc);
            $("label.recntact2").html(data.ctc2);
            $('div.sendMessagehere').show();
            $('div.undermessage').show();
        },"json");
    });

    //send Message
    $("body").on("submit","#frm-send-reply",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.showfailed").slideDown(500);
                $("div.showfailed").slideUp(500);
            } else {
                var infos = {

                    real_id: 		$("#frm-send-reply input[name$=real_id]").val(),
                    sendby: 		$("#frm-send-reply input[name$=sendby]").val(),
                    sendto: 		$("#frm-send-reply input[name$=sendto]").val(),
                    mobileno: 		$("#frm-send-reply input[name$=rmobno]").val(),
                    telno: 		$("#frm-send-reply input[name$=rtelno]").val(),
                    text: 		$("textarea[name$=yourmess]").val()

                }
                $.post(base_url+"rs/messages",{
                    dir: "sendmessage",
                    data: {

                    "real_id": 	infos.real_id,
                    "sendby":   infos.sendby,
                    "sendto":   infos.sendto,
                    "mobno":    infos.mobileno,
                    "telno":    infos.telno,
                    "text":     infos.text

                    }
                },function(data){
                    if(data==="added") {
                        $("#frm-send-reply")[0].reset();
                        $("div.success").slideDown(500);
                        $("div.success").slideUp(500);
                    } else {
                         $("div.showfailed").slideDown(500);
                         $("div.showfailed").slideUp(500);
                    }
                });
            }
        });
    });

    //edit contents submit form
    $("body").on("click","button.closemessage",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_contents = {
                    real_message_id:        $("input[name$=mess_id]")
                };

                checklist_cnv.updateRealmessContents(update_contents);
                checklist_cnv.getAllRealmyAllmessage("",$('div.dyn_content'));
                $("div.undermessage").hide();$('div.sendMessagehere').hide();

            }
        });
    });

    //edit contents submit form
    $("body").on("click","div.undermessage",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_contents = {
                    real_message_id:        $("input[name$=mess_id]")
                };

                checklist_cnv.updateRealmessContents(update_contents);
                checklist_cnv.getAllRealmyAllmessage("",$('div.dyn_content'));
                $("div.undermessage").hide();$('div.sendMessagehere').hide();

            }
        });
    });

     //delete my advert
     $("body").on("click","div.deletemessages",function(){
        var delmess_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/messages",{
            dir: "delete_messages",
            pid: delmess_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });

//search all date
    $("body").on("keyup","div.search1 input[name$=useradvert]",function(){
        $('div.advertisement').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getAdvertiser($(this).val(),$('div.advertisement'));
    });

//search all date
    $("body").on("keyup","input[name$=sendername]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getAllRealmyAllmessage($(this).val(),$('div.dyn_content'));
    });

  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-left"></div>').html(rcv.pagination));
        });
    });

	//pagination
  /*  $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.advertisement').html(rcv.res_advertiser).append($('<div class="pagination to-left"></div>').html(rcv.pagination));
        });
    });
  */
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {

        },
        events: {

        },
        render: function() {

            $('div.contents').html(stocksList);
            checklist_cnv.getAllRealmyAllmessage("",$('div.dyn_content'));
            checklist_cnv.getAdvertiser("",$('div.advertisement'));
			$.post(base_url+"rs/messages", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=location],select[name$=edit-location]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.loc,function(i,val){
               $("select[name$=location], select[name$=edit-location]")
               .append($('<option></option>').val(val.loc_id).html(val.name));
               });

              $("select[name$=real_property], select[name$=edit-prop]").empty()
	      .append($('<option></option>').val("").html(""));
	      $.each(rcv.classif,function(i,val){
	      $("select[name$=real_property], select[name$=edit-prop]")
	      .append($("<option></option>").val(val.property_id).html(val.name))
	      });

               $("select[name$=edit-realhold]").empty()
	      .append($('<option></option>').val("").html(""));
	      $.each(rcv.realhold,function(i,val){
	      $("select[name$=edit-realhold]")
	      .append($("<option></option>").val(val.real_hold_id).html(val.name))
	      });

              $("select[name$=stype]").empty().append($('<option></option>').val("").html(""));
              $.each(rcv.seltype, function(i,val){
              $("select[name$=stype]").append($('<option></option>').val(val.cat_id).html(val.name));
              })

            });

             $('input[name$=edit-time]').timepicker({
		ampm: true
	     });
        }

    });
    return new stock_list;
}); 