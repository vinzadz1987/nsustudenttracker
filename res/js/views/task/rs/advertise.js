define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'jQ-plugins/jquery.numeric',
    'text!tpl/task/rs/advertise.html'
],function($,_,Backbone,base_url,numeric, stocksList){
    var checklist_cnv = {};
  
	//add adverts
    $("body").on("submit","#frm-add-advertise",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var infos = {
                    
                    property: 		$("select[name$=property]").val(),
                    loc: 		$("select[name$=location]").val(),
		    real_hold:		$("input[name$=realHold]").val(),
                    title:		$("input[name$=title]").val(),
		    desc:		$("textarea[name$=desc]").val(),
                    price:		$("input[name$=price]").val(),
                    stype:		$("input[name$=sellerType]").val(),
                    bdrooms:		$("input[name$=bedrooms]").val()
                    
                }
                $.post(base_url+"rs/advertise",{
                    dir: "advertise",
                    data: {
                        
                    "property": 	infos.property,
                    "loc": 		infos.loc,
					"real_hold":	infos.real_hold,
					"title":		infos.title,
					"desc":			infos.desc,
					"price":		infos.price,
					"stype":		infos.stype,
					"bedrooms":		infos.bdrooms
					
                    }
                },function(data){
                    if(data==="added") { 
                        $("#frm-add-advertise")[0].reset();
                        $('div.successBox').fadeIn(1200);
						Backbone.history.navigate("#/task/myadvert");
                    } else {                       
                         $.customAlert("An error occured.");
                    }
                });
            }
        });
    });
    
	//initialize uploadphotos
 /*   $("body").on("click","button.advert1",function(){
        var ai_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"rs/main_supplies",{
            dir: "get_tools_details",
            bid: st_no 
        },function(data){
            var rcv = $.parseJSON(data);
            $("#toolsupdate input[name$=tools-id]").val(rcv.tools.tools_id);
            $("#toolsupdate input[name$=edit-item-num]").val(rcv.tools.item_num).data('original', rcv.tools.item_num);
            $("#toolsupdate input[name$=edit-tools-description]").val(rcv.tools.description).data('original', rcv.tools.descritpion);
            $("#toolsupdate input[name$=edit-quantity]").val(rcv.tools.quantity).data('original', rcv.tools.quantity);
            $("#toolsupdate select[name$=edit-tools-unit]").val(rcv.tools.uom).data('original', rcv.tools.uom);
            $("#toolsupdate select[name$=edit-dept]").val(rcv.tools.department).data('original', rcv.tools.department);
            $(this).facebox("div.tools-update",{show:true});
        });
    });
  */
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList); 
			$.post(base_url+"rs/advertise", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
			   
               $("select[name$=property]").empty().append($('<option></option>').val("").html(""));
               $.each(rcv.cat,function(i,val){
                   $("select[name$=property]")
                   .append($('<option></option>').val(val.property_id).html(val.name));
               });
			   
			   $("select[name$=location]").empty().append($('<option></option>').val("").html(""));
			   $.each(rcv.loc, function(i,val) {
					$("select[name$=location]").append($('<option></option>').val(val.loc_id).html(val.name));
			   });
			   
			   $("select[name$=realHold]").empty().append($('<option></option>').val("").html(""));
			   $.each(rcv.type, function(i,val) {
					$("select[name$=realHold]").append($('<option></option>').val(val.real_hold_id).html(val.name));
			   });

            });
        }
    });
    return new stock_list;
}); 