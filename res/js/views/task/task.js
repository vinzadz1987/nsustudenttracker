define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/taskmenu.html'
], function($, _, Backbone,base_url, taskView,tasksList) {
    
        var stateCNV = {
        
            getSTATE: function(container) {
                $.post(base_url+"profile",{
                    dir: "get_state"
                }, function(data){
                    container.html(data.res);
                },'json');
            }
        };
    
	var task_view = Backbone.View.extend({
		el: $('#container'),
        content: $('div.content'),
        initialize: function() {
        	this.el.html(taskView);
        },
		render: function() {
			$.ajax({
			    url: base_url+"task_collector",
			    type: "POST",
			    success: function(data) {
			    	var rcv = $.parseJSON(data);
			        var param = {
			            tasks: rcv,
			            _: _
			        };
			        var porma_task = _.template(tasksList,param);
			        $('div#container').html(taskView);
			        $('div.profile-menu').html(porma_task);
			        $('#sub-menu').html("");
                                
                                        $.post(base_url+"profile",{
                                        dir: "get_id"
                                        }, function(data){ 
                                            $("#prof").attr("src",data.img);
                                        }, 'json');  
                                        
                                        $.post(base_url+"profile",{
                                        dir: "get_state"
                                        }, function(data){ 
                                            $("div.contents div.photosSlide").html(data.res).val(data.res);
                                            $("div.contents div.photosSlide2").html(data.res2).val(data.res2);
                                        }, 'json');
                                        
                                        $("body").on("mouseover","div.contents div.slideit", function(){
                                            
                                            stateCNV.getSTATE($('div.photosSlide'));
                                        });
                                    
                                    }
                                });
		}
	});
	return new task_view;
});