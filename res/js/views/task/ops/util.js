define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/main/util.html'
],function($,_,Backbone,base_url,stocksList){
    var instocks_cnv = {
         getallprs: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/util",{
                dir: "all_prs",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         getallsrs: function(param, container) {
             container.html($('<div class="loading-style1"></div>'));
             $.post(base_url+"ops/util",{
                 dir: "all_srs",
                 like: param
             }, function(data){
                 container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
             },"json");
         }
      
    };
   
    //toggle Type on <li> click
    $("body").on("click","div.rs ul li", function(){
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.rs ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "PRS":$("div.prs").slideDown();
                   $("div.srs").slideUp();
                   instocks_cnv.getallprs("",$('div.dyn_content'));break;
               case "SRS":$("div.srs").slideDown(); 
                   $("div.prs").slideUp();
                   instocks_cnv.getallsrs("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });
 
    //search all prs
    $("body").on("keyup","div.search-box1 input[name$=prs]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        instocks_cnv.getallprs($(this).val(),$('div.dyn_content'));
    });
    
    //search all srs
    $("body").on("keyup","div.search-box1 input[name$=srs]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        instocks_cnv.getallsrs($(this).val(),$('div.dyn_content'));
    });
  
  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
  
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            instocks_cnv.getallprs("",$('div.dyn_content'));
           
        }
    });
    return new stock_list;
}); 