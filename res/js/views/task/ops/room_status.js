define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/hk/room_status.html'
],function($,_,Backbone,base_url,stocksList){
    var stocks_cnv = {     
         getall_single_roomslist: function(param, container, room_status_id) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/room_status",{
                dir: "allsinglerooms",
                like: param, room_status_name: room_status_id
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         }, 
         getall_double_roomslist: function(param, container, room_status_id) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/room_status",{
                dir: "alldoublerooms",
                liked: param, room_status_named: room_status_id
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         getall_triple_roomslist: function(param, container, room_status_id) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/room_status",{
                dir: "alltriplerooms",
                liked: param, room_status_named: room_status_id
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         getall_quad_roomslist: function(param, container, room_status_id) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/room_status",{
                dir: "allquadrooms",
                like: param, room_status_name: room_status_id
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         getall_corporate_roomslist: function(param, container, room_status_id) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/room_status",{
                dir: "allcorporaterooms",
                like: param, room_status_name: room_status_id
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         }   
    };  
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.fbwk ul li", function(){
       $("div.room_status").slideUp();
       $("div.sw_contents").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fbwk ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Single":$("div#single").slideDown(); 
                   stocks_cnv.getall_single_roomslist("",$('div.dyn_content'));break;
               case "Double":$("div#double").slideDown(); 
                   stocks_cnv.getall_double_roomslist("",$('div.dyn_content'));break;
               case "Triple":$("div#triple").slideDown();
                   stocks_cnv.getall_triple_roomslist("",$('div.dyn_content'));break;
               case "Quad":$("div#quad").slideDown(); 
                   stocks_cnv.getall_quad_roomslist("",$('div.dyn_content'));break;
               case "Corporate":$("div#corporate").slideDown(); 
                   stocks_cnv.getall_corporate_roomslist("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });  
    //search single room number
    $("body").on("keyup","div.search-box1 input[name$=single_room_number]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_single_roomslist($(this).val(),$('div.dyn_content'),$("select[name$=single_room_status]").val());
    }); 
     //by- single room_status search
    $("body").on("change","div.search-box1 select[name$=single_room_status]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_single_roomslist($("div.search-box1 input[name$=single_room_number]").val(),$('div.dyn_content'),$(this).val());
    });
    //search double room number
    $("body").on("keyup","div.search-box1 input[name$=double_room_number]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_double_roomslist($(this).val(),$('div.dyn_content'),$("select[name$=double_room_status]").val());
    }); 
     //by-double room_status search
    $("body").on("change","div.search-box1 select[name$=double_room_status]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_double_roomslist($("div.search-box1 input[name$=double_room_number]").val(),$('div.dyn_content'),$(this).val());
    });
    //search triple room number
    $("body").on("keyup","div.search-box1 input[name$=triple_room_number]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_triple_roomslist($(this).val(),$('div.dyn_content'),$("select[name$=triple_room_status]").val());
    }); 
     //by-triple room_status search
    $("body").on("change","div.search-box1 select[name$=triple_room_status]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_triple_roomslist($("div.search-box1 input[name$=triple_room_number]").val(),$('div.dyn_content'),$(this).val());
    });
    //search quad room number
    $("body").on("keyup","div.search-box1 input[name$=quad_room_number]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_quad_roomslist($(this).val(),$('div.dyn_content'),$("select[name$=quad_room_status]").val());
    }); 
     //by- quad room_status search
    $("body").on("change","div.search-box1 select[name$=quad_room_status]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_quad_roomslist($("div.search-box1 input[name$=quad_room_number]").val(),$('div.dyn_content'),$(this).val());
    });
    //search corporate room number
    $("body").on("keyup","div.search-box1 input[name$=corporate_room_number]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_corporate_roomslist($(this).val(),$('div.dyn_content'),$("select[name$=corporate_room_status]").val());
    }); 
     //by- corporate room_status search
    $("body").on("change","div.search-box1 select[name$=corporate_room_status]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_corporate_roomslist($("div.search-box1 input[name$=corporate_room_number]").val(),$('div.dyn_content'),$(this).val());
    });
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
            stocks_cnv.getall_single_roomslist("",$('div.dyn_content'));
            $.post(base_url+"ops/room_status", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=room_status]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.room_status,function(i,val){
                   $("select[name$=room_status]")
                   .append($('<option></option>').val(val.room_status_id).html(val.room_status_name));
               });
               
            });
        }
    });
    return new stock_list;
}); 