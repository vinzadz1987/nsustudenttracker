define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/taskmenu.html'
], function($, _, Backbone,base_url, taskView,tasksList) {
	var task_view = Backbone.View.extend({
		el: $('#container'),
        content: $('div.content'),
        initialize: function() {
        	this.el.html(taskView);
        },
		render: function() {
			$.ajax({
			    url: base_url+"task_collector",
			    type: "POST",
			    success: function(data) {
			    	var rcv = $.parseJSON(data);
			        var param = {
			            tasks: rcv,
			            _: _
			        };
			        var porma_task = _.template(tasksList,param);
			        $('div#container').html(taskView);
			        $('div.profile-menu').html(porma_task);
			        $('#sub-menu').html("");
			    }
			});
		}
	});
	return new task_view;
});