define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/task/task_view.html',
    'text!tpl/task/ops/stocks/profile/background_view.html',
    'baseURL',
    'jsHelper'
], function($, _, Backbone,taskView, BackgroundView, baseURL, jsHelper) {
	/*
	var empInfo = Backbone.Model.extend({});
	
	var emp_info = new empInfo;
	emp_info.set({
		user_id: "1",
		pd_id_number: "10 - 102010",
		firstname: "Rombert",
		lastname: "Dakay",
		
		cd_country: "Philippines",
		cd_street: "Perrelos",
		cd_province: "Carcar",
		cd_email: "johndoe@gmail.com",
		cd_city: "Cebu",
		cd_zip_code: "6018",
	});
	
	var bgroundModel = Backbone.Model.extend({});
	var bgroundCollection = Backbone.Collection.extend({
		model: bgroundModel
	});
	var bgCollection = new bgroundCollection;*/
	var db_data = {};
	var employment = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {
			/*
			this.collection = bgCollection;
			
			this.collection = bgCollection.add({
				workexp: {
					workexp_id: "1",
					comp: "Cleverlearn English Language Institute",
					job_title: "Web Designer",
					date_range: "2011-04-01 - 2011-04-02"
				},
				education: {
					education_id: "1",
					school_name: "Cebu Institute of Technology",
					course: "Bachelor of Science in Information Technology",
					year: "2006 - 2010"
				},
				skills: {
					skills_id: "1",
					title: "Programming",
					year_exp: "1 year experience"
				},
				language: {
					language_id: "1",
					lang: "Tagalog",
					type: "Speaking",
					rate: "Good"
				},
				license: {
					license_id: "1",
					name: "Green Cross Organization",
					type: "Platinum",
					expiry_date: "2011-04-05 - 2011-04-20"
				}
			});
			this.collection = bgCollection.add({
				workexp: {
					workexp_id: "2",
					comp: "LexMark",
					job_title: "Junior Programmer",
					date_range: "2011-04-01 - 2011-04-02"
				},
				education: {
					education_id: "2",
					school_name: "Carcar Academy Technical School Inc.",
					course: "High School",
					year: "2002 - 2006"
				},
				skills: {
					skills_id: "2",
					title: "Web Designing",
					year_exp: "2 year experience"
				},
				license: {
					license_id: "2",
					name: "Couples for Christ",
					type: "Gold",
					expiry_date: "2011-04-05 - 2011-04-20"
				}
			});*/
			
			this.el.html(taskView);
		},
		events: {
			"click .employee-background-edit": "doEdit",
			"click .employee-background-save": "doSave"
		},
		doSave: function(evt) {
			var $element = $(evt.currentTarget).parent().parent();
			var $tag = $(evt.currentTarget).attr("rel");
			$tag = $tag.split("-");
			var $d_tab = $tag["0"];
			var $id = $tag['1'];

			var $post_data = {};
			$post_data["id"] = $id;
			$post_data["ajax_request"] = "true";
			$('input[type="text"], select', $element).each(function(index, element){
				$post_data[$(element).attr("name")] = $(element).val();
			});
			var $a_jax = {
				type: "POST",
				data: $post_data,
				url: baseURL+"profile/update/"+$d_tab,
				success: function() {
					Backbone.history.loadUrl();
				}
			};
			$.ajax($a_jax);
		},
		doEdit: function(evt) {
			var type = $(evt.currentTarget).attr("rel");
			$("."+type).html($("#"+type).html());
			$.each(db_data.background.workexp, function(index, key) {
				jsHelper.dateRange("workexpdate-"+key.workexp_id+"a", "workexpdate-"+key.workexp_id+"b");
			});
			$.each(db_data.background.education, function(index, key) {
				jsHelper.dateRange("educationdate-"+key.education_id+"a", "educationdate-"+key.education_id+"b");
			});
			$.each(db_data.background.license, function(index, key) {
				jsHelper.dateRange("licensedate-"+key.license_id+"a", "licensedate-"+key.license_id+"b");
			});
		},
		render: function() {
			$a_jax = {
				type: "POST",
				data: {ajax_request: "true"},
				url: baseURL+'profile/retrieve/background',
				success: function(d) {
					db_data = JSON.parse(d);
					db_data["_"] = _;
					var data = _.template(BackgroundView, db_data);
					$('.contents').html(data);
				}
			};
			$.ajax($a_jax);
		}
	});
	return new employment;
});