define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/main/supplies.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getalltools: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/main_supplies",{
                dir: "all_tools",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
        
         updatetools: function(update_info) {
            $.post(base_url+"ops/main_supplies",{
                dir: "update_tools",
                data: {
                    tools_id: update_info.tools_id.val(),
                    item_num: update_info.item_num.val(),
                    tools_desc: update_info.tools_desc.val(),
                    quant: update_info.quantity.val(),
                    unit: update_info.unit.val(),
                    dept: update_info.dept.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getalltools("",$('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
          
    };
    
     //format dec
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="item-num") {
            $.decimalFormat($(this),e,0);
        }else{
            $.decimalFormat($(this),e,0);
        }
    });
     
      
    //delete tools
     $("body").on("click","label.delete-tools",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/main_supplies",{
            dir: "delete_tools",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    
   
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.fb ul li", function(){
       $("div.form-main").slideUp();
       $("div.main-tools").slideUp();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fb ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Add Tools":$("div#hk-main-form").slideDown();
               
           }
           $(this).addClass('active');
       }
    });
 
    //add item
    $("body").on("submit","#frm-main-tools",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    
                    item_num: $("input[name$=item-num]").val(),
                    desc: $("input[name$=tools-desc]").val(),
                    quant: $("input[name$=quant]").val(),
                    uom: $("select[name$=uom]").val(),
                    deptar: $("select[name$=department]").val()
                    
                }
                $.post(base_url+"ops/main_supplies",{
                    dir: "add_maint_tools",
                    data: {
                        
                        "itemno": infos.item_num,
                        "desc": infos.desc,
                        "quant": infos.quant,
                        "uom": infos.uom,
                        "deptar": infos.deptar
                    }
                },function(data){
                    if(data==="added") { 
                        checklist_cnv.getalltools("",$('div.dyn_content'));
                        $("#frm-main-tools")[0].reset();
                         $('div#showsuccess').fadeIn(1200);
                         $('div#showsuccess').fadeOut(1200);
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    
   
    //initialize update items
    $("body").on("click","button.update-tools",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/main_supplies",{
            dir: "get_tools_details",
            bid: st_no 
        },function(data){
            var rcv = $.parseJSON(data);
            $("#toolsupdate input[name$=tools-id]").val(rcv.tools.tools_id);
            $("#toolsupdate input[name$=edit-item-num]").val(rcv.tools.item_num).data('original', rcv.tools.item_num);
            $("#toolsupdate input[name$=edit-tools-description]").val(rcv.tools.description).data('original', rcv.tools.descritpion);
            $("#toolsupdate input[name$=edit-quantity]").val(rcv.tools.quantity).data('original', rcv.tools.quantity);
            $("#toolsupdate select[name$=edit-tools-unit]").val(rcv.tools.uom).data('original', rcv.tools.uom);
            $("#toolsupdate select[name$=edit-dept]").val(rcv.tools.department).data('original', rcv.tools.department);
            $(this).facebox("div.tools-update",{show:true});
        });
    });
    
    //edit tools submit form
    $("body").on("submit","#update-tools",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    tools_id: $("#update-tools input[name$=tools-id]"),
                    item_num: $("#update-tools input[name$=edit-item-num]"),
                    tools_desc: $("#update-tools input[name$=edit-tools-description]"),
                    quantity: $("#update-tools input[name$=edit-quantity]"),
                    unit: $("#update-tools select[name$=edit-tools-unit]"),
                    dept: $("#update-tools select[name$=edit-dept]")
                };
                $.customConfirm("Sucessfully Edited. Click OK to continue.",function(response){
                    if(response===true) {
                      checklist_cnv.updatetools(update_info);
                    }
               }); 
            }
        });
    });
    
    //search all
    $("body").on("keyup","div.search-box1 input[name$=tools-name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getalltools($(this).val(),$('div.dyn_content'));
    });
     
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
             checklist_cnv.getalltools("",$('div.dyn_content'));
            
            $.post(base_url+"ops/main_supplies", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=department], select[name$=edit-dept]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.dept,function(i,val){
                   $("select[name$=department], select[name$=edit-dept]")
                   .append($('<option></option>').val(val.deptd_id).html(val.dept_name));
               });
               
                $("select[name$=uom], select[name$=edit-tools-unit]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.uom,function(i,val){
                   $("select[name$=uom], select[name$=edit-tools-unit]")
                   .append($('<option></option>').val(val.id).html(val.name));
               });
               
            });
        }
    });
    return new stock_list;
}); 