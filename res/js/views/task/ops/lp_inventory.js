define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/lp/lp_inventory.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getallinvdate: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/lp_inventory",{
                dir: "all_inv_date",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         
         getallpysinv: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/lp_inventory",{
                dir: "all_item"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
        
         updatepysinv: function(update_info) {
            $.post(base_url+"ops/lp_inventory",{
                dir: "update_pys_inv",
                data: {
                    lp_pys_id: update_info.lp_pys_id.val(),
                    sold_items: update_info.sold_items.val(),
                    total_cost: update_info.total_cost.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallpysinv($('div.item-all-show'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
    };
    
    //initialize date picker
    $("body").on("click","input[name$=inv-from], input[name$=inv-to]", function(){
       $(this).datepicker({dateFormat: 'M dd, yy',changeYear: true, changeMonth: true, yearRange: '1940:+10'});
       $(this).datepicker({showOn: 'focus'}).focus();
    });
    
  /*  //format dec
    $("body").on("keydown",".formatdec",function(e){
       $.decimalFormat($(this),e,2); 
    });
    */  
    
    //delete items
     $("body").on("click","li.delete-items",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_inventory",{
            dir: "delete_item",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    
   
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.stocks ul li", function(){
       $("div.form-lp-inv").slideUp();
       $("div.lp-inv").slideUp();
       $("div.lp-inv-update").slideUp();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.stocks ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Add Inventory Covered":$("div#lp-item-inv-form").slideDown(); 
                  
           }
           $(this).addClass('active');
       }
    });
  
 //toggle physical inv
   $("body").on("click","a.pi", function(){
       $("div.lp-inv").slideUp();
       if($(this).hasClass("active")) {
           $(this).attr("class","pic");
       }else {
           $("a.pi").removeClass('active');
           switch($(this).attr("title")) {
               case "Physical Inventory":$("div#show-pi").slideDown(5);
           }
           $(this).addClass('active');
       }
   });
   //toggle update checklist item
   $("body").on("click","a.updatepsyinv", function(){
       if($(this).hasClass("active")) {
           $(this).attr("class","ui");
       }else {
           $("a.updatepsyinv").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Psy Inv":$("div#psy-inv-update").slideDown();
                   $('div.updatepys').show(5);
                   
           }
           $(this).addClass('active');
       }
   });
    //add item
    $("body").on("submit","#frm-lp-inv",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Filled is Empty.").show(3000);
                $('div.rq').html("A Required Filled is Empty").hide(1000);  
       } else {
                var infos = {
                    inv_from: $("input[name$=inv-from]").val(),
                    inv_to: $("input[name$=inv-to]").val()
                }
                $.post(base_url+"ops/lp_inventory",{
                    dir: "add_lp_inv",
                    data: {
                        "inv_from": infos.inv_from,
                        "inv_to": infos.inv_to
                    }
                },function(data){
                    if(data==="added") {         
                         $("#frm-lp-inv")[0].reset();
                         $('div#showsuccess').fadeIn(1200);
                         $('div#showsuccess').fadeOut(1200);
                         checklist_cnv.getallinvdate("",$('div.dyn_content'));
                    } else {
                        $('div.rq').html("The inventory "+ infos.inv_to + " is already exist.").show(3000);
                        $('div.rq').html("The inventory "+ infos.inv_to + " is already exist.").hide(3000);
                        
                    }
                });
            }
        });
    });
    
     //show Physical inv
    $("body").on("click","a.pic", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_inventory",{
            dir: "all_item",
            bid: st_no
        }, function(data){
            $('div.item-all-show').html(data.res);
        },"json");
    });
   
   //delete items
     $("body").on("click","a.item-delete",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_inventory",{
            dir: "delete_items",
            pid: st_no
        },function(data){
             $('div.item-all-show').html(data.res);
        },"json");
    });
   
    //initialize update pys inv
    $("body").on("click","a.psy-inv-update",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_inventory",{
            dir: "get_pys_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#update-pys-inv input[name$=pys-id]").val(data.pys_inv.lp_pys_id).data('original',data.pys_inv.lp_pys_id);
            $("#update-pys-inv label.desc").val(data.pys_inv.description).html(data.pys_inv.description);
            $("#update-pys-inv input[name$=sold-items-edit]").val(data.pys_inv.sold_items).data('original',data.pys_inv.sold_items);
            $("#update-pys-inv input[name$=price-edit]").val(data.pys_inv.price).data('original',data.pys_inv.price);
            $("#update-pys-inv label.price").val(data.pys_inv.price).html(data.pys_inv.price);
            $("#update-pys-inv input[name$=total-cost-edit]").val(data.pys_inv.total_cost).data('original',data.pys_inv.total_cost);
            $("#update-pys-inv label.total_cost").val(data.pys_inv.total_cost).html(data.pys_inv.total_cost);
        },"json");
    });
   
   //auto-compute val price to total cost
    $("body").on("keyup","form#update-pys-inv",function(){
        
        var total_items = $("form#update-pys-inv input[name$=sold-items-edit]").val();
        var price = $("form#update-pys-inv input[name$=price-edit]").val();
        
        var total_cost = (price * total_items);
        
        $("form#update-pys-inv input[name$=total-cost-edit]").val(total_cost.toFixed(2)).html(total_cost.toFixed(2));
        $("form#update-pys-inv label.total_cost").val(total_cost.toFixed(2)).html(total_cost.toFixed(2));
        
    });
    
   
  /* //delete items
     $("body").on("click","label.close",function(){
        
             $('div.updateitem').fadeOut(500);
       
    });
    */
    //close inv
     $("body").on("click","label.close",function(){
        
             $('div.psy_inv').hide(1000);
             $('div.updatepys').hide(1000);
             checklist_cnv.getallinvdate("",$('div.dyn_content'));
       
    });
    
    //close update pyd
     $("body").on("click","button.cancel",function(){
        
             $('div.updatepys').hide(1000);
       
    });
    
    //search all date
    $("body").on("keyup","div.search-box1 input[name$=date]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallinvdate($(this).val(),$('div.dyn_content'));
    });
   
    //edit checklist submit form
    $("body").on("submit","#update-pys-inv",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
               $('div.rq').html("The item is updated succesfully.").show(3000);
               $('div.rq').html("The item is updated succesfully.").hide(3000);
            } else {
                var update_info = {
                    lp_pys_id: $("#update-pys-inv input[name$=pys-id]"),
                    sold_items: $("#update-pys-inv input[name$=sold-items-edit]"),
                    total_cost: $("#update-pys-inv input[name$=total-cost-edit]")
                };
                    checklist_cnv.updatepysinv(update_info);
                    $('div.rq').html("The inventory is updated succesfully.").fadeIn(3000);
                    $('div.rq').html("The inventory is updated succesfully.").slideUp(1000);
                    $('div.updatepys').hide();
                    checklist_cnv.getallpysinv($('div.item-all-show'));
                    $('div.empty').show(5000);
                    $('div.empty').hide(5000);      
               
            }   
        });
    });
  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
  
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            checklist_cnv.getallinvdate("",$('div.dyn_content'));
            
            $.post(base_url+"ops/lp_inventory", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=item-cat], select[name$=edit-item-cat]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.category,function(i,val){
                   $("select[name$=item-cat], select[name$=edit-item-cat]")
                   .append($('<option></option>').val(val.cat_id).html(val.name));
               });
               
            });
        }
    });
    return new stock_list;
}); 