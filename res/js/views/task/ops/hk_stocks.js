define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/hk/hk_stocks.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getallitem: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/hk_stocks",{
                dir: "all_item",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
        
         updateitem: function(update_info) {
            $.post(base_url+"ops/hk_stocks",{
                dir: "update_items",
                data: {
                    item_id: update_info.item_id.val(),
                    item_desc: update_info.item_desc.val(),
                    item_price: update_info.item_price.val(),
                    item_cat: update_info.item_cat.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallitem($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
          
    };
    
    
    
    //format dec
    $("body").on("keydown",".formatdec",function(e){
       $.decimalFormat($(this),e,2); 
    });
    
    
    //delete items
     $("body").on("click","li.delete-items",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_stocks",{
            dir: "delete_item",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    
   
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.fb ul li", function(){
       $("div.form-hk-item").slideUp();
       $("div.hk-item").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fb ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Add Item":$("div#hk-item-form").slideDown(); 
                   checklist_cnv.getallitem("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });
  
   //toggle update checklist item
   $("body").on("click","div.item ul li", function(){
       $("div.hk-item").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","ch");
       }else {
           $("div.item ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Item":$("div#itemupdate").slideDown();
                   checklist_cnv.getallitem("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
   });
    //add item
    $("body").on("submit","#frm-hk-item",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    desc: $("input[name$=item-desc]").val(),
                    price: $("input[name$=item-price]").val(),
                    cat: $("select[name$=item-cat]").val()
                }
                $.post(base_url+"ops/hk_stocks",{
                    dir: "add_hk_item",
                    data: {
                        "desc": infos.desc,
                        "price": infos.price,
                        "cat": infos.cat
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=item-name]").val("");
                        checklist_cnv.getallitem("",$('div.dyn_content'));           
                        $("#frm-hk-item")[0].reset();
                         $('div#showsuccess').fadeIn(1200);
                         $('div#showsuccess').fadeOut(1200);
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    
   
    //initialize update items
    $("body").on("click","li.update-items",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_stocks",{
            dir: "get_item_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#itemupdate label.item-name").html(data.item.item_desc).data('original',data.item.item_desc); 
            $("#update-item input[name$=item-id]").val(data.item.item_id).data('original',data.item.item_id); 
            $("#update-item input[name$=edit-item-desc]").val(data.item.item_desc).data('original',data.item.item_desc); 
            $("#update-item input[name$=edit-item-price]").val(data.item.price).data('original',data.item.price); 
            $("#update-item select[name$=edit-item-cat]").val(data.item.item_cat).data('original',data.item.item_cat); 
        },"json");
    });
    
    //edit checklist submit form
    $("body").on("submit","#update-item",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    item_id: $("#update-item input[name$=item-id]"),
                    item_desc: $("#update-item input[name$=edit-item-desc]"),
                    item_price: $("#update-item input[name$=edit-item-price]"),
                    item_cat: $("#update-item select[name$=edit-item-cat]")
                };
                
                if(update_info.item_desc.val() != true) {
                $.customConfirm("Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            checklist_cnv.updateitem(update_info);
                                             $("div.search-box input1[name$=item-name]").val("");
                                            checklist_cnv.getallitem("",$('div.dyn_content'));
                                            $('div#itemupdate').slideUp();   
                                             
                                        }
                                    });
                }
                else {
                    checklist_cnv.updateitem(update_info);
                }
               
            }
        });
    });
    
    //search all
    $("body").on("keyup","div.search-box1 input[name$=item-name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallitem($(this).val(),$('div.dyn_content'));
    });
     
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
            
            $.post(base_url+"ops/hk_stocks", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=item-cat]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.category,function(i,val){
                   $("select[name$=item-cat]")
                   .append($('<option></option>').val(val.cat_id).html(val.cat_name));
               });
               
            });
        }
    });
    return new stock_list;
}); 