define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/stocks.html'
],function($,_,Backbone,base_url,stocksList){
    var stocks_cnv = {
       getstockscount: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "count_stocks"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getfbstocklist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "fb_stocklist"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getfblist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "fb_stocklist",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getfbstockupdate: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "fb_retrieve_stocklist"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getosstocklist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "os_stocklist"
            },function(data){
               container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getoslist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "os_stocklist",
                like: param
            },function(data){
               container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getlpstocklist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "lp_stocklist"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));          
            },"json");
        },
        getlplist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "lp_stocklist",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));          
            },"json");
        },
        gethsstocklist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "hs_stocklist"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));          
            },"json");
        },
        gethslist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/stocks",{
                dir: "hs_stocklist",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));          
            },"json");
        }
    };
    //toggle stocks forms on <li> click
    $("body").on("click","div.stocks-tsk ul li", function(){
       $("div.stocks_form").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.stocks-tsk ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Stock Count":$("").slideDown();
                    stocks_cnv.getstockscount($('div.dyn_content'));break;
               case "Add Stock":$("div#stocks_in").slideDown();
                   stocks_cnv.getstockscount($(''));break;
               case "Stock List":$("div#stocks-list").slideDown();
                   stocks_cnv.getstockscount($(''));break;
                   
           }
           $(this).addClass('active');
       }
    });
    //toggle fb, os, lp and hs list <li> click
    $("body").on("click","div.STOCK-LIST ul li",function(){
        $("div.stocks-list").slideUp();
        $('div.dyn_content').empty();
        if($(this).hasClass("active")) {
            $(this).attr("class","white-litegray"); 
        } else {
            $("div.STOCK-LIST ul li").removeClass('active');
            switch ($(this).attr("title")) {
                case "Food and Beverage":$("div#searchfb").slideDown();
                    stocks_cnv.getfbstocklist($('div.dyn_content'));break;
                case "Office Supplies":$("div#searchos").slideDown();
                    stocks_cnv.getosstocklist($('div.dyn_content'));break;
                case "Larry's Place":$("div#searchlp").slideDown();
                    stocks_cnv.getlpstocklist($('div.dyn_content'));break;
                case "Hotel Supplies":$("div#searchhs").slideDown();
                    stocks_cnv.gethsstocklist($('div.dyn_content'));break;
            }
            $(this).addClass('active');
        }
    });
    //showfb stocks details list
    $("body").on("click","button.edit-stock-btn-bal",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/stocks",{
            dir: "fb_retrieve_stocklist",
            bid: st_no 
        },function(data){   
            $('div.stocks-detail').html(data.res);
            $(this).facebox("div.stocks-detail",{show:true});
        },"json");
    });
    //showfb stocks details list
    $("body").on("click","button.add",function(){
        $('div.updateform').show(1000);    
    });
     //handle unit of measure add submit form
    $("body").on("submit","form#additemcount", function(e){
      e.preventDefault();
      $(this).formvalidate(function(data){
          if(data.result === false) {
              $.customAlert("A required field is empty.");    
          } else {
              $.post(base_url+"ops/stocks",{
                 dir: "additemcount",
                 data: {
                     'ws_tracker_type' : $("form#additemcount input[name$=trackertype]").val(),
                     'ws_tracker_stock_id': $("form#additemcount input[name$=stock_id]").val(),
                     'ws_tracker_item_ctr': $("form#additemcount input[name$=itemnum]").val(),
                     'ws_tracker_date' : $("form#additemcount input[name$=date]").val(),
                     'ws_binder_item_bal' : $("form#additemcount input[name$=itembal]").val(),
                     'ws_recipient' : $("form#additemcount input[name$=supplier]").val()
                 }
              }, function(data){
                  if(data.res == true) {
                       $('div.stocks-detail').html(data.res);
                          $(this).facebox("div.stocks-detail",{show:true});
                        $("form#additemcount")[0].reset(); 
                  }else{
                      $.customAlert("An error occured. Please refresh this page and try again");
                  }
              }, "json");
          }
     });
    });
    //show os stocks details list
    $("body").on("click","div.stocks-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/stocks",{
            dir: "os_retrieve_stocklist",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('label.desc').html(rcv.osstocks.description);
            $('label.type').html(rcv.osstocks.type_name);
            $('label.unt').html(rcv.osstocks.unit_name);
            $('label.reorder').html(rcv.osstocks.reorder_point);
            $('label.stinhand').html(rcv.osstocks.qty_inhand);
            $(this).facebox("div.stocks-detail",{show:true});
        });
    });
    //show lp stocks details list
    $("body").on("click","div.stocks-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/stocks",{
            dir: "lp_retrieve_stocklist",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('label.desc').html(rcv.lpstocks.description);
            $('label.type').html(rcv.lpstocks.type_name);
            $('label.unt').html(rcv.lpstocks.unit_name);
            $('label.reorder').html(rcv.lpstocks.reorder_point);
            $('label.stinhand').html(rcv.lpstocks.qty_inhand);
            $(this).facebox("div.stocks-detail",{show:true});
        });
    });
    //show hs stocks details list
    $("body").on("click","div.stocks-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/stocks",{
            dir: "hs_retrieve_stocklist",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('label.desc').html(rcv.hsstocks.description);
            $('label.type').html(rcv.hsstocks.type_name);
            $('label.unt').html(rcv.hsstocks.unit_name);
            $('label.reorder').html(rcv.hsstocks.reorder_point);
            $('label.stinhand').html(rcv.hsstocks.qty_inhand);
            $(this).facebox("div.stocks-detail",{show:true});
        });
    });
    //search fb
    $("body").on("keyup","div.search-box2 input[name$=fbstock_descrip]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getfblist($(this).val(),$('div.dyn_content'));
    });
    //search os
    $("body").on("keyup","div.search-box2 input[name$=osstock_descrip]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getoslist($(this).val(),$('div.dyn_content'));
    });
    //search lp
    $("body").on("keyup","div.search-box2 input[name$=lpstock_descrip]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getlplist($(this).val(),$('div.dyn_content'));
    });
    //search hs
    $("body").on("keyup","div.search-box2 input[name$=hsstock_descrip]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.gethslist($(this).val(),$('div.dyn_content'));
    });
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(stocksList);
            
        }
    });
    return new stock_list;
}); 