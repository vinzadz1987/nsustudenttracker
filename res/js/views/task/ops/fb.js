define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/fb/main-view.html'
],function($,_,Backbone,base_url,stocksList){
    var stocks_cnv = {
         getall_inglist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "all"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getalllist: function(param, container, day) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "all",
                like: param, day:day
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getall_menulist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "allmenu"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getallmenulist: function(param, container,ml_time, ml_month) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "allmenu",
                like: param, mealtype:ml_time, month:ml_month
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getall_utensilslist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "allutensils"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getallutensilslist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "allutensils",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getall_deliverylist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "deliverysched"
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        getalldeliverylist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/fb",{
                dir: "deliverysched",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        },
        updateing: function(update_info) {
            $.post(base_url+"ops/fb",{
                dir: "update_ingred",
                data: {
                    id: update_info.id.val(),
                    name: update_info.name.val(),
                    desc: update_info.desc.val(),
                    day: update_info.day.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    stocks_cnv.getall_inglist($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        updatemenu: function(update_info) {
            $.post(base_url+"ops/fb",{
                dir: "update_menu",
                data: {
                    id: update_info.id.val(),
                    m_name: update_info.m_name.val(),
                    m_desc: update_info.m_desc.val(),
                    ml_time: update_info.ml_time.val(),
                    ml_month: update_info.ml_month.val(),
                    ml_week: update_info.ml_week.val()
                   
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    stocks_cnv.getall_menulist($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        updateutensils: function(update_info) {
            $.post(base_url+"ops/fb",{
                dir: "update_utensils",
                data: {
                    id: update_info.id.val(),
                    u_name: update_info.u_name.val(),
                    u_desc: update_info.u_desc.val(),
                    u_quantity: update_info.u_quantity.val()
            
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    stocks_cnv.getall_utensilslist($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        updatescheddelivery: function(update_info) {
            $.post(base_url+"ops/fb",{
                dir: "update_sched",
                data: {
                    id: update_info.id.val(),
                    item_name: update_info.item_name.val(),
                    supplier_name: update_info.supplier_name.val(),
                    delivery_date: update_info.delivery_date.val()
         
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    stocks_cnv.getall_deliverylist($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
    };
    
    //initialize date picker
    $("body").on("click","input[name$=date-deliver], input[name$=edit_date]", function(){
       $(this).datepicker({dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends});
       $(this).datepicker({showOn: 'focus'}).focus();
    });
    
    
    $("body").on("click","button.del-ing-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "delete_ingred",
            pid: st_no
        },function(data){
            $.customConfirm("Are you sure you want to Delete this ingredient? Click 'OK' to continue.",function(response){
                                        if(response===true) {
                                            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
                                        }
                                    });
            
        },"json");
    });
    $("body").on("click","button.del-menu-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "delete_menu",
            pid: st_no
        },function(data){
            $.customConfirm("Are you sure you want to Delete this menu? Click 'OK' to continue.",function(response){
                                        if(response===true) {
                                            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
                                        }
                                    });
            
        },"json");
    });
    $("body").on("click","button.del-utensils-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "delete_utensils",
            pid: st_no
        },function(data){
            $.customConfirm("Are you sure you want to Delete this menu? Click 'OK' to continue.",function(response){
                                        if(response===true) {
                                            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
                                        }
                                    });
            
        },"json");
    });
    $("body").on("click","button.del-sd-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "delete_sched",
            pid: st_no
        },function(data){
            $.customConfirm("Are you sure you want to Delete this Schedule of Delivery? Click 'OK' to continue.",function(response){
                                        if(response===true) {
                                            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
                                        }
                                    });
            
        },"json");
    });
    //toggle stocks forms on <li> click
    $("body").on("click","div.fb ul li", function(){
       $("div.ing_frm").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fb ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Stocks":$("div#ing").slideDown();
           }
           $(this).addClass('active');
       }
    });
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.fboption ul li", function(){
       $("div.week_menu").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fboption ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Ingredients":$("div#ingredient").slideDown(); 
                   stocks_cnv.getall_inglist($('div.dyn_content'));break;
               case "Weekly Menu":$("div#wk_menu").slideDown(); 
                   stocks_cnv.getall_menulist($('div.dyn_content'));break;
               case "Utensils":$("div#menu_utensils").slideDown();
                   stocks_cnv.getall_utensilslist($('div.dyn_content'));break;
               case "Deliveries":$("div#sched_deliver").slideDown(); 
                   stocks_cnv.getall_deliverylist($('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });
    //add ingredient
    $("body").on("submit","#frm_ing",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    name : $("input[name$=ingred-name]").val(),
                    desc: $("input[name$=ingred-desc]").val(),
                    day: $("select[name$=day]").val()
                }
                $.post(base_url+"ops/fb",{
                    dir: "add_ingredient",
                    data: {
                        "name": infos.name,
                        "desc": infos.desc,
                        "day" : infos.day
                    }
                },function(data){
                    if(data==="added") {
                       
                        $("div.search-box input2[name$=all_ing_search]").val("");
                        stocks_cnv.getalllist("",$('div.dyn_content'));          
                        $("#frm_ing")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    
    //add menu
    $("body").on("submit","#frm_menu",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var menu_info = {
                    m_name : $("input[name$=m_name]").val(),
                    m_desc: $("input[name$=m_desc]").val(),
                    ml_time: $("select[name$=ml_time]").val(),
                    ml_month: $("select[name$=ml_month]").val(),
                    ml_week: $("select[name$=ml_week]").val()
                }
                $.post(base_url+"ops/fb",{
                    dir: "add_menu",
                    data: {
                        "m_name": menu_info.m_name,
                        "m_desc": menu_info.m_desc,
                        "ml_time" : menu_info.ml_time,
                        "ml_month": menu_info.ml_month,
                        "ml_week" : menu_info.ml_week
                    }
                },function(data){
                    if(data==="added") {
                       
                       
                        stocks_cnv.getall_menulist($('div.dyn_content'));
                        
                        
                        $("#frm_menu")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    //add utensils
    $("body").on("submit","#frm_utensils",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var uten_info = {
                    uten_name: $("input[name$=uten_name]").val(),
                    u_desc: $("input[name$=u_desc]").val(),
                    quantity: $("input[name$=quantity]").val()              
                }
                $.post(base_url+"ops/fb",{
                    dir: "add_utensils",
                    data: {
                        "uten_name": uten_info.uten_name,
                        "u_desc": uten_info.u_desc,
                        "quantity": uten_info.quantity       
                    }
                },function(data){
                    if(data==="added") {
                       
                       
                        stocks_cnv.getall_utensilslist($('div.dyn_content'));
                        
                        
                        $("#frm_utensils")[0].reset();
                    } else {
                        $("div.err_msg").html("Quantity must not be value 0.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    //add deliver
    $("body").on("submit","#frm_deliver_sched",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var deliver_info = {
                    item : $("select[name$=item-name]").val(),
                    supplier: $("select[name$=item-supplier]").val(),
                    date_deliver: $("input[name$=date-deliver]").val()  
                }
                $.post(base_url+"ops/fb",{
                    dir: "add_deliver_sched",
                    data: {
                        "item": deliver_info.item,
                        "supplier": deliver_info.supplier,
                        "date_deliver" : deliver_info.date_deliver   
                    }
                },function(data){
                    if(data==="added") { 
                        stocks_cnv.getall_deliverylist($('div.dyn_content'));   
                        $("#frm_deliver_sched")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="quantity") {
            $.decimalFormat($(this),e,0);
        }else{
            $.decimalFormat($(this),e,0);
        }
    });
     
    //initialize edit ingredients
    $("body").on("click","button.edit-ing-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "retrieve_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit-ingred input[name$=edit-id]").val(rcv.ingredients.id);
            $("#edit-ingred input[name$=edit-ing-name]").val(rcv.ingredients.name).data('original',rcv.ingredients.name);         
            $("#edit-ingred input[name$=edit-ing-desc]").val(rcv.ingredients.desc).data('original',rcv.ingredients.desc);
            $("#edit-ingred select[name$=edit-ing-day]").val(rcv.ingredients.day).data('original',rcv.ingredients.day);
            $(this).facebox("div.ingredient-edit",{show:true});
        });
    });
    //initialize menu 
    $("body").on("click","button.edit-menu-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "retrieve_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit_frm_menu input[name$=edit-id]").val(rcv.menu.id);
            $("#edit_frm_menu input[name$=edit_m_name]").val(rcv.menu.m_name).data('original',rcv.menu.m_name);         
            $("#edit_frm_menu input[name$=edit_m_desc]").val(rcv.menu.m_desc).data('original',rcv.menu.m_desc);
            $("#edit_frm_menu select[name$=edit_ml_time]").val(rcv.menu.ml_time).data('original',rcv.menu.ml_time);
            $("#edit_frm_menu select[name$=edit_ml_month]").val(rcv.menu.ml_month).data('original',rcv.menu.ml_month);
            $("#edit_frm_menu select[name$=edit_ml_week]").val(rcv.menu.ml_week).data('original',rcv.menu.ml_week);
            $(this).facebox("div.menu-edit",{show:true});
        });
    });
    //initialize utensil
    $("body").on("click","button.edit-utensils-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "retrieve_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit_frm_utensils input[name$=edit-id]").val(rcv.utensils.id);
            $("#edit_frm_utensils input[name$=edit_uten_name]").val(rcv.utensils.u_name).data('original',rcv.utensils.u_name);         
            $("#edit_frm_utensils input[name$=edit_u_desc]").val(rcv.utensils.u_desc).data('original',rcv.utensils.u_desc);
            $("#edit_frm_utensils input[name$=edit-quantity]").val(rcv.utensils.quantity).data('original',rcv.utensils.quantity);            
            $(this).facebox("div.utensils-edit",{show:true});
        });
    });
    //initialize schedule deliveries
    $("body").on("click","button.edit-sd-btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/fb",{
            dir: "retrieve_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit_sd input[name$=edit-id]").val(rcv.sd.id);
            $("#edit_sd select[name$=edit_item]").val(rcv.sd.item_name).data('original',rcv.sd.item_name);         
            $("#edit_sd select[name$=edit_supplier]").val(rcv.sd.supplier).data('original',rcv.sd.supplier);
            $("#edit_sd input[name$=edit_date]").val(rcv.sd.date_delivered).data('original',rcv.sd.date_delivered);
            $(this).facebox("div.sd-edit",{show:true});
        });
    });
    //edit ingredients submit form
    $("body").on("submit","#edit-ingred",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit-ingred input[name$=edit-id]"),
                    name: $("#edit-ingred input[name$=edit-ing-name]"),
                    desc: $("#edit-ingred input[name$=edit-ing-desc]"),
                    day: $("#edit-ingred select[name$=edit-ing-day]")
                };
                if(update_info.name.val() != true) {
                $.customConfirm("Ingredient Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            stocks_cnv.updateing(update_info);
                                        }
                                    });
                }
                else {
                    stocks_cnv.updateing(update_info);
                }
            }
        });
    });
    //edit menu submit form
    $("body").on("submit","#edit_frm_menu",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit_frm_menu input[name$=edit-id]"),
                    m_name: $("#edit_frm_menu input[name$=edit_m_name]"),
                    m_desc: $("#edit_frm_menu input[name$=edit_m_desc]"),
                    ml_time: $("#edit_frm_menu select[name$=edit_ml_time]"),
                    ml_month: $("#edit_frm_menu select[name$=edit_ml_month]"),
                    ml_week: $("#edit_frm_menu select[name$=edit_ml_week]")  
                };
                if(update_info.m_name.val() != true) {
                $.customConfirm("Menu Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            stocks_cnv.updatemenu(update_info);
                                        }
                                    });
                }
                else {
                    stocks_cnv.updatemenu(update_info);
                }
            }
        });
    });
     //edit utensils submit form
    $("body").on("submit","#edit_frm_utensils",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit_frm_utensils input[name$=edit-id]"),
                    u_name: $("#edit_frm_utensils input[name$=edit_uten_name]"),
                    u_desc: $("#edit_frm_utensils input[name$=edit_u_desc]"),
                    u_quantity: $("#edit_frm_utensils input[name$=edit-quantity]")   
                };
                if(update_info.u_name.val() != true) {
                $.customConfirm("Utensil Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            stocks_cnv.updateutensils(update_info);
                                        }
                                    });
                }
                else {
                    stocks_cnv.updateutensils(update_info);
                }
            }
        });
    });
    //edit schedule deliveries submit form
    $("body").on("submit","#edit_sd",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit_sd input[name$=edit-id]"),
                    item_name: $("#edit_sd select[name$=edit_item]"),
                    supplier_name: $("#edit_sd select[name$=edit_supplier]"),
                    delivery_date: $("#edit_sd input[name$=edit_date]")
                    
                };
                if(update_info.item_name.val() != true) {
                $.customConfirm("Menu Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            stocks_cnv.updatescheddelivery(update_info);
                                        }
                                    });
                }
                else {
                    stocks_cnv.updatescheddelivery(update_info);
                }
            }
        });
    });
    //search utensils all
    $("body").on("keyup","div.search-box1 input[name$=all_utensils_name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getallutensilslist($(this).val(),$('div.dyn_content'));
    });
    //search menu all
    $("body").on("keyup","div.search-box1 input[name$=all_menu_name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getallmenulist($(this).val(),$('div.dyn_content'),"",$("select[name$=search_month]").val());
    });
    //search deliver all
    $("body").on("keyup","div.search-box1 input[name$=item_name_search]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getalldeliverylist($(this).val(),$('div.dyn_content'));
    });
    //by-time search
    $("body").on("change","div.search-box1 select[name$=search_mealtype]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getallmenulist($("div.search-box1 input[name$=all_menu_name]").val(),$('div.dyn_content'),$(this).val());
    });
    //by-month search
    $("body").on("change","div.search-box1 select[name$=search_month]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        $(this).siblings("div.search-box1 select[name$=search_mealtype]");
        stocks_cnv.getallmenulist($("div.search-box1 input[name$=all_menu_name]").val(),$('div.dyn_content'),"",$(this).val());
    });
    //search all
    $("body").on("keyup","div.search-box1 input[name$=all_ing_name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getalllist($(this).val(),$('div.dyn_content'),$("select[name$=search-day]").val());
    });
     //by-day search
    $("body").on("change","div.search-box1 select[name$=search-day]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getalllist($("div.search-box1 input[name$=all_ing_name]").val(),$('div.dyn_content'),$(this).val());
    });
    
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
            $('div.contents').html(stocksList);
            
            $.post(base_url+"ops/fb", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=day], select[name$=edit-ing-day]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.day,function(i,val){
                   $("select[name$=day], select[name$=edit-ing-day]")
                   .append($('<option></option>').val(val.id).html(val.name));
               });
               $("select[name$=ml_time], select[name$=edit_ml_time]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.ml_time,function(i,val){
                   $("select[name$=ml_time], select[name$=edit_ml_time]")
                   .append($('<option></option>').val(val.id).html(val.name));
               });
               $("select[name$=ml_month], select[name$=edit_ml_month]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.ml_month,function(i,val){
                   $("select[name$=ml_month], select[name$=edit_ml_month]")
                   .append($('<option></option>').val(val.id).html(val.name));
               });
               $("select[name$=ml_week], select[name$=edit_ml_week]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.ml_week,function(i,val){
                   $("select[name$=ml_week], select[name$=edit_ml_week]")
                   .append($('<option></option>').val(val.id).html(val.name));
               });
               $("select[name$=department]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.department,function(i,val){
                   $("select[name$=department]")
                   .append($('<option></option>').val(val.deptd_id).html(val.dept_name));
               });
               $("select[name$=item-name], select[name$=edit_item]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.item, function(i,val){
                  $("select[name$=item-name], select[name$=edit_item]")
                  .append($('<option></option>').val(val.prs_details_id).html(val.description));
               });
               $("select[name$=item-supplier], select[name$=edit_supplier]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.supplier, function(i, val){
                   $("select[name$=item-supplier], select[name$=edit_supplier]")
                   .append($('<option></option>').val(val.supplier_id).html(val.name));
               });
            });
        }
    });
    return new stock_list;
}); 