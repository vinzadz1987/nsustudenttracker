define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/lp/lp_price_list.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getallitemcat: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/lp_price_list",{
                dir: "all_item_cat"
            },function(data){
                container.html(data.res);
            },"json");
         },
        
         updateitem: function(update_info) {
            $.post(base_url+"ops/lp_price_list",{
                dir: "update_items",
                data: {
                    item_id: update_info.item_id.val(),
                    item_desc: update_info.item_desc.val(),
                    item_price: update_info.item_price.val(),
                    item_cat: update_info.item_cat.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallitem($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
          
    };
    
    
    
    //format dec
    $("body").on("keydown",".formatdec",function(e){
       $.decimalFormat($(this),e,2); 
    });
    
    
    //delete items
     $("body").on("click","li.delete-items",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_price_list",{
            dir: "delete_item",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json");
    });
    
   
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.fb ul li", function(){
       $("div.form-lp-item").slideUp();
       $("div.lp-item").slideUp();
       $("div.lp-item-update").slideUp();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fb ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Add Item":$("div#lp-item-form").slideDown(); 
                  
           }
           $(this).addClass('active');
       }
    });
  
   //toggle update checklist item
   $("body").on("click","div.cat-item", function(){
       $("div.lp-item").slideUp();
       if($(this).hasClass("active")) {
           $(this).attr("class","white-litegray");
       }else {
           $("div.cat-item").removeClass('active');
           switch($(this).attr("title")) {
               case "Category":$("div#show-item").slideDown();
                   checklist_cnv.getallitemcat($('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
   });
   //toggle update checklist item
   $("body").on("click","a.updateitem", function(){
       $("div.lp-item-update").slideUp();
       if($(this).hasClass("active")) {
           $(this).attr("class","ui");
       }else {
           $("a.updateitem").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Item":$("div#item-update").slideDown();
           }
           $(this).addClass('active');
       }
   });
    //add item
    $("body").on("submit","#frm-lp-item",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Filled is Empty.").show(3000);
                $('div.rq').html("A Required Filled is Empty").hide(1000);  
       } else {
                var infos = {
                    desc: $("input[name$=item-desc]").val(),
                    price: $("input[name$=item-price]").val(),
                    cat: $("select[name$=item-cat]").val()
                }
                $.post(base_url+"ops/lp_price_list",{
                    dir: "add_lp_item",
                    data: {
                        "desc": infos.desc,
                        "price": infos.price,
                        "cat": infos.cat
                    }
                },function(data){
                    if(data==="added") {         
                         $("#frm-lp-item")[0].reset();
                         $('div#showsuccess').fadeIn(1200);
                         $('div#showsuccess').fadeOut(1200);
                    } else {
                        $('div.rq').html("The item "+ infos.desc+ " is already exist.").show(3000);
                        $('div.rq').html("The item "+ infos.desc+ " is already exist.").hide(3000);
                        
                    }
                });
            }
        });
    });
    
     //show cat item
    $("body").on("click","div.menucenter", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_price_list",{
            dir: "all_item",
            bid: st_no
        }, function(data){
            $('div.item-all-show').html(data.res);
        },"json");
    });
   
   //delete items
     $("body").on("click","a.item-delete",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_price_list",{
            dir: "delete_items",
            pid: st_no
        },function(data){
             $('div.item-all-show').html(data.res);
        },"json");
    });
   
    //initialize update items
    $("body").on("click","a.item-update",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_price_list",{
            dir: "get_item_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#update-item input[name$=item-id]").val(data.item.id).data('original',data.item.id);
            $("#update-item input[name$=edit-item-desc]").val(data.item.description).data('original',data.item.description);      
            $("#update-item input[name$=edit-item-price]").val(data.item.price).data('original',data.item.price);      
            $("#update-item select[name$=edit-item-cat]").val(data.item.cat_id).data('original',data.item.cat_id);      
  
       },"json");
    });
   
   //delete items
     $("body").on("click","label.close-item",function(){
        
             $('div.updateitem').fadeOut(500);
       
    });
   
    //edit checklist submit form
    $("body").on("submit","#update-item",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    item_id: $("#update-item input[name$=item-id]"),
                    item_desc: $("#update-item input[name$=edit-item-desc]"),
                    item_price: $("#update-item input[name$=edit-item-price]"),
                    item_cat: $("#update-item select[name$=edit-item-cat]")
                };
                
                
                    checklist_cnv.updateitem(update_info);
                    $('div.rq').html("The item is updated succesfully.").show(3000);
                    $('div.rq').html("The item is updated succesfully.").hide(3000);
                    checklist_cnv.getallitemcat($('div.dyn_content'));
                    $('div.updateitem').fadeOut(500);
               
            }
        });
    });
    
   
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            checklist_cnv.getallitemcat($('div.dyn_content'));
            
            $.post(base_url+"ops/lp_price_list", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=item-cat], select[name$=edit-item-cat]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.category,function(i,val){
                   $("select[name$=item-cat], select[name$=edit-item-cat]")
                   .append($('<option></option>').val(val.cat_id).html(val.name));
               });
               
            });
        }
    });
    return new stock_list;
}); 