define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/lp/lp_instocks.html'
],function($,_,Backbone,base_url,stocksList){
    var instocks_cnv = {
         getallinstocks: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/lp_instocks",{
                dir: "all_item_stocks",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         }
      
    };
   
 
    //search all date
    $("body").on("keyup","div.search-box1 input[name$=item]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        instocks_cnv.getallinstocks($(this).val(),$('div.dyn_content'));
    });
 
  
  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
  
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            instocks_cnv.getallinstocks("",$('div.dyn_content'));
           
        }
    });
    return new stock_list;
}); 