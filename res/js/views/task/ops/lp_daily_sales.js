define([
    'jquery',
    'underscore',
    'backbone',
	'jQ-plugins/jquery.ui.core', // loaded for draggable event
	'jQ-plugins/jquery.ui.widget', // loaded for draggable event
	'jQ-plugins/jquery.ui.mouse', // loaded for draggable event
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/ops/lp/daily-sales-report/main-view.html',
	'text!tpl/task/ops/lp/daily-sales-report/add-daily-sales.html',
	'text!tpl/task/ops/lp/daily-sales-report/sales-field-template.html',
	'text!tpl/task/ops/lp/daily-sales-report/add-recap-fields.html',
	'text!tpl/task/ops/lp/daily-sales-report/error-log.html',
	'text!tpl/task/ops/lp/daily-sales-report/view-daily-sales.html'
], function(
		$, _, Backbone, jCore, jWidget, jMouse, baseURL, jsHelper, lbox, taskView, taskMenu, 
		mainView, addDailySales, salesFieldTemplate, recapFieldsTemplate, error_log, viewDailySales) {
	
	var collect_task = Backbone.Model.extend({});
	var user_name = Backbone.Model.extend({});
	var daily_sales = Backbone.Model.extend({});
        var item_name = Backbone.Model.extend({});
       
      
       
        item_name.prototype.url = baseURL+"ops/lp_daily_sales/get/items";
        var itmname = new item_name;
       
        
        //select attributes and data
        var $select = {
            
            attr: {"description": "lp_item","class": "css3-select2"},
            options:[]
           
        };
       
        itmname.fetch({
            success: function() {
                //console.log(itmname.attributes);
                //add item name
                $select.options = itmname.get("options");
              
            }
        });
        
        jsHelper.editing_items('.dcrc-itemnames', $select);
        
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	var d = new Date();
	
	var today = d.getMonth()+"/"+d.getDate()+"/"+d.getFullYear();
	
	user_name.prototype.url = baseURL+"ops/lp_daily_sales/uname";
	var userName =  new user_name;
	
	daily_sales.prototype.url = baseURL+"ops/lp_daily_sales/get/reports";
	var dailySales = new daily_sales;
	
	// insert the daily sales report when clicked
	$('body').delegate('#dcr-add-field', 'click', function(evt) {
		// $('.dcr-contents').append(salesFieldTemplate);
		$(salesFieldTemplate).insertBefore('.dcrc-total');
	});
	//show remove button when the sales field were hovered over
	$('.dcrc').live('mouseover mouseout', function(evt) {
		//console.log(evt.type);
		(evt.type === "mouseover") ? $(this).find('.dcrc-rem').show() : $(this).find('.dcrc-rem').hide();
	});
	
	//remove fields when the x button was clicked
	$('.dcrc-rem').live('click', function(evt) {
		var yen = 0, peso = 0, usdollar = 0, kwon = 0, slditems = 0, card = 0, $this = $(this);

		yen = parseFloat( $this.parent().find('.dcrc-yen').html() );
		peso = parseFloat( $this.parent().find('.dcrc-peso').html() );
		usdollar = parseFloat( $this.parent().find('.dcrc-usdollar').html() );
		kwon = parseFloat( $this.parent().find('.dcrc-koreanwon').html() );
		slditems = parseFloat( $this.parent().find('.dcrc-slditems').html() );
		card = parseFloat( $this.parent().find('.dcrc-card').html() );
                    
		// subtract from the total
		$('.dcrct-yen').html( (parseFloat( $('.dcrct-yen').html() ) - yen).toFixed(2) );
		$('.dcrct-peso').html( (parseFloat( $('.dcrct-peso').html() ) - peso).toFixed(2) );
		$('.dcrct-usdollar').html( (parseFloat( $('.dcrct-usdollar').html() ) - usdollar).toFixed(2) );
		$('.dcrct-koreanwon').html( (parseFloat( $('.dcrct-koreanwon').html() ) - kwon).toFixed(2) );
		$('.dcrct-slditems').html( (parseFloat( $('.dcrct-slditems').html() ) - slditems).toFixed(2) );
		$('.dcrct-card').html( (parseFloat( $('.dcrct-card').html() ) - card).toFixed(2) );
		
		$(this).parent().remove(); // remove the container
	});
	
	/*
	 * Make the div elements editable when clicked
	 * 
	 * This is on Daily Collection Report table not on the recap fields
	 */
	$(".dcrc-or, .dcrc-yen, .dcrc-peso, .dcrc-usdollar, .dcrc-koreanwon, .dcrc-card").live('click', function(evt){
		evt.preventDefault();evt.stopPropagation();
		//console.log($(this));
		var $elementClicked = $(this);
		var $width = $elementClicked.width();
		var $originalValue = $elementClicked.html();
		
		var $textbox = $("<input>").attr("type", "text").css({
			"border": "none", "height": "18px", 
			"width": $width-2, "text-align": "center", 
			//"border-bottom": "1px solid #CCC",
			"font-style": "italic",
			"background": "#FFF"
		});
		
		//console.log($originalValue);
		
		$elementClicked.html("").html($textbox);
		$textbox.val($originalValue);
		$textbox.click(function(e){e.stopPropagation();});
		$textbox.focus();
		
		if( $elementClicked.hasClass('dcrc-yen') || $elementClicked.hasClass('dcrc-peso') || $elementClicked.hasClass('dcrc-usdollar') || $elementClicked.hasClass('dcrc-koreanwon') || $elementClicked.hasClass('dcrc-card')) {
			$elementClicked.keypress(function(e){
                if( e.which!=8 && e.which!=0 && (e.which<46 || e.which>57 || e.which == 47 ) )
                    e.preventDefault();
            });
			
			/*
			 * calculate the total amount
			 * 
			 * @params:
			 * 			element			- the element being clicked
			 * 			totalContainer 	- the div element where the total number is placed
			 * @return void
			 */
			var calculateTotal = function( element, totalContainer ) {
				var total = 0;
				element.each(function(){
					var $this = $(this);
					//we need to check if the html value of the div element is a textbox and then get the value
					//ex. <input type='text' value="00.00" />
					//otherwise, just parseFloat the html value
					total += ( (/^<input\s.+$/i).test($this.html()) === true ) ? 
						parseFloat( $this.find('input[type="text"]').val() ) : 
							parseFloat( $this.html() );
				});
				totalContainer.html( total.toFixed(2) ); // insert the total value with 2 decimal places/point
			};
			
			// we need to calculate the amount on key-up event
			$textbox.keyup(function() {
				$parent = $(this).parent();
				
				if( $parent.hasClass('dcrc-yen') )
					calculateTotal( $('.dcrc-yen'), $('.dcrct-yen') );
				else if( $parent.hasClass('dcrc-peso') )
					calculateTotal( $('.dcrc-peso'), $('.dcrct-peso') );
				else if( $parent.hasClass('dcrc-usdollar') )
					calculateTotal( $('.dcrc-usdollar'), $('.dcrct-usdollar') );
				else if( $parent.hasClass('dcrc-koreanwon') )
					calculateTotal( $('.dcrc-koreanwon'), $('.dcrct-koreanwon') );
                                else if( $parent.hasClass('dcrc-slditems') )
                                        calculateTotal( $('.dcrc-slditems'), $('.dcrct-slditems'));
                                
			});
		}   
		
		$textbox.blur(function(evt) {
			evt.stopPropagation();
			$originalValue = ( $textbox.val() === "" || $textbox.val === null ) ? $originalValue : $textbox.val();
			$elementClicked.html($originalValue);
		});
		
	});

        /*
	 * Make the div elements editable when clicked
	 * 
	 * This is on Daily Collection Report table not on the recap fields
	 */
	$(".dcrc-slditems, .dcrc-price").live('click', function(evt){
		evt.preventDefault();evt.stopPropagation();
		//console.log($(this));
		var $elementClicked = $(this);
		var $width = $elementClicked.width();
		var $originalValue = $elementClicked.html();
		
		var $textbox = $("<input>").attr("type", "text","id","x").css({
			"border": "none", "height": "18px", 
			"width": $width-2, "text-align": "center", 
			//"border-bottom": "1px solid #CCC",
			"font-style": "italic",
			"background": "#FFF"
		});
		
		//console.log($originalValue);
		
		$elementClicked.html("").html($textbox);
		$textbox.val($originalValue);
		$textbox.click(function(e){e.stopPropagation();});
		$textbox.focus();
		
		if(  $elementClicked.hasClass('dcrc-slditems') || $elementClicked.hasClass('dcrc-price')) {
			$elementClicked.keypress(function(e){
                if( e.which!=8 && e.which!=0 && (e.which<46 || e.which>57 || e.which == 47 ) )
                    e.preventDefault();
            });
			
			/*
			 * calculate the total amount
			 * 
			 * @params:
			 * 			element			- the element being clicked
			 * 			totalContainer 	- the div element where the total number is placed
			 * @return void
			 */
			var calculateTotal = function( element, totalContainer ) {
				var total = 0;
				element.each(function(){
					var $this = $(this);
					//we need to check if the html value of the div element is a textbox and then get the value
					//ex. <input type='text' value="00.00" />
					//otherwise, just parseFloat the html value
					total += ( (/^<input\s.+$/i).test($this.html()) === true ) ? 
						parseFloat( $this.find('input[type="text"]').val() ) : 
							parseFloat( $this.html() );
				});
				totalContainer.html( total.toFixed(2) ); // insert the total value with 2 decimal places/point
			};
			
			// we need to calculate the amount on key-up event
			$textbox.keyup(function() {
				$parent = $(this).parent();
				
				if( $parent.hasClass('dcrc-slditems') )
					calculateTotal( $('.dcrc-slditems'), $('.dcrct-slditems') );
                                if( $parent.hasClass('dcrc-price') )
					calculateTotal( $('.dcrc-price'), $('.dcrct-price') );
                                
			});
		}   
		
		$textbox.blur(function(evt) {
			evt.stopPropagation();
			$originalValue = ( $textbox.val() === "" || $textbox.val === null ) ? $originalValue : $textbox.val();
			$elementClicked.html($originalValue);
		});
		
	});
        
	// insert the recap fields when clicked
	$('body').delegate('#dcr-add-rfield', 'click', function(evt) {
		$('.dcr-recapl').append(recapFieldsTemplate);
		// $(salesFieldTemplate).insertBefore('.dcrc-total');
		// console.log('sales');
	});
	//show remove button when the recap fields were hovered over
	$('.dcr-recaplc').live('mouseover mouseout', function(evt) {
		//console.log(evt.type);
		(evt.type === "mouseover") ? $(this).find('.dcrc-remrcp').show() : $(this).find('.dcrc-remrcp').hide();
	});
	
	$(".dcrc-remrcp").live("click", function() {
		$(this).parent().remove(); // remove the container
	});
	
	/*
	 * Make the recap fields editable when clicked
	 */
	$(".acct-name, .sales-acct, .ssp-acct, .sales-amt, .ssp-amt").live("click", function(evt){
		evt.preventDefault();evt.stopPropagation();
		
		var $elementClicked = $(this);
		var $width = $elementClicked.width();
		var $originalValue = $elementClicked.html();
		
		var $textbox = $("<input>").attr("type", "text").css({
			"border": "none", "height": "18px", 
			"width": $width-2, "text-align": "center", 
			//"border-bottom": "1px solid #CCC",
			"font-style": "italic",
			"background": "#FFF"
		});
		
		$elementClicked.html("").html($textbox);
		$textbox.val($originalValue);
		$textbox.click(function(e){e.stopPropagation();});
		$textbox.focus();
		
		if( $elementClicked.hasClass('sales-amt') || $elementClicked.hasClass('ssp-amt')) {
			// we won't allow to users to enter non-digit
			$elementClicked.keypress(function(e){
                if( e.which!=8 && e.which!=0 && (e.which<46 || e.which>57 || e.which == 47 ) )
                    e.preventDefault();
            });
			
			// calculate the total on keyup
			$textbox.keyup(function() {
				var $parent = $(this).parent();
				var $salesAmt = 0, $sspAmt = 0;
				var $totalContainer = $parent.parent().parent().parent().find('.dcr-total-dep'); 
				
                            	if( $parent.hasClass('sales-amt') ) {
					$salesAmt = parseFloat($textbox.val());
					$sspAmt = parseFloat($parent.next().next().html());					
				} else if( $parent.hasClass('ssp-amt') ) {
					$salesAmt = parseFloat($parent.prev().prev().html());
					$sspAmt = parseFloat($textbox.val());
				}
				
				$totalContainer.html( ($salesAmt + $sspAmt).toFixed(2) );
			});
		}
		
		$textbox.blur(function(evt) {
			evt.stopPropagation();
			$originalValue = ( $textbox.val() === "" || $textbox.val === null ) ? $originalValue : $textbox.val();
			$elementClicked.html($originalValue);
		});
	});
	
	$('body').delegate('.save-dailysales','click', function(evt) {
		//console.log($(this));
		var dailySales = new Array();
		var recap = new Array();
		var errorLog = new Array();
		
		$('.dcrc').each(function() {
			var salesData = {
				name: $(this).find('.dcrc-itemnames').html(),
				or_num: $(this).find('.dcrc-or').html(),
				yen: $(this).find('.dcrc-yen').html(),
				peso: $(this).find('.dcrc-peso').html(),
				us_dollar: $(this).find('.dcrc-usdollar').html(),
				k_won: $(this).find('.dcrc-koreanwon').html(),
				check: $(this).find('.dcrc-check').html(),
				check_num: $(this).find('.dcrc-price').html(),
				card: $(this).find('.dcrc-card').html(),
				card_num: $(this).find('.dcrc-cardn').html()
			};
			
			// make sure that some of the fields are not empty
			if( $(this).find('.dcrc-itemnames').html() !== 'Select Item' && $(this).find('.dcrc-particulars').html() !== '---' && $(this).find('.dcrc-or').html() !== '---' ) {
				dailySales.push(salesData);
			} else {
				if( !jsHelper.inArray('<p>Daily Sales Report Field must not be empty.</p>', errorLog) ) {
					errorLog.push("<p>Daily Sales Report Field must not be empty.</p>");
				}
				
			}
		});
                
                
		
		$('.dcr-recaplc').each(function() {
			var recapData = {
				acct_name: $(this).find('.acct-name').html(),
				sales_acct: $(this).find('.sales-acct').html(),
				sales_amt: $(this).find('.sales-amt').html(),
				ssp_acct: $(this).find('.ssp-acct').html(),
				ssp_amt: $(this).find('.ssp-amt').html()
			};
			
			// make sure that some of the fields are not empty
			if( $(this).find('.acct-name').html() !== "---" && $(this).find('.sales-acct').html() !== "---" && $(this).find('.ssp-acct').html() !== "---" ) {
				recap.push(recapData);
			}
			/*
			if( $(this).find('.acct-name').html() === "---" || (/^\-+$/g).test($(this).find('.acct-name').html()) ) {
				errorLog += "<p>Account name must not be empty or must not be \"---\"</p>";
			} if( $(this).find('.sales-acct').html() === "---" || (/^\-+$/g).test($(this).find('.acct-name').html())  ) {
				errorLog += "<p>Sales account number must not be empty or must not be \"---\"</p>";
			} if ( $(this).find('.ssp-acct').html() === "---" || (/^\-+$/g).test($(this).find('.acct-name').html())  ) {
				errorLog += "<p>SSP, VE & KM account number must not be empty or must not be \"---\"</p>";
			}*/
			
			if( $(this).find('.acct-name').html() === "---" && !jsHelper.inArray("<p>Account name must not be empty or must not be \"---\".</p>", errorLog) ) {
				errorLog.push("<p>Account name must not be empty or must not be \"---\".</p>");
			}if( $(this).find('.sales-acct').html() === "---" && !jsHelper.inArray("<p>Sales account number must not be empty or must not be \"---\".</p>", errorLog) ) {
				errorLog.push("<p>Sales account number must not be empty or must not be \"---\".</p>");
			}if ( $(this).find('.ssp-acct').html() === "---"  && !jsHelper.inArray("<p>SSP, VE & KM account number must not be empty or must not be \"---\".</p>", errorLog) ) {
				errorLog.push("<p>SSP, VE & KM account number must not be empty or must not be \"---\".</p>");
			}
			
		});
		
		if( _.isEmpty(errorLog) ) {
			// submit form here

			var data = {
				daily_sales: dailySales,
				recap: recap
			};
			
			//var saveDailySales = Backbone.Model.extend({});
			//saveDailySales.prototype.url = baseURL+"hote/daily_sales/save/reports";
			
			//var SaveDailySales = new saveDailySales;
			//SaveDailySales.set(data);
			
			//Backbone.emulateJSON = true;
			
			//Backbone.save({}, {success: function() {
			//		console.log('wee');
			//	}
			//});
			
			$.ajax({url: baseURL+"ops/lp_daily_sales/save/reports", type: "POST", data: data,
				success: function(data) {
					//console.log('wee');
					// close dialog box here
					$.lbox('removeData'); // remove the dialog box
					Backbone.history.loadUrl();
				}
			});
			
			//console.log(data);
		} else {
			// display error messages
			$('.dcr-errorlog').html(_.template(error_log, {errorLog: errorLog, _:_}));
		}
	});
	
	//Backbone View
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() {this.el.html(taskView);},
		events: {
			"click #dsa-report" : "addReport",
			"click .dsrli > li > a " : "viewReport"
		},
		viewReport: function(evt) {
			var id = parseInt($(evt.currentTarget).attr('rel'));
			//console.log(id);
			userName.fetch({success: function() {
					$.lbox({
						title: "View Daily Sales",
						content: _.template(viewDailySales,{id: id, _:_, result: dailySales.attributes, details: userName.attributes} )
					});
					$('#lbox').draggable({handle: "div.lbox-header"});
					$('.lbox-header').css({"cursor": "move"});
				}
			});
		},
		addReport: function(evt) {
			var $this = $(evt.currentTarget);
			userName.fetch({success: function() {
				
					$.lbox({
						title: "Daily Collection Report",
						content: _.template(addDailySales, {details: userName.attributes, _:_})
					});
					$('#lbox').draggable({handle: "div.lbox-header"});
					$('.lbox-header').css({"cursor": "move"});
				}
			});
		},
		render: function() {
			taskCollector.fetch({success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					
					dailySales.fetch({success: function() {
							$('.contents').html(_.template(mainView, {result: dailySales.attributes, _:_}));
							//console.log(dailySales.attributes);
						}
					});
					
				}
			});
		}
	});
	return new task_view;
});