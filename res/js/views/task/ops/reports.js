define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/reports.html'
],function($,_,Backbone,base_url,stocksList){
    var stocks_cnv = {
         getall_studlist: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/reports",{
                dir: "allstudents",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         }   
 
    };
    
    //initialize date picker
    $("body").on("click","input[name$=from_date], input[name$=to_date]", function(){
       $(this).datepicker({dateFormat: 'yy-mm-dd', changeYear: true, changeMonth: true, yearRange: '1940:+10'});
       $(this).datepicker({showOn: 'focus'}).focus();
    });
    
    
    $("body").on("click","li.delete-reports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/reports",{
            dir: "delete_date",
            pid: st_no
        },function(data){   
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    
     $("body").on("click","button.delete-daily-report",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/reports",{
            dir: "delete_dr",
            pid: st_no
        },function(data){
        $('div#sw').slideUp();
        $('div#swadd').slideDown();
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.fbwk ul li", function(){
       $("div.week_menu").slideUp();
       $("div.sw_contents").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fbwk ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Shortage Wastage":$("div#shortage_wastage").slideDown(); 
                   stocks_cnv.getall_studlist("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.day ul li", function(){
       $("div.sw_contents").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.day ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Reports":$("div#sw").slideDown();  
               stocks_cnv.getall_studlist("",$('div.dyn_content'));break;
               case "Add":$("div#swadd").slideDown();  
               stocks_cnv.getall_studlist("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });
   //add shortage_wastage
    $("body").on("submit","#addreports",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var sw_infos = {
                    sw_id : $("input[name$=sw_id]").val(),
                    day : $("select[name$=day]").val(),
                    b_shortage: $("input[name$=b_shortage]").val(),
                    b_wastage: $("input[name$=b_wastage]").val(),
                    b_comment: $("input[name$=b_comment]").val(),
                    l_shortage: $("input[name$=l_shortage]").val(),
                    l_wastage: $("input[name$=l_wastage]").val(),
                    l_comment: $("input[name$=l_comment]").val(),
                    d_shortage: $("input[name$=d_shortage]").val(),
                    d_wastage: $("input[name$=d_wastage]").val()
                    
                }
                $.post(base_url+"ops/reports",{
                    dir: "add_wst",
                    data: {
                        "sw_id": sw_infos.sw_id, 
                        "day": sw_infos.day,
                        "b_shortage": sw_infos.b_shortage,
                        "b_wastage": sw_infos.b_wastage,
                        "b_comment": sw_infos.b_comment,
                        "l_shortage": sw_infos.l_shortage,
                        "l_wastage": sw_infos.l_wastage,
                        "l_comment": sw_infos.l_comment,
                        "d_shortage": sw_infos.d_shortage,
                        "d_wastage": sw_infos.d_wastage
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input2[name$=number_of_stud]").val("");
                        stocks_cnv.getall_studlist("",$('div.dyn_content'));           
                        $("#addreports")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    //add shortage_wastage
    $("body").on("submit","#frm_sw",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    number_of_students : $("input[name$=number_of_students]").val(),
                    from: $("input[name$=from_date]").val(),
                    to: $("input[name$=to_date]").val()
                }
                $.post(base_url+"ops/reports",{
                    dir: "add_ws",
                    data: {
                        "number_of_students": infos.number_of_students,
                        "from": infos.from,
                        "to" : infos.to
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input2[name$=number_of_stud]").val("");
                        stocks_cnv.getall_studlist("",$('div.dyn_content'));           
                        $("#frm_sw")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    
    //initialize edit sw
    $("body").on("click","li.show-reports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/reports",{
            dir: "allsw",
            bid: st_no 
        },function(data){   
            $('div.sw-edit').html(data.res);    
        },"json");
    });
    //initialize edit sw
    $("body").on("click","li.add-reports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/reports",{
            dir: "allsw",
            bid: st_no 
        },function(data){   
            $('div.sw-edit').html(data.res);    
        },"json");
    });
    //initialize edit sw
    $("body").on("click","li.add-reports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/reports",{
            dir: "addreports",
            bid: st_no 
        },function(data){   
            $('div.sw-add').html(data.res);
            $("#addreports input[name$=sw_id]").val(data.sw.sw_id).data('original',data.sw.sw_id); 
        },"json");
    });
    
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="nos") {
            $.decimalFormat($(this),e,0);
        }else{
            $.decimalFormat($(this),e,0);
        }
    });
    //edit ingredients submit form
    $("body").on("submit","#edit-ingred",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit-ingred input[name$=edit-id]"),
                    name: $("#edit-ingred input[name$=edit-ing-name]"),
                    desc: $("#edit-ingred input[name$=edit-ing-desc]"),
                    day: $("#edit-ingred select[name$=edit-ing-day]")
                };
                if(update_info.name.val() != true) {
                $.customConfirm("Ingredient Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            stocks_cnv.updateing(update_info);
                                        }
                                    });
                }
                else {
                    stocks_cnv.updateing(update_info);
                }
            }
        });
    });
    
    //search all
    $("body").on("keyup","div.search-box1 input[name$=number_of_stud]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        stocks_cnv.getall_studlist($(this).val(),$('div.dyn_content'));
    }); 
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
            
            $.post(base_url+"ops/reports", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=day]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.day,function(i,val){
                   $("select[name$=day]")
                   .append($('<option></option>').val(val.id).html(val.name));
               });
               
            });
        }
    });
    return new stock_list;
}); 