define([
    'jquery',
    'underscore',
    'backbone',
    'collections/forms_menu/submenuFormsCollection',
    'text!tpl/submenu.html',
    'text!tpl/forms/prs/list.html',
    'text!tpl/forms/prs/list-template.html',
    'jsHelper',
    'baseURL'
], function($, _, Backbone, submenuFormsCollection, submenuTemplate, listView, listTemplate, jsHelper, baseURL) {
	var prs_list = Backbone.Model.extend({});
	var cancel_prs = Backbone.Model.extend({});
	
	jsHelper.page_pagination(".forms-list", listTemplate);
	jsHelper.hovered_items(".srs-pendapvd-list");
	
	var View = Backbone.View.extend({
		el: $('#container'),
		events: {
			"click .prs-list-search": "doSearchPrs",
			"click .cancel-prs": "doCancel"
		},
		doCancel: function(evt) {
			var id = $(evt.currentTarget).attr("class");
			id = id.split(" "); id = id[1];
			
			cancel_prs.prototype.url = baseURL+"forms/cancel/prs";
			var cancelPrs = new cancel_prs;
			cancelPrs.set({"ajax_request": "true", "id": id});
			
			Backbone.emulateHTTP = true;
			Backbone.emulateJSON = true;
			
			cancelPrs.save({}, {
				success: function() {
					Backbone.history.loadUrl();
				}
			});
			//console.log(id);
		},
		doSearchPrs: function(evt) {
			
			var $period_from = $("#period_from").val();
			var $period_to = $("#period_to").val();
			var $status = $("#forms_list_status").val();
			
			if( $period_from && $period_to && $status ) {
				$.ajax({
					type: "POST",
					url: baseURL+"forms/search/prs-list/",
					data: {
						ajax_request: "true",
						period_from: $period_from,
						period_to: $period_to,
						status: $status
					},
					success: function(data) {
						data = JSON.parse(data);
						var d = _.template(listTemplate, $.extend({}, data, {_:_}));
						$('.forms-list').html(d);
					}
				});	
			}
		},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			prs_list.prototype.url = baseURL+"forms/retrieve/prs-list";
			var prsList = new prs_list;
			
			prsList.fetch({
				success: function() {
					var data = _.template(listView, $.extend({}, prsList.attributes, {_:_}));
					$('#container').html(data);
					
					$("#period_from").datepicker();
					$("#period_to").datepicker();
				}
			});
		}
	});
	return new View;
});