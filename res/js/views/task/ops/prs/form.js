define([
    'jquery',
    'underscore',
    'backbone',
    'text!tpl/forms/prs/form.html',
    'text!tpl/submenu.html',
    'collections/forms_menu/submenuFormsCollection',
    'text!tpl/task/prs/add-field.html',
    'baseURL',
    'jsHelper'
], function($, _, Backbone, srsF, submenuTemplate, submenuFormsCollection, addField, baseURL, jsHelper){
	
	var prs_form = Backbone.Model.extend({});
	var units_of_measure = Backbone.Model.extend({});
	var save_prs = Backbone.Model.extend({});
	
	units_of_measure.prototype.url = baseURL+"forms/retrieve/unitsofmeasure";
	var uom = new units_of_measure;
	
	//select attributes and data
	var $select = {
		attr: {"name": "prs-uom","class": "css3-select"},
		options: []
	};
	uom.fetch({
		success: function() {
			//console.log(uom.attributes);
			// add the units of measure
			$select.options = uom.get("options");
		}
	});
	// for editing items/elements
	jsHelper.editing_items('.prs-add-content-description, .prs-add-content-qty, .prs-add-content-unitofmeasure, .prs-add-content-unitprice', $select);
	
	$('.prs-add-field a').live("click", function(evt) {
		evt.preventDefault();
		$(".prs-add-list").append(addField);
	});	
	
	var rView = Backbone.View.extend({
		el: $('#container'),
		initialize: function() {},
		events: {
			"click .prs-add-content-remove a": "doRemove",
			"click .prs-submit-form input[type='button']": "doSave"
		},
		doSave: function() {
			var prs = new Array();
			// validate added prs items
			$(".prs-add-list .prs-add-list-content").each(function() {
				var $item = {}, empty = false;
				$(".prs-add-content-description, .prs-add-content-unitofmeasure, .prs-add-content-qty, .prs-add-content-unitprice, .prs-add-content-amt", $(this)).each(function(index, element) {
					//$item.push($(this).html());
					var $e = $(element);
					if( $e.hasClass("prs-add-content-description") && ($e.html() === "Enter the product name or description") ) {
						empty = true;
					} if( $e.hasClass("prs-add-content-unitofmeasure") && ($e.html() === "(ex. each, kilo, grams)") ) {
						empty = true;
					} if( $e.hasClass("item-qty") && ( $e.html() == "0" || parseInt($e.html()) <= 0 ) ) {
						empty = true;
					} if( $e.hasClass("item-price") && ( $e.html() == "0.00" || parseFloat($e.html()) <= 0 ) ) {
						empty = true;
					} if( $e.hasClass("item-amt") && ( $e.html() == "0.00" || parseFloat($e.html()) <= 0 ) ) {
						empty = true;
					}
					//$item.push($e.html());
					$item[ $e.attr("id") ] = $e.html();
				});
				if( !empty ) prs.push($item);
			});

			var validate = jsHelper.validate("input[type='text'], select",$(".container-contents"));
			
			//console.log(validate);
			if( validate !== true && !_.isEmpty(prs) ) {
				save_prs.prototype.url = baseURL+"forms/submit/prs-form";
				var savePrsData = $.extend({}, validate, {ajax_request: "true", prs_items: prs});
				var savePrs = new save_prs;
				
				savePrs.set(savePrsData);
				
				Backbone.emulateJSON = true;
				savePrs.save({}, {
					success: function() {
						Backbone.history.navigate("#/forms/prs/list");	
					}
				});
				//console.log(savePrs);
			}
		},
		doRemove: function(evt) {
			// remove fields
			var $parent = $(evt.currentTarget).parent().parent();
			$parent.fadeOut(500, function(){$(this).remove();});
		},
		render: function() {
			var data = {
				subs: submenuFormsCollection.models,
				_: _
			};
			var sub = _.template(submenuTemplate, data);
			$('#sub-menu').html(sub);
			
			prs_form.prototype.url = baseURL+"forms/retrieve/prs-form/";
			var prsForm = new prs_form;
			prsForm.fetch({
				success: function() {
					var $data = _.template(srsF, $.extend({}, prsForm.attributes, {_:_}));
					$('#container').html($data);
					$('#prs_daterequired').datepicker();
				}
			});
		}
	});
	return new rView;
});