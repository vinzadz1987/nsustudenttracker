define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/inventory.html',
],function($,_,Backbone,base_url,inventorylist){
    var item_cnv = {
        getprscancelledlist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "cancelled_prs_list"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getprscount: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "count_prs"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getprsrejectedlist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "rejected_prs_list"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getprspendinglist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "pending_prs_list"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getprsapprovedlist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "approved_prs_list"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getsupplierlist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "retrieve_supplier_list"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getstocktype: function(container){
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
               dir: "stocktype"
            }, function(data){
                container.html(data.res);
            },"json");  
        },
        getuom: function(container){
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
               dir: "uom"
            }, function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");  
        },
        updatestocktype: function(update_info) {
            $.post(base_url+"ops/inventory",{
                dir: "stocktypeupdate",
                data: {
                    id: update_info.id.val(),
                    name: update_info.name.val()     
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {    
                    item_cnv.getstocktype($('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
         updateuom: function(update_info) {
            $.post(base_url+"ops/inventory",{
                dir: "uomupdate",
                data: {
                    id: update_info.id.val(),
                    name: update_info.name.val()     
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {    
                    item_cnv.getuom($('div.dyn_content'));
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        getitemlist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/inventory",{
                dir: "retrieve_item_list"
            },function(data){
                container.html(data.res);
            },"json");
        }
    };
    //input with "formatdec" class is passed to the decimalFormat plugin (jquery.decimalFormat)
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="acqui_price") {
            $.decimalFormat($(this),e,2);
        } else {
            $.decimalFormat($(this),e,0);
        }
    });
    //toggle item-forms forms on <li> click
    $("body").on("click","div.stocks ul li",function(){
        $("div.item-forms").slideUp();
        $('div.dyn_content').empty();
        if($(this).hasClass("active")) {
            $(this).attr("class","white-litegray");
        } else {
            $("div.stocks ul li").removeClass('active');
            switch ($(this).attr("title")) {
                case "PRS List":$("div#PRS-List").slideDown();
                    item_cnv.getuom($(''));break;
                case "Add Item":$("div#additem-form").slideDown();
                    item_cnv.getitemlist($("div.dyn_content"));break;
                case "Supplier":$("div#supplier-form").slideDown();
                    item_cnv.getsupplierlist($('div.dyn_content'));break;
               /* case "Stock type":$("div#item-stocktype").slideDown();
                    item_cnv.getstocktype($('div.dyn_content'));break;*/
                case "Unit of Measure":$("div#item-uom").slideDown();
                    item_cnv.getuom($('div.dyn_content'));break; 
                case "Stock Items":$("div#item-entry").slideDown();
                    item_cnv.getuom($('div.dyn_content'));break; 
            }
            $(this).addClass('active');
        }
    });
    //toggle PRS pending, approved, rejected and cancelled <li> click
    $("body").on("click","div.PRS ul li",function(){
        $("div.PRS-list").slideUp();
        $('div.dyn_content').empty();
        if($(this).hasClass("active")) {
            $(this).attr("class","white-litegray"); 
            item_cnv.getprscount($('div.dyn_content'));
        } else {
            $("div.PRS ul li").removeClass('active');
            switch ($(this).attr("title")) {
                case "Pending PRS":$("").slideDown();
                    item_cnv.getprspendinglist($('div.dyn_content'));break;
                case "Approved PRS":$("").slideDown();
                    item_cnv.getprsapprovedlist($('div.dyn_content'));break;
                case "Cancelled PRS":$("").slideDown();
                    item_cnv.getprscancelledlist($('div.dyn_content'));break;
                case "Rejected PRS":$("").slideDown();
                    item_cnv.getprsrejectedlist($('div.dyn_content'));break;
            }
            $(this).addClass('active');
        }
    });
    //handle stock type form submit
    $("body").on("submit","form#item-st", function(e){
      e.preventDefault();
      $(this).formvalidate(function(data){
          if(data.result === false) {
              $.customAlert("A required field is empty.");    
          } else {
              $.post(base_url+"ops/inventory",{
                 dir: "add_stocktype",
                 data: {
                     'name': $("form#item-st input[name$=name]").val()
                 }
              }, function(data){
                  if(data.res == true) {
                      item_cnv.getstocktype($('div.dyn_content'));
                      $("form#item-st")[0].reset();
                  }else{
                      $.customAlert("An error occured. Please refresh this page and try again");
                  }
              }, "json");
          }
     });
    });
    //handle unit of measure add submit form
    $("body").on("submit","form#item_uom", function(e){
      e.preventDefault();
      $(this).formvalidate(function(data){
          if(data.result === false) {
              $.customAlert("A required field is empty.");    
          } else {
              $.post(base_url+"ops/inventory",{
                 dir: "adduom",
                 data: {
                     'name': $("form#item_uom input[name$=name]").val()
                 }
              }, function(data){
                  if(data.res == true) {
                      item_cnv.getuom($('div.dyn_content'));
                      $("form#item_uom")[0].reset();
                  }else{
                      $.customAlert("An error occured. Please refresh this page and try again");
                  }
              }, "json");
          }
     });
    });
    //edit stock type submit form
    $("body").on("submit","#edit-st",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit-st input[name$=edit-id]"),
                    name: $("#edit-st input[name$=edit-stock-name]")           
                };
                if(update_info.name.val() != true) {
                $.customConfirm("Updated successfully ",function(response){
                                        if(response===true) {
                                            item_cnv.updatestocktype(update_info);
                                        }
                                    });
                }
                else {
                    item_cnv.updatestocktype(update_info);
                }
            }
        });
    });
    //edit unit of measure submit form
    $("body").on("submit","#edit-uom-frm",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    id: $("#edit-uom-frm input[name$=edit-id]"),
                    name: $("#edit-uom-frm input[name$=edit-uom-name]")           
                };
                if(update_info.name.val() != true) {
                $.customConfirm("Updated successfully ",function(response){
                                        if(response===true) {
                                            item_cnv.updateuom(update_info);
                                        }
                                    });
                }
                else {
                    item_cnv.updateuom(update_info);
                }
            }
        });
    });
    //edit stock type initialize form
    $("body").on("click","div.edit_stock_btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_stocks",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit-st input[name$=edit-id]").val(rcv.stock.id);
            $("#edit-st input[name$=edit-stock-name]").val(rcv.stock.name).data('original',rcv.stock.name);         
            $(this).litebox("div.edit-stock",{show:true});
        });
    });
    //show approve prs details list
    $("body").on("click","div.prs-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_approved_prs",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('div.desc').html(rcv.apr_prs.descript);
            $('div.quant').html(rcv.apr_prs.quant);
            $('div.unt').html(rcv.apr_prs.unt);
            $('div.untp').html(rcv.apr_prs.untp);
            $('div.amt').html(rcv.apr_prs.amt);
            $(this).facebox("div.prs-detail",{show:true});
        });
    });
    //show pending prs details list
    $("body").on("click","div.prs-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_pending_prs",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('div.desc').html(rcv.apr_prs.descript);
            $('div.quant').html(rcv.apr_prs.quant);
            $('div.unt').html(rcv.apr_prs.unt);
            $('div.untp').html(rcv.apr_prs.untp);
            $('div.amt').html(rcv.apr_prs.amt);
            $(this).facebox("div.prs-detail",{show:true});
        });
    });
    //show cancelled prs details list
    $("body").on("click","div.prs-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_cancelled_prs",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('div.desc').html(rcv.apr_prs.descript);
            $('div.quant').html(rcv.apr_prs.quant);
            $('div.unt').html(rcv.apr_prs.unt);
            $('div.untp').html(rcv.apr_prs.untp);
            $('div.amt').html(rcv.apr_prs.amt);
            $(this).facebox("div.prs-detail",{show:true});
        });
    });
    //show rejected prs details list
    $("body").on("click","div.prs-contents",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_rejected_prs",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $('div.desc').html(rcv.apr_prs.descript);
            $('div.quant').html(rcv.apr_prs.quant);
            $('div.unt').html(rcv.apr_prs.unt);
            $('div.untp').html(rcv.apr_prs.untp);
            $('div.amt').html(rcv.apr_prs.amt);
            $(this).facebox("div.prs-detail",{show:true});
        });
    });
    //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
           dir: "uom"
    },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    //edit Unit of Measure initialize form
    $("body").on("click","div.edit_uom_btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_uom",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit-uom-frm input[name$=edit-id]").val(rcv.uom.id);
            $("#edit-uom-frm input[name$=edit-uom-name]").val(rcv.uom.name).data('original',rcv.uom.name);         
            $(this).litebox("div.edit-uom",{show:true});
        });
    });

    /* delete item/supplier entry mod/stock/stocktype */ 
    $("body").on("hover","div.phic-contents, div.item-contents",function(e){
        if(e.type=="mouseenter") {
            $(this).children("div.options").stop(true,true).fadeIn();
        }
        else if(e.type=="mouseleave") {
            $(this).children("div.options").stop(true,true).fadeOut();
        }
    });
    $("body").on("click","div.delete_supplier_btn",function(){
        var phic_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "delete_supplier_entry",
            pid: phic_no
        },function(data){
            $('div.dyn_content').html(data.res);
        },"json");
    });
     $("body").on("click","div.delete_stock_btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "delete_stocktype",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res);
        },"json");
    });
    $("body").on("click","div.delete_uom_btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "deleteuom",
            pid: st_no
        },function(data){
            $('div.dyn_content').html(data.res);
        },"json");
    });
   
    $("body").on("click","div.edit_item_btn",function(){
        var item_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_details",
            bid: item_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit-item input[name$=edit-id]").val(rcv.item.stock_id);
            $("#edit-item select[name$=edit-item-stocktype]").val(rcv.item.type_name).data('original',rcv.item.type_name);         
            $("#edit-item select[name$=edit-item-unit]").val(rcv.item.unit_name).data('original', rcv.item.unit_name);
            $("#edit-item input[name$=edit-item-name]").val(rcv.item.name).data('original', rcv.item.name);
            $("#edit-item input[name$=edit-item-brand]").val(rcv.item.brand).data('original', rcv.item.brand);
            $("#edit-item input[name$=edit-item-quantity]").val(rcv.item.qty_inhand).data('original', rcv.item.qty_inhand);
            $("#edit-item input[name$=edit-item-reorder]").val(rcv.item.reorder_point).data('original', rcv.item.reorder_point);
            $("#edit-item input[name$=edit-item-acqprice]").val(rcv.item.aqui_price).data('original', rcv.item.aqui_price);
            $("#edit-item select[name$=edit-item-supplier]").val(rcv.item.supplier_name).data('original', rcv.item.supplier_name);
            $("#edit-item textarea[name$=edit-item-descript]").val(rcv.item.description).data('original', rcv.item.description);
            $(this).litebox("div.item-edit",{show:true});
        });
    });
    //edit stock initialize form
    $("body").on("click","div.edit_stock_btn",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/inventory",{
            dir: "retrieve_stocks",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res);
            var rcv = $.parseJSON(data);
            $("#edit-st input[name$=edit-id]").val(rcv.stock.id);
            $("#edit-st input[name$=edit-stock-name]").val(rcv.stock.name).data('original',rcv.stock.name);         
            $(this).litebox("div.edit-stock",{show:true});
        });
    });
    //handle item form submit
    $("body").on("submit","form#additem-frm",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result === false) {
                 $.customAlert("A required field is empty.");
            } else {
                $.post(base_url+"ops/inventory",{
                    dir: "add_item_entry",
                    data: {
                        'stock_type': $("form#additem-frm select[name$=stock_type]").val(),
                        'brand': $("form#additem-frm input[name$=brand]").val(),
                        'name': $("form#additem-frm input[name$=name]").val(),
                        'unitof_measure': $("form#additem-frm select[name$=unitof_measure]").val(),
                        'qty_inhand': $("form#additem-frm input[name$=qty_inhand]").val(),
                        'reorder_point': $("form#additem-frm input[name$=reorder_point]").val(),
                        'acqui_price': $("form#additem-frm input[name$=acqui_price]").val(),
                        'suppliers': $("form#additem-frm select[name$=suppliers]").val(), 
                        'added_by': $("form#additem-frm input[name$=added_by]").val(),
                        'stock_image': $("form#additem-frm input[name$=stock_image]").val(),
                        'description': $("form#additem-frm textarea[name$=description]").val()
                    }
                },function(data){
                    if(data.res == true) {
                        item_cnv.getitemlist($('div.dyn_content'));
                        $("form#additem-frm")[0].reset();
                    } else {
                        $.customAlert("An error occurred. Please refresh this page and try again.");
                    }
                },"json");  
            }
        });
    }); 
   
    var contributions_list = Backbone.View.extend({
        el: $("div#container"),
        initialize: function() {
        },
        events: {},
        render: function() {
            $('div.contents').html(inventorylist);
           
            $.post(base_url+"ops/inventory",{
                dir: 'initializer'
               
            },function(data){
                var rcv = $.parseJSON(data);
                
                $("select[name$=stock_type],select[name$=edit-item-stocktype]").empty()
                .append($('<option></option>').val("").html(""));
                $("select[name$=unitof_measure], select[name$=edit-item-unit]").empty()
                .append($('<option></option>').val("").html(""));
                $("select[name$=suppliers], select[name$=edit-item-supplier]").empty()
                .append($('<option></option>').val("").html(""));
                
                $.each(rcv.stock_type,function(i,val){
                    $("select[name$=stock_type],select[name$=edit-item-stocktype]").append($('<option></option>')
                    .val(val.id).html(val.name));
                });
                $.each(rcv.unitof_measure,function(i,val){
                    $("select[name$=unitof_measure], select[name$=edit-item-unit]").append($('<option></option>')
                    .val(val.id).html(val.name));
                });
                $.each(rcv.suppliers,function(i,val){
                    $("select[name$=suppliers],#edit-item select[name$=edit-item-supplier]").append($('<option></option>')
                    .val(val.supplier_id).html(val.name));
                });
                
                
                
            });
        }
    });
    return new contributions_list;
});


