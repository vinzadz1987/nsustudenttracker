define([
    'jquery',
    'underscore',
    'backbone',
    'jQ-plugins/timepicker',
    'baseURL',
    'text!tpl/task/ops/hk/checklist_form.html'
],function($,_,Backbone, timepicker, base_url,stocksList){
    var checklist_cnv = {
         getallhkattendant: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/checklist_form",{
                dir: "allchecklistattendant",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         getallattendant: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/checklist_form",{
                dir: "allattendant",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         updatechecklist: function(update_info) {
            $.post(base_url+"ops/checklist_form",{
                dir: "updatechecklistcontents",
                data: {
                    ra_checklist_id: update_info.uch_id.val(),
                    remarks: update_info.remarks.val(),
                    room_no: update_info.room_no.val(),
                    time_in: update_info.time_in.val(),
                    time_out: update_info.time_out.val(),
                    room_status: update_info.room_status.val(),
                    guest_no: update_info.guest_no.val(),
                    bt_in: update_info.bt_out.val(),
                    bt_out: update_info.bt_out.val(),
                    bm_in: update_info.bm_in.val(),
                    bm_out: update_info.bm_out.val(),
                    fit_in: update_info.fit_in.val(),
                    fit_out: update_info.fit_out.val(),
                    flat_in: update_info.flat_in.val(),
                    flat_out: update_info.flat_out.val(),
                    blanket_in: update_info.blanket_in.val(),
                    blanket_out: update_info.blanket_out.val(),
                    pilow_in: update_info.pilow_in.val(),
                    pilow_out: update_info.pilow_out.val(),
                    pilocs_in: update_info.pilowcs_in.val(),
                    pilocs_out: update_info.pilowcs_out.val(),
                    toilet_tissue_in: update_info.toilet_tissue_in.val(),
                    toilet_tissue_out: update_info.toilet_tissue_out.val(),
                    extra_work_done: update_info.extra_work_done.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallhkattendant($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
          
    };
    //delete room report
     $("body").on("click","li.delete-chroomchecklist",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/checklist_form",{
            dir: "delete_atnroomchecklist",
            pid: st_no
        },function(data){
        $('div#cr').html(data.res).slideUp();
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
     $("body").on("click","li.delete-chreports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/checklist_form",{
            dir: "delete_atnchecklist",
            pid: st_no
        },function(data){   
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    $("body").on("click","li.delete-attendant",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/checklist_form",{
            dir: "delete_attendant",
            pid: st_no
        },function(data){   
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    //format dec
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="guest-no") {
            $.decimalFormat($(this),e,0);
        }else{
            $.decimalFormat($(this),e,0);
        }
    });
    //initialize date picker
    $("body").on("click","input[name$=date]", function(){
       $(this).datepicker({dateFormat: 'yy-mm-dd',changeYear: true, changeMonth: true, yearRange: '1940:+10'});
       $(this).datepicker({showOn: 'focus'}).focus();
    });
  
    //toggle stocks weekly forms on <li> click
    $("body").on("click","div.rs ul li", function(){
       $("div.form_checklist").slideUp();
       $("div.checklist-report").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.rs ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Checklist":$("div#form").slideDown(); 
                   checklist_cnv.getallhkattendant("",$('div.dyn_content'));break;
               case "Attendant":$("div#form_attendant").slideDown(); 
                   checklist_cnv.getallattendant("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
    });
   
   //toggle stocks weekly forms on <li> click
    $("body").on("click","div.day ul li", function(){
       $("div.checklist-report").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.day ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Show Reports":$("div#cr").slideDown();  
               checklist_cnv.getallhkattendant("",$('div.dyn_content'));break;
               case "Add Reports":$("div#chradd").slideDown();  
               checklist_cnv.getallhkattendant("",$('div.dyn_content'));break;
              
           }
           $(this).addClass('active');
       }
    });
   
   //toggle update checklist attendant report
   $("body").on("click","div.chl ul li", function(){
       $("div.checklist-report").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","ch");
       }else {
           $("div.chl ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Checklist":$("div#chrupdate").slideDown();
                   checklist_cnv.getallhkattendant("",$('div.dyn_content'));break;
                case "Delete Checklist":$("div#cr").slideDown();
                   checklist_cnv.getallhkattendant("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }
   });
    //add checklist attendant
    $("body").on("submit","#frm_checklist",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    date: $("input[name$=date]").val(),
                    am: $("select[name$=am_hr]").val(),
                    pm: $("select[name$=pm_hr]").val(),
                    prepared: $("select[name$=prepared_by]").val()
                }
                $.post(base_url+"ops/checklist_form",{
                    dir: "addchecklistattendant",
                    data: {
                        "date": infos.date,
                        "am": infos.am,
                        "pm" : infos.pm,
                        "prepared": infos.prepared
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=attendant_name]").val("");
                        checklist_cnv.getallhkattendant("",$('div.dyn_content'));           
                        $("#frm_checklist")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
     //add attendant
    $("body").on("submit","#frm_attendant",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    fname: $("input[name$=fname]").val(),
                    lname: $("input[name$=lname]").val()  
                }
                $.post(base_url+"ops/checklist_form",{
                    dir: "addattendant",
                    data: {
                        "fname": infos.fname,
                        "lname": infos.lname
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=attendant_fname]").val("");
                        checklist_cnv.getallattendant("",$('div.dyn_content'));           
                        $("#frm_attendant")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    //initilize id of checklist form reports
    $("body").on("click","li.show-chreports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/checklist_form",{
            dir: "getreport",
            bid: st_no 
        },function(data){   
            $('div.cl-report').html(data.res);    
        },"json");
    });
    //initialize add checklist reports
    $("body").on("click","li.add-chreports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/checklist_form",{
            dir: "addchecklist",
            bid: st_no 
        },function(data){   
            $('div.chr-add').html(data.res);    
            $("#addchecklist input[name$=ch_id]").val(data.checklist_reports.tracker_id).data('original',data.checklist_reports.tracker_id); 
            $("#chradd label.ra_date").html(data.checklist_reports.date).data('original',data.checklist_reports.date);
            $("#chradd label.am").html(data.checklist_reports.am).data('original',data.checklist_reports.am);
            $("#chradd label.pm").html(data.checklist_reports.pm).data('original',data.checklist_reports.pm);
            $("#chradd label.fname").html(data.checklist_reports.fname).data('original',data.checklist_reports.fname);
            $("#chradd label.lname").html(data.checklist_reports.lname).data('original',data.checklist_reports.lname);
        },"json");
    });
    //initialize update checklist reports
    $("body").on("click","li.update-chreports",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/checklist_form",{
            dir: "updatechecklist",
            bid: st_no 
        },function(data){   
            $('div.chr-update').html(data.res);    
            $("#updatechecklist input[name$=uch_id]").val(data.checklist_reports.ra_checklist_id).data('original',data.checklist_reports.ra_checklist_id); 
            $("#chrupdate label.ra_date").html(data.checklist_reports.date).data('original',data.checklist_reports.date);
            $("#chrupdate label.am").html(data.checklist_reports.am).data('original',data.checklist_reports.am);
            $("#chrupdate label.pm").html(data.checklist_reports.pm).data('original',data.checklist_reports.pm);
            $("#chrupdate label.fname").html(data.checklist_reports.fname).data('original',data.checklist_reports.fname);
            $("#chrupdate label.lname").html(data.checklist_reports.lname).data('original',data.checklist_reports.lname);
            $("#updatechecklist select[name$=edit-room_no]").val(data.checklist_reports.room_no).data('original',data.checklist_reports.room_no);
            $("#updatechecklist input[name$=edit-time-in]").val(data.checklist_reports.time_in).data('original',data.checklist_reports.time_in);
            $("#updatechecklist input[name$=edit-time-out]").val(data.checklist_reports.time_out).data('original',data.checklist_reports.time_out);
            $("#updatechecklist select[name$=edit-room_status]").val(data.checklist_reports.room_status_id).data('original',data.checklist_reports.room_status_id);
            $("#updatechecklist input[name$=edit-guest-no]").val(data.checklist_reports.guest_no).data('original',data.checklist_reports.guest_no);
            $("#updatechecklist input[name$=edit-bt-in]").val(data.checklist_reports.bt_in).data('original',data.checklist_reports.bt_in);
            $("#updatechecklist input[name$=edit-bt-out]").val(data.checklist_reports.bt_out).data('original',data.checklist_reports.bt_out);
            $("#updatechecklist input[name$=edit-bm-in]").val(data.checklist_reports.bm_in).data('original',data.checklist_reports.bm_in);
            $("#updatechecklist input[name$=edit-bm-out]").val(data.checklist_reports.bm_out).data('original',data.checklist_reports.bm_out);
            $("#updatechecklist input[name$=edit-fit-in]").val(data.checklist_reports.fit_in).data('original',data.checklist_reports.fit_in);
            $("#updatechecklist input[name$=edit-fit-out]").val(data.checklist_reports.fit_out).data('original',data.checklist_reports.fit_out);
            $("#updatechecklist input[name$=edit-flat-in]").val(data.checklist_reports.flat_in).data('original',data.checklist_reports.flat_in);
            $("#updatechecklist input[name$=edit-flat-out]").val(data.checklist_reports.flat_out).data('original',data.checklist_reports.flat_out);
            $("#updatechecklist input[name$=edit-blanket-in]").val(data.checklist_reports.blanket_in).data('original',data.checklist_reports.blanket_in);
            $("#updatechecklist input[name$=edit-blanket-out]").val(data.checklist_reports.blanket_out).data('original',data.checklist_reports.blanket_out);
            $("#updatechecklist input[name$=edit-pilow-in]").val(data.checklist_reports.pilow_in).data('original',data.checklist_reports.pilow_in);
            $("#updatechecklist input[name$=edit-pilow-out]").val(data.checklist_reports.pilow_out).data('original',data.checklist_reports.pilow_out);
            $("#updatechecklist input[name$=edit-pilowcs-in]").val(data.checklist_reports.pilocs_in).data('original',data.checklist_reports.pilocs_in);
            $("#updatechecklist input[name$=edit-pilowcs-out]").val(data.checklist_reports.pilocs_out).data('original',data.checklist_reports.pilocs_out);
            $("#updatechecklist input[name$=edit-toilet-tissue-in]").val(data.checklist_reports.toilet_tissue_in).data('original',data.checklist_reports.toilet_tissue_in);
            $("#updatechecklist input[name$=edit-toilet-tissue-out]").val(data.checklist_reports.toilet_tissue_out).data('original',data.checklist_reports.toilet_tissue_out);
            $("#updatechecklist input[name$=edit-remarks]").val(data.checklist_reports.remarks).data('original',data.checklist_reports.remarks);
            $("#updatechecklist input[name$=edit-extra-work-done]").val(data.checklist_reports.extra_work_done).data('original',data.checklist_reports.extra_work_done);
        },"json");
    });
    //add daily checklist
    $("body").on("submit","#addchecklist",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var checklist_infos = {
                    ch_id : $("input[name$=ch_id]").val(),
                    room_no: $("select[name$=room_no]").val(),
                    time_in: $("input[name$=time-in]").val(),
                    time_out: $("input[name$=time-out]").val(),
                    room_status: $("select[name$=room_status]").val(),
                    guest_no: $("input[name$=guest-no]").val(),
                    bt_in: $("input[name$=bt-in]").val(),
                    bt_out: $("input[name$=bt-out]").val(),
                    bm_in: $("input[name$=bm-in]").val(),
                    bm_out: $("input[name$=bm-out]").val(),
                    fit_in: $("input[name$=fit-in]").val(),
                    fit_out: $("input[name$=fit-out]").val(),
                    flat_in: $("input[name$=flat-in]").val(),
                    flat_out: $("input[name$=flat-out]").val(),
                    blanket_in: $("input[name$=blanket-in]").val(),
                    blanket_out: $("input[name$=blanket-out]").val(),
                    pilow_in: $("input[name$=pilow-in]").val(),
                    pilow_out: $("input[name$=pilow-out]").val(),
                    pilowcs_in: $("input[name$=pilowcs-in]").val(),
                    pilowcs_out: $("input[name$=pilowcs-out]").val(),
                    toilet_tissue_in: $("input[name$=toilet-tissue-in]").val(),
                    toilet_tissue_out: $("input[name$=toilet-tissue-out]").val(),
                    remarks: $("input[name$=remarks]").val(),
                    extra_work_done: $("input[name$=extra-work-done]").val()
                }
                $.post(base_url+"ops/checklist_form",{
                    dir: "addchecklistreports",
                    data: {
                        "ch_id": checklist_infos.ch_id, 
                        "room_no": checklist_infos.room_no,
                        "time_in": checklist_infos.time_in,
                        "time_out": checklist_infos.time_out,
                        "room_status": checklist_infos.room_status,
                        "guest_no": checklist_infos.guest_no,
                        "bt_in": checklist_infos.bt_in,
                        "bt_out": checklist_infos.bt_out,
                        "bm_in": checklist_infos.bm_in,
                        "bm_out": checklist_infos.bm_out,
                        "fit_in": checklist_infos.fit_in,
                        "fit_out": checklist_infos.fit_out,
                        "flat_in": checklist_infos.flat_in,
                        "flat_out": checklist_infos.flat_out,
                        "blanket_in": checklist_infos.blanket_in,
                        "blanket_out": checklist_infos.blanket_out,
                        "pilow_in": checklist_infos.pilow_in,
                        "pilow_out": checklist_infos.pilow_out,
                        "pilowcs_in": checklist_infos.pilowcs_in,
                        "pilowcs_out": checklist_infos.pilowcs_out,
                        "toilet_tissue_in": checklist_infos.toilet_tissue_in,
                        "toilet_tissue_out": checklist_infos.toilet_tissue_out,
                        "remarks": checklist_infos.remarks,
                        "extra_work_done": checklist_infos.extra_work_done
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input[name$=attendant_name]").val("");
                        checklist_cnv.getallhkattendant("",$('div.dyn_content'));           
                        $("#addchecklist")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                });
            }
        });
    });
    
    //edit checklist submit form
    $("body").on("submit","#updatechecklist",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    uch_id : $("#updatechecklist input[name$=uch_id]"),
                    remarks: $("#updatechecklist input[name$=edit-remarks]"),
                    room_no: $("#updatechecklist select[name$=edit-room_no]"),
                    time_in: $("#updatechecklist input[name$=edit-time-in]"),
                    time_out: $("#updatechecklist input[name$=edit-time-out]"),
                    room_status: $("#updatechecklist select[name$=edit-room_status]"),
                    guest_no: $("#updatechecklist input[name$=edit-guest-no]"),
                    bt_in: $("#updatechecklist input[name$=edit-bt-in]"),
                    bt_out: $("#updatechecklist input[name$=edit-bt-out]"),
                    bm_in: $("#updatechecklist input[name$=edit-bm-in]"),
                    bm_out: $("#updatechecklist input[name$=edit-bm-out]"),
                    fit_in: $("#updatechecklist input[name$=edit-fit-in]"),
                    fit_out: $("#updatechecklist input[name$=edit-fit-out]"),
                    flat_in: $("#updatechecklist input[name$=edit-flat-in]"),
                    flat_out: $("#updatechecklist input[name$=edit-flat-out]"),
                    blanket_in: $("#updatechecklist input[name$=edit-blanket-in]"),
                    blanket_out: $("#updatechecklist input[name$=edit-blanket-out]"),
                    pilow_in: $("#updatechecklist input[name$=edit-pilow-in]"),
                    pilow_out: $("#updatechecklist input[name$=edit-pilow-out]"),
                    pilowcs_in: $("#updatechecklist input[name$=edit-pilowcs-in]"),
                    pilowcs_out: $("#updatechecklist input[name$=edit-pilowcs-out]"),
                    toilet_tissue_in: $("#updatechecklist input[name$=edit-toilet-tissue-in]"),
                    toilet_tissue_out: $("#updatechecklist input[name$=edit-toilet-tissue-out]"),
                    extra_work_done: $("#updatechecklist input[name$=edit-extra-work-done]")
                };
               
                if(update_info.remarks.val() != true) {
                $.customConfirm("Updated successfully, Click 'OK' to continue. ",function(response){
                                        if(response===true) {
                                            checklist_cnv.updatechecklist(update_info);
                                             $("div.search-box input[name$=attendant_name]").val("");
                                             checklist_cnv.getallhkattendant("",$('div.dyn_content'));
                                             $('div#chrupdate').slideUp();
                                        }
                                    });
                }
                else {
                    checklist_cnv.updatechecklist(update_info);
                }
               
            }
        });
    });
    
    //search all
    $("body").on("keyup","div.search-box1 input[name$=attendant_name]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallhkattendant($(this).val(),$('div.dyn_content'));
    });
     //search all attendant
    $("body").on("keyup","div.search-box1 input[name$=attendant_fname]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallattendant($(this).val(),$('div.dyn_content'));
    });
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
            
            $.post(base_url+"ops/checklist_form", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=prepared_by]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.prepared,function(i,val){
                   $("select[name$=prepared_by]")
                   .append($('<option></option>').val(val.hk_id).html(val.fname+ " "+val.lname));
               });
               $("select[name$=room_no]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.room_no, function(i,val) {
                   $("select[name$=room_no]")
                   .append($('<option></option>').val(val.room_number_id).html(val.room_no));
               });
               $("select[name$=room_status]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.room_status, function(i,val) {
                   $("select[name$=room_status]")
                   .append($('<option></option>').val(val.room_status_id).html(val.room_status_name));
               });
               
            });
            
            $('input[name$=time-in], input[name$=time-out]').timepicker({
		ampm: true
	     });
        }
    });
    return new stock_list;
}); 