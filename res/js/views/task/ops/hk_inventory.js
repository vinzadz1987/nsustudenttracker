define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/hk/hk_inventory.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getallmininventory: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/hk_inventory",{
                dir: "all_amin_inventory",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
        getallminbarinventory: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/hk_inventory",{
                dir: "all_minb_inventory",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         getalllineninventory: function(param, container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"ops/hk_inventory",{
                dir: "all_linen_inventory",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
         },
         updateamininv: function(update_info) {
            $.post(base_url+"ops/hk_inventory",{
                dir: "update_inv_amin",
                data: {
                    amin_id: update_info.amin_id.val(),
                    amen_item: update_info.amen_item.val(),
                    beg_bal: update_info.beg_bal.val(),
                    purchased: update_info.purchased.val(),
                    t_out: update_info.t_out.val(),
                    balance: update_info.balance.val(),
                    p_count: update_info.p_count.val(),
                    remarks: update_info.remarks.val(),
                    recon: update_info.recon.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallmininventory($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        updateminbinv: function(update_info) {
            $.post(base_url+"ops/hk_inventory",{
                dir: "update_inv_minb",
                data: {
                    mini_id: update_info.mini_id.val(),
                    minb_item: update_info.minb_item.val(),
                    minb_beg_bal: update_info.minb_beg_bal.val(),
                    minb_purchased: update_info.minb_purchased.val(),
                    minb_t_stock: update_info.minb_t_stock.val(),
                    minb_out: update_info.minb_out.val(),
                    minb_balance: update_info.minb_balance.val(),
                    minb_psy_count: update_info.minb_psy_count.val(),
                    minb_hotel_from: update_info.minb_hotel_from.val(),
                    minb_hotel_to: update_info.minb_hotel_to.val(),
                    minb_remarks: update_info.minb_remarks.val()
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getallminbarinventory($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        },
        
        updatelinennv: function(update_info) {
            $.post(base_url+"ops/hk_inventory",{
                dir: "update_inv_linen",
                data: {
                    linen_id: update_info.linen_id.val(),
                    linen_item: update_info.linen_item.val(),
                    linen_begbal: update_info.linen_beg_bal.val(),
                    linen_purchased: update_info.linen_purchased.val(),
                    linen_stock_room: update_info.linen_stock_room.val(),
                    linen_rooms: update_info.linen_rooms.val(),
                    linen_pul: update_info.linen_pul.val(),
                    linen_stotal: update_info.linen_stotal.val(),
                    linen_damaged: update_info.linen_damaged.val(),
                    linen_total: update_info.linen_total.val()
                   
                }
            },function(data){
                var rcv = $.parseJSON(data);
                if(rcv.result===true) {         
                    checklist_cnv.getalllineninventory($('div.dyn_content')); 
                } else {
                    $.customAlert('An error occurred. Please refresh the page and try again.');
                }
            });
        }
          
    };
    
     //format dec
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="amin-beg-bal") {
            $.decimalFormat($(this),e,0);
        }else{
            $.decimalFormat($(this),e,0);
        }
    });
    
    //initialize date picker
    $("body").on("click","input[name$=inv-from], input[name$=inv-to]", function(){
       $(this).datepicker({dateFormat: 'mm-dd-yy',changeYear: true, changeMonth: true, yearRange: '1940:+10'});
       $(this).datepicker({showOn: 'focus'}).focus();
    });
     
    //toggle all hk inventory  on <li> click
    $("body").on("click","div.fboption ul li", function(){
       $("div.form-hk-inventory").slideUp();
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class", "white-litegray");
       }else {
           $("div.fboption ul li").removeClass('active');
           switch ($(this).attr("title")) {
               case "Amenities Inventory":$("div#hk-inventory").slideDown(); 
                   checklist_cnv.getallmininventory("",$('div.dyn_content'));break;
               case "Mini Bar Inventory":$("div#hk-minb-inventory").slideDown(); 
                   checklist_cnv.getallminbarinventory("",$('div.dyn_content'));break;
               case "Linen Inventory":$("div#hk-linen-inventory").slideDown();
                   checklist_cnv.getalllineninventory("",$('div.dyn_content'));break;
               
           }
           $(this).addClass('active');
       }
    });
  
   //toggle amenities inventory date
   $("body").on("click","div.inv ul li", function(){
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","ch");
       }else {
           $("div.inv ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Add Inventory":$("div#inventory-add").slideDown();
                   checklist_cnv.getallmininventory("",$('div.dyn_content'));break;
               case "Show Inventory":$("div#show-amin-inv").slideDown();
                   checklist_cnv.getallmininventory("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }    
   });
   
   //toggle minibar inventory date
   $("body").on("click","div.minibar ul li", function(){
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","white-litegray");
       }else {
           $("div.minibar ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Add Mini Bar Inventory":$("div#minibaradd").slideDown();
                   checklist_cnv.getallminbarinventory("",$('div.dyn_content'));break;
               case "Show Mini Bar Inventory":$("div#show-minb-inv").slideDown();
                   checklist_cnv.getallminbarinventory("",$('div.dyn_content'));break;
           }
           $(this).addClass('active');
       }    
   });
   
    //toggle linen inventory date
   $("body").on("click","div.inv-linen ul li", function(){
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","white-litegray");
       }else {
           $("div.inv-linen ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Add Linen Inventory":$("div#linenadd").slideDown();
                   checklist_cnv.getalllineninventory("",$('div.dyn_content'));break;
               case "Show Linen Inventory":$("div#show-linen-inv").slideDown();
                   checklist_cnv.getalllineninventory("",$('div.dyn_content'));break;
               
           }
           $(this).addClass('active');
       }    
   });
   
   //toggle update inv aminities inventory
   $("body").on("click","div.inv-amin ul li", function(){
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","ch");
       }else {
           $("div.inv-amin ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Inventory":$("div#inventory-update").slideDown();
                   checklist_cnv.getallmininventory("",$('div.dyn_content'));break;
               
           }
           $(this).addClass('active');
       }    
   });
   
   //toggle update inv minibar inventory
   $("body").on("click","div.inv-minb ul li", function(){
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","white-litegray");
       }else {
           $("div.inv-minb ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Mini Bar Inventory":$("div#minibarupdate").slideDown();
                   checklist_cnv.getallminbarinventory("",$('div.dyn_content'));break;
               
           }
           $(this).addClass('active');
       }    
   });
   
   //toggle update inv linen inventory
   $("body").on("click","div.linen ul li", function(){
       $("div.hk-inventory").slideUp();
       $('div.dyn_content').empty();
       if($(this).hasClass("active")) {
           $(this).attr("class","ch");
       }else {
           $("div.linen ul li").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Linen Inventory":$("div.linen").slideDown();
                   checklist_cnv.getalllineninventory("",$('div.dyn_content'));break;
               
           }
           $(this).addClass('active');
       }    
   });
   
    //add inventory sched for amenities
    $("body").on("submit","#frm-hk-inventory",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    inv_from: $("input[name$=inv-from]").val(),
                    inv_to: $("input[name$=inv-to]").val(),
                    cat_id: $("input[name$=cat-id]").val()
                }
                $.post(base_url+"ops/hk_inventory",{
                    dir: "add_hk_inventory",
                    data: {
                        "inv_from": infos.inv_from,
                        "inv_to": infos.inv_to,
                        "cat_id": infos.cat_id
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=inv-date]").val("");
                        checklist_cnv.getallmininventory("",$('div.dyn_content'));           
                        $("#frm-hk-inventory")[0].reset();
                         $('div#showsuccess').fadeIn(1200);
                         $('div#showsuccess').fadeOut(1200);
                    } else {
                       $('div#showfailed').html("Inventory as of "+infos.inv_to+" is already exist.").fadeIn(1200);
                       $('div#showfailed').html("Inventory as of "+infos.inv_to+" is already exist.").fadeOut(5000);  
                    }
                });
            }
        });
    });
    
    //add inventory sched for minibar
    $("body").on("submit","#frm-hk-minb-inventory",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    minb_inv_from: $("input[name$=minb-inv-from]").val(),
                    minb_inv_to: $("input[name$=minb-inv-to]").val(),
                    minb_cat_id: $("input[name$=minb-cat-id]").val()
                }
                $.post(base_url+"ops/hk_inventory",{
                    dir: "add_hk_inventory",
                    data: {
                        "inv_from": infos.minb_inv_from,
                        "inv_to": infos.minb_inv_to,
                        "cat_id": infos.minb_cat_id
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=minb-date]").val("");
                        checklist_cnv.getallminbarinventory("",$('div.dyn_content'));           
                        $("#frm-hk-minb-inventory")[0].reset();
                         $('div#showsuccessminb').fadeIn(1200);
                         $('div#showsuccessminb').fadeOut(1200);
                    } else {
                       $('div#showfailedminb').html("Inventory as of "+infos.minb_inv_to+" is already exist.").fadeIn(1200);
                       $('div#showfailedminb').html("Inventory as of "+infos.minb_inv_to+" is already exist.").fadeOut(5000);  
                    }
                });
            }
        });
    });
    
    //add inventory sched for linen 
    $("body").on("submit","#frm-hk-linen-inventory",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $("div.err_msg").html(data.message)
                .facebox("div.err_msg",{show:true});
            } else {
                var infos = {
                    linen_inv_from: $("input[name$=linen-inv-from]").val(),
                    linen_inv_to: $("input[name$=linen-inv-to]").val(),
                    linen_cat_id: $("input[name$=linen-cat-id]").val()
                }
                $.post(base_url+"ops/hk_inventory",{
                    dir: "add_hk_inventory",
                    data: {
                        "inv_from": infos.linen_inv_from,
                        "inv_to": infos.linen_inv_to,
                        "cat_id": infos.linen_cat_id
                    }
                },function(data){
                    if(data==="added") {
                        $("div.search-box input1[name$=linendate]").val("");
                        checklist_cnv.getalllineninventory("",$('div.dyn_content'));           
                        $("#frm-hk-linen-inventory")[0].reset();
                         $('div#showsuccesslinen').fadeIn(1200);
                         $('div#showsuccesslinen').fadeOut(1200);
                    } else {
                       $('div#showfailedlinen').html("Inventory as of "+infos.linen_inv_to+" is already exist.").fadeIn(1200);
                       $('div#showfailedlinen').html("Inventory as of "+infos.linen_inv_to+" is already exist.").fadeOut(5000);  
                    }
                });
            }
        });
    });
    
     //add amenities inventory
    $("body").on("submit","#add-amin-inv",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Field is Empty.").fadeIn(1000);
                $('div.rq').html("A Required Field is Empty.").fadeOut(3000);  
           } else {
                var infos = {
                    amin_id: $("input[name$=amin-inv-id]").val(),
                    amen_item: $("select[name$=amin-item]").val(),
                    amen_beg_bal: $("input[name$=amin-beg-bal]").val(),
                    amen_purchased: $("input[name$=amin-purchased]").val(),
                    amen_t_out: $("input[name$=amin-t_out]").val(),
                    amen_bal: $("input[name$=amin-bal]").val(),
                    amen_p_count: $("input[name$=amin-p_count]").val(),
                    amen_remark: $("input[name$=amin-remark]").val(),
                    amen_recon : $("input[name$=amin-recon]").val()
                }
                $.post(base_url+"ops/hk_inventory",{
                    dir: "add_amin_inventory",
                    data: {
                        
                        "amin_id": infos.amin_id,
                        "amen_item": infos.amen_item,
                        "amen_beg_bal": infos.amen_beg_bal,
                        "amen_purchased": infos.amen_purchased,
                        "amen_t_out": infos.amen_t_out,
                        "amen_bal": infos.amen_bal,
                        "amen_p_count": infos.amen_p_count,
                        "amen_remark": infos.amen_remark,
                        "amen_recon": infos.amen_recon
                        
                    }
                },function(data){
                    if(data==="added") {    
                        $("#add-amin-inv")[0].reset();
                        $('div#showfailed').html("Inventory Added Successfully.").show(3000);
                        $('div#showfailed').html("Inventory Added Successfully.").hide(1000);  
                    
                    } else {
                         $('div.rq').html("The Inventory item that you selected already exist.").show(3000);
                         $('div.rq').html("The Inventory item that you selected already exist.").hide(1000);  
                     }
                });
            }
        });
    });
   
   //add minibar inventory
    $("body").on("submit","#minibarformadd",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Field is Empty.").fadeIn(1000);
                $('div.rq').html("A Required Field is Empty.").fadeOut(3000);  
            } else {
                var infos = {
                    inv_id: $("input[name$=minibar-inv-id]").val(),
                    minb_item: $("select[name$=minb-item]").val(),
                    minb_begbal: $("input[name$=minb-beg-bal]").val(),
                    minb_purchased: $("input[name$=minb-purchased]").val(),
                    minb_t_stock: $("input[name$=minb-t_stock]").val(),
                    minb_out: $("input[name$=minb-out]").val(),
                    minb_balance: $("input[name$=minb-bal]").val(),
                    minb_psy_count: $("input[name$=minb-psy_count]").val(),
                    hotel_from: $("select[name$=hotel-from]").val(),
                    hotel_to: $("select[name$=hotel-to]").val(),
                    minb_remarks: $("input[name$=minb-remarks]").val()
                }
                $.post(base_url+"ops/hk_inventory",{
                    dir: "add_minb_inventory",
                    data: {
                        
                        "inv_id": infos.inv_id,
                        "minb_item": infos.minb_item,
                        "minb_begbal": infos.minb_begbal,
                        "minb_purchased": infos.minb_purchased,
                        "minb_t_stock": infos.minb_t_stock,
                        "minb_out": infos.minb_out,
                        "minb_balance": infos.minb_balance,
                        "minb_psy_count": infos.minb_psy_count,
                        "hotel_from": infos.hotel_from,
                        "hotel_to": infos.hotel_to,
                        "minb_remarks": infos.minb_remarks
                       
                        
                    }
                },function(data){
                    if(data==="added") {    
                        $("#minibarformadd")[0].reset();
                        $('div.rq').html("Inventory Added Successfully.").show(3000);
                        $('div.rq').html("Inventory Added Successfully.").hide(1000);  
                    
                    } else {
                         $('div.rq').html("The Inventory item that you selected already exist.").show(3000);
                         $('div.rq').html("The Inventory item that you selected already exist.").hide(1000);  
                     }
                });
            }
        });
    });
   
   //add linen inventory
    $("body").on("submit","#linenformadd",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Field is Empty.").fadeIn(1000);
                $('div.rq').html("A Required Field is Empty.").fadeOut(3000);  
            } else {
                var infos = {
                    inv_id: $("input[name$=linen-inv-id]").val(),
                    linen_item: $("select[name$=linen-item]").val(),
                    linen_begbal: $("input[name$=linen-beg-bal]").val(),
                    linen_purchased: $("input[name$=linen-purchased]").val(),
                    linen_stock_room: $("input[name$=linen-stock-room]").val(),
                    linen_room: $("input[name$=linen-rooms]").val(),
                    linen_pck_upl: $("input[name$=linen-pck_laundry]").val(),
                    linen_s_total: $("input[name$=linen-subtotal]").val(),
                    linen_damaged: $("input[name$=linen-damaged]").val(),
                    linen_total: $("input[name$=linen-total]").val()
                }
                $.post(base_url+"ops/hk_inventory",{
                    dir: "add_linen_inventory",
                    data: {
                        
                        "inv_id": infos.inv_id,
                        "linen_item": infos.linen_item,
                        "linen_begbal": infos.linen_begbal,
                        "linen_purchased": infos.linen_purchased,
                        "linen_stock_room": infos.linen_stock_room,
                        "linen_rooms": infos.linen_room,
                        "linen_pck_upl": infos.linen_pck_upl,
                        "linen_s_total": infos.linen_s_total,
                        "linen_damaged": infos.linen_damaged,
                        "linen_total": infos.linen_total
                       
                        
                    }
                },function(data){
                    if(data==="added") {    
                        $("#linenformadd")[0].reset();
                        $('div.rq').html("Inventory Added Successfully.").show(3000);
                        $('div.rq').html("Inventory Added Successfully.").hide(1000);  
                    
                    } else {
                         $('div.rq').html("The Inventory item that you selected already exist.").show(3000);
                         $('div.rq').html("The Inventory item that you selected already exist.").hide(1000);  
                     }
                });
            }
        });
    });
   
    //initialize inventory
    $("body").on("click","li.add-amin-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "get_inv_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#inventory-add label.inv-from").html(data.amin_inv.inv_from).data('original',data.amin_inv.inv_from); 
            $("#inventory-add label.inv-to").html(data.amin_inv.inv_to).data('original',data.amin_inv.inv_to); 
            $("#add-amin-inv input[name$=amin-inv-id]").val(data.amin_inv.inv_id).data('original',data.amin_inv.inv_id);
       },"json");
    });
    
    //initialize minibar inventory
    $("body").on("click","li.add-minb-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "get_inv_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#minibaradd label.inv-from").html(data.amin_inv.inv_from).data('original',data.amin_inv.inv_from); 
            $("#minibaradd label.inv-to").html(data.amin_inv.inv_to).data('original',data.amin_inv.inv_to); 
            $("#minibarformadd input[name$=minibar-inv-id]").val(data.amin_inv.inv_id).data('original',data.amin_inv.inv_id);
       },"json");
    });
    
    //initialize linen inventory
    $("body").on("click","li.add-linen-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "get_inv_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#linenadd label.inv-from").html(data.amin_inv.inv_from).data('original',data.amin_inv.inv_from); 
            $("#linenadd label.inv-to").html(data.amin_inv.inv_to).data('original',data.amin_inv.inv_to); 
            $("#linenformadd input[name$=linen-inv-id]").val(data.amin_inv.inv_id).data('original',data.amin_inv.inv_id);
       },"json");
    });
    
    //initialize inventory
    $("body").on("click","li.update-tr-amin-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "get_inv_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#edit-amin-inv input[name$=edit-amin-inv-id]").val(data.amin_tr_inv.amin_id).data('original',data.amin_tr_inv.amin_id);
            $("#inventory-update label.inv-from").html(data.amin_tr_inv.amen_from).data('original',data.amin_tr_inv.amen_from);
            $("#inventory-update label.inv-to").html(data.amin_tr_inv.amen_to).data('original',data.amin_tr_inv.amen_to);
            $("#edit-amin-inv select[name$=edit-amin-item]").val(data.amin_tr_inv.item_id).data('original',data.amin_tr_inv.item_id);
            $("#edit-amin-inv input[name$=edit-amin-beg-bal]").val(data.amin_tr_inv.beg_bal).data('original', data.amin_tr_inv.beg_bal);
            $("#edit-amin-inv input[name$=edit-amin-purchased]").val(data.amin_tr_inv.purchased).data('original',data.amin_tr_inv.purchased);
            $("#edit-amin-inv input[name$=edit-amin-t_out]").val(data.amin_tr_inv.t_out).data('original',data.amin_tr_inv.t_out);
            $("#edit-amin-inv input[name$=edit-amin-bal]").val(data.amin_tr_inv.balance).data('original',data.amin_tr_inv.balance);
            $("#edit-amin-inv span.sum").html(data.amin_tr_inv.balance).data('original',data.amin_tr_inv.balance);
            $("#edit-amin-inv input[name$=edit-amin-p_count]").val(data.amin_tr_inv.psy_count).data('original',data.amin_tr_inv.psy_count);
            $("#edit-amin-inv input[name$=edit-amin-remark]").val(data.amin_tr_inv.remarks).data('original', data.amin_tr_inv.remarks);
            $("#edit-amin-inv input[name$=edit-amin-recon]").val(data.amin_tr_inv.reconcilation).data('original',data.amin_tr_inv.reconcilation);
       },"json");
    }); 
    
     //initialize minibar inventory
    $("body").on("click","li.update-tr-minb-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "get_inv_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#minibarformupdate input[name$=edit-minibar-inv-id]").val(data.minb_tr_inv.mini_id).data('original',data.minb_tr_inv.mini_id);
            $("#minibarupdate label.inv-from").html(data.minb_tr_inv.minb_from).data('original',data.minb_tr_inv.minb_from);
            $("#minibarupdate label.inv-to").html(data.minb_tr_inv.minb_to).data('original',data.minb_tr_inv.minb_to);
            $("#minibarformupdate select[name$=edit-minb-item]").val(data.minb_tr_inv.item_id).data('original',data.minb_tr_inv.item_id);
            $("#minibarformupdate input[name$=edit-minb-beg-bal]").val(data.minb_tr_inv.beg_bal).data('original',data.minb_tr_inv.beg_bal);
            $("#minibarformupdate input[name$=edit-minb-purchased]").val(data.minb_tr_inv.purchased).data('original',data.minb_tr_inv.purchased);
            $("#minibarformupdate input[name$=edit-minb-t_stock]").val(data.minb_tr_inv.t_stock).data('original',data.minb_tr_inv.t_stock);
            $("#minibarformupdate input[name$=edit-minb-out]").val(data.minb_tr_inv.out).data('original',data.minb_tr_inv.out);
            $("#minibarformupdate input[name$=edit-minb-bal]").val(data.minb_tr_inv.balance).data('original',data.minb_tr_inv.balance);
            $("#minibarformupdate input[name$=edit-minb-psy_count]").val(data.minb_tr_inv.psy_count).data('original',data.minb_tr_inv.psy_count);
            $("#minibarformupdate select[name$=edit-hotel-from]").val(data.minb_tr_inv.hotel_room_from).data('original',data.minb_tr_inv.hotel_room_from);
            $("#minibarformupdate select[name$=edit-hotel-to]").val(data.minb_tr_inv.hotel_room_to).data('original',data.minb_tr_inv.hotel_room_to);
            $("#minibarformupdate input[name$=edit-minb-remarks]").val(data.minb_tr_inv.remarks).data('original',data.minb_tr_inv.remarks);
           
        },"json");
    }); 
    
    //initialize linen inventory
    $("body").on("click","li.update-tr-linen-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "get_inv_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#linenformupdate input[name$=edit-linen-inv-id]").val(data.linen_tr_inv.linen_id).data('original',data.linen_tr_inv.linen_id);
            $("#linenupdate label.inv-from").html(data.linen_tr_inv.linen_from).data('original',data.linen_tr_inv.linen_from);
            $("#linenupdate label.inv-to").html(data.linen_tr_inv.linen_to).data('original',data.linen_tr_inv.linen_to); 
            $("#linenformupdate select[name$=edit-linen-item]").val(data.linen_tr_inv.item_id).data('original',data.linen_tr_inv.item_id);
            $("#linenformupdate input[name$=edit-linen-beg-bal]").val(data.linen_tr_inv.beg_bal).data('original',data.linen_tr_inv.beg_bal);
            $("#linenformupdate input[name$=edit-linen-purchased]").val(data.linen_tr_inv.purchased).data('original',data.linen_tr_inv.purchased);
            $("#linenformupdate input[name$=edit-linen-stock-room]").val(data.linen_tr_inv.stock_rooms).data('original',data.linen_tr_inv.stock_rooms);
            $("#linenformupdate input[name$=edit-linen-rooms]").val(data.linen_tr_inv.rooms).data('original',data.linen_tr_inv.rooms);
            $("#linenformupdate input[name$=edit-linen-pck_laundry]").val(data.linen_tr_inv.pul).data('original',data.linen_tr_inv.pul);
            $("#linenformupdate input[name$=edit-linen-subtotal]").val(data.linen_tr_inv.sub_total).data('original',data.linen_tr_inv.sub_total);
            $("#linenformupdate input[name$=edit-linen-damaged]").val(data.linen_tr_inv.damaged).data('original',data.linen_tr_inv.damaged);
            $("#linenformupdate input[name$=edit-linen-total]").val(data.linen_tr_inv.total).data('original',data.linen_tr_inv.total);
         
        },"json");
    }); 
    
    //edit aminities submit form
    $("body").on("submit","#edit-amin-inv",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    amin_id : $("#edit-amin-inv input[name$=edit-amin-inv-id]"),
                    amen_item: $("#edit-amin-inv select[name$=edit-amin-item]"),
                    beg_bal: $("#edit-amin-inv input[name$=edit-amin-beg-bal]"),
                    purchased: $("#edit-amin-inv input[name$=edit-amin-purchased]"),
                    t_out: $("#edit-amin-inv input[name$=edit-amin-t_out]"),
                    balance: $("#edit-amin-inv input[name$=edit-amin-bal]"),
                    p_count: $("#edit-amin-inv input[name$=edit-amin-p_count]"),
                    remarks: $("#edit-amin-inv input[name$=edit-amin-remark]"),
                    recon: $("#edit-amin-inv input[name$=edit-amin-recon]")
                };
               
                if(update_info.remarks.val() != true) {
                
                        checklist_cnv.updateamininv(update_info);
                        $('div.rq').html("Inventory Updated Successfully.").show(3000);
                        $('div.rq').html("Inventory Updated Successfully.").hide(1000);
                                      
                }
                else {
                    checklist_cnv.updateamininv(update_info);
                }
               
            }
        });
    });
    
    //edit minibar submit form
    $("body").on("submit","#minibarformupdate",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    mini_id : $("#minibarformupdate input[name$=edit-minibar-inv-id]"),
                    minb_item : $("#minibarformupdate select[name$=edit-minb-item]"),
                    minb_beg_bal : $("#minibarformupdate input[name$=edit-minb-beg-bal]"),
                    minb_purchased : $("#minibarformupdate input[name$=edit-minb-purchased]"),
                    minb_t_stock : $("#minibarformupdate input[name$=edit-minb-t_stock]"),
                    minb_out : $("#minibarformupdate input[name$=edit-minb-out]"),
                    minb_balance : $("#minibarformupdate input[name$=edit-minb-bal]"),
                    minb_psy_count : $("#minibarformupdate input[name$=edit-minb-psy_count]"),
                    minb_hotel_from : $("#minibarformupdate select[name$=edit-hotel-from]"),
                    minb_hotel_to : $("#minibarformupdate select[name$=edit-hotel-to]"),
                    minb_remarks: $("#minibarformupdate input[name$=edit-minb-remarks]")
                    
                };
               
                if(update_info.minb_remarks.val() != true) {
                
                        checklist_cnv.updateminbinv(update_info);
                        $('div.rq').html("Inventory Updated Successfully.").show(3000);
                        $('div.rq').html("Inventory Updated Successfully.").hide(1000);
                        $('div.show-minb-inventory').hide();
                                      
                }
                else {
                    checklist_cnv.updateminbinv(update_info);
                }
               
            }
        });
    });
    
    //edit linen submit form
    $("body").on("submit","#linenformupdate",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data) {
            if(data.result===false) {
                $.customAlert(data.message);
            } else {
                var update_info = {
                    linen_id : $("#linenformupdate input[name$=edit-linen-inv-id]"),
                    linen_item : $("#linenformupdate select[name$=edit-linen-item]"),
                    linen_beg_bal : $("#linenformupdate input[name$=edit-linen-beg-bal]"),
                    linen_purchased : $("#linenformupdate input[name$=edit-linen-purchased]"),
                    linen_stock_room : $("#linenformupdate input[name$=edit-linen-stock-room]"),
                    linen_rooms : $("#linenformupdate input[name$=edit-linen-rooms]"),
                    linen_pul: $("#linenformupdate input[name$=edit-linen-pck_laundry]"),
                    linen_stotal : $("#linenformupdate input[name$=edit-linen-subtotal]"),
                    linen_total: $("#linenformupdate input[name$=edit-linen-total]"),
                    linen_damaged: $("#linenformupdate input[name$=edit-linen-damaged]")
                    
                };
               
                
                
                        checklist_cnv.updatelinennv(update_info);
                        $('div.linen').hide();
                        $('div.rq').html("Inventory Updated Successfully.").show(3000);
                        $('div.rq').html("Inventory Updated Successfully.").hide(1000);
                        $('div.show-linen-inventory').hide();
                                      
               
               
            }
        });
    });
    
    //show amenities inventory
    $("body").on("click","li.show-amin-inventory", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "all_tr_amin_inv",
            bid: st_no
        }, function(data){
            $('div.show-amin-inventory').html(data.res);
        },"json");
    });
    
    //show minibar inventory
    $("body").on("click","li.show-minb-inventory", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "all_tr_minb_inv",
            bid: st_no
        }, function(data){
            $('div.show-minb-inventory').html(data.res);
        },"json");
    });
    
    //show linen inventory
    $("body").on("click","li.show-linen-inventory", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "all_tr_linen_inv",
            bid: st_no
        }, function(data){
            $('div.show-linen-inventory').html(data.res);
        },"json");
    });
    
    //delete aminities date
    $("body").on("click", "li.delete-amin-date", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "delete_amen_date",
            pid: st_no 
        }, function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json")
    });
    
    //delete minibar date
    $("body").on("click", "li.delete-minb-date", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "delete_minb_date",
            pid: st_no 
        }, function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json")
    });
    
    //delete linen date
    $("body").on("click", "li.delete-linen-date", function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "delete_linen_date",
            pid: st_no 
        }, function(data){
            $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
        },"json")
    });
    
    //delete aminities inv report
     $("body").on("click","li.delete-tr-amin-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "delete_amin_inv",
            pid: st_no
        },function(data){
        $('div#show-amin-inv').html(data.res).slideUp();
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    
    //delete minibar inv report
     $("body").on("click","li.delete-tr-minb-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "delete_minb_inv",
            pid: st_no
        },function(data){
        $('div#show-minb-inv').html(data.res).slideUp();    
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    
    //delete linen inv report
     $("body").on("click","li.delete-tr-linen-inventory",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/hk_inventory",{
            dir: "delete_linen_inv",
            pid: st_no
        },function(data){
        $('div#show-linen-inv').html(data.res).slideUp();
        $('div.dyn_content').html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));    
        },"json");
    });
    
    //search all aminities
    $("body").on("keyup","div.search-box1 input[name$=date]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallmininventory($(this).val(),$('div.dyn_content'));
    });
    //search all minibar
    $("body").on("keyup","div.search-box1 input[name$=minbdate]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallminbarinventory($(this).val(),$('div.dyn_content'));
    });
     //search all linen
    $("body").on("keyup","div.search-box1 input[name$=linendate]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getalllineninventory($(this).val(),$('div.dyn_content'));
    });
   //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
    
            $('div.contents').html(stocksList);
            
            $.post(base_url+"ops/hk_inventory", {
                dir: 'init'
            }, function(data){
               var rcv = $.parseJSON(data);
               $("select[name$=amin-item], select[name$=edit-amin-item]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.amin_items,function(i,val){
                   $("select[name$=amin-item], select[name$=edit-amin-item]")
                   .append($('<option></option>').val(val.item_id).html(val.item_desc+" "+val.price));
               });
               $("select[name$=minb-item], select[name$=edit-minb-item]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.minb_items,function(i,val){
                   $("select[name$=minb-item], select[name$=edit-minb-item]")
                   .append($('<option></option>').val(val.item_id).html(val.item_desc+" "+val.price));
               });
               
               $("select[name$=linen-item], select[name$=edit-linen-item]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.linen_items,function(i,val){
                   $("select[name$=linen-item], select[name$=edit-linen-item]")
                   .append($('<option></option>').val(val.item_id).html(val.item_desc+" "+val.price));
               });
               
               $("select[name$=hotel-from], select[name$=edit-hotel-from]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.rooms, function(i,val){
                   $("select[name$=hotel-from], select[name$=edit-hotel-from]")
                   .append($('<option></option>').val(val.room_no).html(val.room_no));
               });
               
               $("select[name$=hotel-to], select[name$=edit-hotel-to]").empty()
               .append($('<option></option>').val("").html(""));
               $.each(rcv.rooms, function(i,val){
                   $("select[name$=hotel-to], select[name$=edit-hotel-to]")
                   .append($('<option></option>').val(val.room_no).html(val.room_no));
               });
               
            });
        }
    });
    return new stock_list;
}); 