define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/ops/lp/lp_onduty_dsales.html'
],function($,_,Backbone,base_url,stocksList){
    var checklist_cnv = {
         getallitems: function(param, container) {
            $.post(base_url+"ops/lp_onduty_dsales",{
                dir: "all_items",
                like: param
            },function(data){
                container.html(data.res).append($('<div class="pagination to-left shadowbox"></div>').html(data.pagination));
            },"json");
         },
         getallsales: function(container) {
            $.post(base_url+"ops/lp_onduty_dsales",{
                dir: "all_sales"
            },function(data){
                container.html(data.res);
            },"json");
         }
        
    };
    
   
    
    //delete items
     $("body").on("click","label.cancel-item",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_onduty_dsales",{
            dir: "delete_items",
            pid: st_no
        },function(data){
            $('div.dsales').html(data.res);
        },"json");
    });
    
   
    
  
 //toggle physical inv
   $("body").on("click","div.item", function(){
       
       if($(this).hasClass("active")) {
           $(this).attr("class","i");
       }else {
           $("div.item").removeClass('active');
           switch($(this).attr("title")) {
               case "Item Name":$("").slideDown(5);
           }
           $(this).addClass('active');
       }
   });
   //toggle update checklist item
   $("body").on("click","a.updatepsyinv", function(){
       if($(this).hasClass("active")) {
           $(this).attr("class","ui");
       }else {
           $("a.updatepsyinv").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Psy Inv":$("div#psy-inv-update").slideDown();
                   $('div.updatepys').show(5);
                   
           }
           $(this).addClass('active');
       }
   });
    //add item
    $("body").on("click","div.i",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Filled is Empty.").show(3000);
                $('div.rq').html("A Required Filled is Empty").hide(1000);  
       } else {
                var infos = {
                    item: $("form#additem input[name$=item]").val(),
                    price: $("form#additem input[name$=price]").val(),
                    total_sales: $("form#additem input[name$=isales]").val()
                }
                $.post(base_url+"ops/lp_onduty_dsales",{
                    dir: "add_item_sales",
                    data: {
                        "item": infos.item,
                        "price": infos.price,
                        "total_sales": infos.total_sales
                    }
                },function(data){
                    if(data==="added") { 
                        
                         checklist_cnv.getallsales($('div.dsales'));
                         checklist_cnv.getallitems("",$('div.dyn_content'));
                         
                    } else {
                        $('div.rq').html("The inventory "+ infos.item + " is already exist.").show(3000);
                        $('div.rq').html("The inventory "+ infos.item + " is already exist.").hide(3000);
                        
                    }
                });
            }
        });
    });
    
   
   
   //delete items
     $("body").on("click","a.item-delete",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_onduty_dsales",{
            dir: "delete_items",
            pid: st_no
        },function(data){
             $('div.item-all-show').html(data.res);
        },"json");
    });
   
    //initialize update pys inv
    $("body").on("mouseover","div.item",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_onduty_dsales",{
            dir: "get_details",
            bid: st_no 
        },function(data){   
            $('div.dyn_content').html(data.res); 
            $("#additem input[name$=item-id]").val(data.item.id).data('original',data.item.id);
            $("#additem input[name$=item]").val(data.item.description).data('original',data.item.description);
            $("#additem input[name$=price]").val(data.item.price).data('original',data.item.price);
            $("#additem input[name$=isales]").val(data.item.price).data('original',data.item.price);
            $("#additem label.item").val(data.item.description).html(data.item.description);
            $("#additem label.price").val(data.item.price).html(data.item.price);
        },"json");
    });
  
    $("body").on("click","button.view-sales-or", function(){
        var sales_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"ops/lp_onduty_dsales", {
            dir: "get_details",
            bid: sales_no
        }, function(data) {
            $("#OR-view label.pass-sales-id").val(data.dsales.dsales_id).html(data.dsales_id);
            $("#OR-view label.sales-date").val(data.dsales.date_added).html(data.dsales.date_added);
            $("#OR-view label.total-sum").val(data.dsales.ts).html(data.dsales.ts);
            $(this).facebox("div.view-or",{show:true});
        },"json");
    });
    
  /*  $("body").on("click","button[name$=print_or]", function() {
        $("div.OR-view").printElement({
            pageTitle: "Larry's Place official receipt",
            overrideElementCSS:['./res/css/printbox.css'],
            printBodyOptins
        })
    })
  */
    //close update pyd
     $("body").on("click","div.additem",function(){
        
             $('div.additem').hide(1000);
       
    });
    
    //search all date
    $("body").on("keyup","div.search-box1 input[name$=desc]",function(){
        $('div.dyn_content').html($('<div class="loading-style1"></div>'));
        checklist_cnv.getallitems($(this).val(),$('div.dyn_content'));
    });
   
    
  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.dyn_content').html(rcv.res).append($('<div class="pagination to-left shadowbox"></div>').html(rcv.pagination));
        });
    });
  
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            checklist_cnv.getallitems("",$('div.dyn_content'));
            checklist_cnv.getallsales($('div.dsales'));
            
        }
    });
    return new stock_list;
}); 