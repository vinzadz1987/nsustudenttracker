define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/task/hr/employees_list.html',
],function($,_,Backbone,base_url,taskView,empList) {
    var employees_list_cnv = {
        employee: null,
        work_exp: 0,
        retrieveEmpList: function(container,params) {  
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"hr/employees_list",{
                dir: "retrieve_employees",
                data: params
            },function(data){
                container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
            },"json");
        }
    };
    //initialize datepickers
    $("body").on("click","input[name$=birthdate], input[name$=imm_date_issued], input[name$=imm_expiration_date], input[name$=imm_review_date]",function(){
        $(this).datepicker({ dateFormat: 'yy-mm-dd', changeYear: true, changeMonth: true, yearRange: '1940:+10' });
        $(this).datepicker({showOn: 'focus'}).focus();
    });
    //hover employee; show options
    $("body").on("hover","div.employees_container div.emp_row",function(e){
        if(e.type=="mouseenter"){
            $(this).children("div.emp_options").stop(true,true).slideDown();
        }
        else if(e.type=="mouseleave") {
            $(this).children("div.emp_options").stop(true,true).slideUp();
        }
    });
    //edit/view employee personal details
    $("body").on("click","div.employees_container div.edit-emp-personal-details-btn",function(){
        employees_list_cnv.employee = $(this).parent().parent().attr("id").split("_",2)[1];
        $.post(base_url+"hr/employees_list",{
            dir: "retrieve_emp_personal_details",
            eid: employees_list_cnv.employee
        },function(data){
            $("div#emp_personal_details input[name$=id_number]").val(data.id_number);
            $("div#emp_personal_details input[name$=lastname]").val(data.lname);
            $("div#emp_personal_details input[name$=firstname]").val(data.fname);
            $("div#emp_personal_details input[name$=middlename]").val(data.mname);
            $("div#emp_personal_details input[name$=nickname]").val(data.nickname);
            $("div#emp_personal_details input[name$=status]").val(data.status);
            $("div#emp_personal_details input[name$=nationality]").val(data.nationality);
            $("div#emp_personal_details input[name$=birthdate]").val(data.birthdate);
            switch (data.gender) {
                case "Male": $("div#emp_personal_details input:radio[name$=gender]")
                    .filter('[value=Male]').attr("checked",true); break;
                case "Female": $("div#emp_personal_details input:radio[name$=gender]")
                    .filter('[value=Female]').attr("checked",true); break;
                default: $("div#emp_personal_details input:radio[name$=gender]").attr("checked",false);
            }
            $("div#emp_personal_details input[name$=sss_number]").val(data.sss);
            $("div#emp_personal_details input[name$=philhealth_number]").val(data.phic);
            $("div#emp_personal_details input[name$=tin_number]").val(data.tin);
            $("div#emp_personal_details input[name$=hdmf_number]").val(data.hdmf);
            $("div#emp_personal_details input[name$=immigration]").val(data.immigration);
            $("div#emp_personal_details select[name$=imm_type]").val(data.imm_type);
            $("div#emp_personal_details input[name$=imm_status]").val(data.imm_status);
            $("div#emp_personal_details input[name$=imm_citizenship]").val(data.imm_citizenship);
            $("div#emp_personal_details input[name$=imm_date_issued]").val(data.imm_date_issued);
            $("div#emp_personal_details input[name$=imm_expiration_date]").val(data.imm_expiration_date);
            $("div#emp_personal_details input[name$=imm_review_date]").val(data.imm_review_date);
            $(this).litebox("div#emp_personal_details",{show: true});
        },"json");
    });
    //update employee personal details
    $("body").on("submit","form#emp_personal_details_frm",function(e){
        e.preventDefault();
        $.post(base_url+"hr/employees_list",{
            dir: "update_employee_personal_details",
            eid: employees_list_cnv.employee,
            data: {
                id_number: $("div#emp_personal_details input[name$=id_number]").val(),
                lname: $("div#emp_personal_details input[name$=lastname]").val(),
                fname: $("div#emp_personal_details input[name$=firstname]").val(),
                mname: $("div#emp_personal_details input[name$=middlename]").val(),
                nickname: $("div#emp_personal_details input[name$=nickname]").val(),
                status: $("div#emp_personal_details input[name$=status]").val(),
                nationality: $("div#emp_personal_details input[name$=nationality]").val(),
                birthdate: $("div#emp_personal_details input[name$=birthdate]").val(),
                gender: $("div#emp_personal_details input:radio[name$=gender]").val(),
                sss: $("div#emp_personal_details input[name$=sss_number]").val(),
                phic: $("div#emp_personal_details input[name$=philhealth_number]").val(),
                tin: $("div#emp_personal_details input[name$=tin_number]").val(),
                hdmf: $("div#emp_personal_details input[name$=hdmf_number]").val(),
                immigration: $("div#emp_personal_details input[name$=immigration]").val(),
                imm_type: $("div#emp_personal_details select[name$=imm_type]").val(),
                imm_status: $("div#emp_personal_details input[name$=imm_status]").val(),
                imm_citizenship: $("div#emp_personal_details input[name$=imm_citizenship]").val(),
                imm_date_issued: $("div#emp_personal_details input[name$=imm_date_issued]").val(),
                imm_expiration_date: $("div#emp_personal_details input[name$=imm_expiration_date]").val(),
                imm_review_date: $("div#emp_personal_details input[name$=imm_review_date]").val()
            }
        },function(data){
            if(data.result===true) {
                $.customAlert("Employee information updated.");
            } else {
                $.customAlert("An error occurred. Please refresh the page and try again.");
            }
        },"json");
    });
    //edit/view employee contact details
    $("body").on("click","div.employees_container div.edit-emp-contact-details-btn",function(){
        employees_list_cnv.employee = $(this).parent().parent().attr("id").split("_",2)[1];
        $.post(base_url+"hr/employees_list",{
            dir: "retrieve_emp_contact_details",
            eid: employees_list_cnv.employee
        },function(data){
            $("div#emp_contact_details input[name$=street]").val(data.street);
            $("div#emp_contact_details input[name$=city]").val(data.city);
            $("div#emp_contact_details input[name$=province]").val(data.province);
            $("div#emp_contact_details input[name$=country]").val(data.country);
            $("div#emp_contact_details input[name$=zip_code]").val(data.zip_code);
            $("div#emp_contact_details input[name$=email]").val(data.email);
            $("div#emp_contact_details input[name$=telephone]").val(data.telephone);
            $("div#emp_contact_details input[name$=mobile]").val(data.mobile);
            $("div#emp_contact_details input[name$=ec_name]").val(data.ec_name);
            $("div#emp_contact_details input[name$=ec_relationship]").val(data.ec_relationship);
            $("div#emp_contact_details input[name$=ec_telephone]").val(data.ec_telephone);
            $("div#emp_contact_details input[name$=ec_mobile]").val(data.ec_mobile);
            $("div#emp_contact_details input[name$=ec_address]").val(data.ec_address);
            $(this).litebox("div#emp_contact_details",{show: true});
        },"json");
    });
    //update employee contact details
    $("body").on("submit","form#emp_contact_details_frm",function(e){
        e.preventDefault();
        $.post(base_url+"hr/employees_list",{
            dir: "update_employee_contact_details",
            eid: employees_list_cnv.employee,
            data: {
                street: $("div#emp_contact_details input[name$=street]").val(),
                city: $("div#emp_contact_details input[name$=city]").val(),
                province: $("div#emp_contact_details input[name$=province]").val(),
                country: $("div#emp_contact_details input[name$=country]").val(),
                zip_code: $("div#emp_contact_details input[name$=zip_code]").val(),
                email: $("div#emp_contact_details input[name$=email]").val(),
                telephone: $("div#emp_contact_details input[name$=telephone]").val(),
                mobile: $("div#emp_contact_details input[name$=mobile]").val(),
                ec_name: $("div#emp_contact_details input[name$=ec_name]").val(),
                ec_relationship: $("div#emp_contact_details input[name$=ec_relationship]").val(),
                ec_telephone: $("div#emp_contact_details input[name$=ec_telephone]").val(),
                ec_mobile: $("div#emp_contact_details input[name$=ec_mobile]").val(),
                ec_address: $("div#emp_contact_details input[name$=ec_address]").val()
            }
        },function(data){
            if(data.result===true) {
                $.customAlert("Employee information updated.");
            } else {
                $.customAlert("An error occurred. Please refresh the page and try again.");
            }
        },"json");
    });
    //edit/view employee background details
    $("body").on("click","div.employees_container div.edit-emp-background-btn",function(){
        var elem = $(this);
        employees_list_cnv.employee = $(this).parent().parent().attr("id").split("_",2)[1];
        employees_list_cnv.work_exp = 0;
        $.post(base_url+"hr/employees_list",{
            dir: "retrieve_emp_work_details",
            eid: employees_list_cnv.employee
        },function(data){
            $("div#emp_background_details").html(data.res);
            $.each($(data.res).find("input.entry_covered-from, input.entry_covered-to"),function(i,elem){
                $("body").on("click","input[name$="+$(elem).attr("name")+"]",function(){
                    $(this).datepicker({ dateFormat: 'yy-mm-dd', changeYear: true, changeMonth: true});
                    $(this).datepicker({showOn: 'focus'}).focus();
                });
            });
            elem.litebox("div#emp_background_details",{show: true});
        },"json");
    });
    //add employment background details
    $("body").on("click","form#emp_background_details_frm input:button[name$=add_experience]",function(){
        var copy = $("div.emp_work_exp").clone()[0];
        employees_list_cnv.work_exp += 1;
        $("div.emp_work_exp").last().after(
        $(copy)).after($('<div class="clr"></div>'));
        $(copy).attr("id","new-entry_"+employees_list_cnv.work_exp);
        $(copy).find("input").attr("value","");
        $(copy).find("input.entry_company").attr("name","new-work-company_"+employees_list_cnv.work_exp);
        $(copy).find("input.entry_position").attr("name","new-work-position_"+employees_list_cnv.work_exp);
        $(copy).find("input.entry_covered-from").attr("name","new-work-covered-from_"+employees_list_cnv.work_exp);
        $(copy).find("input.entry_covered-to").attr("name","new-work-covered-to_"+employees_list_cnv.work_exp);
    });
    //update/add employee background details
    $("body").on("submit","form#emp_background_details_frm",function(e){
        e.preventDefault();
        var updates = {}; var new_entry = {};
        $.each($("div#emp_background_details div.emp_work_exp"),function(i,elem){
            if($(elem).attr("id").search("new-entry")<0) {
                updates[i] = {
                    "exp_no": $(elem).attr("id").split("_",2)[1],
                    "company": $(elem).find("input.entry_company").val(),
                    "position": $(elem).find("input.entry_position").val(),
                    "covered-from": $(elem).find("input.entry_covered-from").val(),
                    "covered-to": $(elem).find("input.entry_covered-to").val()
                };
            } else {
                new_entry[$(elem).attr("id").split("_",2)[1]] = {
                    "company": $(elem).find("input.entry_company").val(),
                    "position": $(elem).find("input.entry_position").val(),
                    "covered-from": $(elem).find("input.entry_covered-from").val(),
                    "covered-to": $(elem).find("input.entry_covered-to").val()
                }
            }
        });
        $.post(base_url+"hr/employees_list",{
            dir: "employee_work_experience",
            eid: employees_list_cnv.employee,
            data: {
                old_ones: (updates.hasOwnProperty(0)) ? updates:null,
                new_ones: (new_entry.hasOwnProperty(1)) ? new_entry:null
            }
        },function(data){
            $("div#emp_background_details").html(data.res);
            $.customAlert("Data Sent.");
        },"json");
    });
    //hover to enable delete
    $("body").on("hover","div.emp_work_exp",function(e){
        if(e.type=="mouseenter"){
            $(this).append($('<div class="sprite-close delete-work-exp-btn to-right"></div>')
        .css({"margin-top":"-40px"}));
        }
        else if(e.type=="mouseleave") {
            $(this).children("div.delete-work-exp-btn").remove();
        }
    });
    //delete work experience entry
    $("body").on("click","div.delete-work-exp-btn",function(){
        if($(this).parent().attr("id").search("new-entry")<0) {
            var exp_no = $(this).parent().attr("id").split("_",2)[1];
            $.post(base_url+"hr/employees_list",{
                dir: "remove_work_experience",
                eid: employees_list_cnv.employee,
                exp_no: exp_no
            },function(data){
                $("div#emp_background_details").html(data.res);
            },"json");
        } else {
            if($(this).parent().attr("id") == "new-entry_1") $(this).parent().remove();
        }
    });
    //search mod
    $("body").on("change","div.employees_list select[name$=srch_department]",function(){
        var params = {
            dept: $(this).val(),
            srch: $("div.employees_list input[name$=srch_employee]").val()
        };
        employees_list_cnv.retrieveEmpList($('div.dyn_content'),params);
    });
    $("body").on("keyup","div.employees_list input[name$=srch_employee]",function(){
        $('div.dyn_content').html('<div class="loading-style1"></div>');
        var params = {
            dept: $("div.employees_list select[name$=srch_department]").val(),
            srch: $(this).val()
        };
        employees_list_cnv.retrieveEmpList($('div.dyn_content'),params);
    });
    var employees_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function(){
        },
        events: {},
        render: function() {
            $('div.contents').html(empList);
            $.post(base_url+"hr/employees_list",{
                dir: "initialize_department_select"
            },function(data){
                $("div.employees_list select[name$=srch_department]").empty()
                .append($('<option></option>').val("").html(""));
                $.each(data.departments,function(i,val){
                    $("div.employees_list select[name$=srch_department]")
                    .append($('<option></option>').val(val.deptd_id).html(val.dept_name));
                });
            },"json");
            employees_list_cnv.retrieveEmpList($('div.dyn_content'));
        }
    });
    
    return new employees_list;
});