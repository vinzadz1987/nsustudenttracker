    define([
    'jquery',
    'underscore',
    'backbone',
    'jQ-plugins/ckeditor/ckeditor',
    'jQ-plugins/ckeditor/adapters/jquery',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/jquery.Oaccordion',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hr/forms/main-view.html',
	'text!tpl/task/hr/forms/leave-list/main-view.html',
	'text!tpl/task/hr/forms/srs-list/main-view.html',
	'text!tpl/task/hr/forms/prs-list/main-view.html',
	'text!tpl/task/hr/forms/petty-cash-list/main-view.html'
], function(
	$, _, Backbone, ckeditor, ckeditorJqueryAdapter, baseURL, jsHelper, lbox, Oaccordion,
	taskView, taskMenu, mainView, leaveListMain, srsListMain, prsListMain, pettyCashListMain
) {
	
	var collect_task = Backbone.Model.extend({});
	var collect_emp_leaves = Backbone.Model.extend({});
	
	var SearchedItems = Backbone.Model.extend({});
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	collect_emp_leaves.prototype.url = baseURL+"hr/forms/retrieve/leave-list";
	var collectEmpLeaves = new collect_emp_leaves;
	
	var FormPaginationData = Backbone.Model.extend({});
	FormPaginationData.prototype.url = "";
	var formPaginationData = new FormPaginationData;
	
	// click event for srs pagination
	$('body').delegate('.hsrsl-pagination > a', 'click', function(e) {
		e.preventDefault();
		var $url = $(this).attr('href');
		formPaginationData.url = $url;
		formPaginationData.fetch({ success: function() {
				//srsListMain
				$('.hrlfc').html(_.template(srsListMain, $.extend({}, formPaginationData.attributes, {_:_} )));
				$('.oaccordion-list').Oaccordion();
		}});
	});

	// click event for leave list pagination
	$('body').delegate('.hll-pagination > a', 'click', function(e) {
		e.preventDefault();
		var $url = $(this).attr('href');
		formPaginationData.url = $url;
		formPaginationData.fetch({ success: function() {
				//srsListMain
				$('.hrlfc').html(_.template(leaveListMain, $.extend({}, formPaginationData.attributes, {_:_} )));
				$('.oaccordion-list').Oaccordion();
			}
		});
	});
	
	// click event for PRS list pagination
	$('body').delegate('.hprsl-pagination > a', 'click', function(e) {
		e.preventDefault();
		var $url = $(this).attr('href');
		formPaginationData.url = $url;
		formPaginationData.fetch({ success: function() {
				//srsListMain
				$('.hrlfc').html(_.template(prsListMain, $.extend({}, formPaginationData.attributes, {_:_} )));
				$('.oaccordion-list').Oaccordion();
			}
		});
	});
	
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click .hotel-RT a" : "ClickedFormTab",
			"click #leave-lists" : "LeaveList",
			"click #srs-lists" : "SrsList",
			"click #prs-lists" : "PrsList",
			"click #petty-cash-lists" : "PettyCashList",
			
			"click .setLeaveStatus" : "setLeaveStatus",
			"click .setPrsStatus" : "setPrsStatus",
			
			
			"keyup #hvbdsearchI" : "hvbdsearchI",
			"change .hvbdsort" : "hvbdsort", //determine if the search/select box value was changed
			"click .hvbdsearch" : "hvbdsearch"
		},
		hvbdsearchI: function(evt) {
			if( $(evt.currentTarget).prev().val() === "name" ) {
				
				SearchedItems.prototype.url = baseURL+"hr/forms/aml/"+$(evt.currentTarget).next().attr('name')+"/"+$(evt.currentTarget).val();
				var searchedItems = new SearchedItems;
				
				Backbone.emulateHTTP = true;
				Backbone.emulateJSON = true;
				
				searchedItems.fetch({ success: function() {
						
					}
				});
				
			}
		},
		hvbdsearch: function(evt) {
			var $this = $(evt.currentTarget);
			
			var $searchby = $this.parent().find('.hvbdsort').val();
			var $searchVal = $this.parent().find('#hvbdsearchI').val();
			
			if( ($searchby === "date") && ($searchVal !== "") && !( /^\d{2}?\/\d{2}?\/\d{4}?$/.test($searchVal) )) {
				//console.log('invalid date');
			} else if( $searchVal !== "" ) {
				//console.log($searchby+"/"+$searchVal);
			}
			
		},
		hvbdsort: function(evt) {
			//console.log($(evt.currentTarget).val());
			( $(evt.currentTarget).val() === "date" ) ?
				$(".h-viewbdN span").show() : $(".h-viewbdN span").hide();
		},
		setPrsStatus: function(evt) {
			// set the leave status
			var $this = $(evt.currentTarget);
			var $id = $this.attr('id'), $status = $this.prev().val(); 
			
			//if not empty status
			if( $status !== "" ) {
				
				var SetPrsStatus = Backbone.Model.extend({});
				SetPrsStatus.prototype.url = baseURL+"hr/forms/set_status/prs";
				
				var setPrsStatus = new SetPrsStatus;
				setPrsStatus.set({id: $id, status: $status});
				
				Backbone.emulateHTTP = true; //fake the PUT method/request to POST
				Backbone.emulateJSON = true; //remove the JSON mime type
				
				setPrsStatus.save({}, { success: function() {
						Backbone.history.loadUrl();
					}
				});
			}
		},
		setLeaveStatus: function(evt) {
			// set the leave status
			var $this = $(evt.currentTarget);
			var $id = $this.attr('id'), $status = $this.prev().val(); 
			
			//if not empty status
			if( $status !== "" ) {
				
				var SetLeaveStatus = Backbone.Model.extend({});
				SetLeaveStatus.prototype.url = baseURL+"hr/forms/set_status/leave";
				
				var setLeaveStatus = new SetLeaveStatus;
				setLeaveStatus.set({id: $id, status: $status});
				
				Backbone.emulateHTTP = true; //fake the PUT method/request to POST
				Backbone.emulateJSON = true; //remove the JSON mime type
				
				setLeaveStatus.save({}, { success: function() {
						Backbone.history.loadUrl();
					}
				});
			}
		},
		ClickedFormTab: function(evt) {
			$this = $(evt.currentTarget);
			$('.hotel-RT a').removeClass('hotel-roomtype-active');
			//show it up
			$this.addClass('hotel-roomtype-active');
		},
		LeaveList: function(evt) {
			//change the url	
			collectEmpLeaves.url = baseURL+"hr/forms/retrieve/leave-list";
			
			collectEmpLeaves.fetch({ success: function() {
					$('.hrlfc').html(_.template(leaveListMain, $.extend({}, collectEmpLeaves.attributes, {_:_, jsHelper: jsHelper} ))); //leave-list main
					$('.oaccordion-list').Oaccordion();
					$('.hvbdsearch').attr('name', 'leave-list');
				}
			});
		},
		SrsList: function(evt) {
			collectEmpLeaves.url = baseURL+"hr/forms/retrieve/srs-list";
			collectEmpLeaves.fetch({ success: function() {
					//$('.hrlfc').html(srsListMain);
					$('.hrlfc').html(_.template(srsListMain, $.extend({}, collectEmpLeaves.attributes, {_:_, jsHelper: jsHelper} ))); //leave-list main
					$('.oaccordion-list').Oaccordion();
					$('.hvbdsearch').attr('name', 'srs-list');
				}
			});
		},
		PrsList: function(evt) {
			//$('.hrlfc').html(prsListMain);
			collectEmpLeaves.url = baseURL+"hr/forms/retrieve/prs-list";
			collectEmpLeaves.fetch({ success: function() {
					//$('.hrlfc').html(srsListMain);
					$('.hrlfc').html(_.template(prsListMain, $.extend({}, collectEmpLeaves.attributes, {_:_, jsHelper: jsHelper} ))); //leave-list main
					$('.oaccordion-list').Oaccordion();
					$('.hvbdsearch').attr('name', 'prs-list');
				}
			});
		},
		PettyCashList: function(evt) {
			$('.hrlfc').html(pettyCashListMain);
		},
		render: function() {
			taskCollector.fetch({ success: function() {
					//data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(mainView);
					//
					collectEmpLeaves.url = baseURL+"hr/forms/retrieve/leave-list";
					
					collectEmpLeaves.fetch({ success: function() {
							$('.hrlfc').html(_.template(leaveListMain, $.extend({}, collectEmpLeaves.attributes, {_:_, jsHelper: jsHelper} ))); //leave-list main
							//$('.sample-test').Oaccordion();
							$('.oaccordion-list').Oaccordion();
							$('.hvbdsearch').attr('name', 'leave-list');
						}
					});
				}
			});
		}
	});
	return new task_view;
});