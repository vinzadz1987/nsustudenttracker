define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/task/hr/contributions_list.html',
],function($,_,Backbone,base_url,taskView,contributionsList){
    var contributions_cnv = {
        getPHIClist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"hr/contributions_list",{
                dir: "retrieve_phic_list"
            },function(data){
                container.html(data.res);
            },"json");
        },
        getSSSlist: function(container) {
            container.html($('<div class="loading-style1"></div>'));
            $.post(base_url+"hr/contributions_list",{
                dir: "retrieve_sss_list"
            },function(data){
                container.html(data.res);
            },"json");
        }
    };
    //input with "formatdec" class is passed to the decimalFormat plugin (jquery.decimalFormat)
    $("body").on("keydown",".formatdec",function(e){
        if($(this).attr("name")=="salary_bracket") {
            $.decimalFormat($(this),e,0);
        } else {
            $.decimalFormat($(this),e,2);
        }
    });
    //toggle contribution forms on <li> click
    $("body").on("click","div.contributions ul li",function(){
        $("div.contrib-forms").slideUp();
        $('div.dyn_content').empty();
        if($(this).hasClass("active")) {
            $(this).attr("class","white-litegray");
        } else {
            $("div.contributions ul li").removeClass('active');
            switch ($(this).attr("title")) {
                case "SSS":$("div#sss-form").slideDown();
                    contributions_cnv.getSSSlist($('div.dyn_content'));break;
                case "PhilHealth":$("div#phic-form").slideDown();
                    contributions_cnv.getPHIClist($('div.dyn_content'));break;
            }
            $(this).addClass('active');
        }
    });
    //auto-compute utility for phic personal and employer share
    $("body").on("blur","form#phic-frm input[name$=salary_base]",function(){
        var salary_base = $(this).val();
        if(salary_base>0) {
            var share = (salary_base * 0.0125).toFixed(2);
            $("form#phic-frm input[name$=personal_share]").val(share);
            $("form#phic-frm input[name$=employer_share]").val(share);
            $("form#phic-frm input[name$=monthly_contribution]").val((share*2).toFixed(2));
        }
    });
    //handle philhealth form submit
    $("body").on("submit","form#phic-frm",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result === false) {
                $.customAlert("A required field is empty.");
            } else {
                $.post(base_url+"hr/contributions_list",{
                    dir: "add_phic_entry",
                    data: {
                        'bracket': $("form#phic-frm input[name$=salary_bracket]").val(),
                        'range_min': $("form#phic-frm input[name$=salary_range_min]").val(),
                        'range_max': $("form#phic-frm input[name$=salary_range_max]").val(),
                        'base': $("form#phic-frm input[name$=salary_base]").val(),
                        'personal_share': $("form#phic-frm input[name$=personal_share]").val(),
                        'employer_share': $("form#phic-frm input[name$=employer_share]").val(),
                        'monthly_contrib': $("form#phic-frm input[name$=monthly_contribution]").val()
                    }
                },function(data){
                    if(data.res == true) {
                        contributions_cnv.getPHIClist($('div.dyn_content'));
                        $("form#phic-frm")[0].reset();
                    } else {
                        $.customAlert("An error occurred. Please refresh this page and try again.");
                    }
                },"json");
            }
        });
    });
    /* delete phic/sss entry mod */ 
    $("body").on("hover","div.phic-contents, div.sss-contents",function(e){
        if(e.type=="mouseenter") {
            $(this).children("div.options").stop(true,true).fadeIn();
        }
        else if(e.type=="mouseleave") {
            $(this).children("div.options").stop(true,true).fadeOut();
        }
    });
    $("body").on("click","div.delete_phic_btn",function(){
        var phic_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"hr/contributions_list",{
            dir: "delete_phic_entry",
            pid: phic_no
        },function(data){
            $('div.dyn_content').html(data.res);
        },"json");
    });
    $("body").on("click","div.delete_sss_btn",function(){
        var sss_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"hr/contributions_list",{
            dir: "delete_sss_entry",
            sid: sss_no
        },function(data){
            $('div.dyn_content').html(data.res);
        },"json");
    });
    /* end of delete phic entry mod */
    //handle sss form submit
    $("body").on("submit","form#sss-frm",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result === false) {
                 $.customAlert("A required field is empty.");
            } else {
                $.post(base_url+"hr/contributions_list",{
                    dir: "add_sss_entry",
                    data: {
                        'range_min': $("form#sss-frm input[name$=range_min]").val(),
                        'range_max': $("form#sss-frm input[name$=range_max]").val(),
                        'employer': $("form#sss-frm input[name$=employer_contrib]").val(),
                        'employee': $("form#sss-frm input[name$=employee_contrib]").val(),
                        'ecer': $("form#sss-frm input[name$=ecer_contrib]").val()
                    }
                },function(data){
                    if(data.res == true) {
                        contributions_cnv.getSSSlist($('div.dyn_content'));
                        $("form#sss-frm")[0].reset();
                    } else {
                        $.customAlert("An error occurred. Please refresh this page and try again.");
                    }
                },"json");
            }
        });
    });
    var contributions_list = Backbone.View.extend({
        el: $("div#container"),
        initialize: function() {
        },
        events: {},
        render: function() {
            $('div.contents').html(contributionsList);
        }
    });
    return new contributions_list;
});