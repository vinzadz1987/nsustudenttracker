define([
    'jquery',
    'underscore',
    'backbone',

	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/jquery.Oaccordion',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/assets-master-list/main-view.html'
	
], function(
	$, _, Backbone, baseURL, jsHelper, lbox, Oaccordion,
	taskView, taskMenu, mainView
) {
	var collect_task = Backbone.Model.extend({});
	
	var AssetMLists = Backbone.Model.extend({});
	var AssetMTAssets = Backbone.Model.extend({});
	var AssetMRassets = Backbone.Model.extend({}); 
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	AssetMLists.prototype.url = baseURL+"";
	//var assetMLists = new AssetMLists.
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click #tskviewCAlist" : "tskviewCAlist",
			"click #tskviewCTAssets" : "tskviewCTAssets",
			"click #tskviewCRAssets" : "tskviewCRAssets"
		},
		tskviewCAlist: function(evt) {
			var $this = $(evt.currentTarget);
			$('#tskviewC').html('tskviewCAlist');
		},
		tskviewCTAssets: function(evt) {
			var $this = $(evt.currentTarget);
			$('#tskviewC').html('tskviewCTAssets');
		},
		tskviewCRAssets: function(evt) {
			var $this = $(evt.currentTarget);
			$('#tskviewC').html('tskviewCRAssets');
		},
		render: function() {
			taskCollector.fetch({
				success: function() {
					//data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(mainView);
				}
			});
		}
	});
	return new task_view;
});