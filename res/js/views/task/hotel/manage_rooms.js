define([
	'jquery',
	'underscore',
	'backbone',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/manage_rooms/main-view.html',
	'text!tpl/task/hotel/manage_rooms/rooms-template.html',
	'text!tpl/task/hotel/manage_rooms/change-room-status.html'
], function( $, _, Backbone, baseURL, jsHelper, lbox, taskView, taskMenu, mainView, roomsTemplate, changeRoomStatus ){
	var collect_task = Backbone.Model.extend({});
	var room_status = Backbone.Model.extend({});
	var get_rooms = Backbone.Model.extend({});
	var update_room_status = Backbone.Model.extend({});
	
	//String.prototype.firstLetterUpperCase = function() {
		//return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
	//};
	
	room_status.prototype.url = baseURL+"hotel/rooms/retrieve/status-list";
	var roomStatus = new room_status;
	
	get_rooms.prototype.url = baseURL+"hotel/rooms/get/single-rooms";
	var getRooms = new get_rooms;
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	update_room_status.prototype.url = baseURL+"hotel/rooms/update/rm-status";
	var updateRoomStatus = new update_room_status;
	
	$('.mhr-display').live("mouseover mouseout", function(e) {
		//console.log(e.type);
		(e.type === 'mouseover') ? 
			$(this).css({"border": "1px solid #ddd", "cursor": "pointer"}) : 
				$(this).css({"border": "1px solid #000"});
	});
	
	$('body').delegate('.changeRm-status', 'click', function() {
		var $room = $(this).attr('id'), $new_status = $('#change_room_status').val();
		
		updateRoomStatus.set({room: $room, new_status: $new_status});
		
		Backbone.emulateJSON = true;
		
		updateRoomStatus.save({}, {
			success: function() {
				$.lbox('removeData');
				Backbone.history.loadUrl();
			}
		});
		//console.log($new_status);
	});
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click #msingle-rooms" : "singleRooms",
			"click #mdouble-rooms" : "doubleRooms",
			"click #mquad-rooms" : "quadRooms",
			"click #mcorporate-rooms" : "corporateRooms",
			"click .mhotel-RT a" : "getRooms",
			"click .mhr-display" : "makeOrView", //make a reservation or view the information
		},
		makeOrView: function(evt) {
			$this = $(evt.currentTarget);
			var room_number = parseInt($this.find("p").eq(0).html());
			var room_desc = $this.find("p").eq(1).html();
			//console.log(room_desc);
			//console.log(room_number);
			//$.lbox({title: "waa", content: "werer"});
			
			// if reserved and occupied the comment in the accommodations table will be used
			// if vacant - dirty, vacant - clean and undermaintenance the comment in the rooms table will be used
			
			if( room_desc === "Occupied" || room_desc === "Reserved" || room_desc === "Occupied with vacant bed" ) {
				//console.log("Limited Access");
				$.lbox({
					title: "Warning: Limited Access",
					//content: "You don't have enough priviliged to access this thing. Report this warning to" +
							//"<br>your administrator/webmaster so that they can immediately dispatched their"+
							//"<br>highly trained monkeys to assist you."
					content: "You don't have enough priviliged to access this thing. Please" +
					"<br>contact your administrator/webmaster for further assistance."
				});
				
			//} else if( room_desc === "Vacant - Clean") {

				
			} else {
				$.lbox({
					title: "Room No: "+room_number+" - Changing Room Status to: ",
					content: _.template(changeRoomStatus, $.extend({}, roomStatus.attributes, {rn: room_number, currentStatus: room_desc, _:_}))
				});
				//console.log(roomStatus.attributes);
			}
		},
		getRooms: function(evt) {
			$this = $(evt.currentTarget);
			$('.mhotel-RT a').removeClass('hotel-roomtype-active');
			$this.addClass('hotel-roomtype-active');
		},
		singleRooms: function(evt) {
			//console.log("single rooms");
			getRooms.url = baseURL+"hotel/rooms/get/single-rooms"; //change the url
			getRooms.fetch({ 
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});
		},
		doubleRooms: function(evt) {
			getRooms.url = baseURL+"hotel/rooms/get/double-rooms"; //change the url
			getRooms.fetch({
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});
		},
		quadRooms: function(evt) {
			getRooms.url = baseURL+"hotel/rooms/get/quad-rooms"; //change the url
			getRooms.fetch({
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});;
		},
		corporateRooms: function(evt) {
			getRooms.url = baseURL+"hotel/rooms/get/corporate-rooms"; //change the url
			getRooms.fetch({
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});
		},
		render: function() {
			taskCollector.fetch({
				success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					roomStatus.fetch({
						success: function() {
							//console.log(roomStatus.attributes);
							//$('.contents').html(mainView);
							$('.contents').html(_.template(mainView, $.extend({}, roomStatus.attributes, {_:_} )));
							getRooms.url = baseURL+"hotel/rooms/get/single-rooms";
							getRooms.fetch({
								success: function() {
									$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
									//console.log(getRooms.attributes);
								}
							});
						}
					});
				}
			});
		}
	});
	return new task_view;
});