define([
	'jquery',
	'underscore',
	'backbone',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/timepicker',
	'jQ-plugins/tTip',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/create/create-student-form.html'
], function( 
	$, _, Backbone, baseURL, jsHelper, lbox, timepicker, tTip,
	taskView, taskMenu, mainView
){
	var collect_task = Backbone.Model.extend({});
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	var room_id = 0;
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click .glfr-add-accom-ST" : "addStudent"
		},
		addStudent: function(evt) {
			var personal_info = jsHelper.validate("input[type='text'], input[type='checkbox']:checked, select, textarea", $(".gs-contents"));

			if( personal_info != true ) {
				
				personal_info['rid'] = room_id; // add the room id
				var save_student = Backbone.Model.extend({});
				save_student.prototype.url = baseURL+"hotel/accommodation/add/student";
				var saveStudent = new save_student;
				saveStudent.set(personal_info); // set the values to be saved to the database
				
				Backbone.emulateJSON = true;

				saveStudent.save({}, {
					success: function() {
						Backbone.history.navigate("#/task/hotel-list/student");
					}
				}); // fire a request
				
			}
		},
		render: function(item1, item2, item3) {
			taskCollector.fetch({ success: function() {
					room_id = item1;
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(_.template(mainView,{_:_,rid: item1,rn:item2}));
					
					// for the datepicker with time
					$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
						ampm: true, changeYear: true, changeMonth: true,
						separator: ' @ '
					});
					
					$("#ag_dob, #ag_expiry_date, #ag_dateof_issue, #ag_visa_expiry, #ag_ssp_validity").datepicker();
					// for timepicker
					$('#ag_pickup_time, #ag_sendoff_time').timepicker({ampm: true});
				}
			});
		}
	});
	return new task_view;
});