define([
    'jquery',
    'underscore',
    'backbone',
    'jsHelper',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/taskmenu.html',
    'text!tpl/task/hotel/create/create-main-view.html',
    'text!tpl/task/hotel/create/create-student-form.html',
    'jQ-plugins/timepicker' // date picker with time
], function( $, _, Backbone, jsHelper, baseURL, taskView, taskMenu, createMainView, studentForm, timepicker) {
	var collect_task = Backbone.Model.extend({}); // get the tasks assigned to the user
	var clean_vacant_rooms = Backbone.Model.extend({}); // retrieve the list of vacant - clean rooms
	
	// get the tasks assigned to the user
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	// retrieve the list of vacant - clean rooms
	clean_vacant_rooms.prototype.url = baseURL+"hotel/accommodation/retrieve/vacant-clean-rooms";
	var cleanVacantRooms = new clean_vacant_rooms;
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); }, // load the necessary div elements first
    	events: {
    		"click .glfr-add-accom-ST" : "saveStudentsAccommodation"
		},
		saveStudentsAccommodation: function(evt) {
			var studAccom = jsHelper.validate("input[type='text'], select, textarea", $(".gs-contents"));

			if( studAccom !== true ) {
				var SaveStudentAccom = Backbone.Model.extend({});
				
				SaveStudentAccom.prototype.url = baseURL+"hotel/accommodation/save/create-student";
				var saveStudentAccom = new SaveStudentAccom;
				
				saveStudentAccom.set(studAccom);
				Backbone.emulateJSON = true;
				
				saveStudentAccom.save({}, { success: function() {
						Backbone.history.loadUrl();
						//Backbone.history.navigate('#/task/hotel-list');
					}
				});
				
			}
		},
		render: function() {
			// get the tasks assigned to the user
			taskCollector.fetch({ success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(createMainView);
					
					// retrieve the list of vacant - clean rooms
			    	cleanVacantRooms.fetch({ success: function() {
	    					var studentFormData = _.template(studentForm, {results: cleanVacantRooms.attributes, _:_ });
	    					$('.gs-contents').html(studentFormData);
	    					
	    					// for the datepicker with time
	    					$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
	    						ampm: true,
	    						separator: ' @ '
	    					});
	    					
	    					$("#ag_dob, #ag_expiry_date, #ag_dateof_issue, #ag_visa_expiry, #ag_ssp_validity").datepicker();
	    					// for timepicker
	    					$('#ag_pickup_time, #ag_sendoff_time').timepicker({ampm: true});
			    		}
			    	});
				}
			});
		}
	});
	return new task_view;
});