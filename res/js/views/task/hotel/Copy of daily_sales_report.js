define([
    'jquery',
    'underscore',
    'backbone',
    'jQ-plugins/ckeditor/ckeditor',
    'jQ-plugins/ckeditor/adapters/jquery',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/daily-sales-report/main-view.html',
	'text!tpl/task/hotel/daily-sales-report/add-daily-sales.html',
	'text!tpl/task/hotel/daily-sales-report/daily-sales-list.html',
	'text!tpl/task/hotel/daily-sales-report/edit-daily-sales.html'
], function($, _, Backbone, ckeditor, ckeditorJqueryAdapter, baseURL, jsHelper, lbox, taskView, taskMenu, mainView, addDailySales, dailySalesList, editDailySales) {
	
	// ckeditor configuration
	var ckeditor_config = {
		toolbar: [
		    [
				'Image','Table', '-', 'Bold', 'Italic', 'Underline', '-', 'Strike', 'Subscript', 'Superscript',
				'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'
		    ],
		    ['NumberedList','BulletedList','-','Outdent','Indent'],
		    ['Format','Font','FontSize'],
		    ['TextColor','BGColor']
		],
		skin: 'kama'
	};
	
	var collect_task = Backbone.Model.extend({});
	var save_report = Backbone.Model.extend({});
	var sales_report_list = Backbone.Model.extend({});
	var UpdateReport = Backbone.Model.extend({});
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	save_report.prototype.url = baseURL+"hotel/reports/save/daily-sales";
	var saveReport = new save_report;
	
	UpdateReport.prototype.url = baseURL+"hotel/reports/update/daily-sales";
	var updateReport = new UpdateReport;
	
	sales_report_list.prototype.url = baseURL+"hotel/reports/retrieve/daily-sales";
	var salesReportList = new sales_report_list;
	
	
	jsHelper.page_pagination(".hdsr-container", dailySalesList); //when the pagination was clicked
	jsHelper.hovered_items('.dslC-content'); //when hovering the list or items
	
	//$('body').delegate('.dslC-content', 'mouseover mouseout', function(e) {
		//console.log(e.type);
		//e.type === 'mouseover' : $(this).css({"cursor": "poionter"}) ? ;
	//});
	
	$('body').delegate('.dslC-content', 'click', function(e) {
		var $id = $(this).find('.dslc-subject-content').attr('id');
		$('.hdsr-container').html(_.template(editDailySales, $.extend({}, salesReportList.attributes, {_:_, id: $id})));
		$('.ckeditor-daily-sales-rep').ckeditor(ckeditor_config);
	});
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click .save-daily-sales-rep": "saveDailySales",
			"click #daily-sales-list" : "dailySalesList",
			"click #daily-sales-add-report" : "dailySalesAddReport",
			"click .page-pagination a" : "DoPagePaginationClick",
			"click .update-daily-sales-rep" : "UpdateDailySalesRep"
		},
		UpdateDailySalesRep: function(evt) {
			var $id = $(evt.currentTarget).attr('id'), msg = "";
			
			var $subject = $('#add-daily-sales-sl').val();
			var $content = $('.ckeditor-daily-sales-rep').val();
			
			if( $subject === "" && $content === "" ) msg += "Please enter a subject and a content.";
			else if( $subject === "" && $content !== "" ) msg += "Please enter a subject.";
			else if( $subject !== "" && $content === "" ) msg += "Please enter the content.";
			
			if( msg === "" ) {
				//console.log("way error");
				updateReport.set({subject: $subject, content: $content, id: $id});
				
				Backbone.emulateHTTP = true;
				Backbone.emulateJSON = true;
				
				updateReport.save({}, {
					success: function() {
						// redirect to the list of mga listahan Backbone.history.loadUrl() ra na bogart
						Backbone.history.loadUrl();
					}
				});
			} else $.lbox({title: "Warning Message", content: msg});
		},
		DoPagePaginationClick: function(evt) {
			var url = $(evt.currentTarget).attr('href');
			salesReportList.url = url;
			salesReportList.fetch();
		},
		dailySalesList: function(evt) {
			var $this = $(evt.currentTarget);
			
			$('.hotel-RT a').removeClass('hotel-roomtype-active');
			$this.addClass('hotel-roomtype-active');
			
			//$('.hdsr-container').html(dailySalesList);
			$('.hdsr-container').html(_.template(dailySalesList, $.extend({}, salesReportList.attributes, {_:_, jsHelper: jsHelper})));
			
		},
		dailySalesAddReport: function(evt) {
			var $this = $(evt.currentTarget);
			
			$('.hotel-RT a').removeClass('hotel-roomtype-active');
			$this.addClass('hotel-roomtype-active');
			
			$('.hdsr-container').html(addDailySales);
			$('.ckeditor-daily-sales-rep').ckeditor(ckeditor_config);

		},
		saveDailySales: function(evt) {
			var $this = $(evt.currentTarget), msg = "";
			var $subject = $('#add-daily-sales-sl').val();
			var $content = $('.ckeditor-daily-sales-rep').val();
			
			//msg += ($subject === "") ? "<p>Please enter a subject.</p>" : "";
			//msg += ($content === "") ? "<p>No content detected.</p>" : "";
			
			if( $subject === "" && $content === "" ) msg += "Please enter a subject and a content.";
			else if( $subject === "" && $content !== "" ) msg += "Please enter a subject.";
			else if( $subject !== "" && $content === "" ) msg += "Please enter the content.";
			
			if( msg === "" ) {
				//console.log("way error");
				saveReport.set({subject: $subject, content: $content});
				
				Backbone.emulateJSON = true;
				
				saveReport.save({}, {
					success: function() {
						// redirect to the list of mga listahan Backbone.history.loadUrl() ra na bogart
						Backbone.history.loadUrl();
					}
				});
			} else $.lbox({title: "Warning Message", content: msg});
		},
		render: function() {
			taskCollector.fetch({
				success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(mainView);
					
					//get the list of sales report
					salesReportList.fetch({
						success: function() {
							$('.hdsr-container').html(_.template(dailySalesList, $.extend({}, salesReportList.attributes, {_:_, jsHelper: jsHelper})));
							//$('.ckeditor-daily-sales-rep').ckeditor(config);
						}
					});
				}
			});
		}
	});
	return new task_view;
});