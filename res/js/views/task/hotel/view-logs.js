define([
	'jquery',
	'underscore',
	'backbone',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/timepicker',
	'jQ-plugins/tTip',
	'jQ-plugins/jquery.Oaccordion',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/view-logs/main-view.html',
	'text!tpl/task/hotel/view-logs/occupied.html',
	'text!tpl/task/hotel/view-logs/reserved.html',
	'text!tpl/task/hotel/view-logs/history.html',
	'text!tpl/task/hotel/view-logs/templates/update-guest-view.html',
	'text!tpl/task/hotel/view-logs/templates/update-student-view.html'
], function( 
	$, _, Backbone, baseURL, jsHelper, lbox, timepicker, tTip, Oaccordion,
	taskView, taskMenu, mainView, occupiedView, reservedView, historyView, updateGuestView, updateStudentView
){
	
	var $item1 = 0, $item2 = 0, woc = {}, rev = {};
	
	var collect_task = Backbone.Model.extend({});
	var who_occupy = Backbone.Model.extend({});
	var who_makereservation = Backbone.Model.extend({});
	var history = Backbone.Model.extend({});
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	//$('body').delegate('.lgvg', 'mouseover mouseout', function(evt) {
		//$(this).find('.lgv-ag').show();
	//});
	
	
	$('body').delegate('.lgv-as-checkout', 'click', function(evt) {
		//evt.preventDefault();
	});
	
	$('body').delegate('.lgv-ag-checkout', 'click', function(evt) {
		//evt.preventDefault();
	});
	
	// when edit icon was clicked.. this is for guests
	$('body').delegate('.lgv-ag-edit', 'click', function(evt) {
		evt.preventDefault();
		var $parent = $(this).parent().parent();
		var ids = $(this).attr('rel').split(':'), raid = ids[0], gapid = ids[1], gaid = ids[2], attr = {};
		
		// we will determine what data to display in the edit/update page
		if( $(this).hasClass('oc') ) attr = woc;
		else if( $(this).hasClass('rev') ) attr = rev;
			
		$parent.html(_.template(updateGuestView, $.extend({}, attr, {_:_, raid: raid, gapid: gapid, gaid:gaid} )));
		// for the datepicker with time
		$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
			ampm: true,
			separator: ' @ '
		});
		
		$("#ag_dob").datepicker();
		// for timepicker
		$('#ag_pickup_time, #ag_sendoff_time').timepicker({ampm: true});

	});
	
	$('body').delegate('.lgv-as-edit', 'click', function(evt) {
		evt.preventDefault();
		var $parent = $(this).parent().parent(), ids = $(this).attr('rel').split(':'), attr = {};
		
		// we will determine what data to display in the edit/update page
		if( $(this).hasClass('oc') ) attr = woc;
		else if( $(this).hasClass('rev') ) attr = rev;
		$parent.html(_.template(updateStudentView, $.extend({}, attr, {_:_, raid: ids[0], sapid: ids[1], said: ids[2]} )));
		
		// for the datepicker with time
		$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
			ampm: true,
			separator: ' @ '
		});
		
		$("#ag_dob, #ag_expiry_date, #ag_dateof_issue, #ag_visa_expiry, #ag_ssp_validity").datepicker();
		// for timepicker
		$('#ag_pickup_time, #ag_sendoff_time').timepicker({ampm: true});
		
	});
	
	
	$('.oalist-head a').live('click',function(e){e.preventDefault();});
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click .glfr-update-logh" : "updateGuest",
			"click .glfr-update-losh" : "updateStudent"
		},
		updateStudent: function(evt) {
			var $this = $(evt.currentTarget), $parent = $this.parent().parent();
			var personal_info = jsHelper.validate("input[type='text'], input[type='checkbox']:checked, select, textarea", $parent);
			
			if( personal_info != true ) {
				var ids = $this.attr('id').split(':');
				
				personal_info['raid'] = ids[0]; personal_info['sapid'] = ids[1]; personal_info['said'] = ids[2];
				personal_info['rni'] = ids[3]; personal_info['rmaid'] = ids[4];
				
				var update_student = Backbone.Model.extend({});
				update_student.prototype.url = baseURL+"hotel/accommodation/update/student";
				var updateStudent = new update_student;
				updateStudent.set(personal_info); // set the values to be saved to the database
				
				Backbone.emulateJSON = true;
				updateStudent.save({}, { success: function() {
						Backbone.history.loadUrl();
					}
				}); // fire a request
			}
		},
		updateGuest: function(evt) {
			var $this = $(evt.currentTarget), $parent = $this.parent().parent();
			var personal_info = jsHelper.validate("input[type='text'], input[type='checkbox']:checked, select, textarea", $parent);
			
			if( personal_info != true ) {
				var ids = $this.attr('id').split(':');
				
				personal_info['raid'] = ids[0]; personal_info['gapid'] = ids[1]; personal_info['gaid'] = ids[2];
				personal_info['rni'] = ids[3]; personal_info['rmaid'] = ids[4];
				
				var update_guest = Backbone.Model.extend({});
				update_guest.prototype.url = baseURL+"hotel/accommodation/update/guest";
				var updateGuest = new update_guest;
				updateGuest.set(personal_info); // set the values to be saved to the database
				
				Backbone.emulateJSON = true;
				updateGuest.save({}, { success: function() {
						Backbone.history.loadUrl();
					}
				}); // fire a request
			}
		},
		render: function(item1, item2, item3) {
			$item1 = item1; $item2 = item2;
			// item1 is the room number..
			
			taskCollector.fetch({ success: function() {
				
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(_.template(mainView, {_:_,rid: item1,rn:item2}));
					
					who_occupy.prototype.url = baseURL+'hotel/accommodation/logs/checkin/'+item1;
					var WhoOccupy = new who_occupy;
					
					who_makereservation.prototype.url = baseURL+'hotel/accommodation/logs/reserve/'+item1;
					var WhoMakeReservation = new who_makereservation;
					
					history.prototype.url = baseURL+'hotel/accommodation/logs/checkout/'+item1;
					var History = new history;

					// get the list of guest or students who occupy this room..
					WhoOccupy.fetch({ success: function() {
							$('.hr-logs-occupied').html(_.template(occupiedView, $.extend({}, WhoOccupy.attributes, {_:_})));
							$('.hr-logs-info1').Oaccordion();
							woc = WhoOccupy.attributes;
						}
					});
                                        
					// get the list of guest or students who make reservation on this room..
					WhoMakeReservation.fetch({ success: function() {
							$('.hr-logs-reserved').html(_.template(reservedView, $.extend({}, WhoMakeReservation.attributes, {_:_})));
							$('.hr-logs-info2').Oaccordion();
							rev = WhoMakeReservation.attributes;
						}
					});
					
					// the list of guest and students who ocupied this room and has already been checked out
					History.fetch({ success: function() {
							$('.hr-logs-history').html(_.template(historyView, $.extend({}, History.attributes, {_:_})));
							$('.hr-logs-info3').Oaccordion();
						}
					});
				}
			});
		}
	});
	return new task_view;
});