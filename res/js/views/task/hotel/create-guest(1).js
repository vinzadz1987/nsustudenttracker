define([
    'jquery',
    'underscore',
    'backbone',
    'jsHelper',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/taskmenu.html',
    'text!tpl/task/hotel/create/create-main-view.html',
    'text!tpl/task/hotel/create/create-guest-form.html',
    'jQ-plugins/timepicker' // date picker with time
], function( $, _, Backbone, jsHelper, baseURL, taskView, taskMenu, createMainView, guestForm, timepicker) {
	var collect_task = Backbone.Model.extend({}); // get the tasks assigned to the user
	var clean_vacant_rooms = Backbone.Model.extend({}); // retrieve the list of vacant - clean rooms
	
	// save the guest accommodation
	var save_guest = Backbone.Model.extend({});
	
	// get the tasks assigned to the user
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	// retrieve the list of vacant - clean rooms
	clean_vacant_rooms.prototype.url = baseURL+"hotel/accommodation/retrieve/vacant-clean-rooms";
	var cleanVacantRooms = new clean_vacant_rooms;
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); }, // load the necessary div elements first
    	events: {
    		"click .hotel-guest-form": "guestForm",
			"click .glfr-add-accom" : "saveGuest"
		},
		saveGuest: function(evt) {
			var personal_info = jsHelper.validate("input[type='text'], input[type='checkbox']:checked, select, textarea", $(".gs-contents"));
			//console.log(personal_info);
			
			if( personal_info != true ) {
				// add url
				save_guest.prototype.url = baseURL+"hotel/accommodation/save/create-guest";
				var saveGuest = new save_guest;
				saveGuest.set(personal_info); // set the values to be saved to the database
				
				Backbone.emulateJSON = true;

				saveGuest.save({}, {
					success: function() {
						Backbone.history.navigate("#/task/hotel-list");
					}
				}); // fire a request
			}
		},
		/*
		 * For Guest accommodation
		 * 
		 */
		guestForm: function(evt) {
			var $this = $(evt.currentTarget);
			$(".create-guest-student a").removeClass("create-guest-student-active");
			$this.addClass("create-guest-student-active");
			
			var guestFormData = _.template(guestForm, {results: cleanVacantRooms.attributes, _:_ });
			$('.gs-contents').html(guestFormData);
			
			// for the datepicker with time
			$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
				ampm: true,
				separator: ' @ '
			});
			
			$("#ag_dob").datepicker();
			// for timepicker
			$('#ag_pickup_time, #ag_sendoff_time').timepicker({ampm: true});
		},
		render: function() {
			// get the tasks assigned to the user
			taskCollector.fetch({
				success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(createMainView);
					
					// retrieve the list of vacant - clean rooms
			    	cleanVacantRooms.fetch({
			    		success: function() {
			    			
	    					var guestFormData = _.template(guestForm, {results: cleanVacantRooms.attributes, _:_ });
	    					$('.gs-contents').html(guestFormData);
	    					
	    					// for the datepicker with time
	    					$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
	    						ampm: true,
	    						separator: ' @ '
	    					});
	    					
	    					$("#ag_dob").datepicker();
	    					// for timepicker
	    					$('#ag_pickup_time, #ag_sendoff_time').timepicker({ampm: true});
			    		}
			    	});
				}
			});
		}
	});
	return new task_view;
});
