define([
	'jquery',
	'underscore',
	'backbone',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/timepicker',
	'jQ-plugins/tTip',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/rooms/main-view.html'
], function( 
	$, _, Backbone, baseURL, jsHelper, lbox, timepicker, tTip,
	taskView, taskMenu, mainView 
){   
        var room_accom = {
            
            retrieve_reserve_accom_s: function(param, container) {
                container.html($('<div class="loading-style1"></div>'));
                $.post(baseURL+"hotel/accommodation",{
                    dir: "all_rsv_students",
                    like: param
                },function(data){
                    container.html(data.res);
                },"json");
            },
            retrieve_reserve_accom_sc: function(param, container) {
                container.html($('<div class="loading-style1"></div>'));
                $.post(baseURL+"hotel/accommodation",{
                    dir: "all_rsv_students",
                    lc: param
                },function(data){
                    container.html(data.res);
                },"json");
            },
            retrieve_reserve_accom_g: function(param, container) {
                $.post(baseURL+"hotel/accommodation",{
                    dir: "all_rsv_guest",
                    like: param
                },function(data){
                    container.html(data.res);
                },"json");
            },
            retrieve_reserve_accom_gc: function(param, container) {
                $.post(baseURL+"hotel/accommodation",{
                    dir: "all_rsv_guest",
                    lc: param
                },function(data){
                    container.html(data.res);
                },"json");
            },
            retrieve_arc: function(container){
                $.post(baseURL+"hotel/accommodation",{
                    dir: "all_arc"
                },function(data){
                    container.html(data.res);
                },"json");
            }
            
        };
    
	var collect_task = Backbone.Model.extend({});
	var Rooms = Backbone.Model.extend({});
        
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
       
	Rooms.prototype.url = baseURL+"hotel/rooms/get/rooms-list";
	var rooms = new Rooms;
	
      /*  Room_status.prototype.url = baseURL+"hotel/accomodation/get_arc";
        var roomStatus = new Room_status;
            */
        
	$('body').delegate(".hr-display", "mouseover mouseout", function(e) {
		
		if(e.type === 'mouseover') {
			$(this).css({"border": "1px solid #ccc"}).find('.r-action').show();
			$(this).find('.or-status').hide();
		} else {
			$(this).css({"border": "1px solid #000"}).find('.r-action').hide();
			$(this).find('.or-status').show();
		}
		//(e.type === 'mouseover') ?
			//$(this).css({"border": "1px solid #ccc"}).find('.r-action').show() :
			//	$(this).css({"border": "1px solid #000"}).find('.r-action').hide();
	});
        
        $('body').on("click","a.activate-nr", function(){
            $(this).litebox("div.new-room",{show:true});
        }); 
        
      
        $('body').on("click","button.s", function(){
            $("div.srfsi").show();
            $("div.srfgi").hide();
        });
        
        $('body').on("click","button.g", function(){
            $("div.srfgi").show();
            $("div.srfsi").hide();
        });
      
        $('body').on("mouseover mouseout","div.content", function(){
            room_accom.retrieve_arc($('div.arc'));
        });
        
        
       //format dec
        $("body").on("keydown",".formatdec",function(e){
            if($(this).attr("name")=="room_no") {
            $.decimalFormat($(this),e,0);
            }else{
            $.decimalFormat($(this),e,0);
        }
        });
       
       
    //add rooms
        $("body").on("submit","#frm-nr",function(e){
            e.preventDefault();
            $(this).formvalidate(function(data){
                if(data.result===false) {
                $.customAlert(data.message);
                    } else {
                     var infos = {
                    
                        room_num: $("input[name$=room_no]").val(),
                        room_type: $("select[name$=rtype]").val()
                    
                    }
                $.post(baseURL+"hotel/accommodation",{
                    dir: "add_rooms",
                    data: {
                        
                        "room_no": infos.room_num,
                        "rtype": infos.room_type
                    }
                },function(data){
                    if(data==="added") { 
                        Backbone.history.navigate("#/hotel-rooms");
                        Backbone.history.navigate("#/task/hotel-rooms");
                        $("#frm-nr")[0].reset();
                    } else {
                        $("div.err_msg").html("An error occurred. Please refresh this page and try again.")
                        .litebox("div.err_msg",{show:true});
                    }
                    });
                }
            });
        });
       
       
       
       //initialize date picker
    $("body").on("click","input[name$=ci], input[name$=co]", function(){
       $(this).datetimepicker({dateFormat: 'yy-mm-dd',changeYear: true, changeMonth: true, yearRange: '1940:+10', timeFormat: 'hh:mm:ss', showSecond: true});
       $(this).datetimepicker({showOn: 'focus'}).focus();
    });
    
    $("body").on("keyup","div.srfsi input[name$=ci]", function(){
        $('div.ssr').html($('<div class="loading-style1"></div>'));
        $(this).facebox("div.ssr",{show:true});
        room_accom.retrieve_reserve_accom_s($(this).val(),$('div.ssr'));
    });
    
    $("body").on("keyup","div.srfsi input[name$=co]", function(){
        $('div.ssr').html($('<div class="loading-style1"></div>'));
        $(this).facebox("div.ssr",{show:true});
        room_accom.retrieve_reserve_accom_sc($(this).val(),$('div.ssr'));
    });
 
    $("body").on("keyup","div.srfgi input[name$=ci]", function(){
        $('div.ssr').html($('<div class="loading-style1"></div>'));
        $(this).facebox("div.ssr",{show:true});
        room_accom.retrieve_reserve_accom_g($(this).val(),$('div.ssr'));
    });
    
    $("body").on("keyup","div.srfgi input[name$=co]", function(){
        $('div.ssr').html($('<div class="loading-style1"></div>'));
        $(this).facebox("div.ssr",{show:true});
        room_accom.retrieve_reserve_accom_gc($(this).val(),$('div.ssr'));
    });
 
	//$('.hr-save-guest').tTip({message: "test"});
	$('body').delegate(".hr-save-guest, .hr-save-student, .hr-view, .hr-cancel-resrv, .hr-chkout-occpd", "mouseover mouseout", function(e) {
		var txt = "";
		//txt = $(this).hasClass('hr-save-guest') ? "Add Guest" : "Add Student";
		
		if( $(this).hasClass('hr-save-guest') ) txt = "Add Guest";
		else if( $(this).hasClass('hr-save-student') ) txt = "Add Student";
		else if( $(this).hasClass('hr-view') ) txt = "View Logs";
		else if( $(this).hasClass('hr-cancel-resrv') ) txt = "Cancel";
		else if( $(this).hasClass('hr-chkout-occpd') ) txt = "Check Out";
		
		(e.type === 'mouseover') ?
			$(this).parent().parent().prev().show().html(txt) :
				$(this).parent().parent().prev().hide().html('');
	});
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() {this.el.html(taskView);},
		events: {
			
		},
                
		render: function() {
                    
                    
                    
			taskCollector.fetch({success: function() {
                                       
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
                                        
                                        /*roomStatus.fetch({success: function(){
                                            
                                           }
                                        });*/
                                        
					rooms.fetch({success: function() {
						
							var single_rooms = [], double_rooms = [], triple_rooms = [], quad_rooms = [], corporate_rooms = [];
							
							//separate room types
							_.each(rooms.attributes.result, function(res) {
								if( res.room_type_name === 'Single' )
									single_rooms.push(res);
								else if(res.room_type_name === 'Double')
									double_rooms.push(res);
                                                                else if(res.room_type_name === 'Triple')
                                                                        triple_rooms.push(res);
								else if(res.room_type_name === 'Quad')
									quad_rooms.push(res);
								else if(res.room_type_name === 'Corporate')
									corporate_rooms.push(res);
							});
							
							var room = {
								single: single_rooms,
								double: double_rooms,
                                                                triple: triple_rooms,
								quad: quad_rooms,
								corporate: corporate_rooms
							};
							$('.contents').html(_.template(mainView, $.extend({}, room, {_:_})));
						}
					});
                                        
                                       
				}
                                
                                
			});   
		}
	});
	return new task_view;
});