define([
	'jquery',
	'underscore',
	'backbone',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/timepicker',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/rooms/main-view.html',
	'text!tpl/task/hotel/rooms/rooms-template.html',
	'text!tpl/task/hotel/rooms/add-accom.html',
	'text!tpl/task/hotel/rooms/display-accom-info.html',
	'text!tpl/task/hotel/rooms/edit-accom-info.html',
	'text!tpl/task/hotel/rooms/add-new-room.html'
], function( $, _, Backbone, baseURL, jsHelper, lbox, timepicker, taskView, taskMenu, mainView, roomsTemplate, addAccommodation, dispAccomInfo, editAccomInfo, addNewRoom ){
	var collect_task = Backbone.Model.extend({});
	var room_status = Backbone.Model.extend({});
	var get_rooms = Backbone.Model.extend({});
	var room_info = Backbone.Model.extend({});
	var save_room_accommodation = Backbone.Model.extend({});
	var update_room_accommodation = Backbone.Model.extend({});
	
	//String.prototype.firstLetterUpperCase = function() {
		//return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
	//};
	
	room_status.prototype.url = baseURL+"hotel/rooms/retrieve/status-list";
	var roomStatus = new room_status;
	
	get_rooms.prototype.url = baseURL+"hotel/rooms/get/single-rooms";
	var getRooms = new get_rooms;
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	room_info.prototype.url = baseURL+"hotel/rooms/info/";
	var roomInformation = new room_info;
	
	//adding new room functionality
	$('body').delegate('#save-new-room', 'click', function() {
		//console.log($(this));
		var data = jsHelper.validate("input[type='text'], select", $(".crs-container"));
		console.log(data);
	});
	
	//saving edited guest accommodation
	$("body").delegate(".glf-r-save", "click", function() {
		//console.log("test save");
		var data = jsHelper.validate("input[type='text'], select, textarea", $(".glf-r"));
		if( data != true ) { 
			data['le_id'] = $(this).attr('id');
			data['room_number'] = parseInt($('.room-number').html());
			
			update_room_accommodation.prototype.url = baseURL+"hotel/accommodation/update/guest-list";
			var updateRoomAccommodation = new update_room_accommodation;
			updateRoomAccommodation.set(data);
			
			Backbone.emulateJSON = true;
			
			updateRoomAccommodation.save({}, {
				success: function() {
					$.lbox('removeData');
					Backbone.history.loadUrl();
				}
			});
		}
	});
	
	//add guest accommodation on room click
	$("body").delegate(".glfr-add-accom", "click", function() {
		//console.log("test save");
		var data = jsHelper.validate("input[type='text'], select, textarea", $(".add-accom-rclick"));
		
		if( data != true ) {
			data['ag_rn'] = $(this).attr('id');
			save_room_accommodation.prototype.url = baseURL+"hotel/accommodation/save/create-guest";
			var saveRoomAccommodation = new save_room_accommodation;
			
			saveRoomAccommodation.set(data);
			//Backbone.emulateHTTP = true;
			Backbone.emulateJSON = true;
			
			saveRoomAccommodation.save({}, {
				success: function() {
					$.lbox('removeData');
					Backbone.history.loadUrl();
				}
			});
		}
	});
	
	//when clicking the edit button on the occupied room
	$("body").delegate(".glf-r-edit","click", function() {
		
		var $parent = $(this).parent().parent();
		$parent.removeClass("glfr-is-disabled");
		
		$parent.html(_.template(editAccomInfo, $.extend({}, roomInformation.attributes, {_:_})));
		
		$('#le_dob').datepicker();
		// for the datepicker with time
		$('#check_in, #check_out').datetimepicker({
			ampm: true,
			separator: ' @ '
		});
		$("#le_pickup_time, #le_send_off_time").timepicker({ampm: true});
	});
	
	$('.hr-display').live("mouseover mouseout", function(e) {
		//console.log(e.type);
		(e.type === 'mouseover') ? 
			$(this).css({"box-shadow": "1px 1px 25px #f9f9f9 inset", "cursor": "pointer"}) : 
				$(this).css({"box-shadow": "4px 9px 30px #F8F8F8"});
	});
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click #single-rooms" : "singleRooms",
			"click #double-rooms" : "doubleRooms",
			"click #quad-rooms" : "quadRooms",
			"click #corporate-rooms" : "corporateRooms",
			"click .hotel-RT a" : "getRooms",
			"click .hr-display" : "makeOrView", //make a reservation or view the information
			"click #add-new-room" : "doAddNewRoom"
			
			//"click .glf-r-edit" : "editGLF" // whats GLF? ambot nimo.. ayaw kog pangutan-a
		},
		editGLF: function(evt) {
			var $this = $(evt.currentTarget);
			//$('.lbox-contents .glf-r').html("waa");
			console.log("wee");
		},
		doAddNewRoom: function(evt) {
			//console.log($(evt.currentTarget));
			$.lbox({
				//title: "Add New Room",
				//content: _.template(addNewRoom, $.extend({}, roomStatus.attributes, {_:_}))
				title: "Notice: Functionality Disabled",
				content: "Sorry, this functionality has been disabled for now."
			});
			jsHelper.digit_only($('#rm_no'));
		},
		makeOrView: function(evt) {
			$this = $(evt.currentTarget);
			var room_number = parseInt($this.find("p").eq(0).html());
			var room_desc = $this.find("p").eq(1).html();
			//console.log(room_desc);
			//console.log(room_number);
			//$.lbox({title: "waa", content: "werer"});
			
			// if reserved and occupied the comment in the accommodations table will be used
			// if vacant - dirty, vacant - clean and undermaintenance the comment in the rooms table will be used
			
			if( room_desc === "Occupied" || room_desc === "Reserved" || room_desc === "Occupied with vacant bed" ) {
				//$.lbox({title: "waa", content: "werer"});
				roomInformation.url = baseURL+"hotel/rooms/info/"+room_desc.toLowerCase()+"/"+room_number;
				roomInformation.fetch({
					success: function() {
						$.lbox({
							title: "View Accommodation: "+room_desc,
							content: _.template(dispAccomInfo, $.extend({}, roomInformation.attributes, {_:_, room: room_number}))
						});
						$('#ag_dob').datepicker();
						// for the datepicker with time
						$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
							ampm: true,
							separator: ' @ '
						});
						$("#ag_pickup_time, #ag_sendoff_time").timepicker({ampm: true});
						//console.log($.extend({}, roomInformation.attributes, {_:_, room: room_number}));
					}
				});
				
			} else if( room_desc === "Vacant - Clean") {
				//room_info.prototype.url = baseURL+"hotel/rooms/info/"+room_desc.toLowerCase().replace(/\s/g, "")+"/"+room_number;
				//var roomInformation = new room_info;
				
				//roomInformation.fetch();
				$.lbox({
					title: "Add accommodation: "+room_desc,
					content: _.template(addAccommodation, {room: room_number})
				});
				$('#ag_dob').datepicker();
				// for the datepicker with time
				$('#ag_checkin_date, #ag_checkout_date').datetimepicker({
					ampm: true,
					separator: ' @ '
				});
				$("#ag_pickup_time, #ag_sendoff_time").timepicker({ampm: true});
				//console.log(addAccommodation);
			} else {
				$.lbox({
					title: "Warning: Limited Access",
					content: "You don't have enough priviliged to access this thing. Please" +
							"<br>contact your administrator/webmaster for further assistance."
				});
			}
		},
		getRooms: function(evt) {
			$this = $(evt.currentTarget);
			$('.hotel-RT a').removeClass('hotel-roomtype-active');
			$this.addClass('hotel-roomtype-active');
		},
		singleRooms: function(evt) {
			//console.log("single rooms");
			getRooms.url = baseURL+"hotel/rooms/get/single-rooms"; //change the url
			getRooms.fetch({ 
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});
		},
		doubleRooms: function(evt) {
			getRooms.url = baseURL+"hotel/rooms/get/double-rooms"; //change the url
			getRooms.fetch({
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});
		},
		quadRooms: function(evt) {
			getRooms.url = baseURL+"hotel/rooms/get/quad-rooms"; //change the url
			getRooms.fetch({
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});;
		},
		corporateRooms: function(evt) {
			getRooms.url = baseURL+"hotel/rooms/get/corporate-rooms"; //change the url
			getRooms.fetch({
				success: function() {
					$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
				}
			});
		},
		render: function() {
			taskCollector.fetch({
				success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					roomStatus.fetch({
						success: function() {
							//console.log(roomStatus.attributes);
							//$('.contents').html(mainView);
							$('.contents').html(_.template(mainView, $.extend({}, roomStatus.attributes, {_:_} )));
							getRooms.url = baseURL+"hotel/rooms/get/single-rooms";
							getRooms.fetch({
								success: function() {
									$('.hotel-RT-Rooms').html(_.template(roomsTemplate, $.extend({}, getRooms.attributes, {_:_})));
									//console.log(getRooms.attributes);
								}
							});
						}
					});
				}
			});
		}
	});
	return new task_view;
});