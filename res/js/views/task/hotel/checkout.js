define([
	'jquery',
	'underscore',
	'backbone',
	'baseURL',
	'jsHelper',
	'jQ-plugins/lbox',
	'jQ-plugins/timepicker',
	'jQ-plugins/tTip',
	'text!tpl/task/task_view.html',
	'text!tpl/taskmenu.html',
	'text!tpl/task/hotel/checkout/mainView.html',
	'text!tpl/task/hotel/checkout/additional-payments.html'
], function( 
	$, _, Backbone, baseURL, jsHelper, lbox, timepicker, tTip,
	taskView, taskMenu, mainView, additionalPaymentsTemplate
){
	var collect_task = Backbone.Model.extend({});
	var checkout = Backbone.Model.extend({});
	
	var account_type = "", room_number_id = "", room_number = "", account_type_id = "";
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	// remove the payments template
	$(".chk-rmv-item").live('click', function(evt) {
		evt.preventDefault();
		$(this).parent().parent().remove();
	});
	// insert payments template before the button
	$('.chk-add-apymts').live('click', function(evt){
		evt.preventDefault();
		$(additionalPaymentsTemplate).insertBefore('.chk-adp-NBB');
	});
	
	jsHelper.digit_only($('.chk-adp-amt')); //make sure that the textbox is digit only
	
	var roomNum_id = 0; // the room number id	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click .chk-button" : "save"
		},
		save: function(evt) {
			var $this = $(evt.currentTarget);
			//console.log($this);
			//var additional_payments = jsHelper.validate("input[type='text'], input[type='checkbox']:checked, select, textarea", $(".chkf"));

			checkout.prototype.url = baseURL+"hotel/accommodation/checkout/"+account_type+"/"+room_number_id+"/"+account_type_id;
			
			if( $this.parent().prev().hasClass('chk-adpf') ) {
				var additional_payments = new Array();
				// loop through the div element
				$('.chk-adpf').each(function(){
					var description = $(this).find('.chk-adp-desc').val();
					var amount = $(this).find('.chk-adp-amt').val();
					
					if( description !== "" && amount !== "" ) {
						additional_payments.push({chk_desc: description, chk_amount: amount});
					}
				});
			} else {}
			
			var CheckOut = new checkout;
			CheckOut.set({adP: additional_payments}); // set the POST data items;
			
			Backbone.emulateJSON = true;
			
			CheckOut.save({}, {success: function(){
					Backbone.history.navigate('#/task/hotel-rooms');
				}
			});
			
		},
		/*
		 * @params:
		 * 		accType, either guest or student
		 * 		roomNumberId
		 * 		roomNumber
		 * 		sorgNumber, either student or guest number
		 * 
		 * @return:
		 */
		render: function(accType, roomNumberId, roomNumber, sorgNumber) {
			//console.log(arguments);
			account_type = accType, room_number_id = roomNumberId, room_number = roomNumber, account_type_id = sorgNumber;
			taskCollector.fetch({ success: function() {
					roomNum_id = roomNumberId;
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(_.template(mainView, {roomNumberId: roomNumberId, roomNumber: roomNumber, _:_}));
				}
			});
		}
	});
	return new task_view;
});