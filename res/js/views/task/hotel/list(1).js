define([
    'jquery',
    'underscore',
    'backbone',
    'jsHelper',
    'baseURL',
    'text!tpl/task/task_view.html',
    'text!tpl/taskmenu.html',
    'text!tpl/task/hotel/list/list-main-view.html',
    'text!tpl/task/hotel/list/list-guest-accom.html',
    'text!tpl/task/hotel/list/lga-edit-template.html',
    'text!tpl/task/hotel/list/glv-template.html',
    'jQ-plugins/timepicker'
], function( $, _, Backbone, jsHelper, baseURL, taskView, taskMenu, createMainView, listGuestAccom, lgaEditTemp, glvTemplate, timepicker) {
	var collect_task = Backbone.Model.extend({});
	var guest_list = Backbone.Model.extend({});
	var clean_vacant_rooms = Backbone.Model.extend({});
	var update_guest_list = Backbone.Model.extend({});
	
	collect_task.prototype.url = baseURL+"task_collector";
	var taskCollector = new collect_task;
	
	guest_list.prototype.url = baseURL+"hotel/accommodation/retrieve/guest-list";
	var guestList = new guest_list;
	
	clean_vacant_rooms.prototype.url = baseURL+"hotel/accommodation/retrieve/lcvr";
	var cleanVacantRooms = new clean_vacant_rooms;
	
	jsHelper.page_pagination(".guest-list-view", glvTemplate); //when the pagination was clicked
	
	var task_view = Backbone.View.extend({
		el: $("#container"),
		initialize: function() { this.el.html(taskView); },
		events: {
			"click .list-toggle" : "doToggle", // toggle contents when clicking the +/- button 
			"click .active-list-view" : "doSlideup", // slide up or hide the displayed data
			"click .gli-edit" : "doEditGli",
			"click .gli-save" : "doSaveGli",
			"keyup .gli-search-box" : "doSearchGli",
			"click .page-pagination a" : "doPaginationClick",
			"change #gli-search-by" : "doChangeGli"
		},
		doChangeGli: function(evt) {
			var $this = $(evt.currentTarget);
			var search_item = $this.prev().val(), search_itemby = $this.val();
			var search = Backbone.Model.extend({
				url: baseURL+"hotel/accommodation/search/gli/"+search_itemby+"/"+search_item
			});
			
			//add date picker if the search_itemby is checkin or checkout
			/*
			if( search_itemby == 'checkin' || search_itemby == 'checkout' ) {
				//$this.prev()
				$this.prev().attr("id", "gli-search-box");
				$this.prev().datepicker();
			} else {
				$this.prev().removeAttr("id").removeClass('hasDatepicker').datepicker("destroy");
				//$("#gli-search").datepicker("option", "disabled");
			}*/
			
			if( search_item != "" ) {
				var search_ = new search;
				search_.fetch({
					success: function() {
						$(".guest-list-view").html(
							_.template(glvTemplate, $.extend({}, search_.attributes, {_:_}))
						);
					}
				});
			} else {
				//Backbone.history.loadUrl(); //load the previous result
				guestList.fetch({
					success: function() {
						$(".guest-list-view").html(
							_.template(glvTemplate, $.extend({}, guestList.attributes, {_:_}))
						);
					}
				});
			}
		},
		doPaginationClick: function(evt) {
			var $this = $(evt.currentTarget);
			guestList.url = $this.attr("href");
			//console.log(guestList);
			guestList.fetch(); //re-initialize the guestList variable..
			//console.log($this.attr("href"));
		},
		doSearchGli: function(evt) {
			var $this = $(evt.currentTarget);
			var search_item = $this.val(), search_itemby = $this.next().val();
			var search = Backbone.Model.extend({
				url: baseURL+"hotel/accommodation/search/gli/"+search_itemby+"/"+search_item
			});
			/*
			var search_ = new search;
			search_.fetch({
				success: function() {
					$(".guest-list-view").html(
						_.template(glvTemplate, $.extend({}, search_.attributes, {_:_}))
					);
				}
			});*/
			
			if( search_item != "" ) {
				var search_ = new search;
				search_.fetch({
					success: function() {
						$(".guest-list-view").html(
							_.template(glvTemplate, $.extend({}, search_.attributes, {_:_}))
						);
					}
				});
			} else {
				//Backbone.history.loadUrl(); //load the previous result
				guestList.fetch({
					success: function() {
						$(".guest-list-view").html(
							_.template(glvTemplate, $.extend({}, guestList.attributes, {_:_}))
						);
					}
				});
			}
		},
		doSaveGli: function(evt) {
			var $this = $(evt.currentTarget);	
			//$this.attr({"class": "gli-edit", "value": "Edit"});
			//console.log($this.parent().parent());
			var data = {le_id: $this.attr('id')};
			$("select, textarea, input[type='text']", $this.parent().parent()).each(function(index, element){
				var $e = $(element);
				data[$e.attr("id")] = $e.val();
				//console.log($e.attr("id"));
			});
			//console.log(data);
			//console.log($this.attr('id'));
			update_guest_list.prototype.url = baseURL+"hotel/accommodation/update/guest-list";
			var updateGuestList = new update_guest_list;
			updateGuestList.set(data);
			
			Backbone.emulateJSON = true;

			updateGuestList.save({}, {
				success: function() {
					Backbone.history.loadUrl();
				}
			});
			//Backbone.sync('create', updateGuestList, {});
		},
		doEditGli: function(evt) {
			var $this = $(evt.currentTarget);
			var ids = $this.attr("id");
			ids = ids.split(":");
			//$this.attr({"class": "gli-save", "value": "Save"});
			//console.log($this.parent().parent().html(lgaEditTemp));
			//$('.e'+ids).html(lgaEditTemp);
			$this.parent().parent().html(
				_.template(lgaEditTemp, $.extend(guestList.attributes,{
					rooms: cleanVacantRooms.attributes,
					aid: ids[0], 
					gid: ids[1],
					_:_, 
				}))
			);
			$("#le_dob").datepicker();

			// for the datepicker with time
			$("#check_in, #check_out").datetimepicker({
				ampm: true,
				separator: ' @ '
			});
			$("#le_send_off_time, #le_pickup_time").timepicker({ampm: true});

			//console.log(cleanVacantRooms.attributes);
		},
		doSlideup: function(evt) {
			var $this = $(evt.currentTarget);			
			$this.html("+").removeClass('active-list-view').addClass("list-toggle").parent().next().slideUp();
		},
		doToggle: function(evt) {
			var $this = $(evt.currentTarget);
			var gLength = $(".guest-list").length;
			var cIndex = $this.parent().parent().index();
			//( $this.html() != "-" ) ? $this.html("-") : $this.html("+"); //add +/- text
			/*
			$(".guest-list-head").each(function(index, element){
				var $e = $(element);
				if( $e.hasClass("active-list-view") ) {
					$e.find(".gl-pm-toggle").html("+");
					$e.next().slideUp();
					$e.removeClass("active-list-view");
				}
			});*/
			
			if($this.html() != "-") {
				$this.html("-");
				//$this.parent().next().stop().slideDown();
				$this.parent().next().slideDown();
				// remove the bottom border if its the last guest-list class 
				if( cIndex === (gLength - 1) ) {
					$this.parent().parent().css({"border-bottom": "none"});
				}
				// hide active class and add the list-toggle class
				$(".guest-list-head").find(".active-list-view").html("+").addClass("list-toggle").removeClass("active-list-view").parent().next().slideUp();
				$this.addClass("active-list-view").removeClass("list-toggle");
			} else {
				$this.html("+");
				//$this.parent().next().stop().slideUp();
				$this.parent().next().slideUp();
				//add bottom border if its the last guest-list class 
				if( cIndex === (gLength - 1) ) {
					$this.parent().parent().css({"border-bottom": "1px solid #AAAAAA"});
				}
			}

			//console.log($this.parent().parent().index());
			/*
			if( cIndex === (gLength - 1) ) {
				$this.parent().parent().css({"border-bottom": "none"});
				console.log($this.parent().parent());
			}*/
			//$this.parent().next().stop().slideToggle(); // show the contents
			
		},
		render: function() {
			taskCollector.fetch({ success: function() {
					// data for the task menu or list
					var taskList = _.template(taskMenu, {tasks: taskCollector.attributes, _:_});
					$(".profile-menu").html(taskList);
					$('.contents').html(createMainView);
					
					guestList.fetch({ success: function() {
							$(".gs-contents").html(_.template(listGuestAccom, $.extend(guestList.attributes, {_:_})));
							// add border bottom to the last list
							var gl_length = $(".guest-list").length; // guest list length or number div elements
							if( gl_length ) $(".guest-list:last").css({"border-bottom": "1px solid #AAAAAA"});
							cleanVacantRooms.fetch();
						}
					});
				}
			});
		}
	});
	return new task_view;
});
