define([
    'jquery',
    'underscore',
    'backbone',
    'baseURL',
    'jQ-plugins/printElement',
    'text!tpl/task/hotel/dsales_or/dsales_or.html'
],function($,_,Backbone,base_url, printElement,stocksList){
    var sales_cnv = {
         getallitems: function(container) {
            $.post(base_url+"hotel/dsales_or",{
                dir: "all_items"
            },function(data){
                container.html(data.res);
            },"json");
         },
         getallsales: function(container) {
            $.post(base_url+"hotel/dsales_or",{
                dir: "all_sales"
            },function(data){
                container.html(data.res);
            },"json");
         },
         getalldsales: function(container){
             container.html($('<div class="loading-style1"></div>'));
             $.post(base_url+"hotel/dsales_or",{
                 dir: "daily_sales"
             }, function(data){
                 container.html(data.res).append($('<div class="pagination to-right"></div>').html(data.pagination));
             },"json");
         }
        
    };
   
$("body").on("click","input[name$=from], input[name$=to]",function(){
    $(this).datepicker({dateFormat: 'mm-dd-yy',changeYear: true, changeMonth: true, yearRange:'1940:+10'});
    $(this).datepicker({showOn: 'focus'}).focus();
});

$("body").on("click","button[name$=print_page]",function(){
   $("div.OR-view").printElement({
            pageTitle: "LARRY'S Place Official Receipt",
            overrideElementCSS:['./res/css/printbox.css'],
            printBodyOptions:{styleToAdd:'margin-top: 150px !important;'}
            });             
});
   
    //delete items
     $("body").on("click","label.cancel-item",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"hotel/dsales_or",{
            dir: "delete_items",
            pid: st_no
        },function(data){
            $('div.dsales').html(data.res);
        },"json");
    });
 
 //toggle physical inv
   $("body").on("click","div.item", function(){
       
       if($(this).hasClass("active")) {
           $(this).attr("class","i");
       }else {
           $("div.item").removeClass('active');
           switch($(this).attr("title")) {
               case "Item Name":$("").slideDown(5);
           }
           $(this).addClass('active');
       }
   });
   //toggle update checklist item
   $("body").on("click","a.updatepsyinv", function(){
       if($(this).hasClass("active")) {
           $(this).attr("class","ui");
       }else {
           $("a.updatepsyinv").removeClass('active');
           switch($(this).attr("title")) {
               case "Update Psy Inv":$("div#psy-inv-update").slideDown();
                   $('div.updatepys').show(5);
                   
           }
           $(this).addClass('active');
       }
   });
    //add item
    $("body").on("click","div.i",function(e){
        e.preventDefault();
        $(this).formvalidate(function(data){
            if(data.result===false) {
                $('div.rq').html("A Required Filled is Empty.").show(3000);
                $('div.rq').html("A Required Filled is Empty").hide(1000);  
       } else {
                var infos = {
                    item: $("form#additem input[name$=item]").val(),
                    price: $("form#additem input[name$=price]").val(),
                    total_sales: $("form#additem input[name$=isales]").val()
                }
                $.post(base_url+"ops/lp_onduty_dsales",{
                    dir: "add_item_sales",
                    data: {
                        "item": infos.item,
                        "price": infos.price,
                        "total_sales": infos.total_sales
                    }
                },function(data){
                    if(data==="added") { 
                        
                         sales_cnv.getallsales($('div.dsales'));
                         sales_cnv.getallitems("",$('div.dyn_content'));
                         
                    } else {
                        $('div.rq').html("The inventory "+ infos.item + " is already exist.").show(3000);
                        $('div.rq').html("The inventory "+ infos.item + " is already exist.").hide(3000);
                        
                    }
                });
            }
        });
    });
   
   //delete items
     $("body").on("click","a.item-delete",function(){
        var st_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"hotel/dsales_or",{
            dir: "delete_items",
            pid: st_no
        },function(data){
             $('div.item-all-show').html(data.res);
        },"json");
    });
   
    //initialize report
   $("body").on("click","button.view-or", function(){
        var or_no = $(this).attr("id").split("_",2)[1];
        $.post(base_url+"hotel/dsales_or", {
            dir: "get_details",
            bid: or_no
        },function(data){ 
            $("#OR-view label.pass-sales-id").val(data.dsales.dsales_id).html(data.dsales.dsales_id);
            $("#OR-view label.sales-date").val(data.dsales.date_added).html(data.dsales.date_added);
            $("#OR-view label.total-sum").val(data.dsales.ts).html(data.dsales.ts);
            $(this).facebox("div.view-or",{show:true});
        },"json");
    });
    
    //search mode
  /*  $("body").on("change","input[name$=from]", function(){
        if($(this).val()!="" && $("input[name$=to]").val()!=""){
          var param = {
              from : $("input[name$=from]").val(),
              to : $("input[name$=to]").val()
          }
          sales_cnv.getalldsales($('div.alldsales'), param);
        }
    });
    $("body").on("change","input[name$=to]", function(){
        if($(this).val()!="" && $("input[name$=from]").val()!=""){
            var param = {
                from : $("input[name$=from]").val(),
                to : $("input[name$=to]").val()
            }
            sales_cnv.getalldsales($('div.alldsales'), param);
        }
    });*/
    //close update pyd
     $("body").on("click","div.additem",function(){
        
             $('div.additem').hide(1000);
       
    });
   
  //pagination
    $("body").on("click","div.pagination a",function(e){
        e.preventDefault();
        $.post($(this).attr("href"),{
        },function(data){
            var rcv = $.parseJSON(data);
            $('div.alldsales').html(rcv.res).append($('<div class="pagination to-right"></div>').html(rcv.pagination));
        });
    });
  
    var stock_list = Backbone.View.extend({
        el: $('div#container'),
        initialize: function() {
            
        },
        events: {
            
        },
        render: function() {
      
            $('div.contents').html(stocksList);
            sales_cnv.getallitems($('div.sales-price'));
            sales_cnv.getallsales($('div.dsales'));
            sales_cnv.getalldsales($('div.alldsales'));
            
        }
    });
    return new stock_list;
}); 