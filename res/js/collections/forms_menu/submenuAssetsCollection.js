define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
	var subs = Backbone.Model.extend({});
	var formSubsCollection = Backbone.Collection.extend({
		model: subs
	});
	
	var subs1 = new subs();
	var subs2 = new subs();
	var subs3 = new subs();
	var subs4 = new subs();
	
	subs1.set({title: "Asset List", href: "#/assets/asset-list"});
	subs2.set({title: "Transfered Assets", href: "#/assets/transfered"});
	subs3.set({title: "Returned Assets", href: "#/assets/returned"});
	subs4.set({
		title: "Accountable Assets", 
		href: "#/assets/accountable-asset",
		img: "../res/img/ui/drop_icon.png",
		submenu: {
			Transfered: "#/assets/accountable-asset/transfered", 
			Assigned: "#/assets/accountable-asset/assigned"
		}
	});
	
	return new formSubsCollection([subs1, subs2, subs3, subs4]);
	//return new formSubsCollection([subs1, subs2, subs3]);
});