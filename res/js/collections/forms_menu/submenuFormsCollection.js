define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
	var subs = Backbone.Model.extend({});
	var formSubsCollection = Backbone.Collection.extend({
		model: subs
	});
	
	var subs1 = new subs();
	var subs2 = new subs();
	var subs3 = new subs();
	var subs4 = new subs();
	subs1.set({
		title: "Leave", 
		href: "#/forms/leave",
		img: "../res/img/ui/drop_icon.png",
		submenu: {
			Form: "#/forms/leave/form", 
			"Leave List": "#/forms/leave/list",
			"Leave Summary": "#/forms/leave/summary"
		}
	});
	subs2.set({
		title: "SRS", 
		href: "#/forms/srs",
		img: "../res/img/ui/drop_icon.png",
		submenu: {
			Form: "#/forms/srs/form", 
			"SRS Status List": "#/forms/srs/list"
		}
	});
	
	subs3.set({
		title: "PRS", 
		href: "#/forms/prs",
		img: "../res/img/ui/drop_icon.png",
		submenu: {
			Form: "#/forms/prs/form",
			"PRS Status List": "#/forms/prs/list",
		}
	});
	
	subs4.set({
		title: "Petty Cash", 
		href: "#/forms/pty",
		img: "../res/img/ui/drop_icon.png",
		submenu: {
			Form: "#/forms/pty/form", 
			Pending: "#/forms/pty/pending",
			Approved: "#/forms/pty/approved"
		}
	});
	
	return new formSubsCollection([subs1, subs2, subs3, subs4]);
});