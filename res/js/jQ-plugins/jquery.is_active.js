/*
 * celi-andro
 */
define(['jquery'], function($){
	$.is_active = function(user_settings, callback) {
		//$.isActive_displayed = false; // check if the the message is already displayed
		// default options
		var defaults = {
			title: "This is title here!",
			content : "This is contents here!"
		};
		var options = $.extend({}, defaults, user_settings);
		//console.log($.isActive_displayed);
		if( $.isActive_displayed ) return; // stop here
		else main();
		
		function main() {
			setup_container();
			center();
			
			$("input[type='button']", $(".is-active-LF")).each(function(index, element) {
				var $e = $(element);
				$e.live("click", function() {
					//console.log(typeof callback);
					if($e.hasClass("no-click") && (typeof callback == 'function'))
						callback.call(this, false);
					else if($e.hasClass("login-click") && (typeof callback == 'function'))
						callback.call(this, true);
					
					// remove elements
					$(".is-active-login").fadeOut(500, function() {
						$(this).remove();
						$(".is-active-background").fadeTo("slow", 0.1, function() {
							$(this).remove();
							destroy();
						});
					});
				});
			});
			// center the is_active box on window resize
			$(window).bind("resize", function(){center();});
		}
		
		function setup_container() {
			var html = "<div class='is-active-login'>";
				html += "<div class='is-active-login-header container-header'>"+options.title+"</div>";
				html += "<div class='is-active-LC'>"+options.content+"</div>";
				html += "<div class='is-active-LF'>" +
							"<input class='login-click css3-button' type='button' value='Login'>"+
							//"<input class='no-click css3-button' type='button' value='No'>"+
							"<div class='clr'></div>"+
						"</div>";
			   html += "</div>";
			var bground = "<div class='is-active-background'></div>";
			$("body").append(html); $("body").append(bground); //insert the container
			$(".is-active-LF").css({background: "#f2f2f2"});
			$.isActive_displayed = true; //set the display to true
		}
		
		function center() {
			var box = $(".is-active-login"), bg = $(".is-active-background");
			var winH = $(window).height(), winW = $(window).width();
			var docH = $(document).height(), docW = $(document).width();
			
			bg.css({width: docW, height: docH}).fadeTo("slow", 0.3);
			box.css({top: winH/2 - ((box.height() / 2) + 200 ), left: winW/2 - (box.width() / 2)}); // center screen
		}
		
		function destroy() {
			//delete variables here
			delete options;
			delete $.isActive_displayed;
		}
		
		return this.each(function(){
			//console.log("test");
		});
	}
	
});