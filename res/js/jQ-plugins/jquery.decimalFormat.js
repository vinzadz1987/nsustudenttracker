(function($){
    /*
     * Decimal format value
     * 
     * @params:
     *      obj: element to implement formatting;
     *      e: keydown event object;
     *      dotposition: desired dot position from the right;
     * 
     * Created by: celi-rombert
     * Friday, 23 September 2011 - 11:52 PM
     * 
    */
    $.decimalFormat = function(obj, e, dotposition) {
        var formatted = "";
        var unform = "";
        var chars = new Array();
        var foolCode = null;
        //special attention for keyCode on num pad numbers;
        switch(e.keyCode) {
            case 96: foolCode = 48; break;
            case 97: foolCode = 49; break;
            case 98: foolCode = 50; break;
            case 99: foolCode = 51; break;
            case 100: foolCode = 52; break;
            case 101: foolCode = 53; break;
            case 102: foolCode = 54; break;
            case 103: foolCode = 55; break;
            case 104: foolCode = 56; break;
            case 105: foolCode = 57; break;
            default: foolCode = e.keyCode;
        }
        //Prevent default delete and backspace we have to manage that a bit differently.
        if(foolCode == 46 || foolCode == 8) {
            e.preventDefault();
            unform = obj.val().substring(0,obj.val().length-1);
            if(unform.length > (dotposition+1) && dotposition !== 0) {
                chars = unform.split("");
                for(var i=chars.length-1; i>=0; i--) {
                    if(i==chars.length-(dotposition+1)) {
                        chars[i+1] = chars[i];
                        chars[i] = ".";
                    }
                }
                formatted = chars.join("");
            } else {
                chars.splice(unform.indexOf("."),1);
                formatted = unform;
            }
        }
        else if(foolCode >= 48 && foolCode <= 57) {
            e.preventDefault();
            unform = (obj.val()=='0.00') ? String.fromCharCode(foolCode) : obj.val()+String.fromCharCode(foolCode);
            if(unform.length > (dotposition) && dotposition !== 0) {
                unform = unform+String.fromCharCode(foolCode);
                chars = unform.split("");
                if(unform.indexOf(".")!=-1) {
                    chars.splice(unform.indexOf("."),1);
                }
                for(var i=chars.length-1; i>=0; i--) {
                    if(i==chars.length-(dotposition+1)) {
                        chars[i+1] = chars[i];
                        chars[i] = ".";
                    }
                }
                formatted = chars.join("");
            } else {
                formatted = unform;
            }
        }
        else if(e.keyCode == 9 || e.keyCode == 13 || e.keyCode == 45) {
            //allow execution of tab, enter and insert
            formatted = obj.val();
        }
        else {
            //everything else is not enabled
            e.preventDefault();
            formatted = obj.val();
        }
        obj.val(formatted);
    }
})(jQuery);