(function($){
    $.customConfirm = function(message,callback) {
        var bg = $('<div class="customConfirmBG"></div>');
        var box = $('<div class="customConfirmBox"></div>');
        var documentHeight = $(document).height();
        var confirm_btn = $('<button class="blue-button" name="confirm">Ok</button>');
        bg.css({
            "width":"100%", "height":"100%", "background-color": "white",
            "opacity":"0.7", "z-index":"2001", "position":"absolute",
            "top":"0","left":"0"});
        box.css({
            "border": "1px solid #e0e0e0", "top":"30%","left":"50%",
            "width":"300px", "min-height":"80px", "position":"absolute",
            "margin-left":"-150px","z-index":"2002", "text-align":"center",
            "vertical-align":"middle","font-size":"12px", "font-weight":"700",
            "background-color":"white", "padding-top":"40px"
        });
        bg.height(documentHeight).prependTo('body').fadeIn(100,function(){
            box.prependTo('body')
            .html("<p>"+message+"</p>").append(confirm_btn)
            .stop(true,true).fadeIn(100);
        });
        if(bg.length && box.length) {
            bg.unbind(); confirm_btn.unbind();
            bg.bind('click',function(){
                $(this).stop(true,true).fadeOut(100,function(){
                    box.stop(true,true).fadeOut(100,function(){
                        bg.remove(); box.remove();
                    });
                });
            });
            confirm_btn.bind('click',function(){
                if(typeof callback === "function") {
                    callback.call(this,true);
                }
                bg.stop(true,true).fadeOut(100,function(){
                    box.stop(true,true).fadeOut(100,function(){
                        bg.remove(); box.remove();
                    });
                });
            });
        }
    }
})(jQuery);
(function($){
    $.customAlert = function(message) {
        var bg = $('<div class="customAlertBG"></div>');
        var box = $('<div class="customAlertBox"></div>');
        var documentHeight = $(document).height();
        bg.css({
            "width":"100%", "height":"100%", "background-color": "white",
            "opacity":"0.7", "z-index":"2001", "position":"absolute",
            "top":"0","left":"0"});
        box.css({
            "border": "1px solid #e0e0e0", "top":"20%","left":"50%",
            "width":"300px", "min-height":"80px", "position":"absolute",
            "margin-left":"-150px","z-index":"2002", "text-align":"center",
            "vertical-align":"middle","font-size":"12px", "font-weight":"700",
            "background-color":"white", "padding-top":"40px"
        });
        bg.height(documentHeight).prependTo('body').fadeIn(100,function(){
            box.prependTo('body')
            .html("<p>"+message+"</p>")
            .stop(true,true).fadeIn(100);
        });
        if(bg.length && box.length) {
            bg.unbind();
            bg.bind('click',function(){
                $(this).stop(true,true).fadeOut(100,function(){
                    box.stop(true,true).fadeOut(100,function(){
                        bg.remove(); box.remove();
                    });
                });
            });
        }
    }
})(jQuery);