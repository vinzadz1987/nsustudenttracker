(function($) {
    jQuery.fn.litebox = function(pop_elem, settings) {
        var options = $.extend({
            toDisplay: (pop_elem) ? pop_elem: "",
            show: false, close: false
        },settings);
        return this.each(function() {
            var elem = $(this);
            var clone_parent = $(options.toDisplay).parent();
            var clone = $(options.toDisplay).clone();
            $('div.litebox-bg').height($(document).height());
            if(options.show===false) {
                elem.click(function(){
                    if($('div.litebox-bg').length && $('div.litebox').length) {
                        $('div.litebox-bg').height($(document).height()).stop(true,true).fadeIn(200,function(){
                            $('div.litebox')
                            .html($(options.toDisplay).show())
                            .stop(true,true).fadeIn(200,function(){
                                $('div.litebox-bg').height($(document).height());
                            });
                        });
                    } else {
                        $('<div class="litebox-bg"></div>').height($(document).height())
                        .prependTo('body')
                        .stop(true,true).fadeIn(200,function(){
                            $('<div class="litebox"></div>')
                            .prependTo('body')
                            .html($(options.toDisplay).show())
                            .stop(true,true).fadeIn(200,function(){
                                $('div.litebox-bg').height($(document).height());
                            });
                        })
                        .click(function() {
                            clone_parent.append(clone.hide());
                            $('div.litebox').empty().stop(true,true).fadeOut(200);
                            $(this).stop(true,true).fadeOut(200,function(){
                                $('div.litebox-bg , div.litebox').remove();
                            });
                        });
                    }
                });
            }
            if(options.show===true) {
                if($('div.litebox-bg').length && $('div.litebox').length) {
                    $('div.litebox-bg').height($(document).height()).stop(true,true).fadeIn(200,function(){
                        $('div.litebox')
                        .html($(options.toDisplay).show())
                        .stop(true,true).fadeIn(200,function(){
                            $('div.litebox-bg').height($(document).height());
                        });
                    });
                } else {
                    $('<div class="litebox-bg"></div>').height($(document).height())
                    .prependTo('body')
                    .stop(true,true).fadeIn(200,function(){
                        $('<div class="litebox"></div>')
                        .prependTo('body')
                        .html($(options.toDisplay).show())
                        .stop(true,true).fadeIn(200,function(){
                            $('div.litebox-bg').height($(document).height());
                        });
                    })
                    .click(function() {
                        clone_parent.append(clone.hide());
                        $('div.litebox').empty().stop(true,true).fadeOut(200);
                        $(this).stop(true,true).fadeOut(200,function(){
                            $('div.litebox-bg , div.litebox').remove();
                        });
                    });
                }
            }
            if(options.close===true) {
                $('div.litebox-bg').click();
            }
            $(document).bind('DOMSubtreeModified',function(){
                if($(document).height()>$('div.litebox-bg').height()) {
                    $('div.litebox-bg').height($(document).height() + 200);
                }
            });
        });
    }
})(jQuery);