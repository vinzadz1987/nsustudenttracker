/*
 * celi-andro-programmer
 */
define(['jquery'], function($) {
	
	var methods = {
		init: function(user_settings, callback) {
			methods._process_data(user_settings, callback);
			///console.log($.lboxoptions);
			var $lbox_footer = $(".lbox-footer"), $data = {}; $.lboxoptions.response = false;
			$("input[type='button']", $lbox_footer).each(function(index, element){
				var $e = $(element);
				$e.live("click", function() {
					if( $(this).hasClass("lbox-no") || $(this).hasClass("lbox-close") || $(this).hasClass("lbox-cancel") ) {
						$.lboxoptions.response = false;
					} else if($(this).hasClass("lbox-submit") || $(this).hasClass("lbox-yes")) {
						$.lboxoptions.response = true;
					}
					if( $.lboxoptions.response ) {
						$('input[type="checkbox"]:checked, input[type="input"], input[type="radio"], textarea, select', $('.lbox-contents')).each(function(index, element) {
							var $element = $(element);
							$data[$element.attr("name")] = $element.val();
						});
					}
					
					// return the response to the callback
					if(typeof callback === 'function') {
						var response = ($.lboxoptions.response && !$.isEmptyObject($data) ) ? $data : false;
						callback.call(this, response);
					}
					
					$('.lbox').fadeOut(500, function() {
						$(this).remove();
						$('.lbox-background').fadeTo("slow", 0.1, function() {
							$(this).remove();
							methods.destroy();
						});
					});
				});
			});
		},
		_build_box: function() {
			var lbox = $("<div>").addClass("lbox").attr('id', 'lbox'), header = $("<div>").addClass("lbox-header container-header");
			var contents = $("<div>").addClass("lbox-contents"), footer = $("<div>").addClass("lbox-footer");
			var background = $("<div>").addClass("lbox-background"), clear = $("<div>").addClass("clr");
			
			lbox.append(header).append(contents).append(footer);
			lbox.appendTo("body");
			clear.appendTo(footer);
			background.insertAfter(lbox);
			
			// add the title and the content
			header.html($.lboxoptions.title);
			contents.html($.lboxoptions.content);
			
			// add necessary buttons
			if( $.lboxoptions.buttons.no ) {
				var no = $("<input>").addClass("lbox-no css3-button").attr({"type": "button", "value": "No"});
				no.prependTo(footer);
			}
			if( $.lboxoptions.buttons.yes ) {
				var yes = $("<input>").addClass("lbox-yes css3-button").attr({"type": "button", "value": "Yes"});
				yes.prependTo(footer);
			}
			if( $.lboxoptions.buttons.close ) {
				var close = $("<input>").addClass("lbox-close css3-button").attr({"type": "button", "value": "Close"});
				close.prependTo(footer);
			}
			if( $.lboxoptions.buttons.cancel ) {
				var cancel = $("<input>").addClass("lbox-cancel css3-button").attr({"type": "button", "value": "Cancel"});
				cancel.prependTo(footer);
			}
			if( $.lboxoptions.buttons.submit ) {
				var submit = $("<input>").addClass("lbox-submit css3-button").attr({"type": "button", "value": "Submit"});
				submit.prependTo(footer);
			}
			
			methods._set_background();
			$(window).resize(function(){methods._set_background();});
			
			// set box displayed to true
			$.lboxoptions.isbox_displayed = true;
		},
		_process_data: function(user_settings, callback) {
			if( ! $.lboxoptions ) $.lboxoptions = $.extend({}, $.lboxdefaults, user_settings);
			if( ! $.lboxoptions.isbox_displayed ) methods._build_box();
		},
		_set_background: function() {
			var docH = $(document).height(), docW = $(document).width();
			var winH = $(window).height(), winW = $(window).width();
			var lbox = $('.lbox');
			
			$('.lbox-background').css({"width": docW, "height": docH}).fadeTo("slow",0.5);
			lbox.css({"top": winH/2 - (lbox.height() / 2), "left": winW/2 - (lbox.width() / 2)});
		},
		removeData: function() {
			$('.lbox').fadeOut(500, function() {
				$(this).remove();
				$('.lbox-background').fadeTo("slow", 0.1, function() {
					$(this).remove();
					methods.destroy();
				});
			});
		},
		destroy: function() {
			if( !$.isEmptyObject( $.lboxoptions ) ) delete $.lboxoptions;
		}
	};
	
	$.lbox  = function(method) {
		var lbox = "";
		if( methods[method] ) lbox = methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		else  if ( typeof method === "object" || !method ) lbox = methods.init.apply(this, arguments)
		else $.error("Method "+method+" does not exists in lbox jquery plugin");
		return lbox;
	};
	
	$.lboxdefaults = {
		title: "Hi there:",
		content: "How may I help you?",
		isbox_displayed: false,
		buttons: {
			yes: false,
			no: false,
			close: true,
			submit: false,
			cancel: false
		},
		response: false
	};
	
});