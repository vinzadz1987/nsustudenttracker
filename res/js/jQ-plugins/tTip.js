define(['jquery'], function($) {
/*
 * jQuery Tooltip
 * Added by: celi-andro-programmer
 * Date: Thursday, 13 October 2011 - 10:28AM
 */
    $.fn.tTip = function( user_settings ) {
        var defaults = {
            message: "Okay pa sa alright!",
            css: {
                "color": "#fff",
                "border": "1px solid #BBB",
                "padding": "10px",
                "position": "absolute",
                "z-index": "9999",
                "background": "#000",
                "border-radius": "3px",
                "-moz-border-radius": "3px",
                "-webkit-border-radius": "3px"
            }
        };
        if( user_settings ) $.extend(defaults, user_settings);
        var tTip = function(t) {
            var msg = defaults.message;
            var tip = $("<div>").addClass("tTip").html(msg);
            tip.css(defaults.css);
            tip.insertAfter(t);
        };
        return this["live"]("mouseover mouseout mousemove", function(e) {
            if(e.type == 'mouseover') tTip($(this));
            if(e.type == 'mouseout' ) $(".tTip").remove();
            if(e.type == 'mousemove') {
                // code from sohtanaka.com
                var mouseX = e.pageX + 10, mouseY = e.pageY + 10;
                var tW = $(".tTip").width(), tH = $(".tTip").height();
                var tX = $(window).width() - (mouseX + tW);
                var tY = $(window).height() - (mouseY + tH);

                if(tX < 20) mouseX = mouseX - tW - 40;
                if(tY < 20) mouseY = mouseY - tH - 40;

                $(".tTip").css({left: mouseX, top:mouseY});
            }
        });
        /*
        return this.each(function(){
            var $this = $(this);
            $this.hover(function(){
                tTip($this);
            },function(){
                $(".tTip").remove();
            }).mousemove(function(e){
                var mouseX = e.pageX + 20, mouseY = e.pageY + 20;
                var tW = $(".tTip").width(), tH = $(".tTip").height();
                var tX = $(window).width() - (mouseX + tW);
                var tY = $(window).height() - (mouseY + tH);

                if(tX < 20) mouseX = mouseX - tW - 55;
                if(tY < 20) mouseY = mouseY - tH - 55;

                $(".tTip").css({left: mouseX, top:mouseY});

            });
        });
        */
    };
});