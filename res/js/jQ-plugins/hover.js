define(['jquery'], function($) {
	$("#main-menu ul li").on("click", function() {
		$("#main-menu ul li").removeClass('active-menu');
		$(this).addClass('active-menu');
	});
	$("#sub-menu ul li").on("mouseover mouseout", function(e) {
		(e.type === 'mouseover') ? $(this).find("ul").show() : $(this).find("ul").hide();
	});
	$('.profile-menu ul li').on("click", function() {
		$(".profile-menu ul li").removeClass('active-pmenu');
		$(this).addClass('active-pmenu');
	});
	$('.sub-tasks ul li').on("click", function() {
		$(".sub-tasks ul li").removeClass('active');
		$(this).addClass('active');
	});
});