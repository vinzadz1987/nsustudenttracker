/*
 * andro-programmer
 */
define(['jquery', 'underscore', 'backbone', 'jsHelper'], function($, _, Backbone, jsHelper) {
	var methods = {
		init: function(user_settings, callback) {
			//return this.each(function(){
				if( !$.bs_opts ) $.bs_opts = $.extend({}, $._defaults, user_settings);
				if( !$.bs_opts.isboxDisplayed ) methods._displayBox();
				//console.log($.bs_opts);
				$.dataResponse = {resultIds:[]};
				$.callback = callback;
				
				if($.bs_opts.events) {
					for( var x in $.bs_opts.events ) {
						var xx = x.split(" ");
						var selector = xx[1], eventName = xx[0];
						$("body").delegate(selector, eventName, $.bs_opts[$.bs_opts.events[x]]);
						//console.log(typeof $.bs_opts[$.bs_opts.events[x]]);
					}
				}
			//});
		},
		_displayBox: function() {
			
			var box_step = $("<div>").addClass("box-step");
			var header = $("<div>").addClass("box-step-header container-header");
			var contents = $("<div>").addClass("box-step-contents");
			var footer = $("<div>").addClass("box-step-footer");
			var background = $("<div>").addClass("box-step-background");
			var clear = $("<div>").addClass("clr");
			
			box_step.append(header).append(contents).append(footer);
			methods._setContents(); // add contents
			
			box_step.appendTo("body");
			clear.appendTo(footer);
			background.insertAfter(box_step);

			// display the box on the center
			methods._boxPosition();
			// listen to the window resize event and reposition the box
			$(window).bind("resize", function(){methods._boxPosition();});
			
			// set box displayed to true
			$.bs_opts.isboxDisplayed = true;
		},
		_setHeader: function() {
			var header = $(".box-step-header");
			// check whether the title is an array using the underscore function isArray()
			if( _.isArray($.bs_opts.title) ) {
				header.html($.bs_opts.title[$.bs_opts.contentIndex]); // add the header title based on the index of the content
			} else header.html($.bs_opts.title);
			
		},
		_setContents: function() {
			$(".box-step-footer").html("<div class='clr'></div>"); // clear the contents
			var contentLength = $.bs_opts.contents.length;
			var contentIndex = $.bs_opts.contentIndex;
			
			$.ajax({
				type: "POST",
				url: $.bs_opts.baseURL+$.bs_opts.contents[contentIndex]+"?t=d"+Math.floor(Math.random()*100),
				beforeSend: function() {
				},
				success: function(dataView) {
					//console.log($.extend({}, $.extend({},$.bs_opts.data, $.dataResponse), {_:_, jsHelper: jsHelper}));
					var data = _.template( dataView, $.extend({}, $.extend({},$.bs_opts.data, $.dataResponse), {_:_, jsHelper: jsHelper})  );
					//console.log($.extend({}, $.extend({},$.bs_opts.data, $.dataResponse), {_:_, jsHelper: jsHelper}));
					$(".box-step-contents").html(data); // display the data using the underscore template
					//$(".disp-assets-transret").lionbars();
					if( $.bs_opts.lionbars ) {
						//$($.bs_opts.lionbars).lionbars();
						$.each($.bs_opts.lionbars, function(i,v){
							$(v).lionbars();
						});
					}
					
					methods._setHeader(); // add the title
					methods._insertButtons(); // determine which button to insert/ add
					methods._boxPosition(); // reposition the box
					
					var $el = (".box-step-contents");
					var elements = $.bs_opts.selector;
					
					if( elements !== "" ) {
						$(elements, $el).each( function(key, val) {
							var $v = $(val);
							$v.live("click", function() {
								// console.log($v);
								var id = $v.attr("id");
								if( $v.hasClass("item-disabled") && $v.hasClass("item-italic") ) {
									$v.removeClass("item-disabled").removeClass("item-italic");
									for(var x in $.dataResponse.resultIds ) {
										if( $.dataResponse.resultIds[x] === id )
											$.dataResponse.resultIds.splice(x, 1); // remove the asset id in the array
									}
								} else {
									$.dataResponse.resultIds.push(id); // add the asset id
									$v.addClass("item-disabled item-italic");
								}
							});
						});
					}
					methods._buttonClicked(); // determine which button was clicked
				}
			});
		},
		_buttonClicked: function( ) {
			var button_clicked = $(".box-step-footer");
			$("input[type='button']", button_clicked).each(function(index, element) {
				var $e = $(element);
				//console.log($(this).attr("class"));
				$e.live("click", function() {
					//console.log($e.attr("class"));
					if( $e.hasClass("box-step-next") && !($.bs_opts.contentIndex === ($.bs_opts.contents.length - 1) ) ) {
						$.bs_opts.contentIndex++; // move forward
						$.bs_opts.response = false;
					} else if( $e.hasClass("box-step-back") && (($.bs_opts.contents.length - 1) !== 0) ) {
						$.bs_opts.contentIndex--; // move backward
						$.bs_opts.response = false;
					} else if( $e.hasClass("box-step-cancel") ) $.bs_opts.response = true;
					else if( $e.hasClass("box-step-submit") ) $.bs_opts.response = true;
					
					if( $.bs_opts.response ) {
						//console.log(element.tagName.toLowerCase());
						if( !$e.hasClass("box-step-cancel")  && typeof $.callback === 'function') {
							$.callback.call(this, $.dataResponse.resultIds);
						}

						
						$(".box-step").fadeOut(500, function() {
							$(this).remove();
							$('.box-step-background').fadeTo("slow", 0.1, function() {
								$(this).remove();
								methods.destroy();
								delete $.dataResponse;
								delete $.callback;
							});
						});
						
						//console.log($.dataResponse);
						
					} else methods._setContents(); // display the right contents
				});
			});
		},
		_boxPosition: function() {
			var box = $(".box-step"), background = $(".box-step-background");
			var docH = $(document).height(), docW = $(document).width();
			var winH = $(window).height(), winW = $(window).width();
			
			background.css({width: docW, height: docH}).fadeTo("slow", 0.8);
			// put the box in the center of the screen
			box.css({top: winH/2 - ( box.height() / 2 ), left: winW/2 - ( box.width() / 2 )});
		},
		_insertButtons: function() {
			
			var contentLength = $.bs_opts.contents.length;
			var contentIndex = $.bs_opts.contentIndex;
			
			if( contentIndex == 0 ) {
				var cancel = $("<input>").addClass("box-step-cancel css3-button")
					.attr({type: "button", value: "Cancel"});
				var next = $("<input>").addClass("box-step-next css3-button")
					.attr({type: "button", value: "Next"});
				
				cancel.prependTo(".box-step-footer");
				next.prependTo(".box-step-footer");
				
			} else if( contentIndex == (contentLength - 1) ) {
				var back = $("<input>").addClass("box-step-back css3-button")
					.attr({type: "button", value: "Back"});
				var submit = $("<input>").addClass("box-step-submit css3-button")
					.attr({type: "button", value: "Submit"});
				
				back.prependTo(".box-step-footer");
				submit.prependTo(".box-step-footer");
			} else {
				var back = $("<input>").addClass("box-step-back css3-button")
					.attr({type: "button", value: "Back"});
				var next = $("<input>").addClass("box-step-next css3-button")
					.attr({type: "button", value: "Next"});
				
				back.prependTo(".box-step-footer");
				next.prependTo(".box-step-footer");
			}
		},
		destroy: function() {
			//return this.each(function() {
				// remove or delete options 
				( $.bs_opts && !$.isEmptyObject($.bs_opts) ) ? delete $.bs_opts : "";
			//});
		}
	};
	
	$.box_step = function(method) {
		var box = "";
		if( methods[method] ) box = methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		else if( typeof method === 'object' || !method ) box = methods.init.apply(this, arguments);
		else $.error("Method "+method+" does not exists in box_step plugin");
		return box;
	};
	
	$._defaults = {
		title: [],
		contents: [],
		data: {},
		buttons: { back: true, next: true, submit: true, cancel: true },
		contentIndex: 0,
		selector: "", // the selectors ex('div .sample1 #sample2 input[type="text"]')
		response: false,
		isboxDisplayed: false,
		baseURL: ""
	};
});