define(['jquery'], function($) {
	/*celi-andro*/
	$.fn.Oaccordion = function() {
		return this.each(function(){
			var $this = $(this);
			$this.find('li > div.oa-head').next().hide();
			$this.find('li > div.oa-head > a').live('click', function(){
				var $head = $(this).parent();
				if( $head.next().is(':visible') && $head.next().is('div') ) {
					$head.next().slideUp('normal');
					$(this).html("+");
				} else if( !$head.next().is(':visible') && $head.next().is('div') ) {
					$this.find('li > div.oa-head > a').html("+");
					$this.find('li > div.oa-content:visible').slideUp('normal');
					$head.next().slideDown('normal');
					$(this).html("-");
				}
			});
		});
	};
});