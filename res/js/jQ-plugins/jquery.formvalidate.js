(function($) {
    $.fn.formvalidate = function(callback) {
        var res = {
            result: true,
            message: ""
        }
        return this.each(function(){
            res.result = true;
            res.message = "";
            $("input, select", this).each(function(i,val){
                var field = $(val);
                if(field.hasClass('fld-required')) {
                    res.result = (field.val()=="") ? false:true;
                    res.message = "A required field is empty";
                } else {
                    //a series of validations
                }
                if(res.result===false) {
                    return false;
                    //exit loop right away when an element didn't pass validation.
                } else {
                    return true;
                }
            });
            if(typeof callback =="function"){
                callback.call(this,res);
                return false;
            } else {
                return res;
            }
            
        });
    }
})(jQuery);