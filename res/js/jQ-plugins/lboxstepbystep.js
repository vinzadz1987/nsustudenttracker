/*
 * celi-andro-programmer
 */
define(['jquery', 'underscore', 'backbone', 'baseURL'], function($, _, Backbone, baseURL) {
	var methods = {
			init: function(user_settings, callback) {
				//console.log($.lsOptions);
				if( ! $.lsOptions ) $.lsOptions = $.extend({}, $.lsdefault, user_settings);
				if( ! $.lsOptions.isDisplayed ) methods._build_box();
				// increment the index if the next button was clicked
				
			},
			_contents: function() {
				//$(".lbox-contents").html(""); // clear the contents
				$(".lbox-footer").html("<div class='clr'></div>"); // clear the contents
				var contentLength = $.lsOptions.contents.length;
				var contentIndex = $.lsOptions.contentIndex;
				$.ajax({
					type: "POST",
					url: baseURL+"res/tpl/assets/asset-return-template/"+$.lsOptions.contents[contentIndex]+"?t=data",
					success: function(data) {
						
						// add data here and append it to the div
						var d = _.template(data, $.extend({}, $.lsOptions.data, {_:_}));
						$(".lbox-contents").html(d);
						$(".disp-assets-transret").lionbars();
						
						
						console.log($.lsOptions.data);
						
						// determine which buttons that needs to be used
						if( contentIndex == 0 ) {
							var cancel = $("<input>").addClass("lbs-cancel css3-button")
								.attr({type: "button", value: "Cancel"});
							var next = $("<input>").addClass("lbs-next css3-button")
								.attr({type: "button", value: "Next"});
							
							cancel.prependTo(".lbox-footer");
							next.prependTo(".lbox-footer");
							
						} else if( contentIndex == (contentLength - 1) ) {
							var back = $("<input>").addClass("lbs-back css3-button")
								.attr({type: "button", value: "Back"});
							var submit = $("<input>").addClass("lbs-submit css3-button")
								.attr({type: "button", value: "Submit"});
							
							back.prependTo(".lbox-footer");
							submit.prependTo(".lbox-footer");
						} else {
							var back = $("<input>").addClass("lbs-back css3-button")
								.attr({type: "button", value: "Back"});
							var next = $("<input>").addClass("lbs-next css3-button")
								.attr({type: "button", value: "Next"});
							
							back.prependTo(".lbox-footer");
							next.prependTo(".lbox-footer");
						}
						
						// make sure that the box is displayed on the center
						methods._set_background();
						
						var button_clicked = $(".lbox-footer");
						$("input[type='button']", button_clicked).each(function(index, element) {
							var $e = $(element);
							//console.log($(this).attr("class"));
							$e.live("click", function() {
								//console.log($e.attr("class"));
								if( $e.hasClass("lbs-next") && !($.lsOptions.contentIndex === ($.lsOptions.contents.length - 1) ) ) {
									$.lsOptions.contentIndex++; // move forward
									$.lsOptions.response = false;
								} else if( $e.hasClass("lbs-back") && (($.lsOptions.contents.length - 1) !== 0) ) {
									$.lsOptions.contentIndex--; // move backward
									$.lsOptions.response = false;
								} else if( $e.hasClass("lbs-cancel") ) $.lsOptions.response = true;
								else if( $e.hasClass("lbs-submit") ) $.lsOptions.response = true;
								
								var $el = (".lbox-contents");
								var elements = $.lsOptions.elements;
								var data_response = {};
								
								$(elements, $el).each(function(index, el) {
									
									var elmt = $(el);
									if( (elmt.attr("id") !== undefined) && ( elmt.attr("id") !== null  ) ) {
										//console.log(elmt.attr("id"));
										data_response[elmt.attr("id")] = ( elmt.is("div") ) ? elmt.html() : elmt.val();
									} else	$.error("Please specify an id");
								});
								
								$.extend($.lsOptions.data, data_response);
								//console.log($.lsOptions.data);
								
								
								if( $.lsOptions.response ) {
									//console.log(element.tagName.toLowerCase());
									$(".lbox").fadeOut(500, function() {
										$(this).remove();
										$('.lbox-background').fadeTo("slow", 0.1, function() {
											$(this).remove();
											methods.destroy();
										});
									});
								} else	methods._contents(); // display the right contents
							});
						});
					}
				});
			},
			_build_box: function() {
				var lbox = $("<div>").addClass("lbox"), header = $("<div>").addClass("lbox-header container-header");
				var contents = $("<div>").addClass("lbox-contents"), footer = $("<div>").addClass("lbox-footer");
				var background = $("<div>").addClass("lbox-background"), clear = $("<div>").addClass("clr");
				
				lbox.append(header).append(contents).append(footer);
				
				// add the title and the content
				header.html($.lsOptions.title);
				//contents.html($.lsOptions.contents);
				// add contents
				methods._contents();
				
				
				lbox.appendTo("body");
				clear.appendTo(footer);
				background.insertAfter(lbox);


				// header
				// contents
				// footer
				
				// display the box on the center
				methods._set_background();
				$(window).resize(function(){methods._set_background();});
				
				// set box displayed to true
				$.lsOptions.isDisplayed = true;
			},
			_set_background: function() {
				var docH = $(document).height(), docW = $(document).width();
				var winH = $(window).height(), winW = $(window).width();
				var lbox = $('.lbox');
				
				$('.lbox-background').css({"width": docW, "height": docH}).fadeTo("slow",0.5);
				lbox.css({"top": winH/2 - (lbox.height() / 2), "left": winW/2 - (lbox.width() / 2)});
			},
			destroy: function() {
				if( !$.isEmptyObject( $.lsOptions ) ) delete $.lsOptions;
			}
	};
	$.lboxstepbystep = function( method ) {
		var lbox = "";
		if( methods[method] ) lbox = methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		else if( typeof method === 'object' || !method ) lbox = methods.init.apply(this, arguments);
		else $.error("Method "+method+" does not exists in lboxstepbystep plugin");
		return lbox;
	};
	$.lsdefault = {
		title: "Hey",
		contents: ["Content1","content2"],
		buttons: {
			"back": true,
			"next": true,
			"submit": true,
			"cancel": true
		},
		data: {},
		elements: "",
		response: false,
		isDisplayed: false,
		contentIndex: 0
	};
});