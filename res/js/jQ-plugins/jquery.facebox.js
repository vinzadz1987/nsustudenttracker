(function($) {
    jQuery.fn.facebox = function(pop_elem, settings) {
        var options = $.extend({
            toDisplay: (pop_elem) ? pop_elem: "",
            show: false, close: false
        },settings);
        return this.each(function() {
            var elem = $(this);
            var clone_parent = $(options.toDisplay).parent();
            var clone = $(options.toDisplay).clone();
            $('div.facebox-bg').height($(document).height());
            if(options.show===false) {
                elem.click(function(){
                    if($('div.facebox-bg').length && $('div.facebox').length) {
                        $('div.facebox-bg').height($(document).height()).stop(true,true).fadeIn(200,function(){
                            $('div.facebox')
                            .html($(options.toDisplay).show())
                            .stop(true,true).fadeIn(200,function(){
                                $('div.facebox-bg').height($(document).height());
                            });
                        });
                    } else {
                        $('<div class="facebox-bg"></div>').height($(document).height())
                        .prependTo('body')
                        .stop(true,true).fadeIn(200,function(){
                            $('<div class="facebox"></div>')
                            .prependTo('body')
                            .html($(options.toDisplay).show())
                            .stop(true,true).fadeIn(200,function(){
                                $('div.facebox-bg').height($(document).height());
                            });
                        })
                        .click(function() {
                            clone_parent.append(clone.hide());
                            $('div.facebox').empty().stop(true,true).fadeOut(200);
                            $(this).stop(true,true).fadeOut(200,function(){
                                $('div.facebox-bg , div.facebox').remove();
                            });
                        });
                    }
                });
            }
            if(options.show===true) {
                if($('div.facebox-bg').length && $('div.facebox').length) {
                    $('div.facebox-bg').height($(document).height()).stop(true,true).fadeIn(200,function(){
                        $('div.facebox')
                        .html($(options.toDisplay).show())
                        .stop(true,true).fadeIn(200,function(){
                            $('div.facebox-bg').height($(document).height());
                        });
                    });
                } else {
                    $('<div class="facebox-bg"></div>').height($(document).height())
                    .prependTo('body')
                    .stop(true,true).fadeIn(200,function(){
                        $('<div class="facebox"></div>')
                        .prependTo('body')
                        .html($(options.toDisplay).show())
                        .stop(true,true).fadeIn(200,function(){
                            $('div.facebox-bg').height($(document).height());
                        });
                    })
                    .click(function() {
                        clone_parent.append(clone.hide());
                        $('div.facebox').empty().stop(true,true).fadeOut(200);
                        $(this).stop(true,true).fadeOut(200,function(){
                            $('div.facebox-bg , div.facebox').remove();
                        });
                    });
                }
            }
            if(options.close===true) {
                $('div.facebox-bg').click();
            }
            $(document).bind('DOMSubtreeModified',function(){
                if($(document).height()>$('div.facebox-bg').height()) {
                    $('div.facebox-bg').height($(document).height() + 200);
                }
            });
        });
    }
})(jQuery);