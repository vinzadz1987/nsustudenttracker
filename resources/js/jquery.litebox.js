(function($) {
    jQuery.fn.litebox = function(pop_elem, settings) {
        var options = $.extend({
            toDisplay: (pop_elem) ? pop_elem: ""
        },settings);
        return this.each(function() {
            var elem = $(this);
            var clone_parent = $(options.toDisplay).parent();
            var clone = $(options.toDisplay).clone();
            elem.click(function(){
                if($('div.litebox-bg').length && $('div.litebox').length) {
                    $('div.litebox-bg').fadeIn(400,function(){
                        $('div.litebox')
                        .html($(options.toDisplay).show())
                        .fadeIn(400);
                    });
                } else {
                    $('<div class="litebox-bg"></div>')
                    .prependTo('body')
                    .fadeIn(400,function(){
                        $('<div class="litebox"></div>')
                        .prependTo('body')
                        .html($(options.toDisplay).show())
                        .fadeIn(400);
                    })
                    .click(function() {
                        clone_parent.append(clone.hide());
                        $('div.litebox').empty().fadeOut(400);
                        $(this).fadeOut(400);
                    });
                }
            });
        });
    }
})(jQuery);