-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 02, 2012 at 02:12 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `real_state`
--

-- --------------------------------------------------------

--
-- Table structure for table `acad_change_requests`
--

CREATE TABLE IF NOT EXISTS `acad_change_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(70) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `instructional_hours` enum('0','1') DEFAULT '0',
  `classroom` enum('0','1') DEFAULT '0',
  `additional_class` enum('0','1') DEFAULT '0',
  `class_swapping` enum('0','1') DEFAULT '0',
  `course_extension` enum('0','1') DEFAULT '0',
  `others` enum('0','1') DEFAULT '0',
  `others_specify` varchar(70) DEFAULT NULL,
  `request_detail` varchar(400) NOT NULL,
  `acad_approval` int(11) DEFAULT '0',
  `sales_approval` int(11) DEFAULT '0',
  `acad_date_rcv` int(11) DEFAULT '0',
  `sales_date_rcv` int(11) DEFAULT '0',
  `date_added` int(11) DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `acad_change_requests`
--

INSERT INTO `acad_change_requests` (`request_id`, `student_name`, `student_id`, `instructional_hours`, `classroom`, `additional_class`, `class_swapping`, `course_extension`, `others`, `others_specify`, `request_detail`, `acad_approval`, `sales_approval`, `acad_date_rcv`, `sales_date_rcv`, `date_added`) VALUES
(1, 'Amadeo Pelaez', '11-011102', '1', '1', '1', '0', '0', '0', NULL, 'my reason here.', 1342403456, 1327902306, 1327917056, 1327902306, 1327569501),
(2, 'asd', '1', '1', '1', '1', '0', '0', '0', NULL, '', 1338448754, 0, 55154, 0, 1334892817);

-- --------------------------------------------------------

--
-- Table structure for table `acad_classes`
--

CREATE TABLE IF NOT EXISTS `acad_classes` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL COMMENT 'from books table',
  `course_id` int(11) NOT NULL COMMENT 'from course table',
  `level_id` int(11) DEFAULT NULL COMMENT 'from course_level table',
  `classroom_id` int(4) NOT NULL COMMENT 'from classrooms table',
  `class_type` enum('1:1','1:4','1:8') DEFAULT '1:1',
  `teacher_id` int(11) NOT NULL COMMENT 'from emp_user table(emp_id)',
  `time` enum('08:00-09:50','10:00-11:50','12:40-02:30','02:40-04:30','04:40-06:30','06:40-08:30') DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`class_id`),
  KEY `book_id` (`book_id`),
  KEY `course_id` (`course_id`),
  KEY `level_id` (`level_id`),
  KEY `classroom_id` (`classroom_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `acad_classes`
--

INSERT INTO `acad_classes` (`class_id`, `book_id`, `course_id`, `level_id`, `classroom_id`, `class_type`, `teacher_id`, `time`, `date_added`, `status`) VALUES
(1, 2, 2, 1, 2, '1:1', 11, '08:00-09:50', 1323237576, 0),
(2, 12, 15, 3, 2, '1:1', 11, '10:00-11:50', 1323312741, 1),
(5, 2, 8, 3, 11, '1:1', 12, '10:00-11:50', 1323396830, 1),
(6, 18, 15, 1, 2, '1:1', 12, '02:40-04:30', 1323410885, 1),
(7, 4, 8, 6, 15, '1:4', 13, '08:00-09:50', 1323411109, 1),
(8, 2, 8, 3, 15, '1:1', 13, '10:00-11:50', 1323411279, 1),
(10, 2, 8, 3, 21, '1:8', 12, '08:00-09:50', 1323764089, 1),
(11, 12, 15, 1, 1, '1:1', 11, '06:40-08:30', 1325053574, 1),
(12, 2, 8, 1, 5, '1:1', 14, '08:00-09:50', 1332985362, 1),
(13, 3, 8, 1, 6, '1:1', 12, '12:40-02:30', 1333012424, 1),
(14, 5, 8, 1, 2, '1:4', 12, '04:40-06:30', 1333075199, 0),
(15, 5, 8, 8, 6, '1:4', 11, '04:40-06:30', 1338458331, 1);

-- --------------------------------------------------------

--
-- Table structure for table `acad_lesson_plans`
--

CREATE TABLE IF NOT EXISTS `acad_lesson_plans` (
  `lesson_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `inclusive_date_from` int(11) DEFAULT NULL,
  `inclusive_date_to` int(11) DEFAULT NULL,
  `date_added` int(11) NOT NULL,
  `date_modified` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `date_approved` int(11) NOT NULL,
  PRIMARY KEY (`lesson_plan_id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `acad_lesson_plans`
--

INSERT INTO `acad_lesson_plans` (`lesson_plan_id`, `class_id`, `inclusive_date_from`, `inclusive_date_to`, `date_added`, `date_modified`, `teacher_id`, `date_approved`) VALUES
(5, 1, 1325503040, 1325848640, 1326694631, 1334805440, 11, 0),
(6, 2, 1326728806, 1327074406, 1326703606, 0, 11, 1339645479),
(7, 11, 1326729620, 1327075220, 1326704420, 0, 11, 0),
(8, 12, 1338890962, 1339409362, 1339643362, 0, 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acad_lesson_plan_details`
--

CREATE TABLE IF NOT EXISTS `acad_lesson_plan_details` (
  `lesson_plan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_plan_id` int(11) NOT NULL,
  `weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  `objectives` varchar(300) DEFAULT NULL,
  `topic` varchar(200) DEFAULT NULL,
  `class_starter` varchar(300) DEFAULT NULL,
  `introduction` varchar(300) DEFAULT NULL,
  `lesson_proper` varchar(300) DEFAULT NULL,
  `guided_practice` varchar(300) DEFAULT NULL,
  `independent_practice` varchar(300) DEFAULT NULL,
  `homework` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`lesson_plan_detail_id`),
  KEY `lesson_plan_id` (`lesson_plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `acad_lesson_plan_details`
--

INSERT INTO `acad_lesson_plan_details` (`lesson_plan_detail_id`, `lesson_plan_id`, `weekday`, `objectives`, `topic`, `class_starter`, `introduction`, `lesson_proper`, `guided_practice`, `independent_practice`, `homework`) VALUES
(5, 5, 'Monday', '', '', '', '', '', '', '', ''),
(6, 6, 'Monday', 'second', 'second', 'second', 'second', 'second', 'second', 'second', 'second'),
(7, 5, 'Tuesday', 'here\nhere\nhere\nhere\nhere\nhere\nhere\nhere', 'here', 'here\nhere\nhere\nhere\nhere', 'here\nhere\nhere\nhere\nhere\nhere\nhere\nhere\nhere\n', 'here\nhere\nhere\nhere\nhere\nhere\nhere\n', 'here\nhere\nhere\nhere\nhere\nhere\nhere', 'here\nhere\nhere\nhere\nhere\nhere\nhere', 'here\nhere\nhere\nhere\nhere\nhere\nhere'),
(8, 5, 'Wednesday', 'To enhance listening skill.\nTo build words with noun suffixes.\nTo learn world building.', 'Word Building (noun suffixes)', 'Greetings. What is a suffix?. Explain what a suffix is & give examples.', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `accommodations`
--

CREATE TABLE IF NOT EXISTS `accommodations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'number of persons',
  `type` enum('student','guest') NOT NULL DEFAULT 'student',
  `room_number` int(11) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `booking_type` varchar(35) NOT NULL,
  `check_in` int(11) NOT NULL DEFAULT '0',
  `check_out` int(11) NOT NULL DEFAULT '0',
  `pick_up_time` time NOT NULL DEFAULT '00:00:00',
  `send_off_time` time NOT NULL DEFAULT '00:00:00',
  `comment` text NOT NULL,
  `date_added` int(11) NOT NULL,
  `status` enum('reserved','checkin','checkout') DEFAULT 'checkin',
  PRIMARY KEY (`id`),
  KEY `room_number` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admissions`
--

CREATE TABLE IF NOT EXISTS `admissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` int(11) NOT NULL COMMENT 'student id',
  `status` enum('stand by','cancelled','enrolled','graduated') DEFAULT NULL,
  `invoice_id` int(11) NOT NULL COMMENT 'link to the invoices table',
  `course_id` int(11) NOT NULL COMMENT 'link to the courses table',
  `course_level` int(11) NOT NULL COMMENT 'link to the course_level table',
  `class_type` enum('1:1','1:4','1:8') NOT NULL,
  `type` enum('individual','group','family') NOT NULL,
  `duration` tinyint(2) NOT NULL COMMENT 'number of weeks',
  `adm_date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stud_id` (`stud_id`),
  KEY `course_id` (`course_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `course_level` (`course_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `units` int(11) NOT NULL DEFAULT '9' COMMENT 'units of measure',
  `tag` varchar(50) NOT NULL COMMENT 'asset''s tag',
  `serial_num` varchar(120) NOT NULL,
  `item_code` varchar(120) NOT NULL COMMENT 'like (processor, memory, etc)',
  `brand` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(25) NOT NULL,
  `po_number` varchar(35) DEFAULT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`asset_id`),
  KEY `units` (`units`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`asset_id`, `units`, `tag`, `serial_num`, `item_code`, `brand`, `description`, `location`, `po_number`, `date_added`) VALUES
(38, 2, '2', '2', '22', 'jkljkl', 'iuiu', 'DAVAO', '', 0),
(44, 3, '3', '7', '7', 'asdasd', 'asd', 'DAVAOa', '', 0),
(49, 3, '1', '345', '432', 'HARDISK', 'dasd', 'CEBU CITYl', '', 0),
(50, 1, '1', '1234', '1232', 'lkmlkm', 'k', 'lkm', '', 0),
(51, 1, '32', '2', '21', 'ioj', 'oi', 'oij', '', 0),
(52, 1, '3', '2', '123', 'asdas', 'asdasd', 'asdasd', '', 0),
(53, 2, '3', '234', '345', 'sdfsdf', 'sfsf', 'DAVAO', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `asset_stats`
--

CREATE TABLE IF NOT EXISTS `asset_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `asset_stats`
--

INSERT INTO `asset_stats` (`id`, `status_name`) VALUES
(1, 'vacant'),
(2, 'assigned'),
(3, 'returned'),
(4, 'transferred');

-- --------------------------------------------------------

--
-- Table structure for table `asset_tracker`
--

CREATE TABLE IF NOT EXISTS `asset_tracker` (
  `tracker_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_tracker` int(11) NOT NULL DEFAULT '0',
  `asset_status` enum('vacant','assigned','returned','transfered') DEFAULT NULL,
  `from_emp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Default to zero if its the first time to be assigned',
  `to_emp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'employee id to be assigned',
  `asset_id` int(11) NOT NULL COMMENT 'the id of the asset in the assets table',
  `date` int(11) NOT NULL DEFAULT '0',
  `received` int(11) NOT NULL DEFAULT '0' COMMENT '0 if not yet received, the date value if received',
  `fin_appr` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  `for_log` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 or 1 if its one then this will be used for logs/history',
  PRIMARY KEY (`tracker_id`),
  KEY `asset_id` (`asset_id`),
  KEY `from_emp_id` (`from_emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `asset_tracker`
--

INSERT INTO `asset_tracker` (`tracker_id`, `parent_tracker`, `asset_status`, `from_emp_id`, `to_emp_id`, `asset_id`, `date`, `received`, `fin_appr`, `for_log`) VALUES
(1, 0, 'assigned', 5, 11, 38, 1324438918, 1324438918, 'approved', '1'),
(2, 0, 'assigned', 5, 11, 44, 1324438918, 1324438918, 'approved', '0'),
(3, 0, 'assigned', 5, 11, 49, 1324438918, 1324438918, 'approved', '0'),
(4, 0, 'assigned', 5, 11, 50, 1324438918, 1324438918, 'approved', '0'),
(5, 0, 'assigned', 5, 13, 51, 1324438918, 1324438918, 'approved', '1'),
(6, 0, 'assigned', 5, 13, 52, 1324438918, 1324438918, 'approved', '1'),
(7, 0, 'assigned', 5, 13, 53, 1324438918, 1324438918, 'approved', '0'),
(24, 5, 'returned', 13, 0, 51, 1324606765, 0, 'cancelled', '1'),
(26, 6, 'transfered', 13, 12, 52, 1324612372, 0, 'cancelled', '1'),
(27, 7, 'transfered', 13, 12, 53, 1324617933, 0, 'cancelled', '1'),
(28, 6, 'returned', 13, 0, 52, 1324628835, 0, 'cancelled', '1'),
(29, 6, 'returned', 13, 0, 52, 1324629348, 0, 'cancelled', '1'),
(30, 5, 'transfered', 13, 12, 51, 1324630881, 1324869187, 'approved', '1'),
(31, 30, 'transfered', 12, 13, 51, 1324870062, 0, 'cancelled', '1'),
(32, 30, 'returned', 12, 0, 51, 1324870080, 0, 'cancelled', '1'),
(33, 6, 'transfered', 13, 12, 52, 1324870195, 1324870429, 'approved', '0'),
(34, 33, 'transfered', 12, 13, 52, 1324889140, 0, 'cancelled', '1'),
(35, 33, 'returned', 12, 0, 52, 1324889153, 0, 'cancelled', '1'),
(36, 33, 'returned', 12, 0, 52, 1330305856, 0, 'cancelled', '1'),
(37, 30, 'returned', 12, 0, 51, 1330305856, 0, 'cancelled', '1'),
(38, 7, 'transfered', 13, 12, 53, 1330399350, 0, 'cancelled', '1'),
(39, 30, 'returned', 12, 0, 51, 1330405339, 0, 'cancelled', '1'),
(40, 33, 'returned', 12, 0, 52, 1330405340, 0, 'cancelled', '1'),
(41, 30, 'transfered', 12, 11, 51, 1330418070, 0, 'pending', '0'),
(42, 1, 'transfered', 11, 10, 38, 1341798374, 0, 'pending', '0');

-- --------------------------------------------------------

--
-- Table structure for table `as_account_categories`
--

CREATE TABLE IF NOT EXISTS `as_account_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(25) NOT NULL COMMENT 'ex. assets, liabilities, equity/capital revenue/income, expenses',
  `is_pl` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `as_account_categories`
--

INSERT INTO `as_account_categories` (`id`, `category_name`, `is_pl`) VALUES
(1, 'asset', '0'),
(2, 'liability', '0'),
(3, 'equity', '0'),
(4, 'income', '1'),
(5, 'expense', '1');

-- --------------------------------------------------------

--
-- Table structure for table `as_account_type`
--

CREATE TABLE IF NOT EXISTS `as_account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(35) NOT NULL COMMENT 'ex. bank, accounts, receivable, advances to suppliers, accounts payable, fixed asset, other current asset... etc. (note: this is based in quickbooks)',
  `category_id` int(11) NOT NULL COMMENT 'linked to the as_acctcategories table',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `as_account_type`
--

INSERT INTO `as_account_type` (`id`, `type_name`, `category_id`) VALUES
(1, 'bank', 1),
(2, 'ar', 1),
(3, 'ocasset', 1),
(4, 'fixasset', 1),
(5, 'oasset', 1),
(6, 'ap', 2),
(7, 'ocliab', 2),
(8, 'ltliab', 2),
(9, 'equity', 3),
(10, 'inc', 4),
(11, 'cogs', 4),
(12, 'exp', 5),
(13, 'exinc', 4),
(14, 'exexp', 5),
(15, 'nonposting', 5);

-- --------------------------------------------------------

--
-- Table structure for table `as_assets`
--

CREATE TABLE IF NOT EXISTS `as_assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `units` int(11) NOT NULL COMMENT 'units of measure | disregard this column since assets don''t have units of measure',
  `tag` varchar(50) NOT NULL COMMENT 'asset''s tag',
  `serial_num` varchar(120) NOT NULL,
  `item_code` varchar(120) NOT NULL COMMENT 'like (processor, memory, etc)',
  `brand` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(25) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'link to the asset_stats table',
  `owner` int(11) NOT NULL DEFAULT '0' COMMENT 'employee id',
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `as_chartdetails`
--

CREATE TABLE IF NOT EXISTS `as_chartdetails` (
  `account_code` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `banknum` varchar(25) NOT NULL COMMENT 'the bank number, if there is any',
  PRIMARY KEY (`account_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `as_chartofaccounts`
--

CREATE TABLE IF NOT EXISTS `as_chartofaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_code` int(11) NOT NULL COMMENT 'the account code',
  `parent_code` int(11) NOT NULL DEFAULT '0' COMMENT 'the parent code and is refering to the account_code in this table',
  `account_name` varchar(65) NOT NULL,
  `account_type_id` int(11) NOT NULL COMMENT 'refering to the as_account_type table',
  PRIMARY KEY (`id`),
  KEY `account_type_id` (`account_type_id`),
  KEY `account_code` (`account_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=375 ;

--
-- Dumping data for table `as_chartofaccounts`
--

INSERT INTO `as_chartofaccounts` (`id`, `account_code`, `parent_code`, `account_name`, `account_type_id`) VALUES
(1, 1010100, 1010101, 'Cash on Hand', 1),
(2, 1010101, 1010100, 'Petty Cash Fund', 1),
(3, 1010102, 1010100, 'Revolving Fund', 1),
(4, 1010103, 1010100, 'Key Money Fund', 1),
(5, 1010104, 1010100, 'Marketing Fund', 1),
(6, 1010300, 0, 'Cash In Bank', 1),
(7, 1010301, 1010300, 'SCB Peso Checking - 688', 1),
(8, 1010302, 1010300, 'SCB Dollar Savings - 722', 1),
(9, 1010303, 1010300, 'RCBC Peso Checking - 495', 1),
(10, 1010304, 1010300, 'RCBC Dollar Savings - 922', 1),
(11, 1010305, 1010300, 'BDO Peso Checking -277', 1),
(12, 1010306, 1010300, 'BDO Dollar Account - 188', 1),
(13, 1010307, 1010300, 'Union Bank Peso Savings - 404', 1),
(14, 1010308, 1010300, 'Union Bank Dollar Savings - 835', 1),
(15, 1010309, 1010300, 'Union Bank Peso Savings - 118', 1),
(16, 1010310, 1010300, 'SCB Hnkg USD acct/CELI Ltd- 037', 1),
(17, 6014903, 0, 'Salary Adjustments', 1),
(18, 1020100, 0, 'Account Receivable', 2),
(19, 1020101, 1020100, 'Account Receivable-Trade', 2),
(20, 1020102, 1020100, 'Account Receivable-Others', 2),
(21, 1020103, 1020100, 'Accounts Receivable-NCLEX', 2),
(22, 1020104, 1020100, 'Accounts Receivable - Caregiver', 2),
(23, 1020108, 1020104, 'Acc. Receivable - Non Schollars', 2),
(24, 1020109, 1020104, 'Acc. Receivable - TESDA scholar', 2),
(25, 1020105, 1020100, 'Accounts Receivable-Call Center', 2),
(26, 1020106, 1020100, 'Accounts Receivable-Credit Card', 2),
(27, 1020107, 1020100, 'Accounts Receivable - Dormitory', 2),
(29, 1020300, 0, 'Advances to Officers & Employee', 3),
(30, 1020301, 1020300, 'Adv to Off/Employee-Kainos', 3),
(31, 1020302, 1020300, 'Adv to Off/Employee-SSS', 3),
(32, 1020303, 1020300, 'Adv to Off/Employees-Canteen', 3),
(33, 1020304, 1020300, 'Adv To Off/Employees-Telephone', 3),
(34, 1020305, 1020300, 'Adv To Off/Employees-Others', 3),
(35, 1020400, 0, 'Advances to/from Affiliates', 3),
(36, 1, 1020400, 'Adv. to/from CELI-Vietnam', 3),
(37, 1020401, 1020400, 'Adv to/frm CELI Ltd', 3),
(38, 1020402, 1020400, 'Adv to/frm IAFT-NCM', 3),
(39, 1020403, 1020400, 'Adv to/frm IAFT-CELI', 3),
(40, 1020404, 1020400, 'Adv to/from CELI-Vietnam', 3),
(41, 1020405, 1020400, 'Adv to/from Bigfoot Venture Ltd', 3),
(42, 1020406, 1020400, 'Adv to/from BF Capital Ventures', 3),
(43, 1020407, 1020400, 'Adv to/from Bigfoot Entertainme', 3),
(44, 1020408, 1020400, 'Adv to/from Bigfoot Properties', 3),
(45, 1020409, 1020400, 'Adv to/from Bigfoot Communicati', 3),
(46, 1020410, 1020400, 'Adv to/from CL BVI', 3),
(47, 1020411, 1020400, 'Adv to/from IAFT', 3),
(48, 1020412, 1020400, 'Adv to/from JL Mercado', 3),
(49, 1020413, 1020400, 'Adv to/from John Lawrence Merca', 3),
(50, 1020414, 1020400, 'Advances to/from Poddy O''Bryan', 3),
(51, 1020500, 0, 'Advances to Suppliers', 3),
(52, 1020600, 0, 'Other Receivables', 3),
(53, 1020900, 0, 'Undeposited Funds', 3),
(54, 1030100, 0, 'Prepaid Expenses', 3),
(55, 1030101, 1030100, 'Prepaid Rental', 3),
(56, 1030102, 1030100, 'Prepaid Supplies', 3),
(57, 1030103, 1030100, 'Prepaid Cards', 3),
(58, 1030200, 0, 'Inventory', 3),
(59, 1030201, 1030200, 'Office Supplies on Hand', 3),
(60, 1030202, 1030200, 'Mini Bar Supplies on Hand', 3),
(61, 1030203, 1030200, 'C-Spot Supplies on Hand', 3),
(62, 1030204, 1030200, 'Housekeeping Supplies on Hand', 3),
(63, 1030205, 1030200, 'Kitchen Supplies on Hand', 3),
(64, 1030206, 1030200, 'Amenities supplies on Hand', 3),
(65, 1030207, 1030200, 'ACAD Supplies on Hand', 3),
(66, 1030208, 0, 'Larry''s Place', 3),
(67, 1120, 0, 'Inventory Asset', 3),
(68, 1040000, 0, 'Fixed Assets', 4),
(69, 1, 1040000, 'Accumulated Depreciation', 4),
(70, 1040100, 1040000, 'Equipment', 4),
(71, 1040101, 1040100, 'Audio Equipment', 4),
(72, 1040102, 1040100, 'Camera Equipment', 4),
(73, 1040103, 1040100, 'Computer Hardware & Peripherals', 4),
(74, 1040104, 1040100, 'Computer Softwares', 4),
(75, 1040105, 1040100, 'Kitchen Eqt & Appliances', 4),
(76, 1040106, 1040100, 'Lighting Equipment', 4),
(77, 1040107, 1040100, 'Maintenance & Other Safety Eqpt', 4),
(78, 1040108, 1040100, 'Network Component & Peripherals', 4),
(79, 1040109, 1040100, 'Office Equipment', 4),
(80, 1040110, 1040100, 'Transpo & Other Service Vehicle', 4),
(81, 1040111, 1040100, 'Video Equipment', 4),
(82, 1040112, 1040100, 'Medical Equipment', 4),
(83, 1040113, 1040100, 'Laundry Equipment', 4),
(84, 1040200, 1040000, 'Dormitory Assets', 4),
(85, 1040201, 1040200, 'Dormitory Equipment', 4),
(86, 1040202, 1040200, 'Dormitory Kitchen Utensils', 4),
(87, 1040250, 1040000, 'Furniture and Fixtures', 4),
(88, 1040300, 1040000, 'Leasehold Improvements', 4),
(89, 1040400, 1040000, 'Library Asset', 4),
(90, 1040600, 1040000, 'Land', 4),
(91, 1040700, 1040000, 'Building', 4),
(92, 1040800, 1040000, 'Service Vehicle', 4),
(93, 1040899, 1040000, 'Fixed Assets-Others', 4),
(94, 1040500, 0, 'Construction In Progress', 4),
(95, 1040501, 1040500, 'CIP-Leasehold Improvements', 4),
(96, 1040502, 1040500, 'CIP-Land', 4),
(97, 1040503, 1040500, 'CIP-Building', 4),
(98, 1040504, 1040500, 'CIP-Safety & Maintenance Equipm', 4),
(99, 1040505, 1040500, 'CIP-Kitchen Equipment and App.', 4),
(100, 1040506, 1040500, 'CIP-Others', 4),
(101, 1040507, 1040500, 'CIP-Coffee Shop', 4),
(102, 1040900, 0, 'Accumulated Depreciation', 4),
(103, 1040901, 1040900, 'Acc.Dep.-Equipment', 4),
(104, 1040902, 1040901, 'Acc. Dep.-Network Components &', 4),
(105, 1040903, 1040901, 'Acc.Dep.-Audio Equipment', 4),
(106, 1040904, 1040901, 'Acc.Dep.-Camera Equipment', 4),
(107, 1040905, 1040901, 'Acc.Dep.-Computer Hardwares & P', 4),
(108, 1040906, 1040901, 'Acc.Dep.-Computer Softwares', 4),
(109, 1040907, 1040901, 'Acc.Dep.-Kitchen Eqt & Applianc', 4),
(110, 1040908, 1040901, 'Acc.Dep.-Lighting Equipment', 4),
(111, 1040909, 1040901, 'Acc.Dep.-Maintenance&OtherSafet', 4),
(112, 1040910, 1040901, 'Acc.Dep.-Office Equipment', 4),
(113, 1040911, 1040901, 'Acc.Dep.-Transpo&OtherVehicles', 4),
(114, 1040912, 1040901, 'Acc.Dep.-Video Equipment', 4),
(115, 1040918, 1040901, 'Acc.Dep.-Medical Equipment', 4),
(116, 1040919, 1040901, 'Acc.Dep.-Laundry Equipment', 4),
(117, 1040922, 1040901, 'Acc.Dep.-Hotel Equipment', 4),
(118, 1040923, 1040901, 'Acc.Dep.-Dormitory Equipment', 4),
(119, 1040913, 1040900, 'Acc.Dep.-Furniture and Fixtures', 4),
(120, 1040914, 1040900, 'Acc.Dep.-Leasehold Improvements', 4),
(121, 1040915, 1040900, 'Acc.Dep.-Library Asset', 4),
(122, 1040917, 1040900, 'Acc.Dep.-Building', 4),
(123, 1040920, 1040900, 'Acc.Dep.-Fixed Assets-Others', 4),
(124, 1040921, 1040900, 'Acc.Dep.-Service Vehicle', 4),
(125, 1050100, 0, 'Refundable Deposit', 5),
(126, 1050200, 0, 'Deferred Charges', 5),
(127, 1050201, 1050200, 'Deferred-CELI Ltd', 5),
(128, 1050202, 1050200, 'Deferred-CL BVI', 5),
(129, 1050203, 1050200, 'Deferred-BVI Ltd', 5),
(130, 1050204, 1050200, 'Deferred-BF Entertainment', 5),
(131, 1050205, 1050200, 'Deferred-NOLCO', 5),
(132, 1050300, 0, 'Creditable Withholding Tax', 5),
(133, 1050400, 0, 'Input VAT', 5),
(134, 1050500, 0, 'Other Assets', 5),
(135, 1050600, 0, 'Security Deposit-MECO', 5),
(136, 1050700, 0, 'Advance Rental', 5),
(137, 1050800, 0, 'Start Up Costs - BPO Fin&Acctg', 5),
(138, 101, 0, '+', 6),
(139, 2010100, 0, 'Accounts Payable', 6),
(140, 2010101, 2010100, 'Accounts Payable-Trade', 6),
(141, 2010102, 2010100, 'Accounts Payable-Others', 6),
(142, 2010103, 2010100, 'Accounts payable-L.M. Cred.Card', 6),
(143, 101, 0, 'Payroll Liabilities', 7),
(144, 2010200, 0, 'Advances from Affiliates', 7),
(145, 2010300, 0, 'Security Deposit on Rental', 7),
(146, 2010400, 0, 'Output VAT', 7),
(147, 2020100, 0, 'SSS Payable', 7),
(148, 2020101, 2020100, 'SSS Premium Payable', 7),
(149, 2020102, 2020100, 'SSS Salary Loans Payable', 7),
(150, 2020103, 2020100, 'SSS Payable - Others', 7),
(151, 2020200, 0, 'HDMF Payable', 7),
(152, 2020201, 2020200, 'HDMF Premium Payable', 7),
(153, 2020202, 2020200, 'HDMF Multi-Purpose Loans', 7),
(154, 2020203, 2020200, 'HDMF Housing Loans Payable', 7),
(155, 2020300, 0, 'PHIC Payable', 7),
(156, 2020301, 2020300, 'PHIC Premium Payable', 7),
(157, 2020400, 0, 'Withholding Taxes Payable', 7),
(158, 2020401, 2020400, 'WTP-Compensation', 7),
(159, 2020402, 2020400, 'WTP-Expanded', 7),
(160, 2020403, 2020400, 'WTP-Final', 7),
(161, 2020404, 2020400, 'Tax Refund', 7),
(162, 2020500, 0, 'Retention Payable', 7),
(163, 2020600, 0, 'Advances from Stockholders', 7),
(164, 2030100, 0, 'Accrued Expense Payable', 7),
(165, 2030101, 2030100, 'Accrued Rental', 7),
(166, 2030102, 2030100, 'Accrued Expense Payable-Others', 7),
(167, 2030103, 2030100, 'Accrued Salaries Payable', 7),
(168, 2030200, 0, 'Unearned Revenue', 7),
(169, 2030900, 0, 'Other Payable', 7),
(170, 2030901, 2030900, 'Other Payable-TOEIC', 7),
(171, 2030902, 2030900, 'Other Payable-Key Money', 7),
(172, 2030903, 2030900, 'Other Payable-Gym Usage', 7),
(173, 2030909, 2030900, 'Other Payable-Others', 7),
(174, 2030910, 2030900, 'Other Payable-Damages', 7),
(175, 2030911, 2030900, 'Other Payble-Temp Borwng-officr', 7),
(176, 2040100, 0, 'Deposit for Future Subscription', 8),
(177, 2050100, 0, 'Notes Payable', 8),
(178, 101, 0, 'Opening Bal Equity', 9),
(179, 3010100, 0, 'Capital Stock Subscribed', 9),
(180, 3010200, 0, 'CS Subscription Receivable', 9),
(181, 3020100, 0, 'Retained Earnings', 9),
(182, 4010000, 0, 'Gross Sales', 10),
(183, 4010100, 4010000, 'Sales', 10),
(184, 4010101, 4010100, 'General English', 10),
(185, 4010102, 4010100, 'Immersion Courses', 10),
(186, 4010103, 4010100, 'Sales-Others', 10),
(187, 4010104, 4010100, 'NCLEX Processing', 10),
(188, 4010105, 4010100, 'Caregiver Courses', 10),
(189, 4010106, 4010105, 'Caregiver - Scholars', 10),
(190, 4010107, 4010105, 'Caregiver - Non Scholars Tuitio', 10),
(191, 4010108, 4010100, 'Rental Income', 10),
(192, 4010109, 4010100, 'Call Center Sales', 10),
(193, 4010118, 4010100, 'Application Fees Income', 10),
(194, 4010119, 4010100, 'SSP Processing Income', 10),
(195, 4010110, 4010000, 'NCLEX Review Courses', 10),
(196, 4010111, 4010000, 'NCLEX Training Courses', 10),
(197, 4010113, 4010000, 'Call Center Tuition Fee', 10),
(198, 4010114, 4010000, 'Call Center - Scholarship', 10),
(199, 4010115, 4010000, 'English for Nurses Program(ENP)', 10),
(200, 4010116, 4010000, 'Int''l. Nurses Training Program', 10),
(201, 4010200, 4010000, 'Export of Services', 10),
(202, 4010300, 4010000, 'Sales Discounts', 10),
(203, 4010400, 4010000, 'Sales Returns', 10),
(204, 4010112, 0, 'Caregiving Review Courses', 10),
(205, 4010117, 0, 'Hotel Sales-Students Accomodati', 10),
(206, 5000, 0, 'Cost of Goods Sold', 11),
(207, 5010000, 0, 'Cost of Sales', 11),
(208, 5010101, 5010000, 'Student Activity Cost', 11),
(209, 5010102, 5010000, 'SSP Processing Expense', 11),
(210, 5010103, 5010000, 'Teaching Materials', 11),
(211, 5010104, 5010000, 'SAC-Immersion Courses', 11),
(212, 5010105, 5010000, 'Students'' Accommodation', 11),
(213, 5010106, 5010000, 'Rentals', 11),
(214, 5010107, 5010000, 'Food Accommodation', 11),
(215, 5010108, 5010000, 'Camps/Family Program Cost', 11),
(216, 5010109, 5010000, 'COS-Others', 11),
(217, 5010110, 5010000, 'Cspot & Waynes Place', 11),
(218, 5010111, 5010000, 'Hotel Amenities', 11),
(219, 5010112, 5010000, 'Annual Fee', 11),
(220, 501112, 0, 'JY Square Over-all Dept', 11),
(221, 5020000, 0, 'Cost of Sales-Immersion Courses', 11),
(222, 5020001, 0, 'Cost of Sales- NCLEX', 11),
(223, 5020002, 0, 'Cost of Sales-Caregiver', 11),
(224, 5020003, 0, 'Cost of Sales - Rental', 11),
(225, 5020004, 0, 'Cost of Sales- BPO Fin & Acctg.', 11),
(226, 5020005, 0, 'Marketing Dept. -JY', 11),
(227, 101, 0, 'BDO Peso Checking -277', 12),
(228, 101, 0, 'Makro Pilipinas', 12),
(229, 101, 0, 'Payroll Expenses', 12),
(230, 6010000, 0, 'Salaries and Wages Expense', 12),
(231, 6011000, 0, 'Overtime Pay', 12),
(232, 6012000, 0, 'Night Differential Pay', 12),
(233, 6013000, 0, '13th Month Pay Exp', 12),
(234, 6014000, 0, 'Employee Benefits', 12),
(235, 6014100, 6014000, 'Vacation Leave', 12),
(236, 6014200, 6014000, 'Sick Leave', 12),
(237, 6014300, 6014000, 'Bonus', 12),
(238, 6014400, 6014000, 'Rice Subsidy', 12),
(239, 6014500, 6014000, 'Separation Pay', 12),
(240, 6014600, 6014000, 'Other Incentives', 12),
(241, 6014700, 6014000, 'Transportation Allowance', 12),
(242, 6014800, 6014000, 'Food Allowance', 12),
(243, 6014900, 6014000, 'Performance Incentive', 12),
(244, 6014901, 6014000, 'Task Allowance', 12),
(245, 6014902, 6014000, 'Personal Allowance', 12),
(246, 6014001, 0, 'Other Employee Benefits', 12),
(247, 6014010, 6014001, 'Christmas Give aways', 12),
(248, 6014020, 6014001, 'Maternity Expenses - SSS', 12),
(249, 6014002, 0, 'Food & Subsistence - Admin', 12),
(250, 6014030, 0, 'Holiday Pay', 12),
(251, 6015000, 0, 'Allowances Expense', 12),
(252, 6015100, 6015000, 'Transportation Allowance', 12),
(253, 6015200, 6015000, 'Bereavement Assitance', 12),
(254, 6015300, 6015000, 'OJT/Intern', 12),
(255, 6015400, 6015000, 'Clothing Allowance', 12),
(256, 6015500, 6015000, 'Company Parties/Outings', 12),
(257, 6015600, 6015000, 'Meals', 12),
(258, 6015700, 6015000, 'Other Allowance Expense', 12),
(259, 6016000, 0, 'SSS Premium Expense', 12),
(260, 6016100, 6016000, 'SSS Employee Premium Expense', 12),
(261, 6016200, 6016000, 'SSS Employer Premium Expense', 12),
(262, 6017000, 0, 'HDMF Premium Expense', 12),
(263, 6017100, 6017000, 'HDMF Employee Premium Expense', 12),
(264, 6017200, 6017000, 'HDMF Employer Premium Expense', 12),
(265, 6018000, 0, 'PHIC Premium Expense', 12),
(266, 6018100, 6018000, 'PHIC Employee Premium Expense', 12),
(267, 6018200, 6018000, 'PHIC Employer Premium Expense', 12),
(268, 6019000, 0, 'Professional Fees', 12),
(269, 6019100, 6019000, 'Legal Fees', 12),
(270, 6019200, 6019000, 'Audit Fees', 12),
(271, 6019300, 6019000, 'Teaching/Mentoring Fees', 12),
(272, 6019900, 6019000, 'Other Professional Fees', 12),
(273, 6020000, 0, 'Services Fees', 12),
(274, 6020100, 6020000, 'Janitorial Services', 12),
(275, 6020200, 6020000, 'Security Services', 12),
(276, 6020300, 6020000, 'Pest Control Services', 12),
(277, 6020900, 6020000, 'Other Services Fees', 12),
(278, 6021000, 0, 'Rental Expense', 12),
(279, 6021100, 6021000, 'Equipment Rental', 12),
(280, 6021200, 6021000, 'Transpo/Vehicle Rental', 12),
(281, 6021300, 6021000, 'Office Rental', 12),
(282, 6021400, 6021000, 'Houses/Condo Rental', 12),
(283, 6021900, 6021000, 'Other Rental Expense', 12),
(284, 6022000, 0, 'Advertising Expense', 12),
(285, 6022100, 6022000, 'Newspaper/Magazine Ads', 12),
(286, 6022200, 6022000, 'Posters and Banners', 12),
(287, 6022300, 6022000, 'Manpower Recruitment', 12),
(288, 6022400, 6022000, 'Souvenirs (t-shirts,caps,etc.)', 12),
(289, 6022500, 6022000, 'TV/Radio Ads', 12),
(290, 6022600, 6022000, 'Website', 12),
(291, 6022900, 6022000, 'Other Advertising Expense', 12),
(292, 6023000, 0, 'Supplies Expense', 12),
(293, 6023100, 6023000, 'Office Supplies', 12),
(294, 6023200, 6023000, 'Non-consummable Supplies', 12),
(295, 6023300, 6023000, 'Cleaning Supplies and Materials', 12),
(296, 6023400, 6023000, 'Dues and Subscriptions', 12),
(297, 6023900, 6023000, 'Supplies Expense-Others', 12),
(298, 6023901, 6023000, 'Supplies Expense - Medicines', 12),
(299, 6024000, 0, 'Travel Expense', 12),
(300, 6024100, 6024000, 'Travel Expense-Local', 12),
(301, 6024101, 6024100, 'Airfare-Local', 12),
(302, 6024102, 6024100, 'Hotel Accommodation-Local', 12),
(303, 6024109, 6024100, 'Other Travel Expense-Local', 12),
(304, 6024200, 6024000, 'Travel Expense-Foreign', 12),
(305, 6024201, 6024200, 'Airfare-Foreign', 12),
(306, 6024202, 6024200, 'Hotel Accommodation-Foreign', 12),
(307, 6024203, 6024200, 'Visa Extension', 12),
(308, 6024209, 6024200, 'Other Travel Expense-Foreign', 12),
(309, 6025000, 0, 'Utilities Expense', 12),
(310, 6025100, 6025000, 'Electricity', 12),
(311, 6025200, 6025000, 'Water', 12),
(312, 6026000, 0, 'Communication Expense', 12),
(313, 6026100, 6026000, 'Mailing, Postage and Delivery', 12),
(314, 6026200, 6026000, 'Mobile Phones', 12),
(315, 6026300, 6026000, 'Telephone Expense', 12),
(316, 6026301, 6026300, 'Fixed Line Rental', 12),
(317, 6026302, 6026300, 'Call Charges', 12),
(318, 6026303, 6026300, 'Internet/DSL Connections', 12),
(319, 6026304, 6026300, 'Telephone Expense-Others', 12),
(320, 6026900, 6026000, 'Communication Expense-Others', 12),
(321, 6027000, 0, 'Insurance Expense', 12),
(322, 6028000, 0, 'Transportation Expense', 12),
(323, 6028100, 6028000, 'Fare', 12),
(324, 6028200, 6028000, 'Fuel Expenses', 12),
(325, 6028300, 6028000, 'Parking Fee', 12),
(326, 6028400, 6028000, 'Service Vehicle', 12),
(327, 6028900, 6028000, 'Other Transportation Expense', 12),
(328, 6029000, 0, 'Repair & Maintenance', 12),
(329, 6029100, 6029000, 'Computer Repairs', 12),
(330, 6029200, 6029000, 'Equipment Repairs', 12),
(331, 6029300, 6029000, 'Building Repairs', 12),
(332, 6029400, 6029000, 'Kitchen Repairs', 12),
(333, 6029500, 6029000, 'Furniture and Fixtures Repair', 12),
(334, 6029600, 6029000, 'Vehicle Repairs', 12),
(335, 6029700, 6029000, 'Fuel-Generator', 12),
(336, 6029800, 6029000, 'Pool Maintenance', 12),
(337, 6030000, 0, 'Training and Seminar Fees', 12),
(338, 6030100, 0, 'Nursing Examination Expense', 12),
(339, 6030500, 0, 'Referral fees expense', 12),
(340, 6031000, 0, 'Commission Expense', 12),
(341, 6031001, 6031000, 'Commision Incentive Expense', 12),
(342, 6032000, 0, 'Administrative Expense', 12),
(343, 6033000, 0, 'Taxes & Licenses', 12),
(344, 6034000, 0, 'Depreciation Expense', 12),
(345, 6035000, 0, 'Credit Card Charges', 12),
(346, 6036000, 0, 'Other Taxes', 12),
(347, 6037000, 0, 'Interest Expense', 12),
(348, 6037500, 0, 'Subscription Expense', 12),
(349, 6037501, 6037500, 'online banking', 12),
(350, 6038000, 0, 'Bank Service Charges', 12),
(351, 6038001, 6038000, 'Bank Charge on Check Returned', 12),
(352, 6038002, 6038000, 'Bank Charge on SPO', 12),
(353, 6038500, 0, 'Facilitation Fees', 12),
(354, 6039000, 0, 'Miscellaneous Expenses', 12),
(355, 6039001, 6039000, 'Penalty on Rental', 12),
(356, 6039002, 6039000, 'Penalty/surcharge on Utilities', 12),
(357, 6039999, 0, 'Uncategorized Expenses', 12),
(358, 6040000, 0, 'Freight and Handling', 12),
(359, 6041000, 0, 'Donations & charitable cont.', 12),
(360, 6041001, 6041000, 'Community Outreach Program', 12),
(361, 6051000, 0, 'Representation', 12),
(362, 606000, 0, 'Student Refund', 12),
(363, 7010000, 0, 'Other Income', 13),
(364, 7010001, 7010000, 'Other Income Employees Meal', 13),
(365, 7010002, 7010000, 'Realized Gain on Forex Exchange', 13),
(366, 7020000, 0, 'Interest Income', 13),
(367, 8010000, 0, 'Other Expenses', 14),
(368, 8010100, 8010000, 'Gain (Loss) on Forex Exchange', 14),
(369, 8010200, 8010000, 'Provision for Income Tax', 14),
(370, 8010300, 8010000, 'Realized Gain(Loss) on Forex Ex', 14),
(371, 2, 0, 'Purchase Orders', 15),
(372, 4, 0, 'Estimates', 15),
(373, 5, 0, 'Sales Orders', 15),
(374, 12345, 1010300, 'This is a test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `as_class`
--

CREATE TABLE IF NOT EXISTS `as_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `as_class`
--

INSERT INTO `as_class` (`id`, `name`) VALUES
(1, 'Academic Department'),
(2, 'BPO Finance & Acctg'),
(3, 'Call Center'),
(4, 'Caregiver'),
(5, 'CELI Hotel Mactan'),
(6, 'CELI Restobar'),
(7, 'Cofee Shop'),
(8, 'CSpot'),
(9, 'Customer Service'),
(10, 'ESL'),
(11, 'Finance Dept.-Mactan'),
(12, 'Human Resource Dept.'),
(13, 'Immersion'),
(14, 'IT'),
(15, 'JY Square'),
(16, 'JY Square:BPO Finance & Acctg'),
(17, 'Larry''s Place'),
(18, 'Mactan'),
(19, 'Marketing Dept'),
(20, 'NCLEX'),
(21, 'Ramos'),
(22, 'Rentals'),
(23, 'Right Way Program'),
(24, 'Sales/Marketing'),
(25, 'System Ad Dept.'),
(26, 'Tenants'),
(27, 'TESDA');

-- --------------------------------------------------------

--
-- Table structure for table `as_general_ledger`
--

CREATE TABLE IF NOT EXISTS `as_general_ledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_code` int(11) NOT NULL,
  `transaction_details` varchar(255) NOT NULL,
  `amount` float(11,2) NOT NULL DEFAULT '0.00' COMMENT 'The amount of the transaction. Negative value if it was credited and positive it it was debited',
  `transaction_date` int(11) NOT NULL DEFAULT '0' COMMENT 'default to 0 as of this moment, but remove this when this database goes live',
  `payment_terms` int(11) NOT NULL DEFAULT '5' COMMENT 'the payment terms of the transaction especially if its an account receivable',
  `je_type` varchar(30) DEFAULT NULL,
  `date_added` int(11) NOT NULL DEFAULT '0' COMMENT 'default to 0 as of this moment, but remove this when this database goes live',
  PRIMARY KEY (`id`),
  KEY `account_code` (`account_code`),
  KEY `payment_terms` (`payment_terms`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `as_general_ledger`
--

INSERT INTO `as_general_ledger` (`id`, `account_code`, `transaction_details`, `amount`, `transaction_date`, `payment_terms`, `je_type`, `date_added`) VALUES
(11, 1010300, 'Caregiving manual', 264.00, 1319493600, 5, 'sp-12', 1319521346),
(12, 7010000, 'Caregiving manual', -264.00, 1319493600, 5, 'sp-12', 1319521346),
(13, 1020100, 'Enrollment Fee', 6828.00, 1319522400, 1, 'sp-13', 1319521368),
(14, 4010105, 'Enrollment Fee', -6828.00, 1319522400, 1, 'sp-13', 1319521368),
(15, 1030200, 'An egg from the eggs', 60.00, 1319493600, 1, 'po-3', 1319521392),
(16, 2010100, 'An egg from the eggs', -60.00, 1319493600, 1, 'po-3', 1319521392),
(17, 1010307, 'Course Fee', 115.00, 1319580000, 5, 'sp-14', 1319609297),
(18, 4010105, 'Course Fee', -115.00, 1319580000, 5, 'sp-14', 1319609297),
(19, 1010307, 'Application Fee', 595.00, 1319580000, 5, 'sp-15', 1319609297),
(20, 4010101, 'Application Fee', -595.00, 1319580000, 5, 'sp-15', 1319609297),
(21, 1010300, 'Caregiving manual', 21.00, 1320361200, 5, 'sp-15', 1320369283),
(22, 7010000, 'Caregiving manual', -21.00, 1320361200, 5, 'sp-15', 1320369283),
(23, 1010300, '12', 2431.00, 1320361200, 5, 'sp-16', 1320369380),
(24, 4010102, '12', -2431.00, 1320361200, 5, 'sp-16', 1320369380);

-- --------------------------------------------------------

--
-- Table structure for table `as_invitems`
--

CREATE TABLE IF NOT EXISTS `as_invitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `description` varchar(125) NOT NULL,
  `account_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_code` (`account_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `as_invitems`
--

INSERT INTO `as_invitems` (`id`, `name`, `description`, `account_code`) VALUES
(1, '12% Value Added Tax', '12 % Value Added Tax', 2010400),
(2, 'Call Center', '', 4010102),
(3, 'Call Center:Call Center-Book', 'Call Center - Book', 4010102),
(4, 'Call Center:Call Center Training', 'Call Center Training', 4010102),
(5, 'Call Center:Referral Fee', 'Referral Fee', 7010000),
(6, 'Cancelled', 'Cancelled', 4010000),
(7, 'Caregiver', '', 4010100),
(8, 'Caregiver:Caregiving Books', 'Caregiving manual', 7010000),
(9, 'Caregiver:Course Discount', 'course fee discount', 4010300),
(10, 'Caregiver:Course Fee', 'Course Fee', 4010105),
(11, 'Caregiver:Enrollment Fee', 'Enrollment Fee', 4010105),
(12, 'CELI Dorm Income', '', 4010108),
(13, 'CELI Dorm Income:Rental Income', '', 4010108),
(14, 'CSPOT Sales', 'Various Sales', 7010000),
(15, 'Deferred Charges', '', 1050200),
(16, 'Deferred Charges:Deferred-BF Entertainment', '', 7010000),
(17, 'Deferred Charges:Deferred-BV Ltd.', '', 1020405),
(18, 'Deferred Charges:Deferred-Celi Ltd', '', 1050201),
(19, 'Deferred Charges:Deferred Local', '', 1050200),
(20, 'Dormitory Services', 'Dormitory Services for Students', 4010200),
(21, 'Electrical Charges', 'Electrical Charges', 7010000),
(22, 'English for Nursing Program', 'English for Nurses Program (ENP)', 4010000),
(23, 'General English', 'Course Duration:\\nAccommodation:\\n', 4010101),
(24, 'General English:1. GE- Application Fee', 'Application Fee', 4010101),
(25, 'General English:10. Agent Commissiion', ' Agent Commission', 6031000),
(26, 'General English:10. Others-GE', 'Others:', 4010101),
(27, 'General English:11. Diving Courses', 'Diving Courses', 4010101),
(28, 'General English:11. Gym Usage-GE', 'Gym Usage: Period', 4010101),
(29, 'General English:11.Gym Usage-SAC', 'Gym Usage: Period', 5010101),
(30, 'General English:12. Visa Extension', 'Visa Extension', 4010101),
(31, 'General English:13. Class swapping', 'Class Swapping', 4010101),
(32, 'General English:14.  Key Money', 'Key Money', 2030902),
(33, 'General English:15. Additional Class', ' Additional Class', 4010100),
(34, 'General English:2.   GE-Course Fee', 'Course Fee', 4010101),
(35, 'General English:3. GE-Accommodation', 'Accommodation:', 4010101),
(36, 'General English:4. Discounts', 'Discount', 4010300),
(37, 'General English:5. SSP', 'SSP', 4010101),
(38, 'General English:6. Room Key Deposit-OP', 'Room Key Deposit', 2030902),
(39, 'General English:7. GE-Books', 'GE-Books', 4010101),
(40, 'General English:8. Airport Pick-up', 'Airport Pick-Up:', 4010101),
(41, 'General English:9. ID', 'ID', 4010101),
(42, 'General English:9.I Card', 'I Card', 4010100),
(43, 'General English-CELI LTD', 'English Teaching Services', 4010200),
(44, 'Gym Usage-OP', 'Gym Usage Period:', 2030903),
(45, 'Int''l.Nurses Training Program', 'INTP for Nurses', 4010116),
(46, 'NCLEX', '', 4010102),
(47, 'NCLEX:Application Fee', 'NCLEX  Processing Fee', 4010104),
(48, 'NCLEX:Course Fee', 'NCLEX tuition fees', 4010104),
(49, 'NCLEX:Course Fee Discount', 'Course Discount', 4010300),
(50, 'NCLEX:Course Materials', 'various NCLEX Course Materials', 4010110),
(51, 'NCLEX:NCLEX Trainings', 'Various trainings conducted related to NCLEX', 4010111),
(52, 'Other Income', 'Other Income', 7010000),
(53, 'Other Income:1.Guest Meal', 'Guest Meal', 7010000),
(54, 'Other Income:10. Extra Blanket', '10. Extra Blanket', 4010000),
(55, 'Other Income:2. Van rental', 'Van rental', 7010000),
(56, 'Other Income:3.Employees Meal', 'Employees Meal', 7010001),
(57, 'Other Income:4.Realized Gain on Forex Ex', 'Realized Gain on Forex Ex', 7010000),
(58, 'Other Income:5.Mini Bar', 'Mini Bar', 7010000),
(59, 'Other Income:6.Extra Pillow', 'Extra Pillow', 7010000),
(60, 'Other Income:7. Uniform', 'Uniform', 7010000),
(61, 'Other Income:8. Extra Towel', 'Extra Towel', 4010000),
(62, 'Other Income:9. Dan''s Grill', 'Dan''s Grill', 7010000),
(63, 'Realized (Loss) on Forex Ex', 'Realized (Loss) on Forex Ex', 8010000),
(64, 'Security Deposit', '', 2010300),
(65, 'Unearned Revenue', 'Unearned Revenue', 2030200),
(66, 'Water Charges', 'Water Charges', 7010000),
(67, 'student payment accomodation', '', 1020404),
(68, 'student payment accomodation:Student Payment Accomodation', '', 1020404),
(69, 'temporary borrowing from Officr', '', 2030911),
(70, '1st downpayment', '', 1020900);

-- --------------------------------------------------------

--
-- Table structure for table `as_paymeth`
--

CREATE TABLE IF NOT EXISTS `as_paymeth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `as_paymeth`
--

INSERT INTO `as_paymeth` (`id`, `name`) VALUES
(1, 'cash'),
(2, 'check'),
(3, 'cash & check'),
(4, 'american express'),
(5, 'discover'),
(6, 'JCB'),
(7, 'fund transfer'),
(8, 'visa'),
(9, 'master card'),
(10, 'none');

-- --------------------------------------------------------

--
-- Table structure for table `as_payterms`
--

CREATE TABLE IF NOT EXISTS `as_payterms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(65) NOT NULL COMMENT 'ex current, 31-60days, 61-90days, over 90days',
  `percent` tinyint(5) NOT NULL DEFAULT '0' COMMENT 'extimated % uncollectible',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `as_payterms`
--

INSERT INTO `as_payterms` (`id`, `age`, `percent`) VALUES
(1, 'current', 1),
(2, '31-60 days', 5),
(3, '61-90 days', 15),
(4, 'over 90 days', 42),
(5, 'none', 0);

-- --------------------------------------------------------

--
-- Table structure for table `as_purchase_orders`
--

CREATE TABLE IF NOT EXISTS `as_purchase_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `po_number` varchar(25) NOT NULL COMMENT 'purchase order number',
  `supplier` int(11) NOT NULL COMMENT 'the supplier id',
  `item` varchar(155) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `rate` float(11,2) NOT NULL DEFAULT '0.00' COMMENT 'multiply the rate to the quantity to get the amount that is in the as_general_ledger table',
  `received` int(11) NOT NULL DEFAULT '0' COMMENT '0 if not received then the date in timestamp if received',
  `payment_method` int(11) NOT NULL DEFAULT '1' COMMENT 'the payment method. Ex. (cas, tseke, ug uban pa) referenced to the as_paymeth table',
  `class` int(11) DEFAULT NULL,
  `date_required` int(11) NOT NULL DEFAULT '0' COMMENT 'the date this purchase order will be required',
  PRIMARY KEY (`id`),
  KEY `payment_method` (`payment_method`),
  KEY `class` (`class`),
  KEY `supplier` (`supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `as_purchase_orders`
--

INSERT INTO `as_purchase_orders` (`id`, `po_number`, `supplier`, `item`, `qty`, `rate`, `received`, `payment_method`, `class`, `date_required`) VALUES
(3, 'PO-0000000001', 4, 'Eggs : each', 12, 5.00, 0, 1, 10, 1319580000);

-- --------------------------------------------------------

--
-- Table structure for table `as_sales_postings`
--

CREATE TABLE IF NOT EXISTS `as_sales_postings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invsn_no` varchar(25) NOT NULL COMMENT 'if its a sales no ''SN-000001'' or the official receipt, if its an invoice number ''INV-000001''',
  `customer` varchar(45) NOT NULL COMMENT 'sold to or the customer id',
  `item` varchar(155) NOT NULL COMMENT 'ex. NCLEX: Application Fee. the transaction_details field in the as_general_ledger table will be the description. thanks ^^',
  `qty` int(11) NOT NULL DEFAULT '0' COMMENT 'quantity',
  `rate` float(11,2) NOT NULL DEFAULT '0.00' COMMENT 'multiply the rate to the quantity to get the amount that is in the as_general_ledger table',
  `paymeth` int(11) NOT NULL DEFAULT '10' COMMENT 'the payment method. Ex. (cash, cheque, etc...). Referenced to the paymeth table',
  `cheque_num` varchar(65) NOT NULL DEFAULT '0' COMMENT 'the cheque number if the payment method is a tseke',
  `type` enum('invoice','sales') NOT NULL,
  `class` int(11) NOT NULL,
  `due_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `customer` (`customer`),
  KEY `paymeth` (`paymeth`),
  KEY `class` (`class`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `as_sales_postings`
--

INSERT INTO `as_sales_postings` (`id`, `invsn_no`, `customer`, `item`, `qty`, `rate`, `paymeth`, `cheque_num`, `type`, `class`, `due_date`) VALUES
(12, 'SN-0000000001', '3:customer', 'Caregiver:Caregiving Books', 12, 22.00, 1, '', 'sales', 10, 0),
(13, 'INV-0000000002', '5:customer', 'Caregiver:Enrollment Fee', 12, 569.00, 10, '0', 'invoice', 6, 1319666400),
(14, 'SN-0000000003', '2:customer', 'Caregiver:Course Fee', 1, 115.00, 2, '55555', 'sales', 11, 0),
(15, 'SN-0000000004', '1:guest', 'Caregiver:Caregiving Books', 1, 21.00, 1, '', 'sales', 2, 0),
(16, 'SN-0000000005', '1:guest', 'Call Center', 11, 221.00, 1, '', 'sales', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `class_type` enum('1:1','1:4','1:8') DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL COMMENT 'from course table',
  PRIMARY KEY (`book_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `title`, `class_type`, `course_id`) VALUES
(2, 'American Headway 3', '1:1', 8),
(3, 'American Headway 3', '1:1', 8),
(4, 'Know How Openera', '1:4', 8),
(5, 'Know How 1', '1:4', 8),
(6, 'Know How 2', '1:4', 8),
(7, 'Know How 3', '1:4', 8),
(8, 'New Interchange Intro', '1:8', 8),
(9, 'New Interchange 1', '1:8', 8),
(10, 'New Interchange 2', '1:8', 8),
(11, 'New Interchange 3', '1:8', 8),
(12, 'Real Writing 1', '1:1', 15),
(13, 'Real Writing 2', '1:1', 15),
(14, 'Real Writing 3', '1:1', 15),
(15, 'Real Reading 1', '1:1', 15),
(16, 'Real Reading 2', '1:1', 15),
(17, 'Real Reading 3', '1:1', 15),
(18, 'Real Listening and Speaking 1', '1:1', 15),
(19, 'Real Listening and Speaking 2', '1:1', 15),
(20, 'Real Listening and Speaking 3', '1:1', 15),
(27, 'Let''s Talk 1', '1:1', 8),
(68, 'Know How 2', '1:1', 2),
(69, 'bar', NULL, NULL),
(70, 'va', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cg_activities`
--

CREATE TABLE IF NOT EXISTS `cg_activities` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(250) NOT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cg_activities`
--

INSERT INTO `cg_activities` (`act_id`, `act_name`) VALUES
(1, 'Lecture Discussion'),
(2, 'Oral Recitation');

-- --------------------------------------------------------

--
-- Table structure for table `cg_class`
--

CREATE TABLE IF NOT EXISTS `cg_class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_id` int(11) NOT NULL,
  `days` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `duration` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `cg_class`
--

INSERT INTO `cg_class` (`class_id`, `mod_id`, `days`, `time`, `duration`, `semester`) VALUES
(21, 1, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20121),
(22, 3, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20121),
(23, 5, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20122),
(25, 3, 'monday-tuesday-wednesday', '06:00-10:00', 3, 20122),
(28, 2, 'monday-tuesday-wednesday', '06:00-10:00', 2, 20122);

-- --------------------------------------------------------

--
-- Table structure for table `cg_enroll`
--

CREATE TABLE IF NOT EXISTS `cg_enroll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `cg_enroll`
--

INSERT INTO `cg_enroll` (`id`, `student_id`, `class_id`, `date_added`) VALUES
(52, 1, 21, '05/10/2012'),
(53, 2, 21, '05/14/2012'),
(54, 1, 22, '05/14/2012'),
(55, 3, 23, '05/14/2012'),
(56, 1, 23, '05/15/2012'),
(57, 3, 25, '05/15/2012'),
(58, 1, 28, '05/17/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cg_fees`
--

CREATE TABLE IF NOT EXISTS `cg_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `fees` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `cg_fees`
--

INSERT INTO `cg_fees` (`id`, `student_id`, `fees`, `semester`, `date_added`) VALUES
(8, 1, 1000, '20121', '05/14/2012'),
(9, 2, 500, '20121', '05/14/2012'),
(10, 3, 1000, '20122', '05/15/2012'),
(11, 1, 1000, '20122', '05/17/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cg_grades`
--

CREATE TABLE IF NOT EXISTS `cg_grades` (
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `mod_id` int(11) NOT NULL,
  `grade` decimal(5,1) NOT NULL,
  `date_added` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cg_grades`
--

INSERT INTO `cg_grades` (`student_id`, `class_id`, `mod_id`, `grade`, `date_added`) VALUES
(1, 21, 0, 99.0, '05/10/2012'),
(1, 23, 0, 88.0, '05/16/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cg_semester`
--

CREATE TABLE IF NOT EXISTS `cg_semester` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cg_semester`
--

INSERT INTO `cg_semester` (`id`, `semester`) VALUES
(2, 20121),
(3, 20122);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('297156b9022aaf0c698849fc097c85f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1', 1349179848, 'a:1:{s:8:"emp_info";a:5:{s:2:"id";s:2:"12";s:6:"status";s:6:"active";s:10:"department";s:9:"Academics";s:10:"date_added";s:1:"0";s:8:"fullname";s:14:"Welcome  Guest";}}');

-- --------------------------------------------------------

--
-- Table structure for table `classrooms`
--

CREATE TABLE IF NOT EXISTS `classrooms` (
  `classroom_id` int(4) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(30) NOT NULL,
  PRIMARY KEY (`classroom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `classrooms`
--

INSERT INTO `classrooms` (`classroom_id`, `room_name`) VALUES
(1, '113'),
(2, '114'),
(3, '115'),
(4, '116'),
(5, '117'),
(6, '118'),
(7, '119'),
(8, '120'),
(9, '121'),
(10, '122'),
(11, '123'),
(12, '124'),
(13, '125'),
(14, '126'),
(15, '127'),
(16, '128'),
(17, '129'),
(18, '130'),
(19, '131'),
(20, '132'),
(21, '133'),
(22, '134'),
(23, '135'),
(24, '136'),
(25, '137'),
(26, '138'),
(27, '139'),
(28, '140'),
(29, '141'),
(30, '142'),
(31, '003'),
(32, '004');

-- --------------------------------------------------------

--
-- Table structure for table `class_activities`
--

CREATE TABLE IF NOT EXISTS `class_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(250) NOT NULL,
  `class_id` int(11) NOT NULL,
  `start_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `class_activities`
--

INSERT INTO `class_activities` (`id`, `act_name`, `class_id`, `start_date`, `end_date`) VALUES
(1, 'Lecture', 21, '05/11/2012', '05/31/2012'),
(8, 'Lecture Discussion', 23, '05/01/2012', '05/16/2012'),
(11, 'Oral', 23, '05/17/2012', '05/18/2012');

-- --------------------------------------------------------

--
-- Table structure for table `class_students`
--

CREATE TABLE IF NOT EXISTS `class_students` (
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  KEY `class_id` (`class_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_students`
--

INSERT INTO `class_students` (`class_id`, `student_id`) VALUES
(12, 1),
(10, 3),
(10, 4),
(10, 5),
(10, 2),
(15, 4);

-- --------------------------------------------------------

--
-- Table structure for table `contrib_phic`
--

CREATE TABLE IF NOT EXISTS `contrib_phic` (
  `contrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `salary_bracket` varchar(4) NOT NULL,
  `range_min` decimal(8,2) NOT NULL,
  `range_max` decimal(8,2) NOT NULL,
  `salary_base` decimal(8,2) NOT NULL,
  `personal_share` decimal(8,2) NOT NULL,
  `employer_share` decimal(8,2) NOT NULL,
  `total_monthly` decimal(8,2) NOT NULL,
  PRIMARY KEY (`contrib_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `contrib_phic`
--

INSERT INTO `contrib_phic` (`contrib_id`, `salary_bracket`, `range_min`, `range_max`, `salary_base`, `personal_share`, `employer_share`, `total_monthly`) VALUES
(6, '4', 7000.00, 7999.99, 7000.00, 87.50, 87.50, 175.00),
(7, '5', 8000.00, 8999.99, 8000.00, 100.00, 100.00, 200.00),
(8, '6', 9000.00, 9999.99, 9000.00, 112.50, 112.50, 225.00),
(9, '7', 10000.00, 10999.99, 10000.00, 125.00, 125.00, 250.00),
(10, '8', 11000.00, 11999.99, 11000.00, 137.50, 137.50, 275.00),
(11, '9', 12000.00, 12999.99, 12000.00, 150.00, 150.00, 300.00),
(12, '10', 13000.00, 13999.99, 13000.00, 162.50, 162.50, 325.00),
(13, '11', 14000.00, 14999.99, 14000.00, 175.00, 175.00, 350.00),
(14, '12', 15000.00, 15999.99, 15000.00, 187.50, 187.50, 375.00),
(15, '13', 16000.00, 16999.99, 16000.00, 200.00, 200.00, 400.00),
(16, '14', 17000.00, 17999.99, 17000.00, 212.50, 212.50, 425.00),
(17, '15', 18000.00, 18999.99, 18000.00, 225.00, 225.00, 450.00),
(18, '16', 19000.00, 19999.99, 19000.00, 237.50, 237.50, 475.00),
(19, '17', 20000.00, 20999.99, 20000.00, 250.00, 250.00, 500.00),
(20, '18', 21000.00, 21999.99, 21000.00, 262.50, 262.50, 525.00),
(21, '19', 22000.00, 22999.99, 22000.00, 275.00, 275.00, 550.00),
(22, '20', 23000.00, 23999.99, 23000.00, 287.50, 287.50, 575.00),
(23, '21', 24000.00, 24999.99, 24000.00, 300.00, 300.00, 600.00),
(24, '22', 25000.00, 25999.99, 25000.00, 312.50, 312.50, 625.00),
(25, '23', 26000.00, 26999.99, 26000.00, 325.00, 325.00, 650.00),
(26, '24', 27000.00, 27999.99, 27000.00, 337.50, 337.50, 675.00),
(27, '25', 28000.00, 28999.99, 28000.00, 350.00, 350.00, 700.00),
(28, '26', 29000.00, 29999.99, 29000.00, 362.50, 362.50, 725.00),
(29, '27', 30000.00, 100000.00, 30000.00, 375.00, 375.00, 750.00),
(30, '2', 5000.00, 5999.99, 5000.00, 62.50, 62.50, 125.00),
(31, '3', 6000.00, 6999.99, 6000.00, 75.00, 75.00, 150.00);

-- --------------------------------------------------------

--
-- Table structure for table `contrib_sss`
--

CREATE TABLE IF NOT EXISTS `contrib_sss` (
  `contrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `range_min` decimal(8,2) NOT NULL,
  `range_max` decimal(8,2) NOT NULL,
  `employer` decimal(8,2) NOT NULL,
  `employee` decimal(8,2) NOT NULL,
  `ecer` decimal(8,2) NOT NULL,
  PRIMARY KEY (`contrib_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `contrib_sss`
--

INSERT INTO `contrib_sss` (`contrib_id`, `range_min`, `range_max`, `employer`, `employee`, `ecer`) VALUES
(5, 1750.00, 2249.99, 141.30, 66.70, 10.00),
(9, 4250.00, 4749.99, 318.00, 150.00, 10.00),
(10, 4750.00, 5249.99, 353.30, 166.70, 10.00),
(11, 5250.00, 5749.99, 388.70, 183.30, 10.00),
(12, 5750.00, 6249.99, 424.00, 200.00, 10.00),
(13, 6250.00, 6749.99, 459.30, 216.70, 10.00),
(14, 6750.00, 7249.99, 494.70, 233.30, 10.00),
(15, 7250.00, 7749.99, 530.00, 250.00, 10.00),
(16, 7750.00, 8249.99, 565.30, 266.70, 10.00),
(17, 8250.00, 8749.99, 600.70, 283.30, 10.00),
(18, 8750.00, 9249.99, 636.00, 300.00, 10.00),
(19, 9250.00, 9749.99, 671.30, 316.70, 10.00),
(20, 9750.00, 10249.99, 706.70, 333.30, 10.00),
(21, 10250.00, 10749.99, 742.00, 350.00, 10.00),
(22, 10750.00, 11249.99, 777.30, 366.70, 10.00),
(23, 11250.00, 11749.99, 812.70, 383.30, 10.00),
(24, 11750.00, 12249.99, 848.00, 400.00, 10.00),
(25, 12250.00, 12749.99, 883.30, 416.70, 10.00),
(26, 12750.00, 13249.99, 918.70, 433.30, 10.00),
(27, 13250.00, 13749.99, 918.70, 433.30, 10.00),
(31, 13750.00, 14249.99, 989.30, 466.70, 10.00),
(32, 14250.00, 14749.99, 1024.70, 483.30, 10.00),
(33, 14750.00, 999999.99, 1060.00, 500.00, 30.00);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `type` enum('elective','regular') NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `name`, `type`) VALUES
(1, 'Toefl IBT', 'elective'),
(2, 'TOEIC', 'elective'),
(3, 'lelts', 'elective'),
(4, 'CS', 'regular'),
(5, 'CK', 'regular'),
(6, 'Pre-Teen', 'elective'),
(7, 'CT', 'regular'),
(8, 'GE', 'regular'),
(9, 'SPK', 'regular'),
(10, 'AW', 'regular'),
(11, 'BE', 'regular'),
(12, 'Test Prep Intensive', 'elective'),
(13, 'IE 1hr', 'regular'),
(14, 'GE 1hr', 'regular'),
(15, 'IE', 'regular');

-- --------------------------------------------------------

--
-- Table structure for table `course_level`
--

CREATE TABLE IF NOT EXISTS `course_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `course_level`
--

INSERT INTO `course_level` (`id`, `name`) VALUES
(1, 'Beginner Low'),
(2, 'Beginner Med'),
(3, 'Beginner High'),
(4, 'Pre-Intermediate Low'),
(5, 'Pre-Intermediate Med'),
(6, 'Pre-Intermediate High'),
(7, 'Intermediate-Low'),
(8, 'Intermediate-Med'),
(9, 'Intermediate-High'),
(10, 'Advanced'),
(11, 'Upper Advanced');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `email_address`, `contact_no`, `address`, `status`, `date_added`) VALUES
(1, 'Rob', 'Smith', 'smith@localhost.com', '54545', 'Maine Street, California, USA', 'active', 0),
(2, 'first 123', 'sample customer', 'email_address@mail.com', '123456789', 'address 1', 'active', 1318303244),
(3, 'retrieve', 'cust test', '123@123.com', '1234123', '123', 'active', 1318386705),
(4, 'url incorrect test', 'here', 'here@here.com', '1234', 'here', 'active', 1318387607),
(5, 'sample sorting', 'added', 'sample@yahoo.com', '1234567', 'here', 'active', 1318399424);

-- --------------------------------------------------------

--
-- Table structure for table `daily_sales_report`
--

CREATE TABLE IF NOT EXISTS `daily_sales_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(65) NOT NULL,
  `contents` text NOT NULL,
  `sales_type` enum('hotel') NOT NULL,
  `filed_by` int(11) NOT NULL,
  `date_filed` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filed_by` (`filed_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `daily_sales_report`
--

INSERT INTO `daily_sales_report` (`id`, `subject`, `contents`, `sales_type`, `filed_by`, `date_filed`) VALUES
(1, 'Sample #1', '<p>\n	<span>:this is a test</span></p>\n', 'hotel', 12, 1326361133),
(2, 'Sample #2', '<p>\n	hello this is a test...</p>\n', 'hotel', 12, 1326362079),
(3, 'Sample #3', '<p>\n	This is a report from</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&lt;table style=&quot;width: 330px; height: 60px;&quot; 500px;&quot;=&quot;&quot; border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot;&gt;</p>\n<tbody>\n	<tr>\n		<td>\n			test</td>\n		<td>\n			test</td>\n	</tr>\n	<tr>\n		<td>\n			1</td>\n		<td>\n			sdf</td>\n	</tr>\n	<tr>\n		<td>\n			2</td>\n		<td>\n			&nbsp;</td>\n	</tr>\n	<tr>\n		<td>\n			&nbsp;</td>\n		<td>\n			&nbsp;</td>\n	</tr>\n</tbody>\n<p>\n	&nbsp;</p>\n', 'hotel', 12, 1326440454),
(4, 'Mao ni ang nakuha...', '<p>\n	We aproximately get this one to the button &#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;&#39;</p>\n', 'hotel', 12, 1330406553);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `deptd_id` int(11) NOT NULL,
  PRIMARY KEY (`dept_id`),
  KEY `emp_id` (`emp_id`),
  KEY `deptd_id` (`deptd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `emp_id`, `deptd_id`) VALUES
(1, 5, 5),
(2, 7, 5),
(3, 10, 7),
(4, 11, 7),
(5, 12, 7),
(8, 14, 7),
(9, 15, 8),
(11, 17, 10),
(12, 18, 2),
(16, 21, 11),
(17, 20, 2),
(18, 16, 11),
(19, 13, 2),
(21, 23, 2),
(22, 24, 2),
(23, 25, 2),
(26, 68, 5),
(27, 69, 2);

-- --------------------------------------------------------

--
-- Table structure for table `department_details`
--

CREATE TABLE IF NOT EXISTS `department_details` (
  `deptd_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(50) NOT NULL,
  `dept_code` varchar(30) NOT NULL,
  `dept_head` int(11) NOT NULL,
  `is_dept` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`deptd_id`),
  KEY `dept_head` (`dept_head`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `department_details`
--

INSERT INTO `department_details` (`deptd_id`, `dept_name`, `dept_code`, `dept_head`, `is_dept`) VALUES
(1, 'Vice President', 'VP', 1, '1'),
(2, 'Operations', 'Operations', 1, '1'),
(3, 'Food And Beverages', 'F&B', 1, '0'),
(4, 'House Keeping', '', 4, '0'),
(5, 'InfoTech', 'IT', 1, '1'),
(7, 'Academics', 'ACAD', 10, '1'),
(8, 'Hotel ( Front Office )', 'FO', 15, '1'),
(10, 'Human Resource', 'HRD', 17, '1'),
(11, 'Care Giver', 'CG', 21, '1');

-- --------------------------------------------------------

--
-- Table structure for table `employment_type`
--

CREATE TABLE IF NOT EXISTS `employment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employment_type`
--

INSERT INTO `employment_type` (`id`, `name`) VALUES
(1, 'probationary'),
(2, 'regular'),
(3, 'contractual');

-- --------------------------------------------------------

--
-- Table structure for table `emp_contact_details`
--

CREATE TABLE IF NOT EXISTS `emp_contact_details` (
  `emp_id` int(11) NOT NULL,
  `country` varchar(30) NOT NULL,
  `street` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `province` varchar(30) DEFAULT NULL,
  `zipcode` int(11) NOT NULL,
  `telno` varchar(25) DEFAULT NULL,
  `mobileno` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_contact_details`
--

INSERT INTO `emp_contact_details` (`emp_id`, `country`, `street`, `city`, `province`, `zipcode`, `telno`, `mobileno`, `email`) VALUES
(10, 'Philippines', 'Street123', 'Cebu', 'Cebu', 6010, '', '', ''),
(11, 'Philippines', 'Perrelos', 'Cebu', 'Cebu', 6018, '123', '', ''),
(12, '', '', 'Opon Lapu-Lapu City', '', 456, '', '', ''),
(13, '', '', '', '', 0, '', '', ''),
(16, 'Korea', 'North East, SOuth Korea', '0', '0', 0, '12345', '', 'kiera@localhost.com'),
(17, '', '', '', '', 0, '', '', ''),
(20, '', '', '', '', 0, '', '', ''),
(21, '', '', '', '', 0, '', '', ''),
(23, '', '', '', '', 745, '', '', ''),
(25, '', '', '', '', 0, '', '', ''),
(68, 'Philippines', 'saac', 'LLC', 'LLC', 6015, '1231234', '09331216391', '09331216391');

-- --------------------------------------------------------

--
-- Table structure for table `emp_contract`
--

CREATE TABLE IF NOT EXISTS `emp_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `start` int(11) NOT NULL DEFAULT '0' COMMENT 'Start date in timestmap',
  `end` int(11) NOT NULL DEFAULT '0' COMMENT 'End date in timestamp',
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emp_contrib_details`
--

CREATE TABLE IF NOT EXISTS `emp_contrib_details` (
  `emp_id` int(11) NOT NULL,
  `sss` varchar(100) DEFAULT NULL,
  `tin` varchar(100) DEFAULT NULL,
  `phic` varchar(100) DEFAULT NULL,
  `hdmf` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_contrib_details`
--

INSERT INTO `emp_contrib_details` (`emp_id`, `sss`, `tin`, `phic`, `hdmf`) VALUES
(10, '1123', '', '12345', ''),
(11, '1110 - 0101 - 1001 ', 'EF92 - D1C5 - 581A ', '11100-1002-110', '23-123-4232-AD23 '),
(12, '23', '232', '23', '2'),
(13, '', '', '', ''),
(16, '0', '0', '0', '0'),
(17, '', '', '', ''),
(20, '', '1024', '', ''),
(21, '', '', '', ''),
(23, '', '', '', ''),
(25, '', '', '', ''),
(68, '111', '222', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `emp_dependents`
--

CREATE TABLE IF NOT EXISTS `emp_dependents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL COMMENT 'can add mutiple data with duplicate emp_id',
  `name` varchar(50) NOT NULL COMMENT 'Dependents Name',
  `relationship` varchar(25) NOT NULL,
  `birthdate` int(11) NOT NULL DEFAULT '0' COMMENT 'In Unix timestamp',
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emp_education`
--

CREATE TABLE IF NOT EXISTS `emp_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `school` varchar(65) NOT NULL,
  `course` varchar(65) NOT NULL,
  `year_start` int(11) NOT NULL,
  `year_end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_education`
--

INSERT INTO `emp_education` (`id`, `emp_id`, `school`, `course`, `year_start`, `year_end`) VALUES
(1, 11, 'Cebu Institute of Technology', 'Bachelor of Science in Computer Engineering', 1323990000, 1325199600);

-- --------------------------------------------------------

--
-- Table structure for table `emp_emergency_contact`
--

CREATE TABLE IF NOT EXISTS `emp_emergency_contact` (
  `emp_id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL COMMENT 'Name of the employee''s emergency contact',
  `relationship` varchar(25) NOT NULL COMMENT 'Relationship to the employee ex(Father, Mother, Sister, etc)',
  `telno` varchar(25) DEFAULT NULL,
  `mobileno` varchar(25) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_emergency_contact`
--

INSERT INTO `emp_emergency_contact` (`emp_id`, `name`, `relationship`, `telno`, `mobileno`, `address`) VALUES
(11, 'Jane Doe', 'Mother', '987456', '0123456798', ''),
(12, '', 'llk', '', 'sdsdfsdf', ''),
(13, '', '', '', '', ''),
(16, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(23, '', '', '', '', ''),
(25, 'dfgdfg', 'dfgdfg', 'dfgfdg', 'fdgfdg', ''),
(68, 'roland', 'brother', '', '', 'saac 1 llc');

-- --------------------------------------------------------

--
-- Table structure for table `emp_immigration`
--

CREATE TABLE IF NOT EXISTS `emp_immigration` (
  `emp_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL COMMENT 'visa/passport',
  `number` varchar(25) NOT NULL COMMENT 'The Immigration Number',
  `status` varchar(25) NOT NULL COMMENT 'Immigration Status',
  `review_date` int(11) NOT NULL DEFAULT '0' COMMENT 'Date in unix timestamp',
  `citizenship` varchar(25) NOT NULL,
  `issued_date` int(11) NOT NULL DEFAULT '0' COMMENT 'Date in unix timestamp',
  `expiry_date` int(11) NOT NULL DEFAULT '0' COMMENT 'Date in unix timestamp',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_immigration`
--

INSERT INTO `emp_immigration` (`emp_id`, `type`, `number`, `status`, `review_date`, `citizenship`, `issued_date`, `expiry_date`) VALUES
(10, '', '', '', 1324567615, '', 1324481215, 1324394815),
(11, 'Visa', '123456789 ', 'Fine', 1325060998, 'Filipino', 1324542598, 1325147398),
(12, '', 'weewr', '', 1334268000, 'sdfdsf', 1329865200, 1334268000),
(13, '', '', '', -3600, '''\\klk', -3600, -3600),
(16, '', '', '', 0, '', 0, 0),
(17, '', '', '', 1343618782, '', 1343618782, 1343618782),
(20, '', '', '', -3600, '', -3600, -3600),
(21, '', '', '', -3600, '', -3600, -3600),
(68, '', '', '', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `emp_job_details`
--

CREATE TABLE IF NOT EXISTS `emp_job_details` (
  `emp_id` int(11) NOT NULL,
  `job_title` int(11) NOT NULL COMMENT 'link to the job_title table',
  `job_salary` decimal(8,2) NOT NULL,
  `curr_empstatus` int(11) NOT NULL COMMENT 'the current employment status of the employee ex(regular, provisionary, resigned) and linked to the employment type table and somehow related to the emp_contract table',
  `job_status` int(11) NOT NULL DEFAULT '1' COMMENT 'Connected to the job status table',
  `join_date` int(11) NOT NULL COMMENT 'date in unix time',
  `end_date` int(11) NOT NULL DEFAULT '0',
  `job_leave` int(11) NOT NULL DEFAULT '7' COMMENT 'number of leave of the employee',
  `job_location` varchar(25) NOT NULL COMMENT 'ex. cebu, etc..',
  PRIMARY KEY (`emp_id`),
  KEY `job_title` (`job_title`),
  KEY `curr_empstatus` (`curr_empstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_job_details`
--

INSERT INTO `emp_job_details` (`emp_id`, `job_title`, `job_salary`, `curr_empstatus`, `job_status`, `join_date`, `end_date`, `job_leave`, `job_location`) VALUES
(10, 6, 20000.00, 2, 1, 996616800, 0, 7, 'mactan'),
(11, 7, 20000.00, 2, 1, 1128376800, 0, 7, 'mactan'),
(12, 7, 20000.00, 1, 1, 0, 0, 7, 'mactan'),
(13, 7, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(14, 7, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(15, 8, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(16, 7, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(17, 1, 20000.00, 2, 1, 1335909600, 0, 7, 'cebu'),
(18, 5, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(20, 8, 20000.00, 3, 1, 0, 0, 7, 'mactan'),
(21, 9, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(23, 8, 20000.00, 1, 1, 0, 0, 7, 'mactan'),
(24, 8, 20000.00, 3, 1, 0, 0, 7, 'mactan'),
(25, 8, 20000.00, 2, 1, 0, 0, 7, 'mactan'),
(68, 1, 20000.00, 3, 1, 1342044000, 1348956000, 7, 'mactan');

-- --------------------------------------------------------

--
-- Table structure for table `emp_language`
--

CREATE TABLE IF NOT EXISTS `emp_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `fluency` varchar(35) NOT NULL,
  `competency` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_language`
--

INSERT INTO `emp_language` (`id`, `emp_id`, `name`, `fluency`, `competency`) VALUES
(1, 11, 'Tagalog', 'Speaking', 'Good, Average');

-- --------------------------------------------------------

--
-- Table structure for table `emp_leaves`
--

CREATE TABLE IF NOT EXISTS `emp_leaves` (
  `leave_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_requested` int(11) NOT NULL COMMENT 'filing date',
  `leave_type` enum('sick leave','vacation leave','maternity leave','paternity leave','others') NOT NULL,
  `reason` varchar(300) NOT NULL,
  `time_requested` float(11,2) NOT NULL,
  `pay_type` enum('0','1') NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `return_date` int(11) NOT NULL,
  `imm_head_appvl` enum('pending','rejected','cancelled','approved') DEFAULT 'pending',
  `hr_appvl` enum('pending','rejected','cancelled','approved') DEFAULT 'pending',
  PRIMARY KEY (`leave_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `emp_leaves`
--

INSERT INTO `emp_leaves` (`leave_id`, `emp_id`, `date_requested`, `leave_type`, `reason`, `time_requested`, `pay_type`, `leave_start`, `leave_end`, `return_date`, `imm_head_appvl`, `hr_appvl`) VALUES
(4, 11, 1323298800, 'others', 'test', 0.50, '1', 1323903600, 1323990000, 1324249200, 'approved', 'rejected'),
(5, 11, 1323385200, 'vacation leave', 'teast', 0.50, '1', 1323644400, 1323730800, 1323817200, 'pending', 'pending'),
(6, 11, 1323212400, 'others', '', 0.50, '1', 1324335600, 1324335600, 1324854000, 'cancelled', 'pending'),
(7, 11, 1322694000, 'others', '', 0.50, '1', 1324940400, 1324940400, 1324940400, 'approved', 'approved'),
(8, 13, 1324508400, 'others', 'd', 0.50, '1', 1324594800, 1324508400, 1324594800, 'pending', 'pending'),
(17, 20, 1331766000, 'others', '', 0.50, '1', 1332799200, 1332198000, 1332111600, 'pending', 'pending'),
(19, 20, 1332111600, 'others', '', 10.00, '1', 1331161200, 1330902000, 1331593200, 'pending', 'pending'),
(24, 10, 1338501600, 'paternity leave', '', 1.00, '1', 1338501600, 1338588000, 1338588000, 'pending', 'pending'),
(25, 17, 1338847200, 'sick leave', '', 1.00, '1', 1338847200, 1338933600, 1339020000, 'cancelled', 'cancelled'),
(26, 17, 1338847200, 'vacation leave', '', 2.00, '1', 1338847200, 1338933600, 1339020000, 'cancelled', 'cancelled'),
(27, 12, 1340056800, 'sick leave', '', 1.00, '1', 1340229600, 1340229600, 1340316000, 'approved', 'approved'),
(28, 12, 1340056800, 'sick leave', '', 2.00, '1', 1340661600, 1340748000, 1340834400, 'approved', 'approved'),
(31, 12, 1340056800, 'sick leave', '', 1.00, '0', 1341784800, 1341784800, 1341871200, 'approved', 'approved'),
(32, 12, 1338415200, 'vacation leave', '', 3.00, '1', 1338760800, 1338933600, 1339020000, 'approved', 'approved'),
(33, 12, 1339711200, 'maternity leave', '', 2.00, '1', 1339970400, 1340056800, 1340143200, 'approved', 'approved'),
(34, 12, 1340575200, 'others', '', 5.00, '1', 1340575200, 1340920800, 1341180000, 'approved', 'approved'),
(36, 12, 1333663200, 'others', '', 5.00, '1', 1333922400, 1334268000, 1334527200, 'approved', 'approved'),
(37, 17, 1340316000, 'maternity leave', '', 3.00, '1', 1340748000, 1340920800, 1340920800, 'rejected', 'rejected');

-- --------------------------------------------------------

--
-- Table structure for table `emp_license`
--

CREATE TABLE IF NOT EXISTS `emp_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `type` varchar(25) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_license`
--

INSERT INTO `emp_license` (`id`, `emp_id`, `name`, `type`, `start`, `end`) VALUES
(1, 11, 'Drivers License', 'Platinum', 1325113200, 1325286000);

-- --------------------------------------------------------

--
-- Table structure for table `emp_ot_ut_detail`
--

CREATE TABLE IF NOT EXISTS `emp_ot_ut_detail` (
  `ot_ut_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_ot_ut_id` int(11) NOT NULL,
  `ot_ut_start` int(11) NOT NULL,
  `ot_ut_end` int(11) NOT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ot_ut_details_id`),
  KEY `emp_ot_ut_id` (`emp_ot_ut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emp_overtime`
--

CREATE TABLE IF NOT EXISTS `emp_overtime` (
  `otime_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_file` int(11) NOT NULL,
  `date_covered` int(11) NOT NULL,
  `reason` varchar(300) NOT NULL,
  `otime_date` int(11) NOT NULL,
  `otime_from` time NOT NULL,
  `otime_to` time NOT NULL,
  `total_hours` decimal(8,2) NOT NULL,
  `imm_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  `hr_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`otime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `emp_overtime`
--

INSERT INTO `emp_overtime` (`otime_id`, `emp_id`, `date_file`, `date_covered`, `reason`, `otime_date`, `otime_from`, `otime_to`, `total_hours`, `imm_head_appvl`, `hr_appvl`) VALUES
(1, 17, 1340661600, 1340661600, 'tyr thet', 1340661600, '12:00:00', '17:30:00', 5.30, 'approved', 'rejected'),
(2, 17, 1340661600, 1340661600, 'tyr thet', 1340661600, '18:00:00', '20:00:00', 2.00, 'approved', 'approved'),
(3, 17, 1340748000, 1340920800, 'going to bantayan', 1340920800, '13:00:00', '18:00:00', 5.00, 'approved', 'approved'),
(4, 10, 1340834400, 1340834400, 'sick', 1340920800, '13:00:00', '18:00:00', 5.00, 'approved', 'approved'),
(5, 10, 1340834400, 1340834400, 'asdasd', 1340920800, '13:05:00', '17:05:00', 4.00, 'approved', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `emp_overtime_undertime`
--

CREATE TABLE IF NOT EXISTS `emp_overtime_undertime` (
  `emp_ot_ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `type` enum('overtime','undertime') DEFAULT NULL,
  `pay_period_start` int(11) NOT NULL,
  `pay_period_end` int(11) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `imm_head_appvl` int(11) DEFAULT '0',
  `hr_appvl` int(11) DEFAULT '0',
  PRIMARY KEY (`emp_ot_ut_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emp_personal_details`
--

CREATE TABLE IF NOT EXISTS `emp_personal_details` (
  `emp_id` int(11) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `nickname` varchar(25) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `birthdate` int(11) NOT NULL DEFAULT '0',
  `nationality` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL,
  `photo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_personal_details`
--

INSERT INTO `emp_personal_details` (`emp_id`, `id_number`, `firstname`, `middlename`, `lastname`, `nickname`, `gender`, `birthdate`, `nationality`, `status`, `photo`) VALUES
(10, '11-011102', 'Acad', 'First', 'Admin', 'acadmin', 'Male', 1970, 'Filipino', 'Widowed', ''),
(11, '0', 'Aidan', 'D', 'Alburo', 'aidan', 'Female', 1343858400, 'Filipino', 'Single', ''),
(12, '123-897', 'Welcome', '', 'Guest', 'Jonie', 'Female', 632358000, 'Filipino', 'Married', ''),
(13, '11-000003', 'Neriza', 'M', 'Trasmel', '', 'Male', -3600, '', 'Married', ''),
(14, '11-000004', 'Shirra', 'S', 'Augusto', 'Shir', 'Male', 1970, '', '', ''),
(15, '2323', 'super', 'hotel', 'user', '', '', 0, '', '', ''),
(16, '0', 'dummy', 'd', 'user', 'dummy', 'Male', 1337896800, 'korean', 'Single', ''),
(17, '0', 'hr', 'admin', 'admin', 'hr', 'Male', -3600, '', 'Widowed', ''),
(18, '11-000009', 'food', 'and', 'beverages', 'f and b', 'Male', 1970, '', '', ''),
(20, '12-022301', 'Mary', 'p', 'Jane', '', 'Female', 1334613600, '', 'Widowed', ''),
(21, '11-000005', 'Sahlee Pearl', 'V', 'Conson', 'Sahlee', 'Female', 0, 'filipino', 'Single', ''),
(23, '123455666', 'Mary Jane', '', 'Orbita', 'Jane', 'Male', 0, '', 'Married', ''),
(24, '125', 'On', '', 'Duty', '', '', 0, '', '', ''),
(25, '1223', 'Main', '', 'Tenance', '', 'Male', 0, '', 'Single', ''),
(68, '12345', 'reyjomer', 'B', 'fernandez', 'jomer', 'Male', 670629600, 'Filipino', 'Single', NULL),
(69, '12345', '', '', '', '', '', 0, '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emp_skills`
--

CREATE TABLE IF NOT EXISTS `emp_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `num_years_exp` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_skills`
--

INSERT INTO `emp_skills` (`id`, `emp_id`, `name`, `num_years_exp`) VALUES
(1, 11, 'Junior Programmer', '1 year experience');

-- --------------------------------------------------------

--
-- Table structure for table `emp_total_leaves`
--

CREATE TABLE IF NOT EXISTS `emp_total_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_leaves` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emp_total_leaves`
--

INSERT INTO `emp_total_leaves` (`id`, `total_leaves`) VALUES
(1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `emp_undertime`
--

CREATE TABLE IF NOT EXISTS `emp_undertime` (
  `utime_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `date_file` int(11) NOT NULL,
  `date_covered` int(11) NOT NULL,
  `reason` varchar(300) NOT NULL,
  `utime_date` int(11) NOT NULL,
  `utime_from` time NOT NULL,
  `utime_to` time NOT NULL,
  `total_hours` decimal(8,2) NOT NULL,
  `imm_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  `hr_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`utime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `emp_undertime`
--

INSERT INTO `emp_undertime` (`utime_id`, `emp_id`, `date_file`, `date_covered`, `reason`, `utime_date`, `utime_from`, `utime_to`, `total_hours`, `imm_head_appvl`, `hr_appvl`) VALUES
(17, 17, 1340661600, 1340748000, 'tututu', 1340661600, '09:00:00', '11:00:00', 2.00, 'approved', 'approved'),
(18, 17, 1340661600, 1340661600, '', 1340661600, '15:30:00', '18:30:00', 3.00, 'cancelled', 'cancelled'),
(19, 17, 1340661600, 1340661600, 'yujyujy', 1340748000, '21:45:00', '22:50:00', 1.05, 'cancelled', 'cancelled'),
(21, 17, 1340748000, 1340920800, 'Going to bantayan island', 1340920800, '13:00:00', '18:00:00', 5.00, 'pending', 'pending'),
(22, 21, 1340834400, 1340834400, 'sick', 1340834400, '13:00:00', '17:00:00', 4.00, 'approved', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `emp_user`
--

CREATE TABLE IF NOT EXISTS `emp_user` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_added` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `emp_user`
--

INSERT INTO `emp_user` (`emp_id`, `username`, `password`, `status`, `date_added`) VALUES
(1, 'VP Head', '1', 'active', 0),
(2, 'OpsHead', '1', 'active', 0),
(3, 'FandBHead', '1', 'active', 0),
(4, 'HouseKeepingHead', '1', 'active', 0),
(5, 'staff', '1', 'active', 0),
(6, 'ITHead', '1', 'active', 0),
(7, 'Programmer1', '1', 'active', 0),
(10, 'acad_admin1', 'cehj6aMWWul2qccSJ1oAi6k9oqclBKqqDrGUMrGik/L5jF6/VjO9JRa7nHqdk/DIqa35ifVjT/RV0ufdLgmQ+A==', 'active', 0),
(11, 'aidan.alburo', 'WJR1xiMUQdwcrjfS21q1YnLClOXwDZo5LNxi0IUF3vzgnx5ttJjodUVQe6ZFjJ7GQ3LxEAv9ad0VXtpKXEYBww==', 'active', 0),
(12, 'guest', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(13, 'hk.user', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(14, 'shirra.augusto', '4IFv2do657PjDxQDe+q+miyap77rAUc+5C+KsIeWf3IkbTe0WZrzGfWwMR983JjHkZM2SgLPDPIGc8Agxo2DPw==', 'active', 0),
(15, 'super.user.hotel', 'IldVpAvqNH75yb4kbGM/AhVNGholhZj2XgltfHqtk+l8lDDWdwh3clyPiRAmwiD6xeoeGCOZIlJAmGhUxdl6jg==', 'active', 0),
(16, 'student.dummy', 'gUu2CH1U/zUDDJ64h/caqnCELh0DynbeS/xu/H3SAeTETeKHlqpIZUif4e+HHV105UUYERmZ9E7oKYqOSAxZWg==', 'active', 0),
(17, 'hr_admin1', 'QgjRo9X8IKVoNG6Gx9NbfKciwH8T0p9EVL8vdaTyKApVXVN6yC8VQ3GtmjVJ5S9cjNPDCt4i/8GZMSl/c7EEBQ==', 'active', 0),
(18, 'fb_admin1', 'c6qg4YtmAWhopfEkECLCj59c1jG3ppsB2ppS7uOEI/yuLtNwVM3dkZ8aeOGCONNs40Ohyh5nNOfe2MUmBfFHkA==', 'active', 0),
(20, 'fb.user', 'xjTWrbBE8iv5NBOvJ/LFY7N8323M5xKSCkWOUUgmiIRaJeym9zuRlFCmOF0fS2jz7EEZ1iPF49MnOw622TwiUw==', 'active', 0),
(21, 'cg_admin', 'QoOT0G7fdohy5jksV8CNMGkOGJTE8RRACLJTvIwoFd1AlpQLgkrR3n4gEelHpQ2pZmpddIhPZhPzyPYJii48Zg==', 'active', 0),
(23, 'lp.user', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(24, 'lp.onduty', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(25, 'maintenance', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0),
(68, 'jomer', '7t+qAS+iCpByTKQnId1F26a7IjkIIGYme6VmcxQE3FfkHq5Vh8YQpE3SGk/05swVmqy1cTQZFkzqYv2bPRKoVg==', 'active', 1342044000),
(69, 'guest', 'XVExqeGKZcxaF+qR07YJ2pB4UQNks9a6iUYJuSdiVR6XsAJZLqFnDqI45fLcFa7l0oR6SG3+7Y0/Ph4izLl9AQ==', 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `emp_work_experience`
--

CREATE TABLE IF NOT EXISTS `emp_work_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `employer` varchar(65) NOT NULL,
  `job_title` varchar(65) NOT NULL,
  `work_start` int(11) NOT NULL,
  `work_end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `emp_work_experience`
--

INSERT INTO `emp_work_experience` (`id`, `emp_id`, `employer`, `job_title`, `work_start`, `work_end`) VALUES
(1, 11, 'Cleverlearn English Language Institute', 'Teacher', 1323817200, 1324076400),
(2, 11, 'LexMark', 'Software Engineer', 1323990000, 1324076400),
(7, 11, 'dummy', 'dummy', 1329433200, 1330038000),
(10, 14, '', '', 1334322000, 1334322000);

-- --------------------------------------------------------

--
-- Table structure for table `fb_menu`
--

CREATE TABLE IF NOT EXISTS `fb_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name_id` int(11) DEFAULT NULL,
  `meal_type_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `ingredient_id` (`ingredient_id`),
  KEY `added_by` (`added_by`),
  KEY `menu_name_id` (`menu_name_id`),
  KEY `meal_type_id` (`meal_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `fb_menu`
--

INSERT INTO `fb_menu` (`menu_id`, `menu_name_id`, `meal_type_id`, `ingredient_id`, `status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 1, 1, 1, 'active', '0000-00-00 00:00:00', '2012-03-20 04:46:18', 20),
(2, 1, 2, 2, 'active', '0000-00-00 00:00:00', '2012-03-20 04:46:18', 20);

-- --------------------------------------------------------

--
-- Table structure for table `guests_1`
--

CREATE TABLE IF NOT EXISTS `guests_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_agent` varchar(30) DEFAULT NULL,
  `confirmation_no` int(11) DEFAULT NULL,
  `fo_staff` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `country` varchar(30) NOT NULL,
  `civil_status` enum('single','married','widowed') NOT NULL,
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `email_address` varchar(30) DEFAULT NULL,
  `company` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `passport_no` varchar(45) NOT NULL,
  `contact_no` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fo_staff` (`fo_staff`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `guest_accom`
--

CREATE TABLE IF NOT EXISTS `guest_accom` (
  `guest_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(25) DEFAULT NULL,
  `booking_agent` varchar(35) DEFAULT NULL,
  `checkin_date` datetime DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`guest_accom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `guest_accom`
--

INSERT INTO `guest_accom` (`guest_accom_id`, `booking_id`, `booking_agent`, `checkin_date`, `checkout_date`, `comments`) VALUES
(1, '123456789', 'Corporate Rate', '2012-02-07 12:00:00', '2012-02-09 12:00:00', '[removed]alert&#40;''test''&#41;[removed]'),
(2, 'asdf', 'Asia Rooms', '2012-02-14 12:00:00', '2012-02-17 12:00:00', 'test'),
(3, '123', 'Late Rooms', '2012-02-08 00:00:00', '2012-02-16 00:00:00', ''),
(4, '123456789', 'Asia Rooms', '2012-02-08 12:00:00', '2012-02-08 12:00:00', ''),
(5, '123654', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(6, '', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(7, '56', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(8, '', 'Rates Go', '2012-07-17 14:52:00', '2012-07-28 00:00:00', ''),
(9, '12564', 'Bigfoot/IAFT', '2012-07-18 00:00:00', '2012-07-17 00:00:00', ''),
(10, '45', 'Rates Go', '2012-07-18 00:00:00', '2012-07-26 00:00:00', 'Na'),
(11, 'asdf', 'Agoda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(12, 'asdf', 'Agoda', '2012-02-16 23:00:00', '2012-02-13 00:00:00', ''),
(13, 'sdf', 'Booking.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(14, 'w3r', 'Agoda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(15, '22', 'Asia Rooms', '2012-02-24 10:29:00', '2012-02-20 00:00:00', 'sd'),
(16, '123-569', 'Bigfoot/IAFT', '2012-03-18 00:39:00', '0000-00-00 00:00:00', 'ds'),
(17, '123', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'comment rasad ni...'),
(18, 'sdfdsf', 'Walk-in', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'sS'),
(19, '123', 'Booking.com', '2012-02-29 07:25:00', '2012-03-01 12:31:00', ''),
(20, '145', 'Asia Rooms', '2012-02-29 12:00:00', '2012-02-29 12:00:00', 'this is comment test'),
(21, 'dfgdfg', 'Booking.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'comment'),
(22, '123', 'Room Rate', '2012-05-22 09:31:00', '0000-00-00 00:00:00', 'ssdfsdfsdfdfds'),
(23, '123', 'Late Rooms', '2012-03-15 11:29:00', '2012-03-20 19:07:00', 'sdsdfsdfdfd'),
(24, 'sdf', 'Asia Rooms', '2012-03-14 12:00:00', '0000-00-00 00:00:00', 'klkj'),
(25, 'dfds', 'Asia Rooms', '2012-03-13 12:00:00', '0000-00-00 00:00:00', 'f'),
(26, '58974', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(27, '23', 'Agoda', '2012-08-21 00:00:00', '2012-08-20 00:00:00', ''),
(28, '145', 'Agoda', '2012-06-18 15:52:00', '2012-06-14 15:52:00', 'fs'),
(29, 'x', 'Booking.com', '2012-07-25 00:00:00', '2012-07-16 00:00:00', ''),
(30, '122304444', 'Agoda', '2012-08-11 00:00:00', '2012-08-12 00:00:00', 'prepaid, need extra bed, mini bardsddds'),
(31, '36', 'Rates Go', '2012-08-15 00:00:00', '2012-08-21 00:00:00', 'wow! it works.'),
(32, '56', 'Asia Rooms', '2012-08-21 00:00:00', '2012-08-21 00:00:00', ''),
(33, '1456', 'Bigfoot/IAFT', '2012-08-13 00:00:00', '2012-08-30 00:00:00', 'it works aha..');

-- --------------------------------------------------------

--
-- Table structure for table `guest_accommodation_1`
--

CREATE TABLE IF NOT EXISTS `guest_accommodation_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_number` int(11) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `booking_type` varchar(35) NOT NULL,
  `check_in` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check_out` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment` text,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('reserved','checkin','checkout') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_number` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hk_attendant`
--

CREATE TABLE IF NOT EXISTS `hk_attendant` (
  `hk_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  PRIMARY KEY (`hk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `hk_attendant`
--

INSERT INTO `hk_attendant` (`hk_id`, `fname`, `lname`) VALUES
(7, 'Reyjomer', 'Fernandez'),
(8, 'Melvin', 'Evangelista'),
(9, 'Alvin', 'Ado');

-- --------------------------------------------------------

--
-- Table structure for table `hk_category`
--

CREATE TABLE IF NOT EXISTS `hk_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hk_category`
--

INSERT INTO `hk_category` (`cat_id`, `cat_name`) VALUES
(1, 'Mini Bar'),
(2, 'Linens'),
(3, 'Aminities');

-- --------------------------------------------------------

--
-- Table structure for table `hk_inv_amenities`
--

CREATE TABLE IF NOT EXISTS `hk_inv_amenities` (
  `amin_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `beg_bal` int(11) NOT NULL,
  `purchased` int(11) NOT NULL,
  `t_out` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `psy_count` int(11) NOT NULL,
  `remarks` varchar(120) NOT NULL,
  `reconcilation` varchar(120) NOT NULL,
  PRIMARY KEY (`amin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `hk_inv_amenities`
--

INSERT INTO `hk_inv_amenities` (`amin_id`, `inv_id`, `item_id`, `beg_bal`, `purchased`, `t_out`, `balance`, `psy_count`, `remarks`, `reconcilation`) VALUES
(34, 17, 46, 29, 72, 39, 62, 62, 'ok', 'na'),
(36, 17, 36, 20, 72, 0, 92, 52, 'na', 'na'),
(37, 17, 47, 57, 140, 0, 197, 65, 'none', 'none'),
(38, 17, 48, 0, 72, 0, 72, 54, 'none', 'none'),
(39, 17, 48, 5, 4, 1, 8, 3, '3', '2'),
(40, 18, 0, 0, 9, 2, 10, 10, 'none', 'ok'),
(42, 17, 36, 4, 4, 2, 6, 6, 'k', 'k'),
(44, 17, 48, 5, 2, 1, 6, 6, 'none', 'none'),
(46, 19, 47, 55, 12, 4, 63, 63, 'na', 'none'),
(51, 18, 36, 8, 8, 9, 7, 14, 'ok', 'na'),
(59, 19, 46, 8, 8, 8, 8, 8, '8', '8'),
(63, 19, 36, 8, 8, 8, 8, 8, '8', 'ko'),
(65, 19, 48, 4, 4, 4, 4, 4, '4', '4'),
(66, 18, 46, 5, 4, 4, 5, 4, '4', '4'),
(67, 27, 48, 44, 43, 5, 82, 4, 'none', 'none'),
(68, 30, 36, 4, 4, 4, 4, 4, '4', '4'),
(69, 18, 48, 7, 7, 7, 7, 7, 'ok', 'ok'),
(70, 27, 46, 1, 14, 2, 13, 1, 'ok', 'ok'),
(71, 18, 47, 4, 4, 4, 4, 4, '4', '4'),
(73, 27, 47, 9, 9, 2, 16, 16, 'ok', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `hk_inv_linen`
--

CREATE TABLE IF NOT EXISTS `hk_inv_linen` (
  `linen_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `beg_bal` int(11) NOT NULL,
  `purchased` int(11) NOT NULL,
  `stock_rooms` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `pick_up_laundry` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  `damaged` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`linen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `hk_inv_linen`
--

INSERT INTO `hk_inv_linen` (`linen_id`, `inv_id`, `item_id`, `beg_bal`, `purchased`, `stock_rooms`, `rooms`, `pick_up_laundry`, `sub_total`, `damaged`, `total`) VALUES
(3, 33, 37, 4, 4, 4, 4, 4, 16, 0, 0),
(6, 33, 58, 4, 4, 4, 4, 4, 16, 4, 16),
(7, 36, 55, 1, 78, 2, 1, 444, 524, 2, 524),
(8, 32, 57, 4, 4, 4, 4, 4, 18, 2, 18);

-- --------------------------------------------------------

--
-- Table structure for table `hk_inv_minibar`
--

CREATE TABLE IF NOT EXISTS `hk_inv_minibar` (
  `mini_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `beg_bal` int(11) NOT NULL,
  `purchased` int(11) NOT NULL,
  `t_stock` int(11) NOT NULL,
  `out` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `psy_count` int(11) NOT NULL,
  `hotel_room_from` int(11) NOT NULL,
  `hotel_room_to` int(11) NOT NULL,
  `remarks` varchar(120) NOT NULL,
  PRIMARY KEY (`mini_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hk_inv_minibar`
--

INSERT INTO `hk_inv_minibar` (`mini_id`, `inv_id`, `item_id`, `beg_bal`, `purchased`, `t_stock`, `out`, `balance`, `psy_count`, `hotel_room_from`, `hotel_room_to`, `remarks`) VALUES
(12, 31, 51, 8, 8, 14, 2, 14, 14, 104, 107, 'ok'),
(13, 29, 38, 44, 42, 61, 25, 61, 61, 103, 102, 'ok'),
(15, 29, 54, 5, 8, 8, 5, 8, 8, 102, 103, 'ok'),
(16, 31, 50, 25, 23, 46, 2, 46, 25, 102, 112, 'ok');

-- --------------------------------------------------------

--
-- Table structure for table `job_functions`
--

CREATE TABLE IF NOT EXISTS `job_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobtitle_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `jobtitle_id` (`jobtitle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_status`
--

CREATE TABLE IF NOT EXISTS `job_status` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(150) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `job_status`
--

INSERT INTO `job_status` (`job_id`, `status`) VALUES
(1, 'Active'),
(2, 'Resigned'),
(3, 'Terminated');

-- --------------------------------------------------------

--
-- Table structure for table `job_title`
--

CREATE TABLE IF NOT EXISTS `job_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `job_title`
--

INSERT INTO `job_title` (`id`, `name`) VALUES
(1, 'Web Programmer'),
(2, 'HR Head'),
(3, 'Web Designer'),
(4, 'Search Engine Optimizer'),
(5, 'IT Head'),
(6, 'Academics Supervisor'),
(7, 'Teacher'),
(8, 'Maintenance'),
(9, 'CG Head');

-- --------------------------------------------------------

--
-- Table structure for table `lp_dsales`
--

CREATE TABLE IF NOT EXISTS `lp_dsales` (
  `dsales_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(120) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` float(15,2) NOT NULL,
  `total_sales` float(15,2) NOT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`dsales_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=529 ;

--
-- Dumping data for table `lp_dsales`
--

INSERT INTO `lp_dsales` (`dsales_id`, `description`, `quantity`, `price`, `total_sales`, `date_added`) VALUES
(354, 'Happy', 1, 1.00, 1.00, '07-20-2012'),
(356, 'Fit and Right', 1, 28.00, 28.00, '07-20-2012'),
(383, 'Lucky Me', 1, 15.00, 15.00, '07-20-2012'),
(399, 'Nagaray', 1, 2.00, 2.00, '07-20-2012'),
(400, 'Max', 1, 1.00, 1.00, '07-20-2012'),
(408, 'Palesin', 1, 15.00, 15.00, '07-20-2012'),
(439, 'Lucky Me', 1, 15.00, 15.00, '07-23-2012'),
(443, 'Fit and Right', 1, 28.00, 28.00, '07-24-2012'),
(444, 'Lucky Me', 1, 15.00, 15.00, '07-24-2012'),
(445, 'Choey Choco', 1, 1.00, 1.00, '07-24-2012'),
(446, 'Happy', 1, 1.00, 1.00, '07-24-2012'),
(447, 'Safegaurd', 1, 12.00, 12.00, '07-24-2012'),
(448, 'Slice Bread', 1, 20.00, 20.00, '07-24-2012'),
(449, 'Safari', 1, 1.00, 1.00, '07-24-2012'),
(450, 'Yari Ka', 1, 1.00, 1.00, '07-24-2012'),
(451, 'Slice Bread', 1, 20.00, 20.00, '07-24-2012'),
(452, 'Slice Bread', 1, 20.00, 20.00, '07-24-2012'),
(453, 'Safegaurd', 1, 12.00, 12.00, '07-24-2012'),
(454, 'Fit and Right', 1, 28.00, 28.00, '07-27-2012'),
(455, 'Happy', 1, 1.00, 1.00, '07-27-2012'),
(456, 'Max', 1, 1.00, 1.00, '07-27-2012'),
(457, 'Palesin', 1, 15.00, 15.00, '07-27-2012'),
(458, 'Choey Choco', 1, 1.00, 1.00, '07-30-2012'),
(459, 'Fit and Right', 1, 28.00, 28.00, '07-30-2012'),
(460, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(461, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(462, 'Max', 1, 1.00, 1.00, '07-31-2012'),
(463, 'Safegaurd', 1, 12.00, 12.00, '07-31-2012'),
(464, 'Max', 1, 1.00, 1.00, '07-31-2012'),
(465, 'Max', 1, 1.00, 1.00, '07-31-2012'),
(466, 'Safegaurd', 1, 12.00, 12.00, '07-31-2012'),
(467, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(468, 'Fit and Right', 1, 28.00, 28.00, '07-31-2012'),
(469, 'Marlboro', 1, 2.00, 2.00, '07-31-2012'),
(470, 'SanMig Light', 1, 15.00, 15.00, '07-31-2012'),
(472, 'Safegaurd', 1, 12.00, 12.00, '07-31-2012'),
(473, 'SkyFlakes', 1, 5.00, 5.00, '07-31-2012'),
(474, 'SkyFlakes', 1, 5.00, 5.00, '07-31-2012'),
(475, 'SanMig Light', 1, 15.00, 15.00, '07-31-2012'),
(476, 'Yari Ka', 1, 1.00, 1.00, '07-31-2012'),
(477, 'SkyFlakes', 1, 5.00, 5.00, '07-31-2012'),
(478, 'Safari', 1, 1.00, 1.00, '07-31-2012'),
(479, 'Happy', 1, 1.00, 1.00, '08-01-2012'),
(480, 'Lucky Me', 1, 15.00, 15.00, '08-01-2012'),
(481, 'Nagaray', 1, 2.00, 2.00, '08-01-2012'),
(482, 'Safegaurd', 1, 12.00, 12.00, '08-01-2012'),
(483, 'SanMig Light', 1, 15.00, 15.00, '08-01-2012'),
(484, 'SkyFlakes', 1, 5.00, 5.00, '08-01-2012'),
(485, 'Slice Bread', 1, 20.00, 20.00, '08-01-2012'),
(486, 'Choey Choco', 1, 1.00, 1.00, '09-27-2012'),
(488, 'Marlboro', 1, 2.00, 2.00, '09-27-2012'),
(494, 'Choey Choco', 1, 1.00, 1.00, '09-27-2012'),
(495, 'Yari Ka', 1, 1.00, 1.00, '09-27-2012'),
(496, 'SkyFlakes', 1, 5.00, 5.00, '09-27-2012'),
(501, 'SanMig Light', 1, 15.00, 15.00, '09-27-2012'),
(502, 'Choey Choco', 1, 1.00, 1.00, '09-27-2012'),
(503, 'Choey Choco', 1, 1.00, 1.00, '09-27-2012'),
(504, 'Fit and Right', 1, 28.00, 28.00, '09-27-2012'),
(511, 'SanMig Light', 1, 15.00, 15.00, '09-27-2012'),
(512, 'SanMig Light', 1, 15.00, 15.00, '09-27-2012'),
(514, 'Choey Choco', 1, 1.00, 1.00, '09-27-2012'),
(515, 'Fit and Right', 1, 28.00, 28.00, '09-27-2012'),
(516, 'Fit and Right', 1, 28.00, 28.00, '09-27-2012'),
(517, 'SanMig Light', 1, 15.00, 15.00, '09-28-2012'),
(518, 'Choey Choco', 1, 1.00, 1.00, '09-28-2012'),
(519, 'Fit and Right', 1, 28.00, 28.00, '09-28-2012'),
(520, 'SanMig Light', 1, 15.00, 15.00, '09-28-2012'),
(521, 'SanMig Light', 1, 15.00, 15.00, '09-28-2012'),
(522, 'SkyFlakes', 1, 5.00, 5.00, '09-28-2012'),
(523, 'Slice Bread', 1, 20.00, 20.00, '09-28-2012'),
(524, 'Slice Bread', 1, 20.00, 20.00, '09-28-2012'),
(525, 'Max', 1, 1.00, 1.00, '09-28-2012'),
(526, 'Max', 1, 1.00, 1.00, '09-28-2012'),
(527, 'Happy', 1, 1.00, 1.00, '09-28-2012'),
(528, 'Happy', 1, 1.00, 1.00, '09-28-2012');

-- --------------------------------------------------------

--
-- Table structure for table `lp_item_cat`
--

CREATE TABLE IF NOT EXISTS `lp_item_cat` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `lp_item_cat`
--

INSERT INTO `lp_item_cat` (`cat_id`, `name`) VALUES
(1, 'BISCUITS'),
(2, 'PEANUTS'),
(3, 'FROZEN / BREADS'),
(4, 'CHOCOLATES'),
(5, 'JUNKS'),
(6, 'DRINKS'),
(7, 'INS. PANCIT CANTON'),
(8, 'CIGARETTES'),
(9, 'CANDIES'),
(10, 'JUICES IN CAN/ BOTTLE'),
(11, 'OTHERS');

-- --------------------------------------------------------

--
-- Table structure for table `lp_price_list`
--

CREATE TABLE IF NOT EXISTS `lp_price_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(120) NOT NULL,
  `price` float(15,2) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `lp_price_list`
--

INSERT INTO `lp_price_list` (`id`, `description`, `price`, `cat_id`) VALUES
(68, 'Happy', 1.00, 2),
(71, 'Slice Bread', 20.00, 3),
(72, 'SanMig Light', 15.00, 6),
(74, 'Marlboro', 2.00, 8),
(75, 'Lucky Me', 15.00, 7),
(76, 'Choey Choco', 1.00, 4),
(77, 'SkyFlakes', 5.00, 1),
(79, 'Max', 1.00, 9),
(80, 'Fit and Right', 28.00, 10),
(81, 'Yari Ka', 1.00, 5),
(82, 'Safari', 1.00, 5),
(84, 'Palesin', 15.00, 6),
(85, 'Safegaurd', 12.00, 11),
(87, 'Nagaray', 2.00, 2);

-- --------------------------------------------------------

--
-- Table structure for table `lp_pys_inv`
--

CREATE TABLE IF NOT EXISTS `lp_pys_inv` (
  `lp_pys_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `description` varchar(120) NOT NULL,
  `price` float(15,2) NOT NULL,
  `sold_items` int(11) NOT NULL,
  `total_cost` float(15,2) NOT NULL,
  PRIMARY KEY (`lp_pys_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `lp_pys_inv`
--

INSERT INTO `lp_pys_inv` (`lp_pys_id`, `inv_id`, `cat_id`, `description`, `price`, `sold_items`, `total_cost`) VALUES
(13, 0, 2, 'Happy', 1.00, 14, 14.00),
(16, 0, 3, 'Slice Bread', 20.00, 9, 180.00),
(17, 0, 6, 'SanMig Light', 15.00, 6, 90.00),
(19, 0, 8, 'Marlboro', 2.00, 25, 50.00),
(20, 0, 7, 'Lucky Me', 15.00, 10, 150.00),
(21, 0, 4, 'Choey Choco', 1.00, 21, 21.00),
(22, 0, 1, 'SkyFlakes', 5.00, 23, 115.00),
(24, 0, 9, 'Max', 1.00, 4, 4.00),
(25, 0, 10, 'Fit and Right', 28.00, 10, 280.00),
(26, 0, 5, 'Yari Ka', 1.00, 40, 40.00),
(27, 0, 5, 'Safari', 1.00, 13, 13.00),
(29, 0, 6, 'Palesin', 15.00, 23, 345.00),
(30, 0, 11, 'Safegaurd', 12.00, 25, 300.00),
(32, 0, 2, 'Nagaray', 2.00, 0, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `mat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(120) NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `date_added` varchar(50) NOT NULL,
  PRIMARY KEY (`mat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`mat_id`, `name`, `description`, `added_by`, `date_added`) VALUES
(3, 'Tables', 'None', 21, '05/02/2012'),
(4, 'Chairs', 'None', 21, '05/15/2012');

-- --------------------------------------------------------

--
-- Table structure for table `menu_meal_type`
--

CREATE TABLE IF NOT EXISTS `menu_meal_type` (
  `meal_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`meal_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_meal_type`
--

INSERT INTO `menu_meal_type` (`meal_type_id`, `meal_name`) VALUES
(1, 'breakfast'),
(2, 'lunch'),
(3, 'dinner');

-- --------------------------------------------------------

--
-- Table structure for table `menu_name`
--

CREATE TABLE IF NOT EXISTS `menu_name` (
  `menu_name_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_name_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `menu_name`
--

INSERT INTO `menu_name` (`menu_name_id`, `menu_name`, `menu_status`, `date_added`, `last_update`, `added_by`) VALUES
(1, 'Chicken Soup', 'active', '0000-00-00 00:00:00', '2012-03-20 03:31:56', 20);

-- --------------------------------------------------------

--
-- Table structure for table `menu_status_id`
--

CREATE TABLE IF NOT EXISTS `menu_status_id` (
  `ingredient_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_status_name` varchar(65) NOT NULL,
  `menu_status_css` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`ingredient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_status_id`
--

INSERT INTO `menu_status_id` (`ingredient_id`, `menu_status_name`, `menu_status_css`) VALUES
(1, 'Ordered', 'hr-vc'),
(2, 'Reserved', 'hr-oc'),
(3, 'Out of stock', 'hr-wr');

-- --------------------------------------------------------

--
-- Table structure for table `ml_month`
--

CREATE TABLE IF NOT EXISTS `ml_month` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `mod_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`mod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`mod_id`, `name`, `description`) VALUES
(1, 'LCP', 'Live-in Caregiver Program'),
(2, 'PDEC', 'Personality Development and Effective Communication'),
(3, 'BHCP 1', 'Basic Healthcare and Procedures 1'),
(4, 'BHCP 2', 'Basic Healthcare and Procedures 2'),
(5, 'ICC', 'Infant and Child Care'),
(6, 'GC', 'Geriatric Care'),
(7, 'DSN', 'Disabilities and Special Needs'),
(8, 'BEC', 'Basic Emergency Care'),
(9, 'HMN', 'Home Management and Nutrition');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL COMMENT 'emp_id where the notification came from',
  `target` varchar(25) NOT NULL COMMENT 'all or by department or by employee id',
  `msg` text NOT NULL COMMENT 'the notification message',
  `date` int(11) NOT NULL COMMENT 'unix time where the notification was made/created',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_am`
--

CREATE TABLE IF NOT EXISTS `ops_am` (
  `am_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(6) NOT NULL,
  PRIMARY KEY (`am_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `ops_am`
--

INSERT INTO `ops_am` (`am_id`, `name`) VALUES
(1, '1:00'),
(2, '1:30'),
(3, '2:00'),
(4, '2:30'),
(5, '3:00'),
(6, '3:30'),
(7, '4:00'),
(8, '4:30'),
(9, '5:00'),
(10, '5:30'),
(11, '6:00'),
(12, '6:30'),
(13, '7:00'),
(14, '7:30'),
(15, '8:00'),
(16, '8:30'),
(17, '9:00'),
(18, '9:30'),
(19, '10:00'),
(20, '10:30'),
(21, '11:00'),
(22, '11:30'),
(23, '12:00'),
(24, '12:30');

-- --------------------------------------------------------

--
-- Table structure for table `ops_day`
--

CREATE TABLE IF NOT EXISTS `ops_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ops_day`
--

INSERT INTO `ops_day` (`id`, `name`) VALUES
(1, 'Monday'),
(2, 'Tuesday'),
(3, 'Wednesday'),
(4, 'Thursday'),
(5, 'Friday'),
(6, 'Saturday'),
(7, 'Sunday');

-- --------------------------------------------------------

--
-- Table structure for table `ops_delivery_schedule`
--

CREATE TABLE IF NOT EXISTS `ops_delivery_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `date_delivered` date NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ops_delivery_schedule`
--

INSERT INTO `ops_delivery_schedule` (`id`, `item_name`, `supplier`, `date_delivered`, `date_added`) VALUES
(3, 9, 11, '2012-05-30', '2012-05-18'),
(9, 16, 7, '2012-05-01', '2012-05-18'),
(10, 18, 12, '2012-05-16', '2012-05-18'),
(11, 8, 3, '2012-05-08', '2012-05-21'),
(12, 14, 7, '2012-05-11', '2012-05-21');

-- --------------------------------------------------------

--
-- Table structure for table `ops_fb_shortage_wastage`
--

CREATE TABLE IF NOT EXISTS `ops_fb_shortage_wastage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_of_students` int(11) NOT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `day` varchar(5) NOT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `ops_fb_shortage_wastage`
--

INSERT INTO `ops_fb_shortage_wastage` (`id`, `number_of_students`, `from`, `to`, `day`, `date_added`) VALUES
(25, 67, '2012-05-21', '2012-05-24', '', NULL),
(26, 23, '2012-05-28', '2012-05-31', '', NULL),
(29, 5, '2012-05-30', '2012-05-29', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ops_hk_inventory`
--

CREATE TABLE IF NOT EXISTS `ops_hk_inventory` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_from` varchar(20) NOT NULL,
  `inv_to` varchar(20) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `ops_hk_inventory`
--

INSERT INTO `ops_hk_inventory` (`inv_id`, `inv_from`, `inv_to`, `cat_id`) VALUES
(18, '06-01-2012', '06-30-2012', 3),
(27, '06-05-2012', '06-04-2012', 3),
(29, '06-05-2012', '06-11-2012', 1),
(31, '06-13-2012', '06-30-2012', 1),
(32, '06-13-2012', '06-10-2012', 2),
(36, '06-17-2012', '06-03-2012', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ops_hk_items`
--

CREATE TABLE IF NOT EXISTS `ops_hk_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_desc` varchar(120) NOT NULL,
  `price` float(11,2) NOT NULL,
  `item_cat` int(11) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `ops_hk_items`
--

INSERT INTO `ops_hk_items` (`item_id`, `item_desc`, `price`, `item_cat`) VALUES
(36, 'Colgate toothpaste', 12.55, 3),
(37, 'Pilow Case', 10.30, 2),
(38, 'Piattos', 5.00, 1),
(46, 'Cleene Economy Tootbrush', 12.00, 3),
(47, 'Sunsilk shampoo', 12.60, 0),
(48, 'Safeguard soap 25s', 21.26, 1),
(49, 'Nova Small', 6.00, 1),
(50, 'Choco Mucho', 10.00, 1),
(51, 'Cloud 9', 9.00, 1),
(52, 'Gatorade', 13.23, 1),
(53, 'Coke reg', 12.00, 1),
(54, '7 Up', 30.00, 1),
(55, 'Fitted', 0.00, 2),
(56, 'Flat', 0.00, 2),
(57, 'Pillow Case', 0.00, 2),
(58, 'Bed Pad', 0.00, 2),
(59, 'Blanket', 0.00, 2),
(60, 'Blanket', 0.00, 2),
(61, 'Bath Mat', 0.00, 2),
(62, 'Bath Towel', 0.00, 2),
(63, 'Bath Towel New', 0.00, 2),
(64, 'Pillow', 0.00, 2),
(65, 'Window Curtain', 0.00, 2),
(66, 'Shower Curtain', 0.00, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ops_ingredient`
--

CREATE TABLE IF NOT EXISTS `ops_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `day` int(11) NOT NULL,
  `dte` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `ops_ingredient`
--

INSERT INTO `ops_ingredient` (`id`, `name`, `desc`, `day`, `dte`) VALUES
(25, 'beverage powder', 'beverage powder', 6, '2012-05-14'),
(26, 'Food', 'food', 2, '2012-05-24'),
(27, 'soy sauce', 'sauce', 2, '2012-05-24'),
(28, 'Sugar', 'Sugar', 0, '2012-06-20'),
(29, 'Alvin', 'Ado', 1, '2012-07-19');

-- --------------------------------------------------------

--
-- Table structure for table `ops_lp_inv`
--

CREATE TABLE IF NOT EXISTS `ops_lp_inv` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_from` varchar(100) NOT NULL,
  `inv_to` varchar(100) NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_lp_inv`
--

INSERT INTO `ops_lp_inv` (`inv_id`, `inv_from`, `inv_to`) VALUES
(1, 'Jun 18, 2012', 'Jun 30, 2012'),
(2, 'Jul 03, 2012', 'Jul 02, 2012');

-- --------------------------------------------------------

--
-- Table structure for table `ops_main_tools`
--

CREATE TABLE IF NOT EXISTS `ops_main_tools` (
  `tools_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_num` int(11) NOT NULL,
  `description` varchar(120) NOT NULL,
  `quantity` int(11) NOT NULL,
  `uom` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  PRIMARY KEY (`tools_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ops_main_tools`
--

INSERT INTO `ops_main_tools` (`tools_id`, `item_num`, `description`, `quantity`, `uom`, `department`, `date`) VALUES
(8, 156, 'Wires', 4, 1, 2, '07-11-2012'),
(9, 18, 'Table', 18, 1, 7, '07-11-2012'),
(10, 12, 'toolstry', 21, 1, 3, '07-11-2012');

-- --------------------------------------------------------

--
-- Table structure for table `ops_rm_attendant_dchecklist`
--

CREATE TABLE IF NOT EXISTS `ops_rm_attendant_dchecklist` (
  `ra_checklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_attendant_id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `time_in` varchar(30) NOT NULL,
  `time_out` varchar(30) NOT NULL,
  `room_status_id` int(11) NOT NULL,
  `guest_no` int(11) NOT NULL,
  `bt_in` int(11) NOT NULL,
  `bt_out` int(11) NOT NULL,
  `bm_in` int(11) NOT NULL,
  `bm_out` int(11) NOT NULL,
  `fit_in` int(11) NOT NULL,
  `fit_out` int(11) NOT NULL,
  `flat_in` int(11) NOT NULL,
  `flat_out` int(11) NOT NULL,
  `blanket_in` int(11) NOT NULL,
  `blanket_out` int(11) NOT NULL,
  `pilow_in` int(11) NOT NULL,
  `pilow_out` int(11) NOT NULL,
  `pilocs_in` int(11) NOT NULL,
  `pilocs_out` int(11) NOT NULL,
  `toilet_tissue_in` int(11) NOT NULL,
  `toilet_tissue_out` int(11) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `extra_work_done` varchar(100) NOT NULL,
  PRIMARY KEY (`ra_checklist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `ops_rm_attendant_dchecklist`
--

INSERT INTO `ops_rm_attendant_dchecklist` (`ra_checklist_id`, `room_attendant_id`, `room_no`, `time_in`, `time_out`, `room_status_id`, `guest_no`, `bt_in`, `bt_out`, `bm_in`, `bm_out`, `fit_in`, `fit_out`, `flat_in`, `flat_out`, `blanket_in`, `blanket_out`, `pilow_in`, `pilow_out`, `pilocs_in`, `pilocs_out`, `toilet_tissue_in`, `toilet_tissue_out`, `remarks`, `extra_work_done`) VALUES
(2, 48, 13, '10:05 AM', '11:05 AM', 3, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 'broken vase', 'wipe railings'),
(28, 48, 1, '10:00 AM', '10:00 AM', 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 0, 0, 1, 1, 'Broken Glass', 'Swipe Dust'),
(30, 53, 16, '11:00 AM', '11:03 AM', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 'Busted Ako', 'Everything is Done'),
(34, 54, 6, '11:00AM', '12:00AM', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Dark Color', 'Nothing Done'),
(50, 40, 11, '11:00 AM', '11:30 AM', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 'try', 'try'),
(52, 44, 4, '11:05AM', '11:06PM', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'try', 'try'),
(54, 45, 7, '5:00 AM ', '5:00 AM ', 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 'trythis', 'None'),
(55, 42, 10, '10:21 am', '5:00 AM ', 3, 1, 2, 2, 0, 2, 1, 0, 2, 0, 2, 2, 0, 2, 0, 0, 2, 1, 'try', 'wipe railing'),
(56, 42, 16, '10:00 am', '10:00 am', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 0, 0, 2, 2, 'ambot', 'nindot');

-- --------------------------------------------------------

--
-- Table structure for table `ops_rooms_attendant`
--

CREATE TABLE IF NOT EXISTS `ops_rooms_attendant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ra_date` date DEFAULT NULL,
  `am` int(11) NOT NULL,
  `pm` int(11) NOT NULL,
  `prepared_by` int(11) NOT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `ops_rooms_attendant`
--

INSERT INTO `ops_rooms_attendant` (`id`, `ra_date`, `am`, `pm`, `prepared_by`, `date_added`) VALUES
(40, '2012-05-23', 9, 7, 2, '2012-05-25'),
(41, '2012-05-01', 3, 4, 1, '2012-05-31'),
(42, '2012-05-02', 3, 5, 7, '2012-05-31'),
(44, '2012-05-09', 4, 3, 5, '2012-05-31'),
(45, '2012-05-14', 9, 7, 8, '2012-05-31'),
(48, '2012-07-09', 11, 1, 9, '2012-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `ops_utensils`
--

CREATE TABLE IF NOT EXISTS `ops_utensils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(50) NOT NULL,
  `u_desc` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `added_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `ops_utensils`
--

INSERT INTO `ops_utensils` (`id`, `u_name`, `u_desc`, `quantity`, `added_date`) VALUES
(30, 'Pork', 'Porkie', 10, '2012-05-18');

-- --------------------------------------------------------

--
-- Table structure for table `ops_uten_status`
--

CREATE TABLE IF NOT EXISTS `ops_uten_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utensils_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ops_uten_status`
--

INSERT INTO `ops_uten_status` (`id`, `utensils_id`) VALUES
(1, 0),
(2, 0),
(3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payrolls`
--

CREATE TABLE IF NOT EXISTS `payrolls` (
  `payroll_id` int(11) NOT NULL AUTO_INCREMENT,
  `period_from` int(11) NOT NULL,
  `period_to` int(11) NOT NULL,
  `pay_date` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stat` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`payroll_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `payrolls`
--

INSERT INTO `payrolls` (`payroll_id`, `period_from`, `period_to`, `pay_date`, `date_added`, `stat`) VALUES
(13, 1337119200, 1338415200, 1338847200, '2012-06-05 10:17:43', 'active'),
(14, 1339711200, 1341007200, 1341007200, '2012-06-19 10:57:22', 'active'),
(15, 1342303200, 1343599200, 1343599200, '2012-06-21 03:26:49', 'active'),
(16, 1343253600, 1344549600, 1344981600, '2012-07-30 03:42:25', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_details`
--

CREATE TABLE IF NOT EXISTS `payroll_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `daily_rate` decimal(8,2) NOT NULL,
  `basic_pay` decimal(8,2) NOT NULL,
  `absences` decimal(8,2) NOT NULL,
  `late` decimal(8,2) NOT NULL,
  `reg_ot` decimal(8,2) NOT NULL,
  `reg_hol` decimal(8,2) NOT NULL,
  `sp_hol` decimal(8,2) NOT NULL,
  `leave` decimal(8,2) NOT NULL,
  `night_diff` decimal(8,2) NOT NULL,
  `allowance` decimal(8,2) NOT NULL,
  `adjustment` decimal(8,2) NOT NULL,
  `adj_non_tax` decimal(8,2) NOT NULL,
  `thirteenth_month` decimal(8,2) NOT NULL,
  `gross_income` decimal(8,2) NOT NULL,
  `sss` decimal(8,2) NOT NULL,
  `phic` decimal(8,2) NOT NULL,
  `hdmf` decimal(8,2) NOT NULL,
  `wtax` decimal(8,2) NOT NULL,
  `sss_loan` decimal(8,2) NOT NULL,
  `pibig_sloan` decimal(8,2) NOT NULL,
  `pibig_hloan` decimal(8,2) NOT NULL,
  `phone_bill` decimal(8,2) NOT NULL,
  `advances` decimal(8,2) NOT NULL,
  `uniform` decimal(8,2) NOT NULL,
  `others` decimal(8,2) NOT NULL,
  `canteen` decimal(8,2) NOT NULL,
  `total_deductions` decimal(8,2) NOT NULL,
  `total_net_pay` decimal(8,2) NOT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `payroll_details`
--

INSERT INTO `payroll_details` (`id`, `payroll_id`, `emp_id`, `daily_rate`, `basic_pay`, `absences`, `late`, `reg_ot`, `reg_hol`, `sp_hol`, `leave`, `night_diff`, `allowance`, `adjustment`, `adj_non_tax`, `thirteenth_month`, `gross_income`, `sss`, `phic`, `hdmf`, `wtax`, `sss_loan`, `pibig_sloan`, `pibig_hloan`, `phone_bill`, `advances`, `uniform`, `others`, `canteen`, `total_deductions`, `total_net_pay`, `date_added`) VALUES
(1, 14, 12, 305.75, 4650.00, 3057.53, 50.00, 0.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 1582.47, 316.70, 112.50, 100.00, 224.59, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 793.79, 788.68, 1341784800),
(2, 14, 11, 305.75, 4650.00, 5.00, 50.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 4640.00, 316.70, 112.50, 100.00, 225.34, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 794.54, 3845.46, 1341784800),
(3, 15, 11, 361.64, 5500.00, 11.00, 70.00, 10.00, 5.00, 2.00, 3.00, 1.00, 4.00, 5.00, 2.00, 40.00, 5491.00, 366.70, 137.50, 100.00, 337.13, 0.00, 0.00, 0.00, 5.00, 6.00, 8.00, 9.00, 4.00, 973.33, 4517.67, 1341784800);

-- --------------------------------------------------------

--
-- Table structure for table `pay_adjustments`
--

CREATE TABLE IF NOT EXISTS `pay_adjustments` (
  `adjustment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`adjustment_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pay_allowances`
--

CREATE TABLE IF NOT EXISTS `pay_allowances` (
  `allowance_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `task` varchar(200) NOT NULL,
  `personal` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`allowance_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pay_canteen`
--

CREATE TABLE IF NOT EXISTS `pay_canteen` (
  `pay_canteen_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pay_canteen_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pay_holiday`
--

CREATE TABLE IF NOT EXISTS `pay_holiday` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `type` enum('special','legal') NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`holiday_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pay_night_diff`
--

CREATE TABLE IF NOT EXISTS `pay_night_diff` (
  `nightdiff_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`nightdiff_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pay_uniform`
--

CREATE TABLE IF NOT EXISTS `pay_uniform` (
  `pay_uniform_id` int(11) NOT NULL AUTO_INCREMENT,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pay_uniform_id`),
  KEY `payroll_id` (`payroll_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prs`
--

CREATE TABLE IF NOT EXISTS `prs` (
  `prs_id` int(11) NOT NULL AUTO_INCREMENT,
  `requested_by` int(11) NOT NULL COMMENT 'emp_id of the employee',
  `date_requested` int(11) NOT NULL DEFAULT '0',
  `date_required` int(11) NOT NULL DEFAULT '0',
  `supplier_name` varchar(50) NOT NULL,
  `type` enum('stock','asset') NOT NULL DEFAULT 'stock',
  `payment_type` enum('cash','check','credit card','others') NOT NULL DEFAULT 'others' COMMENT 'cc = credit card, c = cash, chq = cheque',
  `dept_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending' COMMENT 'department head approval, 0 means not yet approved',
  `fin_head_appvl` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending' COMMENT 'finance head approval, 0 means not yet approved',
  `date_received` int(11) NOT NULL DEFAULT '0' COMMENT 'date the item/items recieved in timestamp, 0 means the item is not yet recieved. If this has a value other than zero means the item/items were recieved and update the stock table if it is stock and the assets table if it is an asset of the employee',
  PRIMARY KEY (`prs_id`),
  KEY `requested_by` (`requested_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `prs`
--

INSERT INTO `prs` (`prs_id`, `requested_by`, `date_requested`, `date_required`, `supplier_name`, `type`, `payment_type`, `dept_head_appvl`, `fin_head_appvl`, `date_received`) VALUES
(5, 11, 1323834604, 1323730800, 'la nueva', 'stock', 'cash', 'approved', 'rejected', 0),
(6, 11, 1323834838, 1323730800, 'la nueva', 'stock', 'cash', 'cancelled', 'approved', 0),
(7, 11, 1323840386, 1323817200, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(8, 13, 1324541726, 1324508400, 'la nueva', 'stock', 'cash', 'cancelled', 'cancelled', 0),
(9, 12, 1325740667, 1325718000, 'la nueva', 'stock', 'check', 'cancelled', 'cancelled', 0),
(10, 12, 1325740816, 1326150000, 'macro', 'stock', 'credit card', 'cancelled', 'cancelled', 0),
(11, 12, 1325741535, 1326236400, 'macro', 'stock', 'cash', 'approved', 'approved', 0),
(12, 13, 1330398286, 1330470000, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(13, 20, 1331513989, 1331679600, 'la nueva', 'stock', 'cash', 'pending', 'pending', 0),
(14, 20, 1331703553, 1331593200, 'la nueva', 'stock', 'cash', 'pending', 'pending', 0),
(15, 20, 1331719031, 1332198000, 'macro', 'stock', 'cash', 'pending', 'pending', 0),
(16, 20, 1331719031, 1332198000, 'macro', 'stock', 'cash', 'cancelled', 'cancelled', 0),
(17, 10, 1333086484, 1330902000, 'la nueva', 'asset', 'check', 'approved', 'approved', 0),
(18, 20, 1333093888, 1331074800, 'la nueva', 'asset', 'check', 'cancelled', 'cancelled', 0),
(19, 12, 1333343774, 1333404000, 'macro', 'stock', 'others', 'approved', 'pending', 0),
(20, 20, 1334021143, 1334095200, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(21, 20, 1335855000, 1335909600, 'la nueva', 'stock', 'check', 'pending', 'pending', 0),
(22, 13, 1339460924, 1338760800, 'macro', 'stock', 'credit card', 'cancelled', 'cancelled', 0),
(23, 23, 1339666706, 1338933600, 'la nueva', 'asset', 'cash', 'approved', 'approved', 0),
(24, 23, 1339666706, 1338933600, 'la nueva', 'asset', 'cash', 'pending', 'pending', 0),
(25, 23, 1340159803, 1340056800, 'la nueva', 'stock', 'cash', 'approved', 'approved', 0),
(26, 23, 1340161042, 1338847200, 'nutech marketing', 'stock', 'cash', 'approved', 'approved', 0),
(27, 17, 1340268581, 1340229600, 'macro', 'stock', 'credit card', 'cancelled', 'cancelled', 0),
(28, 17, 1340335751, 1339538400, 'nutech marketing', 'stock', 'check', 'cancelled', 'cancelled', 0),
(29, 17, 1340335751, 1339538400, 'nutech marketing', 'stock', 'check', 'pending', 'pending', 0),
(30, 23, 1340755427, 1341007200, 'macro', 'stock', 'cash', 'pending', 'pending', 0),
(31, 25, 1341565326, 1341871200, 'la nueva', 'stock', 'cash', 'pending', 'pending', 0),
(32, 25, 1341799005, 1341957600, 'macro', 'asset', 'check', 'pending', 'pending', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prs_details`
--

CREATE TABLE IF NOT EXISTS `prs_details` (
  `prs_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `prs_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` float(11,2) NOT NULL DEFAULT '0.00',
  `units` varchar(50) NOT NULL COMMENT 'unit of measure',
  `unit_price` float(15,2) NOT NULL DEFAULT '0.00',
  `amount` float(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`prs_details_id`),
  KEY `prs_id` (`prs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `prs_details`
--

INSERT INTO `prs_details` (`prs_details_id`, `prs_id`, `description`, `quantity`, `units`, `unit_price`, `amount`) VALUES
(8, 12, 'Manok Palabok', 10.00, 'liters', 10.00, 100.00),
(9, 13, 'Chair', 1.00, 'each', 2.50, 2.50),
(10, 14, 'friend chicken', 5.00, 'each', 100.00, 500.00),
(11, 15, 'Enter the product namfdase or description', 13.00, 'liters', 30.00, 390.00),
(12, 16, 'sdfEnter the product name or description', 13.00, 'kilo', 30.00, 390.00),
(13, 17, 'try', 1.00, 'length', 20.00, 20.00),
(14, 18, 'Item1', 2.00, 'each', 20.00, 40.00),
(15, 19, 'Black Wire', 5.00, 'each', 170.52, 852.60),
(16, 20, 'Meat', 2.00, 'kilo', 200.00, 400.00),
(18, 21, 'Lechon Manok', 1.00, 'each', 150.00, 150.00),
(19, 22, 'Fitted', 5.00, 'each', 28.00, 140.00),
(20, 23, 'Luto', 5.00, 'meters', 40.00, 200.02),
(21, 24, 'Grails Meat', 8.00, 'bottle', 700.00, 5600.00),
(22, 25, 'Kitkat Chocolate', 1.00, 'bottle', 50.00, 50.00),
(23, 26, 'Marlboro Menthol', 3.00, 'pack', 300.00, 900.00),
(24, 27, 'try', 1.00, 'liters', 22.00, 22.00),
(25, 28, 'ASD', 1.00, 'length', 22.00, 22.00),
(26, 29, 'SDF', 2.00, 'liters', 2.00, 4.00),
(27, 30, 'Choey Choco', 1.00, 'pack', 40.00, 40.00),
(28, 31, 'This is for kitchen', 1.00, 'each', 25.00, 25.00),
(29, 32, 'Wires', 1.00, 'kilo', 40.00, 40.00);

-- --------------------------------------------------------

--
-- Table structure for table `real_category`
--

CREATE TABLE IF NOT EXISTS `real_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `real_category`
--

INSERT INTO `real_category` (`cat_id`, `name`) VALUES
(1, 'Private'),
(2, 'Company');

-- --------------------------------------------------------

--
-- Table structure for table `real_hold`
--

CREATE TABLE IF NOT EXISTS `real_hold` (
  `real_hold_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY (`real_hold_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `real_hold`
--

INSERT INTO `real_hold` (`real_hold_id`, `name`) VALUES
(1, 'For sale'),
(2, 'For rent'),
(3, 'Looking for');

-- --------------------------------------------------------

--
-- Table structure for table `real_location`
--

CREATE TABLE IF NOT EXISTS `real_location` (
  `loc_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`loc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `real_location`
--

INSERT INTO `real_location` (`loc_id`, `name`) VALUES
(1, 'Adlawan'),
(2, 'Cabugao'),
(3, 'Cagay'),
(4, 'Olotayan'),
(5, 'Punta Cogon');

-- --------------------------------------------------------

--
-- Table structure for table `real_property`
--

CREATE TABLE IF NOT EXISTS `real_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `real_property`
--

INSERT INTO `real_property` (`property_id`, `name`) VALUES
(1, 'Residential'),
(2, 'Commercial'),
(3, 'Industrial'),
(4, 'Agriculture'),
(5, 'Special purpose');

-- --------------------------------------------------------

--
-- Table structure for table `real_state_info`
--

CREATE TABLE IF NOT EXISTS `real_state_info` (
  `real_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `real_hold_id` int(11) NOT NULL,
  `loc_id` int(11) NOT NULL,
  `property` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(15,2) NOT NULL,
  `pic1` longblob NOT NULL,
  `seller_type` int(11) NOT NULL,
  `bedrooms` int(11) NOT NULL,
  `tell_no` int(11) NOT NULL,
  `phone_no` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  PRIMARY KEY (`real_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `real_state_info`
--

INSERT INTO `real_state_info` (`real_id`, `cat_id`, `real_hold_id`, `loc_id`, `property`, `date`, `time`, `title`, `description`, `price`, `pic1`, `seller_type`, `bedrooms`, `tell_no`, `phone_no`, `added_by`) VALUES
(1, 1, 1, 1, 3, 'Jan 24, 1987', '10:40 pm', 'Cherry House ', 'he City of Roxas is politi Cherry House Cherry House Cherry House Cherry House\r\nhe City of Roxas is politi Cherry House Cherry House Cherry House Cherry House\r\nhe City of Roxas is politi Cherry House Cherry House Cherry House Cherry House\r\nhe City of Roxas is politi Cherry House Cherry House Cherry House Cherry Househe City of Roxas is politi Cherry House Cherry House Cherry House Cherry House', 40000.00, 0xffd8ffe000104a46494600010101004800480000fffe003b43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d2039350affe20bf84943435f50524f46494c4500010100000be800000000020000006d6e74725247422058595a2007d90003001b00150024001f61637370000000000000000000000000000000000000000100000000000000000000f6d6000100000000d32d0000000029f83ddeaff255ae7842fae4ca83390d00000000000000000000000000000000000000000000000000000000000000106465736300000144000000796258595a000001c00000001462545243000001d40000080c646d6464000009e0000000886758595a00000a680000001467545243000001d40000080c6c756d6900000a7c000000146d65617300000a9000000024626b707400000ab4000000147258595a00000ac80000001472545243000001d40000080c7465636800000adc0000000c7675656400000ae8000000877774707400000b70000000146370727400000b84000000376368616400000bbc0000002c64657363000000000000001f735247422049454336313936362d322d3120626c61636b207363616c65640000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000058595a2000000000000024a000000f840000b6cf63757276000000000000040000000005000a000f00140019001e00230028002d00320037003b00400045004a004f00540059005e00630068006d00720077007c00810086008b00900095009a009f00a400a900ae00b200b700bc00c100c600cb00d000d500db00e000e500eb00f000f600fb01010107010d01130119011f0125012b01320138013e0145014c0152015901600167016e0175017c0183018b0192019a01a101a901b101b901c101c901d101d901e101e901f201fa0203020c0214021d0226022f02380241024b0254025d02670271027a0284028e029802a202ac02b602c102cb02d502e002eb02f50300030b03160321032d03380343034f035a03660372037e038a039603a203ae03ba03c703d303e003ec03f9040604130420042d043b0448045504630471047e048c049a04a804b604c404d304e104f004fe050d051c052b053a05490558056705770586059605a605b505c505d505e505f6060606160627063706480659066a067b068c069d06af06c006d106e306f507070719072b073d074f076107740786079907ac07bf07d207e507f8080b081f08320846085a086e0882089608aa08be08d208e708fb09100925093a094f09640979098f09a409ba09cf09e509fb0a110a270a3d0a540a6a0a810a980aae0ac50adc0af30b0b0b220b390b510b690b800b980bb00bc80be10bf90c120c2a0c430c5c0c750c8e0ca70cc00cd90cf30d0d0d260d400d5a0d740d8e0da90dc30dde0df80e130e2e0e490e640e7f0e9b0eb60ed20eee0f090f250f410f5e0f7a0f960fb30fcf0fec1009102610431061107e109b10b910d710f511131131114f116d118c11aa11c911e81207122612451264128412a312c312e31303132313431363138313a413c513e5140614271449146a148b14ad14ce14f01512153415561578159b15bd15e0160316261649166c168f16b216d616fa171d17411765178917ae17d217f7181b18401865188a18af18d518fa19201945196b199119b719dd1a041a2a1a511a771a9e1ac51aec1b141b3b1b631b8a1bb21bda1c021c2a1c521c7b1ca31ccc1cf51d1e1d471d701d991dc31dec1e161e401e6a1e941ebe1ee91f131f3e1f691f941fbf1fea20152041206c209820c420f0211c2148217521a121ce21fb22272255228222af22dd230a23382366239423c223f0241f244d247c24ab24da250925382568259725c725f726272657268726b726e827182749277a27ab27dc280d283f287128a228d429062938296b299d29d02a022a352a682a9b2acf2b022b362b692b9d2bd12c052c392c6e2ca22cd72d0c2d412d762dab2de12e162e4c2e822eb72eee2f242f5a2f912fc72ffe3035306c30a430db3112314a318231ba31f2322a3263329b32d4330d3346337f33b833f1342b3465349e34d83513354d358735c235fd3637367236ae36e937243760379c37d738143850388c38c839053942397f39bc39f93a363a743ab23aef3b2d3b6b3baa3be83c273c653ca43ce33d223d613da13de03e203e603ea03ee03f213f613fa23fe24023406440a640e74129416a41ac41ee4230427242b542f7433a437d43c044034447448a44ce45124555459a45de4622466746ab46f04735477b47c04805484b489148d7491d496349a949f04a374a7d4ac44b0c4b534b9a4be24c2a4c724cba4d024d4a4d934ddc4e254e6e4eb74f004f494f934fdd5027507150bb51065150519b51e65231527c52c75313535f53aa53f65442548f54db5528557555c2560f565c56a956f75744579257e0582f587d58cb591a596959b85a075a565aa65af55b455b955be55c355c865cd65d275d785dc95e1a5e6c5ebd5f0f5f615fb36005605760aa60fc614f61a261f56249629c62f06343639763eb6440649464e9653d659265e7663d669266e8673d679367e9683f689668ec6943699a69f16a486a9f6af76b4f6ba76bff6c576caf6d086d606db96e126e6b6ec46f1e6f786fd1702b708670e0713a719571f0724b72a67301735d73b87414747074cc7528758575e1763e769b76f8775677b37811786e78cc792a798979e77a467aa57b047b637bc27c217c817ce17d417da17e017e627ec27f237f847fe5804780a8810a816b81cd8230829282f4835783ba841d848084e3854785ab860e867286d7873b879f8804886988ce8933899989fe8a648aca8b308b968bfc8c638cca8d318d988dff8e668ece8f368f9e9006906e90d6913f91a89211927a92e3934d93b69420948a94f4955f95c99634969f970a977597e0984c98b89924999099fc9a689ad59b429baf9c1c9c899cf79d649dd29e409eae9f1d9f8b9ffaa069a0d8a147a1b6a226a296a306a376a3e6a456a4c7a538a5a9a61aa68ba6fda76ea7e0a852a8c4a937a9a9aa1caa8fab02ab75abe9ac5cacd0ad44adb8ae2daea1af16af8bb000b075b0eab160b1d6b24bb2c2b338b3aeb425b49cb513b58ab601b679b6f0b768b7e0b859b8d1b94ab9c2ba3bbab5bb2ebba7bc21bc9bbd15bd8fbe0abe84beffbf7abff5c070c0ecc167c1e3c25fc2dbc358c3d4c451c4cec54bc5c8c646c6c3c741c7bfc83dc8bcc93ac9b9ca38cab7cb36cbb6cc35ccb5cd35cdb5ce36ceb6cf37cfb8d039d0bad13cd1bed23fd2c1d344d3c6d449d4cbd54ed5d1d655d6d8d75cd7e0d864d8e8d96cd9f1da76dafbdb80dc05dc8add10dd96de1cdea2df29dfafe036e0bde144e1cce253e2dbe363e3ebe473e4fce584e60de696e71fe7a9e832e8bce946e9d0ea5beae5eb70ebfbec86ed11ed9cee28eeb4ef40efccf058f0e5f172f1fff28cf319f3a7f434f4c2f550f5def66df6fbf78af819f8a8f938f9c7fa57fae7fb77fc07fc98fd29fdbafe4bfedcff6dffff64657363000000000000002e4945432036313936362d322d312044656661756c742052474220436f6c6f7572205370616365202d20735247420000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000058595a2000000000000062990000b785000018da58595a20000000000000000000500000000000006d656173000000000000000100000000000000000000000000000000000000000000000258595a20000000000000031600000333000002a458595a200000000000006fa2000038f50000039073696720000000004352542064657363000000000000002d5265666572656e63652056696577696e6720436f6e646974696f6e20696e204945432036313936362d322d31000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000058595a20000000000000f6d6000100000000d32d7465787400000000436f7079726967687420496e7465726e6174696f6e616c20436f6c6f7220436f6e736f727469756d2c20323030390000736633320000000000010c44000005dffffff326000007940000fd8ffffffba1fffffda2000003db0000c075ffdb0043000503040404030504040405050506070c08070707070f0b0b090c110f1212110f111113161c1713141a1511111821181a1d1d1f1f1f13172224221e241c1e1f1effdb0043010505050706070e08080e1e1411141e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1effc000110800de013003012200021101031101ffc4001d000001050101010100000000000000000003020405060701000809ffc4003d1000020103030204050203050803010000010203000411051221314106132251073261718114912342a10815b1c1f033435262728292d1162453e1ffc400190100030101010000000000000000000000020304010005ffc40026110002020202020203010003000000000000010211032112310441225113143261527181ffda000c03010002110311003f00c07cd953a8271ed4a4b9473c8e7eb5cfd4606d78f3f5aea342c3a72474a9346dab08d140e73900f7c524daf747c2d124b78caa9590e7d8520472af0ae71db342d3ec313fc78db01723bd17cfc2e0ae0639aec72caab875e7de96ad1b83bb8e3dabbfece4b60cbc4d8c7008e696b1ae331bd2dada0651b5b0715e8ad640a4ab9c7d2b291cc47f114f4cfd6970b079155c5270e83907af535d575072463deb6951c49186d51158e33f5a4bf95361531f8a6777393b4212571dabb670cf3c8042ae7ed5b1548149fb1535acd08dc3d4b436b85c28618f7ab74b6623b28a29b22465e41a86d474a755cc6a5d48ed5d5672688691a165f4512de20ceacbc9c525ec4c6cb90df6c54ce8762643bc03b57deb274a21455b25b4cb64b7b30c7e76e718a70d72a4ec23b51194451e08e31cd0ca5bc98edc54b6bd8d6a9060903ed3d0e29e416926034726299a5a823d0d52b6504e91a91c8cf4a0663439b49658582b8c8c54cdb5b4572464609a1d82c723289542fbe6ac50d841e48689d7a75cd25cbe8ea1506976f0e9cf3b3ec58c6e27dab3ad7b5196feeca891bc9538504d4af8c7539771b18ae19a3fe600d55e364009ee477aec77db064cebb855201e050895241cf6a54a149047b501873c5313b62f4119c7df149debbba5239033448109c7d6b6a982c7312311bf1902932cb7123ed45e7a0a74ac6140817345088abbc9c135b746714b62605f241321f5e39143bfb9f4e51b8c7340b99c61b9cb638ae5bdbee8c3ca4313d8d076f665e86f6ffc698bb2fd8d2eeeea2821f5bf2074a55d4b0dbc6cd90b81d0555352bb6b99b21b8ed4f8ae5a396d84d4752798ec56da3d8d0ec23c0dc573438605e0bb7aa8d2c8234215b3c6689eb41d6c35c5caa0c03cfb547cb3076c93c52277f3064b76a03eec020e68a2924735b187ea0332823d38e68cde4b0c0f48a6c180c657b5113cb66e78e29ef46a4ac5f9407caf45432c4339dc3da84623feecfdfed44cca0707728ed59274834c2a5c05f993ad157f4ce3ae0d012718f5a7dc5293c873c1c0a1766d87308c611bad7825c447086b8b110c363669c5b33a13bf0467bd653b3ad5898da47c2bc79352d63a21b91ba40178c906911cd004dfe9e052e3d60a2e17af6ada6c193de833e871aca373288c715276b269fa6a663dbb80e4d57db50b99d8e73cfb5712d6e666ce4b0fad6aa464632f63c9afa5bebb2c18819e315226defbc8fe1a96e38cd2747d35a3911dd72055d6c36345e988600ee2b1cb60c95333abc5ba00adc5b0071d48a97d0d6386d7e52c719c7b55b2e6cede6e648463af0299c9a6471e5edf1f63433a92d058e6ba21f2376e90707a0a3c56d6b3fcbe9fad125995731cb0807be053bb5b7b39c2956d871c0a95c5a7b1ea982874d624794dbbd854adac73da954963c83ed5db3b19639018e504fb54edb5c089963b98ff0038a53951b1561b4f7b429868d4311d4d4578befe1b38bf4f6929f35bae0f6a7fe23bfb2b2b3dc8aa6465c285e08fad67d70d24d3798c493df26831c793b064e91d3820939c9ee69ac89b58e2892c983b68648ce4d3a843ec413fc361db149001e3345c07e98fcd71d4271c7e2b523842819c13458b28d903ed5c45dca79ed4eec2db3eb63c015a63a14877b07938ae5c4d0e19371ce38c51ae4a2c6581ce3ad44ab79927a23c64d746ac5bb0a917a77303f4268b7338b5b6dee71c52ee808d15a47e40ce2abdaeea42e710c438f7ae572d1d1891da8debdc48c73e9278a1416f210180ed4e2ded0280cdedcd2e7b858c6c8ce7eb4d93e3a43a2a86b310a85779ddec29ac8e78e0f4ea6bb2162c4d02563b4d317c8d695e8583bb814648b6a9627b50a2cede4f6a749cc44668582d902a0a8cb0e3ed5d0c3380b44dd22fa187e6bdbbd4018cf1d48aaad05da1406064b7e294259178535c7c30f4b1c7b1ae2a120e1bb505db37fc16b2e7865fcd155222bed410cc304f23bd75d942e14706b68ea08ac55b2ac71f4a224859fe639fad37018e303b549e9d66d2c9c0cfbd65d6cd4840573c1273f4a90d3f4bb8bb9144503303f4ce6ac3a0785e6beb855543b49e715b57c3ef07a5b491b476a64523d7915366f2a38d1462c3c9ecc834ef026a92c6cc903fb8014d4e59f80f568b69781d78e72b5f53e8da05a242a0400703820715369a5da2a85f2c63dabce8f99393d20e6b1c7b3e449fc21ad32909149b71c610d319748d6ec97702f9f60735f65ff75d9e3fd847fb536bbf0ee99768526b64753d88a7af2727d0be5899f18cb75ad424b4c9263be45261d6a4046f8c8fa1afaa35ff00863a45ec4dfa70216ec36e45645e39f849a8d96fb9b588ba1e72b8ff000a6c3c8dfc902f1465fcb28867b6b9b72cc8158fbd4d697e1fb7bcb3f3a3942b01daa97a8d9ded9ccd14a1d194e31560f87ba8349aa4761737062f30e1493de9d99728f215862e2f64b41697b693ed55f3314f2eb534b6b72f7500565ec6ad50e8b247be759936af258fb7bd66fe36d5d350bd10c2a162b7ca023f98e7ad40b2727c687cba21f52bb9b50bb33c99ebc0c7414de4385c0eb442022e73da9abb166e58e3e94d5f1744ced883c8cbf5ae1407bf4a536181c93c7bd24ab12083db9a6f6cc14b8c71c9a14cd80703ef4477f2d4edee2bd670bcedb98e050efb3685d8c0d212edc2e29ecd32a3ac71f3c5127912180468b938eb4d205712991ce1bb0a1e56048518d9919a53d3b50a79edad610cc57f15ebfba48109909ce3b555352b9334f8424e7deba09ca46531c6a3aa4972e510fa73c50ad600a3749f375af5ac0238f7c98dc7de857171b7728c74ed54dfa43146825ddcafc8a6a3643839ae4ac49193493eae335c95bd9b68448cc4e3349c0ee735c91761f5b73da924b0edcfbd35c5d684f3dd0b048efc51e37e3ad3659067069c46db6b25676fd9c68a303d4287e5231c01f7a517071c76a2c00118344c68992d51b1b4718e6846d56352ead851d6a40200bd78a8cd56618d91b678e6855d9bdb19c8e0bf1d3de9607b1a1229c023a53a8319c6334ef46fb1c5a47b9d462af9e0df0fcd793a94562b9ed555d36d5d65460bc6735bd7c17b7805d2473c6a59f918f7a9b34dc62371c6e45dbe1a78560b6b5433db7abdc8ad4f4fb3b784010c4abc63814d6cad9511428eded52f6498c578326e72d96e56a31d0ea04da3a51941cd7075a22d5be3e34d9e6ca4df6294734a00578576bdec78a2a227625b0075c521d14af3839a2d7a8a58a3254d1db5b33bf889f0d345f135abca912dadf11e89a318dc7eb5f2f6bba1dee81e257d3efa37827b7931ff0052f66afb91ba566bf1afc130f887483aadb459d4ac9372151cc8bdd7fcff00152e5c5c152e87c3237a661fe2cd624b78ce9f657b23c122f386eded54c955082d9c31f7a97d5adf6bfca41c60e7b7d2a22e1087c541c141e86301360a601ed4d957031f5a3cb93c0ae4790d8acdd8b7a07b38af12a80fd28b20241e7b535914c922a13d68a9b40bec5dbc4f72fcfcb4f997c94daa78e95d8bc9861da0e18537c1965de5b2bda8d7d1d7a0f147b949979f6a15d4e902619f9c71459e6558890dd066aa1a9dd4b71725549233da821153968142352ba9aeee8aef247b8a25bdb244325893dc9af43108532c4927b9a05c5c100a0aa1fd218925b3b7936e3b54fa453099c0049e78a5c870013d71c5088dca4bf6a38a324c182cee096e31d28920c0eb5c7e00c771416dc0e73c51f164f276c2ce03460e680e38033da9c43ea1c722bcd1ede718a252ad1aaac1795f2f3da8aa981c8cd78114a5209f49f4d0cd37b353b2385c2e385c8f7a3c72e76b06c71d298aaae3247145553d40c5171451a1ecf7788480f826a294b124fd7ad3999484e6830807af4a28a540fbd078132c0919a9eb0d2cdc152a0807a63dea3b4f843b0c0f4d68de13d344d6e63db9c918a5e49507156f60f42d0e52555d1f19adb7e19f852f124b7b91948958364d32f00786cdd5c246620594f208ed5b769364b69671c08a1428c600af273666dd15c62a283dac5ce00edfb54844368c53604478e78a6175a9ddbbb456501dc0f523bd4d08bb0325c8b0640ea6889cfb55422b7d6657cdcac85c9ee46051ae60f10c5166ddc9c7401aadc3f0764b2896d15daa758df7899109b880363dd6a774ed45e7658e780c6e47bd7ab8fc98b542de36894af52010457462a98cd4ba00550e4c11da974395d234259828fa9a0c9c5a68e567cd5f1a7c38346f12cd25b26db5b95f3907b139dc3f07fc6b30b9c1e547515f477c785b5bcd1a1921db24f0b10481fca79ff102be75bc8c79840e07b579524eca50c08e318ae6d655231468d02cbcfe2bb773143e5e0163c50192a1819a457daa2931452b48583f3ed522b6816d99dc60f5a0db053c838a28b4dd02d098ad58a31958f5a1dcdcc76cb8520003a52b50bc5b58d80e491559ba9a5b89325baf414518b90343ad4efda64db193b9bda9bc307929be524b9ee6896d0ec397386c7340bd9c331543da99c5748348e5ecfb9701bb5322ace4103b7268aa84819f6a736eaaa41c739c53631d83290c7cae703a52d9008f38ab77873c2377afdb5fdc5a320fd1db9b875eec3be2a025b46e38201518cf7aa561648f32774444800e9ed4196366207d2a5a7b2705703b521ad98738a358d82e5b195b2146c668d7084f7a23c4719c52f68da3771c52a78e9d87190c1e338e7de8d1a284fad79d40248a48249e0d04ba0d095b7889c05a279288bc2511000a2bc8c379fb52393b1fd8c7538c18b23dba5318d4ef01baf7a90d5a4fe18a6b6803ca01e98a746da0a3a25b468d77003a66b5af06c2a8908c6775665a3c488e371c0ed5ac7c358bf53ab42848da1875a466d2191f948debe1dd8886d96e9976b489b47db22ae7b4051dfeb513a5aac50a4698180381efdea66252ca2bc5fea4539550d6e14b8d9ee69ce9f6690a671eaa308803935d799529b1d7626537254830c52c03daa2ee757b2b6e679e3897dd8e05319bc67e1d84856d4e266270020273fb5538a4af62658e7f458180229250673df18cd35b1d634fbde6deea37246719e7f6a7a0e4d14d46fe2c1daecf21ed9a28e95c515dc57a1e3639455d8a93196ab3b476cdb33bff00971ef548b9d0357d66eda4b9b8f2d3b17e7fa55f66457ea33cd7900518150ce53fcbd8f528a8f4669aafc3abd16327e9afd26247285319ac17c4fa6bd86a33c3226c646c30c77afb1c924735f35ff682b58ed7c6f3346a479f6f1ca71efc8ff2a6e4a4e8ec4f93d9965cf5014726bb6f10077c8dea1da9cc51a0cb4d9dd8e334198e7e5e0d2396e8d7472e27210a83d4531493cb2ca5b1ef4ea6898a6eef8a6b3db82a1bbd124806476b481cee5e98eb516a76b823ad4d4c85e32950ef188a420ae69f167767ae27006d079a681598e40cf34595036483db814bb4214956ebda8d23a5a3a91850327f14f20837e08e6b821dc549eb9a9ed12d036075e6aac30e4c932c922d9f0b15acbc55a4cc4651e7114899c6f560411578f8a9f0a9acaeaeb59d3238df4d76de625186873d723db3dfec3ef0fe16d3e348a1b845cc904a9320ce32ca4103bf5e9f9afa46c9edf55d21700496b7117eeadf8af53243f142326b5679f8fe7925147c63aa787ff4f9c8381c722abf7b6aaa781815b2f8ff00487b3d42eacdd30d0395ce3e61d8fe45663a95b80cd9ed4dcb892e8cc526dd32a93a282734c26620fd054bddc63cc3c545dc2ff1301baf6af3f25745f04076e7d43a1af283d0f4af2b159761e98a54e307d352349ba1c27ce84a1f5d744f08fe6e314c9600148c7dcd145b00d95e98a528ab1e0b5331c9b4a37142b74dadc2fe69ec96a4c7900fe2bd676cc582e1b93471b41a8d937a346b22a1c743cd6b5f0e1163d5ad8c7eaf50c8f6acfb40d0ef160fd4189c4239dd8ad33e19d96759876b1f4b0ebdea5ced34c6e34d4d23e82d1c6e19fa54fdbae16a2f438bf843239f7a98501702bc9c6b61f932dd1e719522a175686e9d196262b9e841c62a73b5776d50a3cb54231e5fc665979e0a9753b993fbc2f2e5d7185ce5f1f8a5e97f0d6cad6fe0b89351792389c379663c6ec1c81fb8ad44463b8a50400702a9c7e3ceba0e7e65909a8e8b05fed662d1baf468f83feb9a75a3d8b585bfe9daea5b80a7d26403207b7d6a482e3b57bf14efd6689e599b54790f38a5e693d39a5039aab0cb8e98036d43cc5b6778537c814955ce371c702b2ed4fc49e38b6976c9a6bc5b8e14243bd7f7ad658645024855fdf3f4a9bcac4f97288dc59147b450aebc45afe8f6305eea1045731b05de8abb5c13c5667f19668356d66cb574dc125b4d8149f94ab1c8feb5bddf6991ddc26297d4a41078ac9fe36787974ff0dd8ddda459f26568e43ff0ab0383fbe2a35c93f922872835aecc32edd99f0defc537b73895909eb4e6ed7a8f627f14d1b0841ce3039a6f14f62a4a8712a7b9c014ce556627683b7dc53ccb4e1428e31d69ca40214db839239c562690bd9051ac7bba71dea3756b708fe6a743531a9c223cb2838cf7a1cb109ec7eb8a6c5dbb0396f455ca166cf4f71440aa3185e3de9730d93957ed5e0b938ed4f4b66c9e8756f203b547bd58b489150e335588408e41daa5adee7691cd59869322cb1b668ba06a3e595557c73deb63f849ae32dec9a44d366392332c009e8411903f7fe95f35e9da8346f90d56bd2fc412452432453c91491b02ae87d4a7a647ef5ebc32c67078e7d32094258e7ce26d9f1d74d8468d0eae32248dc42dc7553935f34f881d43385e31dbf7afa6bc2572bf11fe1ddcd86acd0ade24af1b4b12e02b839491476e0f3d79cfdabe6af14d84f0de5c412c663961768e453d4106a696492870fa294a2e5cd7b29f772b3648f6a8b0499739c9f7a96bd876819a8b9308c483514fb298bde824a9115de5b691d681b95e3257b5379a663c76cd2227daf8cfa4f5a4c90cd924b12b20e3a8a3c5046d322f1ffbe6a112ea52c3d7f8ab2781b4d7d6fc4da7e9dbf6b5cceb183f9c9fe80d4d2549b7a2d85366c3f0d3c01a3368916b1e21b08ef04a330c1260ab2f6ebf4ad16cbc33f0cf5a8459dcf8534cb59140546102ab7dc3015777d02cacec638e3847936f1ac68a47603ad213c3768f60d288516661b9481cd78f2cb372ecb6f1714308fc0fa45868b25a5883e515c286c1feb59bf80b4efd178b258594a856381f4cf15a6d8df4b6f6ef6d21dae9c7deaad691793e2633631e6139ac591b6d1b1c6d3d9a669cc020fb549a7233503632fa473ed5376ec0a0ef5d889bc88d3b0c0668a8302863ad2d4d7abe2b8c5ec8a428d7b9af576bd354fa00e7deb8718aed71ba5664f8c4e5b119c9c52d471f4a0e79cd55bc49178c6f35558b489a0b0b38f9f399b7339fb60f1f4e3ef50e1c8ab93ec6461c9d170af62a2ed05e456a8b7eeb3ca07a9d136eefc556757f12788975b4b2b0f0f4cd6cac332c83e7fb533f6e3d346c70c9b2f2c33517e26d22db5bd16eb4cba50639d36e4ff29ea0fe0e29fdb316456618257919cf34b900ef4534a70e48057167c77e25d2e6d2f54b9d3af2331cd6ce6320f71d8fe460d41c96e5a41bbdab60fed0b6090f8b6dee94006eed327eac871fe045664b095019fe6af3f934537a056e8234da076ae49bb9e28c46077fc506400fab9e3deb00632bf5dd0f23b532b39006319e2a4a40591c63208a899e0319f31180e3a1a35f42f8a4889f105bb47759c641ef4c158a8ce2ad3a8c6b77661b0372af51555937248509c9a762959de832b8719ce0d1166654cb0e87ad0a12b8c9f6a4c99073d453d4f60b8a63c8ee1b8352ba7dcb315e71f5a82898776fc53fb29402096a74725bd93e487d1ba7c0ad7a0d3bc48b05cddf9105c46c9eb7c234848da4ff0051f9a9ff00ed27a4db436b67af2c11a3ca4db4ce00dce71b9771ef80a715f3f437f2a86d8413dbef5b8782b51b8f8b5e02d5fc27accabfde365e5cb6977b71c83c13edff0009fa31aae59e2f6bff0044638f1b8b3e7bd6594b30007e2a02e1896c76c558bc4d6179a5ead75a7df42d0dc5b4ad148bff003038aaecf967e2a39c9d9443e3a184a496c57922694e076a7ab66edea2768ae3e211b5471ef4b94d7a18936c6084e383570f86fa98d27c51a7df310041708e49ec3355019dbe9507eb4f74b91927563ce3a8cd04e3ce345b8f52d9fa1b35c4573a3adcc4d98e5557523b8383fe744d4256b6d3c880ff00102855e3bf4acebe04ebadaffc3fb38a493f8f687c9619eaabc0fe95a4dc280ab21cf18e0d78728352614a2a2d153f0b687750dddd5c6a52f98f2e5c8cf4e6a0f588d935112af0a1f1f8abcebfe6269cd716dbc38ebb7daa99745a78f320f57522b24a8ab14dc9d963d3650f1ab29ed53f60e4ae0f4aab68a06c41566b50474ae4f7a0332b8ec91535d1d6908723f14b5e955636c81a160d285205281af530cf42da155cc573ef5edc2a8724fb301ba77070694ac0704835e2777515d5c7da8618e31f9240b93627037579d011c814af4fd2bc4f1c0a39a84b4cd5a768f46b81cfb575b18e693d4f435e278a5394631e26fb311fed066393c41a5ab7cd1da498fcb2ff00eab2b9c9efc1c5687f1e6e1a4f1a2463a416a83ff2c9ace6639538e87ad797bb28b004f079e69bcb919c8c8a24bff29e31cd09d4bc2cca781cd750127b06c06383938e94cde191d8e785ef4f90031efea71489096c67a628e2d00f6450610968faa93501adc482e1994715699618cb13dea3b59b132d9991570cbd4fd28d3a7a3a12f457525c1503f34495d597d26a3ae98a498535d86525704d51c6d1b276c788c471bb3f5a756b21661518bbb239e8734eed4e32c4f7ae7b6056c928e470df4079abe7c19f175bf847c630deddcc62b29d1a1bb2470aa470df860b59cb4d9e413f8a1991c9ce48fbd33064e12b62f3414ff9d1b1ff0069c1a76a1e21d335ed224866b7d4ec3719633f3ba36327fed2bfb56436f691a9fe27cd456bd26dd220cc5464aa93f2e6800313bf762bb265e6cdc78dafe8edf70a514715152236573523733ff0f6e39f7a8d9df602cdd6a7e8a74862878e68c8c0700f34c4330031c8a71130dc095cd3f8b4ecde491ae7c11f1dbf85b565577636d390b221ef8ef5f57e8bacdb6b76a9736ec1a32015ff00dd7c0104c54e55b041e315a57c3df883e20d09563b5badd083cabf4af3bc8f1793e45319c64b67d98f116b668e560108e4fd2a8fa9c712ea932c2dfc3cfa7fa562bab7c5af156a132db25f450424fabca8f04fd339e95a6f873503796169248e59ca0c927afd7fd7bd4b931b8446e0853bb2e1a5615473560b57ce09f6aae69ecbb80f7a9fb6240c54f17b0f32b44a273de9628111c8a32d578d9e6cb4281ae8a4114a155424d300f3734d2e2fedade5f2a59915f19c13da9db0cd36b8d3ed67944b2c11bb8e8cc9934dfc79326e27478dec6adac5b4876db0331079d8a4d285c5f49f25a6d1d8b1a7d1c3147808001ec0517d2053231c8dd64905cb1afe6246336a9b785b7cf6f51149966d52388b7e9a390fb07ebfd2a466b886219925451ee5b1488eead65e639a37fb30a670c6fa91ab23ff008a226cb5a0f398ae13c871d54fbd4c07565041ce68171a7d9dc4e934d0472c883d2c57245165db14658e02a8ce07d39a9b24250f7674a5093f8aa3e70f8b538baf1bead229e922c47fec502a97d23209e054d789eeff005babdedc97de25b892407df7313fe7502f969801d31cd27b0a5d889806c6c3c1a428dbe96e94790ac4a02d3791c1f56727deba85c9d8352a7705e80d0df1839ae79acb26df7aee0804127af6ad8ad82b437b804e0671f5ae5c95f2cdbb484ef5ed45b8cfa71ed4d24d99da719ed9a2f62d7652f59b516d72cbeaebc1a68b8460493c8ef563f135a3b9121c3e076aabcd9f7e83a5550926837fe1208ca5335e33246319a60b70446154f38a4a2b3be5ce0d728bb359230cdb9ba9c7d29de772f04edfad304608323d3c75a379e590003f340d34c28a41125540d8e4e682f7259f97c0af3c8810ae3248a6ef03b26e030315d1568c612e2e90fa50127e94d4a1704c84fd8d16d12341cfcd9a3cb1b31c1a2a05bb642003b1cd1909030c79a0fcbb40f6a3c6a5ce0d3ec6e87766bbe4c03566b5220836af5239a8cd2edd5503b53f328dc1473499d5851a1fdbe1e6427f15b5f816f596ce08c9ce05627604f98b95c8f7ad47c1d70c2355278ed51792be257e3ca99b0d8dd64a9ab3e9f2ef4049ace2c2ec84f9b18e9567d17545f4867fbd79c96ca24ad17381f0714e979a88b5b9593e56a918640569b095320cb8da63815eef49078ae835446564e2d7ad2c50d7ae6883debd5f164a80959c349214d12bd544b1c65da077e86371a7c139cc91ab1f7cd26dec6da16f4468a47702a42b94bfd6c776907f925420281f5fad55fe27eb0ba4783ef2457d93cc9e4c3f73ff00f3356a7e9f9e2b06f8f9ac35c6bf169218f95671866fabb0e0fed9fde93e4d455246e356eccd2e5cb32818518ff5febeb4d656543807d55e79096391d7bfbd04b019cf4c545a0dbd8aca919cfde9b4c40068e76e171cfb9a1cbc1ff0a176031bcbea0a40e7b5222977ab2e36b0eb445c632682de99411deb637663d208cb800679c536bf8c08f72af38e4d3884b6f3e67e2897032b8ed45b4c5688d9630f6e4313ea1d6a99aada18656c9ea78abadcfa59547cb513e208375b0703b734c8698c8da2a4806ec0ae8562f5e2bfc4f49dbcd156372acdfd6a8b36858002faa92858b6233e9ae85270a4f6e69c44823391d297cb61550b8210a3731e6a46de38dd76bf5fad33f4b00475a731615b24d7362f602ef4f9a38da658cf97bbad0662368c376e957cf05588f125d8f0ef11cb780adbb1381e68195e7b74aa2ead6b3e9d7b3d8dd214b8b791a29548c6194e081f622bbf22e5c583db20634dce0e29f58c43cdcb0e3bd7218544b9039a9082df3c67009a6b1a9b08d2045555f969711c7e7a53886dd241b0ae71de9c47a7a338f57e28341c585d3c918c1ad17c18ccd0a9eb83cd50bf4263518638fa5693f0b6d5e7b77dca70ae141352e7ae25187fa2f3a6a9922c638c53fb7324241069fe99a6150081daa4df4b691785e6bcdb45d4d03d2353751827ef560b4d4430cab715577b296ddf818f7a736ec474247dab6b66527d970b7bd53dfad3c8e75638cd53a39de3e771fa668f1ea5200bb9c0fa9a28b69934f026f45c370e99a229e2aaf1eb0aae0b12c07535211eb36c42904804d558f2b8bd134f0b44d0615dc8a8e1a84058282589e98a20bc83382e01cf7ab63e5be853c521ee457a998bc80b603f2294b7311cfac0c51fed83f8d8e5ba7bd7ca7f10e7bc7f1d78822bf8dd254bd60037ff009e018c8fba1535f52472c7321319dc338e2b0efed19a20b5f10697e208976adea35a4f8ef22faa327ea5778ffb68ebf22e403938f464b293b871d050c9cf27a5399d0018c9e9c629a48ac19720e3eb50b54c3e567723dfed437c9f566bb290010338f715cb74c839ce3eb5aa2e8197604939c5163443d7da8e611e6653a578020018ae4049b02506f0d8e95e9436338eb4565f56715e6504f23b56f604b4ac8dbd8fd2185359d04b61271bb8a949e3050ae0fe2a302347218b2486e326b03e4fb29179104908cf43d314b89880063b54af88ac3ca93cd1fce3351517181cf4e314f8bb4314b4102e4e7a529d4b1e0d2d15bf9b39fad1231b4edc75aeb39cad9db74da39a74146dcd21130b8239a32ae1726b9ecc43ed1eee5b4be8a7b7731cd1b06423a83dbfad5b3e3968f06a3a5695f1074e05a2d442daea2abcecbb453cfe541ffc6a8f11f5e41e6ae1a26b57b7be15bef0486592d75374222d9ba4695583a61beea2ba2d7342249c5f23350a41cd3e870db73cf1568d33e1ef8a2f7688743be653d498c201fb9a9db7f845e2c2a5a4b24451d09931fe54d6974d8e7911465f4118247daa4ad11e42bb1589ff001abae8ff000975f93568edefa28a0b71cb3f999c8fdab75f0e783343d074c4b7d36cd0ccca3cd99c7adcfd69139462bbb3164d99ff00c31f8736f7fa3aea5adc2c0c8c7ca8738c007193563d374bb7b0d56582d2044855b0800c715a7e9b6021b609200323a0aac6a1606d75291c742dd7e950e59368a7049b9929a5dba9507fa54cc76a38e2a3f47218656a7631c0a9610b2acd91a6329b4f8dc1c8a86bdd1d90b18c607bd5a71c5719011822aa585fa111cf24f650678648cf2a47d68327407be2aef77a7c520242afed51377a49c1c2f14a945c596c33c64567cc001463826ba4b0db863c7b549cfa6aae32bcd3596d0ae483d282dae8752621669b70224231ef4b17531cfaf773ef4ce64954923269b462677208c56a949b0651487ef75721c6c93233c28f7a81bff16ac1af7f765c920c4c16407af201ff003ab9f85b4df32e3cf906e0befef83599fc5df0e5dd8f892eb5d803bc1712867207fb36da38fe954e14df6459a49e91b96893dbcd6b1b5b36630bc66a1be2af877ff9378466b2840fd5c6eb35a9278120c8e7f0cc3f3544f84de2bf57f77de31e47a49fb8ad8230ac99ce4115ea63cb4b8d1e724d699f3ac5f0a7c5d31c35b5846001cb5d1fdb85a9687e096a32c20cdadd9c5211ca8b56703f3b866b762a08e315177974d06a9042a8cc250338edd6952546716bd9f3c78bfe1a6b9e1c8da79365fd920ff6f0210cbff52f6aa8bdb050319e9ce457d837908741b8fa3043291c118ac83c77f0b6fef2fcddf8712d4c52fa8c32cbb369fa70685a77c68c95a6638b0e339e94868c1e79fc569927c27f15c56be6b25848f8e638ee4eefc6540a84bbf04f89accee9b40d4360eac88b201ff89ff2ac58a57d0a736994b11b06e01c77cd28a67a0e2a62e2d3cb94c726e8dc7549176b0fc1e69b4b0151915928b4c252b444ce046adc545b4b11272327b1a9f9ad99c107a1a889ad7f4f2105739a1ad0517f647ea8a9756c13197038354f951a298a91c83cd5ed6311b9936164ee05563c45062f4c88a555b914cc68dbdd0dedca32e08e6972231209ec68500ca804e39a73c9e0838c75146e2d31898b84798320f1de8de57a0a938e3815cb455071cfe69e188ee07b52de99c9d3a19344ca549e4d48695753d85fdbdedbb6c9e071246d8ced23bd29e0f6f9692cb85c03cd0ed3b46ca29aa3ee43630b0c6cda07b62891da40849f2d49ff0089864d391c5749c8a3e2913a8a236f2de365388954fb81d6bb02c483684c363834f2445c648a0c800e40e9489c36674c15ddd3a26c8fe61df1512d1b4eb2acc7716e953595dbe632e4e714d2e230090bc0f6a5c95a1d8a7c660740429bd08f94e2a747f9530d361018bf5352005252d966576c5ad2b031480296b5e86176a89e470814391030c1146ae628b2e152d9ca5447cf668e738a8fbad380c902a788a43a03d6a29e268a619a48a85d59e17a73da9365a53cf2e76fa475356dfd3c649f4d1638950e1401590c2db1b3f26d5501b4b74b7884683007f5a83d56da292fee20b901edae8aab03d8e00cff005ab362a3c3a5c5d5cc2d1826060509faad591c54433936619e29f0cdef84f5059a02e6d588d921ebc1ef5a7fc35f110d5b4c5b799bff00b110c73dc54f6afa5da6af6020bd4dc8eb838acaadaca6f0a78be3486e3cc8d665c01d4a13820fef45286c152b46d2a71f9a67a928c47301ea59169e21ca83d88cd79915c8dc3383914d70a48c07765bf4ce547ab1c522c642f0292b823fd7fea9cbfcbf9a8ed25c7aa1c7c8c467f34724f9260cb4c92183d6bcc171cd7475af1aa11b4863a9e9961a94261beb586e10ff002c91e6aa9a87c31f095c412245a60b776076c913b02a7ed579e95c3cd649260ca099f37f8abc07aee8c6467b46b8b55ce2e205dc08faa8e56a8d7b67b973f3a8ef9e457d92c3d354ef19f80f49f11933b1367787fdfc480eeffa81eb4b9614d099c1c7a3e5c86119646ee381513aed98b880e3e7415b078e3e1eddf866dc6a12df5addc04edc2c651bf6e47f5aa0dfd985b86cb6436377d7233fe5524a2e2c15269eccc02344d8c67de9cc1ea23dfdaa475bb248679157a6735171ff000d940efc517272284b63eb78c8724fbd4b5a342432b8e2a3e0195e0f6a7f6f1854ddde9126312d86b8108c08fa6298ba13275e28f70d93915c0b90bcd626d9acffd9, 1, 1, 2147483647, 915, 12),
(2, 2, 2, 2, 4, 'December 8, 2012', '9:00 pm', 'Mommy House', '1 day UNLI SURF- o 1 day UNLI SURF- o 1 day UNLI SURF- o 1 day UNLI SURF- o ', 5000.00, '', 1, 3, 678654, 91830, 13),
(3, 1, 3, 3, 5, 'Sept. 27, 2012', '5:41 pm', 'cherry''s castle', 'maraot hin tuda1 day UNLI SURF- o 1 day UNLI SURF- o 1 day UNLI SURF- o 1 day UNLI SURF- o ', 1.00, '', 0, 100, 2147483647, 91443, 13);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_number_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `room_status_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `room_number_id` (`room_number_id`),
  KEY `room_type_id` (`room_type_id`),
  KEY `added_by` (`added_by`),
  KEY `room_status_id` (`room_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `room_number_id`, `room_type_id`, `room_status_id`, `status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 1, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 09:50:36', 7),
(2, 2, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 09:50:36', 7),
(3, 3, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:03:19', 7),
(4, 4, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:03:44', 7),
(5, 5, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:04:34', 7),
(6, 6, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:04:40', 7),
(7, 7, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:04:45', 7),
(8, 8, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:04:49', 7),
(9, 9, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:05:02', 7),
(10, 10, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:05:04', 7),
(11, 11, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:05:11', 7),
(12, 12, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:05:13', 7),
(13, 13, 2, 3, 'active', '0000-00-00 00:00:00', '2012-02-08 06:52:20', 7),
(14, 14, 2, 5, 'active', '0000-00-00 00:00:00', '2012-03-19 05:29:13', 7),
(15, 15, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:07:28', 7),
(16, 16, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:07:39', 7),
(17, 17, 2, 3, 'active', '0000-00-00 00:00:00', '2012-08-02 12:55:52', 7),
(18, 18, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:08:24', 7),
(19, 19, 2, 3, 'active', '0000-00-00 00:00:00', '2012-04-13 03:52:04', 7),
(20, 20, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:08:37', 7),
(21, 21, 3, 3, 'active', '0000-00-00 00:00:00', '2012-02-27 03:32:23', 7),
(22, 22, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:09:08', 7),
(23, 23, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:09:10', 7),
(24, 24, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:09:30', 7),
(25, 25, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:09:43', 7),
(26, 26, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:09:45', 7),
(27, 27, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:09:59', 7),
(28, 28, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:10:39', 7),
(29, 29, 2, 2, 'active', '0000-00-00 00:00:00', '2012-02-14 07:53:11', 7),
(30, 30, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 10:12:00', 7),
(31, 31, 2, 5, 'active', '0000-00-00 00:00:00', '2012-03-16 02:43:12', 7),
(32, 32, 2, 5, 'active', '0000-00-00 00:00:00', '2012-03-15 09:55:45', 7),
(33, 33, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 10:12:00', 7),
(34, 34, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:10:54', 7),
(35, 35, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:11:19', 7),
(36, 36, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:11:30', 7),
(37, 37, 1, 3, 'active', '0000-00-00 00:00:00', '2012-08-02 12:25:23', 7),
(38, 38, 1, 5, 'active', '0000-00-00 00:00:00', '2012-02-28 07:09:56', 7),
(39, 39, 1, 3, 'active', '0000-00-00 00:00:00', '2012-08-02 11:39:56', 7),
(40, 40, 1, 3, 'active', '0000-00-00 00:00:00', '2012-08-01 01:22:12', 7),
(41, 41, 1, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 10:12:00', 7),
(42, 42, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:12:58', 7),
(43, 43, 2, 5, 'active', '0000-00-00 00:00:00', '2012-02-08 07:02:53', 7),
(44, 44, 2, 3, 'active', '0000-00-00 00:00:00', '2012-02-10 05:55:07', 7),
(45, 45, 2, 3, 'active', '0000-00-00 00:00:00', '2012-08-03 00:55:45', 7),
(46, 46, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:13:07', 7),
(47, 47, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:13:09', 7),
(48, 48, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:13:12', 7),
(49, 49, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:13:14', 7),
(50, 50, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:13:58', 7),
(51, 51, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:00', 7),
(52, 52, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:02', 7),
(53, 53, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:04', 7),
(54, 54, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:07', 7),
(55, 55, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:09', 7),
(56, 56, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:12', 7),
(57, 57, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:14', 7),
(58, 58, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:16', 7),
(59, 59, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:19', 7),
(60, 60, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:22', 7),
(61, 61, 3, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:14:25', 7),
(62, 62, 4, 3, 'active', '0000-00-00 00:00:00', '2012-06-12 02:08:32', 7),
(63, 63, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 10:12:00', 7),
(64, 64, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:07', 7),
(65, 65, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:10', 7),
(66, 66, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:12', 7),
(67, 67, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:14', 7),
(68, 68, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:16', 7),
(69, 69, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:19', 7),
(70, 70, 2, 2, 'active', '0000-00-00 00:00:00', '2012-02-14 07:59:00', 7),
(71, 71, 1, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:39', 7),
(72, 72, 1, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:15:42', 7),
(73, 73, 4, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:01', 7),
(74, 74, 1, 5, 'active', '0000-00-00 00:00:00', '2012-02-08 10:10:59', 7),
(75, 75, 1, 3, 'active', '0000-00-00 00:00:00', '2012-02-23 10:36:46', 7),
(76, 76, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:36', 7),
(77, 77, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:38', 7),
(78, 78, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:41', 7),
(79, 79, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:43', 7),
(80, 80, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:46', 7),
(81, 81, 2, 1, 'active', '0000-00-00 00:00:00', '2012-02-03 03:16:49', 7),
(85, 82, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 05:40:46', 12),
(86, 84, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 05:47:01', 12),
(88, 85, 2, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 05:49:35', 12),
(89, 86, 3, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 05:52:25', 12),
(90, 87, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 05:55:35', 12),
(91, 88, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 05:56:16', 12),
(95, 86, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 06:06:13', 12),
(96, 87, 2, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 06:07:20', 12),
(97, 90, 1, 1, 'active', '0000-00-00 00:00:00', '2012-07-12 06:07:33', 12),
(127, 95, 1, 3, 'active', '2012-07-12 04:37:21', '2012-08-03 02:40:09', 12),
(128, 81, 1, 1, 'active', '2012-07-12 19:32:27', '2012-07-13 01:32:27', 12),
(129, 96, 1, 5, 'active', '2012-07-12 21:51:48', '2012-07-31 08:25:32', 12),
(130, 98, 1, 3, 'active', '2012-07-20 00:02:43', '2012-08-02 10:30:44', 15),
(133, 95, 5, 3, 'active', '2012-08-02 19:51:02', '2012-08-03 02:40:09', 15);

-- --------------------------------------------------------

--
-- Table structure for table `rooms_1`
--

CREATE TABLE IF NOT EXISTS `rooms_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `type` int(2) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `comment` text NOT NULL,
  `date_updated` int(11) NOT NULL DEFAULT '0' COMMENT 'this date here is auto generated by the php code please refer to the room_tags date table thanks',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `type` (`type`),
  KEY `number` (`number`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `rooms_1`
--

INSERT INTO `rooms_1` (`id`, `number`, `type`, `status`, `comment`, `date_updated`) VALUES
(1, 224, 3, 1, '', 0),
(2, 225, 1, 1, '', 0),
(3, 226, 1, 1, 'test ra ni nga comment oie, ayaw pa ilad', 0),
(4, 227, 1, 1, '', 0),
(5, 228, 1, 1, '', 0),
(6, 229, 1, 1, '', 0),
(7, 324, 1, 1, '', 0),
(8, 325, 1, 1, '', 0),
(9, 327, 1, 1, '', 0),
(10, 328, 1, 1, '', 0),
(11, 201, 2, 1, '', 0),
(12, 202, 2, 1, '', 0),
(13, 205, 2, 1, '', 0),
(14, 206, 2, 1, '', 0),
(15, 207, 2, 1, '', 0),
(16, 208, 2, 1, '', 0),
(17, 216, 2, 1, '', 0),
(18, 217, 2, 1, '', 0),
(19, 218, 2, 1, '', 0),
(20, 219, 2, 1, '', 0),
(21, 220, 2, 1, '', 0),
(22, 221, 2, 1, '', 0),
(23, 222, 2, 1, '', 0),
(24, 230, 2, 1, '', 0),
(25, 231, 2, 1, '', 0),
(26, 232, 2, 1, '', 0),
(27, 233, 2, 1, '', 0),
(28, 234, 2, 1, '', 0),
(29, 235, 2, 1, '', 0),
(30, 301, 2, 1, '', 0),
(31, 302, 2, 1, '', 0),
(32, 316, 2, 1, '', 0),
(33, 317, 2, 1, '', 0),
(34, 318, 2, 1, '', 0),
(35, 319, 2, 1, '', 0),
(36, 320, 2, 1, '', 0),
(37, 321, 2, 1, '', 0),
(38, 322, 2, 1, '', 0),
(39, 323, 2, 1, '', 0),
(40, 329, 2, 1, '', 0),
(41, 330, 2, 1, '', 0),
(42, 331, 2, 1, '', 0),
(43, 332, 2, 1, '', 0),
(44, 333, 2, 1, '', 0),
(45, 334, 2, 1, '', 0),
(46, 203, 3, 1, '', 0),
(47, 209, 3, 1, '', 0),
(48, 210, 3, 1, '', 0),
(49, 211, 3, 1, '', 0),
(50, 213, 3, 1, '', 0),
(51, 214, 3, 1, '', 0),
(52, 303, 3, 1, '', 0),
(53, 304, 3, 1, '', 0),
(54, 305, 3, 1, '', 0),
(55, 306, 3, 1, '', 0),
(56, 307, 3, 1, '', 0),
(57, 308, 3, 1, '', 0),
(58, 309, 3, 1, '', 0),
(59, 310, 3, 1, '', 0),
(60, 311, 3, 1, '', 0),
(61, 312, 3, 1, '', 0),
(62, 313, 3, 1, '', 0),
(63, 314, 3, 1, '', 0),
(64, 101, 4, 1, '', 0),
(65, 102, 4, 1, '', 0),
(66, 103, 4, 1, '', 0),
(67, 104, 4, 1, '', 0),
(68, 105, 4, 1, '', 0),
(69, 106, 4, 1, '', 0),
(70, 107, 4, 1, '', 0),
(71, 108, 4, 1, '', 0),
(72, 109, 4, 1, '', 0),
(73, 110, 4, 1, '', 0),
(74, 111, 4, 1, '', 0),
(75, 112, 4, 1, '', 0),
(76, 204, 4, 1, '', 0),
(77, 212, 4, 1, '', 0),
(78, 215, 4, 1, '', 0),
(79, 223, 4, 1, '', 0),
(80, 315, 4, 1, '', 0),
(81, 326, 4, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rooms_accom`
--

CREATE TABLE IF NOT EXISTS `rooms_accom` (
  `room_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `accom_type` enum('guest','student') NOT NULL,
  `accom_type_id` int(11) NOT NULL COMMENT 'the id of the guest or student from the guest_accom table or student_accom table',
  `room_number_id` int(11) DEFAULT NULL,
  `accom_status` enum('checkin','checkout','reserve') NOT NULL,
  `rooms_accom_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_accom_id`),
  KEY `room_number_id` (`room_number_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `rooms_accom`
--

INSERT INTO `rooms_accom` (`room_accom_id`, `accom_type`, `accom_type_id`, `room_number_id`, `accom_status`, `rooms_accom_status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 'guest', 1, 37, 'checkin', 'active', '2012-02-05 19:29:01', '2012-08-02 11:03:56', 12),
(2, 'student', 1, 38, 'checkout', 'active', '2012-02-05 19:32:15', '2012-02-14 07:58:14', 12),
(3, 'guest', 2, 38, 'reserve', 'active', '2012-02-07 01:53:37', '2012-02-07 08:53:37', 12),
(4, 'guest', 3, 38, 'checkin', 'active', '2012-02-07 01:54:17', '2012-02-24 06:33:33', 12),
(5, 'guest', 4, 13, 'checkin', 'active', '2012-02-07 23:52:20', '2012-02-23 08:05:10', 12),
(6, 'guest', 5, 37, 'checkin', 'active', '2012-02-07 23:55:14', '2012-08-02 11:40:43', 12),
(7, 'guest', 6, 70, 'checkout', 'active', '2012-02-08 00:01:52', '2012-02-14 07:58:59', 12),
(8, 'guest', 7, 43, 'reserve', 'active', '2012-02-08 00:02:53', '2012-02-08 07:02:53', 12),
(9, 'guest', 8, 44, 'checkout', 'active', '2012-02-08 00:05:32', '2012-02-17 01:48:40', 12),
(10, 'guest', 9, 44, 'checkin', 'active', '2012-02-08 00:06:10', '2012-02-08 07:06:10', 12),
(11, 'guest', 10, 75, 'checkin', 'active', '2012-02-08 00:08:25', '2012-02-23 10:36:46', 12),
(12, 'guest', 11, 29, 'checkout', 'active', '2012-02-08 00:10:13', '2012-02-14 07:38:46', 12),
(13, 'guest', 12, 31, 'reserve', 'active', '2012-02-08 00:20:40', '2012-02-08 08:34:17', 12),
(14, 'guest', 13, 40, 'checkin', 'active', '2012-02-08 01:50:04', '2012-08-01 01:22:12', 12),
(15, 'student', 2, 44, 'checkin', 'active', '2012-02-08 01:58:38', '2012-02-10 06:29:45', 12),
(16, 'student', 3, 74, 'reserve', 'active', '2012-02-08 02:53:10', '2012-02-08 10:10:59', 12),
(17, 'guest', 14, 75, 'checkin', 'active', '2012-02-14 00:49:24', '2012-04-19 10:41:01', 12),
(18, 'student', 4, 29, 'checkout', 'active', '2012-02-14 00:53:01', '2012-02-14 07:53:11', 12),
(19, 'student', 5, 13, 'checkin', 'active', '2012-02-14 00:53:28', '2012-02-23 08:18:49', 12),
(20, 'guest', 15, 39, 'reserve', 'active', '2012-02-23 02:28:34', '2012-02-24 00:58:53', 12),
(21, 'guest', 16, 37, 'checkin', 'active', '2012-02-26 18:52:47', '2012-08-01 07:14:43', 12),
(22, 'guest', 17, 21, 'checkin', 'active', '2012-02-26 20:32:23', '2012-02-27 03:32:23', 12),
(23, 'guest', 18, 37, 'checkin', 'active', '2012-02-27 23:12:11', '2012-06-14 07:53:33', 12),
(24, 'guest', 19, 38, 'reserve', 'active', '2012-02-28 00:09:56', '2012-02-28 07:09:56', 12),
(25, 'guest', 20, 14, 'reserve', 'active', '2012-02-28 18:34:38', '2012-03-19 05:29:13', 12),
(26, 'guest', 21, 62, 'checkin', 'active', '2012-03-06 18:29:48', '2012-06-12 02:08:32', 12),
(27, 'guest', 22, 37, 'checkin', 'active', '2012-03-12 00:01:07', '2012-03-12 07:01:07', 12),
(28, 'guest', 23, 37, 'reserve', 'active', '2012-03-12 00:03:06', '2012-05-09 03:37:44', 12),
(29, 'student', 6, 31, 'reserve', 'active', '2012-03-12 01:31:33', '2012-05-22 01:29:09', 12),
(30, 'guest', 24, 37, 'checkin', 'active', '2012-03-15 02:52:51', '2012-08-02 01:21:27', 12),
(31, 'guest', 25, 37, 'checkin', 'active', '2012-03-15 02:53:16', '2012-03-15 09:53:16', 12),
(32, 'guest', 26, 37, 'reserve', 'active', '2012-03-15 02:53:37', '2012-05-02 05:48:36', 12),
(33, 'student', 7, 32, 'reserve', 'active', '2012-03-15 02:55:45', '2012-03-15 09:55:45', 12),
(34, 'guest', 27, 39, 'checkin', 'active', '2012-03-15 18:40:12', '2012-03-16 01:40:12', 20),
(35, 'guest', 28, 19, 'checkin', 'active', '2012-04-09 23:52:51', '2012-04-13 03:52:04', 12),
(36, 'student', 8, 37, 'checkout', 'active', '2012-05-01 23:46:36', '2012-07-31 08:49:30', 12),
(37, 'guest', 29, 91, 'checkin', 'active', '2012-07-12 02:34:13', '2012-07-12 10:11:26', 12),
(38, 'student', 9, 21, 'checkout', 'active', '2012-07-17 00:01:36', '2012-08-03 02:02:09', 12),
(39, 'student', 10, 95, 'checkin', 'active', '2012-07-17 21:18:15', '2012-08-03 02:40:09', 12),
(40, 'guest', 30, 96, 'reserve', 'active', '2012-07-31 02:25:32', '2012-07-31 08:25:32', 15),
(41, 'guest', 31, 98, 'checkin', 'active', '2012-08-02 04:30:44', '2012-08-02 10:30:44', 15),
(42, 'guest', 32, 17, 'checkin', 'active', '2012-08-01 18:55:52', '2012-08-02 12:55:52', 15),
(43, 'guest', 33, 45, 'checkin', 'active', '2012-08-02 18:55:45', '2012-08-03 00:55:45', 15);

-- --------------------------------------------------------

--
-- Table structure for table `room_number_ids`
--

CREATE TABLE IF NOT EXISTS `room_number_ids` (
  `room_number_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(5) NOT NULL,
  `room_no_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_number_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Dumping data for table `room_number_ids`
--

INSERT INTO `room_number_ids` (`room_number_id`, `room_no`, `room_no_status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 101, 'active', '2012-07-12 15:00:00', '2012-07-11 09:05:48', 7),
(2, 102, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(3, 103, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(4, 104, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(5, 105, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(6, 106, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(7, 107, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(8, 108, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(9, 109, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(10, 110, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(11, 111, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(12, 112, 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(13, 201, 'active', '0000-00-00 00:00:00', '2012-02-03 02:51:10', 7),
(14, 202, 'active', '0000-00-00 00:00:00', '2012-02-03 02:52:37', 7),
(15, 203, 'active', '0000-00-00 00:00:00', '2012-02-03 02:52:39', 7),
(16, 204, 'active', '0000-00-00 00:00:00', '2012-02-03 02:52:41', 7),
(17, 205, 'active', '0000-00-00 00:00:00', '2012-02-03 02:52:55', 7),
(18, 206, 'active', '0000-00-00 00:00:00', '2012-02-03 02:52:57', 7),
(19, 207, 'active', '0000-00-00 00:00:00', '2012-02-03 02:52:59', 7),
(20, 208, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:00', 7),
(21, 209, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:02', 7),
(22, 210, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:04', 7),
(23, 211, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:06', 7),
(24, 212, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:08', 7),
(25, 213, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:10', 7),
(26, 214, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:11', 7),
(27, 215, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:13', 7),
(28, 216, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:14', 7),
(29, 217, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:16', 7),
(30, 218, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:17', 7),
(31, 219, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:19', 7),
(32, 220, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:21', 7),
(33, 221, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:26', 7),
(34, 222, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:27', 7),
(35, 223, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:32', 7),
(36, 224, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:33', 7),
(37, 225, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:37', 7),
(38, 226, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:40', 7),
(39, 227, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:41', 7),
(40, 228, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:43', 7),
(41, 229, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:45', 7),
(42, 230, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:48', 7),
(43, 231, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:53', 7),
(44, 232, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:56', 7),
(45, 233, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:58', 7),
(46, 234, 'active', '0000-00-00 00:00:00', '2012-02-03 02:53:59', 7),
(47, 235, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:03', 7),
(48, 301, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:09', 7),
(49, 302, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:10', 7),
(50, 303, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:21', 7),
(51, 304, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:22', 7),
(52, 305, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:24', 7),
(53, 306, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:26', 7),
(54, 307, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:28', 7),
(55, 308, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:30', 7),
(56, 309, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:32', 7),
(57, 310, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:37', 7),
(58, 311, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:39', 7),
(59, 312, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:40', 7),
(60, 313, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:41', 7),
(61, 314, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:46', 7),
(62, 315, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:48', 7),
(63, 316, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:49', 7),
(64, 317, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:51', 7),
(65, 318, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:52', 7),
(66, 319, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:54', 7),
(67, 320, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:56', 7),
(68, 321, 'active', '0000-00-00 00:00:00', '2012-02-03 02:54:58', 7),
(69, 322, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:00', 7),
(70, 323, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:01', 7),
(71, 324, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:07', 7),
(72, 325, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:08', 7),
(73, 326, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:09', 7),
(74, 327, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:12', 7),
(75, 328, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:13', 7),
(76, 329, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:15', 7),
(77, 330, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:18', 7),
(78, 331, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:24', 7),
(79, 332, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:26', 7),
(80, 333, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:29', 7),
(81, 334, 'active', '0000-00-00 00:00:00', '2012-02-03 02:55:32', 7),
(82, 335, 'active', '0000-00-00 00:00:00', '2012-07-12 05:12:15', 12),
(83, 336, 'active', '0000-00-00 00:00:00', '2012-07-12 05:12:49', 12),
(84, 337, 'active', '0000-00-00 00:00:00', '2012-07-12 05:19:31', 12),
(85, 338, 'active', '2012-07-11 23:26:36', '2012-07-12 05:26:36', 12),
(86, 339, 'active', '2012-07-11 23:36:23', '2012-07-12 05:36:23', 12),
(87, 339, 'active', '2012-07-11 23:36:25', '2012-07-12 05:36:25', 12),
(88, 340, 'active', '2012-07-11 23:38:21', '2012-07-12 06:12:08', 12),
(89, 341, 'active', '2012-07-12 00:01:56', '2012-07-12 06:12:16', 12),
(90, 342, 'active', '2012-07-12 00:03:02', '2012-07-12 06:12:27', 12),
(91, 91, 'active', '2012-07-12 00:03:33', '2012-07-12 06:35:40', 12),
(92, 92, 'active', '2012-07-12 00:04:23', '2012-07-12 06:35:47', 12),
(93, 93, 'active', '2012-07-12 00:05:00', '2012-07-12 06:35:54', 12),
(94, 94, 'active', '2012-07-12 00:14:44', '2012-07-12 06:36:02', 12),
(95, 95, 'active', '2012-07-12 00:14:52', '2012-07-12 06:36:10', 12),
(96, 96, 'active', '0000-00-00 00:00:00', '2012-07-12 06:52:29', 12),
(97, 97, 'active', '0000-00-00 00:00:00', '2012-07-12 06:52:30', 12),
(98, 98, 'active', '0000-00-00 00:00:00', '2012-07-12 07:06:35', 12),
(99, 99, 'active', '0000-00-00 00:00:00', '2012-07-12 06:52:30', 12),
(100, 100, 'active', '0000-00-00 00:00:00', '2012-07-12 06:52:30', 12);

-- --------------------------------------------------------

--
-- Table structure for table `room_rates`
--

CREATE TABLE IF NOT EXISTS `room_rates` (
  `room_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_rate` float(11,2) DEFAULT NULL,
  `room_rate_status` enum('active','inactive') DEFAULT 'active',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_rate_id`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `room_rates`
--

INSERT INTO `room_rates` (`room_rate_id`, `room_rate`, `room_rate_status`, `date_added`, `last_updated`, `added_by`) VALUES
(1, 0.00, 'active', '0000-00-00 00:00:00', '2012-02-03 02:42:19', 7);

-- --------------------------------------------------------

--
-- Table structure for table `room_status`
--

CREATE TABLE IF NOT EXISTS `room_status` (
  `room_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_status_name` varchar(65) DEFAULT NULL,
  `room_status_css` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`room_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `room_status`
--

INSERT INTO `room_status` (`room_status_id`, `room_status_name`, `room_status_css`) VALUES
(1, 'Vacant - Clean', 'hr-vc'),
(2, 'Vacant - Dirty', 'hr-vd'),
(3, 'Occupied', 'hr-oc'),
(4, 'Occupied with vacant bed', 'hr-ovb'),
(5, 'Reserved', 'hr-wr'),
(6, 'Under Maintenance', 'hr-um'),
(7, 'Corporate', 'hr-c');

-- --------------------------------------------------------

--
-- Table structure for table `room_status_1`
--

CREATE TABLE IF NOT EXISTS `room_status_1` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `css` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `room_status_1`
--

INSERT INTO `room_status_1` (`id`, `name`, `css`) VALUES
(1, 'Vacant - Clean', 'hr-vc'),
(2, 'Vacant - Dirty', 'hr-vd'),
(3, 'Occupied', 'hr-oc'),
(4, 'Occupied with vacant bed', 'hr-ovb'),
(5, 'Reserved', 'hr-wr'),
(6, 'Under Maintenance', 'hr-um'),
(7, 'Corporate', '');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `room_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_name` varchar(35) DEFAULT NULL,
  `room_rate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_type_id`),
  KEY `room_rate_id` (`room_rate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`room_type_id`, `room_type_name`, `room_rate_id`) VALUES
(1, 'Single', 1),
(2, 'Double', 1),
(3, 'Triple', 1),
(4, 'Quad', 1),
(5, 'Corporate', 1);

-- --------------------------------------------------------

--
-- Table structure for table `room_type_1`
--

CREATE TABLE IF NOT EXISTS `room_type_1` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `rate` float(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `room_type_1`
--

INSERT INTO `room_type_1` (`id`, `name`, `rate`) VALUES
(1, 'single', 0.00),
(2, 'double', 0.00),
(3, 'quad', 0.00),
(4, 'corporate', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `srs`
--

CREATE TABLE IF NOT EXISTS `srs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL COMMENT 'employee id',
  `head_appr` enum('pending','rejected','cancelled','approved') NOT NULL DEFAULT 'pending' COMMENT '0 or date, 0 means not approved and if approved the value will be the date in unixtime',
  `srs_released` int(11) NOT NULL DEFAULT '0' COMMENT '0 or date, released from the stock',
  `received` int(11) NOT NULL DEFAULT '0' COMMENT '0 or date',
  `purpose` varchar(255) NOT NULL,
  `date_required` int(11) NOT NULL DEFAULT '0',
  `date_requested` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_id` (`stock_id`),
  KEY `requested_by` (`requested_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `srs`
--

INSERT INTO `srs` (`id`, `stock_id`, `qty`, `requested_by`, `head_appr`, `srs_released`, `received`, `purpose`, `date_required`, `date_requested`) VALUES
(5, 1, 21, 11, 'approved', 0, 0, 'sf', 1323730800, 1323745042),
(6, 1, 1, 11, 'approved', 0, 0, 'asdf', 1323730800, 1323745230),
(7, 2, 1, 11, 'approved', 0, 0, 'sdaf', 1323730800, 1323745295),
(8, 1, 1, 11, 'pending', 0, 0, 'saf', 1323730800, 1323745328),
(9, 1, 1, 11, 'pending', 0, 0, 'as', 1323730800, 1323745406),
(10, 2, 1, 11, 'pending', 0, 0, 'as', 1323730800, 1323745406),
(11, 1, 1, 11, 'pending', 0, 0, 'rteasfsdf', 1323644400, 1323747222),
(12, 2, 1, 11, 'pending', 0, 0, 'rteasfsdf', 1323644400, 1323747222),
(13, 1, 1, 11, 'cancelled', 0, 0, 'dg', 1323817200, 1323856913),
(15, 1, 1, 11, 'pending', 0, 0, 'asdf', 1323212400, 1323857073),
(16, 1, 1, 11, 'pending', 0, 0, 'asdf', 1324249200, 1324281716),
(17, 2, 1, 11, 'pending', 0, 0, 'asdf', 1324249200, 1324281716),
(18, 1, 1, 13, 'pending', 0, 0, 'aasdf', 1324508400, 1324541701),
(19, 1, 1, 12, 'cancelled', 0, 0, 'asdf', 1326322800, 1325740932),
(20, 2, 1, 12, 'cancelled', 0, 0, 'asdf', 1326322800, 1325740932),
(23, 1, 1, 12, 'cancelled', 0, 0, 'For snack', 1330124400, 1330047666),
(25, 2, 4, 20, 'cancelled', 0, 0, 'Hi this is for yougart', 1331074800, 1331513880),
(28, 1, 1, 20, 'cancelled', 0, 0, 'for dugang sa pundo', 1332370800, 1331773623),
(29, 2, 1, 20, 'cancelled', 0, 0, 'for dugang sa pundo', 1332370800, 1331773623),
(31, 1, 1, 20, 'cancelled', 0, 0, 'jhkjkhh', 1331074800, 1333013480),
(32, 2, 1, 20, 'cancelled', 0, 0, 'jhkjkhh', 1331074800, 1333013480),
(35, 1, 1, 20, 'pending', 0, 0, 'xcxvcv', 1331074800, 1333071734),
(36, 2, 1, 20, 'cancelled', 0, 0, 'xcxvcv', 1331074800, 1333071734),
(37, 1, 1, 13, 'pending', 0, 0, '1', 1337119200, 1338458927),
(38, 1, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(39, 2, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(40, 9, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(41, 10, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(42, 11, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(43, 25, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(44, 26, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(45, 27, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(46, 30, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(47, 33, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608610),
(48, 34, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608611),
(49, 35, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608611),
(50, 36, 1, 23, 'pending', 0, 0, 'h', 1338760800, 1340608611),
(51, 1, 1, 17, 'pending', 0, 0, 'try', 1340920800, 1340774283),
(52, 2, 1, 17, 'pending', 0, 0, 'try', 1340920800, 1340774283),
(53, 9, 1, 17, 'pending', 0, 0, 'try', 1340920800, 1340774283),
(54, 9, 1, 25, 'pending', 0, 0, 'for hotel', 1341871200, 1341565286),
(55, 36, 1, 25, 'pending', 0, 0, 'for housekeeping', 1341871200, 1341799053);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE IF NOT EXISTS `stocks` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_type` int(11) NOT NULL DEFAULT '0' COMMENT 'links to stock designation table',
  `meal_type_id` int(11) NOT NULL,
  `units` int(11) NOT NULL COMMENT 'reference to the unitsof_measure table',
  `name` varchar(45) NOT NULL,
  `description` varchar(120) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `qty_inhand` int(11) NOT NULL,
  `reorder_point` int(11) NOT NULL DEFAULT '0',
  `aqui_price` float(15,2) NOT NULL COMMENT 'aquisition price',
  `supplier` int(11) NOT NULL COMMENT 'reference to the suppliers table',
  `stock_image` varchar(255) NOT NULL COMMENT 'the image of the stock',
  `added_by` int(11) NOT NULL COMMENT 'if it is requested by the staff then the staff''s emp_id will be added',
  `date_added` int(11) NOT NULL COMMENT 'the date of stock added',
  PRIMARY KEY (`stock_id`),
  KEY `units` (`units`),
  KEY `supplier` (`supplier`),
  KEY `added_by` (`added_by`),
  KEY `stock_type` (`stock_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`stock_id`, `stock_type`, `meal_type_id`, `units`, `name`, `description`, `brand`, `qty_inhand`, `reorder_point`, `aqui_price`, `supplier`, `stock_image`, `added_by`, `date_added`) VALUES
(1, 2, 1, 2, 'breakfast_food', 'snack namo', 'jollibee', 50, 100, 99.00, 1232, 'default image here', 1, 1318846362),
(2, 1, 2, 1, 'jhubdie', 'Meat', 'brand 2', 20, 7, 3.50, 12, 'default image here', 1, 1318914295),
(9, 2, 0, 1, 'Notebook', 'this is for hotel', 'x', 25, 2, 5.00, 7, 'default image here', 1, 1334647360),
(10, 1, 0, 1, 'Meat', 'this is for kitchen', 'y', 23, 50, 35.00, 3, 'default image here', 1, 1334652286),
(11, 2, 0, 1, 'Ballpen', 'office supplies', 'Pilot', 30, 100, 56.00, 3, 'default image here', 1, 1334656492),
(25, 1, 0, 1, 'fbitem', 'ds', 'x', 57, 100, 21.00, 3, 'default image here', 1, 1335174033),
(26, 1, 0, 1, 'sd', 'Fried Chicken', 'sds', 40, 2, 2.00, 4, 'default image here', 1, 1335174064),
(27, 1, 0, 1, 'Onion', 'Onion', 'jy', 70, 122, 2.00, 3, 'default image here', 1, 1336030614),
(30, 1, 0, 1, 'tisue', 'tissue paper', 'nike', 20, 11, 6.00, 3, 'default image here', 1, 1336036666),
(33, 14, 0, 5, 'Bond Aid', 'Bond Aid', 'ad', 90, 2, 9.00, 3, 'default image here', 1, 1336122313),
(34, 3, 0, 7, 'Nescafe', 'Nescafe 3in1', 'coffemate', 80, 30, 6.00, 3, 'default image here', 1, 1336371347),
(35, 3, 0, 7, 'Rebisco', 'Rebisco Biscuits', 'rebisco', 15, 41, 6.00, 3, 'default image here', 1, 1336371779),
(36, 14, 0, 5, 'Thread', 'Sinulid', 'hope', 89, 41, 6.00, 8, 'default image here', 1, 1336373132);

-- --------------------------------------------------------

--
-- Table structure for table `stock_items`
--

CREATE TABLE IF NOT EXISTS `stock_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `unitsof_measure` int(11) NOT NULL,
  `account_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_code` (`account_code`),
  KEY `unitsof_measure` (`unitsof_measure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stock_items`
--

INSERT INTO `stock_items` (`id`, `name`, `description`, `price`, `unitsof_measure`, `account_code`) VALUES
(1, 'Eggs', 'An egg from the eggs', 5.00, 1, 1020305);

-- --------------------------------------------------------

--
-- Table structure for table `stock_type`
--

CREATE TABLE IF NOT EXISTS `stock_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `stock_type`
--

INSERT INTO `stock_type` (`id`, `name`) VALUES
(1, 'F and B'),
(2, 'Office Supplies'),
(3, 'Larry''s Place'),
(14, 'Hotel Supplies');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_no` varchar(25) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `english_name` varchar(25) NOT NULL,
  `birth_date` int(11) NOT NULL DEFAULT '0',
  `country` varchar(25) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `civil_status` enum('single','married','widowed') NOT NULL,
  `nationality` varchar(25) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_no` varchar(25) NOT NULL,
  `passport_no` varchar(35) NOT NULL,
  `visa_expiry` int(11) NOT NULL DEFAULT '0' COMMENT 'date in unix time',
  `accomodation` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 or 1 of the need accomodation',
  `date_reg` int(11) NOT NULL DEFAULT '0' COMMENT 'date registered or added',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_no`, `firstname`, `lastname`, `english_name`, `birth_date`, `country`, `gender`, `civil_status`, `nationality`, `address`, `email`, `contact_no`, `passport_no`, `visa_expiry`, `accomodation`, `date_reg`) VALUES
(1, 'AKL-002134', 'Kiera', 'Low', 'Kier', 1337810400, 'Korea', 'male', 'single', 'Korean', 'North East, South Korea', 'kiera@localhost.com', '12345678', '23SFD-GBG-23', 0, 0, 0),
(2, 'AKL-001111', 'amadeo', 'pelaez', 'johnny depp', 0, 'russia', 'male', 'single', 'russian', '', '', '', '', 0, 0, 0),
(3, 'AKL-002222', 'andro', 'misa', 'john doe', 0, 'south korea', 'male', 'single', 'korean', '', '', '', '', 0, 0, 0),
(4, 'AKL-003333', 'britta', 'oblan', 'alicia', 0, 'japan', 'female', 'married', 'japanese', '', '', '', '', 0, 0, 0),
(5, 'AKL-004444', 'lou', 'los banos', 'mariah', 0, 'vietnam', 'female', 'married', 'vietnamese', '', '', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_accommodation_1`
--

CREATE TABLE IF NOT EXISTS `students_accommodation_1` (
  `students_accommodation_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(11) DEFAULT NULL,
  `booking_id` varchar(65) DEFAULT NULL,
  `booking_agent` varchar(65) NOT NULL,
  `checkin_date` datetime DEFAULT '0000-00-00 00:00:00',
  `checkout_date` datetime DEFAULT '0000-00-00 00:00:00',
  `sendoff_time` time DEFAULT '00:00:00',
  `pickup_time` time DEFAULT '00:00:00',
  `comments` text,
  `status` enum('reserved','checkin','checkout') DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`students_accommodation_id`),
  KEY `added_by` (`added_by`),
  KEY `room_no` (`room_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `students_accom_1`
--

CREATE TABLE IF NOT EXISTS `students_accom_1` (
  `students_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `english_name` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `numberof_weeks` float(5,2) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `mobile_no` varchar(25) DEFAULT NULL,
  `student_no` varchar(65) DEFAULT NULL,
  `course` varchar(75) DEFAULT NULL,
  `dateof_birth` date DEFAULT NULL,
  `nationality` varchar(65) DEFAULT NULL,
  `passport_no` varchar(65) DEFAULT NULL,
  `expiry_date` date DEFAULT '0000-00-00',
  `placeof_issue` varchar(255) DEFAULT NULL,
  `dateof_issue` date DEFAULT '0000-00-00',
  `visa_expiry` date DEFAULT '0000-00-00',
  `ssp_validity` date DEFAULT '0000-00-00',
  `address_abroad` varchar(300) DEFAULT NULL,
  `nameof_guardian` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`students_accom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_accom`
--

CREATE TABLE IF NOT EXISTS `student_accom` (
  `student_accom_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(25) DEFAULT NULL,
  `booking_agent` varchar(35) DEFAULT NULL,
  `checkin_date` datetime DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `sendoff_time` time DEFAULT NULL,
  `pickup_time` time DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`student_accom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `student_accom`
--

INSERT INTO `student_accom` (`student_accom_id`, `booking_id`, `booking_agent`, `checkin_date`, `checkout_date`, `sendoff_time`, `pickup_time`, `comments`) VALUES
(1, '123456789', 'Corporate Rate', '2012-02-07 12:00:00', '2012-02-09 12:00:00', '00:00:00', '00:00:00', '[removed]alert&#40;''test''&#41;[removed]'),
(2, '', 'Asia Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', 'sdf'),
(3, '134', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(4, '123456789', 'Asia Rooms', '2012-02-08 12:00:00', '2012-02-08 12:00:00', '00:00:00', '00:00:00', ''),
(5, '123654', 'Rates Go', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(6, '', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(7, '56', 'Late Rooms', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', ''),
(8, '', 'Rates Go', '2012-07-17 14:52:00', '2012-07-28 00:00:00', '00:00:00', '00:00:00', ''),
(9, '12564', 'Bigfoot/IAFT', '2012-07-18 00:00:00', '2012-07-17 00:00:00', '00:00:00', '00:00:00', ''),
(10, '45', 'Rates Go', '2012-07-18 00:00:00', '2012-07-26 00:00:00', '00:00:00', '00:00:00', 'Na');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(25) DEFAULT NULL,
  `mobile_no` varchar(25) NOT NULL,
  `faxnum` varchar(35) DEFAULT NULL,
  `note` varchar(25) DEFAULT NULL,
  `terms` varchar(35) DEFAULT NULL,
  `company_name` varchar(35) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_added` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1234 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `name`, `address`, `phone_no`, `mobile_no`, `faxnum`, `note`, `terms`, `company_name`, `status`, `date_added`) VALUES
(3, '8 DEES Trading', 'Pajo, Lapu-lapu City TIN#195-684-046-000  ', '2964', '', '', '', '', '', 'active', 0),
(4, 'A. OCLEASA SIGN PRINTERS', '   ', '', '', '', '', '', '', 'active', 0),
(7, 'Abayon,Meliza R.', '   ', '', '', '', '', '', '', 'active', 0),
(8, 'Abel Mangalo', '   ', '', '', '', '', '', '', 'active', 0),
(9, 'Able Plan Promotions', '   ', '', '', '', '', '', '', 'active', 0),
(10, 'Abraham Arellano', '   ', '', '', '', '', '', '', 'active', 0),
(11, 'Ace Penzionne', '   ', '', '', '', '', '', '', 'active', 0),
(12, 'Ace Rivera', '   ', '', '', '', '', '', '', 'active', 0),
(13, 'Acecolors Outdoor Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(14, 'Adela Eumague', '   ', '', '', '', '', '', '', 'active', 0),
(15, 'Adelaida Ocampo', 'n/a', '', '', '', '', '', '', 'active', 0),
(16, 'Adelina Velayo', '   ', '', '', '', '', '', '', 'active', 0),
(18, 'Aeschylus Rivera', '   ', '', '', '', '', '', '', 'active', 0),
(19, 'Agnes Dianne Dacera', '   ', '', '', '', '', '', '', 'active', 0),
(20, 'Agrifina Breviescas', '   ', '', '', '', '', '', '', 'active', 0),
(21, 'Ahn Kwan Su v', '   ', '', '', '', '', '', '', 'active', 0),
(22, 'Aidan Alburo', '   ', '', '', '', '', '', '', 'active', 0),
(23, 'Aiden Iky Chopdat', '   ', '', '', '', '', '', '', 'active', 0),
(24, 'Aikka Iñigo', '   ', '', '', '', '', '', '', 'active', 0),
(25, 'Aileen R. Secuya', '   ', '', '', '', '', '', '', 'active', 0),
(26, 'Ailen Hazel Oñes', '   ', '', '', '', '', '', '', 'active', 0),
(27, 'Ailen Moralde', '   ', '', '', '', '', '', '', 'active', 0),
(28, 'Ailyn Becalas v', '   ', '', '', '', '', '', '', 'active', 0),
(29, 'Aime Medillo', 'Mabolo, Cebu City   ', '788-500', '9183456689', '963-8900', 'Cool supplier', 'Urgent', 'Bigfoot', 'active', 0),
(30, 'Aimeh Ruiz', '   ', '', '', '', '', '', '', 'active', 0),
(31, 'Aisah Ronaville Ubanan', '   ', '', '', '', '', '', '', 'active', 0),
(32, 'Aivy Saliente', '   ', '', '', '', '', '', '', 'active', 0),
(33, 'Akina Suga v', '   ', '', '', '', '', '', '', 'active', 0),
(34, 'Alain Ralph Arabe', '   ', '', '', '', '', '', '', 'active', 0),
(35, 'Alan Clegg', '   ', '', '', '', '', '', '', 'active', 0),
(36, 'Alan Granada', '   ', '', '', '', '', '', '', 'active', 0),
(37, 'Alan T. Alvez', '   ', '', '', '', '', '', '', 'active', 0),
(38, 'Albatech Engineering', '   ', '', '', '', '', '', '', 'active', 0),
(39, 'Alel Mae Pacres', '   ', '', '', '', '', '', '', 'active', 0),
(40, 'Alfie Bryan M. Cañon', '   ', '', '', '', '', '', '', 'active', 0),
(41, 'Alfredo Plana', '   ', '', '', '', '', '', '', 'active', 0),
(42, 'Alipio Dompor', '   ', '', '', '', '', '', '', 'active', 0),
(43, 'Alisha Tiu', '   ', '', '', '', '', '', '', 'active', 0),
(44, 'Allan Comighud', '   ', '', '', '', '', '', '', 'active', 0),
(45, 'Allen Bercel Cabreros', '   ', '', '', '', '', '', '', 'active', 0),
(46, 'Alpha Sunshine Arias', '   ', '', '', '', '', '', '', 'active', 0),
(47, 'Alsme Chemicals Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(48, 'Alvarez Nuez Galang Espina Lopez', '   ', '', '', '', '', '', '', 'active', 0),
(49, 'Alvie Colegado', '   ', '', '', '', '', '', '', 'active', 0),
(50, 'Amelia Joy Buo', '   ', '', '', '', '', '', '', 'active', 0),
(51, 'Amella Joy Buo', '   ', '', '', '', '', '', '', 'active', 0),
(52, 'An-An and Bebie Boy Meat Store', '   ', '', '', '', '', '', '', 'active', 0),
(53, 'Ana Theresa Singcol', '   ', '', '', '', '', '', '', 'active', 0),
(54, 'Anabelle Nabong', '   ', '', '', '', '', '', '', 'active', 0),
(55, 'Anadel Sabas', '   ', '', '', '', '', '', '', 'active', 0),
(56, 'Analyn Villa', '   ', '', '', '', '', '', '', 'active', 0),
(57, 'Andons Merchandising', '   ', '', '', '', '', '15 days', '', 'active', 0),
(58, 'Andrea Dunlop', '   ', '', '', '', '', '', '', 'active', 0),
(59, 'Andrea Ernestine Gierza', '   ', '', '', '', '', '', '', 'active', 0),
(60, 'Andrew Imbong', '   ', '', '', '', '', '', '', 'active', 0),
(61, 'Andy Kim', '   ', '', '', '', '', '', '', 'active', 0),
(62, 'Anecita Gorre', '   ', '', '', '', '', '', '', 'active', 0),
(63, 'Angelyn Ceniza', '   ', '', '', '', '', '', '', 'active', 0),
(64, 'Anita Yrogirog', '   ', '', '', '', '', '', '', 'active', 0),
(65, 'Anjanette Guibijar', '   ', '', '', '', '', '', '', 'active', 0),
(66, 'Ann Driane Tampus', '   ', '', '', '', '', '', '', 'active', 0),
(67, 'Anna Lou Alvarico', '   ', '', '', '', '', '', '', 'active', 0),
(68, 'Anna Marie Lauron', '   ', '', '', '', '', '', '', 'active', 0),
(69, 'Annadelle Alajar', '   ', '', '', '', '', '', '', 'active', 0),
(70, 'Annaliza Merin', '   ', '', '', '', '', '', '', 'active', 0),
(71, 'Annie Loren Soquilon', '   ', '', '', '', '', '', '', 'active', 0),
(72, 'Annie Lou Laurel', '   ', '', '', '', '', '', '', 'active', 0),
(73, 'Annie Mae Nazareta', '   ', '', '', '', '', '', '', 'active', 0),
(74, 'Antero Montague Fairbanks L. Melgar', '   ', '', '', '', '', '', '', 'active', 0),
(75, 'Anthony Delmo', '   ', '', '', '', '', '', '', 'active', 0),
(76, 'Anthony Hall', '   ', '', '', '', '', '', '', 'active', 0),
(77, 'Anthony Villalvito', '   ', '', '', '', '', '', '', 'active', 0),
(78, 'Antonio A. Teves', '   ', '', '', '', '', '', '', 'active', 0),
(79, 'Apple Cuizon', '   ', '', '', '', '', '', '', 'active', 0),
(80, 'April Balbuena', '   ', '', '', '', '', '', '', 'active', 0),
(81, 'April Barong', '   ', '', '', '', '', '', '', 'active', 0),
(82, 'April Centino', '   ', '', '', '', '', '', '', 'active', 0),
(83, 'April Louise Ayaton', '   ', '', '', '', '', '', '', 'active', 0),
(84, 'April Love Baguio', '   ', '', '', '', '', '', '', 'active', 0),
(85, 'April Sabesaje', '   ', '', '', '', '', '', '', 'active', 0),
(86, 'Arcason & Sons Construction & Dev''t. Corp', '   ', '', '', '', '', '', '', 'active', 0),
(87, 'Archie Sungahid', '   ', '', '', '', '', '', '', 'active', 0),
(88, 'Argeline Orbiso', '   ', '', '', '', '', '', '', 'active', 0),
(89, 'Ariel Baga', '   ', '', '', '', '', '', '', 'active', 0),
(90, 'Aries Distributor', '   ', '', '', '', '', '', '', 'active', 0),
(91, 'Arlene Gallenero', '   ', '', '', '', '', '', '', 'active', 0),
(92, 'Arlene Lascoña', '   ', '', '', '', '', '', '', 'active', 0),
(93, 'Armie Labalan', '   ', '', '', '', '', '', '', 'active', 0),
(94, 'Armie Lenin Labalan', '   ', '', '', '', '', '', '', 'active', 0),
(95, 'Arminue Po', '   ', '', '', '', '', '', '', 'active', 0),
(96, 'Arnel Frias', '   ', '', '', '', '', '', '', 'active', 0),
(97, 'Arnel Ponce', '   ', '', '', '', '', '', '', 'active', 0),
(98, 'Arnel S. Frias', '   ', '', '', '', '', '', '', 'active', 0),
(99, 'Arnulfo B. Oplado', '   ', '', '', '', '', '', '', 'active', 0),
(100, 'Art Crone', '   ', '', '', '', '', '', '', 'active', 0),
(101, 'Artemia Degamo Meat Vendor', '   ', '', '', '', '', '', '', 'active', 0),
(102, 'Arthur Rosalem', '   ', '', '', '', '', '', '', 'active', 0),
(103, 'Arzenith Degollacion', '   ', '', '', '', '', '', '', 'active', 0),
(104, 'Asia Pacific Ads Unlimited', '   ', '', '', '', '', '', '', 'active', 0),
(105, 'Asia Pacific Marketing', 'Jose L. Briones Ave., North Reclamation,   ', '232-0402', '', '232-1022', '', '', 'Asia Pacific Marketing', 'active', 0),
(106, 'Assi Mart', '   ', '', '', '', '', '7 days', '', 'active', 0),
(107, 'Astara Mae Daga-as', '   ', '', '', '', '', '', '', 'active', 0),
(108, 'Athena Mae Ibon', '   ', '', '', '', '', '', '', 'active', 0),
(109, 'Athena Rocero', '   ', '', '', '', '', '', '', 'active', 0),
(110, 'Aubrey Angelique Quirante', '   ', '', '', '', '', '', '', 'active', 0),
(111, 'Audrey Rose Ymbong', '   ', '', '', '', '', '', '', 'active', 0),
(112, 'Aurora Cachapero', '   ', '', '', '', '', '', '', 'active', 0),
(113, 'Austria Parane', '   ', '', '', '', '', '', '', 'active', 0),
(114, 'B.E. Benitez Architects', '   ', '', '', '', '', '', '', 'active', 0),
(115, 'B.P. Deliarte Electrical Works', '   ', '', '', '', '', '', '', 'active', 0),
(116, 'Baby Lyn Alesquio', '   ', '', '', '', '', '', '', 'active', 0),
(117, 'Banco de Oro Unibank', '   ', '', '', '', '', '', '', 'active', 0),
(118, 'Barangay Mactan Treasurer', '   ', '', '', '', '', '', '', 'active', 0),
(119, 'BBC''s Printshop', '   ', '', '', '', '', '', '', 'active', 0),
(120, 'BBC''s Printstop', '   ', '', '', '', '', '', '', 'active', 0),
(121, 'Belen Hermosa', '   ', '', '', '', '', '', '', 'active', 0),
(122, 'Benjamin Dabalos', '   ', '', '', '', '', '', '', 'active', 0),
(123, 'Benjie P. Yu', '   ', '', '', '', '', '', '', 'active', 0),
(124, 'Bernadeth Calderon', '   ', '', '', '', '', '', '', 'active', 0),
(125, 'Berovan Marketing, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(126, 'Beverly Ann Segismar', '   ', '', '', '', '', '', '', 'active', 0),
(127, 'Beverly Lyn Yurong', '   ', '', '', '', '', '', '', 'active', 0),
(128, 'Bianca A. Hernandez', '   ', '', '', '', '', '', '', 'active', 0),
(129, 'Bienvenida Tacumba', '   ', '', '', '', '', '', '', 'active', 0),
(130, 'Bigbucks Coffee Shop, Inc.', 'F. Ramos St., Cogon Central Ceby City  ', '', '', '', '', '', '', 'active', 0),
(131, 'Bigfoot Capital Venture Funds, Inc. v', 'Bigfoot Center F. Ramos Street Cebu City 228-524-321-000 ', '', '', '', '', '', '', 'active', 0),
(132, 'Bigfoot Coffee Shop, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(133, 'Bigfoot Communications Phils.,Inc.', 'F. Ramos St. Cogon Central Cebu City 220-885-777-000 ', '', '', '', '', '', '', 'active', 0),
(134, 'Bigfoot Customer Care Systems, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(135, 'Bigfoot Entertainment', '   ', '', '', '', '', '', '', 'active', 0),
(136, 'Bigfoot Entertainment Phils., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(137, 'Bigfoot Global Solutions, Inc.', 'F. Ramos Center Cogon Street Cebu City 221-721-479-000 ', '', '', '', '', '', '', 'active', 0),
(138, 'Bigfoot Properties, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(139, 'Bigfoot Studios, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(140, 'BIR FAO CELI INC.', '   ', '', '', '', '', '', '', 'active', 0),
(141, 'Bisda Security Agency', '   ', '', '', '', '', '', '', 'active', 0),
(142, 'Blonde Anne Dy', '   ', '', '', '', '', '', '', 'active', 0),
(143, 'Blondelle Belongilot', '   ', '', '', '', '', '', '', 'active', 0),
(144, 'Blue-Eye Security & Investigation Agency', '   ', '', '', '', '', '', '', 'active', 0),
(145, 'Blue Bubbles Express Laundry Services', '   ', '', '', '', '', '', '', 'active', 0),
(146, 'Bob Yu', '   ', '', '', '', '', '', '', 'active', 0),
(147, 'Bon Heber Puracan', '   ', '', '', '', '', '', '', 'active', 0),
(148, 'Bonnie Gallardo', '   ', '', '', '', '', '', '', 'active', 0),
(149, 'Booking.com', '   ', '', '', '', '', '', '', 'active', 0),
(150, 'Boyla Dive Center', '   ', '', '', '', '', '', '', 'active', 0),
(151, 'Brenda Abucay', '   ', '', '', '', '', '', '', 'active', 0),
(152, 'Brian Daly', '   ', '', '', '', '', '', '', 'active', 0),
(153, 'Brian Dominic Padilla v', '   ', '', '', '', '', '', '', 'active', 0),
(154, 'Brianne Mae Belga', '   ', '', '', '', '', '', '', 'active', 0),
(155, 'Brinda Demeterio', '   ', '', '', '', '', '', '', 'active', 0),
(156, 'Britta Mary Oblan', '   ', '', '', '', '', '', '', 'active', 0),
(157, 'Bruce Cunningham', '   ', '', '', '', '', '', '', 'active', 0),
(158, 'Bryan Roy Salvador', '   ', '', '', '', '', '', '', 'active', 0),
(159, 'Brylle Castro', '   ', '', '', '', '', '', '', 'active', 0),
(160, 'Bui Thi Hong Nhung', '   ', '', '', '', '', '', '', 'active', 0),
(161, 'Business Processing Association Phils.', '   ', '', '', '', '', '', '', 'active', 0),
(162, 'Byun Hyun Ju', '   ', '', '', '', '', '', '', 'active', 0),
(163, 'Caesar Francis Lee', '   ', '', '', '', '', '', '', 'active', 0),
(164, 'Carleen Carampatan', '   ', '', '', '', '', '', '', 'active', 0),
(165, 'Carlife Booc', '   ', '', '', '', '', '', '', 'active', 0),
(166, 'Carlito Aguirre Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(167, 'Carlo Emmanuel Redoble', '   ', '', '', '', '', '', '', 'active', 0),
(168, 'Carmel Saluta', '   ', '', '', '', '', '', '', 'active', 0),
(169, 'Carmelo Mialda', '   ', '', '', '', '', '', '', 'active', 0),
(170, 'Carmelyn Grace Mellijor', '   ', '', '', '', '', '', '', 'active', 0),
(171, 'Carolyn Leizlee Lanawan', '   ', '', '', '', '', '', '', 'active', 0),
(172, 'Cassandra Leigh', '   ', '', '', '', '', '', '', 'active', 0),
(173, 'Catalina Rentals', '   ', '', '', '', '', '', '', 'active', 0),
(174, 'Cathay Hardware Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(175, 'Catherine Homecillo', '   ', '', '', '', '', '', '', 'active', 0),
(176, 'Catherine R. Sasam', '   ', '', '', '', '', '', '', 'active', 0),
(177, 'Catron Computer Technology', '   ', '', '', '', '', '', '', 'active', 0),
(178, 'Causeway Printers, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(179, 'Cebu A.I. Logic Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(180, 'Cebu AIDORG Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(181, 'Cebu Ass''n. of Pvt./Public TVET Ins.,Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(182, 'Cebu Beach Club', 'Mactan Island Lapu-lapu City  ', '', '', '', '', '', '', 'active', 0),
(183, 'Cebu City Treasurer Office', '   ', '', '', '', '', '', '', 'active', 0),
(184, 'Cebu Cube Ice', '   ', '', '', '', '', '', '', 'active', 0),
(185, 'Cebu Diamond Iron Works', '   ', '', '', '', '', '7 days', '', 'active', 0),
(186, 'Cebu Educational Supply', '   ', '', '', '', '', '', '', 'active', 0),
(187, 'Cebu General Services,Inc.', 'G/F Heng Bldg. Manuel L. Quezon Avenue, Casuntingan Mandaue City', '', '', '', '', '', '', 'active', 0),
(188, 'Cebu Holdings, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(189, 'Cebu Home and Builders Centre', '   ', '', '', '', '', '', '', 'active', 0),
(190, 'Cebu Island Tour Services', '   ', '', '', '', '', '', '', 'active', 0),
(191, 'Cebu Oversea Hardware Co., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(192, 'Cebu Pacific Air', '   ', '', '', '', '', '', '', 'active', 0),
(193, 'Cebu Progress Commercial', '   ', '', '', '', '', '', '', 'active', 0),
(194, 'Cebu Progress Marketing', 'Cor. Borromeo & P. Lopez STs. Cebu City 004-271-250-000V  ', '', '', '', '', '', '', 'active', 0),
(195, 'Cebu Tristar Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(196, 'Cebu Vascom Trading Corp.', 'Banilad Road Cebu City 081-004-269-624 VAT 3450727/416-2084 ', '', '', '', '', '', '', 'active', 0),
(197, 'Cecilia Guadalupe Corominas', '   ', '', '', '', '', '', '', 'active', 0),
(198, 'CELI Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(199, 'CELI Ltd.', '   ', '', '', '', '', '', '', 'active', 0),
(200, 'Celina Tan', '   ', '', '', '', '', '', '', 'active', 0),
(201, 'Cesar Evan N. Tiu', '   ', '', '', '', '', '', '', 'active', 0),
(202, 'Charina Sarte', '   ', '', '', '', '', '', '', 'active', 0),
(203, 'Charisma Construction Services/A.Lenaming', '   ', '', '', '', '', '', '', 'active', 0),
(204, 'Charissa Keith Padilla', '   ', '', '', '', '', '', '', 'active', 0),
(205, 'Charisse Guenever Sison', '   ', '', '', '', '', '', '', 'active', 0),
(206, 'Charlene Tito', '   ', '', '', '', '', '', '', 'active', 0),
(207, 'Charles Vailoces', '   ', '', '', '', '', '', '', 'active', 0),
(208, 'Charmine Catuburan', '   ', '', '', '', '', '', '', 'active', 0),
(209, 'Chateau de Carmen Hotel', '21 Juana Osmeña Street Cebu City  ', '255-63-66', '', '', '', '', '', 'active', 0),
(210, 'Cherry Ann Arrojado', '   ', '', '', '', '', '', '', 'active', 0),
(211, 'Cherry Mae A. Genesiran', '   ', '', '', '', '', '', '', 'active', 0),
(212, 'Cherry Mae Pabulayan', '   ', '', '', '', '', '', '', 'active', 0),
(213, 'Cherry Mae Sotto', '   ', '', '', '', '', '', '', 'active', 0),
(214, 'Cherry May Agbay', '   ', '', '', '', '', '', '', 'active', 0),
(215, 'Cherryl Gecera', '   ', '', '', '', '', '', '', 'active', 0),
(216, 'Cheryl Ortega', '   ', '', '', '', '', '', '', 'active', 0),
(217, 'Cheryl Rose Carayo', '   ', '', '', '', '', '', '', 'active', 0),
(218, 'Cheryll Rose A. Carayo', '   ', '', '', '', '', '', '', 'active', 0),
(219, 'Chi-Wen Hwang', '   ', '', '', '', '', '', '', 'active', 0),
(220, 'Chinebeth P. Borja', '   ', '', '', '', '', '', '', 'active', 0),
(221, 'Ching-Ching Luningning Tan', '   ', '', '', '', '', '', '', 'active', 0),
(222, 'Chiwen Huan', '   ', '', '', '', '', '', '', 'active', 0),
(223, 'Chloe Leulhati Barcelona', '   ', '', '', '', '', '', '', 'active', 0),
(224, 'Chong Moon Water Corp.', 'Nasipit Talamban Cebu City 236-999-896NV ', '344-1663', '', '', '', '', '', 'active', 0),
(225, 'Chony', '   ', '', '', '', '', '', '', 'active', 0),
(226, 'Chony Adamos', '   ', '', '', '', '', '', '', 'active', 0),
(227, 'Chrisitine Garcia', '   ', '', '', '', '', '', '', 'active', 0),
(228, 'Chrislann Catuburan', '   ', '', '', '', '', '', '', 'active', 0),
(229, 'Christabel S. Apas', '   ', '', '', '', '', '', '', 'active', 0),
(230, 'Christian James Cypres', '   ', '', '', '', '', '', '', 'active', 0),
(231, 'Christine Aura Ceniza', '   ', '', '', '', '', '', '', 'active', 0),
(232, 'Christine Joyce Gabrinez', '   ', '', '', '', '', '', '', 'active', 0),
(233, 'Christine Labata', '   ', '', '', '', '', '', '', 'active', 0),
(234, 'Christine Mae Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(235, 'Christine Marie Labata', '   ', '', '', '', '', '', '', 'active', 0),
(236, 'Christine Michelle Redoña', '   ', '', '', '', '', '', '', 'active', 0),
(237, 'Christine Rabe', '   ', '', '', '', '', '', '', 'active', 0),
(238, 'Christine Tajanlangit', '   ', '', '', '', '', '', '', 'active', 0),
(239, 'Christopher Delfino', '   ', '', '', '', '', '', '', 'active', 0),
(240, 'Christopher John Orongan', '   ', '', '', '', '', '', '', 'active', 0),
(241, 'Chrys Ocampo', '   ', '', '', '', '', '', '', 'active', 0),
(242, 'Ciara Baclayon', '   ', '', '', '', '', '', '', 'active', 0),
(243, 'Cindy Pamela E. Baron', '   ', '', '', '', '', '', '', 'active', 0),
(244, 'Citibank', '   ', '', '', '', '', '', '', 'active', 0),
(245, 'CITIBANK VISA FAO 248-000-0322013', '   ', '', '', '', '', '', '', 'active', 0),
(246, 'CITIBANK VISA FAO 4532-4800-0632-2013', '   ', '', '', '', '', '', '', 'active', 0),
(247, 'CITIBANK VISA FAO 4532-4872-0000-9270', '   ', '', '', '', '', '', '', 'active', 0),
(248, 'Citibank Visa FAO 4532480000322013', '   ', '', '', '', '', '', '', 'active', 0),
(249, 'City Treasure of Cebu', '   ', '', '', '', '', '', '', 'active', 0),
(250, 'City Treasurer of Lapu-Lapu', '   ', '', '', '', '', '', '', 'active', 0),
(251, 'Claire Dipay', '   ', '', '', '', '', '', '', 'active', 0),
(252, 'Clara Villaronza', '   ', '', '', '', '', '', '', 'active', 0),
(253, 'Clarice Armian', '   ', '', '', '', '', '', '', 'active', 0),
(254, 'Clariffel Marie Dinglasa', '   ', '', '', '', '', '', '', 'active', 0),
(255, 'Clarissa Joy Beton', '   ', '', '', '', '', '', '', 'active', 0),
(256, 'Clarita Villacote', '   ', '', '', '', '', '', '', 'active', 0),
(257, 'Classicware Eximport Co., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(258, 'Claudette Blanca Bascon', '   ', '', '', '', '', '', '', 'active', 0),
(259, 'Cleverlearn English Language Ltd.', '   ', '', '', '', '', '', '', 'active', 0),
(260, 'Clifford Kyle Polancos', '   ', '', '', '', '', '', '', 'active', 0),
(261, 'Color Acuity International, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(262, 'Connielyn Martus', '   ', '', '', '', '', '', '', 'active', 0),
(263, 'Country Fresh Trading', 'Suite 210, Crown Bldg. Road North 6 Reclamation Area Cebu City ', '233-0195', '', '', '121-756-119-000', '', '', 'active', 0),
(264, 'Crispen Dela Cruz', '   ', '', '', '', '', '', '', 'active', 0),
(265, 'Crispin Dela Cruz', '   ', '', '', '', '', '', '', 'active', 0),
(266, 'Cristopher Ofril', '   ', '', '', '', '', '', '', 'active', 0),
(267, 'Crown Regency Suites', 'Maximo Patalinghug Jr. Avenue Lapu-lapu Cebu  ', '341-4586-94', '', '', '', '', '', 'active', 0),
(268, 'Cycyl Birog', '   ', '', '', '', '', '', '', 'active', 0),
(269, 'Cynde Rosario Malayo', '   ', '', '', '', '', '', '', 'active', 0),
(270, 'Cynthia Vasquez', '   ', '', '', '', '', '', '', 'active', 0),
(271, 'Cyril John Remonde', '   ', '', '', '', '', '', '', 'active', 0),
(272, 'Daisuke Hotta v', '   ', '', '', '', '', '', '', 'active', 0),
(273, 'Daisy Yu', '   ', '', '', '', '', '', '', 'active', 0),
(274, 'Dan Ryan Gasataya', '   ', '', '', '', '', '', '', 'active', 0),
(275, 'Daniel Malaki', '   ', '', '', '', '', '', '', 'active', 0),
(276, 'Daniella Ma. Milagros Escoto', '   ', '', '', '', '', '', '', 'active', 0),
(277, 'Danielle Ma. Milagros Escoto', '   ', '', '', '', '', '', '', 'active', 0),
(278, 'Dao Duy Thang', '   ', '', '', '', '', '', '', 'active', 0),
(279, 'Darl Carmel Lee Lopez', '   ', '', '', '', '', '', '', 'active', 0),
(280, 'David Aaron Jenkins', '   ', '', '', '', '', '', '', 'active', 0),
(281, 'David Garrison', '   ', '', '', '', '', '', '', 'active', 0),
(282, 'David Page', '   ', '', '', '', '', '', '', 'active', 0),
(283, 'David Wiiliams', '   ', '', '', '', '', '', '', 'active', 0),
(284, 'David Williams', '   ', '', '', '', '', '', '', 'active', 0),
(285, 'Dax Jordan A. Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(286, 'Days Hotel', '   ', '', '', '', '', '', '', 'active', 0),
(287, 'Days Hotel Philippines, Inc', 'Airport Road Matumbo Pusok Hills Lapu-lapu City  ', '', '', '', '', '', '', 'active', 0),
(288, 'Delahne Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(289, 'Demnah Dalde', '   ', '', '', '', '', '', '', 'active', 0),
(290, 'Den Stevie Amodia', '   ', '', '', '', '', '', '', 'active', 0),
(291, 'Dennis Dalanon', '   ', '', '', '', '', '', '', 'active', 0),
(292, 'Dennis Dominquez', '   ', '', '', '', '', '', '', 'active', 0),
(293, 'Dennis Grimalt', '   ', '', '', '', '', '', '', 'active', 0),
(294, 'Dept. of Trade & Industry Securities & Ex', '   ', '', '', '', '', '', '', 'active', 0),
(295, 'Dervis Senica', '   ', '', '', '', '', '', '', 'active', 0),
(296, 'Desert Spring Trading', '   ', '', '', '', '', '', '', 'active', 0),
(297, 'Desiree Ann Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(298, 'Desiree May Ancheta', '   ', '', '', '', '', '', '', 'active', 0),
(299, 'Dheb Muit', '   ', '', '', '', '', '', '', 'active', 0),
(300, 'DIACOMM Communications Equipment', '   ', '', '', '', '', '', '', 'active', 0),
(301, 'Diacomm Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(302, 'Diamond Trading & Services', 'F.U. Bldg A.S. Fortuna St. Bakilid Mandaue City TIN 181-109-883-000 ', '', '', '', '', '', '', 'active', 0),
(303, 'Digitel Mobile Phils., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(304, 'Dina Gorgonia', '   ', '', '', '', '', '', '', 'active', 0),
(305, 'Dina Joanna Lao', '   ', '', '', '', '', '', '', 'active', 0),
(306, 'Dinah Laguerta', '   ', '', '', '', '', '', '', 'active', 0),
(307, 'Diona Lorraine Malaque', '   ', '', '', '', '', '', '', 'active', 0),
(308, 'Dionaly Fajardo', '   ', '', '', '', '', '', '', 'active', 0),
(309, 'Diplomat Hotel', '   ', '', '', '', '', '', '', 'active', 0),
(310, 'Divina Allanic', '   ', '', '', '', '', '', '', 'active', 0),
(311, 'Do Young Kim', '   ', '', '', '', '', '', '', 'active', 0),
(312, 'Dodong Belen Meat Shop', '   ', '', '', '', '', '', '', 'active', 0),
(313, 'Dominador Mimis', '   ', '', '', '', '', '', '', 'active', 0),
(314, 'Donabelle Diwatin', '   ', '', '', '', '', '', '', 'active', 0),
(315, 'dotPH Domains, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(316, 'Dr. Ignacio M. Cortes General Hospital', '   ', '', '', '', '', '', '', 'active', 0),
(317, 'DuPRINTER PHILS., INC.', '   ', '', '', '', '', '', '', 'active', 0),
(318, 'Earl Mullen', '   ', '', '', '', '', '', '', 'active', 0),
(319, 'East Capitol Pensionne', 'Capitol, Cebu City   ', '', '', '', '', '', '', 'active', 0),
(320, 'Easy Gas Convenience Store', '   ', '', '', '', '', '', '', 'active', 0),
(321, 'Echo Electrical Supply Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(322, 'ECO-TECH Appliance Circle Inc', '   ', '', '', '', '', '', '', 'active', 0),
(323, 'Edcon Glass and Aluminum,Inc.', 'H.Cortes St. Mandaue City 220-111-091-000  ', '', '', '', '', '', '', 'active', 0),
(324, 'Eddisio Pepito', '   ', '', '', '', '', '', '', 'active', 0),
(325, 'Edgar Anuada', '   ', '', '', '', '', '', '', 'active', 0),
(326, 'Edgardo Amoin', '   ', '', '', '', '', '', '', 'active', 0),
(327, 'edna Pacaña', '   ', '', '', '', '', '', '', 'active', 0),
(328, 'Edna Remulta', '   ', '', '', '', '', '', '', 'active', 0),
(329, 'Edna Tiongson', '   ', '', '', '', '', '', '', 'active', 0),
(330, 'Edsel Muzares', '   ', '', '', '', '', '', '', 'active', 0),
(331, 'eduardo Piañar', '   ', '', '', '', '', '', '', 'active', 0),
(332, 'Eduardo T. Cabigas', '   ', '', '', '', '', '', '', 'active', 0),
(333, 'Edward Mangaron', '   ', '', '', '', '', '', '', 'active', 0),
(334, 'Eileen Joy Cola', '   ', '', '', '', '', '', '', 'active', 0),
(335, 'Eiress Lugod', '   ', '', '', '', '', '', '', 'active', 0),
(336, 'Elaine Alix', '   ', '', '', '', '', '', '', 'active', 0),
(337, 'Elbe Ackman', '   ', '', '', '', '', '', '', 'active', 0),
(338, 'Elbe Akman', '   ', '', '', '', '', '', '', 'active', 0),
(339, 'Elbert Villariza', '   ', '', '', '', '', '', '', 'active', 0),
(340, 'Eleanor Ouano', '   ', '', '', '', '', '', '', 'active', 0),
(341, 'Electroworld Ayala', '   ', '', '', '', '', '', '', 'active', 0),
(342, 'Elena Freire', '   ', '', '', '', '', '', '', 'active', 0),
(343, 'Elenita Ayangco', '   ', '', '', '', '', '', '', 'active', 0),
(344, 'Elibeth Cozo', '   ', '', '', '', '', '', '', 'active', 0),
(345, 'Eliseo Damalerio', '   ', '', '', '', '', '', '', 'active', 0),
(346, 'Elite Machines', 'Espiritu Bldg. 33 Gen Maxilom Ave. Cebu City 005-679-485-001 ', '', '', '', '', '', '', 'active', 0),
(347, 'Elizabeth Gasco', '   ', '', '', '', '', '', '', 'active', 0),
(348, 'Elizabeth Mendoza', '   ', '', '', '', '', '', '', 'active', 0),
(349, 'Elizabeth Telen', '   ', '', '', '', '', '', '', 'active', 0),
(350, 'Elnie Abrigondo', '   ', '', '', '', '', '', '', 'active', 0),
(351, 'Elnora Jacaban', '   ', '', '', '', '', '', '', 'active', 0),
(352, 'Elorie Jo-anah Montilla', '   ', '', '', '', '', '', '', 'active', 0),
(353, 'Elsa Lim', '   ', '', '', '', '', '', '', 'active', 0),
(354, 'Elsie Dalaguan', '   ', '', '', '', '', '', '', 'active', 0),
(355, 'Emcor Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(356, 'Emergency Rescue Unit Foundation', '   ', '', '', '', '', '', '', 'active', 0),
(357, 'Emily Lawas', '   ', '', '', '', '', '', '', 'active', 0),
(358, 'Emily Tatoy', '   ', '', '', '', '', '', '', 'active', 0),
(359, 'Emma Saldua', '   ', '', '', '', '', '', '', 'active', 0),
(360, 'Emmanuel Pateña', '   ', '', '', '', '', '', '', 'active', 0),
(361, 'Emmylou Laurito', '   ', '', '', '', '', '', '', 'active', 0),
(362, 'Emo Road Runner Enterpises', '   ', '', '', '', '', '', '', 'active', 0),
(363, 'Erika Michell Entrampas', '   ', '', '', '', '', '', '', 'active', 0),
(364, 'Erma Juaton', '   ', '', '', '', '', '', '', 'active', 0),
(365, 'Ernest Villariza', '   ', '', '', '', '', '', '', 'active', 0),
(366, 'Ernesto Caballes Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(367, 'Ernie Jun Actub', '   ', '', '', '', '', '', '', 'active', 0),
(368, 'Erwin Diaz', '   ', '', '', '', '', '', '', 'active', 0),
(369, 'Esacar Agbay, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(370, 'Esmeraldo Camilo', '   ', '', '', '', '', '', '', 'active', 0),
(371, 'Esmeraldo Radam', '   ', '', '', '', '', '', '', 'active', 0),
(372, 'Estala Vianney Tiongco', '   ', '', '', '', '', '', '', 'active', 0),
(373, 'Estrella Igot', '   ', '', '', '', '', '', '', 'active', 0),
(374, 'Eulalia Sagnoy', '   ', '', '', '', '', '', '', 'active', 0),
(375, 'Eve Abalos', '   ', '', '', '', '', '', '', 'active', 0),
(376, 'Evelyn Juaneza', '   ', '', '', '', '', '', '', 'active', 0),
(377, 'Faida Bojos', '   ', '', '', '', '', '', '', 'active', 0),
(378, 'Faith Evangeline Jamandre', '   ', '', '', '', '', '', '', 'active', 0),
(379, 'Faithfully Travel    v', '   ', '', '', '', '', '', '', 'active', 0),
(380, 'Farrah Faye Fariola', '   ', '', '', '', '', '', '', 'active', 0),
(381, 'Fast Net Development Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(382, 'FASTTRACK SOLUTIONS , INC.', '   ', '', '', '', '', '', 'FASTTRACK SOLUTIONS , INC.', 'active', 0),
(383, 'Fatimah Ah-Dawia Apalla', '   ', '', '', '', '', '', '', 'active', 0),
(384, 'Federico Albacite, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(385, 'Fedie Painting Contractor/Fedelito Mata', '   ', '', '', '', '', '', '', 'active', 0),
(386, 'Fel Louise Alingasa', '   ', '', '', '', '', '', '', 'active', 0),
(387, 'Felipe Jose Sarmiento', '   ', '', '', '', '', '', '', 'active', 0),
(388, 'Flerida Ho', '   ', '', '', '', '', '', '', 'active', 0),
(389, 'Flora Mae D. Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(390, 'Floricor Toledo v', '   ', '', '', '', '', '', '', 'active', 0),
(391, 'Floro Jacaban', '   ', '', '', '', '', '', '', 'active', 0),
(392, 'Flory Mae Salintao', '   ', '', '', '', '', '', '', 'active', 0),
(393, 'Fontini Christi Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(394, 'Fooda Saversmart Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(395, 'foods', '   ', '', '', '', '', '', '', 'active', 0),
(396, 'Fotoline Express Incorporated', '   ', '', '', '', '', '', '', 'active', 0),
(397, 'Frameworkx Incorporated', '   ', '', '', '', '', '', '', 'active', 0),
(398, 'Frances Gayle Esimos', '   ', '', '', '', '', '', '', 'active', 0),
(399, 'Francis Caesar Lee', '   ', '', '', '', '', '', '', 'active', 0),
(400, 'Francis Te', '   ', '', '', '', '', '', '', 'active', 0),
(401, 'Francisco Mari Flores', '   ', '', '', '', '', '', '', 'active', 0),
(402, 'Franzina Sol-Gae Jose', '   ', '', '', '', '', '', '', 'active', 0),
(403, 'Fredie Auditor', '   ', '', '', '', '', '', '', 'active', 0),
(404, 'Freya Lynn Mari', '   ', '', '', '', '', '', '', 'active', 0),
(405, 'Freya Lynn Mari Bitas', '   ', '', '', '', '', '', '', 'active', 0),
(406, 'Fujimart Cebu, Inc.', '   ', '', '', '', '', 'Due on receipt', '', 'active', 0),
(407, 'Fukumi Tam', '   ', '', '', '', '', '', '', 'active', 0),
(408, 'Fulvio Calamba', '   ', '', '', '', '', '', '', 'active', 0),
(409, 'Fun & Sun', '   ', '', '', '', '', '', '', 'active', 0),
(410, 'Gacil Larino', '   ', '', '', '', '', '', '', 'active', 0),
(411, 'Gaisano Capital Mactan', '   ', '', '', '', '', '', '', 'active', 0),
(412, 'GAKKEN (Philippines). Inc.', '   ', '', '', '', '', '30 days', '', 'active', 0),
(413, 'Gardens r'' Us, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(414, 'Garry Lou Tenchavez', '   ', '', '', '', '', '', '', 'active', 0),
(415, 'Gary Maxwell', '   ', '', '', '', '', '', '', 'active', 0),
(416, 'Gasera Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(417, 'Gemmarie Dinglasa', '   ', '', '', '', '', '', '', 'active', 0),
(418, 'GENDIESEL Philippines, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(419, 'Genelyn Elveña', '   ', '', '', '', '', '', '', 'active', 0),
(420, 'Genevieve Fuentes', '   ', '', '', '', '', '', '', 'active', 0),
(421, 'Gennet Chiong', '   ', '', '', '', '', '', '', 'active', 0),
(422, 'Genson Surveying &Engineering Consultants', '   ', '', '', '', '', '', '', 'active', 0),
(423, 'Geraldine Olita', '   ', '', '', '', '', '', '', 'active', 0),
(424, 'Geramie Lagumbay', '   ', '', '', '', '', '', '', 'active', 0),
(425, 'Gerard Paul Niño Vergara', '   ', '', '', '', '', '', '', 'active', 0),
(426, 'Gina Francisco', '   ', '', '', '', '', '', '', 'active', 0),
(427, 'Ginebra San Miguel, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(428, 'Girlie Bacay', '   ', '', '', '', '', '', '', 'active', 0),
(429, 'Giselle Ann Lagarnia', '   ', '', '', '', '', '', '', 'active', 0),
(430, 'Glacierview Realty, Inc.', 'Tuplips Center A.S. Fortuna Bakilid Mandaue City  ', '344-7726/344-7727', '', '', '', '', '', 'active', 0),
(431, 'Glenda Garcia', '   ', '', '', '', '', '', '', 'active', 0),
(432, 'Gliceria Alinsonorin/A.G. Copier Ctr', 'F. Ramos St. Cogon Cebu City TIN: 142-898-318-003', '', '', '', '', '', '', 'active', 0),
(433, 'Gloria Olitres', '   ', '', '', '', '', '', '', 'active', 0),
(434, 'Golda Dazo', '   ', '', '', '', '', '', '', 'active', 0),
(435, 'Goldberg Printing Services', '   ', '', '', '', '', '', '', 'active', 0),
(436, 'Golden Cypress Water Co.,Ltd.', '   ', '', '', '', '', '', '', 'active', 0),
(437, 'Gomer Bongcac', '   ', '', '', '', '', '', '', 'active', 0),
(438, 'Good Earth Water Refilling Station', '   ', '', '', '', '', '', '', 'active', 0),
(439, 'Gotta Wash It Studio', '   ', '', '', '', '', '', '', 'active', 0),
(440, 'Grace Augusto', '   ', '', '', '', '', '', '', 'active', 0),
(441, 'Grace Cantero', '   ', '', '', '', '', '', '', 'active', 0),
(442, 'Grace Maglines', '   ', '', '', '', '', '', '', 'active', 0),
(443, 'Grace Marie Albio', '   ', '', '', '', '', '', '', 'active', 0),
(444, 'Grace Tango-an', '   ', '', '', '', '', '', '', 'active', 0),
(445, 'Grand Holidays, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(446, 'Graphic Star', '   ', '', '', '', '', '', '', 'active', 0),
(447, 'Great Eastern Commercial', '   ', '', '', '', '', '', '', 'active', 0),
(448, 'Green Leaf Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(449, 'Gretchen Tongco', '   ', '', '', '', '', '', '', 'active', 0),
(450, 'Gritzie Fulguirinas', '   ', '', '', '', '', '', '', 'active', 0),
(451, 'Guada Corominas', '   ', '', '', '', '', '', '', 'active', 0),
(452, 'Guendolyn Templado', '   ', '', '', '', '', '', '', 'active', 0),
(453, 'Gullem Metal Shop/Richard Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(454, 'Gywn Jomuad', '   ', '', '', '', '', '', '', 'active', 0),
(455, 'Haney Lynn Siclot', '   ', '', '', '', '', '', '', 'active', 0),
(456, 'Hardinado Patnugot III', '   ', '', '', '', '', '', '', 'active', 0),
(457, 'Hardrock General Contractors', '   ', '', '', '', '', '', '', 'active', 0),
(458, 'Hazel Catherine Pagunsan', '   ', '', '', '', '', '', '', 'active', 0),
(459, 'Hazel Daño', '   ', '', '', '', '', '', '', 'active', 0),
(460, 'Hazel Kiamco', '   ', '', '', '', '', '', '', 'active', 0),
(461, 'Hazel Tan', '   ', '', '', '', '', '', '', 'active', 0),
(462, 'Heart Pedroza', '   ', '', '', '', '', '', '', 'active', 0),
(463, 'Helen May Mullet', '   ', '', '', '', '', '', '', 'active', 0),
(464, 'Henjel Bardoquillo', '   ', '', '', '', '', '', '', 'active', 0),
(465, 'Henny Fer Marie Cañete', '   ', '', '', '', '', '', '', 'active', 0),
(466, 'Henry Gerard Reynes', '   ', '', '', '', '', '', '', 'active', 0),
(467, 'Henry Peterson', '   ', '', '', '', '', '', '', 'active', 0),
(468, 'HITECH Supplies & Packaging', '   ', '', '', '', '', '30 days', '', 'active', 0),
(469, 'Holiday Plaza Hotel', 'F Ramos St. Cebu City   ', '', '', '', '', '', '', 'active', 0),
(470, 'Home Development Mutual Fund', '   ', '', '', '', '', '', '', 'active', 0),
(471, 'Home Options, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(472, 'Home Plan Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(473, 'Hope Taboada', '   ', '', '', '', '', '', '', 'active', 0),
(474, 'Hwang Ho In v', '   ', '', '', '', '', '', '', 'active', 0),
(475, 'Hye Kim Kyoung (Tessie)', '   ', '', '', '', '', '', '', 'active', 0),
(476, 'Hygeia Valerie Abellanosa', '   ', '', '', '', '', '', '', 'active', 0),
(477, 'IAFT-CELI', '   ', '', '', '', '', '', '', 'active', 0),
(478, 'Ian Carlos Go', '   ', '', '', '', '', '', '', 'active', 0),
(479, 'Ian Graciano Villacera', '   ', '', '', '', '', '', '', 'active', 0),
(480, 'Ian Lambojon', '   ', '', '', '', '', '', '', 'active', 0),
(481, 'Ilene Angelitud', '   ', '', '', '', '', '', '', 'active', 0),
(482, 'Inca Plastics Philippines, Inc.', 'Unit 2 NGS Bldg GoSotto Compound M.J. Cuenco St Cebu City 000-12-172-001V', '', '', '', '', '', '', 'active', 0),
(483, 'Indiana Aerospace Univ. (IAU) Foundation', '   ', '', '', '', '', '', '', 'active', 0),
(484, 'Industrial Inspection (International),Inc', '3-D Gill Tudtud St. Mabolo Cebu City   ', '', '', '', '', '', '', 'active', 0),
(485, 'Innove Communications, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(486, 'Int''l. Academy of Film & Television, Inc.', 'Bigfoot IT & Media Park, Lapu-lapu city   ', '', '', '', '', '', '', 'active', 0),
(487, 'International Pharmaceuticals, Inc.', '   ', '', '', '', '', '30 days', '', 'active', 0),
(488, 'Invincible Armada Secuirity Agency', '   ', '', '', '', '', '', '', 'active', 0),
(489, 'Ionie Guazon', '   ', '', '', '', '', '', '', 'active', 0),
(490, 'Ionne Llenes', '   ', '', '', '', '', '', '', 'active', 0),
(491, 'Iorena Nabe Vergara', '   ', '', '', '', '', '', '', 'active', 0),
(492, 'Iori Yoshida', '   ', '', '', '', '', '', '', 'active', 0),
(493, 'Irah Miraflor', '   ', '', '', '', '', '', '', 'active', 0),
(494, 'Irene Angelitud', '   ', '', '', '', '', '', '', 'active', 0),
(495, 'Irene Jane Geniston', '   ', '', '', '', '', '', '', 'active', 0),
(496, 'Irene Palacio', '   ', '', '', '', '', '', '', 'active', 0),
(497, 'Iris Hope De Los Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(498, 'Irish Gonzales', '   ', '', '', '', '', '', '', 'active', 0),
(499, 'Irish Jao', '   ', '', '', '', '', '', '', 'active', 0),
(500, 'Irish Lorraine Miraflor', '   ', '', '', '', '', '', '', 'active', 0),
(501, 'Irish Mae Toring', '   ', '', '', '', '', '', '', 'active', 0),
(502, 'Islands Souvenirs, Inc', '   ', '', '', '', '', '', '', 'active', 0),
(503, 'Israel Ken Zacal', '   ', '', '', '', '', '', '', 'active', 0),
(504, 'Ivy Catagcatag', '   ', '', '', '', '', '', '', 'active', 0),
(505, 'Iwojima Arcipe', '   ', '', '', '', '', '', '', 'active', 0),
(506, 'J I G Fish Dealer', '   ', '', '', '', '', '', '', 'active', 0),
(507, 'J. King & Sons Company, Inc.', 'Holy Name Street Mabolo Cebu city   ', '', '', '', '', '', '', 'active', 0),
(508, 'Jack Zhang', '   ', '', '', '', '', '', '', 'active', 0),
(509, 'Jackielou Rocamura', '   ', '', '', '', '', '', '', 'active', 0),
(510, 'Jacob''s Basic First Ventures', '   ', '', '', '', '', '', '', 'active', 0),
(511, 'Jacqueline Canoy', '   ', '', '', '', '', '', '', 'active', 0),
(512, 'Jake Daro', '   ', '', '', '', '', '', '', 'active', 0),
(513, 'James Debellis', '   ', '', '', '', '', '', '', 'active', 0),
(514, 'Jan- Michael Niño Buhay', '   ', '', '', '', '', '', '', 'active', 0),
(515, 'Jancy Paz Basubas', '   ', '', '', '', '', '', '', 'active', 0),
(516, 'Jane Rachel Riconalla', '   ', '', '', '', '', '', '', 'active', 0),
(517, 'Jane Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(518, 'Janice Lucille Villamor', '   ', '', '', '', '', '', '', 'active', 0),
(519, 'Janice Lucille Virtucio', '   ', '', '', '', '', '', '', 'active', 0),
(520, 'Jay''s Square Bakeshop', '   ', '', '', '', '', '', '', 'active', 0),
(521, 'Jay C. Designs, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(522, 'Jay Yrogirog', '   ', '', '', '', '', '', '', 'active', 0),
(523, 'Jayme Electronics & Refrigeration Service', '   ', '', '', '', '', '', '', 'active', 0),
(524, 'Jayziel Palopalo', '   ', '', '', '', '', '', '', 'active', 0),
(525, 'JCAMP General Merchandise', '   ', '', '', '', '', '', '', 'active', 0),
(526, 'Jeane Evangelista', '   ', '', '', '', '', '', '', 'active', 0),
(527, 'Jecyl Malazarte', '   ', '', '', '', '', '', '', 'active', 0),
(528, 'Jed Libby Tadlas', '   ', '', '', '', '', '', '', 'active', 0),
(529, 'Jefferson Murillo', '   ', '', '', '', '', '', '', 'active', 0),
(530, 'Jegger Rasonable', '   ', '', '', '', '', '', '', 'active', 0),
(531, 'Jell-Ann Noynay', '   ', '', '', '', '', '', '', 'active', 0),
(532, 'Jelsan Peñada', '   ', '', '', '', '', '', '', 'active', 0),
(533, 'Jemary Rose Gualiza', '   ', '', '', '', '', '', '', 'active', 0),
(534, 'Jennefer Tumulak', '   ', '', '', '', '', '', '', 'active', 0),
(535, 'Jennifer Andres', '   ', '', '', '', '', '', '', 'active', 0),
(536, 'Jennifer Basan', '   ', '', '', '', '', '', '', 'active', 0),
(537, 'Jennifer Blase', '   ', '', '', '', '', '', '', 'active', 0),
(538, 'Jennifer Tahinay', '   ', '', '', '', '', '', '', 'active', 0),
(539, 'Jeramel Mingueto', '   ', '', '', '', '', '', '', 'active', 0),
(540, 'Jerelyn Wagwag', '   ', '', '', '', '', '', '', 'active', 0),
(541, 'Jerill Galleon', '   ', '', '', '', '', '', '', 'active', 0),
(542, 'Jermaine Gara', '   ', '', '', '', '', '', '', 'active', 0),
(543, 'Jermeine Abapo', '   ', '', '', '', '', '', '', 'active', 0),
(544, 'Jerome Ramirez', '   ', '', '', '', '', '', '', 'active', 0),
(545, 'Jerome Sabac', '   ', '', '', '', '', '', '', 'active', 0),
(546, 'Jesa A. Gonido', '   ', '', '', '', '', '', '', 'active', 0),
(547, 'Jeser Plotinia', '   ', '', '', '', '', '', '', 'active', 0),
(548, 'Jesha Tejero', '   ', '', '', '', '', '', '', 'active', 0),
(549, 'Jessa Capadiso', '   ', '', '', '', '', '', '', 'active', 0),
(550, 'Jessica Fuentes', '   ', '', '', '', '', '', '', 'active', 0),
(551, 'Jessicca Gulane', '   ', '', '', '', '', '', '', 'active', 0),
(552, 'Jessie Carangue', '   ', '', '', '', '', '', '', 'active', 0),
(553, 'Jesster Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(554, 'Jessterads', '   ', '', '', '', '', '', '', 'active', 0),
(555, 'Jestoni Empas', '   ', '', '', '', '', '', '', 'active', 0),
(556, 'JGL Cable and Electrical Services', '   ', '', '', '', '', '', '', 'active', 0),
(557, 'JH Bread Baker Bakeshop', '   ', '', '', '', '', '', '', 'active', 0),
(558, 'Jhonrey Carambias', '   ', '', '', '', '', '', '', 'active', 0),
(559, 'Jimbo Ramirez', '   ', '', '', '', '', '', '', 'active', 0),
(560, 'JJED Philippines Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(561, 'JKY Food Ventures', '   ', '', '', '', '', '7 days', '', 'active', 0),
(562, 'Joan Clavete', '   ', '', '', '', '', '', '', 'active', 0),
(563, 'Joan Eve Lomoljo', '   ', '', '', '', '', '', '', 'active', 0),
(564, 'Joan Marie Quimno', '   ', '', '', '', '', '', '', 'active', 0),
(565, 'Joan Tampus', '   ', '', '', '', '', '', '', 'active', 0),
(566, 'Joan Torreon', '   ', '', '', '', '', '', '', 'active', 0),
(567, 'Joanna Fye Trajico', '   ', '', '', '', '', '', '', 'active', 0),
(568, 'Joanne Cabarles', '   ', '', '', '', '', '', '', 'active', 0),
(569, 'Joanne Castillote', '   ', '', '', '', '', '', '', 'active', 0),
(570, 'Joanne Espina', '   ', '', '', '', '', '', '', 'active', 0),
(571, 'Joanne Rose Bionat', '   ', '', '', '', '', '', '', 'active', 0),
(572, 'Job Paul Cereño', '   ', '', '', '', '', '', '', 'active', 0),
(573, 'Jobs DB Philippines, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(574, 'Jobstreet.com Philippines Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(575, 'Jocel Arong', '   ', '', '', '', '', '', '', 'active', 0),
(576, 'Jocel Bajao', '   ', '', '', '', '', '', '', 'active', 0),
(577, 'Joe Keehan', '   ', '', '', '', '', '', '', 'active', 0),
(578, 'Joebert Apiladas', '   ', '', '', '', '', '', '', 'active', 0),
(579, 'Joevanne Te', '   ', '', '', '', '', '', '', 'active', 0),
(580, 'Joey Tecson', '   ', '', '', '', '', '', '', 'active', 0),
(581, 'Johannah Lunor', '   ', '', '', '', '', '', '', 'active', 0),
(582, 'Johannah Tupas', '   ', '', '', '', '', '', '', 'active', 0),
(583, 'John Alexander Macdonald', '   ', '', '', '', '', '', '', 'active', 0),
(584, 'John Carlo Velasco', '   ', '', '', '', '', '', '', 'active', 0),
(585, 'John Cawley', '   ', '', '', '', '', '', '', 'active', 0),
(586, 'John Derstine', '   ', '', '', '', '', '', '', 'active', 0),
(587, 'John Henry Montebon', '   ', '', '', '', '', '', '', 'active', 0),
(588, 'John Joseph Mercado v', '   ', '', '', '', '', '', '', 'active', 0),
(589, 'John Lawrence Mercado-AP-others', '   ', '', '', '', '', '', '', 'active', 0),
(590, 'John Lawrence Mercado v', '   ', '', '', '', '', '', '', 'active', 0),
(591, 'John Marc Salantandre', '   ', '', '', '', '', '', '', 'active', 0),
(592, 'John Mark De Los Santos', '   ', '', '', '', '', '', '', 'active', 0),
(593, 'John Paul Espejon', '   ', '', '', '', '', '', '', 'active', 0),
(594, 'John Ray Gador', '   ', '', '', '', '', '', '', 'active', 0),
(595, 'Joice Maissa Fernandez', '   ', '', '', '', '', '', '', 'active', 0),
(596, 'Jolie Ann Cenas', '   ', '', '', '', '', '', '', 'active', 0),
(597, 'Jollibee Foods Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(598, 'Joma Valor Echavez', '   ', '', '', '', '', '', '', 'active', 0),
(599, 'Jon-Paul Raymond', '   ', '', '', '', '', '', '', 'active', 0),
(600, 'Jona Ville Dugaduga', '   ', '', '', '', '', '', '', 'active', 0),
(601, 'Jonabeth Minoza', '   ', '', '', '', '', '', '', 'active', 0),
(602, 'Jonah Marie Alegre', '   ', '', '', '', '', '', '', 'active', 0),
(603, 'Jonathan Cabilao', '   ', '', '', '', '', '', '', 'active', 0),
(604, 'Jonathan Casio', '   ', '', '', '', '', '', '', 'active', 0),
(605, 'Joniefer Ligas', '   ', '', '', '', '', '', '', 'active', 0),
(606, 'Jonnabel Ibanez', '   ', '', '', '', '', '', '', 'active', 0),
(607, 'Jonny Ong', '   ', '', '', '', '', '', '', 'active', 0),
(608, 'Joselito Baena', '   ', '', '', '', '', '', '', 'active', 0),
(609, 'Joselito Tabon', '   ', '', '', '', '', '', '', 'active', 0),
(610, 'Joselo Atup', '   ', '', '', '', '', '', '', 'active', 0),
(611, 'Joseph Cabahug', '   ', '', '', '', '', '', '', 'active', 0),
(612, 'Joseph Marchello', '   ', '', '', '', '', '', '', 'active', 0),
(613, 'Joseph Roa', '   ', '', '', '', '', '', '', 'active', 0),
(614, 'Joseph Satumcacal', '   ', '', '', '', '', '', '', 'active', 0),
(615, 'Joshua Margaja', '   ', '', '', '', '', '', '', 'active', 0),
(616, 'Joshua Pritchard', '   ', '', '', '', '', '', '', 'active', 0),
(617, 'Jovima Management & Dev''t. Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(618, 'Joyce Ann Rosima', '   ', '', '', '', '', '', '', 'active', 0),
(619, 'Jude Mathew Dela Cruz', '   ', '', '', '', '', '', '', 'active', 0),
(620, 'Judilyn A. Basubas', '   ', '', '', '', '', '', '', 'active', 0),
(621, 'Julie-Ann Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(622, 'Julie Kornerup', '   ', '', '', '', '', '', '', 'active', 0),
(623, 'Julius Señara', '   ', '', '', '', '', '', '', 'active', 0),
(624, 'Julymarie Demotor', '   ', '', '', '', '', '', '', 'active', 0),
(625, 'June Keith Ramirez', '   ', '', '', '', '', '', '', 'active', 0),
(626, 'June Tesorero', '   ', '', '', '', '', '', '', 'active', 0),
(627, 'Junelyn Sandigan', '   ', '', '', '', '', '', '', 'active', 0),
(628, 'Junrick Jake Laude', '   ', '', '', '', '', '', '', 'active', 0),
(629, 'Jupiter Apa', '   ', '', '', '', '', '', '', 'active', 0),
(630, 'Juveryn Undap', '   ', '', '', '', '', '', '', 'active', 0),
(631, 'Juvy Ann Mendez', '   ', '', '', '', '', '', '', 'active', 0),
(632, 'Juvy Limpangog', '   ', '', '', '', '', '', '', 'active', 0),
(633, 'JV Shop ''N Shop Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(634, 'KAINOShealth Management, Inc.', '10th flr Metrobank Plaza Jones Ave. Cebu City  ', '032-2556300', '', '', '', '', '', 'active', 0),
(635, 'Kamayo Express', '   ', '', '', '', '', '', '', 'active', 0),
(636, 'Kamayo Express Grocery', '   ', '', '', '', '', '', '', 'active', 0),
(637, 'Kareen Ann Surmillon', '   ', '', '', '', '', '', '', 'active', 0),
(638, 'Karen Marie B. Pogoy', '   ', '', '', '', '', '', '', 'active', 0),
(639, 'Karen May Ferrer', '   ', '', '', '', '', '', '', 'active', 0),
(640, 'Karl David Escoto', '   ', '', '', '', '', '', '', 'active', 0),
(641, 'KARTZONE', 'f. Cabahug St,Ayala Access Road, Mabolo Cebu City  ', '', '', '', '', '', '', 'active', 0),
(642, 'Karyl Bolences', '   ', '', '', '', '', '', '', 'active', 0),
(643, 'Kate Grace Anusuriya', '   ', '', '', '', '', '', '', 'active', 0),
(644, 'Katherine Digamon', '   ', '', '', '', '', '', '', 'active', 0),
(645, 'Katherine Homecillo', '   ', '', '', '', '', '', '', 'active', 0),
(646, 'Kathleen Joy R. Suarez', '   ', '', '', '', '', '', '', 'active', 0),
(647, 'Kay Cadilig', '   ', '', '', '', '', '', '', 'active', 0),
(648, 'KC Pelaez', '   ', '', '', '', '', '', '', 'active', 0),
(649, 'KCT International Travel Services', '   ', '', '', '', '', '', '', 'active', 0),
(650, 'Kein Enterprises', '1-85 Osmena ext. Gochan Hills Camputhaw Cebu City TIN#200-191-535V ', '', '', '', '', '', '', 'active', 0),
(651, 'Keith Everette', '   ', '', '', '', '', '', '', 'active', 0),
(652, 'Ken Ryan B. Gimpayan', '   ', '', '', '', '', '', '', 'active', 0),
(653, 'Kenneth Brian Aninipok', '   ', '', '', '', '', '', '', 'active', 0),
(654, 'Kenneth Res Arong', '   ', '', '', '', '', '', '', 'active', 0),
(655, 'Kenneth Ross Standen', '   ', '', '', '', '', '', '', 'active', 0),
(656, 'Kenneth Subaria', '   ', '', '', '', '', '', '', 'active', 0),
(657, 'Kim Cordle', '   ', '', '', '', '', '', '', 'active', 0),
(658, 'Kim Soo Jung', '   ', '', '', '', '', '', '', 'active', 0),
(659, 'Kim Tae Ho', '   ', '', '', '', '', '', '', 'active', 0),
(660, 'Kim Taeho', '   ', '', '', '', '', '', '', 'active', 0),
(661, 'Kim Yuji', '   ', '', '', '', '', '', '', 'active', 0),
(662, 'King''s Quality Foods (CEBU), Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(663, 'King Godfrey Estardo', '   ', '', '', '', '', '', '', 'active', 0),
(664, 'KP LABELS', '   ', '', '', '', '', '', '', 'active', 0),
(665, 'Kristine Caress Morales', '   ', '', '', '', '', '', '', 'active', 0),
(666, 'Kristine Joy Anusuriya', '   ', '', '', '', '', '', '', 'active', 0),
(667, 'Kristine Simplicio', '   ', '', '', '', '', '', '', 'active', 0),
(668, 'Kristoffer Serrano', '   ', '', '', '', '', '', '', 'active', 0),
(669, 'Krustinna Alvarez', '   ', '', '', '', '', '', '', 'active', 0),
(670, 'KTR Marketing/Celso Rivera, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(671, 'KTR Marketing/James Jonas Rivera', '   ', '', '', '', '', '15 days', '', 'active', 0),
(672, 'Kyung Song Nyel', '   ', '', '', '', '', '', '', 'active', 0),
(673, 'La Nueva Supermart', '   ', '', '', '', '', '7 days', '', 'active', 0),
(674, 'Laarni Venus Marie Giango', '   ', '', '', '', '', '', '', 'active', 0),
(675, 'Lady Grace Sarcena', '   ', '', '', '', '', '', '', 'active', 0),
(676, 'Lahug Glass Supply', '   ', '', '', '', '', '', '', 'active', 0),
(677, 'Laine Bresette Alusin', '   ', '', '', '', '', '', '', 'active', 0),
(678, 'Lalie Ledesma', '   ', '', '', '', '', '', '', 'active', 0),
(679, 'Lani Bersales', '   ', '', '', '', '', '', '', 'active', 0),
(680, 'Lanie Rose Colongan', '   ', '', '', '', '', '', '', 'active', 0),
(681, 'Lannil Bering', '   ', '', '', '', '', '', '', 'active', 0),
(682, 'Lapu-Lapu City Treasurer''s Office', '   ', '', '', '', '', '', '', 'active', 0),
(683, 'Lapu-Lapu Electronics', '   ', '', '', '', '', '', '', 'active', 0),
(684, 'Larbeth Wong', '   ', '', '', '', '', '', '', 'active', 0);
INSERT INTO `suppliers` (`supplier_id`, `name`, `address`, `phone_no`, `mobile_no`, `faxnum`, `note`, `terms`, `company_name`, `status`, `date_added`) VALUES
(685, 'Late Rooms Ltd', '   ', '', '', '', '', '', '', 'active', 0),
(686, 'Lavinea Hopkirk', '   ', '', '', '', '', '', '', 'active', 0),
(687, 'Le Tat Thang', '   ', '', '', '', '', '', '', 'active', 0),
(688, 'Leah Marie Inopia', '   ', '', '', '', '', '', '', 'active', 0),
(689, 'Lee Dong Hee v', '   ', '', '', '', '', '', '', 'active', 0),
(690, 'Lee Hoseog', '   ', '', '', '', '', '', '', 'active', 0),
(691, 'Leilane Villahermosa', '   ', '', '', '', '', '', '', 'active', 0),
(692, 'Leizel Lim', '   ', '', '', '', '', '', '', 'active', 0),
(693, 'Lendon Macatonog', '   ', '', '', '', '', '', '', 'active', 0),
(694, 'Leofe Paquibot', '   ', '', '', '', '', '', '', 'active', 0),
(695, 'Leon John Hill', '   ', '', '', '', '', '', '', 'active', 0),
(696, 'Leonila C. Solon/CEC Agent', '   ', '', '', '', '', '', '', 'active', 0),
(697, 'Leopoldo Daño', '   ', '', '', '', '', '', '', 'active', 0),
(698, 'Lera Lumactod', '   ', '', '', '', '', '', '', 'active', 0),
(699, 'Lesley Joy Secusana', '   ', '', '', '', '', '', '', 'active', 0),
(700, 'Leslie Natividad', '   ', '', '', '', '', '', '', 'active', 0),
(701, 'Lexia Cel Sabuga', '   ', '', '', '', '', '', '', 'active', 0),
(702, 'Lexro Engineering Services', '   ', '', '', '', '', '', '', 'active', 0),
(703, 'Liezel E. Abretil', '   ', '', '', '', '', '', '', 'active', 0),
(704, 'Liezel Elcamel', '   ', '', '', '', '', '', '', 'active', 0),
(705, 'Liezel Sonon', '   ', '', '', '', '', '', '', 'active', 0),
(706, 'Liezl Sacnanas', '   ', '', '', '', '', '', '', 'active', 0),
(707, 'Liezl Tampon', '   ', '', '', '', '', '', '', 'active', 0),
(708, 'Light  Bulb Production', '   ', '', '', '', '', '', '', 'active', 0),
(709, 'Lilane Herana', '   ', '', '', '', '', '', '', 'active', 0),
(710, 'Lilibeth Conado', '   ', '', '', '', '', '', '', 'active', 0),
(711, 'Lilibeth Grace Bernabat', '   ', '', '', '', '', '', '', 'active', 0),
(712, 'Lindsay Nea Belocura', '   ', '', '', '', '', '', '', 'active', 0),
(713, 'Lindsey Masiglat', '   ', '', '', '', '', '', '', 'active', 0),
(714, 'Living Water Filtration & Refilling Stati', '   ', '', '', '', '', '15 days', '', 'active', 0),
(715, 'Liza F. Gutladera', '   ', '', '', '', '', '', '', 'active', 0),
(716, 'Liza Francisco', '   ', '', '', '', '', '', '', 'active', 0),
(717, 'Lloyd Aliviado', '   ', '', '', '', '', '', '', 'active', 0),
(718, 'Lloyd Raymond Wells', '   ', '', '', '', '', '', '', 'active', 0),
(719, 'Loida Salve', '   ', '', '', '', '', '', '', 'active', 0),
(720, 'Lordboy Cabrera', '   ', '', '', '', '', '', '', 'active', 0),
(721, 'Lorelie Salve', '   ', '', '', '', '', '', '', 'active', 0),
(722, 'Lorena Nabe Vergara', '   ', '', '', '', '', '', '', 'active', 0),
(723, 'Lorilen Capendit', '   ', '', '', '', '', '', '', 'active', 0),
(724, 'Lorraine Cuñado', '   ', '', '', '', '', '', '', 'active', 0),
(725, 'Lorraine Michelle Sanchez', '   ', '', '', '', '', '', '', 'active', 0),
(726, 'Lorrie Fay Chu', '   ', '', '', '', '', '', '', 'active', 0),
(727, 'Loth Orozco', '   ', '', '', '', '', '', '', 'active', 0),
(728, 'Louigie Malubay', '   ', '', '', '', '', '', '', 'active', 0),
(729, 'Lovely Fuentes', '   ', '', '', '', '', '', '', 'active', 0),
(730, 'Lovely Labra', '   ', '', '', '', '', '', '', 'active', 0),
(731, 'LPN Manna Advertising Co.', '   ', '', '', '', '', '', '', 'active', 0),
(732, 'Luanna Pit Magdales', '   ', '', '', '', '', '', '', 'active', 0),
(733, 'Luchie-Lee Longinos', '   ', '', '', '', '', '', '', 'active', 0),
(734, 'Lucille Alinsug', '   ', '', '', '', '', '', '', 'active', 0),
(735, 'Lucille Belacho', '   ', '', '', '', '', '', '', 'active', 0),
(736, 'Lucille Ceniza', '   ', '', '', '', '', '', '', 'active', 0),
(737, 'Luzviminda Cervantes', '   ', '', '', '', '', '', '', 'active', 0),
(738, 'Lydia Marikit Aworuwa', '   ', '', '', '', '', '', '', 'active', 0),
(739, 'Lydia Nacua', '   ', '', '', '', '', '7 days', '', 'active', 0),
(740, 'Lyndel Inot', '   ', '', '', '', '', '', '', 'active', 0),
(741, 'Lynholly Alforque', '   ', '', '', '', '', '', '', 'active', 0),
(742, 'MA-AY Enterprises', '   ', '', '', '', '', '30 days', '', 'active', 0),
(743, 'Ma. Concetta Cabañero', '   ', '', '', '', '', '', '', 'active', 0),
(744, 'Ma. Cristy D. Angchangco', '   ', '', '', '', '', '', '', 'active', 0),
(745, 'Ma. Gypsy Desquitado', '   ', '', '', '', '', '', '', 'active', 0),
(746, 'Ma. Jennylyn Atizon', '   ', '', '', '', '', '', '', 'active', 0),
(747, 'Ma. Linda T. Ompad', '   ', '', '', '', '', '', '', 'active', 0),
(748, 'Ma. Luisa Pepito', '   ', '', '', '', '', '', '', 'active', 0),
(749, 'Ma. Rodelia Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(750, 'Ma. Theresa Adolfo', '   ', '', '', '', '', '', '', 'active', 0),
(751, 'Ma. Theresa Rom', '   ', '', '', '', '', '', '', 'active', 0),
(752, 'Ma. Veronica Arca', '   ', '', '', '', '', '', '', 'active', 0),
(753, 'Mabel Baguio', '   ', '', '', '', '', '', '', 'active', 0),
(754, 'Macrolite Electrical', '   ', '', '', '', '', '', '', 'active', 0),
(755, 'Macrolite Hardware', '307 Magallanes St. Cebu City  ', '253-3755', '', '', '', '', '', 'active', 0),
(756, 'Mactan Electric Company, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(757, 'Mactan Gas Center', '   ', '', '', '', '', '', '', 'active', 0),
(758, 'Mae Joy Doroy', '   ', '', '', '', '', '', '', 'active', 0),
(759, 'Maebeth Mullero', '   ', '', '', '', '', '', '', 'active', 0),
(760, 'Main Systems Incorporated', '   ', '', '', '', '', '', '', 'active', 0),
(761, 'Mandaue Foam Industries, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(762, 'Mangelie Verde', '   ', '', '', '', '', '', '', 'active', 0),
(763, 'Mansmith and Fielders, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(764, 'Marc and Mattheus', '   ', '', '', '', '', '', '', 'active', 0),
(765, 'Marc Andre Ginete', '   ', '', '', '', '', '', '', 'active', 0),
(766, 'Marcelina Gesalago', '   ', '', '', '', '', '', '', 'active', 0),
(767, 'Marciano Agbay', '   ', '', '', '', '', '', '', 'active', 0),
(768, 'Marcos Pio Alarin', '   ', '', '', '', '', '', '', 'active', 0),
(769, 'Marcy Bansag', '   ', '', '', '', '', '', '', 'active', 0),
(770, 'Margallaine Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(771, 'Margarette Grace Barz', '   ', '', '', '', '', '', '', 'active', 0),
(772, 'Marguad Trading and Services Co.', '   ', '', '', '', '', '', '', 'active', 0),
(773, 'Maria Christine Go', '   ', '', '', '', '', '', '', 'active', 0),
(774, 'Maria Dolores Tedor', '   ', '', '', '', '', '', '', 'active', 0),
(775, 'Maria Eloisa Lasan', '   ', '', '', '', '', '', '', 'active', 0),
(776, 'Maria Joanna Angelica Lacia', '   ', '', '', '', '', '', '', 'active', 0),
(777, 'Maria Kenvi Suan', '   ', '', '', '', '', '', '', 'active', 0),
(778, 'Maria Kristina Quisido', '   ', '', '', '', '', '', '', 'active', 0),
(779, 'Maria Linda Danes', '   ', '', '', '', '', '', '', 'active', 0),
(780, 'Maria Linen Baguio', '   ', '', '', '', '', '', '', 'active', 0),
(781, 'Maria Lourdes Calma', '   ', '', '', '', '', '', '', 'active', 0),
(782, 'Maria Lourdes Noval', '   ', '', '', '', '', '', '', 'active', 0),
(783, 'Maria Reima Potot', '   ', '', '', '', '', '', '', 'active', 0),
(784, 'Maria Theresa Pama', '   ', '', '', '', '', '', '', 'active', 0),
(785, 'Maria Veronica Arca', '   ', '', '', '', '', '', '', 'active', 0),
(786, 'Maria Victoria Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(787, 'Marian D. Canama', '   ', '', '', '', '', '', '', 'active', 0),
(788, 'Marianne Katrina Velasco', '   ', '', '', '', '', '', '', 'active', 0),
(789, 'Maribeth Sembrano', '   ', '', '', '', '', '', '', 'active', 0),
(790, 'Maricar Tito', '   ', '', '', '', '', '', '', 'active', 0),
(791, 'Maricel Cañete', '   ', '', '', '', '', '', '', 'active', 0),
(792, 'Maricel Gemilgo', '   ', '', '', '', '', '', '', 'active', 0),
(793, 'Maricor Carmela Amoguis', '   ', '', '', '', '', '', '', 'active', 0),
(794, 'Maridel Marketing', '2 Sycamore St. Pacific Grand Villas Marigondon Lapu-lapu City 496-8412 171-984-561-000', '', '', '', '', '', '', 'active', 0),
(795, 'Marie Angelique Villamor', '   ', '', '', '', '', '', '', 'active', 0),
(796, 'Marie Joy Madeja', '   ', '', '', '', '', '', '', 'active', 0),
(797, 'Marie Joy Martinez', '   ', '', '', '', '', '', '', 'active', 0),
(798, 'Marie Rose Escabas', '   ', '', '', '', '', '', '', 'active', 0),
(799, 'Mariedel Ardina', '   ', '', '', '', '', '', '', 'active', 0),
(800, 'Mariefe Barata', '   ', '', '', '', '', '', '', 'active', 0),
(801, 'Mariejoy Martinez', '   ', '', '', '', '', '', '', 'active', 0),
(802, 'Mariel Cortes', '   ', '', '', '', '', '', '', 'active', 0),
(803, 'marigold Paclipan', '   ', '', '', '', '', '', '', 'active', 0),
(804, 'Marigold Zaballero/Elsa Lim', '   ', '', '', '', '', '', '', 'active', 0),
(805, 'Marilyn Torres', '   ', '', '', '', '', '', '', 'active', 0),
(806, 'Marissa Noraida Dalupang', '   ', '', '', '', '', '', '', 'active', 0),
(807, 'Marivel Bihag', '   ', '', '', '', '', '', '', 'active', 0),
(808, 'Marivic Calidguid', '   ', '', '', '', '', '', '', 'active', 0),
(809, 'Marivic Dalugdog', '   ', '', '', '', '', '', '', 'active', 0),
(810, 'Mariza Duhaylungsod', '   ', '', '', '', '', '', '', 'active', 0),
(811, 'Mariza Garcia', '   ', '', '', '', '', '', '', 'active', 0),
(812, 'Marjorie Miralles', '   ', '', '', '', '', '', '', 'active', 0),
(813, 'Mark Anthony Tan', '   ', '', '', '', '', '', '', 'active', 0),
(814, 'Mark Carmelo Ganar', '   ', '', '', '', '', '', '', 'active', 0),
(815, 'Mark Machacon', '   ', '', '', '', '', '', '', 'active', 0),
(816, 'Marlowe Toledo Villagonzalo', '   ', '', '', '', '', '', '', 'active', 0),
(817, 'Marquel Andrico', '   ', '', '', '', '', '', '', 'active', 0),
(818, 'Marquel Andrino', '   ', '', '', '', '', '', '', 'active', 0),
(819, 'Marrycar Aquino', '   ', '', '', '', '', '', '', 'active', 0),
(820, 'Marsha Valencia', '   ', '', '', '', '', '', '', 'active', 0),
(821, 'Marteniana H. Tulomia', '   ', '', '', '', '', '', '', 'active', 0),
(822, 'Marvin Ascura', '   ', '', '', '', '', '', '', 'active', 0),
(823, 'Marvin Bryan Gayotin', '   ', '', '', '', '', '', '', 'active', 0),
(824, 'Mary-Ann Ylanan', '   ', '', '', '', '', '', '', 'active', 0),
(825, 'Mary Angelique Villamor', '   ', '', '', '', '', '', '', 'active', 0),
(826, 'Mary Catherine Ariosa', '   ', '', '', '', '', '', '', 'active', 0),
(827, 'Mary Claire Jurolan', '   ', '', '', '', '', '', '', 'active', 0),
(828, 'Mary Emily Ymbong', '   ', '', '', '', '', '', '', 'active', 0),
(829, 'Mary Faith Bate', '   ', '', '', '', '', '', '', 'active', 0),
(830, 'Mary Grace Auman', '   ', '', '', '', '', '', '', 'active', 0),
(831, 'Mary Grace Cantero', '   ', '', '', '', '', '', '', 'active', 0),
(832, 'Mary Grace Labiste', '   ', '', '', '', '', '', '', 'active', 0),
(833, 'Mary Grace N. Torbeso', '   ', '', '', '', '', '', '', 'active', 0),
(834, 'Mary Grace Torbeso', '   ', '', '', '', '', '', '', 'active', 0),
(835, 'Mary Jane Gutib', '   ', '', '', '', '', '', '', 'active', 0),
(836, 'Mary Jane Orbeta', '   ', '', '', '', '', '', '', 'active', 0),
(837, 'Mary Jane S. Orbeta', '   ', '', '', '', '', '', '', 'active', 0),
(838, 'Mary Jonafe Alcover', '   ', '', '', '', '', '', '', 'active', 0),
(839, 'Mary Joy Debalucos', '   ', '', '', '', '', '', '', 'active', 0),
(840, 'Mary Joy Maglahus', '   ', '', '', '', '', '', '', 'active', 0),
(841, 'Mary Joy Manglahus', '   ', '', '', '', '', '', '', 'active', 0),
(842, 'Mary June Tesorero', '   ', '', '', '', '', '', '', 'active', 0),
(843, 'Mary Lad Caliao', '   ', '', '', '', '', '', '', 'active', 0),
(844, 'Mary Maystall Labus', '   ', '', '', '', '', '', '', 'active', 0),
(845, 'Mary Melany Ralloma', '   ', '', '', '', '', '', '', 'active', 0),
(846, 'Marygrace Cantero', '   ', '', '', '', '', '', '', 'active', 0),
(847, 'Masashi Nagata', '   ', '', '', '', '', '', '', 'active', 0),
(848, 'Matt Gershom Talisic', '   ', '', '', '', '', '', '', 'active', 0),
(849, 'Maureen Bayo-on', '   ', '', '', '', '', '', '', 'active', 0),
(850, 'Maureen Bayon-on', '   ', '', '', '', '', '', '', 'active', 0),
(851, 'Maureen Michelle Tan', '   ', '', '', '', '', '', '', 'active', 0),
(852, 'Maximax Systems, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(853, 'May De Vera', '   ', '', '', '', '', '', '', 'active', 0),
(854, 'May Miot', '   ', '', '', '', '', '', '', 'active', 0),
(855, 'Mayfifth Patalinjug', '   ', '', '', '', '', '', '', 'active', 0),
(856, 'Mayflor Montejo', '   ', '', '', '', '', '', '', 'active', 0),
(857, 'Maynard Sian', '   ', '', '', '', '', '', '', 'active', 0),
(858, 'MCM Water Refilling Station', 'TDB Bldg. National Hiway Brgy.Pajo, Lapu-lapu City  ', '342-8714', '', '', '', '', '', 'active', 0),
(859, 'Medelyn Igot', '   ', '', '', '', '', '', '', 'active', 0),
(860, 'Mega Modern Gasul Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(861, 'Melanio Zaraga', '   ', '', '', '', '', '', '', 'active', 0),
(862, 'Melissa Longakit', '   ', '', '', '', '', '', '', 'active', 0),
(863, 'Meliza R. Abayon', '   ', '', '', '', '', '', '', 'active', 0),
(864, 'MELJOY TALINGTING', '   ', '', '', '', '', '', '', 'active', 0),
(865, 'Mellibeth Go', '   ', '', '', '', '', '', '', 'active', 0),
(866, 'Mellicent Ponce', '   ', '', '', '', '', '', '', 'active', 0),
(867, 'Melnie Chavez', '   ', '', '', '', '', '', '', 'active', 0),
(868, 'Merlin T. Manalon', '   ', '', '', '', '', '', '', 'active', 0),
(869, 'Mervin Capili', '   ', '', '', '', '', '', '', 'active', 0),
(870, 'Meryll Jayne Ybañez', '   ', '', '', '', '', '', '', 'active', 0),
(871, 'Mesias Alinsunod', '   ', '', '', '', '', '', '', 'active', 0),
(872, 'Mesraem King Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(873, 'Metaluxe Cebu City', 'Ground Flr Glory Bldg Mabini St Cebu City 102-716-363-003 ', '', '', '', '', '', '', 'active', 0),
(874, 'Metro Sports', '   ', '', '', '', '', '', '', 'active', 0),
(875, 'Metro Sports Badminton Club', '   ', '', '', '', '', '', '', 'active', 0),
(876, 'Metropolitan Cebu Water District', '   ', '', '', '', '', '', '', 'active', 0),
(877, 'Mi Suk Song', '   ', '', '', '', '', '', '', 'active', 0),
(878, 'Mia Pearl Geron', '   ', '', '', '', '', '', '', 'active', 0),
(879, 'Michael B. Berry', '   ', '', '', '', '', '', '', 'active', 0),
(880, 'Michael Birdzell', '   ', '', '', '', '', '', '', 'active', 0),
(881, 'Michael Gerard Fults', '   ', '', '', '', '', '', '', 'active', 0),
(882, 'Michael Short', '   ', '', '', '', '', '', '', 'active', 0),
(883, 'Michael Sulit', '   ', '', '', '', '', '', '', 'active', 0),
(884, 'Michelle A. Cosep', '   ', '', '', '', '', '', '', 'active', 0),
(885, 'Michelle Dianne Arche', '   ', '', '', '', '', '', '', 'active', 0),
(886, 'Michelle Florenda', '   ', '', '', '', '', '', '', 'active', 0),
(887, 'Michelle Go', '   ', '', '', '', '', '', '', 'active', 0),
(888, 'Michelle Ramayla', '   ', '', '', '', '', '', '', 'active', 0),
(889, 'Miki Iwamoto and/or CASH', '   ', '', '', '', '', '', '', 'active', 0),
(890, 'Mildred Baay', '   ', '', '', '', '', '', '', 'active', 0),
(891, 'Milton McGlynn Worthington', '   ', '', '', '', '', '', '', 'active', 0),
(892, 'Minerva Adem', '   ', '', '', '', '', '', '', 'active', 0),
(893, 'Minnie Burlaos', '   ', '', '', '', '', '', '', 'active', 0),
(894, 'Minnielou Chavez', '   ', '', '', '', '', '', '', 'active', 0),
(895, 'Miraflor Maghinay', '   ', '', '', '', '', '', '', 'active', 0),
(896, 'Miraflor Orio', '   ', '', '', '', '', '', '', 'active', 0),
(897, 'Mirasol Bontilao', '   ', '', '', '', '', '', '', 'active', 0),
(898, 'Miren Go', '   ', '', '', '', '', '', '', 'active', 0),
(899, 'MJ Auto ID', 'Escano Bldg. Door 4 P. del Rosario St. Cebu City TIN#181-176-786-000 ', '', '', '', '', '', '', 'active', 0),
(900, 'MJ Auto ID Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(901, 'MK Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(902, 'Mom''s Classy Decors & Dress Shop', '   ', '', '', '', '', '', '', 'active', 0),
(903, 'Monica Reyes', '   ', '', '', '', '', '', '', 'active', 0),
(904, 'Monna Zelda Ursal', '   ', '', '', '', '', '', '', 'active', 0),
(905, 'Mozcom, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(906, 'MUFARRIJ ALSUBAIE', '   ', '', '', '', '', '', '', 'active', 0),
(907, 'Mygill Rago', '   ', '', '', '', '', '', '', 'active', 0),
(908, 'Mykha Dela Pena', '   ', '', '', '', '', '', '', 'active', 0),
(909, 'Mylene Bonganciso', '   ', '', '', '', '', '', '', 'active', 0),
(910, 'Mylene Chavez', '   ', '', '', '', '', '', '', 'active', 0),
(911, 'Mynimo Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(912, 'Myra Monteclar', '   ', '', '', '', '', '', '', 'active', 0),
(913, 'Myrla Bantilan', '   ', '', '', '', '', '', '', 'active', 0),
(914, 'Myrnali Gustilo v', '   ', '', '', '', '', '', '', 'active', 0),
(915, 'Nadine Rorie Jumao-as', '   ', '', '', '', '', '', '', 'active', 0),
(916, 'Naomi Villagonzalo', '   ', '', '', '', '', '', '', 'active', 0),
(917, 'Narumol Jewyoungphol', '   ', '', '', '', '', '', '', 'active', 0),
(918, 'Natasha Juaniza', '   ', '', '', '', '', '', '', 'active', 0),
(919, 'National Bookstore', '   ', '', '', '', '', '30 days', '', 'active', 0),
(920, 'Need Ink', '   ', '', '', '', '', '30 days', '', 'active', 0),
(921, 'Needs and Solutions', '   ', '', '', '', '', '', '', 'active', 0),
(922, 'Neff Grace Tan', '   ', '', '', '', '', '', '', 'active', 0),
(923, 'Neil Braw', '   ', '', '', '', '', '', '', 'active', 0),
(924, 'Neil Nazareno', '   ', '', '', '', '', '', '', 'active', 0),
(925, 'Nelsa Traveno', '   ', '', '', '', '', '', '', 'active', 0),
(926, 'Nelsa Travero', '   ', '', '', '', '', '', '', 'active', 0),
(927, 'NEMPEX Pest Control', '201 A Hiway Tagunol Cogon Pardo Cebu City  ', '', '', '', '', '15 days', '', 'active', 0),
(928, 'Neo-Konsult', '   ', '', '', '', '', '', '', 'active', 0),
(929, 'Neriza Trasmel', '   ', '', '', '', '', '', '', 'active', 0),
(930, 'Newgen Products, Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(931, 'Nguyen Quang Giang', '   ', '', '', '', '', '', '', 'active', 0),
(932, 'Nieva Joy Sevillejo', '   ', '', '', '', '', '', '', 'active', 0),
(933, 'Niño Adonis Litgio', '   ', '', '', '', '', '', '', 'active', 0),
(934, 'Niño Janolino', '   ', '', '', '', '', '', '', 'active', 0),
(935, 'Noe Silario', '   ', '', '', '', '', '', '', 'active', 0),
(936, 'Noe Silorio', '   ', '', '', '', '', '', '', 'active', 0),
(937, 'Noellie Lamis', '   ', '', '', '', '', '', '', 'active', 0),
(938, 'Norris Pat Ruiz', '   ', '', '', '', '', '', '', 'active', 0),
(939, 'NUTECH Marketing', 'Sanciangko cor Borromeo Sts. Cebu City  ', '254-0397', '', '', '204-928-530-000', '', '', 'active', 0),
(940, 'Octagon Computer Superstore', '   ', '', '', '', '', '', '', 'active', 0),
(941, 'Odessa Faye Kong', '   ', '', '', '', '', '', '', 'active', 0),
(942, 'Office of the Building Officials', '   ', '', '', '', '', '', '', 'active', 0),
(943, 'Office of the City Treasurer', '   ', '', '', '', '', '', '', 'active', 0),
(944, 'Ophelia Puson', '   ', '', '', '', '', '', '', 'active', 0),
(945, 'Optima Typographics', '   ', '', '', '', '', '', '', 'active', 0),
(946, 'Orient Star Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(947, 'Osmundo Trinidad, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(948, 'Otelia Nutcha Dacaldacal', '   ', '', '', '', '', '', '', 'active', 0),
(949, 'Othelea Yap', '   ', '', '', '', '', '', '', 'active', 0),
(950, 'Pamela Jane Almaden', '   ', '', '', '', '', '', '', 'active', 0),
(951, 'Pamela Luz Osabel', '   ', '', '', '', '', '', '', 'active', 0),
(952, 'Pan Pacific Travel Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(953, 'Papertech, Inc.', '835 Felipe Pike Street, Bagong Ilog, Pasig City ', '631-5524', '', '', '000-165-824-000', '', '', 'active', 0),
(954, 'Paul Bourassa', '   ', '', '', '', '', '', '', 'active', 0),
(955, 'Paul Fellingham', '   ', '', '', '', '', '', '', 'active', 0),
(956, 'Paul Noel Rendon', '   ', '', '', '', '', '', '', 'active', 0),
(957, 'Paul Sanders', '   ', '', '', '', '', '', '', 'active', 0),
(958, 'Paula Lau', '   ', '', '', '', '', '', '', 'active', 0),
(959, 'Payroll - Part Time Teachers', '   ', '', '', '', '', '', '', 'active', 0),
(960, 'Pearl Gumaroy', '   ', '', '', '', '', '', '', 'active', 0),
(961, 'Perlita Regidor', '   ', '', '', '', '', '', '', 'active', 0),
(962, 'Perri Carpets Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(963, 'Pete Onni', '   ', '', '', '', '', '', '', 'active', 0),
(964, 'Peter Hutchinson', '   ', '', '', '', '', '', '', 'active', 0),
(965, 'Peter Teejay Salgado', '   ', '', '', '', '', '', '', 'active', 0),
(966, 'Petronio B. Cotcot, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(967, 'Philip John Señoren', '   ', '', '', '', '', '', '', 'active', 0),
(968, 'Philip Morris Sarmiento', '   ', '', '', '', '', '', '', 'active', 0),
(969, 'Philipp Tampus', '   ', '', '', '', '', '', '', 'active', 0),
(970, 'Philippine Health Insurance Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(971, 'Philippine Long Distance Telephone Co.', '   ', '', '', '', '', '', '', 'active', 0),
(972, 'Philippine Spring Water Resources, Inc.', '   ', '', '', '', '', '15 days', '', 'active', 0),
(973, 'Philippines Spring Water Resources, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(974, 'Phoebe Marie Luzano', '   ', '', '', '', '', '', '', 'active', 0),
(975, 'Pjlou Abelgas', '   ', '', '', '', '', '', '', 'active', 0),
(976, 'Porferio Alvarez,Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(977, 'Prime Care Cebu', '   ', '', '', '', '', '', '', 'active', 0),
(978, 'Prince Warehouse Club, Inc.', '3rd Ave. North Reclamation Area Cebu City 000-313-880-000 ', '', '', '', '', '', '', 'active', 0),
(979, 'Princess Irene Dy', '   ', '', '', '', '', '', '', 'active', 0),
(980, 'Princess Rea Minguito', '   ', '', '', '', '', '', '', 'active', 0),
(981, 'PTA Software Solution', '   ', '', '', '', '', '', '', 'active', 0),
(982, 'QA Causeway Printers, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(983, 'Queenie D. Valdehueza', '   ', '', '', '', '', '', '', 'active', 0),
(984, 'Queenie Jessale Silvosa', '   ', '', '', '', '', '', '', 'active', 0),
(985, 'R&E Technical Services', 'Fatima Village, Pagsabungan Mandaue City TIN#080-168-287-772 ', '', '', '', '', '', '', 'active', 0),
(986, 'Rachel Cañete', '   ', '', '', '', '', '', '', 'active', 0),
(987, 'Rachel Jane  Riconalla', '   ', '', '', '', '', '', '', 'active', 0),
(988, 'Radical Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(989, 'Rafaella Rowena Niere', '   ', '', '', '', '', '', '', 'active', 0),
(990, 'Rafunzel Bero', '   ', '', '', '', '', '', '', 'active', 0),
(991, 'Rainbow Tours & Travel, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(992, 'Rainbow Tours & Travels, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(993, 'Rajah Park Hotel', 'Fuente Osmeña Cebu City 241-159-332-000  ', '412-3337', '', '', '', '', '', 'active', 0),
(994, 'Ramer Duhino', '   ', '', '', '', '', '', '', 'active', 0),
(995, 'Ramil Madera', '   ', '', '', '', '', '', '', 'active', 0),
(996, 'Ramir Carbonilla', '   ', '', '', '', '', '', '', 'active', 0),
(997, 'Ramir N. Martinez, Jr.', '   ', '', '', '', '', '', '', 'active', 0),
(998, 'Ramir Pielago', '   ', '', '', '', '', '', '', 'active', 0),
(999, 'Ramon De Castro', '   ', '', '', '', '', '', '', 'active', 0),
(1000, 'Rana Maria Melgar', '   ', '', '', '', '', '', '', 'active', 0),
(1001, 'Randy Tangag', '   ', '', '', '', '', '', '', 'active', 0),
(1002, 'Raquel Ebarita', '   ', '', '', '', '', '', '', 'active', 0),
(1003, 'Rasmus Frederiksen', '   ', '', '', '', '', '', '', 'active', 0),
(1004, 'Raul Arnibal', '   ', '', '', '', '', '', '', 'active', 0),
(1005, 'RC Goldline Saleshop', 'Door 2 F Ramos St Cebu City 203-918-627-000 ', '', '', '', '', '', '', 'active', 0),
(1006, 'Rebecca Rebaño', '   ', '', '', '', '', '', '', 'active', 0),
(1007, 'Redjing Calma', '   ', '', '', '', '', '', '', 'active', 0),
(1008, 'Redtail Ventures Fund', 'TIN 220-404-819-000   ', '', '', '', '', '', '', 'active', 0),
(1009, 'Reggell Magalso', '   ', '', '', '', '', '', '', 'active', 0),
(1010, 'Regine Clyde Songcoya', '   ', '', '', '', '', '', '', 'active', 0),
(1011, 'Register of Deeds-Lapu-Lapu City', '   ', '', '', '', '', '', '', 'active', 0),
(1012, 'Rei Tanaka c', '   ', '', '', '', '', '', '', 'active', 0),
(1013, 'Rei Tanaka v', '   ', '', '', '', '', '', '', 'active', 0),
(1014, 'Renan Tumulak', '   ', '', '', '', '', '', '', 'active', 0),
(1015, 'Renie Aro', '   ', '', '', '', '', '', '', 'active', 0),
(1016, 'Rennie Aro', '   ', '', '', '', '', '', '', 'active', 0),
(1017, 'Res Kenneth Arong', '   ', '', '', '', '', '', '', 'active', 0),
(1018, 'Restilyn Segovia', '   ', '', '', '', '', '', '', 'active', 0),
(1019, 'Retchie Mae Sotilleza', '   ', '', '', '', '', '', '', 'active', 0),
(1020, 'Rex Alexander I. Woo Aquino', '   ', '', '', '', '', '', '', 'active', 0),
(1021, 'Rex Camay', '   ', '', '', '', '', '', '', 'active', 0),
(1022, 'Rhea Jagmoc', '   ', '', '', '', '', '', '', 'active', 0),
(1023, 'Rhea Papas', '   ', '', '', '', '', '', '', 'active', 0),
(1024, 'Rhea Paula Lau', '   ', '', '', '', '', '', '', 'active', 0),
(1025, 'Rhoda Mae Herrera', '   ', '', '', '', '', '', '', 'active', 0),
(1026, 'Richard Aying', '   ', '', '', '', '', '', '', 'active', 0),
(1027, 'Ricio Salibay', '   ', '', '', '', '', '', '', 'active', 0),
(1028, 'Rico Lanurias', '   ', '', '', '', '', '', '', 'active', 0),
(1029, 'Rinchen Namgay', '   ', '', '', '', '', '', '', 'active', 0),
(1030, 'Rinnah Flores', '   ', '', '', '', '', '', '', 'active', 0),
(1031, 'Risha Mae Regalado', '   ', '', '', '', '', '', '', 'active', 0),
(1032, 'Rita Fe Jayme', '   ', '', '', '', '', '', '', 'active', 0),
(1033, 'Rita Jayme', '   ', '', '', '', '', '', '', 'active', 0),
(1034, 'Rizal Commercial Banking Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1035, 'Rizalene Carcedo', '   ', '', '', '', '', '', '', 'active', 0),
(1036, 'Roan Hurst', '   ', '', '', '', '', '', '', 'active', 0),
(1037, 'Robert Bailey', '   ', '', '', '', '', '', '', 'active', 0),
(1038, 'Robert Whitfield', '   ', '', '', '', '', '', '', 'active', 0),
(1039, 'Robin Nguyen', '   ', '', '', '', '', '', '', 'active', 0),
(1040, 'Rochelle Arante', '   ', '', '', '', '', '', '', 'active', 0),
(1041, 'Rod Hill', '   ', '', '', '', '', '', '', 'active', 0),
(1042, 'Rodmar Colipano', '   ', '', '', '', '', '', '', 'active', 0),
(1043, 'Rodolfo L. Yap', '   ', '', '', '', '', '', '', 'active', 0),
(1044, 'Rodrigo Ellescas', '   ', '', '', '', '', '', '', 'active', 0),
(1045, 'Rodrigo V. Calonia', '   ', '', '', '', '', '', '', 'active', 0),
(1046, 'Roel Molijon', '   ', '', '', '', '', '', '', 'active', 0),
(1047, 'Roel Tiongson', '   ', '', '', '', '', '', '', 'active', 0),
(1048, 'Rogeno Icoy', '   ', '', '', '', '', '', '', 'active', 0),
(1049, 'Rogeno Mar Icoy', '   ', '', '', '', '', '', '', 'active', 0),
(1050, 'Roi Dana Berador', '   ', '', '', '', '', '', '', 'active', 0),
(1051, 'Roland Caranzo', '   ', '', '', '', '', '', '', 'active', 0),
(1052, 'Rolando Rosales', '   ', '', '', '', '', '', '', 'active', 0),
(1053, 'Roldan Aguirre', '   ', '', '', '', '', '', '', 'active', 0),
(1054, 'Rolher Marble Supply', '   ', '', '', '', '', '', '', 'active', 0),
(1055, 'Roli Lendio', '   ', '', '', '', '', '', '', 'active', 0),
(1056, 'Rombert Dakay', '   ', '', '', '', '', '', '', 'active', 0),
(1057, 'Rommel Amante', '   ', '', '', '', '', '', '', 'active', 0),
(1058, 'Ronald Payao', '   ', '', '', '', '', '', '', 'active', 0),
(1059, 'Ronald Restauro', '   ', '', '', '', '', '', '', 'active', 0),
(1060, 'Ronaldo Goliat', '   ', '', '', '', '', '', '', 'active', 0),
(1061, 'Ronit Remulta', '   ', '', '', '', '', '', '', 'active', 0),
(1062, 'Ronnie Pitogo', '   ', '', '', '', '', '', '', 'active', 0),
(1063, 'Ronnie Pitogo{2}', '   ', '', '', '', '', '', '', 'active', 0),
(1064, 'Rosa Lima Ewican', '   ', '', '', '', '', '', '', 'active', 0),
(1065, 'Rosa Ria Son', '   ', '', '', '', '', '', '', 'active', 0),
(1066, 'Rosalyn P. Lobo', '   ', '', '', '', '', '', '', 'active', 0),
(1067, 'Rosario Carpina', '   ', '', '', '', '', '', '', 'active', 0),
(1068, 'Rose Cañedo', '   ', '', '', '', '', '', '', 'active', 0),
(1069, 'Rosemarie Booc', '   ', '', '', '', '', '', '', 'active', 0),
(1070, 'Rosendo Ajeas', '   ', '', '', '', '', '', '', 'active', 0),
(1071, 'Rosmin Gallego', '   ', '', '', '', '', '', '', 'active', 0),
(1072, 'Rosseanne Jade Gaco', '   ', '', '', '', '', '', '', 'active', 0),
(1073, 'Rowena Camocamo', '   ', '', '', '', '', '', '', 'active', 0),
(1074, 'Rowena Degamo', '   ', '', '', '', '', '', '', 'active', 0),
(1075, 'Roxanne Doronila', '   ', '', '', '', '', '', '', 'active', 0),
(1076, 'RTM Traders', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1077, 'Rubelyn Inot', '   ', '', '', '', '', '', '', 'active', 0),
(1078, 'Rubicon Trading', '   ', '', '', '', '', '', '', 'active', 0),
(1079, 'Ruel S. Molijon', '   ', '', '', '', '', '', '', 'active', 0),
(1080, 'Ruth Montebon', '   ', '', '', '', '', '', '', 'active', 0),
(1081, 'Ryan Esclamado', '   ', '', '', '', '', '', '', 'active', 0),
(1082, 'Ryan Jude Espenido', '   ', '', '', '', '', '', '', 'active', 0),
(1083, 'Ryan Sucaldito', '   ', '', '', '', '', '', '', 'active', 0),
(1084, 'S.T. Marketing', '   ', '', '', '', '', '', '', 'active', 0),
(1085, 'Sabrina Theresa Concepcion', '   ', '', '', '', '', '', '', 'active', 0),
(1086, 'Sahlee B. Opone', '   ', '', '', '', '', '', '', 'active', 0),
(1087, 'Salome Espra', '   ', '', '', '', '', '', '', 'active', 0),
(1088, 'Sanitary Care Products Asia, Inc.-Cebu', '   ', '', '', '', '', '30 days', '', 'active', 0),
(1089, 'Sarah Laborte', '   ', '', '', '', '', '', '', 'active', 0),
(1090, 'Sarah Macabantog', '   ', '', '', '', '', '', '', 'active', 0),
(1091, 'Sarah Mae Ando', '   ', '', '', '', '', '', '', 'active', 0),
(1092, 'Sarah Mae Tapec', '   ', '', '', '', '', '', '', 'active', 0),
(1093, 'Sarinah Soliano', '   ', '', '', '', '', '', '', 'active', 0),
(1094, 'Savemore Mactan', '   ', '', '', '', '', '', '', 'active', 0),
(1095, 'Sayoko Konishi v', '   ', '', '', '', '', '', '', 'active', 0),
(1096, 'Sean Millerick', '   ', '', '', '', '', '', '', 'active', 0),
(1097, 'Security Systems Monitoring,Inc.', '51-A MT Bldg. Subangdako Mandaue City 004-755-903-000 ', '', '', '', '', '', '', 'active', 0),
(1098, 'Senta Kreger', '   ', '', '', '', '', '', '', 'active', 0),
(1099, 'Shaeg Criste Albuladora', '   ', '', '', '', '', '', '', 'active', 0),
(1100, 'Shangrila Mactan Island Resort', '   ', '', '', '', '', '', '', 'active', 0),
(1101, 'Sharon Ia Casayan', '   ', '', '', '', '', '', '', 'active', 0),
(1102, 'Sharon Perla Suico', '   ', '', '', '', '', '', '', 'active', 0),
(1103, 'Sharp (Phils.) Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1104, 'Sheila Bactol', '   ', '', '', '', '', '', '', 'active', 0),
(1105, 'Sheila Mae Tero', '   ', '', '', '', '', '', '', 'active', 0),
(1106, 'Sheila Tabuno', '   ', '', '', '', '', '', '', 'active', 0),
(1107, 'Sheila Umbay', '   ', '', '', '', '', '', '', 'active', 0),
(1108, 'Sheila Veyra', '   ', '', '', '', '', '', '', 'active', 0),
(1109, 'Shelley Kei Sato', '   ', '', '', '', '', '', '', 'active', 0),
(1110, 'Shellou Gamus', '   ', '', '', '', '', '', '', 'active', 0),
(1111, 'Shendelen Lumanog', '   ', '', '', '', '', '', '', 'active', 0),
(1112, 'Shene Dablo', '   ', '', '', '', '', '', '', 'active', 0),
(1113, 'Shene Dablo Ragaza', '   ', '', '', '', '', '', '', 'active', 0),
(1114, 'Shene Ragaza', '   ', '', '', '', '', '', '', 'active', 0),
(1115, 'Sherly Garciano', '   ', '', '', '', '', '', '', 'active', 0),
(1116, 'Sherry Mae Beleno', '   ', '', '', '', '', '', '', 'active', 0),
(1117, 'Sherwin Clark Pasana', '   ', '', '', '', '', '', '', 'active', 0),
(1118, 'Sherylyn Abing', '   ', '', '', '', '', '', '', 'active', 0),
(1119, 'Shewalk''s Engravables', '   ', '', '', '', '', '', '', 'active', 0),
(1120, 'shiela veyra', '   ', '', '', '', '', '', '', 'active', 0),
(1121, 'Shiella Bactol', '   ', '', '', '', '', '', '', 'active', 0),
(1122, 'Shiena Joy Tisoy', '   ', '', '', '', '', '', '', 'active', 0),
(1123, 'Shirley Nacua', '   ', '', '', '', '', '', '', 'active', 0),
(1124, 'Shirly Kwen Sabal', '   ', '', '', '', '', '', '', 'active', 0),
(1125, 'Shirra Mae Augusto', '   ', '', '', '', '', '', '', 'active', 0),
(1126, 'Simon Timmins', '   ', '', '', '', '', '', '', 'active', 0),
(1127, 'Skyline Advertising', '   ', '', '', '', '', '', '', 'active', 0),
(1128, 'SM Prime Holdings, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1129, 'SNESS Merchandising and Service', '   ', '', '', '', '', '', '', 'active', 0),
(1130, 'Social Security System', '   ', '', '', '', '', '', '', 'active', 0),
(1131, 'SOFO Construction Services', '73 Echavez st. Cebu City   ', '', '', '', '', '', '', 'active', 0),
(1132, 'Softwash Laundromat', '   ', '', '', '', '', '', '', 'active', 0),
(1133, 'Solid Construction (CEBU) Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1134, 'Solid Electronics Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1135, 'SONAMCO', '   ', '', '', '', '', '', '', 'active', 0),
(1136, 'Sophia Belle Tan', '   ', '', '', '', '', '', '', 'active', 0),
(1137, 'Sophia Godin', '   ', '', '', '', '', '', '', 'active', 0),
(1138, 'Sta. Ana, Rivera & Co.', '   ', '', '', '', '', '', '', 'active', 0),
(1139, 'Stefan Andrei Flores', '   ', '', '', '', '', '', '', 'active', 0),
(1140, 'Stefanie Guitguit', '   ', '', '', '', '', '', '', 'active', 0),
(1141, 'Stellaly Romares', '   ', '', '', '', '', '', '', 'active', 0),
(1142, 'Stephanie Noynay', '   ', '', '', '', '', '', '', 'active', 0),
(1143, 'Stephany Cuico', '   ', '', '', '', '', '', '', 'active', 0),
(1144, 'Stephen Low v', '   ', '', '', '', '', '', '', 'active', 0),
(1145, 'Stephen Rosales', '   ', '', '', '', '', '', '', 'active', 0),
(1146, 'Stronghold Glass and Aluminum Corp.', '   ', '', '', '', '', '', '', 'active', 0),
(1147, 'Sugar Ganancial', '   ', '', '', '', '', '', '', 'active', 0),
(1148, 'Sun.Star Publishing, Inc.', 'P.del Rosario St. Cebu City 000-565-060-000  ', '', '', '', '', '', '', 'active', 0),
(1149, 'Sunshine Pardiñan', '   ', '', '', '', '', '', '', 'active', 0),
(1150, 'Super Hankuk Mart & General Merchandise', '   ', '', '', '', '', '15 days', '', 'active', 0),
(1151, 'Susana Tero', '   ', '', '', '', '', '', '', 'active', 0),
(1152, 'Synchrotek Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1153, 'Tambuli Development Corp.', 'Mactan Island Cebu Philippines 004-488-112-000V ', '', '', '', '', '', '', 'active', 0),
(1154, 'Tanya Francia Yap v', '   ', '', '', '', '', '', '', 'active', 0),
(1155, 'Tanya Lozano', '   ', '', '', '', '', '', '', 'active', 0),
(1156, 'Temmy Meat Shop/Artemia C. Degamo', '   ', '', '', '', '', '', '', 'active', 0),
(1157, 'Teresa Avila', '   ', '', '', '', '', '', '', 'active', 0),
(1158, 'Teresita Susana Omanito', '   ', '', '', '', '', '', '', 'active', 0),
(1159, 'TESDA/Dervis Cenica', '   ', '', '', '', '', '', '', 'active', 0),
(1160, 'Tharsis Marine Sports', '   ', '', '', '', '', '', '', 'active', 0),
(1161, 'The Hive-All Visuals & Lights System', '   ', '', '', '', '', '', '', 'active', 0),
(1162, 'The Hongkong and Shanghai Banking Corpora', '   ', '', '', '', '', '', '', 'active', 0),
(1163, 'The Only Dewfoam Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1164, 'The Tile Shop', '   ', '', '', '', '', '', '', 'active', 0),
(1165, 'The Tile Shpo', '   ', '', '', '', '', '', '', 'active', 0),
(1166, 'Therese Wenneth Cabus', '   ', '', '', '', '', '', '', 'active', 0),
(1167, 'Thinking Tools, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1168, 'Thomas Joseph Keehan', '   ', '', '', '', '', '', '', 'active', 0),
(1169, 'Tiffany Kristine Tan', '   ', '', '', '', '', '', '', 'active', 0),
(1170, 'Tim Transport', '7-A Maria Cristina St., Cebu City  ', '', '', '', '', '', '', 'active', 0),
(1171, 'Tito Aldecoa III', '   ', '', '', '', '', '', '', 'active', 0),
(1172, 'Toni Enterprises', '   ', '', '', '', '', '', '', 'active', 0),
(1173, 'Toni Enterprisese', 'Saac II Mactan Lapu Lapu City 184-219-143-000NV  ', '340-4399', '', '', '', '', '', 'active', 0),
(1174, 'Toni Rose Tan', '   ', '', '', '', '', '', '', 'active', 0),
(1175, 'Tower Medical Distributing Co., Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1176, 'Tracy Lao', '   ', '', '', '', '', '', '', 'active', 0),
(1177, 'Treasure Island', '   ', '', '', '', '', 'Due on receipt', '', 'active', 0),
(1178, 'Trevor Trinidad', '   ', '', '', '', '', '', '', 'active', 0),
(1179, 'Tricom Systems (Philippines), Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1180, 'Tuzca Camille Cortes', '   ', '', '', '', '', '', '', 'active', 0),
(1181, 'Tzu Hsuan Tsai v', '   ', '', '', '', '', '', '', 'active', 0),
(1182, 'U-BIX Corporation', 'Dr.2 YMCA Arcade, Osmena Boulevard, Cebu City ', '416-5123', '', '', '', '', '', 'active', 0),
(1183, 'UHM SONS CORPORATION', '   ', '', '', '', '', '', '', 'active', 0),
(1184, 'Unicable TV, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1185, 'Union Bank of the Philippines', '   ', '', '', '', '', '', '', 'active', 0),
(1186, 'Universal Telecommunications Services,Inc', '4/F JRDC Bldg. Osmeña Blvd. Cebu City 000-830-195-000V ', '2545956', '', '', '', '', '', 'active', 0),
(1187, 'Van Joyce Sarcos', '   ', '', '', '', '', '', '', 'active', 0),
(1188, 'Vance Cedric Sy', '   ', '', '', '', '', '', '', 'active', 0),
(1189, 'Vanessa Marie Bonjoc', '   ', '', '', '', '', '', '', 'active', 0),
(1190, 'Vantage Point Global Trading', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1191, 'Various Accounts', '   ', '', '', '', '', '', '', 'active', 0),
(1192, 'Venice Mae Gilig', '   ', '', '', '', '', '', '', 'active', 0),
(1193, 'Vennette', '   ', '', '', '', '', '', '', 'active', 0),
(1194, 'Vennette Claire Silvosa', '   ', '', '', '', '', '', '', 'active', 0),
(1195, 'Venus Liquit', '   ', '', '', '', '', '', '', 'active', 0),
(1196, 'Veolia Water Solutions & Technologies', '   ', '', '', '', '', '', '', 'active', 0),
(1197, 'Verah Marie S. Almodal', '   ', '', '', '', '', '', '', 'active', 0),
(1198, 'Veronica Rich', '   ', '', '', '', '', '', '', 'active', 0),
(1199, 'Veya Faye Yu', '   ', '', '', '', '', '', '', 'active', 0),
(1200, 'Vicente Cabrera III', '   ', '', '', '', '', '', '', 'active', 0),
(1201, 'Victor Ma. Liza Mercader', '   ', '', '', '', '', '', '', 'active', 0),
(1202, 'Viena Rose Jumarito', '   ', '', '', '', '', '', '', 'active', 0),
(1203, 'Villahermosa Bus Liner', '   ', '', '', '', '', '', '', 'active', 0),
(1204, 'Vincent Guerra', '   ', '', '', '', '', '', '', 'active', 0),
(1205, 'Viner Pingol', '   ', '', '', '', '', '', '', 'active', 0),
(1206, 'Virgilio Lumapas', '   ', '', '', '', '', '', '', 'active', 0),
(1207, 'Virgilio V. Calonia', '   ', '', '', '', '', '', '', 'active', 0),
(1208, 'Virginia Food, Inc.', '   ', '', '', '', '', '', '', 'active', 0),
(1209, 'Virginia Monares', '   ', '', '', '', '', '', '', 'active', 0),
(1210, 'Virginia Roble', '   ', '', '', '', '', '', '', 'active', 0),
(1211, 'Visayan Educational Supply', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1212, 'VR & W COLLECIONE/ROMOFF', '3/F Krizia Building, Gorordo Avenue Lahug Cebu City  ', '', '', '', '', '', '', 'active', 0),
(1213, 'Wakaba Ito', '   ', '', '', '', '', '', '', 'active', 0),
(1214, 'Waterfront Cebu City Hotel & Casino', '   ', '', '', '', '', '', '', 'active', 0),
(1215, 'Welmer Lo-oc', '   ', '', '', '', '', '', '', 'active', 0),
(1216, 'WESTLINK Travel and Tours', '   ', '', '', '', '', '', '', 'active', 0),
(1217, 'Wilfredo Conejos, Jr.', '   ', '', '', '', '', '7 days', '', 'active', 0),
(1218, 'Willy Beth Barillo', '   ', '', '', '', '', '', '', 'active', 0),
(1219, 'Wilmer S. Maquilan', '   ', '', '', '', '', '', '', 'active', 0),
(1220, 'Wilter Cabarrubias', '   ', '', '', '', '', '', '', 'active', 0),
(1221, 'Windell Magale', '   ', '', '', '', '', '', '', 'active', 0),
(1222, 'Winnie Tugahan', '   ', '', '', '', '', '', '', 'active', 0),
(1223, 'Woo Soo Youn', '   ', '', '', '', '', '', '', 'active', 0),
(1224, 'Wrenleys Motor Plaza', '   ', '', '', '', '', '', '', 'active', 0),
(1225, 'WS Pacific Publications, Inc.(Cebu)', 'G/F Lyden Bldg. F Ramos St. Cogon Cebu City  ', '', '', '', '004-526-852-001', '', '', 'active', 0),
(1226, 'Y101', '   ', '', '', '', '', '', '', 'active', 0),
(1227, 'Yale Hardware', '   ', '', '', '', '', '', '', 'active', 0),
(1228, 'YN Realty Corporation', '   ', '', '', '', '', '', '', 'active', 0),
(1229, 'Yoko Povadora', '   ', '', '', '', '', '', '', 'active', 0),
(1230, 'Yuko Kishi', '   ', '', '', '', '', '', '', 'active', 0),
(1231, 'Zefren Suan', '   ', '', '', '', '', '', '', 'active', 0),
(1232, 'testing 123 purposes only', 'here', '134134', '419240912374', '120481209', 'here', 'here', 'here', 'active', 1318559747),
(1233, 'sfsd', 'sdsd', '125', 'fsdfsfdsfdsfsdsfsdfsdf', '', '', '', '', 'active', 1333353569);

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE IF NOT EXISTS `supplies` (
  `supply_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`supply_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `supplies`
--

INSERT INTO `supplies` (`supply_id`, `name`, `supplier_id`, `quantity`) VALUES
(1, 'cottons', 1103, 10);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(65) NOT NULL,
  `task_link` varchar(120) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_id`, `emp_id`) VALUES
(1, 1, 10),
(2, 14, 11),
(3, 3, 11),
(4, 4, 10),
(5, 5, 10),
(6, 6, 10),
(7, 7, 10),
(9, 9, 11),
(10, 10, 10),
(11, 11, 11),
(12, 3, 14),
(14, 13, 11),
(15, 16, 11),
(16, 39, 10),
(17, 2, 10),
(18, 16, 10),
(19, 2, 16),
(20, 18, 11),
(21, 15, 17),
(22, 19, 17),
(23, 48, 17),
(24, 20, 17),
(31, 26, 20),
(33, 28, 20),
(37, 30, 21),
(38, 29, 21),
(39, 25, 21),
(40, 27, 21),
(45, 33, 16),
(46, 30, 16),
(47, 31, 16),
(48, 32, 20),
(50, 37, 13),
(51, 38, 13),
(52, 36, 16),
(53, 34, 13),
(54, 35, 13),
(55, 18, 10),
(56, 40, 23),
(57, 41, 23),
(58, 42, 23),
(59, 43, 23),
(60, 44, 24),
(61, 45, 11),
(62, 46, 25),
(63, 47, 25),
(64, 9, 15),
(65, 51, 12),
(66, 21, 17),
(67, 11, 15),
(68, 13, 15),
(69, 34, 15),
(70, 49, 15),
(71, 50, 11),
(72, 12, 15),
(73, 52, 12),
(74, 53, 12),
(75, 54, 12),
(76, 55, 12);

-- --------------------------------------------------------

--
-- Table structure for table `task_master_list`
--

CREATE TABLE IF NOT EXISTS `task_master_list` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(45) NOT NULL,
  `link` varchar(120) NOT NULL,
  `class_icon` varchar(30) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `task_master_list`
--

INSERT INTO `task_master_list` (`task_id`, `task_name`, `link`, `class_icon`) VALUES
(1, 'Class Schedules', '#/task/class-schedule', 'sprite-create'),
(2, 'Change Form', '#/task/change-form', 'sprite-create'),
(3, 'Lesson Plan', '#/task/lesson-plan-create', 'sprite-create'),
(4, 'Books', '#/task/books-list', 'sprite-list'),
(5, 'Courses', '#/task/courses-list', 'sprite-list'),
(6, 'Course Levels', '#/task/course-levels-list', 'sprite-list'),
(7, 'Classrooms', '#/task/classrooms-list', 'sprite-list'),
(8, 'Create Form', '#/task/hotel-create/guest', 'sprite-create'),
(9, 'Accommodations', '#/task/hotel-list/guest', 'sprite-create'),
(10, 'Teachers', '#/task/teachers-list', 'sprite-list'),
(11, 'Rooms', '#/task/hotel-rooms', 'sprite-list'),
(12, 'Manage Rooms', '#/task/manage-hotel-rooms', 'sprite-create'),
(13, 'Daily Sales Rep', '#/task/hotel-daily-sales-report', 'sprite-create'),
(14, 'Class Schedule', '#/task/teacher-class-schedule', 'sprite-list'),
(15, 'Forms', '#/task/hr-forms', 'sprite-create'),
(16, 'Lesson Plans', '#/task/lesson-plans-list', 'sprite-list'),
(17, 'Asset Master List', '#/task/asset-master-list', 'sprite-list'),
(18, 'Change Requests', '#/task/change-requests', 'sprite-list'),
(19, 'Employees', '#/task/employees-list', 'sprite-list'),
(20, 'Contributions', '#/task/contributions-list', 'sprite-list'),
(21, 'Payroll', '#/task/libro_paga', 'sprite-create'),
(22, 'Stocks', '#/task/ops-stock', 'sprite-create'),
(24, 'Inventory', '#/task/ops-inventory', 'sprite-list'),
(25, 'Materials', '#/task/materials-list', 'sprite-list'),
(26, 'F and B', '#/task/ops-menu', 'sprite-create'),
(27, 'Clinic supplies', '#/task/clinic-supplies', 'sprite-list'),
(28, 'Forms', '#/task/ops-forms', 'sprite-create'),
(29, 'Students', '#/task/student-list', 'sprite-create'),
(30, 'Class Schedules', '#/task/class-list', 'sprite-create'),
(31, 'Grades', '#/task/grades', 'sprite-create'),
(32, 'Reports', '#/task/ops-reports', 'sprite-create'),
(33, 'Profile', '#/task/profile', 'sprite-create'),
(34, 'Room Status', '#/task/ops-room', 'sprite-list'),
(35, 'Forms', '#/task/ops-checklist', 'sprite-create'),
(36, 'Fees', '#/task/fees', 'sprite-list'),
(37, 'Stocks', '#/task/ops-hkstocks', 'sprite-create'),
(38, 'Inventory', '#/task/ops-hkinventory', 'sprite-create'),
(39, 'Students', '#/task/student-sched', 'sprite-list'),
(40, 'Price List', '#/task/ops-pricelist', 'sprite-list'),
(41, 'Inventory', '#/task/ops-lpinventory', 'sprite-create'),
(42, 'Incoming Stocks', '#/task/ops-lpinstocks', 'sprite-list'),
(43, 'Daily Sales', '#/task/ops-lpdreports', 'sprite-create'),
(44, 'Daily Sales', '#/task/ops-lpondutysales', 'sprite-create'),
(45, 'Payslip', '#/task/payslip', 'sprite-view1'),
(46, 'Utilities', '#/task/ops-mainutil', 'sprite-list'),
(47, 'Supplies', '#/task/ops-mainsupplies', 'sprite-create'),
(48, 'Department', '#/task/department-list', 'sprite-list'),
(49, 'Lp Day Sales', '#/task/fo-dsales-or', 'sprite-list'),
(50, 'Loans', '#/task/loans', 'sprite-list'),
(51, 'Sale / Rent', '#/task/allreal', 'sprite-real'),
(52, 'For Sale', '#/task/forSale', 'sprite-sale'),
(53, 'For Rent', '#/task/forRent', 'sprite-rent'),
(54, 'Looking For', '#/task/lookingFor', 'sprite-lookfor'),
(55, 'Join Us', '#/task/joinUs', 'sprite-real');

-- --------------------------------------------------------

--
-- Table structure for table `tax_base_over`
--

CREATE TABLE IF NOT EXISTS `tax_base_over` (
  `base_id` int(11) NOT NULL AUTO_INCREMENT,
  `over` decimal(8,2) NOT NULL,
  `base` decimal(8,2) NOT NULL,
  PRIMARY KEY (`base_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tax_base_over`
--

INSERT INTO `tax_base_over` (`base_id`, `over`, `base`) VALUES
(1, 0.00, 0.00),
(2, 0.05, 0.00),
(3, 0.10, 20.83),
(4, 0.15, 104.17),
(5, 0.20, 354.17),
(6, 0.25, 937.50),
(7, 0.30, 2083.33),
(8, 0.32, 5208.33);

-- --------------------------------------------------------

--
-- Table structure for table `tax_table`
--

CREATE TABLE IF NOT EXISTS `tax_table` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `exemption` int(11) NOT NULL,
  `base_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `tax_table`
--

INSERT INTO `tax_table` (`tax_id`, `status`, `exemption`, `base_id`) VALUES
(1, 'Zero', 1, 1),
(2, 'SME', 1, 1),
(3, 'SME', 2083, 2),
(4, 'SME', 2500, 3),
(5, 'SME', 3333, 4),
(6, 'SME', 5000, 5),
(7, 'SME', 7917, 6),
(8, 'SME', 12500, 7),
(9, 'SME', 22917, 8),
(10, 'SME1', 1, 1),
(11, 'SME1', 3125, 2),
(12, 'SME1', 3542, 3),
(13, 'SME1', 4375, 4),
(14, 'SME1', 6042, 5),
(15, 'SME1', 8958, 6),
(16, 'SME1', 13542, 7),
(17, 'SME1', 23958, 8),
(18, 'SME2', 1, 1),
(19, 'SME2', 4167, 2),
(20, 'SME2', 4583, 3),
(21, 'SME2', 5417, 4),
(22, 'SME2', 7083, 5),
(23, 'SME2', 10000, 6),
(24, 'SME2', 14583, 7),
(25, 'SME2', 25000, 8),
(26, 'SME3', 1, 1),
(27, 'SME3', 5208, 2),
(28, 'SME3', 5625, 3),
(29, 'SME3', 6458, 4),
(30, 'SME3', 8125, 5),
(31, 'SME3', 11042, 6),
(32, 'SME3', 15625, 7),
(33, 'SME3', 26042, 8),
(34, 'SME4', 1, 1),
(35, 'SME4', 6250, 2),
(36, 'SME4', 6667, 3),
(37, 'SME4', 7500, 4),
(38, 'SME4', 9167, 5),
(39, 'SME4', 12083, 6),
(40, 'SME4', 16667, 7),
(41, 'SME4', 27083, 8),
(42, 'Zero', 0, 2),
(43, 'Zero', 417, 3),
(44, 'Zero', 1250, 4),
(45, 'Zero', 2917, 5),
(46, 'Zero', 5833, 6),
(47, 'Zero', 10417, 7),
(48, 'Zero', 20833, 8);

-- --------------------------------------------------------

--
-- Table structure for table `unitsof_measure`
--

CREATE TABLE IF NOT EXISTS `unitsof_measure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `unitsof_measure`
--

INSERT INTO `unitsof_measure` (`id`, `name`) VALUES
(1, 'each'),
(2, 'kilo'),
(3, 'length'),
(4, 'liters'),
(5, 'meters'),
(6, 'bottle'),
(7, 'pack'),
(8, 'pcs'),
(11, 'others');

-- --------------------------------------------------------

--
-- Table structure for table `with_stock_tracker`
--

CREATE TABLE IF NOT EXISTS `with_stock_tracker` (
  `ws_tracker_id` int(11) NOT NULL AUTO_INCREMENT,
  `ws_tracker_type` int(11) NOT NULL,
  `ws_tracker_stock_id` int(11) NOT NULL,
  `ws_tracker_item_ctr` int(11) NOT NULL,
  `ws_binder_item_bal` int(11) NOT NULL,
  `ws_tracker_date` date NOT NULL,
  `ws_recipient` int(11) NOT NULL,
  PRIMARY KEY (`ws_tracker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `with_stock_tracker`
--

INSERT INTO `with_stock_tracker` (`ws_tracker_id`, `ws_tracker_type`, `ws_tracker_stock_id`, `ws_tracker_item_ctr`, `ws_binder_item_bal`, `ws_tracker_date`, `ws_recipient`) VALUES
(1, 1, 2, 20, 20, '2012-05-08', 12),
(3, 1, 10, 23, 23, '2012-05-08', 3),
(4, 1, 25, 57, 57, '2012-05-05', 3),
(5, 1, 26, 40, 40, '2012-05-07', 4),
(7, 1, 27, 70, 70, '2012-05-08', 3),
(135, 1, 2, 6, 26, '2012-05-11', 12),
(136, 1, 2, 5, 25, '2012-05-11', 12),
(137, 1, 27, 8, 78, '2012-05-11', 3),
(138, 1, 2, 9, 29, '2012-05-11', 12),
(139, 1, 2, 9, 29, '2012-05-15', 12),
(140, 1, 2, 45, 65, '2012-05-22', 12),
(141, 1, 2, 6, 26, '2012-05-22', 12),
(142, 1, 2, 6, 26, '2012-05-22', 12),
(143, 1, 2, 6, 26, '2012-05-22', 12),
(144, 1, 10, 6, 29, '2012-05-22', 3),
(145, 1, 10, 9, 32, '2012-05-22', 3),
(146, 1, 10, 0, 23, '2012-05-22', 3),
(147, 1, 26, 6, 46, '2012-05-22', 4);

-- --------------------------------------------------------

--
-- Table structure for table `with_stock_tracker_type`
--

CREATE TABLE IF NOT EXISTS `with_stock_tracker_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `with_stock_tracker_type`
--

INSERT INTO `with_stock_tracker_type` (`id`, `name`) VALUES
(1, 'IN'),
(2, 'OUT');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acad_classes`
--
ALTER TABLE `acad_classes`
  ADD CONSTRAINT `acad_classes_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `acad_classes_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  ADD CONSTRAINT `acad_classes_ibfk_3` FOREIGN KEY (`level_id`) REFERENCES `course_level` (`id`),
  ADD CONSTRAINT `acad_classes_ibfk_4` FOREIGN KEY (`classroom_id`) REFERENCES `classrooms` (`classroom_id`),
  ADD CONSTRAINT `acad_classes_ibfk_5` FOREIGN KEY (`teacher_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `acad_lesson_plans`
--
ALTER TABLE `acad_lesson_plans`
  ADD CONSTRAINT `acad_lesson_plans_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `acad_classes` (`class_id`),
  ADD CONSTRAINT `acad_lesson_plans_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `acad_lesson_plan_details`
--
ALTER TABLE `acad_lesson_plan_details`
  ADD CONSTRAINT `acad_lesson_plan_details_ibfk_1` FOREIGN KEY (`lesson_plan_id`) REFERENCES `acad_lesson_plans` (`lesson_plan_id`);

--
-- Constraints for table `accommodations`
--
ALTER TABLE `accommodations`
  ADD CONSTRAINT `accommodations_ibfk_1` FOREIGN KEY (`room_number`) REFERENCES `rooms_1` (`number`);

--
-- Constraints for table `admissions`
--
ALTER TABLE `admissions`
  ADD CONSTRAINT `admissions_ibfk_1` FOREIGN KEY (`stud_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `admissions_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  ADD CONSTRAINT `admissions_ibfk_3` FOREIGN KEY (`course_level`) REFERENCES `course_level` (`id`);

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_ibfk_1` FOREIGN KEY (`units`) REFERENCES `unitsof_measure` (`id`);

--
-- Constraints for table `asset_tracker`
--
ALTER TABLE `asset_tracker`
  ADD CONSTRAINT `asset_tracker_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`asset_id`),
  ADD CONSTRAINT `asset_tracker_ibfk_4` FOREIGN KEY (`from_emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `as_account_type`
--
ALTER TABLE `as_account_type`
  ADD CONSTRAINT `as_account_type_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `as_account_categories` (`id`);

--
-- Constraints for table `as_chartdetails`
--
ALTER TABLE `as_chartdetails`
  ADD CONSTRAINT `as_chartdetails_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`);

--
-- Constraints for table `as_chartofaccounts`
--
ALTER TABLE `as_chartofaccounts`
  ADD CONSTRAINT `as_chartofaccounts_ibfk_1` FOREIGN KEY (`account_type_id`) REFERENCES `as_account_type` (`id`);

--
-- Constraints for table `as_general_ledger`
--
ALTER TABLE `as_general_ledger`
  ADD CONSTRAINT `as_general_ledger_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`),
  ADD CONSTRAINT `as_general_ledger_ibfk_2` FOREIGN KEY (`payment_terms`) REFERENCES `as_payterms` (`id`);

--
-- Constraints for table `as_invitems`
--
ALTER TABLE `as_invitems`
  ADD CONSTRAINT `as_invitems_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`);

--
-- Constraints for table `as_purchase_orders`
--
ALTER TABLE `as_purchase_orders`
  ADD CONSTRAINT `as_purchase_orders_ibfk_1` FOREIGN KEY (`payment_method`) REFERENCES `as_paymeth` (`id`),
  ADD CONSTRAINT `as_purchase_orders_ibfk_3` FOREIGN KEY (`class`) REFERENCES `as_class` (`id`),
  ADD CONSTRAINT `as_purchase_orders_ibfk_5` FOREIGN KEY (`supplier`) REFERENCES `suppliers` (`supplier_id`);

--
-- Constraints for table `as_sales_postings`
--
ALTER TABLE `as_sales_postings`
  ADD CONSTRAINT `as_sales_postings_ibfk_2` FOREIGN KEY (`paymeth`) REFERENCES `as_paymeth` (`id`),
  ADD CONSTRAINT `as_sales_postings_ibfk_3` FOREIGN KEY (`class`) REFERENCES `as_class` (`id`);

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`);

--
-- Constraints for table `class_students`
--
ALTER TABLE `class_students`
  ADD CONSTRAINT `class_students_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `acad_classes` (`class_id`),
  ADD CONSTRAINT `class_students_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `daily_sales_report`
--
ALTER TABLE `daily_sales_report`
  ADD CONSTRAINT `daily_sales_report_ibfk_1` FOREIGN KEY (`filed_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `department_ibfk_2` FOREIGN KEY (`deptd_id`) REFERENCES `department_details` (`deptd_id`);

--
-- Constraints for table `department_details`
--
ALTER TABLE `department_details`
  ADD CONSTRAINT `department_details_ibfk_1` FOREIGN KEY (`dept_head`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `department_details_ibfk_2` FOREIGN KEY (`dept_head`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_contact_details`
--
ALTER TABLE `emp_contact_details`
  ADD CONSTRAINT `emp_contact_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_contract`
--
ALTER TABLE `emp_contract`
  ADD CONSTRAINT `emp_contract_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_contrib_details`
--
ALTER TABLE `emp_contrib_details`
  ADD CONSTRAINT `emp_contrib_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_dependents`
--
ALTER TABLE `emp_dependents`
  ADD CONSTRAINT `emp_dependents_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_education`
--
ALTER TABLE `emp_education`
  ADD CONSTRAINT `emp_education_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_emergency_contact`
--
ALTER TABLE `emp_emergency_contact`
  ADD CONSTRAINT `emp_emergency_contact_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_immigration`
--
ALTER TABLE `emp_immigration`
  ADD CONSTRAINT `emp_immigration_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_job_details`
--
ALTER TABLE `emp_job_details`
  ADD CONSTRAINT `emp_job_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `emp_job_details_ibfk_3` FOREIGN KEY (`job_title`) REFERENCES `job_title` (`id`),
  ADD CONSTRAINT `emp_job_details_ibfk_4` FOREIGN KEY (`curr_empstatus`) REFERENCES `employment_type` (`id`);

--
-- Constraints for table `emp_language`
--
ALTER TABLE `emp_language`
  ADD CONSTRAINT `emp_language_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_leaves`
--
ALTER TABLE `emp_leaves`
  ADD CONSTRAINT `emp_leaves_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_license`
--
ALTER TABLE `emp_license`
  ADD CONSTRAINT `emp_license_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_ot_ut_detail`
--
ALTER TABLE `emp_ot_ut_detail`
  ADD CONSTRAINT `emp_ot_ut_detail_ibfk_1` FOREIGN KEY (`emp_ot_ut_id`) REFERENCES `emp_overtime_undertime` (`emp_ot_ut_id`);

--
-- Constraints for table `emp_overtime_undertime`
--
ALTER TABLE `emp_overtime_undertime`
  ADD CONSTRAINT `emp_overtime_undertime_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_personal_details`
--
ALTER TABLE `emp_personal_details`
  ADD CONSTRAINT `emp_personal_details_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_skills`
--
ALTER TABLE `emp_skills`
  ADD CONSTRAINT `emp_skills_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `emp_work_experience`
--
ALTER TABLE `emp_work_experience`
  ADD CONSTRAINT `emp_work_experience_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `fb_menu`
--
ALTER TABLE `fb_menu`
  ADD CONSTRAINT `fb_menu_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `fb_menu_ibfk_2` FOREIGN KEY (`menu_name_id`) REFERENCES `menu_name` (`menu_name_id`),
  ADD CONSTRAINT `fb_menu_ibfk_3` FOREIGN KEY (`meal_type_id`) REFERENCES `menu_meal_type` (`meal_type_id`),
  ADD CONSTRAINT `fb_menu_ibfk_4` FOREIGN KEY (`ingredient_id`) REFERENCES `menu_status_id` (`ingredient_id`);

--
-- Constraints for table `guests_1`
--
ALTER TABLE `guests_1`
  ADD CONSTRAINT `guests_1_ibfk_1` FOREIGN KEY (`fo_staff`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `guest_accommodation_1`
--
ALTER TABLE `guest_accommodation_1`
  ADD CONSTRAINT `guest_accommodation_1_ibfk_1` FOREIGN KEY (`room_number`) REFERENCES `rooms_1` (`number`);

--
-- Constraints for table `job_functions`
--
ALTER TABLE `job_functions`
  ADD CONSTRAINT `job_functions_ibfk_1` FOREIGN KEY (`jobtitle_id`) REFERENCES `job_title` (`id`),
  ADD CONSTRAINT `job_functions_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`);

--
-- Constraints for table `menu_name`
--
ALTER TABLE `menu_name`
  ADD CONSTRAINT `menu_name_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `fb_menu` (`added_by`),
  ADD CONSTRAINT `menu_name_ibfk_2` FOREIGN KEY (`menu_name_id`) REFERENCES `fb_menu` (`menu_name_id`);

--
-- Constraints for table `payroll_details`
--
ALTER TABLE `payroll_details`
  ADD CONSTRAINT `payroll_details_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `payroll_details_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_adjustments`
--
ALTER TABLE `pay_adjustments`
  ADD CONSTRAINT `pay_adjustments_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_adjustments_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_allowances`
--
ALTER TABLE `pay_allowances`
  ADD CONSTRAINT `pay_allowances_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_allowances_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_canteen`
--
ALTER TABLE `pay_canteen`
  ADD CONSTRAINT `pay_canteen_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_canteen_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_holiday`
--
ALTER TABLE `pay_holiday`
  ADD CONSTRAINT `pay_holiday_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_holiday_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_night_diff`
--
ALTER TABLE `pay_night_diff`
  ADD CONSTRAINT `pay_night_diff_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_night_diff_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `pay_uniform`
--
ALTER TABLE `pay_uniform`
  ADD CONSTRAINT `pay_uniform_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `payrolls` (`payroll_id`),
  ADD CONSTRAINT `pay_uniform_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `prs`
--
ALTER TABLE `prs`
  ADD CONSTRAINT `prs_ibfk_1` FOREIGN KEY (`requested_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `prs_details`
--
ALTER TABLE `prs_details`
  ADD CONSTRAINT `prs_details_ibfk_1` FOREIGN KEY (`prs_id`) REFERENCES `prs` (`prs_id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`room_type_id`),
  ADD CONSTRAINT `rooms_ibfk_2` FOREIGN KEY (`room_number_id`) REFERENCES `room_number_ids` (`room_number_id`),
  ADD CONSTRAINT `rooms_ibfk_3` FOREIGN KEY (`room_status_id`) REFERENCES `room_status` (`room_status_id`),
  ADD CONSTRAINT `rooms_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `rooms_1`
--
ALTER TABLE `rooms_1`
  ADD CONSTRAINT `rooms_1_ibfk_1` FOREIGN KEY (`status`) REFERENCES `room_status_1` (`id`),
  ADD CONSTRAINT `rooms_1_ibfk_2` FOREIGN KEY (`type`) REFERENCES `room_type_1` (`id`);

--
-- Constraints for table `rooms_accom`
--
ALTER TABLE `rooms_accom`
  ADD CONSTRAINT `rooms_accom_ibfk_1` FOREIGN KEY (`room_number_id`) REFERENCES `room_number_ids` (`room_number_id`),
  ADD CONSTRAINT `rooms_accom_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `room_number_ids`
--
ALTER TABLE `room_number_ids`
  ADD CONSTRAINT `room_number_ids_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `room_rates`
--
ALTER TABLE `room_rates`
  ADD CONSTRAINT `room_rates_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`);

--
-- Constraints for table `room_type`
--
ALTER TABLE `room_type`
  ADD CONSTRAINT `room_type_ibfk_1` FOREIGN KEY (`room_rate_id`) REFERENCES `room_rates` (`room_rate_id`);

--
-- Constraints for table `srs`
--
ALTER TABLE `srs`
  ADD CONSTRAINT `srs_ibfk_1` FOREIGN KEY (`requested_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `srs_ibfk_2` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`stock_id`);

--
-- Constraints for table `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_ibfk_2` FOREIGN KEY (`units`) REFERENCES `unitsof_measure` (`id`),
  ADD CONSTRAINT `stocks_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `stocks_ibfk_5` FOREIGN KEY (`stock_type`) REFERENCES `stock_type` (`id`);

--
-- Constraints for table `stock_items`
--
ALTER TABLE `stock_items`
  ADD CONSTRAINT `stock_items_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `as_chartofaccounts` (`account_code`),
  ADD CONSTRAINT `stock_items_ibfk_2` FOREIGN KEY (`unitsof_measure`) REFERENCES `unitsof_measure` (`id`);

--
-- Constraints for table `students_accommodation_1`
--
ALTER TABLE `students_accommodation_1`
  ADD CONSTRAINT `students_accommodation_1_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `students_accommodation_1_ibfk_2` FOREIGN KEY (`room_no`) REFERENCES `rooms_1` (`number`);

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp_user` (`emp_id`),
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task_master_list` (`task_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
