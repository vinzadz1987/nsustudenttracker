<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js oldie ie6" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js oldie ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js oldie ie8" lang="en"><![endif]-->
<!--[if gt IE 8]<!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title>Enterprise Resource Planning</title>
	<link rel="stylesheet" href="res/css/reset.css"/>
	<link rel="stylesheet" href="res/css/style.css"/>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header-background">
				<div id="header-extras">
					<div id="tagline-extra">
						<p class="international">think International</p>
						<p class="cleverlearn">think Cleverlearn</p>
					</div>
					<div id="status-extra">
						<div class="timer">Tuesday, November 29 - 6:00 PM</div>
						<div class="name-logout">Juan Dela Cruz - Logout</div>
					</div>
					<div class="clr"></div>
				</div>
				<div id="main-menu">
					<ul>
						<li class="active-menu"><a href="#">Task</a></li>
						<li><a href="#">Profile</a></li>
						<li><a href="#">Forms</a></li>
						<li><a href="#">Assets</a></li>
						<li><a href="#">Notifications</a></li>
					</ul>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<div id="sub-menu">
			<ul>
				<li><a href="#">Edit Profile</a></li>
			</ul>
		</div>
		<div id="container">
			<div class="container-header">
				Create forms
			</div>
			<div class="container-contents">
				contents
			</div>
		<!--
			<div class="profile-container">
				<div class="profile-photo">
					?
				</div>
				<div class="profile-menu">
					<ul>
						<li class="active-pmenu sprite-create"><a href="#">Create</a></li>
						<li class="sprite-list"><a href="#">List</a></li>
						<li class="sprite-dsp"><a href="#">Daily Sales Rep</a></li>
					</ul>
				</div>
			</div>
			<div class="contents">
				<div class="content-header">Actions</div>
				<div class="content">
					sdfasdf
				</div>
			</div>
			<div class="clr"></div>
		 -->
		</div>
            <div id="footer">
				<p>&copy; Cleverlearn Cebu</p>
				<p>Enterprise Resource Planning</p>
			</div>
	</div>
</body>
</html>