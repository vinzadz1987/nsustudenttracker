<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js oldie ie6" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js oldie ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js oldie ie8" lang="en"><![endif]-->
<!--[if gt IE 8]<!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title>Real State Online MarketPlace, Roxas City, Capiz</title>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/reset.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/style.css"/>
        <link rel="stylesheet" href="<?php print base_url();?>res/css/litebox.css"/>
        <link rel="stylesheet" href="<?php print base_url();?>res/css/facebox.css"/>
        <link rel="stylesheet" href="<?php print base_url();?>res/css/printbox.css"/>
    <link rel="stylesheet" href="<?php print base_url();?>res/css/chan_style.css"/>
    <link rel="stylesheet" href="<?php print base_url();?>res/css/smoothness/jquery-ui-1.8.16.custom.css"/>
    <link rel="stylesheet" href="<?php print base_url();?>res/css/lionbars.css"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<script data-main="<?php print base_url();?>res/js/main" src="<?php print base_url();?>res/js/libs/require/require.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
		</div>
		<div id="main-menu">
					<ul>
						
						<li class="active-menu"><a href="#/task">Real State</a></li>
						
						<label class="real_log"> Real State Online MarketPlace </label>
						<div class="to-right input">
						<script type="text/javascript">
						function DisplayTime(){
							if (!document.all && !document.getElementById)
							return
							timeElement=document.getElementById? document.getElementById("curTime"): document.all.tick2
							var CurrentDate=new Date()
							var hours=CurrentDate.getHours()
							var minutes=CurrentDate.getMinutes()
							var seconds=CurrentDate.getSeconds()
							var DayNight="PM"
							if (hours<12) DayNight="AM";
							if (hours>12) hours=hours-12;
							if (hours==0) hours=12;
							if (minutes<=9) minutes="0"+minutes;
							if (seconds<=9) seconds="0"+seconds;
							var currentTime=hours+":"+minutes+":"+seconds+" "+DayNight;
							timeElement.innerHTML="<font style='font-family:verdana, arial,tahoma;font-size:12px;color:white; font-weight:bold;'>"+currentTime+""
							setTimeout("DisplayTime()",1000)
						}
							window.onload=DisplayTime
						</script>
							<?php echo date('l, F d, Y'); ?> | <span id=curTime></span> |
							<?php echo $this->session->userdata['emp_info']['fullname']; ?> -
							<a href="<?php print base_url()."user/logout";?>">Login</a>
						</div>
					</ul>
		</div>
		<center><img src="<?php print base_url();?>res/img/ui/utah-real-estate.png" width="1000" height="250"/></center>
		<div id="container">
		</div>
        <div id="footer">
			<p>&copy; Real State Capiz</p>
			<p>Real State Online</p>
		</div>
	</div>
</body>
</html>