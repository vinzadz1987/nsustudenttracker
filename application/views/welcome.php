<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="robots" content="noindex, nofollow" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resources/css/login.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resources/css/litebox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resources/css/facebox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/res/css/fontstyle.css" />
	<link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css" />
	<script type="text/javascript" src="<?php echo base_url();?>/resources/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/resources/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/resources/js/jquery.litebox.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/resources/js/jquery.facebox.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/resources/js/login.js"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>
	<title>NSU Student Tracker System</title>
</head>
<body>

<style type="text/css">
	div#loginSelectContainer {
		padding-left: 10%;
		width: 981px;
		margin: 250px auto;
	}
	.box {
		width:70%;
		height:185px;
		background:#FFF;
		margin:40px auto;
	}
	.effect7
	{
		position:relative;
		-webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
		-moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
		box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
	}
	.effect7:before, .effect7:after
	{
		content:"";
		position:absolute;
		z-index:-1;
		-webkit-box-shadow:0 0 20px rgba(0,0,0,0.8);
		-moz-box-shadow:0 0 20px rgba(0,0,0,0.8);
		box-shadow:0 0 20px rgba(0,0,0,0.8);
		top:0;
		bottom:0;
		left:10px;
		right:10px;
		-moz-border-radius:100px / 10px;
		border-radius:100px / 10px;
	}
	.effect7:after
	{
		right:10px;
		left:auto;
		-webkit-transform:skew(8deg) rotate(3deg);
		-moz-transform:skew(8deg) rotate(3deg);
		-ms-transform:skew(8deg) rotate(3deg);
		-o-transform:skew(8deg) rotate(3deg);
		transform:skew(8deg) rotate(3deg);
	}
	.icon_image {
		height: 175px;
		width: 151px;
		padding: 9px;
	}
	#wrapper{
		margin: 50px auto;
		position: absolute;
		left: 43%;
		top: 203px;
	}
</style>

<div class="clr log_msg">
	<?php echo '<div id="logmsg">'.$this->session->flashdata('logmsg').'</div>';?>
</div>
<div class="word_split" id="wrapper">
	<span class="word word5">CLICK TO LOGIN</span>
</div>
<div id="loginSelectContainer">	
	<div class="loginSelect" style="width: 190px; padding: 22px;">
		<div class="box effect7" id="Teacher" style="width: 177px;">
			<img class="icon_image" src="https://s-media-cache-ak0.pinimg.com/originals/18/47/84/184784df749e62cdb3d0b8bea00136d3.jpg">
		</div>	
	</div>
	<div class="loginSelect" style="width: 190px; padding: 22px;">
		<div class="box effect7" id="Student" style="width: 170px;">
			<img class="icon_image" src="http://img10.deviantart.net/9013/i/2015/356/a/e/chibi_artist_by_by_art-d9l1yp0.png">
		</div>
	</div>
	<div class="loginSelect" style="width: 190px; padding: 22px;">
		<div class="box effect7" id="Parent" style="width: 170px;">
			<img class="icon_image" src="http://skoogle.in/images/parent.png">
		</div>
	</div>
</div>

<div class="loginContainer">
	<span class="loginHeader">Sales</span>
	<div class="loginimg"></div>
	<div class="loginForm">
		<?php echo form_open('login/submit');?>
			<label class="font">Username:</label>
			<input class="imginput" type="text" name="uname" value="" /> <br />
			<label class="font">Password:</label>
			<input class="imginput" type="password" name="passwd" value="" /> <br />
			<input type="hidden" name="dept" value="" />
			<button class="bluebutton" type="submit">login</button>
		<?php echo form_close();?>
	</div>
</div>
<div class="loginContainer2">
	<div class="loginForm2">
		<?php echo form_open('login/submit2');?>
			<label class="font hide">Username:</label>
			<label class="hide"><input class="imginput" type="text" name="uname" value="guest"> <br /></label>
			<label class="hide">Password:</label>
			<label class="hide"><input class="imginput" type="password" name="passwd" value="login" /> <br /></label>
			<input type="hidden" name="dept" value="7" />
			<button class="bluebutton2" type="submit">Loading....</button>
		<?php echo form_close();?>
	</div>
</div>
</body>
</html>

<!-- <div class="container">
  <div class="word_split wrapper">
  <span class="word word1"> Antony </span>
  <span class="word word2">Smith</span>
  <span class="word word3">Personal</span>
  <span class="word word4">Details</span>
  <span class="word word5">Employment</span>
  <span class="word word6">History </span>
  <span class="word word7">Education</span>
  <span class="word word8">Personal</span>
  <span class="word word9">Skills </span>
  <span class="word word10">Technical</span>
  <span class="word word11">Skills </span>
  <span class="word word12">Get In </span>
  <span class="word word13">Touch</span>
    
    <div class="bubble111">
     <span class="the-arrow1"></span>
      HI! <br/> I AM..
   </div>
  <div class="bubble2">
     <span class="the-arrow2"></span>
    <img src="https://dribbble.s3.amazonaws.com/users/10958/screenshots/271458/librarian.jpg"/>
   </div>
  <div class="bubble3">
     <span class="the-arrow3"></span>
    NATIONALITY...<br/>
    LOCATION...<br/>
    BIRTHDAY...<br/>
    HOBBIES<br/>
    ETC...<br/>
    ETC...<br/>
   </div>
  <div class="bubble4">
     <span class="the-arrow4"></span>
    GRAPHIC DESIGNER 2005 - 2007<br/>
    Lorem Ipsum dolor sit amet. Lorem Ipsum dolor.<br/><br/>
    CREATIVE DIRECTOR 2008 - Current
    <br/>
    Lorem Ipsum dolor sit amet.
    
   </div>
  <div class="bubble5">
     <span class="the-arrow5"></span>
    HIGH SCHOOL<br/>
    Lorem Ipsum dolor sit amet<br/>
    May 2004, GPA 1.5<br/><br/>
    UNIVERSITY <br/>
    Lorem Ipsum dolor sit amet<br/>
    July 2007, GPA 1.5
    
   </div>
  <div class="bubble6">
     <span class="the-arrow6"></span>
    SOCIAL COMMITMENT<br/>
    ORGANIZATION<br/>
    CREATIVITY<br/>
    COMMUNICATION<br/>
    TEAMWORK<br/>
   </div>
  <div class="bubble7">
     <span class="the-arrow7"></span>
    PHOTOSHOP<br/>
    ILLUSTRATOR<br/>
    INDESIGN<br/>
    FLASH<br/>
    DREAMWEAVER<br/>
    XHTML/CSS<br/>
    JAVASCRIPT<br/>
   </div>
  <div class="bubble8">
     <span class="the-arrow8"></span>
    PHONE...<br/>
    EMAIL...<br/>
    WEBSITE... <br/>
    TWITTER...<br/>
    FACEBOOK...<br/>
    DRIBBBLE...<br/>
   </div>   
</div>
   
</div><!--end container-->



<!-- <?php echo form_open('login/submit2');?>
<label class="font hide">Username:</label>
<label class="hide"><input class="imginput" type="text" name="uname" value="guest"> <br /></label>
<label class="hide">Password:</label>
<label class="hide"><input class="imginput" type="password" name="passwd" value="login" /> <br /></label>
<input type="hidden" name="dept" value="7" />
<?php echo form_close();?>  -->