<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<title>Real State Online MarketPlace, Roxas City, Capiz</title>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/reset.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/style.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/ligthbox.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/litebox.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/facebox.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/printbox.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/chan_style.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/smoothness/jquery-ui-1.8.16.custom.css"/>
	<link rel="stylesheet" href="<?php print base_url();?>res/css/lionbars.css"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<script data-main="<?php print base_url();?>res/js/main" src="<?php print base_url();?>res/js/libs/require/require.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="header"></div>
			<div id="main-menu">
			<ul>
				<li class="active-menu"><a href="#/task">Home</a></li>
				<li><a href="#/profile"><img src="<?php print base_url();?>res/img/photos/102.jpg" width="25" height="25"/><?php echo $this->session->userdata['emp_info']['fullname']; ?></a></li>
				<label class="real_log"> Real State Online MarketPlace </label>
				<div class="to-right input">
					<script type="text/javascript">
						window.onload=DisplayTime
					</script>
					<?php echo date('l, F d, Y'); ?> | <span id=curTime></span> |
					<a href="<?php print base_url()."user/logout";?>">Logout</a>
				</div>
			</ul>
			</div>
		 <center><img src="<?php print base_url();?>res/img/ui/utah-real-estate.png" width="1000" height="250"/></center>
		<div id="container"></div>
		<div id="footer">
			<p>&copy; Real State Capiz</p>
			<p>Real State Online</p>
		</div>
	</div>
</body>
</html>