<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resources/css/login.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resources/css/litebox.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resources/css/facebox.css" />
    <script type="text/javascript" src="<?php echo base_url();?>/resources/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/resources/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/resources/js/jquery.litebox.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/resources/js/jquery.facebox.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/resources/js/login.js"></script>
    <title>Real State</title>
</head>
<body>
    <div id="loginSelectContainer">
        <center>
            <div class="loginSelect" id="logCountryHead"></div>
            <?php echo '<div id="logmsg">'.$this->session->flashdata('logmsg').'</div>';?>
        </center>
    </div>
    <div class="loginContainer">
        <span class="loginHeader">Sales</span>
        <div class="loginForm">
            <?php echo form_open('login/submit');?>
            <label>Username:</label>
            <input class="imginput" type="text" name="uname" /> <br />
            <label>Password:</label>
            <input class="imginput" type="password" name="passwd" /> <br />
            <button class="bluebutton" type="submit">login</button>
            <?php echo form_close();?>
        </div>
    </div>
</body>
</html>