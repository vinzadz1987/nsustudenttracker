<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class forRent extends CI_Controller {
   var $user_session; var $guest_name;
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->user_session=$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_realforRent": $this->get_all_real_infoAllreal();break;
            case "all_advertiser": $this->get_advertiser();break;
            case "init": $this->_init();break;
            case "get_detailsforRent": $this->_real_detailsforRent();break;
            case "sendmessage": $this->_sendmessage();break;
            case "update_contentsforRent": $this->_update_real_contentsforRent(); break;
            case "delete_advert": $this->_delete_advert(); break;
            default: show_404();
        }
    }

     private function _sendmessage() {
        
                $received = $this->input->post('data');
                $now = date('F d, Y');
		$time_now=mktime(date('h')+5,date('i')+30);
		$time = date('h:i a', $time_now);
		
        $values = array(
            
			'real_id'		=>$received['real_id'],
                        'message_status'        =>"2",
                        'sendby'		=>$received['sendby'],
                        'sendto'		=>$received['sendto'],
                        'mobileno'		=>$received['mobno'],
                        'telno'                 =>$received['telno'],
                        'text'                  =>$received['text'],
                        'date'                  =>$now,
                        'time'                  =>$time
			
        );
        if($received['text']!="") {
        if($this->db->insert('real_messaging',$values)) {
            
		 echo "added";
		
        } else {
            echo "failed";
        }
        }else{
            echo"failed";
        }
    } 
    
      private function _update_real_contentsforRent() {
        $received = $this->input->post('data');
        $values = array(

            "real_id"       =>$received['real_id'],
            "title"         =>$received['title'],
            "loc_id"        =>$received['location'],
            "price"         =>$received['price'],
            "date"          =>$received['date'],
            "time"          =>$received['time'],
            "description"   =>$received['desc'],
            "bedrooms"      =>$received['bedrms'],
            "real_hold_id"  =>$received['cat'],
            "cat_id"        =>$received['stype'],
            "property"      =>$received['prop']
        );
        $data['result'] = ($this->db->update('real_state_info',$values,"real_id = ".$received['real_id'])) ? true:false;
        echo json_encode($data);
    }

	public function get_all_real_infoAllreal($offset=0) {
			$user_id =  $this->user_session['id'];
                        $guest_name = "12";
                        $now = date('F d, Y');
			if($this->input->post('data')) {
                        $rcv = $this->input->post('data');
			$loc = ($rcv['loc']!="") ? "AND rsi.loc_id = '".$rcv['loc']."'":"";
			$cat = ($rcv['cat']!="") ? "AND rsi.cat_id = '".$rcv['cat']."'":"";
			$prop = ($rcv['prop']!="") ? "AND rsi.property = '".$rcv['prop']."'":"";

			$real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname lname,
											 rp.name AS realProp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
										    ".$loc." ".$cat." ".$prop."
											AND rsi.title LIKE '".$rcv['srch']."%'
									  ORDER BY rsi.date DESC, rsi.time DESC LIMIT 10 OFFSET ".$offset);


                        $count = "SELECT rsi.*, COUNT(rsi.real_id) as countall, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname lname, rp.name as realProp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
                                                                                        ".$loc." ".$cat." ".$prop."
											AND rsi.title LIKE '".$rcv['srch']."%'
                                                                                        ORDER BY rsi.date DESC, rsi.time";
                        $countit = $this->db->query($count);
                        
			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS fname, epd.lastname lname,
											   rp.name AS realProp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, real_property rp
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
										    ".$loc." ".$cat." ".$prop."
										AND rsi.title LIKE '".$rcv['srch']."%'
									  ");


			/*if($real->num_rows()==0){

			$real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											epd.firstname AS fname, epd.lastname lname, rp.name AS realProp
									  FROM real_state_info rsi, real_location rl, real_hold rh
									  		real_category rc, emp_personal_details epd, real_property rp
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id											AND rsi.added_by = {$user_id}
											".$loc." ".$cat." ".$prop."
									  		AND rsi.title LIKE '".$rcv['srch']."%'
									  ORDER BY rsi.date DESC, rsi.time DESC LIMIT 10 OFFSET ".$offset);


			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS fname, epd.lastname lname,
											   rp.name AS realProp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, real_property rp
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
											  ".$loc." ".$cat." ".$prop."
										AND rsi.title LIKE '".$rcv['srch']."%'");

            }

*/

			} else {

			$real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname lname, rp.name as realProp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
									  ORDER BY rsi.date DESC, rsi.time DESC LIMIT 10 OFFSET ".$offset);

			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS fname, epd.lastname lname,
											   rp.name as realProp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, real_property rp
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
											 ");
                         $count = "SELECT rsi.*, COUNT(rsi.real_id) as countall, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname lname, rp.name as realProp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id AND rsi.real_hold_id=2
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.property=rp.property_id
                                                                                         ORDER BY rsi.date DESC, rsi.time";
                        $countit = $this->db->query($count);

			}

			$config['base_url'] = base_url().'rs/forRent/get_all_real_infoAllreal/';
			$config['total_rows'] = $_count->row()->rows;
			$config['uri_segment'] = 4;
			$config['per_page'] = 10;
			$this->pagination->initialize($config);
			$data['num_rows'] = $real->num_rows();
			$data['pagination'] = $this->pagination->create_links();

			$data['res'] = "";

                        if($real->num_rows()<=0) {
                            $data['res'] .= '<div class="countbox menucenter">Not found</div>';
                        }else{

			if($real->num_rows()>0 OR $countit->num_rows()>0) {

				$data['res'] .= '<div class="countbox2"> '.$countit->row()->countall.' For Rent</div>

                            <div class="items_list_container1"></div>

								';

			foreach($real->result() as $row) {

				$data['res'] .='
								<div class="items-contents1">
								<div>
									<table>
										<tr>'; if(($row->pic1) =="" && ($row->pic2)==""&& ($row->pic3)=="" && ($row->pic4)<1 && ($row->pic5)=="") {

											$data['res'].='<td><img src="./res/img/statephotos/12.jpg" class="mainfirst"></td>
											<td class="small_photos">
												<img src="./res/img/statephotos/12.jpg" class="first">
												<img src="./res/img/statephotos/12.jpg" class="first">
												<img src="./res/img/statephotos/12.jpg" class="first">
												<img src="./res/img/statephotos/12.jpg" class="first">
											</td>';
                                                                                } else {
                                                                                        $data['res'].='<td><img src="./res/img/statephotos/'.$row->pic1.'" class="mainfirst"></td>
											<td class="small_photos">
												<img src="./res/img/statephotos/'.$row->pic2.'" class="first">
												<img src="./res/img/statephotos/'.$row->pic3.'" class="first">
												<img src="./res/img/statephotos/'.$row->pic4.'" class="first">
												<img src="./res/img/statephotos/'.$row->pic5.'" class="first">
											</td>';
                                                                                }

										$data['res'] .='</tr>
									</table>
								</div>
								<div id="contentsInforeal">
									<label class="titleReal"><a id="ti_'.$row->real_id.'" class="title_real_forRent">
										'.ucfirst($row->title).' at '.$row->real_loc.'</a></label><br>
									<label class="st-text">Price:</label> '.$row->price.'<br>';
                                                                        if ($row->date=="$now")  {
                                                                         $data['res'] .='<label class="st-text">New Post:</label> Today at '.$row->time.'<br>';
                                                                        }else if($row->date>"$now") {
                                                                         $data['res'] .='<label class="st-text">Advance Post:</label> '.$row->date.' at '.$row->time.'<br>';
                                                                        }
                                                                        else {
                                                                         $data['res'] .='<label class="st-text">Posted on:</label> '.$row->date.' at '.$row->time.'<br>';
                                                                        }
									$data['res'] .='
									<label class="st-text">Description:</label>
										'.ucfirst(substr($row->description,1,80)).'....<br>
									<label class="st-text">Bedrooms:</label><label class=""> '.$row->bedrooms.'</label><br>';
                                                                        if($row->added_by==$user_id) {
									$me = "Me";
                                                                                $data['res'] .='<img src="./res/img/photos/'.$row->added_by.'.jpg" class="first1"/>';
										$data['res'] .='<label> '.$me.'</label><br>
										';
									}else {
                                                                            $data['res'] .='<img src="./res/img/photos/'.$row->added_by.'.jpg" class="first1"/>';
                                                                            $data['res'] .='<label></label> '.ucfirst($row->fname).' '.ucfirst($row->lname).'<br>';
                                                                        }
                                                                        if($row->added_by=="$user_id") {
									$data['res'] .='<button class="blue-button UploadRealPhotos" id="urp_'.$row->real_id.'">Update / Upload Photos</button><button class="blue-button deleteAdvert" id="da_'.$row->real_id.'">Delete</button>';
                                                                        }else if($this->user_session['id']=="107"){
                                                                        $data['res'] .='<button class="blue-button UploadRealPhotos" id="urp_'.$row->real_id.'">Update / Upload Photos</button><button class="blue-button deleteAdvert" id="da_'.$row->real_id.'">Delete</button>';
                                                                        }else{}
                                                                        $data['res'] .='';

                                                                
									
								
									

								$data['res'] .='</div></div></div>';
			}
                        }
			$data['res'] .= '';
			}
			echo json_encode($data);
		}


        public function get_advertiser($offset=0) {

			if($this->input->post('like')!="") {


			$real = $this->db->query("SELECT rsi.*, ecd.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                        AND rsi.added_by=ecd.emp_id
									  		AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'
									  		GROUP BY rsi.added_by DESC LIMIT 15 OFFSET ".$offset);


			$_count = $this->db->query("SELECT rsi.*, ecd.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS adfname, epd.lastname AS adlname,
											   COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, emp_contact_details ecd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                         AND rsi.added_by=ecd.emp_id
										     AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'");

			}else {

			$real = $this->db->query("SELECT rsi.*, ecd.*,	rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                        AND rsi.added_by=ecd.emp_id
									  GROUP BY rsi.added_by ORDER BY cp DESC LIMIT 15 OFFSET ".$offset);

			$_count = $this->db->query("SELECT rsi.*, ecd.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.lastname AS adlname,
											   epd.firstname AS adfname, COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, emp_contact_details ecd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                         AND rsi.added_by=ecd.emp_id");

			}

		/*	$config['base_url'] = base_url().'rs/allreal/get_advertiser/';
			$config['total_rows'] = $_count->row()->rows;
			$config['uri_segment'] = 4;
			$config['per_page'] = 15;
			$this->pagination->initialize($config);
			$data['num_rows'] = $real->num_rows();
			$data['pagination'] = $this->pagination->create_links();
		*/
			$data['res_advertiser'] = "";

			if($real->num_rows()>0) {

					$data['res_advertiser'] .= '
                            <div class="advertiser">
								';

			foreach($real->result() as $row) {

				$data['res_advertiser'] .='
								<div class="advert_count">
									<img src="./res/img/photos/'.$row->added_by.'.jpg" width="50" height="50" class="advert_photos"/>
									'.ucfirst($row->adfname).' '.ucfirst($row->adlname).' <label class="locatReal"></label>
									<label class="locatReal"><div class="to-right cp">'.$row->cp.' Total Advertisement</div></label>
                                                                        <div class="prof class-form"><table><tr>
                                                                            <td><label><img src="./res/img/photos/'.$row->added_by.'.jpg" width="100" height="100" class="advert_photos"/><label></td>
                                                                            <td><label class="st-text"> Name:</label> <label>'.ucfirst($row->adfname).' '.ucfirst($row->adlname).'</label><br>
                                                                                <label class="st-text">Mobile No:</label><label> '.$row->mobileno.'</label><br>
                                                                                <label class="st-text">Tel. No:</label><label> '.$row->telno.'</label><br>
                                                                                <label class="st-text">Email:</label><label> '.$row->email.'</label>
                                                                            </td></tr></table>
                                                                        </div>
								</div>
							  ';

			}
			$data['res_advertiser'] .= '</div>';
			}
			echo json_encode($data);
    }



    private function _real_detailsforRent() {
           $user = $this->user_session['id'];
           $now = date('F d, Y');
           $real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname AS lname, rp.name AS propname,
                                                                                         ecd.street AS addressname, ecd.telno AS telno, ecd.mobileno AS mobno, ecd.email AS email
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp,
                                                                                        emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.added_by=ecd.emp_id
                                                                                        AND rsi.property = rp.property_id
											AND rsi.real_id ='".$this->input->post('bid')."'");


			$data['res_advertiser'] = "";

			if($real->num_rows()>0) {

					$data['res_advertiser'] .= '
								';

			foreach($real->result() as $row) {

				$data['res_advertiser'] .='
								<div class="advert_info">
									<img src="./res/img/photos/'.$row->added_by.'.jpg" class="realPhotos3"/>
									<label class="advert_info2">Advertiser:</label> ';
                                                                        if($row->added_by=="$user") {

                                                                            $user_id = 'Me';

                                                                            $data['res_advertiser'] .='<label class="titleReal">'.$user_id.'</label>';

                                                                        } else {

                                                                         $data['res_advertiser'] .='<label class="titleReal">'.ucfirst($row->fname).' '.ucfirst($row->lname).'</label>';

                                                                        }
									$data['res_advertiser'] .=' | <label class="advert_info2">Title:</label>
									<label class="titleReal">'.ucfirst($row->title).' at '.$row->real_loc.'</label>

								   <div class="sprite-close to-right close closeAdvertContent"></div>
								</div>
									<table>
										<tr>';
                                                                                if(($row->pic1) =="" && ($row->pic2)==""&& ($row->pic3)=="" && ($row->pic4)<1 && ($row->pic5)=="") {

                                                                                    $data['res_advertiser'] .='<td>
										<div class="shadowbox3">
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-1 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-2 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-3 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-4 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-5 hide"/>
										</div>
										</td>
										<td>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 1" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 2" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 3" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 4" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 5" title="Click to view larger."/><br />
										</td>';

                                                                                }else {

										$data['res_advertiser'] .='<td>
										<div class="shadowbox3">
											<img src="./res/img/statephotos/'.$row->pic1.'" class="realPhotos2 0"/>
											<img src="./res/img/statephotos/'.$row->pic1.'" class="realPhotos2 0-1 hide"/>
											<img src="./res/img/statephotos/'.$row->pic2.'" class="realPhotos2 0-2 hide"/>
											<img src="./res/img/statephotos/'.$row->pic3.'" class="realPhotos2 0-3 hide"/>
											<img src="./res/img/statephotos/'.$row->pic4.'" class="realPhotos2 0-4 hide"/>
											<img src="./res/img/statephotos/'.$row->pic5.'" class="realPhotos2 0-5 hide"/>
										</div>
										</td>
										<td>
											<img src="./res/img/statephotos/'.$row->pic1.'" class="realPhotos 1" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic2.'" class="realPhotos 2" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic3.'" class="realPhotos 3" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic4.'" class="realPhotos 4" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic5.'" class="realPhotos 5" title="Click to view larger."/><br />
										</td>';
                                                                                }
										$data['res_advertiser'] .='<td><br>
											<div class="postAdvert">CONTENT INFORMATION:</div>
                                                                                        <div class="postAdvert">
											<label class="st-text">Price:</label> '.$row->price.'<br>';
                                                                                        if($row->date=="$now"){

                                                                                            $data['res_advertiser'] .='<label class="st-text">New Post:</label> Today at '.$row->time.'<br>';

                                                                                        }else if($row->date>"$now"){

                                                                                            $data['res_advertiser'] .='<label class="st-text">Advance Post:</label> '.$row->date.' at '.$row->time.'<br>';

                                                                                        } else {

                                                                                            $data['res_advertiser'] .='<label class="st-text">Posted on:</label> '.$row->date.' at '.$row->time.'<br>';

                                                                                        }
											$data['res_advertiser'] .='
											<label class="st-text">Description:</label> '.$row->description.'<br>
											<label class="st-text">Bedrooms:</label> '.$row->bedrooms.'<br>
											<label class="st-text">Category:</label> '.$row->real_hold.' | '.$row->real_cat.' | '.$row->propname.'<br><br>
											</div><br>
                                                                                        <div class="postAdvert">CONTACT INFORMATION:</div>
											<div class = "postAdvert">
											<label class="st-text">Address:</label> '.$row->addressname.'<br>
											<label class="st-text">Mobile No.:</label> '.$row->mobno.'<br>
											<label class="st-text">Tel. No.:</label> '.$row->telno.'<br>
											<label class="st-text">Email:</label> '.$row->email.'<br>
											</div>
										</td>
										</tr>
			 						</table>
                                                          <div>';if($this->user_session['id']=="12"){
                                                                 
                                                              $data['res_advertiser'] .='<button class="blue-button to-right sendMessageNotlogRent">Send Message</button>
                                                          </div>';
                                                          }else{
                                                                                        if($row->added_by=="$user"){
                                                              $data['res_advertiser'] .='<button class="blue-button to-right updateAllreal" id="upc_'.$row->real_id.'">Update Contents</button>
                                                          </div>';}
                                                          else if($this->user_session['id']=="107") {
                                                           $data['res_advertiser'] .='<button class="blue-button to-right updateAllreal" id="upc_'.$row->real_id.'">Update Contents</button>
                                                               <button class="blue-button to-right sendMessageInfoAllreal" id="upc_'.$row->real_id.'">Contact Advertiser</button>
                                                           </div>';
                                                          }
                                                          else{
                                                              $data['res_advertiser'] .='<button class="blue-button to-right sendMessageInfoAllreal" id="upc_'.$row->real_id.'">Contact Advertiser</button>
                                                          </div>';}
                                                          }
                                                            
			}
			$data['res_advertiser'] .= '</div>';
			}

           $data['real'] = ($real) ? $real->row(): "Not found";

	   $realURP = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname AS lname
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
											AND  rsi.real_id ='".$this->input->post('urp')."'");

           $data['realURP'] = ($realURP) ? $realURP->row(): "Not found";

          $realContents = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname AS lname, rp.name AS propname,
                                                                                         ecd.street AS addressname, ecd.telno AS telno, ecd.mobileno AS mobno, ecd.email AS email
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp,
                                                                                        emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.added_by=ecd.emp_id
                                                                                        AND rsi.property = rp.property_id
											AND rsi.real_id ='".$this->input->post('upc')."'");

           $data['realContents'] = ($realContents) ? $realContents->row(): "Not found";
           
           $sendMessage = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat, epd.emp_id AS sendid,
											 epd.firstname AS fname, epd.lastname AS lname, rp.name AS propname,
                                                                                         ecd.street AS addressname, ecd.telno AS telno, ecd.mobileno AS mobno, ecd.email AS email
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp,
                                                                                        emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.added_by=ecd.emp_id
                                                                                        AND rsi.property = rp.property_id
											AND rsi.real_id ='".$this->input->post('sm')."'");

           $recepient = $this->db->query("SELECT * FROM emp_personal_details WHERE emp_id={$user}");
           
                     $data['sm'] = "";
                                                                
                                                                
			if($sendMessage->num_rows()>0) {

					$data['sm'] .= '';

			foreach($sendMessage->result() as $row) {
                            
                                $data['sm'] .= ''.ucfirst($row->fname).' <input type="hidden" name="sendto" value="'.$row->sendid.'"/>';
                            
                        }
                        }
                        
          $recepient = $this->db->query("SELECT epd.firstname AS rfname, epd.lastname AS rlname, ecd.email AS recemail,
                                                ecd.mobileno AS rmobno, ecd.telno AS rtelno
                                         FROM emp_personal_details epd, emp_contact_details ecd 
                                         WHERE epd.emp_id={$user} AND ecd.emp_id={$user}");
           
                     $data['rc'] = ""; $data['email'] =""; $data['ctc'] =""; $data['ctc2'] ="";
                                                                
                                                                
			if($recepient->num_rows()>0) {

					$data['rc'] .= '';

			foreach($recepient->result() as $row) {
                            
                                $data['rc'] .= ''.ucfirst($row->rfname).' '.ucfirst($row->rlname).' <input type="hidden" name="sendby" value="'.$user.'"/>';
                                $data['email'] .=''.$row->recemail.'';
                                $data['ctc'] .='<input type="text" name="rmobno" value="'.$row->rmobno.'" class="u2 fontsize">';
                                $data['ctc2'] .='<input type="text" name="rtelno" value="'.$row->rtelno.'" class="u2 fontsize">';
                            
                        }
                        }   
                     
           
           $data['sendMessage'] = ($sendMessage) ? $sendMessage->row(): "Not found";
           
        echo json_encode($data);
    }


	private function _init(){
        $loc = $this->db->query("SELECT * FROM real_location");
        $data['loc'] = ($loc->num_rows()>0) ? $loc->result():'No Location';
	$classif = $this->db->query("SELECT * FROM real_property");
	$data['classif'] = ($classif->num_rows()>0) ? $classif->result():'No Classificition';
        $realhold = $this->db->query("SELECT * FROM real_hold");
        $data['realhold'] = ($realhold->num_rows()>0) ? $realhold->result(): 'No Category';
        $seltype = $this->db->query("SELECT * FROM real_category");
        $data['seltype'] = ($seltype->num_rows()>0) ? $seltype->result(): 'No type';
        echo json_encode($data);
    }

     private function _delete_advert() {
        $sql = $this->db->query("DELETE FROM real_state_info WHERE real_id = '".$this->input->post('pid')."'");
        $this->get_all_real_infoAllreal();
    }


}
/* End of all real  */