<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Location extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_item": $this->get_all_item();break;
            case "add_loc": $this->_add_loc();break;
            case "get_item_details": $this->_item_details();break;
            case "update_items": $this->_update_items();break;
            case "delete_item": $this->_delete_items();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    public function get_all_item() {
        
      if($this->input->post('like')!="") {
          $item = $this->db->query("SELECT * FROM real_location WHERE name LIKE '".$this->input->post('like')."%'");
      }else{
       /*  $item = $this->db->query("SELECT ohi.item_id, ohi.price, ohi.item_desc, hkc.cat_name AS category 
                                  FROM ops_hk_items ohi, hk_category hkc 
                                  WHERE ohi.item_cat=hkc.cat_id AND ohi.item_desc LIKE '".$this->input->post('like')."%' ORDER BY ohi.item_cat DESC  LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ohi.item_id) AS rows FROM ops_hk_items ohi, hk_category hkc 
                                    WHERE ohi.item_cat=hkc.cat_id AND ohi.item_desc LIKE '".$this->input->post('like')."%'");
       }else {*/
        $item = $this->db->query("SELECT * FROM real_location");
      }
       /* $_count = $this->db->query("SELECT COUNT(ohi.item_id) AS rows FROM ops_hk_items ohi, hk_category hkc
                                 WHERE ohi.item_cat=hkc.cat_id");
       }
        $config['base_url']= base_url().'rs/location/get_all_item/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 15;
        $this->pagination->initialize($config);
        $data['num_rows']= $item->num_rows();
        $data['pagination'] = $this->pagination->create_links(); */
        $data['res'] = "";
        if($item->num_rows()>0) {
            $data['res'] .= '
                            <div class="stocks-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Location Name</label></div>
                                
                                   
                            </div>
                            <div class="stocks_list_container1">';
            foreach($item->result() as $row) {
                $data['res'] .='<div class="stocks-contents1">
                    
                                <div>'.$row->name.'</div>
                                <div class="item">        
                                     <ul>
                                     
                                        <li class="white-litegray update-items" title="Update Item" id="st_'.$row->loc_id.'">Update</li>
                                        <li class="white-litegray delete-items" title="Delete Item" id="st_'.$row->loc_id.'">Delete</li>
                                           
                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    
     private function _add_loc() {
        $received = $this->input->post('data');
        $values = array(
            
            'name'=>$received['loc']
              
        );
        if($this->db->insert('real_location',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    } 
    
   
     private function _update_items() {
        $received = $this->input->post('data');
        $values = array(
            "loc_id"=>$received['loc_id'],
            "name"=>$received['locname']
        );
        $data['result'] = ($this->db->update('real_location',$values,"loc_id = ".$received['loc_id'])) ? true:false;
        echo json_encode($data);
    } 
   
    private function _item_details() {
           $item = $this->db->query("SELECT * FROM real_location WHERE loc_id ='".$this->input->post('bid')."'");
           $data['item'] = ($item) ? $item->row(): "Not found";
        echo json_encode($data);
    } 
    
    private function _init(){
        $category = $this->db->query("SELECT * FROM hk_category");
        $data['category'] = ($category->num_rows()>0) ? $category->result():'No category'; 
        echo json_encode($data);
    }   
   
   
     private function _delete_items() {
        $sql = $this->db->query("DELETE FROM real_location WHERE loc_id = '".$this->input->post('pid')."'");
        $this->get_all_item();
    }
   
}
/* End of Stocks  */