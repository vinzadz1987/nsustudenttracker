<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class JoinUs extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
		$this->load->library('encrypt');
    }
    function index() {
        switch ($this->input->post('dir')) {
			case "add_user": $this->_add_user();break;
            default: show_404();
        }
    }
    
	
	
	private function _add_user() {
        $received = $this->input->post('data');
		
		$string = $received['pw'];
        	$key = "c3l1c3bu";  
        	$result =  $this->encrypt->encode($string,$key);
		$date = make_timestamp(date('m/d/Y'), "mm/dd/yyyy");
			
        $values = array(
            
            'username'	=>$received['uname'],
            'password'	=>$result,
			'status'	=>"active",
			'date_added'=>$date
        );
        if($this->db->insert('emp_user',$values)) {
		
			$sql = $this->db->query("SELECT emp_id FROM emp_user WHERE username = '".$received['uname']."' AND password = '".$result."'");
           
            $epd = array(
                'emp_id'        => $sql->row()->emp_id,
                'firstname'     => $received['fname'],
                'middlename'    => $received['mname'],
                'lastname'      => $received['lname']
            );
			
           $this->db->insert('emp_personal_details',$epd);
			
			$ecd = array(
				
				'emp_id'		=> $sql->row()->emp_id,
				'street'		=> $received['address'],
				'telno'			=> $received['tel_no'],
				'mobileno'		=> $received['mob_no'],
				'email'			=> $received['email']
			
			);
				
			$this->db->insert('emp_contact_details',$ecd);
			
			$ul = array(
					
				'emp_id'		=> $sql->row()->emp_id,
				'deptd_id'		=> "2"
					
			);
			
			$this->db->insert('department', $ul);
		
			// $t = array("51", "52", "53", "54", "56", "57", "58");
			// foreach($t as $key => $value ) {
			// $task = array(
			
			// 	'emp_id'		=> $sql->row()->emp_id,
			// 	'task_id'		=>$value
			
			// );
			
			// $this->db->insert('tasks', $task);
			
			// }
			
            echo "added";
		
        } else {
            echo "failed";
        }
    } 
	
       
}
/* End of all real  */