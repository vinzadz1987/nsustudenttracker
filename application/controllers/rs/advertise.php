<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Advertise extends CI_Controller {
	var $user_session;
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
		
        if(!$this->user_session=$this->session->userdata('emp_info')) {
            show_404();
        }
		$this->load->library('encrypt');
    }
    function index() {
        switch ($this->input->post('dir')) {
		
			case "advertise":	 		$this->_advertise();break;
			case "init":		 		$this->_init();break;
			
            default: show_404();
        }
    }
    
	private function _init(){
        $cat = $this->db->query("SELECT * FROM real_property");
        $data['cat'] = ($cat->num_rows()>0) ? $cat->result():'No Property';
		$loc = $this->db->query("SELECT * FROM real_location");
		$data['loc'] = ($loc->num_rows()>0) ? $loc->result():'No location';
		$type = $this->db->query("SELECT * FROM real_hold");
		$data['type'] = ($type->num_rows()>0) ? $type->result():'No hold';
		echo json_encode($data);
    }   
	

	
	private function _advertise() {
		
		$user_id =  $this->user_session['id'];
                $received = $this->input->post('data');
		$now = date('F d, Y');
		$time_now=mktime(date('h')+5,date('i')+30);
		$time = date('h:i a', $time_now);
		
        $values = array(
            
			'cat_id'		=>$received['stype'],			
			'property'		=>$received['property'],
                        'loc_id'		=>$received['loc'],
                        'real_hold_id'          =>$received['real_hold'],
                        'title'			=>$received['title'],
			'description'           =>$received['desc'],
			'bedrooms'		=>$received['bedrooms'],
			'price'			=>$received['price'],
			'date'			=>$now,
			'time'			=>$time,
			'added_by'		=>$user_id
			
        );
        if($this->db->insert('real_state_info',$values)) {
            
		 echo "added";
		
        } else {
            echo "failed";
        }
    } 
       
}
/* End of all real  */