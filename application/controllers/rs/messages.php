<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Messages extends CI_Controller {
   var $user_session;
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->user_session=$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_messages": $this->get_all_real_info_messages();break;
            case "all_advertiser": $this->get_advertiser();break;
            case "init": $this->_init();break;
            case "get_details": $this->_real_details();break;
            case "sendmessage": $this->_sendmessage();break;
            case "update_contents": $this->_update_real_mess_contents(); break;
            case "delete_messages": $this->_delete_messages(); break;
            default: show_404();
        }
    }

    private function _sendmessage() {

                $received = $this->input->post('data');
                $now = date('F d, Y');
		$time_now=mktime(date('h')+5,date('i')+30);
		$time = date('h:i a', $time_now);

        $values = array(

			'real_id'		=>$received['real_id'],
                        'message_status'        =>"2",
                        'sendby'		=>$received['sendby'],
                        'sendto'		=>$received['sendto'],
                        'mobileno'		=>$received['mobno'],
                        'telno'                 =>$received['telno'],
                        'text'                  =>$received['text'],
                        'date'                  =>$now,
                        'time'                  =>$time

        );
        if($received['text']!="") {
        if($this->db->insert('real_messaging',$values)) {

		 echo "added";

        } else {
            echo "failed";
        }
        }else{
            echo "failed";
        }
    } 

      private function _update_real_mess_contents() {
        $received = $this->input->post('data');
        $values = array(

            "real_message_id"       =>$received['real_message_id'],
            "message_status"         =>"1",
        );
        $data['result'] = ($this->db->update('real_messaging',$values,"real_message_id = ".$received['real_message_id'])) ? true:false;
        echo json_encode($data);
    }

	public function get_all_real_info_messages() {

            if($this->user_session['id']=="107") {

                if($this->input->post('like')!="") {

                     $messages = $this->db->query("SELECT rm.*, epd.*, ms.status_css AS messcss, ms.status_name as statusname,
                                                             rsi.title AS topic
                                                      FROM real_messaging rm, emp_personal_details epd, message_status ms,
                                                           real_state_info rsi
                                                      WHERE rm.sendby=epd.emp_id
                                                            AND rm.message_status=ms.real_message_id AND rm.real_id=rsi.real_id
                                                            AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'
                                                            ORDER BY rm.date DESC, rm.time DESC");
		/* 	$real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
									  		AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'
									  		GROUP BY rsi.added_by DESC LIMIT 2 OFFSET ".$offset);


			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS adfname, epd.lastname AS adlname,
											   COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
										     AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'");

			*/}else {

                        $messages = $this->db->query("SELECT rm.*, epd.*, ms.status_css AS messcss, ms.status_name as statusname,
                                                             rsi.title AS topic
                                                      FROM real_messaging rm, emp_personal_details epd, message_status ms,
                                                           real_state_info rsi
                                                      WHERE rm.sendby=epd.emp_id
                                                            AND rm.message_status=ms.real_message_id AND rm.real_id=rsi.real_id
                                                            ORDER BY rm.date DESC, rm.time DESC");

			/*$real = $this->db->query("SELECT rsi.*,	rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
									  GROUP BY rsi.added_by ORDER BY cp DESC LIMIT 2 OFFSET ".$offset);

			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.lastname AS adlname,
											   epd.firstname AS adfname, COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id");

			*/}

			/*$config['base_url'] = base_url().'rs/messages/get_advertiser/';
			$config['total_rows'] = $_count->row()->rows;
			$config['uri_segment'] = 4;
			$config['per_page'] = 2;
			$this->pagination->initialize($config);
			$data['num_rows'] = $real->num_rows();
			$data['pagination'] = $this->pagination->create_links();*/

			$data['res'] = "";

			if($messages->num_rows()>0) {

					$data['res'] .= '
                            <div class="messages">
								';

			foreach($messages->result() as $row) {

				$data['res'] .='                <div class="sprite-close to-right messdel deletemessages" id="delmess_'.$row->real_message_id.'"></div>
								<div class="advert_count2 '.$row->messcss.' showMessage" id="mess_'.$row->real_message_id.'" title="">

                                                                <div class="to-right messdate">'.$row->date.' at '.$row->time.'</div>
                                                                <table>
                                                                    <tr>
									<td>
                                                                        <img src="./res/img/photos/'.$row->sendby.'.jpg" width="50" height="50" class="advert_photos"/>
                                                                        </td>
                                                                        <td>
									 <label class="titleReal">'.$row->firstname.' '.$row->lastname.'</label><br>
                                                                         <label class="sw-text22">'.substr(ucfirst($row->text),0,40).'....</label><br>
                                                                         <label class="messdate"><label class="sw-text"> Topic: '.$row->topic.'</label><br> Mobile no: '.$row->mobileno.' Tel. no: '.$row->telno.' '.$row->statusname.' </label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
								</div>
							  ';

			}
			$data['res'] .= '</div>';
			}
			echo json_encode($data);
                
            }else {

            if($this->input->post('like')!="") {

                     $messages = $this->db->query("SELECT rm.*, epd.*, ms.status_css AS messcss, ms.status_name as statusname,
                                                             rsi.title AS topic
                                                      FROM real_messaging rm, emp_personal_details epd, message_status ms,
                                                           real_state_info rsi
                                                      WHERE rm.sendby=epd.emp_id AND rm.sendto=".$this->user_session['id']."
                                                            AND rm.message_status=ms.real_message_id AND rm.real_id=rsi.real_id
                                                            AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'
                                                            ORDER BY rm.date DESC, rm.time DESC");
		/* 	$real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
									  		AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'
									  		GROUP BY rsi.added_by DESC LIMIT 2 OFFSET ".$offset);


			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS adfname, epd.lastname AS adlname,
											   COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
										     AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'");

			*/}else {

                        $messages = $this->db->query("SELECT rm.*, epd.*, ms.status_css AS messcss, ms.status_name as statusname,
                                                             rsi.title AS topic
                                                      FROM real_messaging rm, emp_personal_details epd, message_status ms,
                                                           real_state_info rsi
                                                      WHERE rm.sendby=epd.emp_id AND rm.sendto=".$this->user_session['id']."
                                                            AND rm.message_status=ms.real_message_id AND rm.real_id=rsi.real_id
                                                            ORDER BY rm.date DESC, rm.time DESC");

			/*$real = $this->db->query("SELECT rsi.*,	rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
									  GROUP BY rsi.added_by ORDER BY cp DESC LIMIT 2 OFFSET ".$offset);

			$_count = $this->db->query("SELECT rsi.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.lastname AS adlname,
											   epd.firstname AS adfname, COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id");

			*/}

			/*$config['base_url'] = base_url().'rs/messages/get_advertiser/';
			$config['total_rows'] = $_count->row()->rows;
			$config['uri_segment'] = 4;
			$config['per_page'] = 2;
			$this->pagination->initialize($config);
			$data['num_rows'] = $real->num_rows();
			$data['pagination'] = $this->pagination->create_links();*/
		
			$data['res'] = "";

			if($messages->num_rows()>0) {

					$data['res'] .= '
                            <div class="messages">
								';

			foreach($messages->result() as $row) {

				$data['res'] .='                <div class="sprite-close to-right messdel deletemessages" id="delmess_'.$row->real_message_id.'"></div>
								<div class="advert_count2 '.$row->messcss.' showMessage" id="mess_'.$row->real_message_id.'" title="">
                                                                
                                                                <div class="to-right messdate">'.$row->date.' at '.$row->time.'</div>
                                                                <table>
                                                                    <tr>
									<td>
                                                                        <img src="./res/img/photos/'.$row->sendby.'.jpg" width="50" height="50" class="advert_photos"/>
                                                                        </td>
                                                                        <td>
									 <label class="titleReal">'.$row->firstname.' '.$row->lastname.'</label><br>
                                                                         <label class="sw-text22">'.substr(ucfirst($row->text),0,40).'....</label><br>
                                                                             <label class="messdate"><label class="sw-text"> Topic: '.$row->topic.'</label><br> Mobile no: '.$row->mobileno.' Tel. no: '.$row->telno.' '.$row->statusname.' </label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
								</div>
							  ';

			}
			$data['res'] .= '</div>';
			}
			echo json_encode($data);
                    }
			
		}


        public function get_advertiser($offset=0) {

			if($this->input->post('like')!="") {


			$real = $this->db->query("SELECT rsi.*, ecd.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                        AND rsi.added_by=ecd.emp_id
									  		AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'
									  		GROUP BY rsi.added_by DESC LIMIT 15 OFFSET ".$offset);


			$_count = $this->db->query("SELECT rsi.*, ecd.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.firstname AS adfname, epd.lastname AS adlname,
											   COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, emp_contact_details ecd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                         AND rsi.added_by=ecd.emp_id
										     AND CONCAT(epd.firstname,' ',epd.lastname) LIKE '".$this->input->post('like')."%'");

			}else {

			$real = $this->db->query("SELECT rsi.*, ecd.*,	rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS adfname, epd.lastname AS adlname,
											 COUNT(rsi.real_id) AS cp
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                        AND rsi.added_by=ecd.emp_id
									  GROUP BY rsi.added_by ORDER BY cp DESC LIMIT 15 OFFSET ".$offset);

			$_count = $this->db->query("SELECT rsi.*, ecd.*, COUNT(rsi.real_id) AS rows, rh.name AS real_hold,
											   rc.name AS real_cat, epd.lastname AS adlname,
											   epd.firstname AS adfname, COUNT(rsi.real_id) AS cp
										FROM real_state_info rsi, real_location rl, real_hold rh,
											 real_category rc, emp_personal_details epd, emp_contact_details ecd
									    WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
											 AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
                                                                                         AND rsi.added_by=ecd.emp_id");

			}

		/*	$config['base_url'] = base_url().'rs/allreal/get_advertiser/';
			$config['total_rows'] = $_count->row()->rows;
			$config['uri_segment'] = 4;
			$config['per_page'] = 15;
			$this->pagination->initialize($config);
			$data['num_rows'] = $real->num_rows();
			$data['pagination'] = $this->pagination->create_links();
		*/
			$data['res_advertiser'] = "";

			if($real->num_rows()>0) {

					$data['res_advertiser'] .= '
                            <div class="advertiser">
								';

			foreach($real->result() as $row) {

				$data['res_advertiser'] .='
								<div class="advert_count">
									<img src="./res/img/photos/'.$row->added_by.'.jpg" width="50" height="50" class="advert_photos"/>
									'.ucfirst($row->adfname).' '.ucfirst($row->adlname).' <label class="locatReal"></label>
									<label class="locatReal"><div class="to-right cp">'.$row->cp.' Total Advertisement</div></label>
                                                                        <div class="prof class-form"><table><tr>
                                                                            <td><label><img src="./res/img/photos/'.$row->added_by.'.jpg" width="100" height="100" class="advert_photos"/><label></td>
                                                                            <td><label class="st-text"> Name:</label> <label>'.ucfirst($row->adfname).' '.ucfirst($row->adlname).'</label><br>
                                                                                <label class="st-text">Mobile No:</label><label> '.$row->mobileno.'</label><br>
                                                                                <label class="st-text">Tel. No:</label><label> '.$row->telno.'</label><br>
                                                                                <label class="st-text">Email:</label><label> '.$row->email.'</label>
                                                                            </td></tr></table>
                                                                        </div>
								</div>
							  ';

			}
			$data['res_advertiser'] .= '</div>';
			}
			echo json_encode($data);
    }



    private function _real_details() {
           $user_id = $this->user_session['id'];
           $now = date('F d, Y');
           $real = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname AS lname, rp.name AS propname,
                                                                                         ecd.street AS addressname, ecd.telno AS telno, ecd.mobileno AS mobno, ecd.email AS email
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp,
                                                                                        emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.added_by=ecd.emp_id
                                                                                        AND rsi.property = rp.property_id
											AND rsi.real_id ='".$this->input->post('bid')."'");


			$data['res_advertiser'] = "";

			if($real->num_rows()>0) {

					$data['res_advertiser'] .= '
								';

			foreach($real->result() as $row) {

				$data['res_advertiser'] .='
								<div class="advert_info">
									<img src="./res/img/photos/'.$row->added_by.'.jpg" class="realPhotos3"/>
									<label class="advert_info2">Advertiser:</label> ';
                                                                        if($row->added_by = $user_id) {

                                                                            $user_id = 'Me';

                                                                            $data['res_advertiser'] .='<label class="titleReal">'.$user_id.'</label>';

                                                                        } else {

                                                                         $data['res_advertiser'] .='<label class="titleReal">'.ucfirst($row->fname).' '.ucfirst($row->lname).'</label>';

                                                                        }
									$data['res_advertiser'] .='| <label class="advert_info2">Title:</label>
									<label class="titleReal">'.ucfirst($row->title).' at '.$row->real_loc.'</label>

								   <div class="sprite-close to-right close closeAdvertContent"></div>
								</div>
									<table>
										<tr>';
                                                                                if(($row->pic1) =="" && ($row->pic2)==""&& ($row->pic3)=="" && ($row->pic4)<1 && ($row->pic5)=="") {

                                                                                    $data['res_advertiser'] .='<td>
										<div class="shadowbox3">
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-1 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-2 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-3 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-4 hide"/>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos2 0-5 hide"/>
										</div>
										</td>
										<td>
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 1" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 2" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 3" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 4" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/12.jpg" class="realPhotos 5" title="Click to view larger."/><br />
										</td>';

                                                                                }else {

										$data['res_advertiser'] .='<td>
										<div class="shadowbox3">
											<img src="./res/img/statephotos/'.$row->pic1.'" class="realPhotos2 0"/>
											<img src="./res/img/statephotos/'.$row->pic1.'" class="realPhotos2 0-1 hide"/>
											<img src="./res/img/statephotos/'.$row->pic2.'" class="realPhotos2 0-2 hide"/>
											<img src="./res/img/statephotos/'.$row->pic3.'" class="realPhotos2 0-3 hide"/>
											<img src="./res/img/statephotos/'.$row->pic4.'" class="realPhotos2 0-4 hide"/>
											<img src="./res/img/statephotos/'.$row->pic5.'" class="realPhotos2 0-5 hide"/>
										</div>
										</td>
										<td>
											<img src="./res/img/statephotos/'.$row->pic1.'" class="realPhotos 1" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic2.'" class="realPhotos 2" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic3.'" class="realPhotos 3" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic4.'" class="realPhotos 4" title="Click to view larger."/><br />
											<img src="./res/img/statephotos/'.$row->pic5.'" class="realPhotos 5" title="Click to view larger."/><br />
										</td>';
                                                                                }
										$data['res_advertiser'] .='<td><br>
											<div class="postAdvert">CONTENT INFORMATION:</div>
                                                                                        <div class="postAdvert">
											<label class="st-text">Price:</label> '.$row->price.'<br>';
                                                                                        if($row->date=="$now"){

                                                                                            $data['res_advertiser'] .='<label class="st-text">New Post:</label> Today at '.$row->time.'<br>';

                                                                                        }else if($row->date>"$now"){

                                                                                            $data['res_advertiser'] .='<label class="st-text">Advance Post:</label> '.$row->date.' at '.$row->time.'<br>';

                                                                                        } else {

                                                                                            $data['res_advertiser'] .='<label class="st-text">Posted on:</label> '.$row->date.' at '.$row->time.'<br>';

                                                                                        }
											$data['res_advertiser'] .='
											<label class="st-text">Description:</label> '.$row->description.'<br>
											<label class="st-text">Bedrooms:</label> '.$row->bedrooms.'<br>
											<label class="st-text">Category:</label> '.$row->real_hold.' | '.$row->real_cat.' | '.$row->propname.'<br><br>
											</div><br>
                                                                                        <div class="postAdvert">CONTACT INFORMATION:</div>
											<div class = "postAdvert">
											<label class="st-text">Address:</label> '.$row->addressname.'<br>
											<label class="st-text">Mobile No.:</label> '.$row->mobno.'<br>
											<label class="st-text">Tel. No.:</label> '.$row->telno.'<br>
											<label class="st-text">Email:</label> '.$row->email.'<br>
											</div>
										</td>
										</tr>
			 						</table>
                                                          <div>
                                                            <button class="blue-button to-right updateContentsInfo" id="upc_'.$row->real_id.'">Update Contents</div>
                                                          </div>';

			}
			$data['res_advertiser'] .= '</div>';
			}

           $data['real'] = ($real) ? $real->row(): "Not found";

	   $realURP = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname AS lname
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id
											AND  rsi.real_id ='".$this->input->post('urp')."'");

           $data['realURP'] = ($realURP) ? $realURP->row(): "Not found";

          $realContents = $this->db->query("SELECT rsi.*, rl.name AS real_loc, rh.name AS real_hold, rc.name AS real_cat,
											 epd.firstname AS fname, epd.lastname AS lname, rp.name AS propname,
                                                                                         ecd.street AS addressname, ecd.telno AS telno, ecd.mobileno AS mobno, ecd.email AS email
									  FROM real_state_info rsi, real_location rl, real_hold rh,
									  		real_category rc, emp_personal_details epd, real_property rp,
                                                                                        emp_contact_details ecd
									  WHERE rsi.loc_id=rl.loc_id AND rsi.real_hold_id=rh.real_hold_id
									  		AND rsi.cat_id=rc.cat_id AND rsi.added_by=epd.emp_id AND rsi.added_by=ecd.emp_id
                                                                                        AND rsi.property = rp.property_id
											AND rsi.real_id ='".$this->input->post('upc')."'");

           $data['realContents'] = ($realContents) ? $realContents->row(): "Not found";
           

           $mess = $this->db->query("SELECT rm.*, epd.lastname
                                     FROM real_messaging rm, emp_personal_details epd
                                     WHERE rm.sendby=epd.emp_id AND real_message_id ='".$this->input->post('mess')."'");

           $data['mess'] = ($mess) ? $mess->row(): "Not Found";

           $user = $this->user_session['id'];

           $recepient = $this->db->query("SELECT epd.firstname AS rfname, epd.lastname AS rlname, ecd.email AS recemail,
                                                ecd.mobileno AS rmobno, ecd.telno AS rtelno
                                         FROM emp_personal_details epd, emp_contact_details ecd
                                         WHERE epd.emp_id={$user} AND ecd.emp_id={$user}");

                     $data['ctc'] =""; $data['ctc2'] ="";


			if($recepient->num_rows()>0) {


			foreach($recepient->result() as $row) {

                                $data['ctc'] .='<input type="text" name="rmobno" value="'.$row->rmobno.'" class="u2 fontsize"/>';
                                $data['ctc2'] .='<input type="text" name="rtelno" value="'.$row->rtelno.'" class="u2 fontsize"/>';

                        }
                        }
           

        echo json_encode($data);
    }


	private function _init(){
        $loc = $this->db->query("SELECT * FROM real_location");
        $data['loc'] = ($loc->num_rows()>0) ? $loc->result():'No Location';
	$classif = $this->db->query("SELECT * FROM real_property");
	$data['classif'] = ($classif->num_rows()>0) ? $classif->result():'No Classificition';
        $realhold = $this->db->query("SELECT * FROM real_hold");
        $data['realhold'] = ($realhold->num_rows()>0) ? $realhold->result(): 'No Category';
        $seltype = $this->db->query("SELECT * FROM real_category");
        $data['seltype'] = ($seltype->num_rows()>0) ? $seltype->result(): 'No type';
        echo json_encode($data);
    }

     private function _delete_messages() {
        $sql = $this->db->query("DELETE FROM real_messaging WHERE real_message_id = '".$this->input->post('pid')."'");
        $this->get_all_real_info_messages();
    }


}
/* End of all real  */