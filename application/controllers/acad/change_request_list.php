<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Change_request_list extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('date');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_change_request_list": $this->change_requests(); break;
            case "retrieve_details": $this->_request_details(); break;
            default: show_404();
        }
    }
    function change_requests($offset=0) {
        if($this->input->post('data')) {
            $rcv = $this->input->post('data');
            $start = gmt_to_local(human_to_unix($rcv['date_from']." 00:00:00 AM"),'UP7');
            $end = gmt_to_local(human_to_unix($rcv['date_to']." 11:59:59 PM"),'UP7');
            $sql = $this->db->query("SELECT request_id, student_name, date_added, acad_approval, sales_approval FROM acad_change_requests WHERE ".$rcv['category']." > ".$start." AND ".$rcv['category']." < ".$end);
            $_count = $this->db->query("SELECT COUNT(request_id) as rows FROM acad_change_requests WHERE ".$rcv['category']." > ".$start." AND ".$rcv['category']." < ".$end);
        } else {
            $sql = $this->db->query("SELECT request_id, student_name, date_added, acad_approval, sales_approval FROM acad_change_requests");
            $_count = $this->db->query("SELECT COUNT(request_id) as rows FROM acad_change_requests");
        }
        $config['base_url'] = base_url().'acad/lesson_plan_list/lesson_plans/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = '';
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="change_request_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $acad_apprv = $row->acad_approval != 0 ? "Approved":"Pending";
                $sales_apprv = $row->sales_approval != 0 ? "Approved":"Pending";
                $data['res'] .= '<div class="data-list-type-2 row'.$i.' lp_row" id="changerequest_'.$row->request_id.'">
                            <label class="field-cont-2">Student:</label><label class="value-cont-2 student-name">'.$row->student_name.'</label>
                            <label class="field-cont-2">Submitted:</label><label class="value-cont-2 date-added">'.date('M d, Y',$row->date_added).'</label>
                            <label class="field-cont-2">Academics:</label><label class="value-cont-2 acad_approval">'.$acad_apprv.'</label>
                            <label class="field-cont-2">Sales:</label><label class="value-cont-2 acad_approval">'.$sales_apprv.'</label>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _request_details() {
        $sql = $this->db->query("SELECT * FROM acad_change_requests WHERE request_id = '".$this->input->post('rid')."'");
        $data = array(
            "student_name" => $sql->row()->student_name,
            "student_id" => $sql->row()->student_id,
            "request_detail" => $sql->row()->request_detail,
            "instructional_hours" => $sql->row()->instructional_hours,
            "classroom" => $sql->row()->classroom,
            "additional_class" => $sql->row()->additional_class,
            "class_swapping" => $sql->row()->class_swapping,
            "course_extension" => $sql->row()->course_extension,
            "others" => $sql->row()->others,
            "others_specify" => $sql->row()->others_specify,
            "acad_approval" => $sql->row()->acad_approval,
            "acad_date_rcv" => date('Y-m-d',$sql->row()->acad_date_rcv),
            "sales_approval" => $sql->row()->sales_approval,
            "sales_date_rcv" => date('Y-m-d',$sql->row()->sales_date_rcv)
        );
        echo json_encode($data);
    }
}
/* End of change_request_list.php */