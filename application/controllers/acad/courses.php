<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Courses extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_courses": $this->get_courses(); break;
            case "course_add": $this->_add_course(); break;
            case "edit_course": $this->_course_update(); break;
            case "remove_course": $this->_course_delete(); break;
            default: show_404();
        }
    }
    function get_courses($offset=0) {
        if($this->input->post('like')!="") {
            $sql = $this->db->query("SELECT * FROM courses WHERE name LIKE '".$this->input->post('like')."%' LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(course_id) as rows FROM courses WHERE name LIKE '".$this->input->post('like')."%'");
        } else {
            $sql = $this->db->query("SELECT * FROM courses LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(course_id) as rows FROM courses");
        }
        $config['base_url'] = base_url().'acad/courses/get_courses/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="courses_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['res'] .= '<div class="data-list-type-1 row'.$i.'" id="course_'.$row->course_id.'">
                            <label class="field-cont">Course:</label><label class="value-cont course-name">'.$row->name.'</label>
                            <label class="field-cont">Type:</label><label class="value-cont course-type">'.$row->type.'</label>
                            <div class="sprite-delete delete-course-btn to-right"></div>
                            <div class="sprite-square-empty edit-course-btn to-right"></div></div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _add_course() {
        $received = $this->input->post('data');
        $values = array(
            'name' => $received['name'],
            'type' => $received['type']
        );
        $data['result'] = ($this->db->insert('courses',$values)) ? true:false;
        echo json_encode($data);
    }
    private function _course_update() {
        $received = $this->input->post('data');
        $values = array(
            "name" => $received['name'],
            "type" => $received['type']
        );
        $data['result'] = ($this->db->update('courses',$values,"course_id = ".$received['cid'])) ? true:false;
        echo json_encode($data);
    }
    private function _course_delete() {
        if($this->db->delete('courses',"course_id = ".$this->input->post('cid'))) {
            $data['result'] = ($this->db->update('acad_classes',array("course_id"=>""),"course_id = ".$this->input->post('cid'))) ? true: false;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
    }
}
/* End of courses.php */