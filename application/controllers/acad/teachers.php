<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Teachers extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_teachers": $this->get_teachers(); break;
            case "retrieve_teacher_info": $this->_get_teacher_info(); break;
            default: show_404();
        }
    }
    function get_teachers($offset=0) {
        $type = $this->input->post('type');
        if($this->input->post('like')!="") {
            if($type == 4) {
                $sql = $this->db->query("SELECT epd.emp_id, CONCAT(epd.firstname,' ',epd.lastname) AS name, et.name AS status 
                    FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id AND ejd.curr_empstatus=et.id AND jt.name='teacher' AND 
                    (epd.firstname LIKE '".$this->input->post('like')."%' OR epd.lastname LIKE '".$this->input->post('like')."%') 
                    LIMIT 15 OFFSET ".$offset);
                $_count = $this->db->query("SELECT COUNT(epd.emp_id) AS rows FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id AND ejd.curr_empstatus=et.id AND jt.name='teacher' AND 
                    (epd.firstname LIKE '".$this->input->post('like')."%' OR epd.lastname LIKE '".$this->input->post('like')."%') ");
            } else {
                $sql = $this->db->query("SELECT epd.emp_id, CONCAT(epd.firstname,' ',epd.lastname) AS name, et.name AS status 
                    FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id AND ejd.curr_empstatus=et.id AND jt.name='teacher' AND et.id='".$type."' 
                    AND (epd.firstname LIKE '".$this->input->post('like')."%' OR epd.lastname LIKE '".$this->input->post('like')."%') 
                    LIMIT 15 OFFSET ".$offset);
                $_count = $this->db->query("SELECT COUNT(epd.emp_id) AS rows FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE (epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id) AND jt.name='teacher' AND et.id='".$type."' 
                    AND (epd.firstname LIKE '".$this->input->post('like')."%' OR epd.lastname LIKE '".$this->input->post('like')."%') ");
            }
        }
        else {
            if($type == 4) {
                $sql = $this->db->query("SELECT epd.emp_id, CONCAT(epd.firstname,' ',epd.lastname) AS name, et.name AS status 
                    FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id AND ejd.curr_empstatus=et.id AND jt.name='teacher' 
                    LIMIT 15 OFFSET ".$offset);
                $_count = $this->db->query("SELECT COUNT(epd.emp_id) AS rows FROM emp_personal_details epd, emp_job_details ejd, job_title jt 
                    WHERE (epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id) AND jt.name='teacher'");
            } else {
                $sql = $this->db->query("SELECT epd.emp_id, CONCAT(epd.firstname,' ',epd.lastname) AS name, et.name AS status 
                    FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id AND ejd.curr_empstatus=et.id AND jt.name='teacher' AND et.id='".$type."' 
                        LIMIT 15 OFFSET ".$offset);
                $_count = $this->db->query("SELECT COUNT(epd.emp_id) AS rows FROM emp_personal_details epd, emp_job_details ejd, job_title jt, employment_type et 
                    WHERE (epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id) AND jt.name='teacher' AND et.id='".$type."'");
            }
        }
        $config['base_url'] = base_url().'acad/teachers/get_teachers/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="teachers_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $sql_class_count = $this->db->query("SELECT COUNT(teacher_id) as rows FROM acad_classes WHERE teacher_id = '".$row->emp_id."'");
                $classes = ($sql_class_count->num_rows()>0) ? $sql_class_count->row()->rows:0;
                $data['res'] .= '<div class="data-list-type-1 row'.$i.'" id="teacher_'.$row->emp_id.'">
                            <label class="field-cont">Name:</label><label class="value-cont teacher-name">'.$row->name.'</label>
                            <label class="field-cont">Status:</label><label class="value-cont teacher-status">'.$row->status.'</label>
                            <label class="field-cont">Classes:</label><label class="value-cont classes-scheduled">'.$classes.'</label>
                            <div class="sprite-square-empty class-teacher-btn to-right"></div>
                            <div class="sprite-inspect info-teacher-btn to-right"></div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _get_teacher_info() {
        $teacher = $this->input->post('tid');
        $sql = $this->db->query("SELECT emp_personal_details.firstname, emp_personal_details.middlename, emp_personal_details.lastname, 
            emp_personal_details.gender, emp_personal_details.nationality, emp_personal_details.photo, emp_contact_details.street, emp_contact_details.city, 
            emp_contact_details.province, emp_contact_details.country, emp_contact_details.zipcode, emp_contact_details.telno, emp_contact_details.mobileno, emp_contact_details.email 
            FROM emp_personal_details LEFT JOIN emp_contact_details ON emp_personal_details.emp_id = emp_contact_details.emp_id 
            WHERE emp_personal_details.emp_id = '".$teacher."'");
        if($sql->num_rows()>0) {
            $data = array(
                "firstname" => ($sql->row()->firstname=="" || $sql->row()->firstname==null) ? "": $sql->row()->firstname,
                "middlename" => ($sql->row()->middlename=="" || $sql->row()->middlename==null) ? "": $sql->row()->middlename,
                "lastname" => ($sql->row()->lastname=="" || $sql->row()->lastname==null) ? "": $sql->row()->lastname,
                "gender" => ($sql->row()->gender=="" || $sql->row()->gender==null) ? "": $sql->row()->gender,
                "nationality" => ($sql->row()->nationality=="" || $sql->row()->nationality==null) ? "": $sql->row()->nationality,
                "street" => ($sql->row()->street=="" || $sql->row()->street==null) ? "": $sql->row()->street,
                "city" => ($sql->row()->city=="" || $sql->row()->city==null) ? "": $sql->row()->city,
                "province" => ($sql->row()->province=="" || $sql->row()->province==null) ? "": $sql->row()->province,
                "zipcode" => ($sql->row()->zipcode=="" || $sql->row()->zipcode==null) ? "": $sql->row()->zipcode,
                "country" => ($sql->row()->country=="" || $sql->row()->country==null) ? "": $sql->row()->country,
                "email" => ($sql->row()->email=="" || $sql->row()->email==null) ? "": $sql->row()->email,
                "telephone" => ($sql->row()->telno=="" || $sql->row()->telno==null) ? "": $sql->row()->telno,
                "mobile" => ($sql->row()->mobileno=="" || $sql->row()->mobileno==null) ? "": $sql->row()->mobileno,
                "photo" => ($sql->row()->photo=="" || $sql->row()->photo==null) ? "": $sql->row()->photo
            );
        }
        echo json_encode($data);
    }
}