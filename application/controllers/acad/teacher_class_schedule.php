<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Teacher_class_schedule extends CI_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_my_schedule": $this->_get_my_schedule(); break;
            case "fetch_my_students": $this->_get_my_students(); break;
            default: show_404();
        }
    }
    private function _get_my_schedule() {
        $t_id = $this->session->userdata['emp_info']['id'];
        $sql = $this->db->query("SELECT ac.class_id, ac.class_type, ac.time, 
            b.title AS book, c.name AS course, cl.name AS level, r.room_name AS room, CONCAT(ep.firstname,' ',ep.lastname) AS teacher
            FROM acad_classes ac, books b, courses c, course_level cl, classrooms r , emp_personal_details ep 
            WHERE ac.teacher_id='".$t_id."' AND ac.book_id=b.book_id AND ac.course_id=c.course_id 
                AND ac.level_id=cl.id AND ac.classroom_id=r.classroom_id AND ac.teacher_id=ep.emp_id ORDER BY ac.time");
        $res = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $res .= '<div class="sched_container"><table class="data-table-1" id="my_class_schedule">
                <caption>'.$sql->row()->teacher.' Class(es)</caption>
                <tr class="sub-tasks">
                    <th>Time</th>
                    <th>Room</th>
                    <th>Course</th>
                    <th>Course Level</th>
                    <th>Type</th>
                    <th>Book</th>
                    <th></th>
                </tr>';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $res .= '<tr class="row'.$i.'" id="'.$row->class_id.'">
                        <td class="sched_time">'.$row->time.'</td>
                        <td class="sched_room">'.$row->room.'</td>
                        <td class="sched_course">'.$row->course.'</td>
                        <td class="sched_level">'.$row->level.'</td>
                        <td class="sched_type">'.$row->class_type.'</td>
                        <td class="sched_book"><p>'.$row->book.'</p></td>
                        <td><div class="sprite-inspect view_students_btn"></div></td>';
                $res .='</tr>';
            }
            $res .='</table></div>';
            $res .= '<div class="sched_container"><table class="data-table-1" id="teacher_class">
                <caption> Wednesday Schedule</caption>
                <tr class="sub-tasks">
                    <th>Time</th>
                    <th>Room</th>
                    <th>Course</th>
                    <th>Course Level</th>
                    <th>Type</th>
                    <th>Book</th>
                    <th></th>
                </tr>';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                switch ($row->time) {
                    case "08:00-09:50": $wed_time = "08:00-09:10"; break;
                    case "10:00-11:50": $wed_time = "09:20-10:30"; break;
                    case "12:40-02:30": $wed_time = "10:40-11:50"; break;
                    case "02:40-04:30": $wed_time = "12:40-01:50"; break;
                    case "04:40-06:30": $wed_time = "02:00-03:10"; break;
                    case "06:40-08:30": $wed_time = "03:20-04:30"; break;
                }
                $res .= '<tr class="row'.$i.'" id="'.$row->class_id.'">
                        <td class="sched_time">'.$wed_time.'</td>
                        <td class="sched_room">'.$row->room.'</td>
                        <td class="sched_course">'.$row->course.'</td>
                        <td class="sched_level">'.$row->level.'</td>
                        <td class="sched_type">'.$row->class_type.'</td>
                        <td class="sched_book"><p>'.$row->book.'</p></td>
                        <td></td>
                    </tr>';
            }
            $res .='</table></div>';
        }
        echo $res;
    }
    private function _get_my_students() {
        $sql = $this->db->query("SELECT st.id, CONCAT(st.firstname,' ',st.lastname,' (',st.english_name,')') AS student_name 
            FROM students st, class_students cs WHERE cs.class_id='".$this->input->post('cid')."' AND cs.student_id=st.id");
        $res = "";
        if($sql->num_rows()>0) {
            $res .= "<ul>";
            foreach($sql->result() as $row) {
                $res .='<li>'.$row->student_name.'</li>';
            }
            $res .= "</ul>";
        }
        echo $res;
    }
}
/* End of teacher_class_schedule.php */
