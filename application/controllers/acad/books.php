<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Books extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_books": $this->get_books(); break;
            case "book_add": $this->_add_book(); break;
            case "retrieve_details": $this->_book_details(); break;
            case "edit_book": $this->_book_update(); break;
            case "remove_book": $this->_book_delete(); break;
            default: show_404();
        }
    }
    public function get_books($offset=0) {
        if($this->input->post('like')!="") {
            $sql = $this->db->query("SELECT b.book_id, b.title, b.class_type, c.name AS course 
                FROM books b, courses c WHERE b.course_id=c.course_id AND b.title LIKE '".$this->input->post('like')."%' LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(b.book_id) as rows FROM books b, courses c WHERE b.course_id=c.course_id AND b.title LIKE '".$this->input->post('like')."%'");
        } else {
            $sql = $this->db->query("SELECT b.book_id, b.title, b.class_type, c.name AS course 
                FROM books b, courses c WHERE b.course_id=c.course_id LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(b.book_id) as rows FROM books b, courses c WHERE b.course_id=c.course_id");
        }
        $config['base_url'] = base_url().'acad/books/get_books/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="books_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['res'] .= '<div class="data-list-type-1 row'.$i.'" id="book_'.$row->book_id.'">
                            <label class="field-cont">Title:</label><label class="value-cont book-title">'.$row->title.'</label>
                            <label class="field-cont">Type:</label><label class="value-cont-mini book-class-type">'.$row->class_type.'</label>
                            <label class="field-cont">Course:</label><label class="value-cont book-course">'.$row->course.'</label>
                            <div class="sprite-delete delete-book-btn to-right"></div>
                            <div class="sprite-square-empty edit-book-btn to-right"></div></div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _add_book() {
        $received = $this->input->post('data');
        $values = array(
            'title'=>$received['title'],
            'class_type'=>$received['class_type'],
            'course_id'=>$received['course']
        );
        if($this->db->insert('books',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    }
    private function _book_details() {
        $sql = $this->db->query("SELECT * FROM books WHERE book_id='".$this->input->post('bid')."'");
        $data['book'] = ($sql) ? $sql->row(): "Not found";
        echo json_encode($data);
    }
    private function _book_update() {
        $received = $this->input->post('data');
        $values = array(
            "title" => $received['title'],
            "class_type" => $received['class_type'],
            "course_id" => $received['course']
        );
        $data['result'] = ($this->db->update('books',$values,"book_id = ".$received['book_id'])) ? true:false;
        echo json_encode($data);
    }
    private function _book_delete() {
//        if($this->db->update('acad_classes',array("book_id"=>""),"book_id = ".$this->input->post('bid'))) {
//            $data['result'] = ($this->db->delete('books',"book_id = ".$this->input->post('bid'))) ? true: false;
//        } else {
//            $data['result'] = false;
//        }
        if($this->db->delete('books',"book_id = ".$this->input->post('bid'))) {
            $data['result'] = ($this->db->update('acad_classes',array("book_id"=>""),"book_id = ".$this->input->post('bid'))) ? true: false;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
    }
}
/* End of books.php */