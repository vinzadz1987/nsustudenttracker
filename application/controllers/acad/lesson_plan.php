<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Lesson_plan extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('date');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_class_list": $this->_get_class_list(); break;
            case "retrieve_class_students_and_lp": $this->_get_class_students_and_lp(); break;
            case "add_lesson_plan": $this->_insert_lesson_plan(); break;
            case "retrieve_lp_day_details": $this->_get_lesson_plan_details(); break;
            case "add_lesson_plan_detail": $this->_insert_lesson_plan_detail(); break;
            case "edit_lesson_plan_detail": $this->_update_lesson_plan_detail(); break;
            default: show_404();
        }
    }
    private function _get_class_list() {
        $sql = $this->db->query("SELECT ac.class_id, ac.class_type, ac.time, 
            b.title AS book, c.name AS course, cl.name AS level, r.room_name AS room, CONCAT(ep.firstname,' ',ep.lastname) AS teacher
            FROM acad_classes ac, books b, courses c, course_level cl, classrooms r , emp_personal_details ep 
            WHERE ac.teacher_id='".$this->session->userdata['emp_info']['id']."' AND ac.book_id=b.book_id AND ac.course_id=c.course_id 
                AND ac.level_id=cl.id AND ac.classroom_id=r.classroom_id AND ac.teacher_id=ep.emp_id ORDER BY ac.time");
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['teacher'] = $sql->row()->teacher;
            $data['res'] .= '<div class="sched_container"><table class="data-table-1" id="teacher_class">
                <caption> Class Schedule </caption>
                <tr class="sub-tasks">
                    <th>Time</th>
                    <th>Room</th>
                    <th>Course</th>
                    <th>Course Level</th>
                    <th>Type</th>
                    <th>Book</th>
                    <th></th>
                </tr>';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['res'] .= '<tr class="row'.$i.'" id="'.$row->class_id.'">
                        <td class="sched_time">'.$row->time.'</td>
                        <td class="sched_room">'.$row->room.'</td>
                        <td class="sched_course">'.$row->course.'</td>
                        <td class="sched_level">'.$row->level.'</td>
                        <td class="sched_type">'.$row->class_type.'</td>
                        <td class="sched_book">'.$row->book.'</td>';
                $data['res'] .= '<td><div class="sprite-blue-square-right select_btn"></div></td>';
                $data['res'] .='</tr>';
            }
            $data['res'] .='</table></div>';
        } else {
            $data['res'] .= 'No class scheduled';
        }
        echo json_encode($data);
    }
    private function _get_class_students_and_lp() {
        $sql_stud = $this->db->query("SELECT st.id, CONCAT(st.firstname,' ',st.lastname) AS student_name 
            FROM students st, class_students cs WHERE cs.class_id='".$this->input->post('cid')."' AND cs.student_id=st.id");
        $data['res'] = ""; $data['lp'] = null;
        if($sql_stud->num_rows()>0) {
            $data['res'] .= '<ul>';
            foreach($sql_stud->result() as $row) {
                $data['res'] .= '<li id="classstudent_'.$row->id.'">'.$row->student_name.'</li>';
            }
            $data['res'] .= '</ul>';
        }
        $sql_lp = $this->db->query("SELECT lesson_plan_id, inclusive_date_from, inclusive_date_to, date_approved FROM acad_lesson_plans WHERE class_id = '".$this->input->post('cid')."'");
        if($sql_lp->num_rows()>0) {
            $data['lp'] = $sql_lp->row()->lesson_plan_id;
            $data['idf'] = date('Y-m-d',$sql_lp->row()->inclusive_date_from);
            $data['idt'] = date('Y-m-d',$sql_lp->row()->inclusive_date_to);
            $data['approved'] = ($sql_lp->row()->date_approved != 0) ? date('M d, Y',$sql_lp->row()->date_approved):0;
        }
        echo json_encode($data);
    }
    private function _insert_lesson_plan() {
        $rcv = $this->input->post('data');
        $time = date('h:i:s A',now()); $data['lp'] = null;
        $values = array (
            'class_id' => $rcv['cid'],
            'inclusive_date_from' => gmt_to_local(human_to_unix($rcv['inc_date_from']." ".$time),'UP7'),
            'inclusive_date_to' => gmt_to_local(human_to_unix($rcv['inc_date_to']." ".$time),'UP7'),
            'date_added' => now(),
            'teacher_id' => $this->session->userdata['emp_info']['id']
        );
        $data['result'] = ($this->db->insert('acad_lesson_plans',$values)) ? true:false;
        $data['lp'] = mysql_insert_id();
        $data['idf'] = date('Y-m-d',$values['inclusive_date_from']);
        $data['idt'] = date('Y-m-d',$values['inclusive_date_to']);
        echo json_encode($data);
    }
    private function _get_lesson_plan_details() {
        $rcv = $this->input->post('data');
        $sql = $this->db->query("SELECT * FROM acad_lesson_plan_details WHERE lesson_plan_id = '".$rcv['lp']."' AND weekday = '".$rcv['day']."'");
        $data['lpd'] = null;
        if($sql->num_rows()>0) {
            $data['lpd'] = $sql->row()->lesson_plan_detail_id;
            $data['objectives'] = $sql->row()->objectives;
            $data['topic'] = $sql->row()->topic;
            $data['class_starter'] = $sql->row()->class_starter;
            $data['introduction'] = $sql->row()->introduction;
            $data['lesson_proper'] = $sql->row()->lesson_proper;
            $data['guided_practice'] = $sql->row()->guided_practice;
            $data['independent_practice'] = $sql->row()->independent_practice;
            $data['homework'] = $sql->row()->homework;
        }
        echo json_encode($data);
    }
    private function _insert_lesson_plan_detail() {
        $rcv = $this->input->post('data'); $data['lpd'] = null;
        $values = array(
            'lesson_plan_id' => $rcv['lp'],
            'weekday' => $rcv['weekday'],
            'objectives' => $rcv['objectives'],
            'topic' => $rcv['topic'],
            'class_starter' => $rcv['class_starter'],
            'introduction' => $rcv['introduction'],
            'lesson_proper' => $rcv['lesson_proper'],
            'guided_practice' => $rcv['guided_practice'],
            'independent_practice' => $rcv['independent_practice'],
            'homework' => $rcv['homework']
        );
        $data['result'] = ($this->db->insert('acad_lesson_plan_details',$values)) ? true:false;
        $data['result_lp'] = $data['result'];
        $data['lpd'] = mysql_insert_id();
        echo json_encode($data);
    }
    private function _update_lesson_plan_detail() {
        $rcv = $this->input->post('data'); $data['lpd'] = null;
        $time = date('h:i:s A',now());
        $values = array(
            'lesson_plan_id' => $rcv['lp'],
            'weekday' => $rcv['weekday'],
            'objectives' => $rcv['objectives'],
            'topic' => $rcv['topic'],
            'class_starter' => $rcv['class_starter'],
            'introduction' => $rcv['introduction'],
            'lesson_proper' => $rcv['lesson_proper'],
            'guided_practice' => $rcv['guided_practice'],
            'independent_practice' => $rcv['independent_practice'],
            'homework' => $rcv['homework']
        );
        
        $values_lp = array(
            'inclusive_date_from' => gmt_to_local(human_to_unix($rcv['idf']." ".$time),'UP7'),
            'inclusive_date_to' => gmt_to_local(human_to_unix($rcv['idt']." ".$time),'UP7'),
            'date_modified' => now()
        );
        $data['result_lp'] = ($this->db->update('acad_lesson_plans',$values_lp,"lesson_plan_id = ".$rcv['lp'])) ? true:false;
        $data['result'] = ($this->db->update('acad_lesson_plan_details',$values,"lesson_plan_detail_id = ".$rcv['lpd'])) ? true:false;
        $data['lpd'] = $rcv['lpd'];
        echo json_encode($data);
    }
}
/* End of classrooms.php */