<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Course_levels extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) show_404();
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_course_levels": $this->get_course_levels(); break;
            case "add_course_level": $this->_add_course_level(); break;
            case "edit_course_level": $this->_course_level_update(); break;
            case "remove_course_level": $this->_course_level_delete(); break;
            default: show_404();
        }
    }
    function get_course_levels($offset=0) {
        $sql = $this->db->query("SELECT * FROM course_level LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(id) as rows FROM course_level");
        $config['base_url'] = base_url().'acad/course_levels/get_course_levels/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="course_levels_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['res'] .= '<div class="data-list-type-1 row'.$i.'" id="courselevel_'.$row->id.'">
                            <label class="field-cont">Level:</label><label class="value-cont course-level-name">'.$row->name.'</label>
                            <div class="sprite-delete delete-course-level-btn to-right"></div>
                            <div class="sprite-square-empty edit-course-level-btn to-right"></div></div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _add_course_level() {
        $received = $this->input->post('data');
        $values = array(
            'name' => $received['name']
        );
        $data['result'] = ($this->db->insert('course_level',$values)) ? true:false;
        echo json_encode($data);
    }
    private function _course_level_update() {
        $received = $this->input->post('data');
        $values = array("name"=>$received['name']);
        $data['result'] = ($this->db->update('course_level',$values,"id = ".$received['clid'])) ? true:false;
        echo json_encode($data);
    }
    private function _course_level_delete() {
        if($this->db->delete('course_level',"id = ".$this->input->post('clid'))) {
            $data['result'] = ($this->db->update('acad_classes',array("level_id"=>""),"level_id = ".$this->input->post('clid'))) ? true: false;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
    }
}
/* End of course_levels.php */