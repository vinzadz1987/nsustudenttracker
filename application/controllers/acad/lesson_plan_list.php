<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Lesson_plan_list extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('date');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_lesson_plans": $this->lesson_plans(); break;
            case "retrieve_lesson_plan_details": $this->_get_lesson_plan_details(); break;
            case "update_lp_approval": $this->_edit_approval(); break;
            default: show_404();
        }
    }
    function lesson_plans($offset=0) {
        if($this->input->post('data')) {
            $rcv = $this->input->post('data');
            $start = gmt_to_local(human_to_unix($rcv['date_from']." 00:00:00 AM"),'UP7');
            $end = gmt_to_local(human_to_unix($rcv['date_to']." 11:59:59 PM"),'UP7');
            $sql = $this->db->query("SELECT lp.*, cl.class_type, cl.time, b.title AS book, c.name AS course, CONCAT(epd.firstname,' ',epd.lastname) AS teacher 
                FROM acad_lesson_plans lp, emp_personal_details epd, acad_classes cl, books b, courses c 
                WHERE lp.teacher_id=epd.emp_id AND lp.class_id=cl.class_id AND cl.book_id=b.book_id AND cl.course_id=c.course_id 
                AND lp.".$rcv['category']." > ".$start." AND lp.".$rcv['category']." < ".$end." 
                ORDER BY lp.date_added DESC LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(lp.lesson_plan_id) as rows 
                FROM acad_lesson_plans lp, emp_personal_details epd, acad_classes cl, books b, courses c 
                WHERE lp.teacher_id=epd.emp_id AND lp.class_id=cl.class_id AND cl.book_id=b.book_id AND cl.course_id=c.course_id 
                AND lp.".$rcv['category']." > ".$start." AND lp.".$rcv['category']." < ".$end." 
                ORDER BY lp.date_added DESC");
        } else {
            $sql = $this->db->query("SELECT lp.*, cl.class_type, cl.time, b.title AS book, c.name AS course, CONCAT(epd.firstname,' ',epd.lastname) AS teacher 
                FROM acad_lesson_plans lp, emp_personal_details epd, acad_classes cl, books b, courses c 
                WHERE lp.teacher_id=epd.emp_id AND lp.class_id=cl.class_id AND cl.book_id=b.book_id AND cl.course_id=c.course_id ORDER BY lp.date_added DESC LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(lp.lesson_plan_id) as rows 
                FROM acad_lesson_plans lp, emp_personal_details epd, acad_classes cl, books b, courses c 
                WHERE lp.teacher_id=epd.emp_id AND lp.class_id=cl.class_id AND cl.book_id=b.book_id AND cl.course_id=c.course_id ORDER BY lp.date_added DESC");
        }
        $config['base_url'] = base_url().'acad/lesson_plan_list/lesson_plans/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = '';
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="lesson_plans_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['res'] .= '<div class="data-list-type-2 row'.$i.' lp_row" id="lessonplan_'.$row->lesson_plan_id.'">
                            <label class="field-cont-2">Teacher:</label><label class="value-cont-2 classroom-name">'.$row->teacher.'</label>
                            <label class="field-cont-2">Type:</label><label class="value-cont-2 classes-scheduled">'.$row->class_type.'</label>
                            <label class="field-cont-2">Time:</label><label class="value-cont-2 classes-scheduled">'.$row->time.'</label>
                            <label class="field-cont-2">Course:</label><label class="value-cont-2 classes-scheduled">'.$row->course.'</label>
                            <label class="field-cont-2">Submitted:</label><label class="value-cont-2 classes-scheduled">'.date('M d, Y',$row->date_added).'</label>
                                <label class="field-cont-2">Modified:</label><label class="value-cont-2 classes-scheduled">'.date('M d, Y',$row->date_modified).'</label>
                            <label class="field-cont-2">From:</label><label class="value-cont-2 classes-scheduled">'.date('M d, Y',$row->inclusive_date_from).'</label>
                            <label class="field-cont-2">To:</label><label class="value-cont-2 classes-scheduled">'.date('M d, Y',$row->inclusive_date_to).'</label>
                            <label class="field-cont-2">Book:</label><label class="value-cont-3 classes-scheduled"><p>'.$row->book.'</p></label>                            
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _get_lesson_plan_details() {
        $sql = $this->db->query("SELECT lpd.*, lp.date_approved FROM acad_lesson_plan_details lpd, acad_lesson_plans lp 
            WHERE lpd.lesson_plan_id = '".$this->input->post('lpid')."' AND lpd.lesson_plan_id=lp.lesson_plan_id ORDER BY weekday ASC");
        $data['res'] = '<div class="lpv_header clr skyblue-gradient">
                            <div>Date</div>
                            <div>Monday</div>
                            <div>Tuesday</div>
                            <div>Wednesday</div>
                            <div>Thursday</div>
                            <div>Friday</div>
                        </div>';
        if($sql->num_rows()>0) {
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Objectives</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->objectives.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Topic</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->topic.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Class Starter</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->class_starter.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Introduction of the lesson</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->introduction.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Lesson proper</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->lesson_proper.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Guided Practice</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->guided_practice.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Independent Practice</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div><p>'.$row->independent_practice.'</p></div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['res'] .= '<div class="lpv_details clr">
                                <div>Homework</div>';
                                foreach($sql->result() as $row) {
                                    $data['res'] .= '<div>'.$row->homework.'</div>';
                                }
                                $data['res'] .= '<div></div></div>';
            $data['approved'] = ($sql->row()->date_approved != 0) ? date('M d, Y',$sql->row()->date_approved):0;
        }
        $data['res'] .= '<div class="clr"></div>';
        echo json_encode($data);
    }
    private function _edit_approval() {
        $rcv = $this->input->post('data');
        $sql = $this->db->query("SELECT * FROM acad_lesson_plan_details WHERE lesson_plan_id = '".$rcv['lpid']."'");
        if($sql->num_rows()>0) {
            $value = array(
                'date_approved'=> ($rcv['apprv'] == "true") ? now():0
            );
            $data['result'] = ($this->db->update('acad_lesson_plans',$value,"lesson_plan_id = '".$rcv['lpid']."'")) ? true:false;
        } else { 
            $data['result'] = ($this->db->update('acad_lesson_plans',array('date_approved'=>0),"lesson_plan_id = '".$rcv['lpid']."'")) ? true:false;
        }
        echo json_encode($data);
    }
}
/* End of lesson_plan_list.php */