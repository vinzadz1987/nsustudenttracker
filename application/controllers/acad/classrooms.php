<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Classrooms extends CI_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_classrooms": $this->get_classrooms(); break;
            case "classroom_add": $this->_add_classroom(); break;
            case "edit_classroom": $this->_classroom_update(); break;
            case "remove_classroom": $this->_classroom_delete(); break;
            case "classroom_occupants": $this->_get_classroom_users(); break;
            default: show_404();
        }
    }
    function get_classrooms($offset=0) {
        if($this->input->post('stat')==="occupied") {
            $sql = $this->db->query("SELECT r.* FROM classrooms r, acad_classes c 
                WHERE r.classroom_id = c.classroom_id AND r.room_name LIKE '".$this->input->post('like')."%' GROUP BY r.classroom_id LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(r.classroom_id) as rows FROM classrooms r, acad_classes c 
                WHERE r.classroom_id = c.classroom_id GROUP BY r.classroom_id");
        }
        else if($this->input->post('stat')==="vacant") {
            $sql = $this->db->query("SELECT * FROM classrooms WHERE classroom_id <> ALL(SELECT r.classroom_id FROM classrooms r, acad_classes c 
                WHERE r.classroom_id = c.classroom_id) AND room_name LIKE '".$this->input->post('like')."%' 
                GROUP BY classroom_id LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(classroom_id) as rows FROM classrooms 
                WHERE classroom_id NOT IN (SELECT r.classroom_id FROM classrooms r, acad_classes c 
                WHERE r.classroom_id = c.classroom_id) AND room_name LIKE '".$this->input->post('like')."%' 
                GROUP BY classroom_id");
        }
        else if($this->input->post('sched')!="") {
            $sql = $this->db->query("SELECT r.* FROM classrooms r, acad_classes c 
                WHERE r.classroom_id = c.classroom_id AND c.time = '".$this->input->post('sched')."' 
                AND r.room_name LIKE '".$this->input->post('like')."%'");
            $_count = $this->db->query("SELECT COUNT(r.classroom_id) as rows FROM classrooms r, acad_classes c 
                WHERE r.classroom_id = c.classroom_id AND c.time = '".$this->input->post('sched')."'");
        }
        else if($this->input->post('like')!="") {
            $sql = $this->db->query("SELECT * FROM classrooms WHERE room_name LIKE '".$this->input->post('like')."%' LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(classroom_id) as rows FROM classrooms WHERE room_name LIKE '".$this->input->post('like')."%'");
        }
        else {
            $sql = $this->db->query("SELECT * FROM classrooms LIMIT 15 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(classroom_id) as rows FROM classrooms");
        }
        $config['base_url'] = base_url().'acad/classrooms/get_classrooms/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $sql->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="classrooms_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $sql_class_count = $this->db->query("SELECT COUNT(class_id) as rows FROM acad_classes WHERE classroom_id = '".$row->classroom_id."'");
                $classes = ($sql_class_count->num_rows()>0) ? $sql_class_count->row()->rows:0;
                $data['res'] .= '<div class="data-list-type-1 row'.$i.'" id="classroom_'.$row->classroom_id.'">
                            <label class="field-cont">Room:</label><label class="value-cont classroom-name">'.$row->room_name.'</label>
                            <label class="field-cont">Classes:</label><label class="value-cont classes-scheduled">'.$classes.'</label>
                            <div class="sprite-delete delete-classroom-btn to-right"></div>
                            <div class="sprite-square-empty edit-classroom-btn to-right"></div>
                            <div class="sprite-inspect inspect-classroom-btn to-right"></div></div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _add_classroom() {
        $received = $this->input->post('data');
        $values = array(
            'room_name' => $received['room']
        );
        $data['result'] = ($this->db->insert('classrooms',$values)) ? true:false;
        echo json_encode($data);
    }
    private function _classroom_update() {
        $received = $this->input->post('data');
        $values = array("room_name" => $received['name']);
        $data['result'] = ($this->db->update('classrooms',$values,"classroom_id = ".$received['rid'])) ? true:false;
        echo json_encode($data);
    }
    private function _classroom_delete() {
        if($this->db->delete('classrooms',"classroom_id = ".$this->input->post('rid'))) {
            $data['result'] = ($this->db->update('acad_classes',array("classroom_id"=>""),"classroom_id = ".$this->input->post('rid'))) ? true: false;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
    }
    private function _get_classroom_users() {
        $sql = $this->db->query("SELECT CONCAT(ep.firstname,' ',ep.lastname) as teacher, r.room_name, ac.class_type, ac.time FROM emp_personal_details ep, classrooms r, acad_classes ac 
            WHERE ac.teacher_id = ep.emp_id AND ac.classroom_id = r.classroom_id AND r.classroom_id = '".$this->input->post('rid')."'");
        $data['occupants'] = "";
        if($sql->num_rows()>0) {
            $data['result'] = true;
            $i = 1;
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['occupants'] .= '<div class="data-list-type-1 row'.$i.'">
                            <label class="field-cont">Time:</label><label class="value-cont">'.$row->time.'</label>
                            <label class="field-cont">Teacher:</label><label class="value-cont">'.$row->teacher.'</label>
                            <label class="field-cont">Class Type:</label><label class="value-cont">'.$row->class_type.'</label>
                            </div>';
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
    }
}
/* End of classrooms.php */