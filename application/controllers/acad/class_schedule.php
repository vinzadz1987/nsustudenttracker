    <?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Class_schedule extends CI_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch($this->input->post('dir')) {
            case 'initialization': $this->_init(); break;
            case 'class_add': $this->_open_class(); break;
            case 'teacher_class': $this->_get_teacher_class(); break;
            case 'rm_class': $this->_delete_class(); break;
            case 'reality_chk': $this->_get_reality_status(); break;
            case 'class_type': $this->_class_type_change(); break;
            case 'fetch_class_students': $this->_get_class_students(); break;
            case 'fetch_students': $this->_get_students(); break;
            case 'add_student_to_class': $this->_add_class_student(); break;
            case 'rm_class_student': $this->_delete_class_student(); break;
            case 'chk_student_class': $this->_student_class_sched(); break;
            case 'retrieve_class_sched_values': $this->_get_class_values(); break;
            case 'class_sched_change': $this->_update_class_sched(); break;
            default: show_404();
        }
    }
    private function _init() {
        if($this->input->post('only')){
            switch($this->input->post('only')) {
                case 'courses': $courses = $this->db->query("SELECT * FROM courses");
                    $data['courses'] = ($courses->num_rows()>0) ? $courses->result():'No courses yet';
                    break;
            }
        } else {
            $teachers = $this->db->query("SELECT epd.emp_id, epd.firstname, epd.lastname 
                FROM emp_personal_details epd, emp_job_details ejd, job_title jt 
                WHERE (epd.emp_id=ejd.emp_id AND ejd.job_title=jt.id) AND jt.name='teacher'");
            $classrooms = $this->db->query("SELECT * FROM classrooms");
            $courses = $this->db->query("SELECT * FROM courses");
            $course_level = $this->db->query("SELECT * FROM course_level");
            $books = $this->db->query("SELECT * FROM books");
            $data['teachers'] = ($teachers->num_rows()>0) ? $teachers->result():'No teachers yet';
            $data['classrooms'] = ($classrooms->num_rows()>0) ? $classrooms->result():'No classrooms yet';
            $data['courses'] = ($courses->num_rows()>0) ? $courses->result():'No courses yet';
            $data['course_level'] = ($course_level->num_rows()>0) ? $course_level->result():'No course level yet';
            $data['books'] = ($books->num_rows()>0) ? $books->result():'No books level yet';
        }
        echo json_encode($data);
    }
    private function _open_class() {
        $received = $this->input->post('data');
        $values = array(
            'book_id'=>$received['book'],
            'course_id'=>$received['course'],
            'level_id'=>$received['course_level'],
            'classroom_id'=>$received['classroom'],
            'class_type'=>$received['class_type'],
            'teacher_id'=>$received['teacher'],
            'time'=>$received['time'],
            'date_added'=>time()
        );
        if($this->db->insert('acad_classes',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    }
    private function _get_teacher_class() {
        $t_id = $this->input->post('tid');
        $sql = $this->db->query("SELECT ac.class_id, ac.class_type, ac.time, 
            b.title AS book, c.name AS course, cl.name AS level, r.room_name AS room, CONCAT(ep.firstname,' ',ep.lastname) AS teacher
            FROM acad_classes ac, books b, courses c, course_level cl, classrooms r , emp_personal_details ep 
            WHERE ac.teacher_id='".$t_id."' AND ac.book_id=b.book_id AND ac.course_id=c.course_id 
            AND ac.level_id=cl.id AND ac.classroom_id=r.classroom_id AND ac.teacher_id=ep.emp_id AND ac.status = 1 ORDER BY ac.time");
        $res = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $res .= '<div class="sched_container"><table class="data-table-1" id="teacher_class">
                <caption>'.$sql->row()->teacher.' Class(es)</caption>
                <tr class="sub-tasks">
                    <th>Time</th>
                    <th>Room</th>
                    <th>Course</th>
                    <th>Course Level</th>
                    <th>Type</th>
                    <th>Book</th>
                    <th></th>
                </tr>';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $res .= '<tr class="row'.$i.'" id="'.$row->class_id.'">
                        <td class="sched_time">'.$row->time.'</td>
                        <td class="sched_room">'.$row->room.'</td>
                        <td class="sched_course">'.$row->course.'</td>
                        <td class="sched_level">'.$row->level.'</td>
                        <td class="sched_type">'.$row->class_type.'</td>
                        <td class="sched_book"><p>'.$row->book.'</p></td>';
                if($this->input->post('access')!='read') {
                $res .= '<td><div class="sprite-delete to-right delete_btn"></div>
                        <div class="sprite-square-empty to-right edit_class_btn"></div>
                        <div class="sprite-square-plus to-right add_btn"></div></td>';
                }
                $res .='</tr>';
            }
            $res .='</table></div>';
            $res .= '<div class="sched_container"><table class="data-table-1" id="teacher_class">
                <caption> Wednesday Schedule</caption>
                <tr class="sub-tasks">
                    <th>Time</th>
                    <th>Room</th>
                    <th>Course</th>
                    <th>Course Level</th>
                    <th>Type</th>
                    <th>Book</th>
                    <th></th>
                </tr>';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                switch ($row->time) {
                    case "08:00-09:50": $wed_time = "08:00-09:10"; break;
                    case "10:00-11:50": $wed_time = "09:20-10:30"; break;
                    case "12:40-02:30": $wed_time = "10:40-11:50"; break;
                    case "02:40-04:30": $wed_time = "12:40-01:50"; break;
                    case "04:40-06:30": $wed_time = "02:00-03:10"; break;
                    case "06:40-08:30": $wed_time = "03:20-04:30"; break;
                }
                $res .= '<tr class="row'.$i.'" id="'.$row->class_id.'">
                        <td class="sched_time">'.$wed_time.'</td>
                        <td class="sched_room">'.$row->room.'</td>
                        <td class="sched_course">'.$row->course.'</td>
                        <td class="sched_level">'.$row->level.'</td>
                        <td class="sched_type">'.$row->class_type.'</td>
                        <td class="sched_book"><p>'.$row->book.'</p></td>
                        <td></td>
                    </tr>';
            }
            $res .='</table></div>';
        }
        echo $res;
    }
    private function _delete_class() {
        $id = $this->input->post('cid');
//        $del1 = $this->db->query("DELETE FROM class_students WHERE class_id='".$id."'");
        $del2 = $this->db->query("UPDATE acad_classes SET status = 0 WHERE class_id='".$id."'");
        $data['result'] = ($del2) ? true:false;
        echo json_encode($data);
    }
    private function _get_reality_status() {
        $rcv = $this->input->post('data');
        $about_room = $this->db->query("SELECT CONCAT(ep.firstname,' ',ep.lastname) AS teacher, ac.class_type
                FROM emp_personal_details ep, acad_classes ac WHERE ac.classroom_id='".$rcv['room_id']."' AND ac.time='".$rcv['sched']."' AND ac.teacher_id=ep.emp_id");
        $about_teacher = $this->db->query("SELECT cl.room_name, CONCAT(ep.firstname,' ',ep.lastname) AS teacher 
                FROM emp_personal_details ep, classrooms cl, acad_classes ac WHERE ac.time='".$rcv['sched']."' AND ac.teacher_id='".$rcv['teacher']."' AND ac.teacher_id=ep.emp_id AND ac.classroom_id=cl.classroom_id");
        if($about_room->num_rows()>0) {
            $res['data']['occupied'] = true;
            $res['data']['teacher'] = $about_room->row()->teacher;
            $res['data']['class_type'] = $about_room->row()->class_type;
        }
        else if($about_teacher->num_rows()>0) {
            $res['data']['scheduled'] = true;
            $res['data']['teacher'] = $about_teacher->row()->teacher;
            $res['data']['room'] = $about_teacher->row()->room_name;
        }
        else {
            $res['data']['occupied'] = false;
        }
        echo json_encode($res);
    }
    private function _class_type_change() {
        if($this->input->post('course')==="") {
            $books = $this->db->query("SELECT * FROM books WHERE class_type='".$this->input->post('type')."'");
        } else {
            $books = $this->db->query("SELECT * FROM books WHERE class_type='".$this->input->post('type')."' AND course_id='".$this->input->post('course')."'");
        }
        $data['books'] = ($books->num_rows()>0) ? $books->result():'';
        echo json_encode($data);
    }
    private function _get_class_students() {
        $sql = $this->db->query("SELECT st.id, CONCAT(st.firstname,' ',st.lastname,' (',st.english_name,')') AS student_name 
            FROM students st, class_students cs WHERE cs.class_id='".$this->input->post('cid')."' AND cs.student_id=st.id");
        $res = "";
        if($sql->num_rows()>0) {
            foreach($sql->result() as $row) {
                $res .='<div class="class_student"><div class="to-left">'.$row->student_name.
                        '</div><div class="sprite-delete to-right del_student_btn" id="classstudent_'.$row->id.'"></div></div>';
            }
        }
        echo $res;
    }
    private function _get_students() {
        $students = $this->db->query("SELECT id, CONCAT(firstname,' ',lastname,' (',english_name,')') AS student_name FROM students");
        $data['students'] = ($students->num_rows()>0) ? $students->result():'No students yet';
        echo json_encode($data);
    }
    private function _add_class_student() {
        $values = array(
            "class_id"=>$this->input->post('cid'),
            "student_id"=>$this->input->post('sid')
        );
        $sql = $this->db->insert("class_students",$values);
        $data['result'] = ($sql) ? true:false;
        echo json_encode($data);
    }
    private function _delete_class_student() {
        $sql = $this->db->query("DELETE FROM class_students WHERE class_id='".$this->input->post('cid')."' AND student_id='".$this->input->post('sid')."'");
        $data['result'] = ($sql) ? true:false;
        echo json_encode($data);
    }
    private function _student_class_sched() {
        $sql = $this->db->query("SELECT CONCAT(ep.firstname,' ',ep.lastname) AS teacher, cl.room_name, ac.class_type 
            FROM emp_personal_details ep, acad_classes ac, classrooms cl , class_students cs
            WHERE ac.teacher_id=ep.emp_id AND ac.classroom_id=cl.classroom_id AND ac.class_id = cs.class_id
            AND ac.time='".$this->input->post('sched')."' AND cs.student_id='".$this->input->post('sid')."'");
        if($sql->num_rows()>0) {
            $data['scheduled'] = true;
            $data['details'] = $sql->row();
        } else {
            $data['scheduled'] = false;
        }
        echo json_encode($data);
    }
    private function _get_class_values() {
        $sql = $this->db->query("SELECT * FROM acad_classes WHERE class_id = '".$this->input->post('cid')."'");
        if($sql->num_rows()>0) {
            $data['book'] = $sql->row()->book_id;
            $data['course'] = $sql->row()->course_id;
            $data['level'] = $sql->row()->level_id;
            $data['classroom'] = $sql->row()->classroom_id;
            $data['class_type'] = $sql->row()->class_type;
            $data['time'] = $sql->row()->time;
        }
        echo json_encode($data);
    }
    private function _update_class_sched() {
        $rcv = $this->input->post('data');
        $values = array(
            'book_id'=>$rcv['book'],
            'course_id'=>$rcv['course'],
            'level_id'=>$rcv['course_level'],
            'classroom_id'=>$rcv['classroom'],
            'class_type'=>$rcv['class_type'],
            'time'=>$rcv['time']
        );
        $data['result'] = ($this->db->update('acad_classes',$values,"class_id = '".$this->input->post('cid')."'")) ? true:false;
        $data['teacher'] = $this->db->query("SELECT teacher_id FROM acad_classes WHERE class_id = '".$this->input->post('cid')."'")->row()->teacher_id;
        echo json_encode($data);
    }
}
/* End of class_schedule.php */