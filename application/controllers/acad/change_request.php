<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Change_request extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('date');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "send_change_request": $this->_add_change_request(); break;
            case "approve_change_request": $this->_update_request_approval(); break;
            default: show_404();
        }
    }
    private function _add_change_request() {
        $rcv = $this->input->post('data');
        $values = array(
            "student_name" => $rcv['student_name'],
            "student_id" => $rcv['student_id'],
            "request_detail" => $rcv['request_detail'],
            "date_added" => time()
            );
        foreach($rcv['request_set'] as $row) {
            switch ($row) {
                case "instructional_hours": $values["instructional_hours"]='1'; break;
                case "classroom": $values["classroom"]='1'; break;
                case "additional_class": $values["additional_class"]='1'; break;
                case "class_swapping": $values["class_swapping"]='1'; break;
                case "course_extension": $values["course_extension"]='1'; break;
                case "others": $values["others"]='1';
                    $values['others_specify'] = $this->input->post('others_specify');
                    break;
            }
        }
        $data['result'] = $this->db->insert("acad_change_requests",$values) ? true:false;
        echo json_encode($data);
    }
    private function _update_request_approval() {
        $rcv = $this->input->post('data');
        $time = date('h:i:s A',now());
        $values = array(
            "acad_approval" => $rcv['approval']=="true" ? now():0,
            "acad_date_rcv" => gmt_to_local(human_to_unix($rcv['acad_date_rcv']." ".$time),'UP7')
        );
        $data["result"] = $this->db->update('acad_change_requests',$values,"request_id = '".$rcv['rid']."'") ? true:false;
        echo json_encode($data);
    }
}
/* End of change_request.php */