<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Login extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('encrypt');
    }
    function index() {
        $this->load->view('welcome');
    }
    function submit() {
        try {
            $emp_data = $this->_authenticate($this->input->post('uname'),$this->input->post('passwd'),$this->input->post('dept'));
            if($emp_data) {
                $this->session->unset_userdata('emp_info');
                $this->session->set_userdata('emp_info',$emp_data);
				redirect('user/');
            } else {
                Throw new Exception('Authentication Failed.');
            }
        } catch(exception $e) {
            $this->session->set_flashdata('logmsg',$e->getMessage());
            redirect('login');
        }
    }
	function submit2() {
        try {
            $emp_data = $this->_authenticate2($this->input->post('uname'),$this->input->post('passwd'),$this->input->post('dept'));
            if($emp_data) {
                $this->session->unset_userdata('emp_info');
                $this->session->set_userdata('emp_info',$emp_data);
                redirect('guest/');
            } else {
                Throw new Exception('Authentication Failed.');
            }
        } catch(exception $e) {
            $this->session->set_flashdata('logmsg',$e->getMessage());
            redirect('login');
        }
    }
    function _authenticate($uname, $passwd,$dept) {
        $sql = $this->db->query("SELECT e.*, dd.dept_name, p.firstname, p.middlename, p.lastname
                                FROM emp_user e, department_details dd, department d, emp_personal_details p
                                WHERE e.username='".$uname."' AND d.deptd_id='".$dept."' AND dd.deptd_id=d.deptd_id AND e.emp_id = d.emp_id AND p.emp_id=e.emp_id AND e.status='active'");
        if($sql->num_rows()>0 && $this->encrypt->decode($sql->row()->password)==$passwd) {
            $data = array(
                'id' => $sql->row()->emp_id,
                'status' => $sql->row()->status,
                'department' => $sql->row()->dept_name,
                'date_added' => $sql->row()->date_added,
                'fullname' => ucfirst($sql->row()->firstname).' '.ucfirst($sql->row()->middlename).' '.ucfirst($sql->row()->lastname)
            );
            return $data;
        } else {
            return false;
        }
    }
	
	function _authenticate2($uname, $passwd,$dept) {
        $sql = $this->db->query("SELECT e.*, dd.dept_name, p.firstname, p.middlename, p.lastname
                                FROM emp_user e, department_details dd, department d, emp_personal_details p
                                WHERE e.username='".$uname."' AND d.deptd_id='".$dept."' AND dd.deptd_id=d.deptd_id AND e.emp_id = 12 AND p.emp_id=e.emp_id AND e.status='active'");
        if($sql->num_rows()>0 && $this->encrypt->decode($sql->row()->password)==$passwd) {
            $data = array(
                'id' => $sql->row()->emp_id,
                'status' => $sql->row()->status,
                'department' => $sql->row()->dept_name,
                'date_added' => $sql->row()->date_added,
                'fullname' => ucfirst($sql->row()->firstname).' '.ucfirst($sql->row()->middlename).' '.ucfirst($sql->row()->lastname)
            );
            return $data;
        } else {
            return false;
        }
    }
	
}
/* End of file login.php */
    
/*
 * 	SELECT tml.link FROM
	task_master_list AS tml
	LEFT JOIN tasks AS t USING(task_id)
	WHERE t.emp_id = 11
 * 
 * 
 */