<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Guest extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	public function index() {
		try {
			if( $this->session->userdata('emp_info') ) {
				#$data['view'] = 'first_load';
				$data['date'] = date('l, F d, Y'); 
				$this->load->view("guest_view");
			} 
		} catch (exception $e) {
			$this->session->sess_destroy();
            redirect(base_url());
		}
	}
	public function active() {
		$this->output
		->set_header('HTTP/1.0 200 OK')
		->set_header('HTTP/1.1 200 OK')
		->set_header('Last Modified: '.gmdate('D, d M Y H:i:s', time()).'GMT')
		->set_header('Cache-Control: no-store, no-cache, must-revalidate')
		->set_header('Cache-Control: post-check=0, pre-check=0')
		->set_header('Pragma: no-cache')
		->set_content_type('application/json')
		->set_output( json_encode(
			array(
				"active" => ( $this->session->userdata('emp_info') ) ? true : false
			)
		));
	}
	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
/* End of file user.php */
