<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Reports extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "allstudents": $this->get_all_sw_list(); break;
            case "allsw": $this->get_all_swt_list(); break;
            case "addreports": $this->get_addlist();break;
            case "add_ws": $this->_add_ws(); break;
            case "add_wst": $this->_add_wst(); break;
            case "retrieve_details": $this->_stocks_details();break;
            case "update_menu": $this->_menu_update();break;
            case "delete_date": $this->_delete_sw();break;
            case "delete_dr": $this->_delete_dr();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }

    public function get_all_sw_list($offset=0) {
        
        if($this->input->post('like')!="") {
        $mon = $this->db->query("SELECT * FROM ops_fb_shortage_wastage WHERE number_of_students LIKE '".$this->input->post('like')."%'  LIMIT 3 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(id) AS rows FROM ops_fb_shortage_wastage WHERE number_of_students LIKE '".$this->input->post('like')."%'");
       
        } else {
        $mon = $this->db->query("SELECT * FROM ops_fb_shortage_wastage LIMIT 3 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(id) AS rows FROM ops_fb_shortage_wastage ");
        }
        $config['base_url']= base_url().'ops/reports/get_all_sw_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 3;
        $this->pagination->initialize($config);
        $data['num_rows']= $mon->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($mon->num_rows()>0) {
            $data['res'] .= '<div class="stocks-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Number of Students</label></div>
                                <div><label>From</label></div>
                                <div><label>To</label></div>
                                
                            </div>
                            <div class="stocks_list_container1">';
            foreach($mon->result() as $row) {
                $data['res'] .='<div class="stocks-contents1">
                                <div>'.$row->number_of_students.'</div>
                                <div>'.$row->from.'</div>
                                <div>'.$row->to.'</div>
                                <div class="day">        
                                     <ul>
                                     
                                        <li class="white-litegray show-reports" title="Reports" id="st_'.$row->id.'">Show</li> 
                                        <li class="white-litegray add-reports" title="Add" id="st_'.$row->id.'">Add</li>
                                        <li class="white-litegray delete-reports" title="Reports" id="st_'.$row->id.'">Delete</li>
                                           
                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
     public function get_all_swt_list($offset=0) {
        
        
        $mon = $this->db->query("SELECT oswt.id AS oswt_id, oswt.day, oswt.shortage_wastage_id, oswt.b_shortage,
                                        oswt.b_wastage, oswt.b_comment, oswt.l_shortage,
                                        sw.number_of_students AS nos, sw.from, sw.to,
                                        oswt.l_wastage, oswt.l_comment, oswt.b_wastage,
                                        oswt.d_wastage, oswt.d_shortage,
                                        sw.id, od.name AS reports_day 
                                 FROM  ops_shortage_wastage_tracker oswt, ops_day od, ops_fb_shortage_wastage sw 
                                 WHERE oswt.day=od.id AND oswt.shortage_wastage_id=sw.id AND oswt.shortage_wastage_id='".$this->input->post('bid')."'");

        $data['res'] = "";
        if($mon->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">Reports Details</div>
                            <div class="sw-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Number of Students:</label> <label class="sw-text">'.$mon->row()->nos.'</label></div>
                                <div><label>From:</label> <label class="sw-text">'.$mon->row()->from.'</label></div>
                                <div><label>To:</label> <label class="sw-text">'.$mon->row()->to.'</label></div>
                                
                                
                            </div>
                            <div class="sw-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Day</label></div>
                                <div><label>Breakfast</label></div>
                                <div><label>Lunch</label></div>
                                <div><label>Dinner</label></div>
                                
                            </div>
                            <div class="sw_list_container1">';
            foreach($mon->result() as $row) {
                $data['res'] .='<div class="sw-contents1">
                                <div>'.$row->reports_day.'</div>
                                <div>'.$row->b_shortage.' '.$row->b_wastage.'% '.$row->b_comment.'</div>
                                <div>'.$row->l_shortage.' '.$row->l_wastage.'% '.$row->l_comment.'</div>
                                <div>'.$row->d_shortage.' '.$row->d_wastage.'% <button class="blue-button delete-daily-report" id="st_'.$row->oswt_id.'">x</button></div>        
                                </div>';
            }
            $data['res'] .= '</div></div>
                            <div class="white-litegray hint">1st column: shortage of food | 2nd column: wastage of food (based on percentage) | 3rd column: quality of food (survey – rate from 1 to 5, 5 is the highest)</div>';
        }
        echo json_encode($data);
    }
    public function get_addlist($offset=0) {
        
        $sw = $this->db->query("SELECT oswt.id, oswt.day, sw.id AS sw_id, oswt.shortage_wastage_id, oswt.b_shortage,
                                        oswt.b_wastage, oswt.b_comment, oswt.l_shortage,
                                        sw.number_of_students AS nos, sw.from, sw.to,
                                        oswt.l_wastage, oswt.l_comment, oswt.b_wastage,
                                        oswt.d_wastage, oswt.d_shortage,
                                        sw.id, od.name AS reports_day 
                                 FROM  ops_shortage_wastage_tracker oswt, ops_day od, ops_fb_shortage_wastage sw 
                                 WHERE oswt.day=od.id AND sw.id='".$this->input->post('bid')."'");
        $data['sw'] = ($sw) ? $sw->row(): "Not found";
       
        echo json_encode($data);
    }
     private function _add_ws() {
        $received = $this->input->post('data');
        
        $values = array(
            'number_of_students'=>$received['number_of_students'],
            'from'=>$received['from'],
            'to'=>$received['to']     
        );
        if($this->db->insert('ops_fb_shortage_wastage',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    }
     private function _add_wst() {
        $received = $this->input->post('data');
        $values = array(
            'shortage_wastage_id'=>$received['sw_id'],
            'day'=>$received['day'],
            'b_shortage'=>$received['b_shortage'],
            'b_wastage'=>$received['b_wastage'],
            'b_comment'=>$received['b_comment'],
            'l_shortage'=>$received['l_shortage'],
            'l_wastage'=>$received['l_wastage'],
            'l_comment'=>$received['l_comment'],
            'd_shortage'=>$received['d_shortage'],
            'd_wastage'=>$received['d_shortage']
        );
        if($this->db->insert('ops_shortage_wastage_tracker',$values)) {      
            echo "added";
        } else {
            echo "failed";
        }
    }
   
    
    private function _stocks_details() { 
        $ingredients = $this->db->query("SELECT * FROM ops_ingredient WHERE id='".$this->input->post('bid')."'");
        $data['ingredients'] = ($ingredients) ? $ingredients->row(): "Not found";
       
        echo json_encode($data);
    }
    
    private function _init(){
        $day = $this->db->query("SELECT * FROM ops_day");
        $data['day'] = ($day->num_rows()>0) ? $day->result():'No day';  
        echo json_encode($data);
    }   
   
    private function _delete_sw() {
        $sql = $this->db->query("DELETE FROM ops_fb_shortage_wastage WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_sw_list();
    }
     private function _delete_dr() {
        $sql = $this->db->query("DELETE FROM ops_shortage_wastage_tracker WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_sw_list();
        
    }
  
}
/* End of Stocks  */