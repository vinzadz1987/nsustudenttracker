<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Hk_inventory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_amin_inventory": $this->get_all_amin_inventory();break;
            case "all_minb_inventory": $this->get_all_minb_inventory();break;
            case "all_linen_inventory": $this->get_all_linen_inventory();break;
            case "all_tr_amin_inv": $this->get_all_amin_tr_inventory();break;
            case "all_tr_minb_inv": $this->get_all_minb_tr_inventory();break;
            case "all_tr_linen_inv": $this->get_all_linen_tr_inventory();break;
            case "add_hk_inventory": $this->_add_hk_inventory();break;
            case "add_amin_inventory": $this->_add_amin_inventory();break;
            case "add_minb_inventory": $this->_add_minb_inventory();break;
            case "add_linen_inventory": $this->_add_linen_inventory();break;
            case "get_inv_details": $this->_inventory_details();break;
            case "update_inv_amin": $this->_update_amin_inv();break;
            case "update_inv_minb": $this->_update_minb_inv();break;
            case "update_inv_linen": $this->_update_linen_inv();break;
            case "delete_amen_date": $this->_delete_amin_date();break;
            case "delete_minb_date": $this->_delete_minb_date();break;
            case "delete_linen_date": $this->_delete_linen_date();break;
            case "delete_amin_inv": $this->_delete_amin_inv();break;
            case "delete_minb_inv": $this->_delete_minb_inv();break;
            case "delete_linen_inv": $this->_delete_linen_inv();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    public function get_all_amin_inventory($offset=0) {
        
       if($this->input->post('like')!="") {
        $inventory = $this->db->query("SELECT * 
                                  FROM ops_hk_inventory 
                                  WHERE cat_id='3' AND inv_to LIKE '".$this->input->post('like')."%' ORDER BY inv_from DESC  LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_hk_inventory 
                                    WHERE cat_id='3' AND inv_to LIKE '".$this->input->post('like')."%'");
       }else {
        $inventory = $this->db->query("SELECT * 
                                 FROM ops_hk_inventory
                                 WHERE cat_id='3' ORDER BY inv_from DESC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_hk_inventory");
       }
        $config['base_url']= base_url().'ops/hk_inventory/get_all_amin_inventory/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $inventory->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= ' <div class="items-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Ending Balance Date</label></div>
                                <div><label>Inventory as of </label></div>
                                   
                            </div>
                            <div class="items_list_container1">';
            foreach($inventory->result() as $row) {
                $data['res'] .='<div class="items-contents1">
                                <div>'.$row->inv_from.'</div>
                                <div>'.$row->inv_to.'</div>
                                    
                                <div class="inv">        
                                     <ul>
                                        
                                        <li class="white-litegray add-amin-inventory" title="Add Inventory" id="st_'.$row->inv_id.'">Add</li>        
                                        <li class="white-litegray show-amin-inventory" title="Show Inventory" id="st_'.$row->inv_id.'">Show</li>
                                        <li class="white-litegray delete-amin-date" title="Delete Date" id="st_'.$row->inv_id.'">Delete</li>
                                           
                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_minb_inventory($offset=0) {
        
       if($this->input->post('like')!="") {
        $inventory = $this->db->query("SELECT * 
                                  FROM ops_hk_inventory 
                                  WHERE cat_id='1' AND inv_to LIKE '".$this->input->post('like')."%' ORDER BY inv_from DESC  LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_hk_inventory 
                                    WHERE cat_id='1' AND inv_to LIKE '".$this->input->post('like')."%'");
       }else {
        $inventory = $this->db->query("SELECT * 
                                 FROM ops_hk_inventory
                                 WHERE cat_id='1' ORDER BY inv_from DESC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_hk_inventory");
       }
        $config['base_url']= base_url().'ops/hk_inventory/get_all_amin_inventory/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $inventory->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= ' <div class="items-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Ending Balance Date</label></div>
                                <div><label>Inventory as of </label></div>
                                   
                            </div>
                            <div class="items_list_container1">';
            foreach($inventory->result() as $row) {
                $data['res'] .='<div class="items-contents1">
                                <div>'.$row->inv_from.'</div>
                                <div>'.$row->inv_to.'</div>
                                    
                                <div class="minibar">        
                                     <ul>
                                        
                                        <li class="white-litegray add-minb-inventory" title="Add Mini Bar Inventory" id="st_'.$row->inv_id.'">Add</li>        
                                        <li class="white-litegray show-minb-inventory" title="Show Mini Bar Inventory" id="st_'.$row->inv_id.'">Show</li>        
                                        <li class="white-litegray delete-minb-date" title="Delete Mini Bar Inventory" id="st_'.$row->inv_id.'">Delete</li>        
                                                
                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
     public function get_all_linen_inventory($offset=0) {
        
       if($this->input->post('like')!="") {
        $inventory = $this->db->query("SELECT * 
                                  FROM ops_hk_inventory 
                                  WHERE cat_id='2' AND inv_to LIKE '".$this->input->post('like')."%' ORDER BY inv_from DESC  LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_hk_inventory 
                                    WHERE cat_id='2' AND inv_to LIKE '".$this->input->post('like')."%'");
       }else {
        $inventory = $this->db->query("SELECT * 
                                 FROM ops_hk_inventory
                                 WHERE cat_id='2' ORDER BY inv_from DESC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_hk_inventory");
       }
        $config['base_url']= base_url().'ops/hk_inventory/get_all_linen_inventory/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $inventory->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= ' <div class="items-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Ending Balance Date</label></div>
                                <div><label>Inventory as of </label></div>
                                   
                            </div>
                            <div class="items_list_container1">';
            foreach($inventory->result() as $row) {
                $data['res'] .='<div class="items-contents1">
                                <div>'.$row->inv_from.'</div>
                                <div>'.$row->inv_to.'</div>
                                    
                                <div class="inv-linen">        
                                     <ul>
                                        
                                        <li class="white-litegray add-linen-inventory" title="Add Linen Inventory" id="st_'.$row->inv_id.'">Add</li>        
                                        <li class="white-litegray show-linen-inventory" title="Show Linen Inventory" id="st_'.$row->inv_id.'">Show</li>        
                                        <li class="white-litegray delete-linen-date" title="Delete '.$row->inv_to.' Inventory Date" id="st_'.$row->inv_id.'">Delete</li>        
                                    

                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_amin_tr_inventory() {
        
          $inventory = $this->db->query("SELECT hia.amin_id, hia.inv_id, ohi.inv_from AS amen_from,
                                                ohi.inv_to AS amen_to, ohit.item_desc AS amen_item_desc, ohit.price AS price,
                                                hia.beg_bal, hia.purchased, hia.t_out, hia.balance, hia.psy_count,
                                                hia.remarks, hia.reconcilation
                                         FROM hk_inv_amenities hia, ops_hk_inventory ohi, ops_hk_items ohit
                                         WHERE hia.inv_id=ohi.inv_id AND hia.item_id=ohit.item_id
                                         AND hia.inv_id='".$this->input->post('bid')."'");
        
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">AMENITIES INVENTORY</div> 
                            <div class="inv-header1 skyblue-gradient"> 
                                <div><label>Forwarded Balance:</label> <label>'.$inventory->row()->amen_from.'</label></div>
                                <div><label>Inventory as of:</label> <label>'.$inventory->row()->amen_to.'</label><button class="skyblue-gradient" onclick="PrintDivamin();">Print</button></div>
                                
                             </div>
                            <div class="hk-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>ITEM DESCRIPTION</label></div>
                                <div><label>BEG BALANCE</label></div>
                                <div><label>PURCHASED</label></div>
                                <div><label>T. OUT</label></div>
                                <div><label>BALANCE</label></div>
                                <div><label>PHYSICAL COUNT</label></div>
                                <div><label>REMARKS</label></div>
                                <div><label>RECONCILATION</label></div>
                                
                                
                                
                            </div>
                            <div class="hk_list_container1 white-shadow">';
            foreach($inventory->result() as $row) {
                $data['res'] .='<div class="hk-contents1 skyblue-gradient ">
                                <div>'.$row->amen_item_desc.' '.$row->price.'</div>
                                <div>'.$row->beg_bal.'</div>
                                <div>'.$row->purchased.'</div>
                                <div>'.$row->t_out.'</div>
                                <div class="sw-text">'.$row->balance.'</div>
                                <div>'.$row->psy_count.'</div>
                                <div>'.$row->remarks.'</div>
                                <div>'.$row->reconcilation.'
                                <div class="inv-amin">        
                                     <ul>
                                        
                                        <li class="update-tr-amin-inventory" title="Update Inventory" id="st_'.$row->amin_id.'">Update</li>        
                                        <li class="delete-tr-amin-inventory" title="Delete Inventory" id="st_'.$row->amin_id.'">x</li>        
                                        

                                    </ul>
                                </div></div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
     public function get_all_minb_tr_inventory() {
        
          $inventory = $this->db->query("SELECT him.mini_id, him.inv_id, ops_i.inv_from AS minb_from,
                                                ops_i.inv_to AS minb_to, ohit.item_desc AS item_desc,
                                                ohit.price AS item_price, him.beg_bal, him.purchased,
                                                him.t_stock, him.out, him.balance, him.psy_count, 
                                                him.hotel_room_from, him.hotel_room_to, him.remarks
                                         FROM hk_inv_minibar him, ops_hk_inventory ops_i, ops_hk_items ohit
                                         WHERE him.item_id=ohit.item_id AND him.inv_id=ops_i.inv_id
                                               AND him.inv_id='".$this->input->post('bid')."'");
        
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">MINI BAR INVENTORY</div>
                            <div class="inv-header1 skyblue-gradient"> 
                                <div><label>Forwarded Balance:</label> <label>'.$inventory->row()->minb_from.'</label></div>
                                <div><label>Inventory as of:</label> <label>'.$inventory->row()->minb_to.'</label><button class="skyblue-gradient" onclick="PrintDiv();">Print</button></div>
                                
                             </div>
                            <div class="hk-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>ITEM DESCRIPTION</label></div>
                                <div><label>BEG BALANCE</label></div>
                                <div><label>PURCHASED</label></div>
                                <div><label>T.STCK</label></div>
                                <div><label>OUT</label></div>
                                <div><label>BALANCE</label></div>
                                <div><label>PSY COUNT</label></div>
                                <div><label>HOTEL ROOMS</label></div>
                                <div><label>REMARKS</label></div>
                                
                                
                                
                            </div>
                            <div class="hk_list_container1  white-shadow">';
            foreach($inventory->result() as $row) {
                $data['res'] .='<div class="hk-contents1 skyblue-gradient">
                                <div>'.$row->item_desc.' '.$row->item_price.' </div>
                                <div>'.$row->beg_bal.'</div>
                                <div>'.$row->purchased.'</div>
                                <div class="white-shadow">'.$row->t_stock.'</div>
                                <div>'.$row->out.'</div>
                                <div class="sw-text">'.$row->balance.'</div>
                                <div>'.$row->psy_count.'</div>
                                <div>'.$row->hotel_room_from.'-'.$row->hotel_room_to.'</div>
                                <div>'.$row->remarks.'
                                <div class="inv-minb">        
                                     <ul>                                  
                                        <li class="update-tr-minb-inventory" title="Update Mini Bar Inventory" id="st_'.$row->mini_id.'">Update</li>        
                                        <li class="delete-tr-minb-inventory" title="Delete Mini Bar Inventory" id="st_'.$row->mini_id.'">x</li>        
                                     </ul>
                                </div></div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
     public function get_all_linen_tr_inventory() {
        
          $inventory = $this->db->query("SELECT hil.linen_id, hil.inv_id, ops_hi.inv_from AS linen_from,
                                                ops_hi.inv_to AS linen_to, hil.item_id, ops_hit.item_desc AS linen_item,
                                                ops_hit.price AS item_price, hil.beg_bal, hil.purchased, hil.stock_rooms,
                                                hil.rooms, hil.pick_up_laundry AS pul, hil.sub_total, hil.damaged, hil.total
                                         FROM hk_inv_linen hil, ops_hk_inventory ops_hi, ops_hk_items ops_hit
                                         WHERE hil.inv_id=ops_hi.inv_id AND hil.item_id=ops_hit.item_id AND hil.inv_id='".$this->input->post('bid')."'");
        
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">LINENS INVENTORY</div>
                            <div class="inv-header1 skyblue-gradient"> 
                                <div><label>Forwarded Balance:</label> <label>'.$inventory->row()->linen_from.'</label></div>
                                <div><label>Inventory as of:</label> <label>'.$inventory->row()->linen_to.'</label><button class="skyblue-gradient" onclick="PrintDivlinen();">Print</button></div>
                                
                             </div>
                            <div class="hk-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>ITEM DESCRIPTION</label></div>
                                <div><label>BEG BALANCE</label></div>
                                <div><label>PURCHASED</label></div>
                                <div><label>STOCK ROOM</label></div>
                                <div><label>ROOMS</label></div>
                                <div><label>P-UP FOR LAUNDRY</label></div>
                                <div><label>SUBTOTAL</label></div>
                                <div><label>DAMAGED</label></div>
                                <div><label>TOTAL</label></div>
                                
                                
                                
                            </div>
                            <div class="hk_list_container1  white-shadow">';
            foreach($inventory->result() as $row) {
                $data['res'] .='<div class="hk-contents1 skyblue-gradient">
                                <div>'.$row->linen_item.' '.$row->item_price.' </div>
                                <div>'.$row->beg_bal.' </div>
                                <div>'.$row->purchased.' </div>
                                <div>'.$row->stock_rooms.' </div>
                                <div>'.$row->rooms.' </div>
                                <div>'.$row->pul.' </div>
                                <div class="white-shadow">'.$row->sub_total.' </div>
                                <div>'.$row->damaged.' </div>
                                <div class="sw-text">'.$row->total.'
                                <div class="linen">        
                                     <ul>                                  
                                        <li class="ch update-tr-linen-inventory" title="Update Linen Inventory" id="st_'.$row->linen_id.'">Update</li>        
                                        <li class="ch delete-tr-linen-inventory" title="Delete Linen '.$row->linen_item.' Inventory item" id="st_'.$row->linen_id.'">x</li>        
                                     </ul>
                                </div></div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
     private function _add_hk_inventory() {
        $received = $this->input->post('data');
        $values = array(
            
            'inv_from'=>$received['inv_from'],
            'inv_to'=>$received['inv_to'],
            'cat_id'=>$received['cat_id']
              
        );
        $sql = $this->db->query("SELECT * FROM ops_hk_inventory WHERE inv_to='".$received['inv_to']."' AND cat_id='".$received['cat_id']."'");
        if($sql->result()==true) {
          echo "failed";  
        }else{
         if($this->db->insert('ops_hk_inventory',$values)){
          echo "added";    
         } 
           
        }   
        
    } 
    
    private function _add_amin_inventory() {
        $received = $this->input->post('data');
        $values = array(
            
            'inv_id'=>$received['amin_id'],
            'item_id'=>$received['amen_item'],
            'beg_bal'=>$received['amen_beg_bal'],
            'purchased'=>$received['amen_purchased'],
            't_out'=>$received['amen_t_out'],
            'balance'=>$received['amen_bal'],
            'psy_count'=>$received['amen_p_count'],
            'remarks'=>$received['amen_remark'],
            'reconcilation'=>$received['amen_recon']
              
        );
        $sql = $this->db->query("SELECT * FROM hk_inv_amenities WHERE item_id='".$received['amen_item']."' AND inv_id='".$received['amin_id']."'");
        if($sql->result()==true) {
          echo "failed"; 
        }else {
           if($this->db->insert('hk_inv_amenities',$values)) {
            echo "added";
        } 
        }
        
    } 
    
     private function _add_minb_inventory() {
        $received = $this->input->post('data');
        $values = array(
            
            'inv_id'=>$received['inv_id'],
            'item_id'=>$received['minb_item'],
            'beg_bal'=>$received['minb_begbal'],
            'purchased'=>$received['minb_purchased'],
            't_stock'=>$received['minb_t_stock'],
            'out'=>$received['minb_out'],
            'balance'=>$received['minb_balance'],
            'psy_count'=>$received['minb_psy_count'],
            'hotel_room_from'=>$received['hotel_from'],
            'hotel_room_to'=>$received['hotel_to'],
            'remarks'=>$received['minb_remarks']
            
        );
        $sql = $this->db->query("SELECT * FROM hk_inv_minibar WHERE item_id='".$received['minb_item']."' AND inv_id='".$received['inv_id']."'");
        if($sql->result()==true) {
          echo "failed"; 
        }else {
           if($this->db->insert('hk_inv_minibar',$values)) {
            echo "added";
        } 
        }
        
    } 
    
     private function _add_linen_inventory() {
        $received = $this->input->post('data');
        $values = array(
            
            'inv_id'=>$received['inv_id'],
            'item_id'=>$received['linen_item'],
            'beg_bal'=>$received['linen_begbal'],
            'purchased'=>$received['linen_purchased'],
            'stock_rooms'=>$received['linen_stock_room'],
            'rooms'=>$received['linen_rooms'],
            'pick_up_laundry'=>$received['linen_pck_upl'],
            'sub_total'=>$received['linen_s_total'],
            'damaged'=>$received['linen_damaged'],
            'total'=>$received['linen_total']
            
        );
        $sql = $this->db->query("SELECT * FROM hk_inv_linen WHERE item_id='".$received['linen_item']."' AND inv_id='".$received['inv_id']."'");
        if($sql->result()==true) {
          echo "failed"; 
        }else {
           if($this->db->insert('hk_inv_linen',$values)) {
            echo "added";
        } 
        }
        
    } 
   
     private function _update_amin_inv() {
        $received = $this->input->post('data');
        $values = array(
            "amin_id"=>$received['amin_id'],
            "item_id"=>$received['amen_item'],
            "beg_bal"=>$received['beg_bal'],
            "purchased"=>$received['purchased'],
            "t_out"=>$received['t_out'],
            "balance"=>$received['balance'],
            "psy_count"=>$received['p_count'],
            "remarks"=>$received['remarks'],
            "reconcilation"=>$received['recon']
        );
        
       
         $data['result'] = ($this->db->update('hk_inv_amenities',$values,"amin_id = ".$received['amin_id'])) ? true:false;
      
        echo json_encode($data);
    } 
    
    private function _update_minb_inv() {
        $received = $this->input->post('data');
        $values = array(
            
            "mini_id"=>$received['mini_id'],
            "item_id"=>$received['minb_item'],
            "beg_bal"=>$received['minb_beg_bal'],
            "purchased"=>$received['minb_purchased'],
            "t_stock"=>$received['minb_t_stock'],
            "out"=>$received['minb_out'],
            "balance"=>$received['minb_balance'],
            "psy_count"=>$received['minb_psy_count'],
            "hotel_room_from"=>$received['minb_hotel_from'],
            "hotel_room_to"=>$received['minb_hotel_to'],
            "remarks"=>$received['minb_remarks']
           
        );
         $data['result'] = ($this->db->update('hk_inv_minibar',$values,"mini_id = ".$received['mini_id'])) ? true:false;      
         echo json_encode($data);
    } 
   
     private function _update_linen_inv() {
        $received = $this->input->post('data');
        $values = array(
            
            "linen_id"=>$received['linen_id'],
            "item_id"=>$received['linen_item'],
            "beg_bal"=>$received['linen_begbal'],
            "purchased"=>$received['linen_purchased'],
            "stock_rooms"=>$received['linen_stock_room'],
            "rooms"=>$received['linen_rooms'],
            "pick_up_laundry"=>$received['linen_pul'],
            "sub_total"=>$received['linen_stotal'],
            "damaged"=>$received['linen_damaged'],
            "total"=>$received['linen_total']
          
        );
         $data['result'] = ($this->db->update('hk_inv_linen',$values,"linen_id = ".$received['linen_id'])) ? true:false;      
         echo json_encode($data);
    } 
    
    private function _inventory_details() {
           $amin_inv = $this->db->query("SELECT *
                                     FROM ops_hk_inventory
                                     WHERE inv_id ='".$this->input->post('bid')."'");
           $data['amin_inv'] = ($amin_inv) ? $amin_inv->row(): "Not found";
           $amin_tr_inv = $this->db->query("SELECT hia.amin_id, hia.inv_id, ohi.inv_from AS amen_from,
                                                ohi.inv_to AS amen_to, hia.item_id, ohit.item_desc AS amen_item_desc,
                                                hia.beg_bal, hia.purchased, hia.t_out, hia.balance, hia.psy_count,
                                                hia.remarks, hia.reconcilation
                                         FROM hk_inv_amenities hia, ops_hk_inventory ohi, ops_hk_items ohit
                                         WHERE hia.inv_id=ohi.inv_id AND hia.item_id=ohit.item_id
                                         AND hia.amin_id='".$this->input->post('bid')."'");
           $data['amin_tr_inv'] = ($amin_tr_inv) ? $amin_tr_inv->row(): "Not Found"; 
           $minb_tr_inv = $this->db->query("SELECT him.mini_id, him.inv_id, ops_i.inv_from AS minb_from,
                                                ops_i.inv_to AS minb_to, him.item_id, ohit.item_desc AS item_desc,
                                                ohit.price AS item_price, him.beg_bal, him.purchased,
                                                him.t_stock, him.out, him.balance, him.psy_count, 
                                                him.hotel_room_from, him.hotel_room_to, him.remarks
                                         FROM hk_inv_minibar him, ops_hk_inventory ops_i, ops_hk_items ohit
                                         WHERE him.item_id=ohit.item_id AND him.inv_id=ops_i.inv_id
                                               AND him.mini_id='".$this->input->post('bid')."'");
           $data['minb_tr_inv'] = ($minb_tr_inv) ? $minb_tr_inv->row(): "Not Found";
          
           $linen_tr_inv = $this->db->query("SELECT hil.linen_id, hil.inv_id, ops_hi.inv_from AS linen_from,
                                                ops_hi.inv_to AS linen_to, hil.item_id, ops_hit.item_desc AS linen_item,
                                                ops_hit.price AS item_price, hil.beg_bal, hil.purchased, hil.stock_rooms,
                                                hil.rooms, hil.pick_up_laundry AS pul, hil.sub_total, hil.damaged, hil.total
                                         FROM hk_inv_linen hil, ops_hk_inventory ops_hi, ops_hk_items ops_hit
                                         WHERE hil.inv_id=ops_hi.inv_id AND hil.item_id=ops_hit.item_id AND hil.linen_id='".$this->input->post('bid')."'");
            $data['linen_tr_inv'] = ($linen_tr_inv) ? $linen_tr_inv->row(): "Not Found";
           echo json_encode($data);
    } 
    
    private function _init(){
        $items = $this->db->query("SELECT * FROM ops_hk_items WHERE item_cat='3'");
        $data['amin_items'] = ($items->num_rows()>0) ? $items->result():'No item';
        $mb_items = $this->db->query("SELECT * FROM ops_hk_items WHERE item_cat='1'");
        $data['minb_items'] = ($mb_items->num_rows()>0) ? $mb_items->result():'No item';
        $linen_items = $this->db->query("SELECT * FROM ops_hk_items WHERE item_cat='2'");
        $data['linen_items'] = ($linen_items->num_rows()>0) ? $linen_items->result():'No Month';
        $rooms = $this->db->query("SELECT * FROM room_number_ids");
        $data['rooms'] = ($rooms->num_rows()>0) ? $rooms->result():'No Month';
        echo json_encode($data);
    }   
   
    private function _delete_amin_date() {
        $sql = $this->db->query("DELETE FROM ops_hk_inventory WHERE inv_id='".$this->input->post('pid')."'");
        $this->get_all_amin_inventory();
        
    }
    private function _delete_minb_date() {
        $sql = $this->db->query("DELETE FROM ops_hk_inventory WHERE inv_id='".$this->input->post('pid')."'");
        $this->get_all_minb_inventory();
        
    }
    
    private function _delete_linen_date() {
        $sql = $this->db->query("DELETE FROM ops_hk_inventory WHERE inv_id='".$this->input->post('pid')."'");
        $this->get_all_linen_inventory();
        
    }
     
     private function _delete_amin_inv() {
        $sql = $this->db->query("DELETE FROM hk_inv_amenities WHERE amin_id='".$this->input->post('pid')."'");
        $this->get_all_amin_inventory();
        
    }
    
     private function _delete_minb_inv() {
        $sql = $this->db->query("DELETE FROM hk_inv_minibar WHERE mini_id='".$this->input->post('pid')."'");
        $this->get_all_minb_inventory();
        
    }
   
     private function _delete_linen_inv() {
        $sql = $this->db->query("DELETE FROM hk_inv_linen WHERE linen_id='".$this->input->post('pid')."'");
        $this->get_all_linen_inventory();
        
    }
}
/* End of minibar inventory  */