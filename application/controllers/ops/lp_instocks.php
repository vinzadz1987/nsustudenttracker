<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Lp_instocks extends CI_Controller {
    var $user_session;
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        
        if(!$this->user_session=$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_item_stocks": $this->get_all_instocks();break;
            default: show_404();
        }
    }
    
    public function get_all_instocks($offset=0) {
       $user_id =  $this->user_session['id'];
   
       if($this->input->post('like')!="") {
        $instock = $this->db->query("SELECT pd.*, p.* 
                                  FROM prs_details pd, prs p
                                  WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} AND p.dept_head_appvl='approved' AND p.fin_head_appvl='approved' AND pd.description LIKE '".$this->input->post('like')."%' ORDER BY pd.description ASC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(pd.prs_details_id) AS rows FROM prs_details pd, prs p 
                                    WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} AND p.dept_head_appvl='approved' AND p.fin_head_appvl='approved' AND pd.description LIKE '".$this->input->post('like')."%'");
       }else {
        $instock = $this->db->query("SELECT pd.*, p.* 
                                     FROM prs_details pd, prs p 
                                     WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} AND p.dept_head_appvl='approved' AND p.fin_head_appvl='approved' ORDER BY pd.description ASC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(pd.prs_details_id) AS rows FROM prs_details pd, prs p
                                     WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} AND p.dept_head_appvl='approved' AND p.fin_head_appvl='approved'");
       }
        $config['base_url']= base_url().'ops/lp_instocks/get_all_instocks/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $instock->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
     
        $data['res'] = "";
        if($instock->num_rows()>0) {
            $data['res'] .= ' <div class="content-header menucenter">Incoming Stock List</div>
                              <div class="rooms-header1 skyblue-gradient white-shadow">
                                <div><label>Description</label></div>
                                <div><label>Quantity</label></div>
                                <div><label>Price</label></div>
                              </div>
                            <div class="rooms_list_container1">';
                 
            foreach($instock->result() as $row) {
             
            $data['res'] .='<div class="rooms-contents1">
                          <div>
                            <label class="sw-text2"> '.$row->description.'</label>
                          </div>
                          <div>
                            <label class="sw-text2"> '.$row->quantity.' '.$row->units.'</label>
                          </div>
                          <div>
                            <label class="sw-text2"> '.$row->unit_price.' </label>
                          </div>
                          </div>
                        ';
            }
           
        }
         
        echo json_encode($data);
    }
    
   
}
/* End of Lp incoming stocks  */