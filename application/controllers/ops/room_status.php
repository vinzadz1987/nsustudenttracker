<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Room_status extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) { 
            case "allsinglerooms": $this->get_all_single_rooms_list(); break;
            case "alldoublerooms": $this->get_all_double_rooms_list(); break;
            case "alltriplerooms": $this->get_all_triple_rooms_list(); break;
            case "allquadrooms": $this->get_all_quad_rooms_list(); break;
            case "allcorporaterooms": $this->get_all_corporate_rooms_list(); break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }

    public function get_all_single_rooms_list($offset=0) {
        
        if($this->input->post('like')!="") {
        $rooms = $this->db->query("SELECT r.room_id,r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 1 AND rni.room_no LIKE '".$this->input->post('like')."%'  ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=1 AND rni.room_no LIKE '".$this->input->post('like')."%'");
        }else if($this->input->post('room_status_name')) {
         $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 1 AND r.room_status_id='".$this->input->post('room_status_name')."' AND rni.room_no LIKE '".$this->input->post('like')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=1 AND r.room_status_id= '".$this->input->post('room_status_name')."'");
        } else {
        $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=1 ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=1");
        }
        $config['base_url']= base_url().'ops/room_status/get_all_single_rooms_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 20;
        $this->pagination->initialize($config);
        $data['num_rows']= $rooms->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($rooms->num_rows()>0) {
            $data['res'] .= '<div class="rooms-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Room Number</label></div>
                                <div><label>Room Type</label></div>
                                <div><label>Room Status</label></div>
                               
                                
                            </div>
                            <div class="rooms_list_container1">';
            foreach($rooms->result() as $row) {
                $data['res'] .='<div class="rooms-contents1 '.$row->rs_css.'">
                                <div>'.$row->room_number.'</div>
                                <div>'.$row->room_type.'</div>
                                <div>'.$row->room_status_name.'</div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_double_rooms_list($offset=0) {
        
        if($this->input->post('liked')!="") {
        $rooms = $this->db->query("SELECT r.room_id,r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 2 AND rni.room_no LIKE '".$this->input->post('liked')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=2 AND rni.room_no LIKE '".$this->input->post('liked')."%'");
        }else if($this->input->post('room_status_named')) {
         $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 2 AND r.room_status_id='".$this->input->post('room_status_named')."' AND rni.room_no LIKE '".$this->input->post('liked')."%' ORDER BY rs_css ASC  LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=2 AND r.room_status_id= '".$this->input->post('room_status_named')."'");
        } else {
        $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=2 ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=2");
        }
        $config['base_url']= base_url().'ops/room_status/get_all_double_rooms_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 20;
        $this->pagination->initialize($config);
        $data['num_rows']= $rooms->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($rooms->num_rows()>0) {
            $data['res'] .= '<div class="rooms-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Room Number</label></div>
                                <div><label>Room Type</label></div>
                                <div><label>Room Status</label></div>
                               
                                
                            </div>
                            <div class="rooms_list_container1">';
            foreach($rooms->result() as $row) {
                $data['res'] .='<div class="rooms-contents1 '.$row->rs_css.'">
                                <div>'.$row->room_number.'</div>
                                <div>'.$row->room_type.'</div>
                                <div>'.$row->room_status_name.'</div> 
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_triple_rooms_list($offset=0) {
        
        if($this->input->post('liked')!="") {
        $rooms = $this->db->query("SELECT r.room_id,r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 3 AND rni.room_no LIKE '".$this->input->post('liked')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=3 AND rni.room_no LIKE '".$this->input->post('liked')."%'");
        }else if($this->input->post('room_status_named')) {
         $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 3 AND r.room_status_id='".$this->input->post('room_status_named')."' AND rni.room_no LIKE '".$this->input->post('liked')."%' ORDER BY rs_css ASC  LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=3 AND r.room_status_id= '".$this->input->post('room_status_named')."'");
        } else {
        $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=3 ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=3");
        }
        $config['base_url']= base_url().'ops/room_status/get_all_double_rooms_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 20;
        $this->pagination->initialize($config);
        $data['num_rows']= $rooms->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($rooms->num_rows()>0) {
            $data['res'] .= '<div class="rooms-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Room Number</label></div>
                                <div><label>Room Type</label></div>
                                <div><label>Room Status</label></div>
                               
                                
                            </div>
                            <div class="rooms_list_container1">';
            foreach($rooms->result() as $row) {
                $data['res'] .='<div class="rooms-contents1 '.$row->rs_css.'">
                                <div>'.$row->room_number.'</div>
                                <div>'.$row->room_type.'</div>
                                <div>'.$row->room_status_name.'</div> 
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_quad_rooms_list($offset=0) {
        
        if($this->input->post('like')!="") {
        $rooms = $this->db->query("SELECT r.room_id,r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 4 AND rni.room_no LIKE '".$this->input->post('like')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=4 AND rni.room_no LIKE '".$this->input->post('like')."%'");
        }else if($this->input->post('room_status_name')) {
         $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 4 AND r.room_status_id='".$this->input->post('room_status_name')."' AND rni.room_no LIKE '".$this->input->post('like')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=4 AND r.room_status_id= '".$this->input->post('room_status_name')."'");
        } else {
        $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=4 ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=4");
        }
        $config['base_url']= base_url().'ops/room_status/get_all_quad_rooms_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 20;
        $this->pagination->initialize($config);
        $data['num_rows']= $rooms->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($rooms->num_rows()>0) {
            $data['res'] .= '<div class="rooms-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Room Number</label></div>
                                <div><label>Room Type</label></div>
                                <div><label>Room Status</label></div>
                               
                                
                            </div>
                            <div class="rooms_list_container1">';
            foreach($rooms->result() as $row) {
                $data['res'] .='<div class="rooms-contents1 '.$row->rs_css.'">
                                <div>'.$row->room_number.'</div>
                                <div>'.$row->room_type.'</div>
                                <div>'.$row->room_status_name.'</div> 
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_corporate_rooms_list($offset=0) {
        
        if($this->input->post('like')!="") {
        $rooms = $this->db->query("SELECT r.room_id,r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 5 AND rni.room_no LIKE '".$this->input->post('like')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=5 AND rni.room_no LIKE '".$this->input->post('like')."%'");
        }else if($this->input->post('room_status_name')) {
         $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css 
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                   AND r.room_type_id = 5 AND r.room_status_id='".$this->input->post('room_status_name')."' AND rni.room_no LIKE '".$this->input->post('like')."%' ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id
                                    AND r.room_type_id=5 AND r.room_status_id= '".$this->input->post('room_status_name')."'");
        } else {
        $rooms = $this->db->query("SELECT r.room_id, r.room_number_id, rni.room_no AS room_number, rt.room_type_name AS room_type, rs.room_status_name AS room_status_name, rs.room_status_css AS rs_css
                                   FROM rooms r, room_number_ids rni, room_type rt, room_status rs
                                   WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=5 ORDER BY rs_css ASC LIMIT 20 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(r.room_id) AS rows FROM rooms r, room_number_ids rni, room_type rt, room_status rs 
                                    WHERE r.room_number_id=rni.room_number_id AND r.room_type_id=rt.room_type_id AND r.room_status_id=rs.room_status_id AND r.room_type_id=5");
        }
        $config['base_url']= base_url().'ops/room_status/get_all_corporate_rooms_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 20;
        $this->pagination->initialize($config);
        $data['num_rows']= $rooms->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($rooms->num_rows()>0) {
            $data['res'] .= '<div class="rooms-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Room Number</label></div>
                                <div><label>Room Type</label></div>
                                <div><label>Room Status</label></div>
                               
                                
                            </div>
                            <div class="rooms_list_container1">';
            foreach($rooms->result() as $row) {
                $data['res'] .='<div class="rooms-contents1 '.$row->rs_css.'">
                                <div>'.$row->room_number.'</div>
                                <div>'.$row->room_type.'</div>
                                <div>'.$row->room_status_name.'</div>  
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    
     public function get_all_swt_list($offset=0) {
        
        
        $mon = $this->db->query("SELECT oswt.id AS oswt_id, oswt.day, oswt.shortage_wastage_id, oswt.b_shortage,
                                        oswt.b_wastage, oswt.b_comment, oswt.l_shortage,
                                        sw.number_of_students AS nos, sw.from, sw.to,
                                        oswt.l_wastage, oswt.l_comment, oswt.b_wastage,
                                        oswt.d_wastage, oswt.d_shortage,
                                        sw.id, od.name AS reports_day 
                                 FROM  ops_shortage_wastage_tracker oswt, ops_day od, ops_fb_shortage_wastage sw 
                                 WHERE oswt.day=od.id AND oswt.shortage_wastage_id=sw.id AND oswt.shortage_wastage_id='".$this->input->post('bid')."'");

        $data['res'] = "";
        if($mon->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">Reports Details</div>
                            <div class="sw-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Number of Students:</label> <label class="sw-text">'.$mon->row()->nos.'</label></div>
                                <div><label>From:</label> <label class="sw-text">'.$mon->row()->from.'</label></div>
                                <div><label>To:</label> <label class="sw-text">'.$mon->row()->to.'</label></div>
                                
                                
                            </div>
                            <div class="sw-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Day</label></div>
                                <div><label>Breakfast</label></div>
                                <div><label>Lunch</label></div>
                                <div><label>Dinner</label></div>
                                
                            </div>
                            <div class="sw_list_container1">';
            foreach($mon->result() as $row) {
                $data['res'] .='<div class="sw-contents1">
                                <div>'.$row->reports_day.'</div>
                                <div>'.$row->b_shortage.' '.$row->b_wastage.'% '.$row->b_comment.'</div>
                                <div>'.$row->l_shortage.' '.$row->l_wastage.'% '.$row->l_comment.'</div>
                                <div>'.$row->d_shortage.' '.$row->d_wastage.'% <button class="blue-button delete-daily-report" id="st_'.$row->oswt_id.'">x</button></div>        
                                </div>';
            }
            $data['res'] .= '</div></div>
                            <div class="white-litegray hint">1st column: shortage of food | 2nd column: wastage of food (based on percentage) | 3rd column: quality of food (survey – rate from 1 to 5, 5 is the highest)</div>';
        }
        echo json_encode($data);
    }
    private function _init(){
        $room_status = $this->db->query("SELECT * FROM room_status");
        $data['room_status'] = ($room_status->num_rows()>0) ? $room_status->result():'No Status';  
        echo json_encode($data);
    }  
  
}
/* End of Room Status */