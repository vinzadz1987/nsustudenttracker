<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Forms extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
	
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
	
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	/*
	 * Never an honest word, and that was when I ruled the world
	 * by: Coldplay - Viva la Vida
	 */
	public function retrieve( $item = null, $offset = 0 ) {
		switch($item) {
			case "leave-list":
				//leave_type, emp_id, imm_head_appvl, date_requested
				
				$offset = intval($offset);
				$per_page = 15;
				
				$total_rows = $this->db->query("
					SELECT * FROM emp_leaves WHERE imm_head_appvl = 'approved'
				")->num_rows();
				
				$config = array(
					'total_rows'	=> $total_rows,
					'base_url'		=> base_url()."hr/forms/retrieve/leave-list",
					'uri_segment'	=> 5,
					'per_page'		=> $per_page
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "	
					SELECT el.leave_id AS lid, FROM_UNIXTIME(el.date_requested, '%m/%d/%Y') AS date_requested, el.leave_type,
					el.reason, el.time_requested, el.pay_type, FROM_UNIXTIME(el.leave_start, '%m/%d/%Y') AS leave_start,
					FROM_UNIXTIME(el.leave_end, '%m/%d/%Y') AS leave_end, FROM_UNIXTIME(el.return_date, '%m/%d/%Y') AS return_date,
					el.	imm_head_appvl, el.hr_appvl, CONCAT_ws(' ', epd.firstname, epd.lastname) AS requested_by
					FROM emp_leaves AS el
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = el.emp_id
					WHERE el.imm_head_appvl = 'approved' 
					ORDER BY el.date_requested DESC
					LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output(
					json_encode(
						array('result' => $this->db->query($sql)->result_array(), 'pagination' => $pagination)
					)
				);
			break;
			
			case "srs-list":
				$offset = intval($offset);
				$per_page = 15;
				$total_rows = $this->db->query("
					SELECT * FROM srs WHERE head_appr = 'approved'
				")->num_rows();
				
				$config = array(
					'total_rows'	=> $total_rows,
					'base_url'		=> base_url()."hr/forms/retrieve/srs-list",
					'uri_segment'	=> 5,
					'per_page'		=> $per_page
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT s.id as sid, s.qty, s.head_appr, s.srs_released, s.received, s.purpose, 
					FROM_UNIXTIME(s.date_required, '%m/%d/%Y') AS date_required,
					FROM_UNIXTIME(s.date_requested, '%m/%d/%Y') AS date_requested,
					stk.name AS stock_name, stk.description AS stock_description, stk.brand AS stock_brand,
					stk.qty_inhand AS stock_quantity_inhand, CONCAT_ws(' ', epd.firstname, epd.lastname) AS requested_by,
					stktyp.name AS stock_type
					FROM srs AS s
					LEFT JOIN stocks AS stk ON stk.stock_id = s.stock_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = s.requested_by
					LEFT JOIN stock_type AS stktyp ON stktyp.id = stk.stock_type
					LEFT JOIN unitsof_measure AS uom ON uom.id = stk.units
					WHERE s.head_appr = 'approved' 
					LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output(
					json_encode(
						array('result' => $this->db->query($sql)->result_array(), 'pagination' => $pagination)
					)
				);
			break;
			
			case "prs-list":
				$offset = intval($offset);
				$per_page = 15;
				$total_rows = $this->db->query("
					SELECT * FROM prs AS p WHERE p.dept_head_appvl = 'approved'
				")->num_rows();
				
				$config = array(
					'total_rows'	=> $total_rows,
					'base_url'		=> base_url()."hr/forms/retrieve/prs-list",
					'uri_segment'	=> 5,
					'per_page'		=> $per_page
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT p.prs_id AS pid, CONCAT_ws(' ', epd.firstname, epd.lastname) AS requested_by,
					FROM_UNIXTIME(p.date_required, '%m/%d/%Y') AS date_required,
					FROM_UNIXTIME(p.date_requested, '%m/%d/%Y') AS date_requested,
					p.supplier_name, p.type, p.payment_type, p.dept_head_appvl, p.fin_head_appvl,
					FROM_UNIXTIME(p.date_received, '%m/%d/%Y') AS date_received,
					pd.description, pd.quantity, pd.units, pd.unit_price, pd.amount
					FROM prs AS p
					LEFT JOIN prs_details AS pd ON pd.prs_id = p.prs_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = p.requested_by
					WHERE p.dept_head_appvl = 'approved'
					LIMIT {$offset}, {$per_page}
				";
				$this->output->set_output(
					json_encode(
						array('result' => $this->db->query($sql)->result_array(), 'pagination' => $pagination)
					)
				);
			break;
		}
	}
	
	public function set_status( $item = null, $id = 0 ) {
		$id = intval($id);
		$data_items = json_decode( $this->input->post('model') );
		switch( $item ) {
			case "leave":
				$this->db->update('emp_leaves', array('hr_appvl' => $data_items->status), array('leave_id' => $data_items->id));
			break;
			case "prs":
				$this->db->update('prs', array('fin_head_appvl' => $data_items->status), array('prs_id' => $data_items->id));
			break;
		}		
	}
	
	public function info( $item = null, $id = 0 ) {
		$id = intval($id);
		switch( $item ) {
			case "leave":
				/*
				$sql = "
					SELECT FROM_UNIXTIME(el.date_requested, '%m/%d/%Y') AS date_requested, el.leave_type,
					el.reason, el.time_requested, el.pay_type, FROM_UNIXTIME(el.leave_start, '%m/%d/%Y') AS leave_start,
					FROM_UNIXTIME(el.leave_end, '%m/%d/%Y') AS leave_end, FROM_UNIXTIME(el.return_date, '%m/%d/%Y') AS return_date,
					CONCAT_ws(' ', epd.firstname, epd.lastname) AS requested_by
					FROM emp_leaves AS el 
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = el.emp_id
					WHERE el.leave_id = {$id}
				";*/
			break;
		}
	}
	/*
	 *
	*/
	public function aml( $items = null ) {
		switch($items) {
			case "leave-list":
				print "sdf";
				break;
		}
	}
}