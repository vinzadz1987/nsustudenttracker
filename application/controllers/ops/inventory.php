<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Inventory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_supplier_list": $this->_supplier_list(); break;
            case "add_item_entry": $this->_add_item_entry(); break;
            case "retrieve_item_list": $this->_item_list(); break;
            case "delete_supplier_entry": $this->_delete_supplier();  break;
            case "delete_item_entry": $this->_delete_item(); break;
            case "retrieve_details": $this->_item_details(); break;  
            case "stocktype": $this->_stock_type_list();break;
            case "uom": $this->_uom_list();break;
            case "retrieve_uom": $this->_uom_details();break;
            case "uomupdate": $this->_uom_update();break;
            case "adduom": $this->_add_uom();break;
            case "deleteuom": $this->_delete_uom();break;
            case "stocktypeupdate": $this->_stocktype_update();break;
            case "add_stocktype": $this->_add_stock_type();break;
            case "delete_stocktype": $this->_delete_stocktype();break;
            case "retrieve_stocks": $this->_stock_details();break;
            case "initializer": $this->_init(); break;
            case "pending_prs_list": $this->_prs_pending_list(); break;
            case "approved_prs_list": $this->_prs_approved_list(); break;
            case "cancelled_prs_list": $this->_prs_cancelled_list(); break;
            case "rejected_prs_list": $this->_prs_rejected_list(); break;
            case "count_prs": $this->_count_prs_list(); break;
            case "retrieve_approved_prs": $this->_approved_prs_details(); break;
            case "retrieve_pending_prs": $this->_pending_prs_details(); break;
            case "retrieve_cancelled_prs": $this->_cancelled_prs_details();break;
            case "retrieve_rejected_prs": $this->_rejected_prs_details();break;
            default: show_404();
        }
    }
    private function _count_prs_list() {
        $pending = $this->db->query("SELECT COUNT(prs_id) AS pending
                                 FROM prs 
                                 WHERE 
                                 fin_head_appvl = 'pending'");  
         $approved = $this->db->query("SELECT COUNT(prs_id) AS approved
                                 FROM prs 
                                 WHERE 
                                 fin_head_appvl = 'approved'"); 
         $cancelled = $this->db->query("SELECT COUNT(prs_id) AS cancelled
                                 FROM prs 
                                 WHERE 
                                 fin_head_appvl = 'cancelled'"); 
         $rejected = $this->db->query("SELECT COUNT(prs_id) AS rejected
                                 FROM prs 
                                 WHERE 
                                 fin_head_appvl = 'rejected'"); 
        $data['res'] = "";
        if($pending->result()>0) {
            foreach($pending->result() as $row) {
                $data['res'] .='
                              <div class="countbox">  
                                Pending PRS : <i>'.$row->pending.'</i>          
                              </div> 
                            ';
            }
           
        }
        if($approved->result()>0) {
            foreach($approved->result() as $row) {
                $data['res'] .='
                              <div class="countbox">  
                                Approved PRS : <i>'.$row->approved.'</i>
                              </div> 
                            ';
            }
           
        }
        if($cancelled->result()>0) {
            foreach($cancelled->result() as $row) {
                $data['res'] .='
                              <div class="countbox">  
                                Cancelled PRS : <i>'.$row->cancelled.'</i>
                              </div> 
                            ';
            }
           
        }
        if($rejected->result()>0) {
            foreach($rejected->result() as $row) {
                $data['res'] .='
                              <div class="countbox">  
                                Rejected PRS : <i>'.$row->rejected.'</i>
                              </div> 
                            ';
            }
           
        }
        echo json_encode($data);
    }
    private function _supplier_list() {
        $sql = $this->db->query("SELECT * FROM suppliers ORDER BY name");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="item-header skyblue-gradient white-shadow">
                                <div><label>Name</label></div>
                                <div><label>Address</label></div>
                                <div><label>Phone#</label></div>
                                <div><label>Mobile#</label></div>
                                <div><label>Fax#</label></div>
                                <div><label>Note</label></div>
                            </div>
                            <div class="item_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="item-contents">
                                <div>'.$row->name.'</div>
                                <div>'.$row->address.'</div>
                                <div>'.$row->phone_no.'</div>
                                <div>'.$row->mobile_no.'</div>
                                <div>'.$row->faxnum.'</div>
                                <div>'.$row->note.'</div>
                                <div class="options hide to-right sprite-close delete_supplier_btn" id="phic_'.$row->supplier_id.'">
                                </div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _stock_type_list() {
        $sql = $this->db->query("SELECT * FROM stock_type ORDER BY id");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="item-header skyblue-gradient white-shadow">  
                                <div><label>Name</label></div>
                            </div>
                            <div class="item_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="item-contents">
                                <div>'.$row->name.'</div> 
                                <div class="options hide sprite-container to-right sprite-close delete_stock_btn" id="st_'.$row->id.'">
                                </div>             
                                <div class=" options hide sprite-container to-right sprite-edit-item edit_stock_btn" id="st_'.$row->id.'">     
                               </div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _prs_approved_list() {
        $sql = $this->db->query("SELECT p.prs_id,
                                 pd.description AS dt_descript,
                                 pd.quantity AS quant,
                                 pd.units AS unit_name,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, 
                                 p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, emp_personal_details epd, 
                                 department dep, department_details dt, prs_details pd 
                                 WHERE
                                 p.prs_id = pd.prs_id AND
                                 p.requested_by = epd.emp_id 
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'approved' 
                                 ORDER BY p.prs_id");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="prs-header skyblue-gradient white-shadow">  
                                
                                <div><label>Requested by</label></div>
                                <div><label>Department</label></div>
                                <div><label>Supplier</label></div>
                                <div><label>Date Filled</label></div>
                                <div><label>Date Required</label></div>
                                <div><label>PRS Type</label></div>
                                <div><label>Payment Type</label></div>
                                <div><label>Department Head Approval</label></div>
                                <div><label>Finance Head Approval</label></div>
                            </div>
                            <div class="prs_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="prs-contents" id="st_'.$row->prs_id.'">
                                <div>'.$row->fname.' '.$row->lname.'</div> 
                                <div>'.$row->department_name.'</div> 
                                <div>'.$row->supplier_name.'</div>
                                <div>'.date('M d, Y',$row->date_requested).'</div> 
                                <div>'.date('M d, Y',$row->date_required).'</div> 
                                <div>'.$row->type.'</div> 
                                <div>'.$row->payment_type.'</div>
                                <div>'.$row->dept_head_appvl.'</div>
                                <div>'.$row->fin_head_appvl.'</div> 
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _approved_prs_details(){
        $apr_prs = $this->db->query("SELECT 
                                 p.prs_id, pd.description AS descript,
                                 pd.quantity AS quant,
                                 pd.units AS unt,
                                 pd.unit_price AS untp,
                                 pd.amount AS amt,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, prs_details pd, emp_personal_details epd, 
                                 department dep, department_details dt 
                                 WHERE p.requested_by = epd.emp_id
                                 AND p.prs_id = pd.prs_id
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'approved' 
                                 AND p.prs_id ='".$this->input->post('bid')."'");
        $data['apr_prs'] = ($apr_prs) ? $apr_prs->row(): "not found";
        echo json_encode($data);
    }
    private function _prs_pending_list() {
        $sql = $this->db->query("SELECT p.prs_id,
                                 pd.description AS dt_descript,
                                 pd.quantity AS quant,
                                 pd.units AS unit_name,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, 
                                 p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, emp_personal_details epd, 
                                 department dep, department_details dt, prs_details pd 
                                 WHERE
                                 p.prs_id = pd.prs_id AND
                                 p.requested_by = epd.emp_id 
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'pending' 
                                 ORDER BY p.prs_id");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="prs-header skyblue-gradient white-shadow"> 
                                <div><label>Requested by</label></div>
                                <div><label>Department</label></div>
                                <div><label>Supplier</label></div>
                                <div><label>Date Filled</label></div>
                                <div><label>Date Required</label></div>
                                <div><label>PRS Type</label></div>
                                <div><label>Payment Type</label></div>
                                <div><label>Department Head Approval</label></div>
                                <div><label>Finance Head Approval</label></div>
                            </div>
                            <div class="prs_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="prs-contents" id="st_'.$row->prs_id.'">
                                <div>'.$row->fname.' '.$row->lname.'</div>
                                <div>'.$row->department_name.'</div>
                                <div>'.$row->supplier_name.'</div>
                                <div>'.date('M d, Y',$row->date_requested).'</div> 
                                <div>'.date('M d, Y',$row->date_required).'</div> 
                                <div>'.$row->type.'</div>  
                                <div>'.$row->payment_type.'</div>
                                <div>'.$row->dept_head_appvl.'</div>
                                <div>'.$row->fin_head_appvl.'</div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
     private function _pending_prs_details(){
        $apr_prs = $this->db->query("SELECT 
                                 p.prs_id, pd.description AS descript,
                                 pd.quantity AS quant,
                                 pd.units AS unt,
                                 pd.unit_price AS untp,
                                 pd.amount AS amt,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, prs_details pd, emp_personal_details epd, 
                                 department dep, department_details dt 
                                 WHERE p.requested_by = epd.emp_id
                                 AND p.prs_id = pd.prs_id
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'pending' 
                                 AND p.prs_id ='".$this->input->post('bid')."'");
        $data['apr_prs'] = ($apr_prs) ? $apr_prs->row(): "not found";
        echo json_encode($data);
    }
    private function _prs_cancelled_list() {
        $sql = $this->db->query("SELECT p.prs_id,
                                 pd.description AS dt_descript,
                                 pd.quantity AS quant,
                                 pd.units AS unit_name,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, 
                                 p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, emp_personal_details epd, 
                                 department dep, department_details dt, prs_details pd 
                                 WHERE
                                 p.prs_id = pd.prs_id AND
                                 p.requested_by = epd.emp_id 
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'cancelled' 
                                 ORDER BY p.prs_id");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="prs-header skyblue-gradient white-shadow">  
                                <div><label>Requested by</label></div>
                                <div><label>Department</label></div>
                                <div><label>Supplier</label></div>
                                <div><label>Date Filled</label></div>
                                <div><label>Date Required</label></div>
                                <div><label>PRS Type</label></div>
                                <div><label>Payment Type</label></div>
                                <div><label>Department Head Approval</label></div>
                                <div><label>Finance Head Approval</label></div>
                            </div>
                            <div class="prs_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="prs-contents" id="st_'.$row->prs_id.'">
                                <div>'.$row->fname.' '.$row->lname.'</div> 
                                <div>'.$row->department_name.'</div>
                                <div>'.$row->supplier_name.'</div> 
                                <div>'.date('M d, Y',$row->date_requested).'</div> 
                                <div>'.date('M d, Y',$row->date_required).'</div> 
                                <div>'.$row->type.'</div> 
                                <div>'.$row->payment_type.'</div>
                                <div>'.$row->dept_head_appvl.'</div>
                                <div>'.$row->fin_head_appvl.'</div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _cancelled_prs_details(){
        $apr_prs = $this->db->query("SELECT 
                                 p.prs_id, pd.description AS descript,
                                 pd.quantity AS quant,
                                 pd.units AS unt,
                                 pd.unit_price AS untp,
                                 pd.amount AS amt,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, prs_details pd, emp_personal_details epd, 
                                 department dep, department_details dt 
                                 WHERE p.requested_by = epd.emp_id
                                 AND p.prs_id = pd.prs_id
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'cancelled' 
                                 AND p.prs_id ='".$this->input->post('bid')."'");
        $data['apr_prs'] = ($apr_prs) ? $apr_prs->row(): "not found";
        echo json_encode($data);
    }
    private function _prs_rejected_list() {
        $sql = $this->db->query("SELECT p.prs_id,
                                 pd.description AS dt_descript,
                                 pd.quantity AS quant,
                                 pd.units AS unit_name,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, 
                                 p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, emp_personal_details epd, 
                                 department dep, department_details dt, prs_details pd 
                                 WHERE
                                 p.prs_id = pd.prs_id AND
                                 p.requested_by = epd.emp_id 
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'rejected' 
                                 ORDER BY p.prs_id");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="prs-header skyblue-gradient white-shadow">  
                                
                                <div><label>Requested by</label></div>
                                <div><label>Department</label></div>
                                <div><label>Supplier</label></div>
                                <div><label>Date Filled</label></div>
                                <div><label>Date Required</label></div>
                                <div><label>PRS Type</label></div>
                                <div><label>Payment Type</label></div>
                                <div><label>Department Head Approval</label></div>
                                <div><label>Finance Head Approval</label></div>
                            </div>
                            <div class="prs_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="prs-contents" id="st_'.$row->prs_id.'">
                                
                                <div>'.$row->fname.' '.$row->lname.'</div> 
                                <div>'.$row->department_name.'</div> 
                                <div>'.$row->supplier_name.'</div>
                                <div>'.date('M d, Y',$row->date_requested).'</div> 
                                <div>'.date('M d, Y',$row->date_required).'</div> 
                                <div>'.$row->type.'</div> 
                                <div>'.$row->payment_type.'</div>
                                <div>'.$row->dept_head_appvl.'</div>
                                <div>'.$row->fin_head_appvl.'</div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _rejected_prs_details(){
        $apr_prs = $this->db->query("SELECT 
                                 p.prs_id, pd.description AS descript,
                                 pd.quantity AS quant,
                                 pd.units AS unt,
                                 pd.unit_price AS untp,
                                 pd.amount AS amt,
                                 epd.firstname AS fname, 
                                 epd.lastname AS lname, 
                                 dep.deptd_id AS department,
                                 dt.dept_name AS department_name,
                                 p.date_requested, p.date_required, 
                                 p.supplier_name, p.type, 
                                 p.payment_type, p.dept_head_appvl, 
                                 p.fin_head_appvl  
                                 FROM prs p, prs_details pd, emp_personal_details epd, 
                                 department dep, department_details dt 
                                 WHERE p.requested_by = epd.emp_id
                                 AND p.prs_id = pd.prs_id
                                 AND p.requested_by = dep.emp_id 
                                 AND dep.deptd_id = dt.deptd_id
                                 AND p.fin_head_appvl = 'rejected' 
                                 AND p.prs_id ='".$this->input->post('bid')."'");
        $data['apr_prs'] = ($apr_prs) ? $apr_prs->row(): "not found";
        echo json_encode($data);
    }
    private function _uom_list($offset=0) {
        
        $sql = $this->db->query("SELECT * FROM unitsof_measure WHERE id ORDER BY name");
  
        $data['res'] = "";
        if($sql->num_rows()>0) {
            
            $data['res'] .= '<div class="item-header skyblue-gradient white-shadow">  
                                <div><label>Name</label></div>
                            </div>
                            <div class="item_list_container">';
            foreach($sql->result() as $row) {
               
                $data['res'] .='<div class="item-contents">   
                                <div>'.$row->name.'</div> 
                               <div class="options hide sprite-container to-right sprite-close delete_uom_btn" id="st_'.$row->id.'">
                               </div>           
                                <div class="options hide sprite-container to-right sprite-edit-item edit_uom_btn" id="st_'.$row->id.'">     
                               </div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _add_stock_type() {
        $r = $this->input->post('data');
        $v = array(
            "name" => $r['name']
        );
        $data['res'] = ($this->db->insert("stock_type",$v)) ? true:false;
        echo json_encode($data);
    }
    private function _add_uom() {
        $r = $this->input->post('data');
        $v = array(
            "name" => $r['name']
        );
        $data['res'] = ($this->db->insert("unitsof_measure",$v)) ? true:false;
        echo json_encode($data);
    }
    private function _stocktype_update() {
        $received = $this->input->post('data');
        $values = array(
            "id" => $received['id'],
            "name" => $received['name']
        );
        $data['result'] = ($this->db->update('stock_type',$values,"id = ".$received['id'])) ? true:false;
        echo json_encode($data);
    }
    private function _uom_update() {
        $received = $this->input->post('data');
        $values = array(
            "id" => $received['id'],
            "name" => $received['name']
        );
        $data['result'] = ($this->db->update('unitsof_measure',$values,"id = ".$received['id'])) ? true:false;
        echo json_encode($data);
    }
    private function _stock_details() {
        $stock = $this->db->query("SELECT * FROM stock_type WHERE id='".$this->input->post('bid')."'");
        $data['stock'] = ($stock) ? $stock->row(): "not found";
        echo json_encode($data);
    }
    private function _uom_details() {
        $uom = $this->db->query("SELECT * FROM unitsof_measure WHERE id='".$this->input->post('bid')."'");
        $data['uom'] = ($uom) ? $uom->row(): "not found";
        echo json_encode($data);
    }
    private function _delete_supplier() {
        $sql = $this->db->query("DELETE FROM suppliers WHERE supplier_id = '".$this->input->post('pid')."'");
        $this->_supplier_list();
    }
     private function _delete_stocktype() {
        $sql = $this->db->query("DELETE FROM stock_type WHERE id = '".$this->input->post('pid')."'");
        $this->_stock_type_list();
    }
     private function _delete_uom() {
        $sql = $this->db->query("DELETE FROM unitsof_measure WHERE id = '".$this->input->post('pid')."'");
        $this->_uom_list();
    }
    private function _add_item_entry() {
        $rcv = $this->input->post('data');
        $values = array(
            "stock_type" => $rcv['stock_type'],
            "name" => $rcv['name'],
            "brand" => $rcv['brand'],
            "unitof_measure" => $rcv['unitof_measure'],
            "qty_inhand" => $rcv['qty_inhand'],
            "reorder_point" => $rcv['reorder_point'],
            "acqui_price" => $rcv['acqui_price'],
            "suppliers" => $rcv['suppliers'],
            "added_by" => $rcv('added_by'),
            "stock_image" => $rcv('stock_image'),
            "date_added" => time(),
            "description" => $rcv['description']
        );
        $data['res'] = ($this->db->insert("stocks",$values)) ? true:false;
        echo json_encode($data);
    } 
    private function _item_details() {
        $item = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE (st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_id='".$this->input->post('bid')."')");   
        $data['item'] = ($item) ? $item->row(): "Not found";
        echo json_encode($data);
    }
   
   
    private function _init() { 
        $stock_type = $this->db->query("SELECT * FROM stock_type");
        $data['stock_type'] = ($stock_type->num_rows()>0) ? $stock_type->result():'No stock type';
        $unitof_measure = $this->db->query("SELECT * FROM unitsof_measure");
        $data['unitof_measure'] = ($unitof_measure->num_rows()>0) ? $unitof_measure->result():'No stock type';
        $suppliers = $this->db->query("SELECT * FROM suppliers");
        $data['suppliers'] = ($suppliers->num_rows()>0) ? $suppliers->result():'No stock type';  
        echo json_encode($data);  
    }
   
    private function _item_list() {
        $sql = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE (st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id) ORDER BY st.name ");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="item-header skyblue-gradient white-shadow">
                                <div><label>Stock Type</label></div>
                                <div><label>Name</label></div>
                                <div><label>Brand</label></div>
                                <div><label>Quantity</label></div>
                                <div><label>Unit</label></div>
                                <div><label>ReOrder Pt.</label></div>
                                <div><label>Acq. Price</label></div>
                                <div><label>Supplier</label></div>
                                <div><label>Description</label></div>
                            </div>
                                
                            <div class="item _list_container">'; 
        foreach($sql->result() as $row) {   
                $data['res'] .= '<div class="item-contents">
                                 <div>'.$row->type_name.'</div>
                                 <div>'.$row->name.'</div>
                                 <div>'.$row->brand.'</div>
                                 <div>'.$row->qty_inhand.'</div>
                                 <div>'.$row->unit_name.'</div>
                                 <div>'.$row->reorder_point.'</div>
                                 <div>'.$row->aqui_price.'</div>
                                 <div>'.$row->supplier_name.'</div>
                                 <div>'.$row->description.'</div>    
                                <div class="options hide to-right sprite-edit-item edit_item_btn" id="item_'.$row->stock_id.'" title="Update Stock">
                         
                                </div>
                                
                                
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _delete_item() {
        $sql = $this->db->query("DELETE FROM stocks WHERE stock_id = '".$this->input->post('sid')."'");
        $this->_item_list();
    }
}
/*End of contributions_list.php */