<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Lp_price_list extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_item_cat": $this->get_all_item_cat();break;
            case "all_item": $this->get_all_item();break;
            case "add_lp_item": $this->_add_lp_item();break;
            case "get_item_details": $this->_item_details();break;
            case "update_items": $this->_update_items();break;
            case "delete_items": $this->_delete_items();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    
    public function get_all_item_cat() {
        
   
        $item = $this->db->query("SELECT lpc.* FROM lp_item_cat lpc");
     
        $data['res'] = "";
        if($item->num_rows()>0) {
            $data['res'] .= ' <div class="content-header menucenter">Select Category</div>';
                 
            foreach($item->result() as $row) {
             
            $data['res'] .='<div class="ci skyblue-gradient white-shadow menucenter cat-item" id="st_'.$row->cat_id.'" title="Category">'.$row->name.'                             
                            </div>';
            }
           
        }
         
        echo json_encode($data);
    }
    
   
    
    public function get_all_item() {
        
         $catitem = $this->db->query("SELECT lpl.id, lpl.description, lpl.cat_id,lpl.price, lpc.name AS catname 
                                      FROM lp_price_list lpl, lp_item_cat lpc
                                      WHERE lpl.cat_id=lpc.cat_id AND lpl.cat_id='".$this->input->post('bid')."'");
         
        $data['res'] = "";
        if($catitem->num_rows()<=0){
            
            $data['res'] .= '<div class="white-litegray menucenter">No Item Yet.</div>';
            
        }else{
            
        
        if($catitem->num_rows()>0) {
                $data['res'] .= ' <div class="white-litegray menucenter">Items in '.$catitem->row()->catname.' Category</div>
                              ';
            foreach($catitem->result() as $row) {
             
            $data['res'] .='
                            
                    
                                <div class="white-shadow">
                                <label>'.$row->description.',</label>
                                <label class="input-fieldrm"> Php '.$row->price.'</label>
                                <a class="item-delete to-right" id="st_'.$row->description.'" title="Delete '.$row->description.'"> [x]</a>
                                <a class="item-update updateitem ui to-right" title="Update Item" id="st_'.$row->id.'">[/]</a>
                                </div>
                                
                           ';
            }
            $data['res'] .= '';
        }
       }  
        echo json_encode($data);
    }
    
    
     private function _add_lp_item() {
        $received = $this->input->post('data');
        $values = array(
            
            'description'=>$received['desc'],
            'price'=>$received['price'],
            'cat_id'=>$received['cat']
              
        );
        $sql = $this->db->query("SELECT * FROM lp_price_list WHERE cat_id='".$received['cat']."' AND description='".$received['desc']."'");
       
        if($sql->result()==true) {
          echo "failed"; 
        }else {
           if($this->db->insert('lp_price_list',$values) && $this->db->insert('lp_pys_inv',$values)) {
            echo "added";
        } 
        }
        
    } 
    
   
     private function _update_items() {
        $received = $this->input->post('data');
        $values = array(
            "id"=>$received['item_id'],
            "description"=>$received['item_desc'],
            "price"=>$received['item_price'],
            "cat_id"=>$received['item_cat']
        );
        $data['result'] = ($this->db->update('lp_price_list',$values,"id = ".$received['item_id'])) ? true:false;
        $data['result'] = ($this->db->update('lp_pys_inv',$values,"description = ".$received['item_desc'])) ? true:false;
        echo json_encode($data);
    } 
   
    private function _item_details() {
           $item = $this->db->query("SELECT * FROM lp_price_list WHERE id ='".$this->input->post('bid')."'");
           $data['item'] = ($item) ? $item->row(): "Not found";
        echo json_encode($data);
    } 
    
    private function _init(){
        $category = $this->db->query("SELECT * FROM lp_item_cat");
        $data['category'] = ($category->num_rows()>0) ? $category->result():'No category'; 
        echo json_encode($data);
    }   
   
   
     private function _delete_items() {
        $sql = $this->db->query("DELETE FROM lp_price_list WHERE description = '".$this->input->post('pid')."'");
        $sql = $this->db->query("DELETE FROM lp_pys_inv WHERE description = '".$this->input->post('pid')."'");
        $this->get_all_item();
    }
   
}
/* End of Larry Place price list  */