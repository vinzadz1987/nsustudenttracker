<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Checklist_form extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "allchecklistattendant": $this->get_all_checklist();break;
            case "addchecklistattendant": $this->_add_checklist();break;
            case "allattendant": $this->get_all_attendant();break;
            case "addattendant": $this->_add_attendant();break;
            case "getreport": $this->get_all_dc_list();break;
            case "delete_atnroomchecklist": $this->_delete_attnroom_checklist();break;
            case "delete_atnchecklist": $this->_delete_attn_checklist();break;
            case "delete_attendant": $this->_delete_attn();break;
            case "addchecklist": $this->_get_addchlist();break;
            case "updatechecklist": $this->_update_chlist();break;
            case "addchecklistreports": $this->_add_checklistreports();break;
            case "updatechecklistcontents": $this->_update_checklistreports();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    public function get_all_checklist($offset=0) {
        
       if($this->input->post('like')!="") {
        $mon = $this->db->query("SELECT ora.id, ora.ra_date, oa.name AS am_hr, op.name AS pm_hr, hka.fname AS prepared_name, hka.lname AS prepared_lastname, ora.prepared_by 
                                 FROM ops_rooms_attendant ora, ops_am oa, ops_pm op, hk_attendant hka
                                 WHERE ora.am=oa.am_id AND ora.pm=op.pm_id AND ora.prepared_by=hka.hk_id AND hka.fname LIKE '".$this->input->post('like')."%'  LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ora.id) AS rows FROM ops_rooms_attendant ora, ops_am oa, ops_pm op, hk_attendant hka 
                                 WHERE ora.am=oa.am_id AND ora.pm=op.pm_id AND ora.prepared_by=hka.hk_id AND hka.fname LIKE '".$this->input->post('like')."%'");
       }else {
        $mon = $this->db->query("SELECT ora.id, ora.ra_date, oa.name AS am_hr, op.name AS pm_hr, hka.fname AS prepared_name, hka.lname AS prepared_lastname, ora.prepared_by
                                 FROM ops_rooms_attendant ora, ops_am oa, ops_pm op, hk_attendant hka
                                 WHERE ora.am=oa.am_id AND ora.pm=op.pm_id AND ora.prepared_by=hka.hk_id  LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ora.id) AS rows FROM ops_rooms_attendant ora, ops_am oa, ops_pm op, hk_attendant hka 
                                 WHERE ora.am=oa.am_id AND ora.pm=op.pm_id AND ora.prepared_by=hka.hk_id");
       }
        $config['base_url']= base_url().'ops/checklist_form/get_all_checklist/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 15;
        $this->pagination->initialize($config);
        $data['num_rows']= $mon->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($mon->num_rows()>0) {
            $data['res'] .= '<div class="stocks-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Prepared Date</label></div>
                                <div><label>SHIFT</label></div>
                                <div><label>Prepared By</label></div>    
                            </div>
                            <div class="stocks_list_container1">';
            foreach($mon->result() as $row) {
                $data['res'] .='<div class="stocks-contents1">
                    
                                <div>'.$row->ra_date.'</div>
                                <div>'.$row->am_hr.' AM - '.$row->pm_hr.' PM</div>
                                <div>'.$row->prepared_name.' '.$row->prepared_lastname.'</div>
                       
                                <div class="day">        
                                     <ul>
                                     
                                        <li class="white-litegray show-chreports" title="Show Reports" id="st_'.$row->id.'">Show</li> 
                                        <li class="white-litegray add-chreports" title="Add Reports" id="st_'.$row->id.'">Add</li>
                                        <li class="white-litegray delete-chreports" title="Delete" id="st_'.$row->id.'">Delete</li>
                                           
                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_attendant($offset=0) {
        
       if($this->input->post('like')!="") {
        $attendant = $this->db->query("SELECT * FROM hk_attendant  WHERE fname LIKE '".$this->input->post('like')."%'  LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(hk_id) AS rows FROM hk_attendant WHERE fname LIKE '".$this->input->post('like')."%'");
       }else {
        $attendant = $this->db->query("SELECT * FROM hk_attendant LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(hk_id) AS rows FROM hk_attendant ");
       }
        $config['base_url']= base_url().'ops/checklist_form/get_all_attendant/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $attendant->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($attendant->num_rows()>0) {
            $data['res'] .= '<div class="stocks-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Firstname</label></div>
                                <div><label>Lastname</label></div>
                                
                            </div>
                            <div class="stocks_list_container1">';
            foreach($attendant->result() as $row) {
                $data['res'] .='<div class="stocks-contents1">
                    
                                <div>'.$row->fname.'</div>
                                <div>'.$row->lname.' </div>
                       
                                <div class="day">        
                                     <ul>
                                     
                                        <li class="white-litegray delete-attendant" title="Delete" id="st_'.$row->hk_id.'">Delete</li>
                                           
                                    </ul>
                                </div>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    public function get_all_dc_list() {
       
          $checklist = $this->db->query("SELECT opad.ra_checklist_id, ora.ra_date AS attendant_date, oa.name AS am, op.name AS pm, 
                                                hka.fname AS a_fname, hka.lname AS a_lname, rni.room_no AS room_no, opad.time_in, 
                                                opad.time_out, rs.room_status_name AS room_status, rs.room_status_css AS room_css,
                                                opad.guest_no, opad.bt_in, opad.bt_out, opad.bm_in, opad.bm_out, opad.fit_in, opad.fit_out,
                                                opad.flat_in, opad.flat_out, opad.blanket_in, opad.blanket_out, opad.pilow_in, opad.pilow_out,
                                                opad.pilocs_in, opad.pilocs_out, opad.toilet_tissue_in, opad.toilet_tissue_out, opad.remarks,
                                                opad.extra_work_done
                                   FROM ops_rm_attendant_dchecklist opad, ops_rooms_attendant ora, ops_am oa, ops_pm op, hk_attendant hka, room_number_ids rni, room_status rs
                                   WHERE opad.room_attendant_id=ora.id AND ora.am=oa.am_id AND ora.pm=op.pm_id AND ora.prepared_by=hka.hk_id AND opad.room_no=rni.room_number_id AND opad.room_status_id=rs.room_status_id AND opad.room_attendant_id='".$this->input->post('bid')."'");
        
       
        $data['res'] = "";
        if($checklist->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">ROOM ATTENDANT DAILY CHECKLIST</div>
                            <div class="checkuplist-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Date:</label> <label class="sw-text">'.$checklist->row()->attendant_date.' </label></div>
                                <div><label>SHIFT:</label> <label class="sw-text">'.$checklist->row()->am.' AM - '.$checklist->row()->pm.'PM  </label></div> 
                                <div><label>Prepared By:</label> <label class="sw-text">'.$checklist->row()->a_fname.' '.$checklist->row()->a_lname.' </label></div> 
                                
                                
                            </div>
                            <div class="chlist-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Room#</label></div>
                                <div><label>Time</label></div>
                                <div><label>Room Status</label></div>
                                <div><label>Number of Guest</label></div>
                                <div><label>BT</label></div>
                                <div><label>BM</label></div>
                                <div><label>FITTED</label></div>
                                <div><label>FLAT</label></div>
                                <div><label>BLANKET</label></div>
                                <div><label>PILLOW</label></div>
                                <div><label>PILLOW CASE</label></div>
                                <div><label>TOILET TISUE</label></div>
                                
                                
                            </div>
                            <div class="chlist_list_container1">';
            foreach($checklist->result() as $row) {
                $data['res'] .='<div class="chlist-contents1 '.$row->room_css.'">
                                <div id="room">'.$row->room_no.'</div> 
                                <div>'.$row->time_in.' - '.$row->time_out.'</div>
                                <div>'.$row->room_status.'</div>
                                <div>'.$row->guest_no.'</div>
                                <div>IN:'.$row->bt_in.' OUT:'.$row->bt_out.'</div>
                                <div>IN:'.$row->bm_in.' OUT:'.$row->bm_out.'</div>
                                <div>IN:'.$row->fit_in.' OUT:'.$row->fit_out.'</div>
                                <div>IN:'.$row->flat_in.' OUT:'.$row->flat_out.'</div> 
                                <div>IN:'.$row->blanket_in.' OUT:'.$row->blanket_out.'</div>
                                <div>IN:'.$row->pilow_in.' OUT:'.$row->pilow_out.'</div>
                                <div>IN:'.$row->pilocs_in.' OUT:'.$row->pilocs_out.'</div>
                                <div>IN:'.$row->toilet_tissue_in.' OUT:'.$row->toilet_tissue_out.'</div>
                                
                                <div></div></div>
                              <div class="check-contents1 '.$row->room_css.'">
                                  <div> REMARKS: '.$row->remarks.'</div>
                                  <div> EXTRA WORK DONE: '.$row->extra_work_done.'  </div>
                                  <div class="chl">
                                    <ul>
                                        <li class="ch update-chreports" title="Update Checklist" id="st_'.$row->ra_checklist_id.'">Update</li>
                                        <li class="ch delete-chroomchecklist" title="Delete Checklist" id="st_'.$row->ra_checklist_id.'">Delete</li>
                                   </ul>
                                </div>
                              </div>';
            }
            $data['res'] .= '</div> </div>';
        }
        echo json_encode($data);
    }
     private function _add_checklist() {
        $received = $this->input->post('data');
        $now = date('Y-m-d');
        $values = array(
            'ra_date'=>$received['date'],
            'am'=>$received['am'],
            'pm'=>$received['pm'],
            'prepared_by'=>$received['prepared'],
            'date_added'=>$now
        );
        if($this->db->insert('ops_rooms_attendant',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    } 
    
    private function _add_attendant() {
        $received = $this->input->post('data');
        $values = array(
            
            'fname'=>$received['fname'],
            'lname'=>$received['lname'],
            
        );
        if($this->db->insert('hk_attendant',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    } 
    
     private function _add_checklistreports() {
        $received = $this->input->post('data');
        $values = array(
            'room_attendant_id'=>$received['ch_id'],
            'room_no'=>$received['room_no'],
            'time_in'=>$received['time_in'],
            'time_out'=>$received['time_out'],
            'room_status_id'=>$received['room_status'],
            'guest_no'=>$received['guest_no'],
            'bt_in'=>$received['bt_in'],
            'bt_out'=>$received['bt_out'],
            'bm_in'=>$received['bm_in'],
            'bm_out'=>$received['bm_out'],
            'fit_in'=>$received['fit_in'],
            'fit_out'=>$received['fit_out'],
            'flat_in'=>$received['flat_in'],
            'flat_out'=>$received['flat_out'],
            'blanket_in'=>$received['blanket_in'],
            'blanket_out'=>$received['blanket_out'],
            'pilow_in'=>$received['pilow_in'],
            'pilow_out'=>$received['pilow_out'],
            'pilocs_in'=>$received['pilowcs_in'],
            'pilocs_out'=>$received['pilowcs_out'],
            'toilet_tissue_in'=>$received['toilet_tissue_in'],
            'toilet_tissue_out'=>$received['toilet_tissue_out'],
            'remarks'=>$received['remarks'],
            'extra_work_done'=>$received['extra_work_done']
        );
        if($this->db->insert('ops_rm_attendant_dchecklist',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    }
    
     private function _update_checklistreports() {
        $received = $this->input->post('data');
        $values = array(
            "ra_checklist_id"=>$received['ra_checklist_id'],
            "remarks"=>$received['remarks'],
            "room_no"=>$received['room_no'],
            'time_in'=>$received['time_in'],
            'time_out'=>$received['time_out'],
            'room_status_id'=>$received['room_status'],
            'guest_no'=>$received['guest_no'],
            'bt_in'=>$received['bt_in'],
            'bt_out'=>$received['bt_out'],
            'bm_in'=>$received['bm_in'],
            'bm_out'=>$received['bm_out'],
            'fit_in'=>$received['fit_in'],
            'fit_out'=>$received['fit_out'],
            'flat_in'=>$received['flat_in'],
            'flat_out'=>$received['flat_out'],
            'blanket_in'=>$received['blanket_in'],
            'blanket_out'=>$received['blanket_out'],
            'pilow_in'=>$received['pilow_in'],
            'pilow_out'=>$received['pilow_out'],
            'pilocs_in'=>$received['pilowcs_in'],
            'pilocs_out'=>$received['pilowcs_out'],
            'toilet_tissue_in'=>$received['toilet_tissue_in'],
            'toilet_tissue_out'=>$received['toilet_tissue_out'],
            'extra_work_done'=>$received['extra_work_done']
        );
        $data['result'] = ($this->db->update('ops_rm_attendant_dchecklist',$values,"ra_checklist_id = ".$received['ra_checklist_id'])) ? true:false;
        echo json_encode($data);
    } 
    
   private function _get_addchlist() {
        
           $checklist_reports = $this->db->query("SELECT opad.ra_checklist_id, opad.room_attendant_id, 
                                                         ora.id AS tracker_id, ora.id, ora.ra_date AS date,
                                                         oa.name AS am, op.name AS pm, hka.fname AS fname,
                                                         hka.lname AS lname
                                                  FROM ops_rm_attendant_dchecklist opad, ops_rooms_attendant ora, ops_am oa,
                                                       ops_pm op, hk_attendant hka
                                                  WHERE ora.am=oa.am_id AND ora.pm=op.pm_id AND ora.prepared_by=hka.hk_id AND ora.id='".$this->input->post('bid')."'");
 
        $data['checklist_reports'] = ($checklist_reports) ? $checklist_reports->row(): "Not found";
       
        echo json_encode($data);
    } 
    
    private function _update_chlist() {
        
           $checklist_reports = $this->db->query("SELECT opad.ra_checklist_id, opad.room_attendant_id, 
                                                         ora.id AS tracker_id, opad.room_no, opad.time_in, opad.time_out,
                                                         opad.room_status_id, opad.guest_no, opad.bt_in, opad.bt_out, opad.bm_in,
                                                         opad.bm_out, opad.fit_in, opad.fit_out, opad.flat_in, opad.flat_out, opad.blanket_in,
                                                         opad.blanket_out, opad.pilow_in, opad.pilow_out, opad.pilocs_in, opad.pilocs_out, opad.toilet_tissue_in,
                                                         opad.toilet_tissue_out, opad.remarks, opad.extra_work_done, ora.id, ora.ra_date AS date,
                                                         oa.name AS am, op.name AS pm, hka.fname AS fname,
                                                         hka.lname AS lname
                                                  FROM ops_rm_attendant_dchecklist opad, ops_rooms_attendant ora, ops_am oa,
                                                       ops_pm op, hk_attendant hka
                                                  WHERE ora.am=oa.am_id AND ora.pm=op.pm_id AND opad.room_attendant_id=ora.id AND ora.prepared_by=hka.hk_id AND opad.ra_checklist_id='".$this->input->post('bid')."'");
 
        $data['checklist_reports'] = ($checklist_reports) ? $checklist_reports->row(): "Not found";
       
        echo json_encode($data);
    } 
    
    private function _init(){
        $prepared = $this->db->query("SELECT * FROM hk_attendant");
        $data['prepared'] = ($prepared->num_rows()>0) ? $prepared->result():'No day'; 
        $room_no = $this->db->query("SELECT * FROM room_number_ids");
        $data['room_no'] = ($room_no->num_rows()>0) ? $room_no->result(): 'No rooms';
        $room_status = $this->db->query("SELECT * FROM room_status");
        $data['room_status'] = ($room_status->num_rows()>0) ? $room_status->result(): 'No status';
        echo json_encode($data);
    }   
   
    private function _delete_attn_checklist() {
        $sql = $this->db->query("DELETE FROM ops_rooms_attendant WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_checklist();
    }
    private function _delete_attn() {
        $sql = $this->db->query("DELETE FROM hk_attendant WHERE hk_id = '".$this->input->post('pid')."'");
        $this->get_all_attendant();
    }
     private function _delete_attnroom_checklist() {
        $sql = $this->db->query("DELETE FROM ops_rm_attendant_dchecklist WHERE ra_checklist_id = '".$this->input->post('pid')."'");
        $this->get_all_checklist();
    }
  
}
/* End of Stocks  */