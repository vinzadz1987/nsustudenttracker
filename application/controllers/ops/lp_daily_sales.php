<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Lp_daily_sales extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
		
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	
	public function get( $item = null ) {
		switch( $item ) {
			case "reports":
				$result = array();
				$dsr_report = $this->db->query("
					SELECT dsr_report_id, 
					FROM_UNIXTIME(UNIX_TIMESTAMP(date_added ), '%M %d, %Y - %h:%i %p') as date_added
					FROM dsr_report WHERE prepared_by = {$this->user_id} ORDER BY date_added DESC
				");
				
				if( $dsr_report->num_rows() > 0 ):
					foreach( $dsr_report->result_array() as $dsr_report ):
						$sql = "SELECT * FROM dsr_collections WHERE dsr_report_id = {$dsr_report['dsr_report_id']}";
						$dsr_collections = $this->db->query($sql)->result_array();
						
						$sql = "SELECT * FROM dsr_recap WHERE dsr_report_id = {$dsr_report['dsr_report_id']}";
						$dsr_recap = $this->db->query($sql)->result_array();
						
						$result[] = array(
							'dsr_report_id' 	=> $dsr_report['dsr_report_id'],
							'date_added'		=> $dsr_report['date_added'],
							'dsr_collections'	=> $dsr_collections,
							'dsr_recap'             => $dsr_recap
						);
						
					endforeach;
				endif;
				
				$this->output->set_output(
					json_encode( $result )
				);
				
			break;
                        
                        case "items":
                            $result = array();
                            $sql = "SELECT * FROM lp_price_list ORDER BY cat_id ASC";
                            $query = $this->db->query($sql)->result_array();
                            foreach( $query as $val ) 
                                $result[] = $val["description"];
                            $this->output->set_output(json_encode( array("options" => $result) ));
                       break;
                       
		}
	}
	
	public function save( $item = null ) {
		switch( $item ) {
			case "reports":
				#print "<pre>";
				#print_r($this->input->post('daily_sales'));
				#print "wee";
				
				#dsr_report
				#dsr_collections
				#dsr_recap
				
				$this->db->insert('dsr_report', array("prepared_by"	=> $this->user_id));
				$dsr_report_id = $this->db->insert_id();
				
				foreach( $this->input->post('daily_sales') as $daily_sales ):
					$dsr_collections = array(
						"dsr_report_id"		=> $dsr_report_id,
						"name"				=> $daily_sales['name'],
						"particulars"		=> $daily_sales['particulars'],
						"or_num"			=> $daily_sales['or_num'],
						"yen"				=> $daily_sales['yen'],
						"peso"				=> $daily_sales['peso'],
						"us_dollar"			=> $daily_sales['us_dollar'],
						"k_won"				=> $daily_sales['k_won'],
						"check"				=> $daily_sales['check'],
						"check_num"			=> $daily_sales['check_num'],
						"card"				=> $daily_sales['card'],
						"card_num"			=> $daily_sales['card_num']
					);
					$this->db->insert('dsr_collections', $dsr_collections);
				endforeach;
				
				
				foreach( $this->input->post('recap') as $recap ):
					$dsr_recap = array(
						"dsr_report_id"		=> $dsr_report_id,
						"acct_name"			=> $recap['acct_name'],
						"sales_acct"		=> $recap['sales_acct'],
						"sales_amt"			=> $recap['sales_amt'],
						"ssp_acct"			=> $recap['ssp_acct'],
						"ssp_amt"			=> $recap['ssp_amt'],
					);
				
					$this->db->insert('dsr_recap', $dsr_recap);
				endforeach;
			
			break;
		}
	}
	
	public function uname() {
		$this->output->set_output(
			json_encode(nameAndDepartment( $this->user_id ))
		);
	}
}