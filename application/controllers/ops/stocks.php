<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Stocks extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "count_stocks": $this->_count_stocks_list(); break;
            case "fb_stocklist": $this->get_fb_stocklist(); break;
            case "fb_retrieve_stocklist": $this->_fb_stocks_details(); break;
            case "fb_update_stocklist": $this->_fb_updatestocks_details(); break;
            case "additemcount" : $this->_add_stockitem_count(); break;
            case "os_stocklist": $this->get_os_stocklist(); break;
            case "os_retrieve_stocklist": $this->_os_stocks_details();break;
            case "lp_stocklist": $this->get_lp_stocklist(); break;
            case "lp_retrieve_stocklist": $this->_lp_stocks_details();break;
            case "hs_stocklist": $this->get_hs_stocklist(); break;
            case "hs_retrieve_stocklist": $this->_hs_stocks_details();break;
            default: show_404();
        }
    }
    private function _count_stocks_list() {
        $food_beverage = $this->db->query("SELECT COUNT(stock_type) AS food_beverage
                                 FROM stocks 
                                 WHERE 
                                 stock_type = '1'");  
         $office_supplies = $this->db->query("SELECT COUNT(stock_id) AS office_supplies
                                 FROM stocks 
                                 WHERE 
                                 stock_type = '2'"); 
         $larrys_place = $this->db->query("SELECT COUNT(stock_id) AS larrys_place
                                 FROM stocks 
                                 WHERE 
                                 stock_type = '3'"); 
         $hotel_supplies = $this->db->query("SELECT COUNT(stock_id) AS hotel_supplies
                                 FROM stocks 
                                 WHERE 
                                 stock_type = '14'"); 
        $data['res'] = "";
        if($food_beverage->result()>0) {
            foreach($food_beverage->result() as $row) {
                $data['res'] .='
                              <div class="stcountbox"> 
                               <div>  Food and Beverage : <i>'.$row->food_beverage.'</i> </div>         
                              
                            ';
            }
           
        }
        if($office_supplies->result()>0) {
            foreach($office_supplies->result() as $row) {
                $data['res'] .='
                                
                              <div>   Office Supplies : <i>'.$row->office_supplies.'</i> </div>
                              
                            ';
            }
           
        }
        if($larrys_place->result()>0) {
            foreach($larrys_place->result() as $row) {
                $data['res'] .='
                                
                              <div>  Larrys Place : <i>'.$row->larrys_place.'</i> </div>
                              
                            ';
            }
           
        }
        if($hotel_supplies->result()>0) {
            foreach($hotel_supplies->result() as $row) {
                $data['res'] .='
                                
                              <div> Hotel Supplies : <i>'.$row->hotel_supplies.'</i> </div>
                              </div> 
                            ';
            }
           
        }
        echo json_encode($data);
    } 
    public function get_fb_stocklist($offset=0) {
        
        if($this->input->post('like')!="") {
        $fb = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st.aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='1' AND st.description LIKE '".$this->input->post('like')."%' LIMIT 5 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='1' AND st.description LIKE '".$this->input->post('like')."%'");
        } else {
        $fb = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st.aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='1' LIMIT 5 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='1'");
        }
        $config['base_url']= base_url().'ops/stocks/get_fb_stocklist/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 5;
        $this->pagination->initialize($config);
        $data['num_rows']= $fb->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($fb->num_rows()>0) {
            $data['res'] .= '<div class="stocks-header skyblue-gradient white-shadow">  
                                
                                <div><label>Description</label></div>
                                   <div><label>Type</label></div>
                                <div><label>UOM</label></div>
                                <div><label>Re-order point</label></div>
                                <div><label>Stock in hand</label></div>
                                
                            </div>
                            <div class="stocks_list_container">';
            foreach($fb->result() as $row) {
                $data['res'] .='<div class="stocks-contents">
                                <div>'.$row->description.'</div> 
                                <div>'.$row->type_name.'</div> 
                                <div>'.$row->unit_name.'</div>
                                <div>'.$row->reorder_point.'</div>
                                <div>'.$row->qty_inhand.'</div>
                                    <button class="edit-stock-btn-bal" id="st_'.$row->stock_id.'">update stock</button>    
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _fb_stocks_details() {
        $fbstocks = $this->db->query("SELECT st.stock_id, ty.name AS type_name,
                                      un.name AS unit_name, st.description, 
                                      st.name, st.brand, st.qty_inhand, 
                                      st.reorder_point, st. aqui_price,wst.ws_recipient, 
                                      su.name AS supplier_name, wst.ws_tracker_date AS date,
                                      wstt.name AS tracker_type, wst.ws_tracker_stock_id,
                                      wst.ws_tracker_item_ctr,
                                      wst.ws_binder_item_bal
                                      FROM stocks st, stock_type ty, 
                                      unitsof_measure un, suppliers su,
                                      with_stock_tracker wst, with_stock_tracker_type wstt
                                      WHERE (st.stock_type=ty.id AND st.units=un.id 
                                      AND st.stock_id=wst.ws_tracker_stock_id
                                      AND wst.ws_tracker_type=wstt.id 
                                      AND wst.ws_recipient=su.supplier_id
                                      AND st.stock_type = '1' 
                                      AND st.stock_id='".$this->input->post('bid')."')");
       
            $data['res'] = '';
            $now = date('Y-m-d');
        if($fbstocks->num_rows()>0) {
            $additem=$fbstocks->row()->ws_binder_item_bal;
            $data['res'] .= '<div class="hide updateform">
                                <form action="ops/stocks" id="additemcount">
                                    <label>Item no.</label>
                                    <input class="fld-required" type="text" name="itemnum"/>
                                    <input type="hidden" name="trackertype" value="1"/>
                                    <input type="hidden" name="date" value="'.$now.'"/>
                                    <input type="hidden" name="stock_id" value="'.$fbstocks->row()->stock_id.'"/>
                                    <input type="hidden" name="itembal" value="'.$additem.'"/>                 
                                    <input type="hidden" name="supplier" value='.$fbstocks->row()->ws_recipient.'" />
                                    <button class="bluebutton" type="submit">update</button>
                            </form>   
                            </div>
                             <div class="content-header">
                             <label>'.$fbstocks->row()->type_name.'</label> Stock tracker
                              <label class="st-text to-right"> Stock in hand: 
                             <label>'.$fbstocks->row()->ws_binder_item_bal.' '.$fbstocks->row()->unit_name.'<button class="add">add</button></label>
                             </div>
                            <div class="prs-popupheader1 skyblue-gradient">                                
                                <div><label>Date</label></div>
                                <div><label>Type</label></div>
                                <div><label>To/From</label></div>
                                <div><label>No. of Items</label></div>
                                <div><label>Balance</label></div>
                            </div>
                             <div class="popupprs_list_container1">';
            foreach($fbstocks->result() as $row) {
                $data['res'] .='<div class="prs-popupcontents1">
                                <div><label>'.$row->date.'</label></div> 
                                <div><label>'.$row->tracker_type.'</label></div> 
                                <div><label>'.$row->supplier_name.'</label></div>
                                <div><label>'.$row->ws_tracker_item_ctr.'</label></div>
                                <div><label>'.$row->ws_binder_item_bal.'</label></div>
                               
                            </div>';
                        
            }    
            $data['res'] .= '</div>';
        }
        
        echo json_encode($data);
    }
    private function _fb_updatestocks_details() {
        $fbstocks = $this->db->query("SELECT st.stock_id, ty.name AS type_name,
                                      un.name AS unit_name, st.description, 
                                      st.name, st.brand, st.qty_inhand, 
                                      st.reorder_point, st. aqui_price, 
                                      su.name AS supplier_name, wst.ws_tracker_date AS date,
                                      wstt.name AS tracker_type, wst.ws_tracker_item_ctr,
                                      wst.ws_binder_item_bal
                                      FROM stocks st, stock_type ty, 
                                      unitsof_measure un, suppliers su,
                                      with_stock_tracker wst, with_stock_tracker_type wstt
                                      WHERE (st.stock_type=ty.id AND st.units=un.id 
                                      AND st.stock_id=wst.ws_tracker_stock_id
                                      AND wst.ws_tracker_type=wstt.id AND wst.ws_recipient=su.supplier_id
                                      AND st.stock_type = '1' 
                                      AND st.stock_id='".$this->input->post('uid')."')");
       
          $data['fbstocks'] = ($fbstocks->num_rows()>0) ? $fbstocks->result(): "No stock yet";
        echo json_encode($data);
    }
    private function _add_stockitem_count() {
        $r = $this->input->post('data');
        $v = array(
            
            "ws_tracker_type" => $r['ws_tracker_type'],
            "ws_tracker_stock_id" => $r['ws_tracker_stock_id'],
            "ws_tracker_item_ctr" => $r['ws_tracker_item_ctr'],
            "ws_tracker_date" => $r['ws_tracker_date'],
            "ws_binder_item_bal" => $r['ws_binder_item_bal']+$r['ws_tracker_item_ctr'],
            "ws_recipient" => $r['ws_recipient']
        
         );
        $data['res'] = ($this->db->insert("with_stock_tracker",$v)) ? true:false;
        
        echo json_encode($data);
    }
    public function get_os_stocklist($offset=0) {
        
        if($this->input->post('like')!="") {
        $os = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st.aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='2' AND st.description LIKE '".$this->input->post('like')."%' LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='2' AND st.description LIKE '".$this->input->post('like')."%'");
        } else {
        $os = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='2' LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='2'");
        }
        $config['base_url']= base_url().'ops/stocks/get_os_stocklist/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $os->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($os->result()>0) {
            $data['res'] .= '<div class="stocks-header skyblue-gradient white-shadow">  
                                
                                <div><label>Description</label></div>
                                <div><label>Type</label></div>
                                <div><label>UOM</label></div>
                                <div><label>Re-order point</label></div>
                                <div><label>Stock in hand</label></div>
                            </div>
                            <div class="stocks_list_container">';
            foreach($os->result() as $row) {
                $data['res'] .='<div class="stocks-contents" id="st_'.$row->stock_id.'">
                                <div>'.$row->description.'</div> 
                                <div>'.$row->type_name.'</div> 
                                <div>'.$row->unit_name.'</div>
                                <div>'.$row->reorder_point.'</div>
                                <div>'.$row->qty_inhand.'</div>
                                 
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
     private function _os_stocks_details() {
        $osstocks = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE (st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type = '2' AND st.stock_id='".$this->input->post('bid')."')");   
        $data['osstocks'] = ($osstocks) ? $osstocks->row(): "Not found";
        echo json_encode($data);
    }
    public function get_lp_stocklist($offset=0) {
        
        if($this->input->post('like')!="") {
        $lp = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st.aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='3' AND st.description LIKE '".$this->input->post('like')."%' LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='3' AND st.description LIKE '".$this->input->post('like')."%'");
        } else {
        $lp = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='3' LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='3'");
        }
        $config['base_url']= base_url().'ops/stocks/get_lp_stocklist/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $lp->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($lp->result()>0) {
            $data['res'] .= '<div class="stocks-header skyblue-gradient white-shadow">  
                                
                                <div><label>Description</label></div>
                                <div><label>Type</label></div>
                                <div><label>UOM</label></div>
                                <div><label>Re-order point</label></div>
                                <div><label>Stock in hand</label></div>
                            </div>
                            <div class="stocks_list_container">';
            foreach($lp->result() as $row) {
                $data['res'] .='<div class="stocks-contents" id="st_'.$row->stock_id.'">
                                <div>'.$row->description.'</div> 
                                <div>'.$row->type_name.'</div> 
                                <div>'.$row->unit_name.'</div>
                                <div>'.$row->reorder_point.'</div>
                                <div>'.$row->qty_inhand.'</div>
                                 
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _lp_stocks_details() {
        $lpstocks = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE (st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type = '3' AND st.stock_id='".$this->input->post('bid')."')");   
        $data['lpstocks'] = ($lpstocks) ? $lpstocks->row(): "Not found";
        echo json_encode($data);
    }
    public function get_hs_stocklist($offset=0) {
        
        if($this->input->post('like')!="") {
        $hs = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st.aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='14' AND st.description LIKE '".$this->input->post('like')."%' LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='14' AND st.description LIKE '".$this->input->post('like')."%'");
        } else {
        $hs = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='14' LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(st.stock_id) as rows FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type='14'");
        }
        $config['base_url']= base_url().'ops/stocks/get_hs_stocklist/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $hs->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($hs->result()>0){       
            $data['res'] .= '<div class="stocks-header skyblue-gradient white-shadow">  
                                
                                <div><label>Description</label></div>
                                <div><label>Type</label></div>
                                <div><label>UOM</label></div>
                                <div><label>Re-order point</label></div>
                                <div><label>Stock in hand</label></div>
                            </div>
                            <div class="stocks_list_container">';
            
            foreach($hs->result() as $row) {
                
                $data['res'] .='<div class="stocks-contents" id="st_'.$row->stock_id.'">
                                <div>'.$row->description.'</div> 
                                <div>'.$row->type_name.'</div> 
                                <div>'.$row->unit_name.'</div>
                                <div>'.$row->reorder_point.'</div>
                                <div>'.$row->qty_inhand.'</div>
                                 
                            </div>';
            }
            $data['res'] .= '</div>';
            
        } else {
            $data['res'] .= '<div>no</div>';
        }
        echo json_encode($data);
    }
     private function _hs_stocks_details() {
        $hsstocks = $this->db->query("SELECT st.stock_id, ty.name AS type_name, un.name AS unit_name, st.description, st.name, st.brand, st.qty_inhand, st.reorder_point, st. aqui_price, su.name AS supplier_name FROM stocks st, stock_type ty, unitsof_measure un, suppliers su WHERE (st.stock_type=ty.id AND st.units=un.id AND st.supplier=su.supplier_id AND st.stock_type = '14' AND st.stock_id='".$this->input->post('bid')."')");   
        $data['hsstocks'] = ($hsstocks) ? $hsstocks->row(): "Not found";
        echo json_encode($data);
    }
}
/* End of Stocks  */