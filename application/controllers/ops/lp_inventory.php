<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Lp_inventory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_inv_date": $this->get_all_inv_date();break;
            case "all_item": $this->get_all_item();break;
            case "add_lp_inv": $this->_add_lp_inv();break;
            case "get_pys_details": $this->_pys_inv_details();break;
            case "update_pys_inv": $this->_update_pys_inv();break;
            case "delete_items": $this->_delete_items();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    
    public function get_all_inv_date($offset=0) {
        
   
       if($this->input->post('like')!="") {
        $inventory = $this->db->query("SELECT * 
                                  FROM ops_lp_inv
                                  WHERE inv_to LIKE '".$this->input->post('like')."%' ORDER BY inv_to ASC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_lp_inv 
                                    WHERE inv_to LIKE '".$this->input->post('like')."%'");
       }else {
        $inventory = $this->db->query("SELECT * 
                                 FROM ops_lp_inv ORDER BY inv_to ASC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(inv_id) AS rows FROM ops_lp_inv");
       }
        $config['base_url']= base_url().'ops/lp_inventory/get_all_inv_date/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $inventory->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
     
        $data['res'] = "";
        if($inventory->num_rows()>0) {
            $data['res'] .= ' <div class="content-header menucenter">Inventory Covered Date</div>
                              <div class="rooms-header1 skyblue-gradient white-shadow">
                                <div><label>From</label></div>
                                <div><label>To</label></div>
                              </div>
                            <div class="rooms_list_container1">';
                 
            foreach($inventory->result() as $row) {
             
            $data['res'] .='<div class="rooms-contents1">
                          <div class="sw-text1" id="st_'.$row->inv_from.'" title="Covered Date">
                            <label class="st-text"> '.$row->inv_from.'</label>
                          </div>
                          <div class="sw-text1" id="st_'.$row->inv_from.'" title="Covered Date">
                            <label class="st-text">'.$row->inv_to.' </label>
                          </div>
                          <div class="pi"><a class="to-rigth pic pi" id="st_'.$row->inv_id.'" title="Physical Inventory">Physical Inventory</a></div>
                           </div>
                        ';
            }
           
        }
         
        echo json_encode($data);
    }
    
   
    
    public function get_all_item() {
        
         $catitem = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 1 ORDER BY lpi.cat_id ASC");
         $catitemsum = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 1");
         $catitem2 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 2 ORDER BY lpi.cat_id ASC");
          $catitemsum2 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 2");
         $catitem3 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 3 ORDER BY lpi.cat_id ASC");
          $catitemsum3 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 3");
         $catitem4 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 4 ORDER BY lpi.cat_id ASC");
          $catitemsum4 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 4");
         $catitem5 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 5 ORDER BY lpi.cat_id ASC");
         $catitemsum5 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 5");
         $catitem6 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 6 ORDER BY lpi.cat_id ASC");
         $catitemsum6 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 6");
         $catitem7 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 7 ORDER BY lpi.cat_id ASC");
         $catitemsum7 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 7");
         $catitem8 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 8 ORDER BY lpi.cat_id ASC");
         $catitemsum8 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 8");
         $catitem9 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 9 ORDER BY lpi.cat_id ASC");
         $catitemsum9 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 9");
         $catitem10 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 10 ORDER BY lpi.cat_id ASC");
         $catitemsum10 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 10");
         $catitem11 = $this->db->query("SELECT lpi.lp_pys_id, lpi.cat_id, lpi.description, lpi.price,
                                              lpi.sold_items, lpi.total_cost
                                      FROM lp_pys_inv lpi
                                      WHERE lpi.cat_id = 11 ORDER BY lpi.cat_id ASC");
         $catitemsum11 = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi WHERE lpi.cat_id = 11");
         
         $alltotal = $this->db->query("SELECT SUM(lpi.sold_items) AS si_sum, SUM(lpi.total_cost) AS tc_sum
                                      FROM lp_pys_inv lpi ");
         
        $data['res'] = "";
       
        $data['res'] .= ' <div class="white-litegray menucenter">Physical Inventory</div>
                                <div class="stockslp-header skyblue-gradient white-shadow">
                                    <div><label>Item Description</label></div>
                                    <div><label>Sold Items</label></div>
                                    <div><label>Price</label></div>
                                    <div><label>Total Cost</label></div>
                                    <div><label></label></div>
                                </div>
                                <div class="stockslp_list_container">';    
        if($catitem->num_rows()<=0){
            $data['res'].='<div class="input-fieldcat">BISCUITS:</div>';
            $data['res'] .= '<div class="empty menucenter">No Item Yet.</div>';
        }else{
         
        if($catitem->num_rows()>0) {
            $data['res'] .= '<div class="input-fieldcat">BISCUITS:</div>';
        
            foreach($catitem->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                                <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }  
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem2->num_rows()<=0) {
            $data['res'].='<div class="input-fieldcat">PEANUTS:</div>';
            $data['res'] .= '<div class="empty menucenter">No Item Yet.</div>';
        }  else {
        if($catitem2->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">PEANUTS:</div>';
        
            foreach($catitem2->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                    
                                <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }  
             $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum2->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum2->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem3->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">FROZEN / BREADS:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{ 
        if($catitem3->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">FROZEN / BREADS:</div>';
        
            foreach($catitem3->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                    
                                <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }   
             $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum3->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum3->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem4->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">CHOCOLATES:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{ 
        if($catitem4->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">CHOCOLATES:</div>';
        
            foreach($catitem4->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }    
             $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum4->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum4->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem5->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">JUNKS:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{ 
        if($catitem5->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">JUNKS:</div>';
        
            foreach($catitem5->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }  
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum5->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum5->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem6->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">DRINKS:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{ 
        if($catitem6->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">DRINKS:</div>';
        
            foreach($catitem6->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }  
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum6->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum6->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem7->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">INS. PANCIT CANTON:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{ 
        if($catitem7->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">INS. PANCIT CANTON:</div>';
        
            foreach($catitem7->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }    
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum7->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum7->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem8->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">CIGARETTES:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{ 
        if($catitem8->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">CIGARETTES:</div>';
        
            foreach($catitem8->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                              <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }  
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum8->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum8->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem9->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">CANDIES:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{
        if($catitem9->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">CANDIES:</div>';
        
            foreach($catitem9->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }  
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum9->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum9->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem10->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">JUICES IN CAN / BOTTLE:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{
        if($catitem10->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">JUICES IN CAN / BOTTLE:</div>';
        
            foreach($catitem10->result() as $row) {
             
            $data['res'] .='<div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }     
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum10->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum10->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        }
        }
        if($catitem11->num_rows()<=0){
          $data['res'].='<div class="input-fieldcat">OTHERS:</div>
                         <div class="empty menucenter">No Item Yet</div>';
        }else{
        if($catitem11->num_rows()>0) {
                             
            $data['res'] .= '<div class="input-fieldcat">OTHERS:</div>';
        
            foreach($catitem11->result() as $row) {
             
            $data['res'] .='</div><div class="stockslp-contents">
                            
                               <div>
                                <label>'.$row->description.'</label>
                               </div>
                               <div>
                               <label>'.$row->sold_items.'</label>
                               </div>
                               <div>
                               <label>'.$row->price.'</label>
                               </div>
                               <div>
                               <label>'.$row->total_cost.'</label>
                               </div>
                                <div>
                                <a class=" to-right" id="st_'.$row->lp_pys_id.'" title="addbalance '.$row->description.'"> [+]</a>
                                <a class="psy-inv-update updatepsyinv ui to-right" title="Update Psy Inv" id="st_'.$row->lp_pys_id.'">[/]</a>
                                </div>
                                
                           </div>';
            }    
            $data['res'] .= '<div class="stockslp-header">
                                    <div><label class="st-text">TOTAL</label></div>
                                    <div><label class="st-text">'.$catitemsum11->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text sum">'.$catitemsum11->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        } 
        }
 $data['res'] .= '<div class="white-litegray to-right"><label class="blue-button close">x</label></div>';
 $data['res'] .= '<div class="stockslp-header white-litegray alltotal">
                                    <div><label class="sw-text">ALL TOTAL</label></div>
                                    <div><label class="sw-text">'.$alltotal->row()->si_sum.'</label></div>
                                    <div><label class="sw-text"></label></div>
                                    <div><label class="sw-text">'.$alltotal->row()->tc_sum.'</label></div>
                                    <div><label></label></div>
                                </div>';
        echo json_encode($data);
    }
    
    
     private function _add_lp_inv() {
        $received = $this->input->post('data');
        $values = array(
            
            'inv_from'=>$received['inv_from'],
            'inv_to'=>$received['inv_to']
              
        );
        $sql = $this->db->query("SELECT * FROM ops_lp_inv WHERE inv_to='".$received['inv_to']."'");
        if($sql->result()==true) {
          echo "failed"; 
        }else {
           if($this->db->insert('ops_lp_inv',$values)) {
            echo "added";
        } 
        }
    } 
    
   
     private function _update_pys_inv() {
        $received = $this->input->post('data');
        $values = array(
            "lp_pys_id"=>$received['lp_pys_id'],
            "sold_items"=>$received['sold_items'],
            "total_cost"=>$received['total_cost']
        );
        $data['result'] = ($this->db->update('lp_pys_inv',$values,"lp_pys_id = ".$received['lp_pys_id'])) ? true:false;
        echo json_encode($data);
    } 
   
    private function _pys_inv_details() {
           $pys_inv = $this->db->query("SELECT * FROM lp_pys_inv WHERE lp_pys_id ='".$this->input->post('bid')."'");
           $data['pys_inv'] = ($pys_inv) ? $pys_inv->row(): "Not found";
        echo json_encode($data);
    } 
    
    private function _init(){
        $category = $this->db->query("SELECT * FROM lp_item_cat");
        $data['category'] = ($category->num_rows()>0) ? $category->result():'No category'; 
        echo json_encode($data);
    }   
   
   
     private function _delete_items() {
        $sql = $this->db->query("DELETE FROM lp_price_list WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_item();
    }
   
}
/* End of Lp inventory  */