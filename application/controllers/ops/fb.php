<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class fb extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all": $this->get_all_ing_list(); break;
            case "allmenu": $this->get_all_menu_list();break;
            case "allutensils": $this->get_all_utensils_list();break;
            case "deliverysched": $this->get_deliver_schedule();break;
            case "add_ingredient": $this->_add_ing(); break;
            case "add_menu": $this->_add_menu();break;
            case "add_utensils": $this->_add_utensils();break;
            case "add_deliver_sched": $this->_add_deliver_sched();break;
            case "retrieve_details": $this->_stocks_details();break;
            case "update_ingred": $this->_ingred_update();break;
            case "update_menu": $this->_menu_update();break;
            case "update_utensils": $this->_utensils_update();break;
            case "update_sched": $this->_sched_delivery();break;
            case "delete_ingred": $this->_delete_ingredients();break;
            case "delete_menu": $this->_delete_menu();break;
            case "delete_utensils": $this->_delete_utensils();break;
            case "delete_sched": $this->_delete_sched();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    public function get_deliver_schedule() {
      /*  if($this->input->post('like')!="") {
            $deliver = $this->db->query("SELECT ods.id, ods.item_name, prsd.description AS item_description, su.name AS supplier_name, ods.date_delivered FROM ops_delivery_schedule ods, prs_details prsd, suppliers su WHERE ods.item_name=prsd.prs_details_id AND ods.supplier=su.supplier_id AND item_description LIKE '".$this->input->post('like')."%' LIMIT 2 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(ods.id) AS rows FROM ops_delivery_schedule ods, prs_details prsd, suppliers su WHERE ods.item_name=prsd.prs_details_id AND ods.supplier=su.supplier_id AND item_description LIKE '".$this->input->post('like')."%'");           
       }else{*/
            $deliver = $this->db->query("SELECT ods.id, ods.item_name, prsd.description AS item_description, su.name AS supplier_name, ods.date_delivered FROM ops_delivery_schedule ods, prs_details prsd, suppliers su WHERE ods.item_name=prsd.prs_details_id AND ods.supplier=su.supplier_id");
            $_count = $this->db->query("SELECT COUNT(ods.id) AS rows FROM ops_delivery_schedule ods, prs_details prsd, suppliers su WHERE ods.item_name=prsd.prs_details_id AND ods.supplier=su.supplier_id");           
      /* }
        $config['base_url']= base_url().'ops/fb/get_deliver_schedule/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 2;
        $this->pagination->initialize($config);
        $data['num_rows'] = $deliver->num_rows();
        $data['pagination'] = $this->pagination->create_links();*/
        $data['res'] = "";
        if($deliver->num_rows()<=0){
            $data['res']='<div class="countbox">SCHEDULE OF DELIVERY  NOT FOUND.</div>';
        }else{
        if($deliver->num_rows()>0) {
            $data['res'] .= '
                            <div class="menu-header1s less-pale-skyblue-gradient white-shadow">  
                       
                                <div><label>Item Name</label></div>
                                <div><label>Supplier</label></div>
                                <div><label>Date Deliver</label></div>                                                       
                            </div>
                            <div class="menu_list_container1s">';
            foreach($deliver->result() as $row) {
                $data['res'] .='<div class="menu-contents1s">
                                <div>'.$row->item_description.'</div> 
                                <div>'.$row->supplier_name.'</div> 
                                <div>'.$row->date_delivered.'</div>  
                                
                                <button class="edit-sd-btn" id="st_'.$row->id.'">update</button>
                                <button class="del-sd-btn" id="st_'.$row->id.'">x</button>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        }
        echo json_encode($data);
    }
    public function get_all_menu_list($offset=0) {  
        if($this->input->post('like')!="") {
        $menu = $this->db->query("SELECT ops_wm.id, ops_wm.m_name,
                                 ops_wm.m_desc, ops_wmt.name AS mealtime,
                                 ops_wmm.name AS mealmonth, ops_wmw.name AS mealweek, ops_wm.date_added
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id AND ops_wm.m_name LIKE '".$this->input->post('like')."%' ORDER BY ops_wm.date_added DESC LIMIT 5 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ops_wm.id) AS rows                          
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id AND ops_wm.m_name LIKE '".$this->input->post('like')."%'");
        }
        else if($this->input->post('mealtype')){
         $menu = $this->db->query("SELECT ops_wm.id, ops_wm.m_name,
                                 ops_wm.m_desc, ops_wmt.name AS mealtime,
                                 ops_wmm.name AS mealmonth, ops_wmw.name AS mealweek, ops_wm.date_added
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id AND ops_wm.ml_time = '".$this->input->post('mealtype')."' AND ops_wm.m_name LIKE '".$this->input->post('like')."%'");
        $_count = $this->db->query("SELECT COUNT(ops_wm.id) AS rows                          
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id AND ops_wm.ml_time = '".$this->input->post('mealtype')."'");
         
        } else if($this->input->post('month')){
          $menu = $this->db->query("SELECT ops_wm.id, ops_wm.m_name,
                                 ops_wm.m_desc, ops_wmt.name AS mealtime,
                                 ops_wmm.name AS mealmonth, ops_wmw.name AS mealweek, ops_wm.date_added
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id AND ops_wm.ml_month = '".$this->input->post('month')."' AND ops_wm.m_name LIKE '".$this->input->post('like')."%'");
         $_count = $this->db->query("SELECT COUNT(ops_wm.id) AS rows                          
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id AND ops_wm.ml_month = '".$this->input->post('month')."'");
           
        }else{ 
        $menu = $this->db->query("SELECT ops_wm.id, ops_wm.m_name,
                                 ops_wm.m_desc, ops_wmt.name AS mealtime,
                                 ops_wmm.name AS mealmonth, ops_wmw.name AS mealweek, ops_wm.date_added
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id ORDER BY ops_wm.date_added DESC LIMIT 5 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ops_wm.id) AS rows                          
                                 FROM
                                 ops_weekly_menu ops_wm, ops_wk_ml_time ops_wmt,
                                 ops_wk_ml_month ops_wmm, ops_wk_ml_week ops_wmw
                                 WHERE
                                 ops_wm.ml_time=ops_wmt.id AND ops_wm.ml_month=ops_wmm.id
                                 AND ops_wm.ml_week=ops_wmw.id");
        }
        $config['base_url']= base_url().'ops/fb/get_all_menu_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 5;
        $this->pagination->initialize($config);
        $data['num_rows']= $menu->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($menu->num_rows()<=0){
            $data['res']='<div class="countbox">MENU NOT FOUND.</div>';
        }else{
        if($menu->num_rows()>0) {
            $data['res'] .= '
                            <div class="menu-header1 less-pale-skyblue-gradient white-shadow">  
                                
                                <div><label>Date Added</label></div>
                                <div><label>Name</label></div>
                                <div><label>Description</label></div>
                                <div><label>Meal</label></div>
                                <div><label>Month</label></div>
                                <div><label>Week</label></div>
                                     
                            </div>
                            <div class="menu_list_container1">';
            foreach($menu->result() as $row) {
                $data['res'] .='<div class="menu-contents1">
                                <div>'.$row->date_added.'</div> 
                                <div>'.$row->m_name.'</div> 
                                <div>'.$row->m_desc.'</div>  
                                <div>'.$row->mealtime.'</div>
                                <div>'.$row->mealmonth.'</div>
                                <div>'.$row->mealweek.'</div>
                                <button class="edit-menu-btn" id="st_'.$row->id.'">update</button>
                                <button class="del-menu-btn" id="st_'.$row->id.'">x</button>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        }
        echo json_encode($data);
    }
    public function get_all_utensils_list($offset=0) {  
       
        if($this->input->post('like')!="") {
           $utensils = $this->db->query("SELECT * FROM ops_utensils WHERE u_name LIKE '".$this->input->post('like')."%'LIMIT 2 OFFSET".$offset);
           $_count = $this->db->query("SELECT COUNT(id) AS rows FROM ops_utensils WHERE u_name LIKE '".$this->input->post('like')."%'");
        }else{
           $utensils = $this->db->query("SELECT * FROM ops_utensils");
           $_count = $this->db->query("SELECT COUNT(id) AS rows FROM ops_utensils");
        }
        $config['base_url']= base_url().'ops/fb/get_all_utensils_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 2;
        $this->pagination->initialize($config);
        $data['num_rows']= $utensils->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if ($utensils->num_rows()<=0) {
            $data['res'] = '<div class="countbox">UTENSIL NOT FOUND.</div>';
        }else {
        if($utensils->num_rows()>0) {
            $data['res'] .= '
                            <div class="menu-header1 less-pale-skyblue-gradient white-shadow">  
                                
                                <div><label>Date Added</label></div>
                                <div><label>Name</label></div>
                                <div><label>Description</label></div>
                                <div><label>Status</label></div>          
                            </div>
                            <div class="menu_list_container1">';
            foreach($utensils->result() as $row) {
                $data['res'] .='<div class="menu-contents1">
                                <div>'.$row->added_date.'</div> 
                                <div>'.$row->u_name.'</div> 
                                <div>'.$row->u_desc.'</div>
                                <div>idle: '.$row->quantity.', Borrow: '.$row->quantity.'</div>     
                                <button class="edit-utensil-btn" id="st_'.$row->id.'">Borrow</button>
                                <button class="edit-utensils-btn" id="st_'.$row->id.'">Update</button>
                                <button class="del-utensils-btn" id="st_'.$row->id.'">x</button>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        }
        echo json_encode($data);
    }   
    public function get_all_ing_list($offset=0) {
        
        if($this->input->post('like')!="") {
        $mon = $this->db->query("SELECT ops_i.id, ops_i.name, ops_i.desc, ops_d.name AS day, ops_i.dte FROM ops_ingredient ops_i, ops_day ops_d WHERE ops_i.day=ops_d.id  AND ops_i.name LIKE '".$this->input->post('like')."%' ORDER BY ops_i.dte DESC LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ops_i.id) AS rows FROM ops_ingredient ops_i, ops_day ops_d WHERE ops_i.day=ops_d.id AND ops_i.name LIKE '".$this->input->post('like')."%'");
        }
        else if($this->input->post('day')){
        $mon = $this->db->query("SELECT ops_i.id, ops_i.name, ops_i.desc, ops_d.name AS day, ops_i.dte FROM ops_ingredient ops_i, ops_day ops_d WHERE ops_i.day=ops_d.id AND ops_i.day = '".$this->input->post('day')."'  AND ops_i.name LIKE '".$this->input->post('like')."%' ORDER BY ops_i.dte DESC LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ops_i.id) AS rows FROM ops_ingredient ops_i, ops_day ops_d WHERE ops_i.day=ops_d.id AND ops_i.day = '".$this->input->post('day')."'");    
        } else {
        $mon = $this->db->query("SELECT ops_i.id, ops_i.name, ops_i.desc, ops_d.name AS day, ops_i.dte FROM ops_ingredient ops_i, ops_day ops_d WHERE ops_i.day=ops_d.id  ORDER BY ops_i.dte DESC LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(ops_i.id) AS rows FROM ops_ingredient ops_i, ops_day ops_d WHERE ops_i.day=ops_d.id ");
        }
        $config['base_url']= base_url().'ops/fb/get_all_ing_list/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 15;
        $this->pagination->initialize($config);
        $data['num_rows']= $mon->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($mon->num_rows()>0) {
            $data['res'] .= '<div class="menu-header1 less-pale-skyblue-gradient white-shadow">  
                                
                                <div><label>Date Added</label></div>
                                <div><label>Name</label></div>
                                <div><label>Description</label></div>
                                <div><label>Day</label></div>
                                     
                            </div>
                            <div class="menu_list_container1">';
            foreach($mon->result() as $row) {
                $data['res'] .='<div class="menu-contents1">
                                <div>'.$row->dte.'</div> 
                                <div>'.$row->name.'</div> 
                                <div>'.$row->desc.'</div>  
                                <div>'.$row->day.'</div>
                                <button class="edit-ing-btn" id="st_'.$row->id.'">update</button>
                                <button class="del-ing-btn" id="st_'.$row->id.'">x</button>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
     private function _add_ing() {
        $received = $this->input->post('data');
        $now = date('Y-m-d');
        $values = array(
            'name'=>$received['name'],
            'desc'=>$received['desc'],
            'day'=>$received['day'],
            'dte'=>$now
        );
        if($this->db->insert('ops_ingredient',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    }
    private function _add_menu() {
        $r = $this->input->post('data');
        $now = date('Y-m-d');
        $v = array(
            'm_name'=>$r['m_name'],
            'm_desc'=>$r['m_desc'],
            'ml_time'=>$r['ml_time'],
            'ml_month'=>$r['ml_month'],
            'ml_week'=>$r['ml_week'],
            'date_added'=>$now
        );
        if($this->db->insert('ops_weekly_menu',$v)) {
            echo "added";
        } else {
            echo "failed";
        }
    }
    private function _add_utensils() {
        $r = $this->input->post('data');
        $now = date('Y-m-d');
        $v = array(
            'u_name'=>$r['uten_name'],
            'u_desc'=>$r['u_desc'],
            'quantity'=>$r['quantity'],
            'added_date'=>$now
        );
        if($r['quantity']!=0){
        if($this->db->insert('ops_utensils',$v)) {
            echo "added";
        }   
      }
        else {
            echo "failed";
        }
    }
    private function _add_deliver_sched() {
        $r = $this->input->post('data');
        $now = date('Y-m-d');
        $v = array (
            'item_name' => $r['item'],
            'supplier' => $r['supplier'],
            'date_delivered' => $r['date_deliver'],
            'date_added' => $now
        );
        if($this->db->insert('ops_delivery_schedule',$v)){
            echo "added";
        }else{
            echo 'failed';
        }
    }
    private function _stocks_details() { 
        $ingredients = $this->db->query("SELECT * FROM ops_ingredient WHERE id='".$this->input->post('bid')."'");
        $data['ingredients'] = ($ingredients) ? $ingredients->row(): "Not found";
        $menu = $this->db->query("SELECT * FROM ops_weekly_menu WHERE id='".$this->input->post('bid')."'");
        $data['menu'] = ($menu) ? $menu->row(): "Not found";
        $utensils = $this->db->query("SELECT * FROM ops_utensils WHERE id='".$this->input->post('bid')."'");
        $data['utensils'] = ($utensils) ? $utensils->row(): "Not fount";
        $sd = $this->db->query("SELECT * FROM ops_delivery_schedule WHERE id='".$this->input->post('bid')."'");
        $data['sd'] = ($sd) ? $sd->row(): "Not found";
        echo json_encode($data);
    }
    
    private function _init(){
        $day = $this->db->query("SELECT * FROM ops_day");
        $data['day'] = ($day->num_rows()>0) ? $day->result():'No day';
        $ml_time = $this->db->query("SELECT * FROM ops_wk_ml_time");
        $data['ml_time'] = ($ml_time->num_rows()>0) ? $ml_time->result():'No time';
        $ml_month = $this->db->query("SELECT * FROM ops_wk_ml_month");
        $data['ml_month'] = ($ml_month->num_rows()>0) ? $ml_month->result():'No month';
        $ml_week = $this->db->query("SELECT * FROM ops_wk_ml_week");
        $data['ml_week'] = ($ml_week->num_rows()>0) ? $ml_week->result():'No month';
        $department = $this->db->query("SELECT * FROM department_details");
        $data['department'] = ($department->num_rows()>0) ? $department->result(): 'No department';
        $item = $this->db->query("SELECT * FROM prs_details");
        $data['item'] = ($item->num_rows()>0) ? $item->result(): 'No item';
        $supplier = $this->db->query("SELECT * FROM suppliers");
        $data['supplier'] = ($supplier->num_rows()>0) ? $supplier->result(): 'No supplier';
        echo json_encode($data);
    }   
    private function _ingred_update() {
        $received = $this->input->post('data');
        $values = array(
            "id" => $received['id'],
            "name" => $received['name'],
            "desc" => $received['desc'],
            "day" => $received['day']
        );
        $data['result'] = ($this->db->update('ops_ingredient',$values,"id = ".$received['id'])) ? true:false;
        echo json_encode($data);
    }
    private function _menu_update() {
        $received = $this->input->post('data');
        $values = array(
            "id" => $received['id'],
            "m_name" => $received['m_name'],
            "m_desc" => $received['m_desc'],
            "ml_time" => $received['ml_time'],
            "ml_month" => $received['ml_month'],
            "ml_week" => $received['ml_week']
        );
        $data['result'] = ($this->db->update('ops_weekly_menu',$values,"id = ".$received['id'])) ? true:false;
        echo json_encode($data);
    }
    private function _utensils_update() {
        $received = $this->input->post('data');
        $values = array(
            "id" => $received['id'],
            "u_name" => $received['u_name'],
            "u_desc" => $received['u_desc'],
            "quantity" => $received['u_quantity']
        );
        $data['result'] = ($this->db->update('ops_utensils',$values,"id = ".$received['id'])) ? true:false;
        echo json_encode($data);
    }
     private function _sched_delivery() {
        $received = $this->input->post('data');
        $values = array(
            "id" => $received['id'],
            "item_name" => $received['item_name'],
            "supplier" => $received['supplier_name'],
            "date_delivered" => $received['delivery_date']
            
        );
        $data['result'] = ($this->db->update('ops_delivery_schedule',$values,"id = ".$received['id'])) ? true:false;
        echo json_encode($data);
    }
    private function _delete_ingredients() {
        $sql = $this->db->query("DELETE FROM ops_ingredient WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_ing_list();
    }
    private function _delete_menu() {
        $sql = $this->db->query("DELETE FROM ops_weekly_menu WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_menu_list();
    } 
     private function _delete_utensils() {
        $sql = $this->db->query("DELETE FROM ops_utensils WHERE id = '".$this->input->post('pid')."'");
        $this->get_all_utensils_list();
    } 
    private function _delete_sched() {
        $sql = $this->db->query("DELETE FROM ops_delivery_schedule WHERE id = '".$this->input->post('pid')."'");
        $this->get_deliver_schedule();
    } 
}
/* End of Stocks  */