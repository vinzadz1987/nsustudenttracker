<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Main_supplies extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_tools": $this->get_all_tools();break;
            case "add_maint_tools": $this->_add_maint_tools();break;
            case "get_tools_details": $this->_tools_details();break;
            case "update_tools": $this->_update_tools();break;
            case "delete_tools": $this->_delete_tools();break;
            case "init": $this->_init();break;
            default: show_404();
        }
    }
    public function get_all_tools($offset=0) {
        
       if($this->input->post('like')!="") {
        $tools = $this->db->query("SELECT omt.tools_id, omt.item_num, omt.description, omt.quantity, unom.name AS unit_name, dd.dept_name AS dept_name
                                 FROM ops_main_tools omt, unitsof_measure unom, department_details dd
                                 WHERE omt.uom=unom.id AND omt.department=dd.deptd_id AND omt.description LIKE '".$this->input->post('like')."%' ORDER BY omt.description DESC  LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(omt.tools_id) AS rows FROM ops_main_tools omt, unitsof_measure unom, department_details dd 
                                    WHERE omt.uom=unom.id AND omt.department=dd.deptd_id AND omt.description LIKE '".$this->input->post('like')."%'");
       }else {
        $tools = $this->db->query("SELECT omt.tools_id, omt.item_num, omt.description, omt.quantity, unom.name AS unit_name, dd.dept_name AS dept_name
                                 FROM ops_main_tools omt, unitsof_measure unom, department_details dd
                                 WHERE omt.uom=unom.id AND omt.department=dd.deptd_id ORDER BY omt.description DESC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(omt.tools_id) AS rows FROM ops_main_tools omt, unitsof_measure unom, department_details dd
                                 WHERE omt.uom=unom.id AND omt.department=dd.deptd_id");
       }
        $config['base_url']= base_url().'ops/main_supplies/get_all_tools/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $tools->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($tools->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">Maintenance Supplies</div>
                            <div class="stocks-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Item Num</label></div>
                                <div><label>Description</label></div>
                                <div><label>Quantity</label></div>
                                <div><label>Dept. Used</label></div>

                            </div>
                            <div class="stocks_list_container1">';
            foreach($tools->result() as $row) {
                $data['res'] .='<div class="stocks-contents1">
                                
                                <div>'.$row->item_num.'</div>
                                <div>'.$row->description.'</div>
                                <div>'.$row->quantity.' '.$row->unit_name.'</div>
                                <div>'.$row->dept_name.'</div>
                                <button class="blue-button update-tools" id="st_'.$row->tools_id.'">Update</button>
                                <label class="delete-tools" id="st_'.$row->tools_id.'">X</label>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    
    
     private function _add_maint_tools() {
        $received = $this->input->post('data');
        $now = date('m-d-Y');
        $values = array(
            
            'item_num'=>$received['itemno'],
            'description'=>$received['desc'],
            'quantity'=>$received['quant'],
            'uom'=>$received['uom'],
            'department'=>$received['deptar'],
            'date'=>$now
                
        );
        if($this->db->insert('ops_main_tools',$values)) {
            echo "added";
        } else {
            echo "failed";
        }
    } 
    
   
     private function _update_tools() {
        $received = $this->input->post('data');
        $values = array(
            "tools_id"=>$received['tools_id'],
            "item_num"=>$received['item_num'],
            "description"=>$received['tools_desc'],
            "quantity"=>$received['quant'],
            "uom"=>$received['unit'],
            "department"=>$received['dept']
        );
        $data['result'] = ($this->db->update('ops_main_tools',$values,"tools_id = ".$received['tools_id'])) ? true:false;
        echo json_encode($data);
    } 
   
    private function _tools_details() {
        $tools = $this->db->query("SELECT * FROM ops_main_tools WHERE tools_id='".$this->input->post('bid')."'");
        $data['tools'] = ($tools) ? $tools->row(): "Not found";
        echo json_encode($data);
    } 
    
    private function _init(){
        $dept = $this->db->query("SELECT * FROM department_details");
        $data['dept'] = ($dept->num_rows()>0) ? $dept->result():'No Dept';
        $uom = $this->db->query("SELECT * FROM unitsof_measure");
        $data['uom'] = ($uom->num_rows()>0) ? $uom->result():'No uom';
        echo json_encode($data);
    }   
   
     private function _delete_tools() {
        $sql = $this->db->query("DELETE FROM ops_main_tools WHERE tools_id = '".$this->input->post('pid')."'");
        $this->get_all_tools();
    }
   
}
/* End of Stocks  */