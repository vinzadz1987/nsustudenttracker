<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Util extends CI_Controller {
    var $user_session;
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        
        if(!$this->user_session=$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_prs": $this->get_all_prs();break;
            case "all_srs": $this->get_all_srs();break;
            default: show_404();
        }
    }
    
    public function get_all_prs($offset=0) {
       $user_id =  $this->user_session['id'];
   
       if($this->input->post('like')!="") {
        $prs = $this->db->query("SELECT pd.*, p.* 
                                  FROM prs_details pd, prs p
                                  WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} AND pd.description LIKE '".$this->input->post('like')."%' ORDER BY pd.description ASC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(pd.prs_details_id) AS rows FROM prs_details pd, prs p 
                                    WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} AND pd.description LIKE '".$this->input->post('like')."%'");
       }else {
        $prs = $this->db->query("SELECT pd.*, p.* 
                                     FROM prs_details pd, prs p 
                                     WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id} ORDER BY pd.description ASC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(pd.prs_details_id) AS rows FROM prs_details pd, prs p
                                     WHERE pd.prs_id=p.prs_id AND p.requested_by={$user_id}");
       }
        $config['base_url']= base_url().'ops/util/get_all_prs/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $prs->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
     
        $data['res'] = "";
        if($prs->num_rows()>0) {
            $data['res'] .= ' <div class="content-header menucenter">PRS</div>
                             <div class="stocks-header1 skyblue-gradient white-shadow">
                                <div><label>Description</label></div>
                                <div><label>Quantity</label></div>
                                <div><label>Price</label></div>
                                <div><label>Status</label></div>
                             </div>
                            <div class="stocks_list_container1">';
                 
            foreach($prs->result() as $row) {
             
            $data['res'] .='<div class="stocks-contents1">
                          <div>
                            <label> '.$row->description.'</label>
                          </div>
                          <div>
                            <label> '.$row->quantity.' '.$row->units.'</label>
                          </div>
                          <div>
                            <label> '.$row->unit_price.' </label>
                          </div>
                          <div>
                            <label>'.$row->dept_head_appvl.'</label>
                          </div>
                          </div>
                        ';
            }
           
        }
         
        echo json_encode($data);
    }
    
 public function get_all_srs($offset=0) {
       $user_id =  $this->user_session['id'];
       
       if($this->input->post('like')!=""){
          $srs = $this->db->query("SELECT s.id, st.description AS util_desc,
                                        s.qty, s.requested_by, s.head_appr
                                     FROM srs s, stocks st
                                     WHERE s.stock_id=st.stock_id AND s.requested_by={$user_id} AND st.description LIKE '".$this->input->post('like')."%' LIMIT 10 OFFSET ".$offset);
          $_count = $this->db->query("SELECT COUNT(s.id) AS rows
                                     FROM srs s, stocks st
                                     WHERE s.stock_id=st.stock_id AND s.requested_by={$user_id} AND st.description LIKE '".$this->input->post('like')."%'");
        
       } else {
          $srs = $this->db->query("SELECT s.id, st.description AS util_desc,
                                        s.qty, s.requested_by, s.head_appr
                                     FROM srs s, stocks st
                                     WHERE s.stock_id=st.stock_id AND s.requested_by={$user_id} LIMIT 10 OFFSET ".$offset);
          $_count = $this->db->query("SELECT COUNT(s.id) AS rows
                                     FROM srs s, stocks st
                                     WHERE s.stock_id=st.stock_id AND s.requested_by={$user_id}");
        
       }
      
        $config['base_url']= base_url().'ops/util/get_all_srs/';
        $config['total_rows']= $_count->row()->rows;    
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $srs->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        
        $data['res'] = "";
        if($srs->num_rows()>0) {
            $data['res'] .= '<div class="content-header menucenter"> SRS </div>
                            <div class="stocks-header1 skyblue-gradient white-shadow">
                                <div><label>Description</label></div>
                                <div><label>Quantity</label></div>
                                <div><label>Status</label></div>
                              </div>
                            <div class="stocks_list_container1">';
                 
            foreach($srs->result() as $row) {
             
            $data['res'] .='<div class="stocks-contents1">
                          <div>
                            <label> '.$row->util_desc.'</label>
                          </div>
                          <div>
                            <label>'.$row->qty.'</labe>
                          </div>
                          <div>
                            <label>'.$row->head_appr.'</label>
                          </div>
                          </div>
                        ';
            }
           
        }
         
        echo json_encode($data);
    }
}  
/* End of Maintenance PRS  */