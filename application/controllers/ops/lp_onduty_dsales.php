<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Lp_onduty_dsales extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_items": $this->get_all_items();break;
            case "all_sales": $this->get_sales();break;
            case "add_item_sales": $this->_add_item_sales();break;
            case "get_details": $this->_item_details();break;
            case "delete_items": $this->_delete_items();break;
            default: show_404();
        }
    }
    
    public function get_all_items($offset=0) {
        
   
       if($this->input->post('like')!="") {
        $items = $this->db->query("SELECT lpl.id, lpl.description, lpl.price  
                                  FROM lp_price_list lpl
                                  WHERE lpl.description LIKE '".$this->input->post('like')."%' ORDER BY lpl.description ASC LIMIT 30 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(lpl.id) AS rows FROM lp_price_list lpl
                                    WHERE lpl.description LIKE '".$this->input->post('like')."%'");
       }else {
        $items = $this->db->query("SELECT lpl.id, lpl.description, lpl.price
                                 FROM lp_price_list lpl
                                  ORDER BY lpl.description ASC LIMIT 30 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(lpl.id) AS rows FROM lp_price_list lpl");
       }
        $config['base_url']= base_url().'ops/lp_onduty_dsales/get_all_items/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 30;
        $this->pagination->initialize($config);
        $data['num_rows']= $items->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
     
        $data['res'] = "";
        if($items->num_rows()<=0) {
            $data['res'] .= '
                            <div class="shadowbox menucenter ">Match failed.</div>';
        }else{
        if($items->num_rows()>0) {
            $data['res'] .= '';
                 
            foreach($items->result() as $row) {
                
             
            $data['res'] .='<div class="shadowbox">
                          <div class="sw-text i item empty" id="st_'.$row->id.'" title="Item Name">  
                            <label class="item">'.$row->description.'</label>
                            <label class="sw-text2 to-right">Price: '.$row->price.'</label>
                          </div>';
            }
            
           
        }
        }
         
        echo json_encode($data);
    }
    
     public function get_sales() {
        $now = date('m-d-Y');
        $items = $this->db->query("SELECT * FROM lp_dsales WHERE date_added = '".$now."' ORDER BY description ASC");
        $items_tsales = $this->db->query("SELECT SUM(total_sales) AS total_sales, date_added FROM lp_dsales WHERE date_added = '".$now."'");
        $cei = $this->db->query("SELECT description, price, COUNT(description) AS cd 
                                 FROM lp_dsales WHERE date_added = '".$now."' GROUP BY description");
        $data['res'] = "";
        if($items->num_rows()<=0) {
            $data['res'] .= '<div class="shadowbox menucenter">Select Sales Item.</div>';
        }else{
        if($items->num_rows()>0 || $cei->num_rows()>0) {
            $data['res'] .= '';
                    
            foreach($items->result() as $row) {
             
            $data['res'] .='<div class="shadowbox">
                          <div class="st-text1" id="st_'.$row->dsales_id.'">
                            <table width="400">
                                <tr>
                            <td>'.$row->description.'</td>
                            <td><label class="cancel-item blue-button to-right" id="st_'.$row->dsales_id.'" title="Cancel Item '.$row->description.'">x</label></td>    
                                </tr>
                            </table>
                          </div>
                          </div>';
            }
            foreach($cei->result() as $rowcei) {
               $data['res'] .='<table width="400px">
                                <tr> 
                                    <td class="s_i">'.$rowcei->cd.' '.$rowcei->description.' item sold. Price '.$rowcei->price.'</td>
                                </tr>
                               </table>'; 
            }
        }
           
            $data['res'] .= '  <div class="shadowbox">
                                <div class="st-text1 countbox">
                                    <label class="menucenter">TOTAL SALES</label>
                                    <label class="to-right menucenter">'.$items_tsales->row()->total_sales.'</label><button class="blue-button" id="sales_'.$items_tsales->row()->date_added.'">Generate Reports</button><button class="blue-button view-sales-or" id="sales_'.$items_tsales->row()->date_added.'">Generate OR</button>    
                                </div>
                              </div>';
       
        }   
         
        echo json_encode($data);
         $data['res'] .= '';
    }
    
    
     private function _add_item_sales() {
        $received = $this->input->post('data');
        $now = date('m-d-Y');
        $values = array(
            
            'description'=>$received['item'],
            'price'=>$received['price'],
            'total_sales'=>$received['total_sales'],
            'date_added'=>$now
              
        );
           if($this->db->insert('lp_dsales',$values)) {
            echo "added";
     } 
     }
    private function _item_details() {
           $item = $this->db->query("SELECT * FROM lp_price_list WHERE id ='".$this->input->post('bid')."'");
           $data['item'] = ($item) ? $item->row(): "Not found";
           $dsales = $this->db->query("SELECT dsales_id, date_added, SUM(total_sales) AS ts FROM lp_dsales WHERE date_added ='".$this->input->post('bid')."' ORDER BY date_added");
           $data['dsales'] = ($dsales) ? $dsales->row(): "Not found";
        echo json_encode($data);
    } 
    
     private function _delete_items() {
        $sql = $this->db->query("DELETE FROM lp_dsales WHERE dsales_id='".$this->input->post('pid')."'");
        $this->get_sales();
    }
   
}
/* End of on duty sales report  */