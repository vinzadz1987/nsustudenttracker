<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Forms extends CI_Controller {
	var $user_session;
	public function __construct() {
		parent::__construct();
		$this->user_session = $this->session->userdata('emp_info');
		if( empty( $this->user_session ) ) 
			redirect('login');
	}
	/* $status (ex. pending, approved) */
	public function retrieve( $items = null, $status = null, $offset = 0 ) {
		//if( !$this->input->post("ajax_request") || $this->uri->segment(3) !== 'leave-summary' ) show_404();
		$user_id =  $this->user_session['id'];
		switch( $items ) {
			case "leave-form-info":
				#name
				#department
				#leave balance
				$sql = "
					SELECT epd.firstname AS user_firstname, epd.lastname AS user_lastname
					FROM emp_personal_details AS epd
					WHERE epd.emp_id = {$user_id} ";
				
				$query = $this->db->query($sql)->row_array();
				$data = array_merge(leaveDaysLeft($user_id), usersDepartmentAndDeptHead($user_id), $query);
				$this->output->set_output(json_encode($data));
				
				break;
			case "leave-list":
				#emp_leaves
				
				$per_page = 15; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("SELECT * FROM emp_leaves WHERE emp_id = {$user_id} ")->num_rows();
				$config = array(
						'base_url'		=> base_url().'forms/retrieve/leave-list/page/',
						'total_rows'	=> $total_rows,
						'per_page'		=> $per_page,
						'uri_segment'	=> 5
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT leave_id AS leaveID, FROM_UNIXTIME(date_requested, '%m/%d/%Y') AS dateRequest, leave_type as leaveType, reason,
					time_requested AS timeRequested, pay_type as payType, FROM_UNIXTIME(leave_start, '%m/%d/%Y') AS leaveStart, FROM_UNIXTIME(leave_end, '%m/%d/%Y') AS leaveEnd, 
					imm_head_appvl AS dept_head_approval, hr_appvl AS hr_approval
					FROM emp_leaves
					WHERE emp_id = {$user_id}
					ORDER BY date_requested DESC 
					LIMIT {$offset}, {$per_page}";
				
				$user_sql = "
					SELECT epd.firstname AS user_firstname, epd.lastname AS user_lastname
					FROM emp_personal_details AS epd
					WHERE epd.emp_id = {$user_id}";
				
				$user_query = $this->db->query($user_sql)->row_array();
				$query = $this->db->query($sql)->result_array();
				$this->output->set_output( json_encode( 
					array_merge(
						$user_query, 
						leaveDaysLeft($user_id), 
						usersDepartmentAndDeptHead($user_id), 
						array("leave_listdata"	=> $query), 
						array( "pagination"		=> $pagination )
					)
				));
			break;
			case "srs-list":
				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("SELECT * FROM srs WHERE requested_by = {$user_id}")->num_rows();
				$config = array(
						'base_url'		=> base_url()."forms/retrieve/srs-list/page/",
						'total_rows'	=> $total_rows,
						'per_page'		=> $per_page,
						'uri_segment'	=> 5
				);
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT id AS srs_id, FROM_UNIXTIME(date_requested, '%m/%d/%Y') AS date_requested, purpose, head_appr 
					FROM srs
					WHERE requested_by = {$user_id}
					ORDER BY id DESC LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output( json_encode(
					array(
						"srs_forms" => $this->db->query($sql)->result_array(),
						"pagination" => $pagination,
						"users_name" => nameAndDepartment($user_id)
					)
				));
			break;
			case "leave-summary":
				# nameAndDepartment() and leaveDaysLeft() are helpers function
				$users_name_and_department = nameAndDepartment($user_id);
				$users_remaning_leave = leaveDaysLeft($user_id);
				
				$with_pay = leave_summary('1', $user_id);
				$without_pay = leave_summary('0', $user_id);
				
				$this->output->set_output(json_encode(
					array_merge(
						$users_name_and_department, 
						$users_remaning_leave, 
						array('with_pay'=> $with_pay, 'without_pay' => $without_pay)
					)
				));				
			break;
			case "srs-form-info":
				# nameAndDepartment() is a helper function
				$this->output->set_output(json_encode(nameAndDepartment($user_id)));
			break;
			case "unitsofmeasure":
				$result = array();
				$sql = "SELECT * FROM unitsof_measure";
				$query = $this->db->query($sql)->result_array();
				foreach( $query as $val ) $result[] = $val["name"];
				$this->output->set_output(json_encode( array("options" => $result) ));
			break;
			case "stock-items":
				$sql = "
					SELECT s.stock_id AS id, s.name AS stock_name, s.description, s.aqui_price AS unit_price, 
					qty_inhand AS stock_inhand, uom.name AS unitsof_measure 
					FROM stocks AS s
					LEFT JOIN unitsof_measure AS uom ON uom.id = s.units";
				$this->output->set_output( json_encode( $this->db->query($sql)->result_array() ) );
			break;
			
			case "prs-form":
				$this->output->set_output(json_encode(nameAndDepartment( $user_id )));
			break;
			case "prs-list":
				# prs
				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("SELECT * FROM prs WHERE requested_by = {$user_id} ")->num_rows();
				$config = array(
						'base_url'		=> base_url().'forms/retrieve/prs-list/page/',
						'total_rows'	=> $total_rows,
						'per_page'		=> $per_page,
						'uri_segment'	=> 5
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT prs_id, FROM_UNIXTIME(date_requested, '%m/%d/%Y') AS date_requested, FROM_UNIXTIME(date_required, '%m/%d/%Y') AS date_required,
					type AS prs_type, dept_head_appvl, fin_head_appvl
					FROM prs
					WHERE requested_by = {$user_id}
					LIMIT {$offset}, {$per_page}";
				
				$query = $this->db->query($sql)->result_array();
				$this->output->set_output( json_encode(
					array_merge(
						array( "prs_list"	=> $query ),
						array( "pagination"	=> $pagination )
					)
				));
			break;
		}
	}
	public function submit( $items = null ) {
		//if( !$this->input->post("ajax_request") ) show_404();
		$user_id =  $this->user_session['id'];
		switch($items) {
			case "leave-form":
				#emp_leaves
				$request_date = make_timestamp($this->input->post("date_requested"), "mm/dd/yyyy");
				$start = make_timestamp($this->input->post("firstday_leave"), "mm/dd/yyyy");
				$end = make_timestamp($this->input->post("lastday_leave"), "mm/dd/yyyy");
				$return_date = make_timestamp($this->input->post("return_date"), "mm/dd/yyyy");
				$leaves = array(
					'date_requested' => $request_date,
					'leave_type' => $this->input->post("leave_type"),
					'reason' => $this->input->post("leave_reason"),
					'time_requested' => $this->input->post("leave_type_request"),
					'pay_type' => $this->input->post("leave_paytype"),
					'leave_start' => $start,
					'leave_end' => $end,
					'return_date' => $return_date
				);
				$this->db->insert("emp_leaves", array_merge($leaves, array("emp_id" => $user_id)));
			break;
			case "srs-form":
				#srs
				$user_id =  $this->user_session['id'];
				//$post_items = json_decode( file_get_contents("php://input") );
				$post_items = json_decode($this->input->post("model"));
				
				$srs_purpose = $post_items->srs_purpose;
				$date_required = make_timestamp($post_items->srs_date_required, "mm/dd/yyyy");
				$srs_items = json_decode($post_items->srs_items);
				
				foreach( $srs_items as $key => $val ) {
					$data = array(
						"stock_id"	=> $key,
						"qty"		=> $val,
						"requested_by" => $user_id,
						"purpose"	=> $srs_purpose,
						"date_required" => $date_required,
						"date_requested" => time()
					);
					$this->db->insert("srs", $data);
				}
			break;
			case "prs-form":
				#prs
				#prs_details
				#$post_items = json_decode(file_get_contents("php://input"));
				//print_r($_POST);exit();
				$post_items = json_decode($this->input->post("model"));
				$date_required = make_timestamp($post_items->prs_daterequired, "mm/dd/yyyy");
				
				foreach( $post_items->prs_items as $val ) {
					$prs_data = array(
						"requested_by"	=> $user_id,
						"date_requested"=> time(),
						"date_required" => $date_required,
						"supplier_name" => $post_items->prs_supplier,
						"type"			=> $post_items->prs_type,
						"payment_type" 	=> $post_items->prs_payment_type
					);
					$this->db->insert("prs", $prs_data);
					
					$prs_details = array(
						"prs_id" 		=> $this->db->insert_id(),
						"description"	=> $val->prs_description,
						"quantity"		=> $val->prs_qty,
						"units"			=> $val->prs_uom,
						"unit_price"	=> $val->prs_price,
						"amount"		=> $val->prs_amt
					);
					$this->db->insert("prs_details", $prs_details);
				}
			break;
		}
	}
	public function search( $items = null, $from = 0, $to = 0, $status = null, $offset = 0  ) {
		//if( !$this->input->post("ajax_request") ) show_404();
		$user_id =  $this->user_session['id'];
		switch( $items ) {
			case "prs-list":
				# prs
				
				if( $from === 0 && $to === 0 ) {
					$from = make_timestamp($this->input->post('period_from'), "mm/dd/yyyy");
					$to = make_timestamp($this->input->post('period_to'), "mm/dd/yyyy");
					$status = strtolower($this->input->post('status'));
				}
				
				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("SELECT * FROM prs WHERE requested_by = {$user_id} AND (dept_head_appvl = '{$status}' OR fin_head_appvl = '{$status}' )")->num_rows();
				$config = array(
						'base_url'		=> base_url()."forms/search/prs-list/{$from}/{$to}/{$status}/",
						'total_rows'	=> $total_rows,
						'per_page'		=> $per_page,
						'uri_segment'	=> 7
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT prs_id, FROM_UNIXTIME(date_requested, '%m/%d/%Y') AS date_requested, FROM_UNIXTIME(date_required, '%m/%d/%Y') AS date_required,
					type AS prs_type, dept_head_appvl, fin_head_appvl
					FROM prs
					WHERE requested_by = {$user_id} AND (dept_head_appvl = '{$status}' OR fin_head_appvl = '{$status}' ) AND ( date_requested >= {$from} AND date_requested <= {$to} )
					LIMIT {$offset}, {$per_page}";
				
				$query = $this->db->query($sql)->result_array();
				$this->output->set_output( json_encode(
						array_merge(
								array( "prs_list"	=> $query ),
								array( "pagination"	=> $pagination )
						)
				));
			break;
			case "srs-list":
				if( $from === 0 && $to === 0 ) {
					$from = make_timestamp($this->input->post('period_from'), "mm/dd/yyyy");
					$to = make_timestamp($this->input->post('period_to'), "mm/dd/yyyy");
					$status = strtolower($this->input->post('status'));
				}

				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);				
				
				$total_rows = $this->db->query("SELECT * FROM srs WHERE requested_by = {$user_id} AND head_appr = '{$status}'")->num_rows();
				$config = array(
						'base_url'		=> base_url()."forms/search/srs-list/{$from}/{$to}/{$status}/",
						'total_rows'	=> $total_rows,
						'per_page'		=> $per_page,
						'uri_segment'	=> 7
				);
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT id AS srs_id, FROM_UNIXTIME(date_requested, '%m/%d/%Y') AS date_requested,
					purpose, head_appr FROM srs
					WHERE requested_by = {$user_id} AND (head_appr = '{$status}') AND (date_requested >= {$from} AND date_requested <= {$to} )
					ORDER BY id DESC LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output( json_encode(
					array(
						"srs_forms" => $this->db->query($sql)->result_array(),
						"pagination" => $pagination,
						"users_name" => nameAndDepartment($user_id)
					)
				));
			break;
			case "leave-list-status":

				if( $from === 0 && $to === 0 ) {
					$from = make_timestamp($this->input->post('period_from'), "mm/dd/yyyy");
					$to = make_timestamp($this->input->post('period_to'), "mm/dd/yyyy");
					$status = strtolower($this->input->post('status'));
				}

				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("SELECT * FROM emp_leaves WHERE emp_id = {$user_id} AND ( imm_head_appvl = '{$status}' OR hr_appvl = '{$status}' )")->num_rows();
				$config = array(
					'base_url'		=> base_url()."forms/search/leave-list-status/{$from}/{$to}/".strtolower($status)."/",
					'total_rows'	=> $total_rows,
					'per_page'		=> $per_page,
					'uri_segment'	=> 7
				);
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				# query using per_page and offset
				
				#emp_leaves
				$sql = "
					SELECT leave_id AS leaveID, FROM_UNIXTIME(date_requested, '%m/%d/%Y') AS dateRequest, leave_type as leaveType, reason,
					time_requested AS timeRequested, pay_type as payType, FROM_UNIXTIME(leave_start, '%m/%d/%Y') AS leaveStart, FROM_UNIXTIME(leave_end, '%m/%d/%Y') AS leaveEnd, 
					imm_head_appvl AS dept_head_approval, hr_appvl AS hr_approval
					FROM emp_leaves
					WHERE emp_id = {$user_id} AND ( imm_head_appvl = '{$status}' OR hr_appvl = '{$status}' ) AND (date_requested >= {$from} AND date_requested <= {$to} )
					ORDER BY date_requested DESC
					LIMIT {$offset}, {$per_page} ";

				$user_sql = "
					SELECT epd.firstname AS user_firstname, epd.lastname AS user_lastname
					FROM emp_personal_details AS epd
					WHERE epd.emp_id = {$user_id} ";
				
				$user_query = $this->db->query($user_sql)->row_array();
				$query = $this->db->query($sql)->result_array();
				$this->output->set_output( json_encode(
					array_merge(
						$user_query, 
						leaveDaysLeft($user_id), 
						usersDepartmentAndDeptHead($user_id), 
						array("leave_listdata" => $query, "pagination" => $pagination)
					)
				));
			break;
		}
	}
	public function cancel( $items = null ) {
		$user_id =  $this->user_session['id'];
		#$post_items = json_decode(file_get_contents("php://input"));
		$post_items = json_decode($this->input->post('model'));
		if( !$post_items->ajax_request ) show_404();
		switch( $items ) {
			case "leave":
				$sql = "
					UPDATE emp_leaves SET imm_head_appvl = 'cancelled', hr_appvl = 'cancelled'
					WHERE leave_id = {$post_items->id} AND emp_id = {$user_id} ";
				$this->db->query($sql);
			break;
			case "srs":
				$data = array("head_appr" => "cancelled");
				$this->db->update("srs", $data, array("id" => $post_items->id, "requested_by" => $user_id));
			break;
			case "prs":
				$data = array("dept_head_appvl" => "cancelled", "fin_head_appvl" => "cancelled");
				$this->db->update("prs", $data, array("prs_id" => $post_items->id, "requested_by" => $user_id));
			break;
		}
	}
}