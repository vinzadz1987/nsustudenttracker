<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Task_collector extends CI_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('emp_info')) {
            redirect('login');
        }
    }
    function index() {
        echo $this->_compile();
    }
    function _compile() {
        $sql = $this->db->query("SELECT ta.id, tl.* FROM tasks ta, task_master_list tl 
            WHERE ta.emp_id='".$this->session->userdata['emp_info']['id']."' AND tl.task_id=ta.task_id");
        if($sql->num_rows()>0) {
            $res = json_encode($sql->result());
        } else {
            $res = "No task assigned";
        }
        return $res;
    }
}
/* End of task_collector.php*/