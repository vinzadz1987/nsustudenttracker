<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Profile extends CI_Controller {
	var $user_session;
	public function __construct() {
		parent::__construct();
		$this->user_session = $this->session->userdata('emp_info');
                $this->load->helper(array('form', 'url'));
                $filename = $this->input->post('filename');
                if(empty($filename)){
                    if( empty( $this->user_session ) ) 
                            redirect('login');
                } else {
                    
                }
                
	}
	function index(){
        
            switch ($this->input->post('dir')) {
                case "get_id": $this->_get_id(); break;
                case "get_state": $this->_get_state();break;
                case "upload_file": $this-> do_upload(); break;
                
                default: show_404();
            }
        }
        private function _get_id(){
            $user_id =  $this->user_session['id'];
            $default = base_url()."res/img/photos/default.jpg";
            $client = base_url()."res/img/photos/$user_id.jpg";
            $img = '';
           $info = @getimagesize($client);
           if(isset($info['mime'])){
                $img = $client;
            } else {
                $img = $default;
            }
            $data['img'] = $img;   
            $data['res'] = $user_id;
            echo json_encode($data);
        }
        public function _get_state(){
           
            $sql ="SELECT * FROM real_state_info ORDER BY RAND() LIMIT 1";
            
            $statephotos =  $this->db->query($sql);
            $data['res']='';$data['res2']='';
            if($statephotos->num_rows()>0){ 
                foreach($statephotos->result() as $row){
                   
                    $data['res'].='
                                    <table>
                                        <tr>
                                            <td>
                                            <img src="res/img/statephotos/'.$row->pic2.'" class="homephotos"/>
                                            <div class="maintitle">'.ucfirst($row->title).'</div>
                                            </td>
                                        </tr>
                                    </table> 
                            ';
                    
                }
            }
            $data['slide'] = ($statephotos) ? $statephotos->row(): "Not found";
            echo json_encode($data);
        }
        function do_upload()
	{
            try{

             $filename = $this->input->post('filename',TRUE)  ;
             $config['upload_path'] = './res/img/photos/';
             $config['allowed_types'] = 'jpg|png|gif|jpeg';
             $config['file_name'] = $filename;
             $config['overwrite'] = TRUE;
             $config['max_size']	= '15000';
             $config['max_width']  = '5500';
             $config['max_height']  = '5500';


             $this->load->library('upload');
             $this->upload->initialize($config);


             if(!$this->upload->do_upload())
                throw new Exception;
                $image_data = $this->upload->data();
                
                
                $config['image_library'] = 'gd2';
                $config['source_image'] = $image_data ['full_path'];
                $config['new_image'] = './res/img/photos/'.$image_data ['raw_name'].'.jpg';
                $config['overwrite'] = TRUE;    
                //$config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = FALSE; 
                $config['width'] = 140;
                $config['height'] = 130;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();
                if($image_data ['file_ext'] != ".jpg"){
                    $file_dir = './res/img/photos/'.$image_data ['raw_name'].''.$image_data ['file_ext'];
                    $deleted_file = unlink($file_dir);
                }
                

             redirect('user/');
         }
         catch(Exception $e)
         {
          echo $this->upload->display_errors();
         }
         
	}
       /* public function do_upload(){

            $file = $this->input->post('filename');
            $config['upload_path'] = './res/img/photos/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $this->input->post('userfile',TRUE);
            $config['overwrite'] = TRUE;
            $config['max_size'] = '2000';
            $config['max_width']  = '1500';
            $config['max_height']  = '1500';
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload()){
                
                $error = array('error' => $this->upload->display_errors());
                $data['res'] = $error;
            } else {
                $data = array('upload_data' => $this->upload->data());
                $data['res'] = $data;
            }
            echo json_encode($data);
            
        }
        function do_upload()
	{
                $filename = $this->input->post('filename');
                $file = $this->input->post('userfile');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($file))
		{
			$error = array('error' => $this->upload->display_errors());

                        $data['res'] = $error;
			//$this->load->view('upload_form', $error);
                      
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$this->load->view('upload_success', $data);
		}
                  echo json_encode($data);
	}*/
        public function update( $params = null ) {
		if( !$this->input->post("ajax_request") ) show_404();
		#if( $this->input->post("user_id") !== $user_id ) show_404();
		$user_id =  $this->user_session['id'];
		switch( $params ) {
			case "personal-details":
				#emp_contrib_details
				#emp_personal_details
				#$this->db->update("emp_personal_details");
				#$this->db->affected_rows();
				$bdate = make_timestamp($this->input->post("pd_bdate"), "mm/dd/yyyy");
				$personal_details = array(
					"id_number"      => $this->input->post("pd_id_number"),
                                        "firstname"      => $this->input->post("firstname"),
                                        "middlename"     => $this->input->post("middlename"),
                                        "lastname"       => $this->input->post("lastname"),
					"gender"	 => $this->input->post("pd_gender"),
					"birthdate"      => $bdate,
					"nationality"    => $this->input->post("pd_nationality"),
					"status"	 => $this->input->post("pd_marital_status")
				);
				$contrib_details = array(
					"sss" => $this->input->post("pd_sss_number"),
					"tin" => $this->input->post("pd_tin_number"),
					"phic" => $this->input->post("pd_phic"),
					"hdmf" => $this->input->post("pd_hdmf")
				);
				$this->db->update("emp_personal_details", $personal_details, array("emp_id" => $user_id));
				#make sure that we have datas to be updated
				$query = $this->db->query("SELECT * FROM emp_contrib_details WHERE emp_id={$user_id}");
				( $this->db->affected_rows() && ($query->num_rows() > 0) ) ? 
					$this->db->update("emp_contrib_details", $contrib_details, array("emp_id" => $user_id)) :
						#otherwise insert the data
						$this->db->insert("emp_contrib_details", array_merge($contrib_details, array("emp_id" => $user_id)));
			break;
			case "contact-details":
				#emp_contact_details
				$contact_details = array(
					'country'	=> $this->input->post("cd_country"),
					'street'	=> $this->input->post("cd_street"),
					'city'		=> $this->input->post("cd_city"),
					'province'	=> $this->input->post("cd_province"),
					'zipcode'	=> $this->input->post("cd_zip_code"),
					'telno'		=> $this->input->post("cd_telephone"),
                                        'mobileno'      => $this->input->post("cd_mobile_number"),
					'email'		=> $this->input->post("cd_email")
				);
				$query = $this->db->query("SELECT * FROM emp_contact_details WHERE emp_id={$user_id}");
				$query->num_rows() > 0 ? 
					$this->db->update("emp_contact_details", $contact_details, array("emp_id" => $user_id)) : 
						$this->db->insert("emp_contact_details", array_merge($contact_details, array("emp_id" => $user_id)));
			break;
			
		}
	}
	public function retrieve( $params = null ) {
		if( !$this->input->post("ajax_request") ) show_404();
		$user_id =  $this->user_session['id'];
		switch( $params ) {
			case "information":
				$sql = "
					SELECT epd.emp_id AS user_id, epd.id_number AS pd_id_number, epd.firstname, epd.middlename, epd.lastname, epd.nationality AS pd_nationality, FROM_UNIXTIME(epd.birthdate, '%m/%d/%Y') AS pd_bdate, epd.status AS pd_marital_status, epd.gender as pd_gender,
					e_contribd.sss AS pd_sss_number, e_contribd.tin AS pd_tin_number, e_contribd.phic AS pd_phic, e_contribd.hdmf AS pd_hdmf,
					e_contactd.country AS cd_country, e_contactd.street AS cd_street, e_contactd.province AS cd_province, e_contactd.city AS cd_city, e_contactd.zipcode AS cd_zip_code, e_contactd.telno AS cd_telephone, e_contactd.mobileno AS cd_mobile_number,  e_contactd.email AS cd_email,
					ec.name AS ec_name, ec.relationship AS ec_relationship, ec.telno AS ec_telephone, ec.mobileno AS ec_mobile, ec.address AS ec_address,
					im.type AS im_immigration, im.number AS im_visa_passport, im.status AS im_status, FROM_UNIXTIME(im.review_date, '%m/%d/%Y') AS im_review_date, im.citizenship AS im_citizenship, FROM_UNIXTIME(im.issued_date,'%m/%d/%Y') AS im_issued_date, FROM_UNIXTIME(im.expiry_date,'%m/%d/%Y') AS im_date_expiry
					
					FROM emp_personal_details AS epd 
					LEFT JOIN emp_contrib_details AS e_contribd USING( emp_id )
					LEFT JOIN emp_contact_details AS e_contactd USING( emp_id )
					LEFT JOIN emp_emergency_contact AS ec USING( emp_id )
					LEFT JOIN emp_immigration AS im USING( emp_id )
					WHERE epd.emp_id = {$user_id}
				";
				$query = $this->db->query($sql)->row_array();
				$this->output->set_output(json_encode($query));
			break;
			
			case "background":
				$userinfo_sql = "
					SELECT epd.id_number AS pd_id_number, epd.firstname, epd.lastname, 
					e_contactd.country AS cd_country, e_contactd.street AS cd_street, e_contactd.province AS cd_province, e_contactd.city AS cd_city, e_contactd.zipcode AS cd_zip_code, e_contactd.email AS cd_email
					
					FROM emp_personal_details as epd
					LEFT JOIN emp_contact_details AS e_contactd USING( emp_id )
					
					WHERE epd.emp_id = {$user_id}
				";
				
				$workexp_sql = "
					SELECT id AS workexp_id, employer AS comp, job_title AS job_title,
					CONCAT_WS(' - ', FROM_UNIXTIME(work_start, '%m/%d/%Y'), FROM_UNIXTIME(work_end, '%m/%d/%Y')) AS date_range
					FROM emp_work_experience 
					WHERE emp_id = {$user_id}
				";
				$education_sql = "
					SELECT id AS education_id, school AS school_name, course AS course,
					CONCAT_WS(' - ', FROM_UNIXTIME(year_start, '%m/%d/%Y'), FROM_UNIXTIME(year_end, '%m/%d/%Y')) AS year
					FROM emp_education 
					WHERE emp_id = {$user_id}
				";
				$skills_sql = "
					SELECT id AS skills_id, name AS title, num_years_exp AS year_exp 
					FROM emp_skills 
					WHERE emp_id = {$user_id}
				";
				$language_sql = "
					SELECT id AS language_id, name AS lang, fluency AS type, competency AS rate
					FROM emp_language 
					WHERE emp_id = {$user_id}
				";
				$license_sql = "
					SELECT id AS license_id, name AS name, type AS type,
					CONCAT_WS(' - ', FROM_UNIXTIME(start, '%m/%d/%Y'), FROM_UNIXTIME(end, '%m/%d/%Y')) AS expiry_date
					FROM emp_license 
					WHERE emp_id = {$user_id}
				";
				
				$userinfo_query = $this->db->query($userinfo_sql)->row_array();
				$workexp_query = $this->db->query($workexp_sql)->result_array();
				$education_query = $this->db->query($education_sql)->result_array();
				$skills_query = $this->db->query($skills_sql)->result_array();
				$language_query = $this->db->query($language_sql)->result_array();
				$license_query = $this->db->query($license_sql)->result_array();
				
				$results = array(
					'user_info'		=> $userinfo_query,
					'background'	=> array(
						'workexp'	=> $workexp_query,
						'education'	=> $education_query,
						'skills'	=> $skills_query,
						'language'	=> $language_query,
						'license'	=> $license_query
					)
				);
				
				$this->output->set_output(json_encode($results));
				
			break;
                    case "profile":
                                $user_id = 1;
				$sql = "SELECT student_no, lastname, firstname, FROM_UNIXTIME(birth_date, '%m/%d/%Y') AS birth_date, country, gender, civil_status, address, nationality, email, contact_no, passport_no FROM students WHERE id = {$user_id}";
				$query = $this->db->query($sql)->row_array();
				$this->output->set_output(json_encode($query));
			break;
		}
	}
}
/* End of file profile.php */