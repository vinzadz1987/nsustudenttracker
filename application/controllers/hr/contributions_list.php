<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Contributions_list extends CI_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "add_phic_entry": $this->_add_phic_contrib(); break;
            case "retrieve_phic_list": $this->_phic_list(); break;
            case "add_sss_entry": $this->_add_sss_contrib(); break;
            case "retrieve_sss_list": $this->_sss_list(); break;
            case "delete_phic_entry": $this->_delete_phic();  break;
            case "delete_sss_entry": $this->_delete_sss(); break;
        }
    }
    private function _phic_list() {
        $sql = $this->db->query("SELECT * FROM contrib_phic ORDER BY salary_base");
        $data['res'] = "";
        if($sql->result()>0) {
            $data['res'] .= '<div class="phic-header skyblue-gradient white-shadow">
                                <div><label>Monthly Salary Bracket</label></div>
                                <div><label>Monthly Salary Range</label></div>
                                <div><label>Salary Base</label></div>
                                <div><label>Total Monthly Contribution</label></div>
                                <div><label>Personal Share (PS=SBx1.25%)</label></div>
                                <div><label>Employer Share (ES=PS)</label></div>
                            </div>
                            <div class="phic_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .='<div class="phic-contents">
                                <div>'.$row->salary_bracket.'</div>
                                <div>'.number_format($row->range_min,2,'.',',').' - '.number_format($row->range_max,2,'.',',').'</div>
                                <div>'.number_format($row->salary_base,2,'.',',').'</div>
                                <div>'.number_format($row->total_monthly,2,'.',',').'</div>
                                <div>'.$row->personal_share.'</div>
                                <div>'.$row->employer_share.'</div>
                                <div class="options hide to-right sprite-close delete_phic_btn" id="phic_'.$row->contrib_id.'">
                                </div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _add_phic_contrib() {
        $rcv = $this->input->post('data');
        $values = array(
            "salary_bracket" => $rcv['bracket'],
            "range_min" => $rcv['range_min'],
            "range_max" => $rcv['range_max'],
            "salary_base" => $rcv['base'],
            "personal_share" => $rcv['personal_share'],
            "employer_share" => $rcv['employer_share'],
            "total_monthly" => $rcv['monthly_contrib'],
        );
        $data['res'] = ($this->db->insert("contrib_phic",$values)) ? true:false;
        echo json_encode($data);
    }
    private function _delete_phic() {
        $sql = $this->db->query("DELETE FROM contrib_phic WHERE contrib_id = '".$this->input->post('pid')."'");
        $this->_phic_list();
    }
    private function _add_sss_contrib() {
        $rcv = $this->input->post('data');
        $values = array(
            "range_min" => $rcv['range_min'],
            "range_max" => $rcv['range_max'],
            "employer" => $rcv['employer'],
            "employee" => $rcv['employee'],
            "ecer" => $rcv['ecer']
        );
        $data['res'] = ($this->db->insert("contrib_sss",$values)) ? true:false;
        echo json_encode($data);
    }
    private function _sss_list() {
        $sql = $this->db->query("SELECT contrib_id, range_min, range_max, employer, employee, ecer, 
            (employer + employee) AS total, (employer + employee + ecer) AS total_contrib FROM contrib_sss ORDER BY range_min");
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $data['res'] .= '<div class="sss-header skyblue-gradient white-shadow">
                                <div><label>Range of Compensation</label></div>
                                <div><label>ER</label></div>
                                <div><label>EE</label></div>
                                <div><label>Total</label></div>
                                <div><label>ECER</label></div>
                                <div><label>Total Contribution</label></div>
                            </div>
                            <div class="sss_list_container">';
            foreach($sql->result() as $row) {
                $data['res'] .= '<div class="sss-contents">
                                <div>'.number_format($row->range_min,2,'.',',').' - '.number_format($row->range_max,2,'.',',').'</div>
                                <div>'.number_format($row->employer,2,'.',',').'</div>
                                <div>'.number_format($row->employee,2,'.',',').'</div>
                                <div>'.number_format($row->total,2,'.',',').'</div>
                                <div>'.$row->ecer.'</div>
                                <div>'.number_format($row->total_contrib,2,'.',',').'</div>
                                <div class="options hide to-right sprite-close delete_sss_btn" id="sss_'.$row->contrib_id.'">
                                </div>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _delete_sss() {
        $sql = $this->db->query("DELETE FROM contrib_sss WHERE contrib_id = '".$this->input->post('sid')."'");
        $this->_sss_list();
    }
}
/*End of contributions_list.php */