<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Employees_list extends CI_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
        $this->load->helper('date');
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "retrieve_employees": $this->get_employees(); break;
            case "initialize_department_select": $this->_init_dept(); break;
            case "retrieve_emp_personal_details": $this->_get_emp_personal_details(); break;
            case "retrieve_emp_contact_details": $this->_get_emp_contact_details(); break;
            case "retrieve_emp_work_details": $this->_get_emp_work_details(); break;
            case "update_employee_personal_details": $this->_set_emp_personal_details(); break;
            case "update_employee_contact_details": $this->_set_emp_contact_details(); break;
            case "employee_work_experience": $this->_emp_work_experience_details(); break;
            case "remove_work_experience": $this->_del_work_experience(); break;
        }
    }
    private function _init_dept() {
        $departments = $this->db->query("SELECT deptd_id, dept_name FROM department_details");
        $data['departments'] = ($departments->num_rows()>0) ? $departments->result():"No department added";
        echo json_encode($data);
    }
    function get_employees($offset=0) {
        if($this->input->post('data')) {
            $rcv = $this->input->post('data');
            $dept = ($rcv['dept']!="") ? "AND d.deptd_id = '".$rcv['dept']."'":"";
            $sql = $this->db->query("SELECT epd.emp_id, CONCAT(epd.lastname,', ',epd.firstname,' ',epd.middlename) AS name, e.status 
                FROM emp_personal_details epd, emp_user e, department d 
                WHERE (epd.emp_id = d.emp_id AND e.emp_id = d.emp_id) ".$dept."  
                AND (epd.firstname LIKE '".$rcv['srch']."%' OR epd.middlename LIKE '".$rcv['srch']."%' OR epd.lastname LIKE '".$rcv['srch']."%') 
                LIMIT 2 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(epd.emp_id) as rows FROM emp_personal_details epd, emp_user e, department d 
                WHERE (epd.emp_id = d.emp_id AND e.emp_id = d.emp_id) ".$dept."  
                AND (epd.firstname LIKE '".$rcv['srch']."%' OR epd.middlename LIKE '".$rcv['srch']."%' OR epd.lastname LIKE '".$rcv['srch']."%')");
        } else {
            $sql = $this->db->query("SELECT epd.emp_id, CONCAT(epd.lastname,', ',epd.firstname,' ',epd.middlename) AS name, e.status 
                FROM emp_personal_details epd, emp_user e 
                WHERE epd.emp_id = e.emp_id LIMIT 20 OFFSET ".$offset);
            $_count = $this->db->query("SELECT COUNT(epd.emp_id) as rows FROM emp_personal_details epd, emp_user e 
                WHERE epd.emp_id = e.emp_id");
        }
        $config['base_url'] = base_url().'hr/employees_list/get_employees/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4;
        $config['per_page'] = 2;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['res'] = "";
        if($sql->num_rows()>0) {
            $i = 1;
            $data['res'] .= '<div class="employees_container">';
            foreach($sql->result() as $row) {
                $i = ($i==2) ? 1:2;
                $data['res'] .= '<div class="data-list-type-1 row'.$i.' emp_row" id="employee_'.$row->emp_id.'">
                                <div class="hide pale-skyblue-gradient emp_options to-right">
                                    <div class="sprite-container to-right edit-emp-personal-details-btn">
                                        <div class="sprite-edit-profile"></div>
                                    </div>
                                    <div class="sprite-container to-right edit-emp-contact-details-btn">
                                        <div class="sprite-edit-contact"></div>
                                    </div>
                                    <div class="sprite-container to-right edit-emp-background-btn">
                                        <div class="sprite-edit-work-exp"></div>
                                    </div>
                                </div>
                                <label class="value-cont-4 employee_name">'.$row->name.'</label>
                            </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
    private function _get_emp_personal_details() {
        $sql = $this->db->query("SELECT emp_personal_details.*, 
            emp_immigration.type, emp_immigration.status AS imm_status, emp_immigration.number, emp_immigration.review_date, emp_immigration.issued_date, emp_immigration.expiry_date, emp_immigration.citizenship, 
            emp_contrib_details.* FROM emp_personal_details 
            LEFT JOIN emp_contrib_details ON emp_personal_details.emp_id = emp_contrib_details.emp_id 
            LEFT JOIN emp_immigration ON emp_personal_details.emp_id = emp_immigration.emp_id 
            WHERE emp_personal_details.emp_id = '".$this->input->post('eid')."'");
        $data['id_number'] = $sql->row()->id_number;
        $data['fname'] = $sql->row()->firstname;
        $data['mname'] = $sql->row()->middlename;
        $data['lname'] = $sql->row()->lastname;
        $data['nickname'] = $sql->row()->nickname;
        $data['status'] = $sql->row()->status;
        $data['nationality'] = $sql->row()->nationality;
        $data['birthdate'] = date('Y-m-d',$sql->row()->birthdate);
        $data['gender'] = $sql->row()->gender;
        $data['sss'] = $sql->row()->sss;
        $data['tin'] = $sql->row()->tin;
        $data['phic'] = $sql->row()->phic;
        $data['hdmf'] = $sql->row()->hdmf;
        $data['immigration'] = $sql->row()->number;
        $data['imm_type'] = $sql->row()->type;
        $data['imm_status'] = $sql->row()->imm_status;
        $data['imm_citizenship'] = $sql->row()->citizenship;
        $data['imm_date_issued'] = ($sql->row()->issued_date!=null && $sql->row()->issued_date > 0) ? date('Y-m-d',$sql->row()->issued_date):"";
        $data['imm_expiration_date'] = ($sql->row()->expiry_date!=null && $sql->row()->expiry_date > 0) ? date('Y-m-d',$sql->row()->expiry_date):"";
        $data['imm_review_date'] = ($sql->row()->review_date!=null && $sql->row()->review_date > 0) ? date('Y-m-d',$sql->row()->review_date):"";
        echo json_encode($data);
    }
    private function _set_emp_personal_details() {
        $rcv = $this->input->post('data');
        $time = date('h:i:s A',now());
        $personal = array(
            "id_number"=>$rcv['id_number'],
            "firstname"=>$rcv['fname'],
            "middlename"=>$rcv['mname'],
            "lastname"=>$rcv['lname'],
            "nickname"=>$rcv['nickname'],
            "gender"=>$rcv['gender'],
            "birthdate"=>$rcv['birthdate'],
            "nationality"=>$rcv['nationality'],
            "status"=>$rcv['status']
        );
        $contribution = array(
            "sss"=>$rcv['sss'],
            "phic"=>$rcv['phic'],
            "tin"=>$rcv['tin'],
            "hdmf"=>$rcv['hdmf']
        );
        $immigration = array(
            "type"=>$rcv['imm_type'],
            "number"=>$rcv['immigration'],
            "status"=>$rcv['imm_status'],
            "issued_date"=>gmt_to_local(human_to_unix($rcv['imm_date_issued'].' '.$time),'UP7'),
            "expiry_date"=>gmt_to_local(human_to_unix($rcv['imm_expiration_date'].' '.$time),'UP7'),
            "review_date"=>gmt_to_local(human_to_unix($rcv['imm_review_date'].' '.$time),'UP7')
        );
        $personal_update = ($this->db->update('emp_personal_details',$personal,"emp_id = ".$this->input->post('eid'))) ? true:false;
        $contribution_update = ($this->db->update('emp_contrib_details',$contribution,"emp_id = ".$this->input->post('eid'))) ? true:false;
        $immigration_update = ($this->db->update('emp_immigration',$immigration,"emp_id = ".$this->input->post('eid'))) ? true:false;
        $data['result'] = ($personal_update == true && $contribution_update == true && $immigration_update == true) ? true:false;
        echo json_encode($data);
    }
    private function _get_emp_contact_details() {
        $sql = $this->db->query("SELECT emp_contact_details.*, 
            emp_emergency_contact.name AS ec_name,  emp_emergency_contact.relationship AS ec_relationship, emp_emergency_contact.telno AS ec_telephone, emp_emergency_contact.mobileno AS ec_mobile, emp_emergency_contact.address AS ec_address 
            FROM emp_contact_details 
            LEFT JOIN emp_emergency_contact ON emp_contact_details.emp_id = emp_emergency_contact.emp_id 
            WHERE emp_contact_details.emp_id = '".$this->input->post('eid')."'");
        $data['street'] = $sql->row()->street;
        $data['city'] = $sql->row()->city;
        $data['province'] = $sql->row()->city;
        $data['country'] = $sql->row()->country;
        $data['zip_code'] = $sql->row()->zipcode;
        $data['telephone'] = $sql->row()->telno;
        $data['mobile'] = $sql->row()->mobileno;
        $data['email'] = $sql->row()->email;
        $data['ec_name'] = $sql->row()->ec_name;
        $data['ec_relationship'] = $sql->row()->ec_relationship;
        $data['ec_telephone'] = $sql->row()->ec_telephone;
        $data['ec_mobile'] = $sql->row()->ec_mobile;
        $data['ec_address'] = $sql->row()->ec_address;
        echo json_encode($data);
    }
    private function _set_emp_contact_details() {
        $rcv = $this->input->post('data');
        $contact = array(
            "street"=>$rcv['street'],
            "city"=>$rcv['city'],
            "province"=>$rcv['province'],
            "country"=>$rcv['country'],
            "zipcode"=>$rcv['zip_code'],
            "telno"=>$rcv['telephone'],
            "mobileno"=>$rcv['mobile'],
            "email"=>$rcv['mobile']
        );
        $e_contact = array(
            "name"=>$rcv['ec_name'],
            "relationship"=>$rcv['ec_relationship'],
            "telno"=>$rcv['ec_telephone'],
            "mobileno"=>$rcv['ec_mobile'],
            "address"=>$rcv['ec_address'],
        );
        $contact_update = ($this->db->update('emp_contact_details',$contact,"emp_id = ".$this->input->post('eid'))) ? true:false;
        $e_contact_update = ($this->db->update('emp_emergency_contact',$e_contact,"emp_id = ".$this->input->post('eid'))) ? true:false;
        $data['result'] = ($contact_update == true && $e_contact_update == true) ? true:false;
        echo json_encode($data);
    }
    private function _get_emp_work_details() {
        $sql = $this->db->query("SELECT * FROM emp_work_experience  WHERE emp_id = '".$this->input->post('eid')."'");
        $data['res'] = '<div class="content-header">Work Background</div>
                        <form action="hr/employee_work_info" id="emp_background_details_frm">
                        <label class="header">Experience</label>
                        <input class="blue-button to-right" type="button" name="add_experience" value="Add" />
                        <div class="clr"></div>';
        if($sql->num_rows()>0) {
            foreach($sql->result() as $row) {
                $data['res'] .= '<div class="emp_work_exp" id="experience_'.$row->id.'">
                                    <div class="info_container">
                                        <input class="hr-field-type-1 entry_company" type="text" name="work-company_'.$row->id.'" value="'.$row->employer.'" />
                                        <label class="field">Company Name</label>
                                    </div>
                                    <div class="info_container">
                                        <input class="hr-field-type-1 entry_position" type="text" name="work-position_'.$row->id.'" value="'.$row->job_title.'" />
                                        <label class="field">Position</label>
                                    </div>
                                    <div class="info_container">
                                        <label>From:</label>
                                        <input class="hr-field-type-2 entry_covered-from" type="text" name="work-covered-from_'.$row->id.'" value="'.date('Y-m-d',$row->work_start).'" />
                                    </div>
                                    <div class="info_container">
                                        <label>To:</label>
                                        <input class="hr-field-type-2 entry_covered-to" type="text" name="work-covered-to_'.$row->id.'" value="'.date('Y-m-d',$row->work_end).'" />
                                    </div>
                                    <div class="clr"></div>
                                </div>';
            }
        } else {
            $data['res'] .= '<div class="emp_work_exp" id="new-entry_1">
                                    <div class="info_container">
                                        <input class="hr-field-type-1 entry_company" type="text" name="new-work-company_1" value="" />
                                        <label class="field">Company Name</label>
                                    </div>
                                    <div class="info_container">
                                        <input class="hr-field-type-1 entry_position" type="text" name="new-work-position_1" value="" />
                                        <label class="field">Position</label>
                                    </div>
                                    <div class="info_container">
                                        <label>From:</label>
                                        <input class="hr-field-type-2 entry_covered-from" type="text" name="new-work-covered-from_1" value="" />
                                    </div>
                                    <div class="info_container">
                                        <label>To:</label>
                                        <input class="hr-field-type-2 entry_covered-to" type="text" name="new-work-covered-to_1" value="" />
                                    </div>
                                    <div class="clr"></div>
                                </div>';
        }
        $data['res'] .= '<input class="blue-button to-right" type="submit" value="Update" />
                        <div class="clr"></div></form>';
        echo json_encode($data);
    }
    private function _emp_work_experience_details() {
        $rcv = $this->input->post('data');
        if($rcv['old_ones']!="null") {
            foreach($rcv['old_ones'] as $row) {
                $values = array(
                    "employer" => $row['company'],
                    "job_title" => $row['position'],
                    "work_start" => gmt_to_local(human_to_unix($row['covered-from']." 08:00:00 AM"),"UP7"),
                    "work_end" => gmt_to_local(human_to_unix($row['covered-to']." 08:00:00 AM"),"UP7")
                );
                $sql = $this->db->update("emp_work_experience",$values,"id = ".$row['exp_no']);
            }
        }
        if($rcv['new_ones']!="null") {
            foreach($rcv['new_ones'] as $row) {
                $values = array(
                    "emp_id" => $this->input->post('eid'),
                    "employer" => $row['company'],
                    "job_title" => $row['position'],
                    "work_start" => gmt_to_local(human_to_unix($row['covered-from']." 08:00:00 AM"),"UP7"),
                    "work_end" => gmt_to_local(human_to_unix($row['covered-to']." 08:00:00 AM"),"UP7")
                );
                $sql = $this->db->insert("emp_work_experience",$values);
            }
        }
        $this->_get_emp_work_details();
    }
    private function _del_work_experience() {
        $sql = $this->db->query("DELETE FROM emp_work_experience WHERE id = '".$this->input->post('exp_no')."'");
        $this->_get_emp_work_details();
    }
}
/* End of employees_list.php */     