<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Dsales_or extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        if(!$this->session->userdata('emp_info')) {
            show_404();
        }
    }
    function index() {
        switch ($this->input->post('dir')) {
            case "all_items": $this->get_all_items();break;
            case "all_sales": $this->get_sales();break;
            case "add_item_sales": $this->_add_item_sales();break;
            case "get_details": $this->_item_details();break;
            case "delete_items": $this->_delete_items();break;
            case "daily_sales": $this->get_sales_daily();break;
            default: show_404();
        }
    }
    
   
    
    public function get_all_items() {
        
   
     /*  if($this->input->post('like')!="") {
        $items = $this->db->query("SELECT lpl.id, lpl.description, lpl.price  
                                  FROM lp_price_list lpl
                                  WHERE lpl.description LIKE '".$this->input->post('like')."%' ORDER BY lpl.description ASC LIMIT 40 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(lpl.id) AS rows FROM lp_price_list lpl
                                    WHERE lpl.description LIKE '".$this->input->post('like')."%'");
       }else {*/
        $items = $this->db->query("SELECT lpl.id, lpl.description, lpl.price
                                 FROM lp_price_list lpl
                                  ORDER BY lpl.description");
        $_count = $this->db->query("SELECT COUNT(lpl.id) AS rows FROM lp_price_list lpl");
      /* }
        $config['base_url']= base_url().'hotel/dsales_or/get_all_items/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 40;
        $this->pagination->initialize($config);
        $data['num_rows']= $items->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
     */
        $data['res'] = "";
        if($items->num_rows()<=0) {
            $data['res'] .= '
                            <div class="shadowbox menucenter ">Match failed.</div>';
        }else{
        if($items->num_rows()>0) {
            $data['res'] .= '';
                 
            foreach($items->result() as $row) {
                
             
            $data['res'] .='<div class="shadowbox">
                          <div class="empty">  
                            <label class="item">'.$row->description.'</label>
                            <label class="sw-text2 to-right">Price: '.$row->price.'</label>
                          </div>';
            }
            
           
        }
        }
         
        echo json_encode($data);
    }
    
     public function get_sales() {
        $now = date('m-d-Y');
        $items = $this->db->query("SELECT * FROM lp_dsales WHERE date_added = '".$now."' ORDER BY description ASC");
        $items_tsales = $this->db->query("SELECT SUM(total_sales) AS total_sales FROM lp_dsales WHERE date_added = '".$now."'");
        $cei = $this->db->query("SELECT description, COUNT(description) AS cd 
                                 FROM lp_dsales WHERE date_added = '".$now."' GROUP BY description");
        $data['res'] = "";
        if($items->num_rows()<=0) {
            $data['res'] .= '<div class="shadowbox menucenter">No sales item as of '.$now.'</div>';
        }else{
        if($items->num_rows()>0 || $cei->num_rows()>0) {
            $data['res'] .= '';
                    
            foreach($items->result() as $row) {
             
            $data['res'] .='<div class="shadowbox">
                          <div class="st-text1" id="st_'.$row->dsales_id.'">
                            <table width="400">
                                <tr>
                                    <td>'.$row->description.'</td>
                              </tr>
                            </table>
                          </div>
                          </div>';
            }
            foreach($cei->result() as $rowcei) {
               $data['res'] .='<table width="400px">
                                <tr> 
                                    <td class="s_i">'.$rowcei->cd.' '.$rowcei->description.' item sold.</td>
                                </tr>
                               </table>'; 
            }
        }
           
            $data['res'] .= '  <div class="shadowbox">
                                <div class="st-text1 countbox">
                                    <label class="menucenter">NOW TOTAL SALES</label>
                                    <label class="to-right menucenter">'.$items_tsales->row()->total_sales.'</label>    
                                </div>
                              </div>';
       
        }   
         
        echo json_encode($data);
         $data['res'] .= '';
    }
    
    public function get_sales_daily($offset=0) {
       /* if($this->input->post('data')) {
            $rcv = $this->input->post('data');
            $now = date('m-d-y');
            $start = gmt_to_local(human_to_unix($rcv['from']." 00:00:00 AM"), 'UP7');
            $end = gmt_to_local(human_to_unix($rcv['to']." 11:59:59 PM"), 'UP7');
            $dsales = $this->db->query("SELECT dsales_id, date_added, SUM(total_sales) AS allsum FROM lp_dsales WHERE date_added !='".$now."' AND date_added > ".$start." AND date_added < ".$end." GROUP BY date_added DESC LIMIT 15 OFFSET ". $offset);  
            $count = $this->db->query("SELECT COUNT(dsales_id) AS rows, date_added FROM lp_dsales WHERE date_added !='".$now."' AND date_added > ".$start." AND date_added < ".$end." GROUP BY date_added DESC");
        }else{*/
        $now = date('m-d-Y');
        $dsales = $this->db->query("SELECT dsales_id, date_added, SUM(total_sales) AS allsum FROM lp_dsales WHERE date_added !='".$now."' GROUP BY date_added DESC LIMIT 15 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(dsales_id) AS rows FROM lp_dsales WHERE date_added !='".$now."' GROUP BY date_added DESC");
       /* } */
        $config['base_url'] = base_url().'hotel/dsales_or/get_sales_daily/';
        $config['total_rows'] = $_count->row()->rows;
        $config['uri_segment'] = 4; 
        $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['num_rows'] = $dsales->num_rows();
        $data['pagination'] = $this->pagination->create_links();
        
        $data['res'] = "";
        if($dsales->num_rows()>0){
            $data['res'] .='';
        
        foreach($dsales->result() as $row){
            $data['res'] .='<div class="countbox">The Sales of '.$row->date_added.' is Php '.$row->allsum.'<button class="blue-button view-or to-right" id="or_'.$row->date_added.'">Generate OR</button></div>';
        }  
    }
    
        echo json_encode($data);
    }
    
    private function _item_details() {
           $item = $this->db->query("SELECT * FROM lp_price_list WHERE id ='".$this->input->post('bid')."'");
           $data['item'] = ($item) ? $item->row(): "Not found";
           $dsales = $this->db->query("SELECT dsales_id, date_added, SUM(total_sales) AS ts FROM lp_dsales WHERE date_added ='".$this->input->post('bid')."' ORDER BY date_added");
           $data['dsales'] = ($dsales) ? $dsales->row(): "Not found";
        echo json_encode($data);
    } 
}
/* End of on duty sales report  */