<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Reports extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
		
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	
	public function retrieve( $item = null, $offset = 0 ) {
		switch($item) {
			case "daily-sales":
				
				$per_page = 15;
				$offset = intval($offset);
				
				$total_rows = $this->db->query("
					SELECT *
					FROM daily_sales_report AS dsr
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = dsr.filed_by
				")->num_rows();
				
				$config = array(
					'base_url' 		=> base_url().'hotel/reports/retrieve/daily-sales',
					'uri_segment' 	=> 5,
					'total_rows' 	=> $total_rows,
					'per_page'		=> $per_page
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT dsr.id, dsr.subject, dsr.contents, dsr.sales_type,
					FROM_UNIXTIME(dsr.date_filed, '%m/%d/%Y') AS date_filed,
					CONCAT_WS(' ', epd.firstname, epd.lastname) AS filed_by
					FROM daily_sales_report AS dsr
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = dsr.filed_by
					ORDER BY dsr.date_filed DESC
					LIMIT {$offset}, {$per_page}
				";
				$this->output->set_output(
					json_encode(
						array('dsr' => $this->db->query($sql)->result_array(), 'pagination' => $pagination)
					)
				);
			break;
		}
	}
	
	public function update( $item = null ) {
		$data_items = json_decode($this->input->post('model'));
		switch($item) {
			case "daily-sales":
				#daily_sales_report
				$sales_report = array(
					'subject'		=> $data_items->subject,
					'contents'		=> $data_items->content,
				);
				$this->db->update('daily_sales_report', $sales_report, array("id" => $data_items->id));
			break;
		}
	}
	
	/*
	 * save reports bogart ayaw cgeg samok2x nako...
	 */
	public function save( $item = null ) {
		
		$data_items = json_decode($this->input->post('model'));
		switch($item) {
			case "daily-sales":
				#daily_sales_report
				$sales_report = array(
					'subject'		=> $data_items->subject,
					'contents'		=> $data_items->content,
					'sales_type' 	=> 'hotel',
					'filed_by' 		=> $this->user_id,
					'date_filed' 	=> time()
				);
				$this->db->insert('daily_sales_report', $sales_report);
			break;
		}
	}
}