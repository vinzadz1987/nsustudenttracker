<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Accommodation extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
		
                
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	/*
	 * Retrieve information from the database 
	 * based on the given parameter
	 * 
	 * @params:
	 * 		$items - String
	 * 
	 * If you don't go after what you want, you'll never have it.. 
	 * And if you don't step forward, you're always be in the same place..
	 * Life really isn't fair but it is still good.
	 * 
	 * 
	 * Taking risk makes your heart beats faster and sets your mind on fire..
	 * 
	 */
        
        public function index() {
            switch ($this->input->post('dir')){
                case "all_guest": $this->get_all_guest();break;
                case "all_tools": $this->get_all_tools();break;
                case "add_rooms": $this->_add_rooms();break;
                case "all_rsv_students": $this->get_all_rserve_student();break;
                case "all_rsv_guest": $this->get_all_rserve_guest();break;
                case "all_arc": $this->get_arc();break;
            default : show_404();
            }
        } 
        private function _add_rooms() {
        $received = $this->input->post('data');
        $rsi = '1';
        $values2 = array(
            'room_number_id'=>$received['room_no'],
            'room_type_id'=>$received['rtype'],
            'room_status_id'=>$rsi,
            'date_added'=> date("Y-m-d h:i:s",time()),
            'added_by'=> $this->user_id
        );
         if($this->db->insert('rooms',$values2)) {  
            echo "added";
        } else {
            echo "failed";
        }
        } 
        
        public function get_all_rserve_student() {
           
           if($this->input->post('like')!="") {
           $rsv_1 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=1 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkin_date LIKE '".$this->input->post('like')."%'");
           
           $rsv_2 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=2 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkin_date LIKE '".$this->input->post('like')."%'");
         
           
           $rsv_3 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=3 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkin_date LIKE '".$this->input->post('like')."%'");
         
           $rsv_4 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=4 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkin_date LIKE '".$this->input->post('like')."%'");
         
           } elseif ($this->input->post('lc')!="") {
               
               $rsv_1 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=1 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkout_date LIKE '".$this->input->post('lc')."%'");
           
           $rsv_2 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=2 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkout_date LIKE '".$this->input->post('lc')."%'");
         
           
           $rsv_3 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=3 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkout_date LIKE '".$this->input->post('lc')."%'");
         
           $rsv_4 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, sa.checkin_date AS cd, 
                                              sa.checkout_date AS cod, sta.english_name AS name  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, student_accom sa, student_accomp sta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=sa.student_accom_id AND room_type_id=4 AND ra.accom_type='student' 
                                     AND sa.student_accom_id=sta.student_accom_id AND sa.checkout_date LIKE '".$this->input->post('lc')."%'");
              
           }else {}   
            $data['res'] = "";
            
            if($rsv_1->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Single</div>
                             <div class="rooms-header1 skyblue-gradient white-shadow">
                                <div><label>Room/Type</label></div>
                                <div><label>Check-in</label></div>
                                <div><label>Check-out</label></div>
                                <div><labe>English Name</label></div>
                             </div>
                             <div class="rooms_list_container1">';
                
                foreach ($rsv_1->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->name.'</label>
                          </div>
                          </div>';
                }
              }
            
            
            if($rsv_2->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Double</div>
                                <div class="rooms-header1 skyblue-gradient white-shadow">
                                    <div><label>Room/Type</label></div>
                                    <div><label>Check-in</label></div>
                                    <div><label>Check-out</label></div>
                                    <div><label>English Name</label></div>
                                </div>
                                <div class="rooms_list_container1">';
                
                foreach ($rsv_2->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->name.'</label>
                          </div>
                          </div>';
                }
            }
            
            if($rsv_3->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Quad</div>
                                <div class="rooms-header1 skyblue-gradient white-shadow">
                                    <div><label>Room/Type</label></div>
                                    <div><label>Check-in</label></div>
                                    <div><label>Check-out</label></div>
                                    <div><label>English Name</label></div>
                                </div>
                                <div class="rooms_list_container1">';
                
                foreach ($rsv_3->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->name.'</label>
                          </div>
                          </div>';
                }
            }
            
            if($rsv_4->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Corporate</div>
                                <div class="rooms-header1 skyblue-gradient white-shadow">
                                    <div><label>Room/Type</label></div>
                                    <div><label>Check-in</label></div>
                                    <div><label>Check-out</label></div>
                                    <div><label>English Name</label></div>
                                </div>
                                <div class="rooms_list_container1">';
                
                foreach ($rsv_4->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->name.'</label>
                          </div>
                          </div>';
                }
            }
            
           echo json_encode($data);         
        }

       
        public function get_all_rserve_guest() {
            
           if($this->input->post('like')!="") {
           $rsv_1 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=1 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkin_date LIKE '".$this->input->post('like')."%'");
           
           $rsv_2 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=2 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkin_date LIKE '".$this->input->post('like')."%'");
         
           
           $rsv_3 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=3 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkin_date LIKE '".$this->input->post('like')."%'");
         
           $rsv_4 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=4 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkin_date LIKE '".$this->input->post('like')."%'");
         
           } elseif ($this->input->post('lc')!="") {
               
               $rsv_1 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=1 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkout_date LIKE '".$this->input->post('lc')."%'");
           
           $rsv_2 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=2 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkout_date LIKE '".$this->input->post('lc')."%'");
         
           
           $rsv_3 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=3 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkout_date LIKE '".$this->input->post('lc')."%'");
         
           $rsv_4 = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss, ga.checkin_date AS cd, 
                                              ga.checkout_date AS cod, gta.lastname AS lname, gta.firstname AS fname  
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND room_type_id=4 AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND ga.checkout_date LIKE '".$this->input->post('lc')."%'");
              
           }else {}   
            $data['res'] = "";
            if($rsv_1->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Single</div>
                             <div class="rooms-header1 skyblue-gradient white-shadow">
                                <div><label>Room/Type</label></div>
                                <div><label>Check-in</label></div>
                                <div><label>Check-out</label></div>
                                <div><labe>Name</label></div>
                             </div>
                             <div class="rooms_list_container1">';
                
                foreach ($rsv_1->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->lname.', '.$row->fname.'</label>
                          </div>
                          </div>';
                }
            }
            
            if($rsv_2->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Double</div>
                                <div class="rooms-header1 skyblue-gradient white-shadow">
                                    <div><label>Room/Type</label></div>
                                    <div><label>Check-in</label></div>
                                    <div><label>Check-out</label></div>
                                    <div><label>Name</label></div>
                                </div>
                                <div class="rooms_list_container1">';
                
                foreach ($rsv_2->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->lname.', '.$row->fname.'</label>
                          </div>
                          </div>';
                }
            }
            
            if($rsv_3->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Quad</div>
                                <div class="rooms-header1 skyblue-gradient white-shadow">
                                    <div><label>Room/Type</label></div>
                                    <div><label>Check-in</label></div>
                                    <div><label>Check-out</label></div>
                                    <div><label>Name</label></div>
                                </div>
                                <div class="rooms_list_container1">';
                
                foreach ($rsv_3->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->lname.', '.$row->fname.'</label>
                          </div>
                          </div>';
                }
            }
            
            if($rsv_4->num_rows()>0) {
                $data['res'] .= '<div class="countbox menucenter">Corporate</div>
                                <div class="rooms-header1 skyblue-gradient white-shadow">
                                    <div><label>Room/Type</label></div>
                                    <div><label>Check-in</label></div>
                                    <div><label>Check-out</label></div>
                                    <div><label>Name</label></div>
                                </div>
                                <div class="rooms_list_container1">';
                
                foreach ($rsv_4->result() as $row) {
                    $data['res'] .='<div class="rooms-contents1">
                          <div class="'.$row->rss.'" title="'.$row->rsn.'">
                            <label class="sw-text1"> '.$row->room_number.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cd.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->cod.'</label>
                          </div>
                          <div>
                            <label class="sw-text">'.$row->lname.', '.$row->fname.'</label>
                          </div>
                          </div>';
                }
            }
            
           echo json_encode($data);          
        }
            
        public function get_arc() {
            
            $arc_s_vcS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 1 ");
            $arc_s_vcD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 1 ");
            $arc_s_vcQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 1 ");
            $arc_s_vcC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 1 ");
            $arc_s_vdS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 2 ");
            $arc_s_vdD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 2 ");
            $arc_s_vdQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 2 ");
            $arc_s_vdC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 2 ");
            $arc_s_oS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 3 ");
            $arc_s_oD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 3 ");
            $arc_s_oQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 3 ");
            $arc_s_oC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 3 ");
            $arc_s_ovS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 4 ");
            $arc_s_ovD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 4 ");
            $arc_s_ovQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 4 ");
            $arc_s_ovC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 4 ");
            $arc_s_rS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 5 ");
            $arc_s_rD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 5 ");
            $arc_s_rQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 5 ");
            $arc_s_rC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 5 ");
            $arc_s_umS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 6 ");
            $arc_s_umD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 6 ");
            $arc_s_umQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 6 ");
            $arc_s_umC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 6 ");
            $arc_s_cS = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 1 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 7 ");
            $arc_s_cD = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 2 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 7 ");
            $arc_s_cQ = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 3 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 7 ");
            $arc_s_cC = $this->db->query("SELECT COUNT(r.room_id) AS sc, rs.room_status_css AS rsc, 
                                                 rs.room_status_name AS rsn, rt.room_type_name AS rtn
                                       FROM rooms r, room_status rs, room_type rt
                                       WHERE r.room_status_id=rs.room_status_id AND r.room_type_id = 4 
                                             AND r.room_type_id=rt.room_type_id AND r.room_status_id = 7 ");
            
            
            $data['res'] = "";
                
                    if($arc_s_vcS->num_rows()>0 || $arc_s_vcD->num_rows()>0 || $arc_s_vcQ->num_rows()>0 || $arc_s_vcC->num_rows()>0
                       || $arc_s_vdS->num_rows()>0 || $arc_s_vdD->num_rows()>0 || $arc_s_vdQ->num_rows()>0 || $arc_s_vdC->num_rows()>0
                        || $arc_s_oS->num_rows()>0 || $arc_s_oD->num_rows()>0 || $arc_s_oQ->num_rows()>0 || $arc_s_oC->num_rows()>0
                         || $arc_s_ovS->num_rows()>0 || $arc_s_ovD->num_rows()>0 || $arc_s_ovQ->num_rows()>0 || $arc_s_ovC->num_rows()>0
                          || $arc_s_rS->num_rows()>0 || $arc_s_rD->num_rows()>0 || $arc_s_rQ->num_rows()>0 || $arc_s_rC->num_rows()>0
                            || $arc_s_umS->num_rows()>0 || $arc_s_umD->num_rows()>0 || $arc_s_umQ->num_rows()>0 || $arc_s_umC->num_rows()>0
                             || $arc_s_cS->num_rows()>0 || $arc_s_cD->num_rows()>0 || $arc_s_cQ->num_rows()>0 || $arc_s_cC->num_rows()>0) {
                        $data['res'] .='<div class="spacer"></div>
                            <table>
                                <tr>
                                    <td class=""></td>
                                    <td class=""></td>
                                    <td class="countbox">'.$arc_s_vcS->row()->rtn.'</td>
                                    <td class="countbox">'.$arc_s_vcD->row()->rtn.'</td>
                                    <td class="countbox">'.$arc_s_vcQ->row()->rtn.'</td>
                                    <td class="countbox">'.$arc_s_vcC->row()->rtn.'</td>
                                </tr>';
                    foreach ($arc_s_vcS->result() as $row){
                        foreach ($arc_s_vcD->result() as $rowD) {
                            foreach ($arc_s_vcQ->result() as $rowQ) {
                                foreach ($arc_s_vcC->result() as $rowC) {
                                    foreach ($arc_s_vdS->result() as $rowd){
                                foreach ($arc_s_vdD->result() as $rowdD) {
                            foreach ($arc_s_vdQ->result() as $rowdQ) {
                        foreach ($arc_s_vdC->result() as $rowdC) {
                            foreach ($arc_s_oS->result() as $rowo){
                                foreach ($arc_s_oD->result() as $rowoD) {
                                    foreach ($arc_s_oQ->result() as $rowoQ) {
                                        foreach ($arc_s_oC->result() as $rowoC) {
                                    foreach ($arc_s_ovS->result() as $rowov){
                                foreach ($arc_s_ovD->result() as $rowovD) {
                            foreach ($arc_s_ovQ->result() as $rowovQ) {
                       foreach ($arc_s_ovC->result() as $rowovC) {
                           foreach ($arc_s_rS->result() as $rowr){
                                foreach ($arc_s_rD->result() as $rowrD) {
                                    foreach ($arc_s_rQ->result() as $rowrQ) {
                                        foreach ($arc_s_rC->result() as $rowrC) {
                                    foreach ($arc_s_umS->result() as $rowum){
                                foreach ($arc_s_umD->result() as $rowumD) {
                            foreach ($arc_s_umQ->result() as $rowumQ) {
                        foreach ($arc_s_umC->result() as $rowumC) {
                            foreach ($arc_s_cS->result() as $rowc){
                                foreach ($arc_s_cD->result() as $rowcD) {
                                    foreach ($arc_s_cQ->result() as $rowcQ) {
                                        foreach ($arc_s_cC->result() as $rowcC) {
                        $data['res'] .='
                                <tr>
                                    <td class="'.$row->rsc.' nullbox"></td>
                                    <td class="empty">'.$row->rsn.'</td>
                                    <td class="c_1">'.$row->sc.'</td>
                                    <td class="c_1">'.$rowD->sc.'</td>
                                    <td class="c_1">'.$rowQ->sc.'</td>
                                    <td class="c_1">'.$rowC->sc.'</td>
                                </tr>
                                <tr>
                                    <td class="'.$rowd->rsc.' nullbox"></td>
                                    <td class="empty">'.$rowd->rsn.'</td>
                                    <td class="c_1">'.$rowd->sc.'</td>
                                    <td class="c_1">'.$rowdD->sc.'</td>
                                    <td class="c_1">'.$rowdQ->sc.'</td>
                                    <td class="c_1">'.$rowdC->sc.'</td>
                                </tr>
                                <tr>
                                    <td class="'.$rowo->rsc.' nullbox"></td>
                                    <td class="empty">'.$rowo->rsn.'</td>
                                    <td class="c_1">'.$rowo->sc.'</td>
                                    <td class="c_1">'.$rowoD->sc.'</td>
                                    <td class="c_1">'.$rowoQ->sc.'</td>
                                    <td class="c_1">'.$rowoC->sc.'</td>
                                </tr>
                                <tr>
                                    <td class="'.$rowov->rsc.' nullbox"></td>
                                    <td class="empty">'.$rowov->rsn.'</td>
                                    <td class="c_1">'.$rowov->sc.'</td>
                                    <td class="c_1">'.$rowovD->sc.'</td>
                                    <td class="c_1">'.$rowovQ->sc.'</td>
                                    <td class="c_1">'.$rowovC->sc.'</td>
                                </tr>
                                <tr>
                                    <td class="'.$rowr->rsc.' nullbox"></td>
                                    <td class="empty">'.$rowr->rsn.'</td>
                                    <td class="c_1">'.$rowr->sc.'</td>
                                    <td class="c_1">'.$rowrD->sc.'</td>
                                    <td class="c_1">'.$rowrQ->sc.'</td>
                                    <td class="c_1">'.$rowrC->sc.'</td>
                                </tr>
                                <tr>
                                    <td class="'.$rowum->rsc.' nullbox"></td>
                                    <td class="empty">'.$rowum->rsn.'</td>
                                    <td class="c_1">'.$rowum->sc.'</td>
                                    <td class="c_1">'.$rowumD->sc.'</td>
                                    <td class="c_1">'.$rowumQ->sc.'</td>
                                    <td class="c_1">'.$rowumC->sc.'</td>
                                </tr>
                                <tr>
                                    <td class="'.$rowc->rsc.' nullbox"></td>
                                    <td class="empty">'.$rowc->rsn.'</td>
                                    <td class="c_1">'.$rowc->sc.'</td>
                                    <td class="c_1">'.$rowcD->sc.'</td>
                                    <td class="c_1">'.$rowcQ->sc.'</td>
                                    <td class="c_1">'.$rowcC->sc.'</td>
                                </tr>
                            </table>';
                                        }
                                    }
                                }
                           }
                    
                                }
                            }
                        }
                    }
                                }
                            }
                        }
                    }
               }
                            }
                        }
                    }
               }
               
                            }
                        }
                    }
                }
                            }
                        }
                    } 
               }
                            }
                        }
                    }
               }
               
             echo json_encode($data); 
        }

        public function retrieve( $items = null, $offset = 0 ) {
		switch( $items ) {
                    
                        case "room_no":
				$result = array();
				$sql = "SELECT * FROM room_number_ids";
				$query = $this->db->query($sql)->result_array();
				foreach( $query as $val ) $result[] = $val["room_no"];
				$this->output->set_output(json_encode( array("options" => $result) ));
			break;
                    
			case "guest-list":
				#rooms_accom, guest_accom, guest_accomp, room_number_ids, rooms, room_type, room_rates, emp_user
				
				$offset = intval($offset);
				$per_page = 15;
				
				$total_rows = $this->db->query("SELECT 1 FROM rooms_accom WHERE accom_type = 'guest'")->num_rows;
				
				$config = array(
					'total_rows' 	=> $total_rows,
					'per_page'		=> $per_page,
					'uri_segment' 	=> 5,
					'base_url'		=> base_url().'hotel/accommodation/retrieve/guest-list'
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT ra.accom_type_id AS raid, ra.accom_status, ra.room_number_id, ra.room_accom_id,
					
					gap.guest_accompid AS gapid,
					gap.lastname, gap.firstname, gap.gender, gap.nationality , gap.email,
					gap.company, gap.address, gap.passport_no, gap.contact_no,
					FROM_UNIXTIME(UNIX_TIMESTAMP(gap.dob), '%m/%d/%Y') AS dateof_birth,
					
					ga.guest_accom_id AS gaid, ga.booking_id, ga.booking_agent,
					FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkin_date), '%m/%d/%Y @ %h:%i %p') AS checkin_date,
					FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkout_date), '%m/%d/%Y @ %h:%i %p') AS checkout_date,
					ga.comments,
					
					rni.room_no, rt.room_type_name AS room_type,
					
					FROM_UNIXTIME(UNIX_TIMESTAMP(ra.date_added), '%m/%d/%Y @ %h:%i %p') AS date_added,
					CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by
					FROM rooms_accom AS ra
					LEFT JOIN guest_accomp AS gap ON gap.guest_accompid = ra.accom_type_id
					LEFT JOIN guest_accom AS ga ON ga.guest_accom_id = gap.guest_accompid
					LEFT JOIN room_number_ids AS rni ON rni.room_number_id = ra.room_number_id
					LEFT JOIN rooms AS rm ON rni.room_number_id = rm.room_number_id
					LEFT JOIN room_type AS rt ON rt.room_type_id = rm.room_type_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = ra.added_by
					WHERE ra.accom_type = 'guest' ORDER BY ra.date_added DESC
					LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output(
					json_encode(
						array(
							'result'		=> $this->db->query($sql)->result_array(),
							'pagination'	=> $pagination
						)
					)
				);
				
			break;
			case "student-list":
				
				$offset = intval($offset);
				$per_page = 15;
				
				$total_rows = $this->db->query("SELECT 1 FROM rooms_accom WHERE accom_type = 'student'")->num_rows;
				
				$config = array(
					'total_rows' 	=> $total_rows,
					'per_page'		=> $per_page,
					'uri_segment' 	=> 5,
					'base_url'		=> base_url().'hotel/accommodation/retrieve/student-list'
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT ra.accom_type_id AS raid, ra.accom_status, ra.room_number_id, ra.room_accom_id,
					
					sap.student_accompid AS sapid, sap.lastname, sap.firstname, sap.english_name, sap.gender, sap.age,
					sap.numberof_weeks, sap.email, sap.mobile_no, sap.student_no, sap.course,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sap.dob), '%m/%d/%Y') AS dateof_birth, sap.nationality, sap.passport_no,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sap.p_expiry_date), '%m/%d/%Y') AS p_expiry_date, sap.p_placeof_issue,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sap.p_dateof_issue), '%m/%d/%Y') AS p_dateof_issue,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sap.visa_expiry), '%m/%d/%Y') AS visa_expiry,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sap.ssp_validity), '%m/%d/%Y') AS ssp_validity,
					sap.address_abroad, sap.nameof_guardian,
					
					sa.student_accom_id AS said, sa.booking_id, sa.booking_agent,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sa.checkin_date), '%m/%d/%Y @ %h:%i %p') AS checkin_date,
					FROM_UNIXTIME(UNIX_TIMESTAMP(sa.checkout_date), '%m/%d/%Y @ %h:%i %p') AS checkout_date,
					
					FROM_UNIXTIME(UNIX_TIMESTAMP(CONCAT_WS(' ', '2012-01-01', sa.sendoff_time)), '%m/%d/%Y @ %h:%i %p' ) AS sendoff_time,
					FROM_UNIXTIME(UNIX_TIMESTAMP(CONCAT_WS(' ', '2012-01-01', sa.pickup_time)), '%m/%d/%Y @ %h:%i %p' ) AS pickup_time,
					sa.comments,
					
					rni.room_no, rt.room_type_name AS room_type,
					
					FROM_UNIXTIME(UNIX_TIMESTAMP(ra.date_added), '%m/%d/%Y @ %h:%i %p') AS date_added,
					CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by
					
					FROM rooms_accom AS ra
					LEFT JOIN student_accomp AS sap ON sap.student_accompid = ra.accom_type_id
					LEFT JOIN student_accom AS sa ON sa.student_accom_id = sap.student_accom_id
					LEFT JOIN room_number_ids AS rni ON rni.room_number_id = ra.room_number_id
					LEFT JOIN rooms AS rm ON rni.room_number_id = rm.room_number_id
					LEFT JOIN room_type AS rt ON rt.room_type_id = rm.room_type_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = ra.added_by
					WHERE ra.accom_type = 'student' ORDER BY ra.date_added DESC
					LIMIT {$offset}, {$per_page}
				";
				$this->output->set_output(
					json_encode(
						array(
							'result' => $this->db->query($sql)->result_array(),
							'pagination' => $pagination
						)
					)
				);
			break;
			default:
				show_404();
			break;
		}
	}
	
	public function logs( $item = null, $roomid_number = 0 ) {
		# ari ra diri mo query sa guest ug students dayon e-merge nlng ang result array
		# niya dayon e return; sakto bah? 
		
		$roomid_number = intval($roomid_number);
		$actions = array('checkout', 'checkin', 'reserve');
		
		if( in_array($item, $actions) ) {
			
			$result = array();
			
			$sql = "SELECT accom_type, accom_type_id FROM rooms_accom WHERE accom_status = '{$item}' AND room_number_id = {$roomid_number}";
			$query = $this->db->query($sql);

			# ok here we go.. e join nato ang tables depende sa ilang accom_type
			# kung guest ba siya or student.. lain2x man gud ang tables sa guest accommodation
			# ug sa student
			
			if( $query->num_rows() > 0 ) {
				foreach( $query->result_array() as $q ) {
					$result[] = ($q['accom_type'] === 'guest') ? 
						$this->db->query( $this->guest_logs($item, $roomid_number, $q['accom_type_id'] ) )->row_array() :
							$this->db->query( $this->student_logs($item, $roomid_number, $q['accom_type_id']) )->row_array();
					#if( $q['accom_type'] === 'guest' ) {
					#	$result[] = $this->db->query( $this->guest_logs($item, $roomid_number, $q['accom_type_id'] ) )->row_array();
					#} else if( $q['accom_type'] === 'student' ) {
					#	$result[] = $this->db->query( $this->student_logs($item, $roomid_number, $q['accom_type_id']) )->row_array();
					#}
				}
			}
			
			$this->output->set_output(
				json_encode(
					array(
						'result'		=> $result,
						'pagination'	=> ''
					)
				)
			);
			
		}
	}
	/*
	 * Join guest accommodation tables
	 */
	public function guest_logs( $item = null, $roomid_number = 0, $accom_type_id = 0 ) {
		return "
			SELECT ra.accom_type_id AS raid, ra.accom_status, ra.room_number_id, ra.room_accom_id, ra.accom_type,
			
			gap.guest_accompid AS gapid,
			gap.lastname, gap.firstname, gap.gender, gap.nationality, gap.email,
			gap.company, gap.address, gap.passport_no, gap.contact_no,
			FROM_UNIXTIME(UNIX_TIMESTAMP(gap.dob), '%m/%d/%Y') AS dateof_birth,
			
			ga.guest_accom_id AS gaid, ga.booking_id, ga.booking_agent,
			FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkin_date), '%m/%d/%Y @ %h:%i %p') AS checkin_date,
			FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkout_date), '%m/%d/%Y @ %h:%i %p') AS checkout_date,
			ga.comments,
			
			rni.room_no, rt.room_type_name AS room_type,
			
			FROM_UNIXTIME(UNIX_TIMESTAMP(ra.date_added), '%m/%d/%Y @ %h:%i %p') AS date_added,
			CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by
			FROM rooms_accom AS ra
			LEFT JOIN guest_accomp AS gap ON gap.guest_accompid = ra.accom_type_id
			LEFT JOIN guest_accom AS ga ON ga.guest_accom_id = gap.guest_accompid
			LEFT JOIN room_number_ids AS rni ON rni.room_number_id = ra.room_number_id
			LEFT JOIN rooms AS rm ON rni.room_number_id = rm.room_number_id
			LEFT JOIN room_type AS rt ON rt.room_type_id = rm.room_type_id
			LEFT JOIN emp_personal_details AS epd ON epd.emp_id = ra.added_by
			WHERE ra.room_number_id = {$roomid_number} AND ra.accom_status = '{$item}' AND ra.accom_type_id = {$accom_type_id}
		";
	}
	/*
	 * Join student accommodation tables
	 */
	public function student_logs( $item = null, $roomid_number = 0, $accom_type_id = 0 ) {
		return "
			SELECT ra.accom_type_id AS raid, ra.accom_status, ra.room_number_id, ra.room_accom_id, ra.accom_type,
			
			sap.student_accompid AS sapid, sap.lastname, sap.firstname, sap.english_name, sap.gender, sap.age,
			sap.numberof_weeks, sap.email, sap.mobile_no, sap.student_no, sap.course,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sap.dob), '%m/%d/%Y') AS dateof_birth, sap.nationality, sap.passport_no,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sap.p_expiry_date), '%m/%d/%Y') AS p_expiry_date, sap.p_placeof_issue,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sap.p_dateof_issue), '%m/%d/%Y') AS p_dateof_issue,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sap.visa_expiry), '%m/%d/%Y') AS visa_expiry,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sap.ssp_validity), '%m/%d/%Y') AS ssp_validity,
			sap.address_abroad, sap.nameof_guardian,
			
			sa.student_accom_id AS said, sa.booking_id, sa.booking_agent,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sa.checkin_date), '%m/%d/%Y @ %h:%i %p') AS checkin_date,
			FROM_UNIXTIME(UNIX_TIMESTAMP(sa.checkout_date), '%m/%d/%Y @ %h:%i %p') AS checkout_date,
			sa.sendoff_time, sa.pickup_time, sa.comments,
			
			rni.room_no, rt.room_type_name AS room_type,
			
			FROM_UNIXTIME(UNIX_TIMESTAMP(ra.date_added), '%m/%d/%Y @ %h:%i %p') AS date_added,
			CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by
			
			FROM rooms_accom AS ra
			LEFT JOIN student_accomp AS sap ON sap.student_accompid = ra.accom_type_id
			LEFT JOIN student_accom AS sa ON sa.student_accom_id = sap.student_accom_id
			LEFT JOIN room_number_ids AS rni ON rni.room_number_id = ra.room_number_id
			LEFT JOIN rooms AS rm ON rni.room_number_id = rm.room_number_id
			LEFT JOIN room_type AS rt ON rt.room_type_id = rm.room_type_id
			LEFT JOIN emp_personal_details AS epd ON epd.emp_id = ra.added_by
			WHERE ra.room_number_id = {$roomid_number} AND ra.accom_status = '{$item}' AND ra.accom_type_id = {$accom_type_id}
		";
	}
	/*
	 * Pang update ni siya dinhing dapita..
	 */
	public function update( $item = null ) {
		$data_items = json_decode($this->input->post("model"));
		
		switch( $item ) {
			case "guest":
				$guest_accom = array(
					"booking_id"	=> $data_items->ag_booking_id,
					"booking_agent"	=> $data_items->ag_booking_type,
					"checkin_date"	=> to_mysql_datetime($data_items->ag_checkin_date),
					"checkout_date"	=> to_mysql_datetime($data_items->ag_checkout_date),
					"comments"		=> $data_items->ag_cg_comments
				);
				
				$guest_accomp = array(
					"lastname"			=> $data_items->ag_lname,
					"firstname"			=> $data_items->ag_fname,
					"gender"			=> $data_items->ag_gender,
					"nationality"		=> $data_items->ag_nationality,
					"dob"				=> to_mysql_date($data_items->ag_dob),
					"email"				=> $data_items->ag_email,
					"address"			=> $data_items->ag_address,
					"passport_no"		=> $data_items->ag_passport_no,
					"contact_no"		=> $data_items->ag_contact_no
				);
				
				$rooms_accom = array(
					"accom_status"		=> $data_items->ag_booking_status,
				);
				
				$this->db->update('guest_accom', $guest_accom, array('guest_accom_id' => $data_items->gaid));
				$this->db->update('guest_accomp', $guest_accomp, array('guest_accompid' => $data_items->gapid));
				$this->db->update('rooms_accom', $rooms_accom, array('room_accom_id' => $data_items->rmaid));
				
				# check kung naa bay checkin nga status
				$sql = "SELECT DISTINCT accom_status FROM rooms_accom WHERE (rooms_accom_status = 'active' AND accom_status != 'checkout' AND room_number_id = {$data_items->rni})";
				$query = $this->db->query($sql)->result_array();
				
				#wa koy kinahanglan e-explain dinhi dapita.. haha!
				
				if( !in_array('checkin', $query ) && $data_items->rni == '3' )
					$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rni, "status" => "active"));
				else {
					$data_items->ag_booking_status === 'reserve' ?
						$this->db->update("rooms", array("room_status_id" => 5), array("room_number_id" => $data_items->rni, "status" => "active")) :
							$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rni, "status" => "active"));
				}
				
			break;
			
			case "student":
				
				$student_accom = array(
					"booking_id"	=> $data_items->ag_booking_id,
					"booking_agent"	=> $data_items->ag_booking_type,
					"checkin_date"	=> to_mysql_datetime($data_items->ag_checkin_date),
					"checkout_date"	=> to_mysql_datetime($data_items->ag_checkout_date),
					"sendoff_time"	=> format_timepicker($data_items->ag_sendoff_time),
					"pickup_time"	=> format_timepicker($data_items->ag_pickup_time),
					"comments"		=> $data_items->ag_cg_comments
				);
				
				$student_accomp = array(
					"lastname"			=> $data_items->ag_lname,
					"firstname"			=> $data_items->ag_fname,
					"english_name"		=> $data_items->ag_engname,
					"gender"			=> $data_items->ag_gender,
					"age"				=> $data_items->ag_age,
					"numberof_weeks"	=> $data_items->ag_numof_weeks,
					"email"				=> $data_items->ag_email_add,
					"mobile_no"			=> $data_items->ag_mobile_no,
					"student_no"		=> $data_items->ag_student_no,
					"course"			=> $data_items->ag_course,
					"dob"				=> to_mysql_date($data_items->ag_dob),
					"nationality"		=> $data_items->ag_nationality,
					"passport_no"		=> $data_items->ag_passport_no,
					"p_expiry_date"		=> to_mysql_date($data_items->ag_expiry_date),
					"p_placeof_issue"	=> $data_items->ag_placeof_issue,
					"p_dateof_issue"	=> $data_items->ag_dateof_issue,
					"visa_expiry"		=> to_mysql_date($data_items->ag_visa_expiry),
					"ssp_validity"		=> to_mysql_date($data_items->ag_ssp_validity),
					"address_abroad"	=> $data_items->ag_address_abroad,
					"nameof_guardian"	=> $data_items->ag_nameof_guardian
				);
				
				$rooms_accom = array(
					"accom_status"		=> $data_items->ag_booking_status
				);
				
				$this->db->update('student_accom', $student_accom, array('student_accom_id' => $data_items->said));
				$this->db->update('student_accomp', $student_accomp, array('student_accompid' => $data_items->sapid));
				$this->db->update('rooms_accom', $rooms_accom, array('room_accom_id' => $data_items->rmaid));
				
				# check kung naa bay checkin nga status
				$sql = "SELECT DISTINCT accom_status FROM rooms_accom WHERE (rooms_accom_status = 'active' AND accom_status != 'checkout' AND room_number_id = {$data_items->rni})";
				$query = $this->db->query($sql)->result_array();
				
				#wa koy kinahanglan e-explain dinhi dapita.. haha!
				
				if( !in_array('checkin', $query ) && $data_items->rni == '3' )
					$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rni, "status" => "active"));
				else {
					$data_items->ag_booking_status === 'reserve' ?
						$this->db->update("rooms", array("room_status_id" => 5), array("room_number_id" => $data_items->rni, "status" => "active")) :
							$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rni, "status" => "active"));
				}
				
			break;
		}
		
	}
	
	/*
	 * Add accommodation
	 * 
	 * I think this is pretty self explanatory down here.. :D
	 */
	public function add( $item = null ) {
		$data_items = json_decode($this->input->post("model"));
		switch( $item ) {
			case "guest":
				
				#guest_accom
				#guest_accomp
				#rooms_accom
				
				$guest_accom = array(
					"booking_id"	=> $data_items->ag_booking_id,
					"booking_agent"	=> $data_items->ag_booking_type,
					"checkin_date"	=> to_mysql_datetime($data_items->ag_checkin_date),
					"checkout_date"	=> to_mysql_datetime($data_items->ag_checkout_date),
					"comments"		=> $data_items->ag_cg_comments
				);
				
				$this->db->insert('guest_accom', $guest_accom);
				$guest_accom_id = $this->db->insert_id();
				
				$guest_accomp = array(
					"guest_accom_id"	=> $guest_accom_id,
					"lastname"			=> $data_items->ag_lname,
					"firstname"			=> $data_items->ag_fname,
					"gender"			=> $data_items->ag_gender,
					"nationality"		=> $data_items->ag_nationality,
					"dob"				=> to_mysql_date($data_items->ag_dob),
					"email"				=> $data_items->ag_email,
					"address"			=> $data_items->ag_address,
					"passport_no"		=> $data_items->ag_passport_no,
					"contact_no"		=> $data_items->ag_contact_no
				);
				
				$this->db->insert('guest_accomp', $guest_accomp);
				$guest_accomp_id = $this->db->insert_id();
				
				$rooms_accom = array(
					"accom_type"		=> "guest",
					"accom_type_id"		=> $guest_accomp_id,
					"room_number_id"	=> $data_items->rid,
					"accom_status"		=> $data_items->ag_booking_status,
					"date_added"		=> date("Y-m-d h:i:s",time()),
					"added_by"			=> $this->user_id
				);
				
				$this->db->insert('rooms_accom', $rooms_accom);
				
				# check kung naa bay checkin nga status
				$sql = "SELECT DISTINCT accom_status FROM rooms_accom WHERE (rooms_accom_status = 'active' AND accom_status != 'checkout' AND room_number_id = {$data_items->rid})";
				$query = $this->db->query($sql)->result_array();
				
				#wa koy kinahanglan e-explain dinhi dapita.. haha!
				
				if( !in_array('checkin', $query ) && $data_items->rid == '3' )
					$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rid, "status" => "active"));
				else {
					$data_items->ag_booking_status === 'reserve' ?
						$this->db->update("rooms", array("room_status_id" => 5), array("room_number_id" => $data_items->rid, "status" => "active")) :
							$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rid, "status" => "active"));
				}
			break;
			
			case "student":
				#student_accom
				#student_accomp
				#rooms_accom
				
				$student_accom = array(
					"booking_id"	=> $data_items->ag_booking_id,
					"booking_agent"	=> $data_items->ag_booking_type,
					"checkin_date"	=> to_mysql_datetime($data_items->ag_checkin_date),
					"checkout_date"	=> to_mysql_datetime($data_items->ag_checkout_date),
					"sendoff_time"	=> format_timepicker($data_items->ag_sendoff_time),
					"pickup_time"	=> format_timepicker($data_items->ag_pickup_time),
					"comments"		=> $data_items->ag_cg_comments
				);
				
				$this->db->insert('student_accom', $student_accom);
				$student_accom_id = $this->db->insert_id();
				
				$student_accomp = array(
					"student_accom_id"	=> $student_accom_id,
					"lastname"			=> $data_items->ag_lname,
					"firstname"			=> $data_items->ag_fname,
					"english_name"		=> $data_items->ag_engname,
					"gender"			=> $data_items->ag_gender,
					"age"				=> $data_items->ag_age,
					"numberof_weeks"	=> $data_items->ag_numof_weeks,
					"email"				=> $data_items->ag_email_add,
					"mobile_no"			=> $data_items->ag_mobile_no,
					"student_no"		=> $data_items->ag_student_no,
					"course"			=> $data_items->ag_course,
					"dob"				=> to_mysql_date($data_items->ag_dob),
					"nationality"		=> $data_items->ag_nationality,
					"passport_no"		=> $data_items->ag_passport_no,
					"p_expiry_date"		=> to_mysql_date($data_items->ag_expiry_date),
					"p_placeof_issue"	=> $data_items->ag_placeof_issue,
					"p_dateof_issue"	=> $data_items->ag_dateof_issue,
					"visa_expiry"		=> to_mysql_date($data_items->ag_visa_expiry),
					"ssp_validity"		=> to_mysql_date($data_items->ag_ssp_validity),
					"address_abroad"	=> $data_items->ag_address_abroad,
					"nameof_guardian"	=> $data_items->ag_nameof_guardian
				);
				
				$this->db->insert("student_accomp", $student_accomp);
				$student_accomp_id = $this->db->insert_id();
				
				$rooms_accom = array(
					"accom_type"		=> "student",
					"accom_type_id"		=> $student_accomp_id,
					"room_number_id"	=> $data_items->rid,
					"accom_status"		=> $data_items->ag_booking_status,
					"date_added"		=> date("Y-m-d h:i:s",time()),
					"added_by"			=> $this->user_id
				);
				
				$this->db->insert('rooms_accom', $rooms_accom);
				
				$sql = "SELECT DISTINCT accom_status FROM rooms_accom WHERE (rooms_accom_status = 'active' AND accom_status != 'checkout' AND room_number_id = {$data_items->rid})";
				$query = $this->db->query($sql)->result_array();
				
				#wa koy kinahanglan e-explain dinhi dapita.. haha!				
				if( !in_array('checkin', $query ) && $data_items->rid == '3' )
					$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rid, "status" => "active"));
				else {
					$data_items->ag_booking_status === 'reserve' ?
						$this->db->update("rooms", array("room_status_id" => 5), array("room_number_id" => $data_items->rid, "status" => "active")) :
							$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rid, "status" => "active"));
				}
				
			break;
		}
	}
	
	public function checkout( $accom_type = null, $room_number_id = 0, $accom_type_id = 0) {
		$data_items = json_decode($this->input->post('model'));
		$room_number_id = intval($room_number_id);

		//print_r($data_items->adP);
		$type_list = array('guest', 'student');
		
		if(in_array($accom_type, $type_list)) {
			
			if( !empty($data_items->adP) ) {
				foreach( $data_items->adP AS $adP ) {
					#accom_additional_payments
					$accom_additional_payments = array(
						'accom_type'	=> $accom_type,
						'accom_type_id' => intval($accom_type_id),
						'description'	=> $adP->chk_desc,
						'amount'		=> floatval($adP->chk_amount)
					);
					$this->db->insert('accom_additional_payments', $accom_additional_payments);
				}
			}
			
			#rooms_accom
			$this->db->update('rooms_accom', array('accom_status' => 'checkout'), array('accom_type' => $accom_type, 'accom_type_id' => $accom_type_id));
			$sql = "SELECT 1 FROM rooms_accom WHERE (rooms_accom_status = 'active' AND accom_status != 'checkout' AND room_number_id = {$room_number_id})";
			$query = $this->db->query($sql);
			
			#updating the room status to dirty - clean if there are no more guest/students who
			#checkin
			if( $query->num_rows === 0 ) {
				$this->db->update("rooms", array("room_status_id" => 2), array("room_number_id" => $room_number_id, "status" => "active"));
			}
		}
	}
        public function get_all_guest($offset=0) {   
        if($this->input->post('like')!="") {
        $guest = $this->db->query(" SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss,
                                              
                                              ra.accom_status, ra.room_accom_id, ra.accom_type_id AS raid,
                                              FROM_UNIXTIME(UNIX_TIMESTAMP(ra.date_added), '%m/%d/%Y @ %h:%i %p') AS date_added,
                                              
                                              FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkin_date), '%m/%d/%Y @ %h:%i %p') AS cid,
                                              FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkout_date), '%m/%d/%Y @ %h:%i %p') AS cod,
                                              
                                              ga.guest_accom_id AS gaid, ga.booking_id, ga.booking_agent, ga.comments,
                                              
                                              gta.guest_accompid AS gapid, gta.lastname AS lname, gta.firstname AS fname, gta.gender, 
                                              gta.nationality, gta.passport_no, gta.contact_no, gta.address, 
                                              FROM_UNIXTIME(UNIX_TIMESTAMP(gta.dob), '%m/%d/%Y') AS dob, gta.email,
                                              
                                              CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by
                                              
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta, emp_personal_details epd
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND ra.accom_type='guest' 
                                     AND ga.guest_accom_id=gta.guest_accom_id AND epd.emp_id = ra.added_by
                                     AND CONCAT_WS(' ', gta.firstname, gta.lastname) LIKE '".$this->input->post('like')."%' ORDER BY date_added DESC");
        
        }else{         
        $guest = $this->db->query("SELECT  r.*, rni.room_no AS room_number, rs.room_status_name AS rsn, 
                                              rs.room_status_css AS rss,
                                              
                                              ra.accom_status, ra.accom_type_id AS raid,
                                              FROM_UNIXTIME(UNIX_TIMESTAMP(ra.date_added), '%m/%d/%Y @ %h:%i %p') AS date_added,
                                             
                                              
                                               FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkin_date), '%m/%d/%Y @ %h:%i %p') AS cid,
                                               FROM_UNIXTIME(UNIX_TIMESTAMP(ga.checkout_date), '%m/%d/%Y @ %h:%i %p') AS cod,
                                                
                                               ga.guest_accom_id AS gaid, ga.booking_id, ga.booking_agent, ga.comments,
                                              
                                              gta.guest_accompid AS gapid, gta.lastname AS lname, gta.firstname AS fname, gta.gender, gta.nationality,
                                              gta.passport_no, gta.contact_no, gta.address,
                                              FROM_UNIXTIME(UNIX_TIMESTAMP(gta.dob), '%m/%d/%Y') AS dob, gta.email,
                                              
                                              CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by
                                              
                                     FROM rooms r, room_number_ids rni, room_status rs, 
                                          rooms_accom ra, guest_accom ga, guest_accomp gta, emp_personal_details epd
                                     WHERE r.room_number_id=rni.room_number_id 
                                     AND r.room_status_id=rs.room_status_id AND r.room_number_id=ra.room_number_id
                                     AND ra.accom_type_id=ga.guest_accom_id AND ra.accom_type='guest' AND epd.emp_id = ra.added_by
                                     AND ga.guest_accom_id=gta.guest_accom_id ORDER BY ra.date_added DESC");
       }  
        $data['res'] = "";
        if($guest->num_rows()>0) {
            $data['res'] .= '<div class="gs-contents">
                                <ul class="oaccordion-list">';
            foreach($guest->result() as $row) {
                $data['res'] .='<li>
                                <div class="oalist-head oa-head">
                                <a id="1" href="#/task/hotel-list/guest">+</a>
                                    Name: '.$row->fname.' '.$row->lname.'
                                        <span class="oalist-room-type"> Room Number: '.$row->room_number.'</span>
                                            <span> Date Booked: '.$row->date_added.'</span>
                                                <div class="cicog"> 
                                                Check-in: '.$row->cid.' - Check-out: '.$row->cod.'
                                                </div>
                                                <div class="clr"></div>
                                </div>
                                <div class="oa-content glf-c">
                                    <div class="glf">
                                        <label>First Name: </label> '.$row->fname.'
                                    </div>
                                    <div class="glf">
                                        <label>Room No: </label> '.$row->room_number.'
                                    </div>
                                    <div class="glf">
                                        <label>Last Name: </label> '.$row->lname.'
                                    </div>
                                    <div class="glf">
                                        <label>Booking ID: </label> '.$row->booking_id.'
                                    </div>
                                    <div class="glf">
                                        <label>Gender:</label> '.$row->gender.'
                                    </div>
                                    <div class="glf">
                                        <label>Booking Type:</label> '.$row->booking_agent.'
                                    </div>
                                    <div class="glf">
                                        <label>Date of Birth:</label> '.$row->dob.'
                                    </div>
                                    <div class="glf">
                                        <label>Check-in Date:</label> '.$row->cid.'
                                    </div>
                                    <div class="glf">
                                        <label>Email</label> '.$row->email.'
                                    </div>
                                    <div class="glf">
                                        <label>Check-out Date</label> '.$row->cod.'
                                    </div>
                                    <div class="glf">
                                        <label>Nationality</label> '.$row->nationality.'
                                    </div>
                                    <div class="glf">
                                        <label>Passport No:</label> '.$row->passport_no.'
                                    </div>
                                    <div class="glf">
                                        <label>Contact No:</label> '.$row->contact_no.'
                                    </div>
                                    <div class="glf"><label>&nbsp;</label></div>
                                    <div class="clr"></div>
                                    <div class="glfm">
                                            Address:
                                            <label>'.$row->address.'</label>
                                                <div class="clr"></div>
                                    </div>
                                    <div class="clr"></div>
                                    <div class="clr"></div>
                                    <div class="glfm">
                                            Comments:
                                            <label>'.$row->comments.'</label>
                                                <div class="clr"></div>
                                    </div>
                                    <div class="clr"></div>
                                    <div class="glf">
                                        <label>Status: </label> '.$row->accom_status.' 
                                    </div>
                                    <div class="glf">
                                        <label>Date Added: </label> '.$row->date_added.'
                                    </div>
                                    <div class="glf glf-NBB">
                                        <label>Added by: </label> '.$row->added_by.'
                                    </div>
                                    <div class="glf glf-NBB">
                                        <input type="button" class="gli-edit css3-btton" id="'.$row->raid.':'.$row->gapid.':'.$row->gaid.'" value="Edit">
                                        <div class="clr"></div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                                ';
            }
            $data['res'] .= '   </li>
                                </ul>
                                <div class="page-pagination hll-pagination"></div>
                            </div>';
        }
        echo json_encode($data);
    }
   public function get_all_tools($offset=0) {
        
       if($this->input->post('like')!="") {
        $tools = $this->db->query("SELECT omt.tools_id, omt.item_num, omt.description, omt.quantity, unom.name AS unit_name, dd.dept_name AS dept_name
                                 FROM ops_main_tools omt, unitsof_measure unom, department_details dd
                                 WHERE omt.uom=unom.id AND omt.department=dd.deptd_id AND omt.description LIKE '".$this->input->post('like')."%' ORDER BY omt.description DESC  LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(omt.tools_id) AS rows FROM ops_main_tools omt, unitsof_measure unom, department_details dd 
                                    WHERE omt.uom=unom.id AND omt.department=dd.deptd_id AND omt.description LIKE '".$this->input->post('like')."%'");
       }else {
        $tools = $this->db->query("SELECT omt.tools_id, omt.item_num, omt.description, omt.quantity, unom.name AS unit_name, dd.dept_name AS dept_name
                                 FROM ops_main_tools omt, unitsof_measure unom, department_details dd
                                 WHERE omt.uom=unom.id AND omt.department=dd.deptd_id ORDER BY omt.description DESC LIMIT 10 OFFSET ".$offset);
        $_count = $this->db->query("SELECT COUNT(omt.tools_id) AS rows FROM ops_main_tools omt, unitsof_measure unom, department_details dd
                                 WHERE omt.uom=unom.id AND omt.department=dd.deptd_id");
       }
        $config['base_url']= base_url().'hotel/accommodation/get_all_tools/';
        $config['total_rows']= $_count->row()->rows;
        $config['uri_segment']= 4;
        $config['per_page']= 10;
        $this->pagination->initialize($config);
        $data['num_rows']= $tools->num_rows();
        $data['pagination'] = $this->pagination->create_links(); 
        $data['res'] = "";
        if($tools->num_rows()>0) {
            $data['res'] .= '<div class="white-litegray menucenter">Maintenance Supplies</div>
                            <div class="stocks-header1 skyblue-gradient white-shadow">  
                                
                                <div><label>Item Num</label></div>
                                <div><label>Description</label></div>
                                <div><label>Quantity</label></div>
                                <div><label>Dept. Used</label></div>

                            </div>
                            <div class="stocks_list_container1">';
            foreach($tools->result() as $row) {
                $data['res'] .='<div class="stocks-contents1">
                                
                                <div>'.$row->item_num.'</div>
                                <div>'.$row->description.'</div>
                                <div>'.$row->quantity.' '.$row->unit_name.'</div>
                                <div>'.$row->dept_name.'</div>
                                <button class="blue-button update-tools" id="st_'.$row->tools_id.'">Update</button>
                                <label class="delete-tools" id="st_'.$row->tools_id.'">X</label>
                                </div>';
            }
            $data['res'] .= '</div>';
        }
        echo json_encode($data);
    }
}