<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Rooms extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
		
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	/*
	 * Retrieves information from the database based on the passed parameter
	 * 
	 * @access: public
	 * @params: $item - String
	 */
	public function retrieve( $item = null ) {
		
	}
	/*
	 * Returns the list of rooms
	 */
	
	public function get( $item = null ) {
		switch( $item ) {
			case "rooms-list":
				$checkin = array(); $checkout = array(); $result = array();
				/*
				$sql = "
					SELECT rni.room_number_id AS room_nid, rni.room_no, rt.room_type_name, rs.room_status_name, rs.room_status_css, rmrates.room_rate
					FROM rooms AS r LEFT JOIN room_number_ids AS rni ON rni.room_number_id = r.room_number_id 
					LEFT JOIN room_type AS rt ON rt.room_type_id = r.room_type_id 
					LEFT JOIN room_status AS rs ON rs.room_status_id = r.room_status_id 
					LEFT JOIN room_rates AS rmrates ON rmrates.room_rate_id = rt.room_rate_id
					ORDER BY rni.room_no";
				
				$this->output->set_output(
					json_encode(
						array(
							'result' => $this->db->query($sql)->result_array()
						)
					)
				);*/
				
				$sql = "
					SELECT rni.room_number_id AS room_nid, rni.room_no, rt.room_type_name, rs.room_status_name, rs.room_status_css, rmrates.room_rate
					FROM rooms AS r LEFT JOIN room_number_ids AS rni ON rni.room_number_id = r.room_number_id
					LEFT JOIN room_type AS rt ON rt.room_type_id = r.room_type_id
					LEFT JOIN room_status AS rs ON rs.room_status_id = r.room_status_id
					LEFT JOIN room_rates AS rmrates ON rmrates.room_rate_id = rt.room_rate_id
					ORDER BY rni.room_no";
				$query = $this->db->query($sql)->result_array();
				
				foreach( $query as $q ) {
					$count_checkin = "
						SELECT 1 FROM rooms_accom
						WHERE accom_status = 'checkin' AND room_number_id = ".intval($q['room_nid'])."
					";
					$count_reservation = "
						SELECT 1 FROM rooms_accom
						WHERE accom_status = 'reserve' AND room_number_id = ".intval($q['room_nid'])."
					";
					
					$q['checkin'] = $this->db->query($count_checkin)->num_rows();
					$q['checkout'] = $this->db->query($count_reservation)->num_rows();
					
					$result[] = $q;
				}
				
				$this->output->set_output(
					json_encode(
						array(
							'result' => $result
						)
					)
				);
				
			break;   
                     
		}
	}

}