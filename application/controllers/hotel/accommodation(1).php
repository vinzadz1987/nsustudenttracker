<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Accommodation extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
		
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	/*
	 * Retrieve information from the database 
	 * based on the given parameter
	 * 
	 * @params:
	 * 		$items - String
	 * 
	 * If you don't go after what you want, you'll never have it.. 
	 * And if you don't step forward, you're always be in the same place..
	 * Life really isn't fair but it is still good.
	 * 
	 */
	public function retrieve( $items = null, $offset = 0 ) {
		switch( $items ) {
			case "vacant-clean-rooms":
				
				$used_rooms = "
					SELECT DISTINCT a.room_number
					FROM accommodations AS a
					LEFT JOIN rooms AS r ON	a.room_number = r.number
					LEFT JOIN room_status AS rs ON rs.id = r.status
					WHERE rs.name = 'Vacant - Clean'
				";

				$used_rooms = $this->db->query($used_rooms)->result_array();
				#make the room numbers a comma separated string to be used in mysql query
				#$used_rooms = implode(",", $used_rooms);
				$usedrooms = ""; $length = count($used_rooms); $counter = 1; 
				foreach( $used_rooms as $u ) {
					$usedrooms .= ( $length !== $counter ) ? $u["room_number"]."," : $u["room_number"];
					$counter++;
				}
				$usedrooms = !empty($usedrooms) ? "AND r.number NOT IN({$usedrooms})" : "";
				$sql = "
					SELECT CONCAT_WS(' - ', r.number, rt.name) AS room_name
					FROM rooms AS r
					LEFT JOIN room_type AS rt ON rt.id = r.type
					LEFT JOIN room_status AS rs ON rs.id = r.status
					WHERE (rs.name = 'Vacant - Clean' AND rt.name != 'Corporate')
					{$usedrooms}
					ORDER BY r.number ASC
				";
				$this->output->set_output(json_encode($this->db->query($sql)->result_array()));
			break;
			#lcvr = list clean vacant rooms
			case  "lcvr":
				$sql = "
					SELECT CONCAT_WS(' - ', r.number, rt.name) AS room_name FROM rooms AS r
					LEFT JOIN room_type AS rt ON rt.id = r.type
					LEFT JOIN room_status AS rs ON rs.id = r.status
					WHERE (rs.name = 'Vacant - Clean' AND rt.name != 'Corporate')
					ORDER BY r.number ASC
				";
				$this->output->set_output(json_encode($this->db->query($sql)->result_array()));
			break;
			case "student-list":
				
				$sql = "
					SELECT s.students_accom_id AS said, s.lastname, s.firstname, s.english_name, s.gender, s.age,
					s.numberof_weeks, s.email, s.mobile_no, s.student_no, s.course, 
					FROM_UNIXTIME(UNIX_TIMESTAMP(s.dateof_birth), '%m/%d/%Y') AS dateof_birth,
					s.nationality, s.passport_no, 
					FROM_UNIXTIME(UNIX_TIMESTAMP(s.expiry_date), '%m/%d/%Y') AS expiry_date,
					s.placeof_issue,
					FROM_UNIXTIME(UNIX_TIMESTAMP(s.dateof_issue), '%m/%d/%Y') AS dateof_issue,
					FROM_UNIXTIME(UNIX_TIMESTAMP(s.visa_expiry), '%m/%d/%Y') AS visa_expiry,
					FROM_UNIXTIME(UNIX_TIMESTAMP(s.ssp_validity), '%m/%d/%Y') AS ssp_validity,
					s.address_abroad, s.nameof_guardian,
					
					a.students_accommodation_id AS aid, a.room_no, a.booking_id, a.booking_agent,
					FROM_UNIXTIME(UNIX_TIMESTAMP(a.checkin_date), '%m/%d/%Y @ %h:%i %p') AS checkin_date,
					FROM_UNIXTIME(UNIX_TIMESTAMP(a.checkout_date), '%m/%d/%Y @ %h:%i %p') AS checkout_date,
					a.sendoff_time, a.pickup_time, a.comments, a.status,
					
					CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by,
					FROM_UNIXTIME(UNIX_TIMESTAMP(a.date_added), '%M %d, %Y') AS date_added,
					
					CONCAT_WS(' - ', r.number, rt.name) AS room_type
					
					FROM student_room_tag AS t
					LEFT JOIN students_accom AS s USING(students_accom_id)
					LEFT JOIN students_accommodation AS a USING(students_accommodation_id)
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = a.added_by
					LEFT JOIN rooms AS r ON r.number = a.room_no
					LEFT JOIN room_type AS rt ON rt.id = r.type
					LEFT JOIN room_status AS rs ON rs.id = r.status
				";
				
				$this->output->set_output(
					json_encode(
						array(
							'result' 		=> $this->db->query($sql)->result_array(),
							'pagination'	=> ''
						)
					)
				);
			break;
			case "guest-list":
				
				$sql = "
					SELECT g.id AS gid, g.booking_agent, CONCAT_WS(' ', epd.firstname, epd.lastname) AS added_by,
					g.firstname, g.lastname, g.gender, g.country, g.civil_status, FROM_UNIXTIME(UNIX_TIMESTAMP(g.dob), '%m/%d/%Y') AS dob,
					g.email_address, g.company, g.address, g.passport_no, g.contact_no, 
					ga.id AS gaid, ga.room_number, ga.booking_id, ga.booking_type, 
					FROM_UNIXTIME(UNIX_TIMESTAMP(ga.check_in), '%m/%d/%Y @ %h:%i %p') AS check_in,
					FROM_UNIXTIME(UNIX_TIMESTAMP(ga.check_out), '%m/%d/%Y @ %h:%i %p') AS check_out, 
					ga.comment, 
					FROM_UNIXTIME(UNIX_TIMESTAMP(ga.date_added), '%M %d, %Y') AS date_added,
					ga.status, CONCAT_WS(' - ', r.number, rt.name) AS room_type
					
					FROM guest_room_tag AS grt
					LEFT JOIN guests AS g ON grt.guest_id = g.id
					LEFT JOIN guest_accommodation AS ga ON ga.id = grt.guest_accommodation_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = g.fo_staff
					LEFT JOIN rooms AS r ON r.number = ga.room_number
					LEFT JOIN room_type AS rt ON rt.id = r.type
					LEFT JOIN room_status AS rs ON rs.id = r.status
					ORDER BY ga.date_added DESC
				";
				
				$this->output->set_output(
					json_encode(
						array(
							'result' => $this->db->query($sql)->result_array(),
							'pagination' => ''
						)
					)
				);
			/*
				$per_page  = 15;
				$offset = intval( $offset );
				
				$total_rows = $this->db->query("
					SELECT * 
					FROM guest_room_tags AS grt
					LEFT JOIN guests AS g ON g.id = grt.guest_id
					LEFT JOIN accommodations AS a ON a.id = grt.accommodation_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = g.fo_staff
					LEFT JOIN rooms AS r ON r.number = a.room_number
					LEFT JOIN room_type AS rt ON rt.id = r.type
					LEFT JOIN room_status AS rs ON rs.id = r.status
				")->num_rows();

				$config = array(
					"total_rows"	=> $total_rows,
					"base_url"		=> base_url()."hotel/accommodation/retrieve/guest-list",
					"uri_segment"	=> 5,
					"per_page"		=> $per_page
				);

				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();

				$sql = "
					SELECT a.id AS aid, CONCAT_WS(' - ', r.number, rt.name) AS room_number, a.booking_id, a.booking_type, FROM_UNIXTIME(a.check_in, '%m/%d/%Y @ %h:%i %p') AS check_in, 
					FROM_UNIXTIME(a.check_out, '%m/%d/%Y @ %h:%i %p') AS check_out, a.pick_up_time, a.send_off_time,
					a.comment, a.status, FROM_UNIXTIME(a.date_added, '%m/%d/%Y') AS date_added,
					g.id AS gid,
					g.firstname AS guest_firstname, g.lastname AS guest_lastname,g.gender, g.address, g.dob, g.email_address, 
					g.country, g.civil_status, CONCAT_WS(' ', epd.firstname, epd.lastname) AS staff, g.passport_no, g.contact_no
					FROM guest_room_tags AS grt
					LEFT JOIN guests AS g ON g.id = grt.guest_id
					LEFT JOIN accommodations AS a ON a.id = grt.accommodation_id
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = g.fo_staff
					LEFT JOIN rooms AS r ON r.number = a.room_number
					LEFT JOIN room_type AS rt ON rt.id = r.type
					LEFT JOIN room_status AS rs ON rs.id = r.status
					ORDER BY a.date_added DESC
					LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output(
					json_encode(
						array(
							'result' => $this->db->query($sql)->result_array(),
							'pagination' => $pagination
						)
					)
				); */
			break;
			default:
				show_404();
			break;
		}
	}
	/*
	 * Adds accommodation
	 * 
	 */
	public function add( $item = null ) {
		$data_items = json_decode($this->input->post("model"));
		switch( $item ) {
			case "guest":
				
				#guest_accom
				#guest_accomp
				#rooms_accom
				
				$guest_accom = array(
					"booking_id"	=> $data_items->ag_booking_id,
					"booking_agent"	=> $data_items->ag_booking_type,
					"checkin_date"	=> to_mysql_datetime($data_items->ag_checkin_date),
					"checkout_date"	=> to_mysql_datetime($data_items->ag_checkout_date),
					"comments"		=> $data_items->ag_cg_comments
				);
				
				$this->db->insert('guest_accom', $guest_accom);
				$guest_accom_id = $this->db->insert_id();
				
				$guest_accomp = array(
					"guest_accom_id"	=> $guest_accom_id,
					"lastname"			=> $data_items->ag_lname,
					"firstname"			=> $data_items->ag_fname,
					"gender"			=> $data_items->ag_gender,
					"nationality"		=> $data_items->ag_nationality,
					"dob"				=> to_mysql_date($data_items->ag_dob),
					"email"				=> $data_items->ag_email,
					"address"			=> $data_items->ag_address,
					"passport_no"		=> $data_items->ag_passport_no,
					"contact_no"		=> $data_items->ag_contact_no
				);
				
				$this->db->insert('guest_accomp', $guest_accomp);
				$guest_accomp_id = $this->db->insert_id();
				
				$rooms_accom = array(
					"accom_type"		=> "guest",
					"accom_type_id"		=> $guest_accomp_id,
					"room_number_id"	=> $data_items->rid,
					"accom_status"		=> $data_items->ag_booking_status,
					"date_added"		=> date("Y-m-d h:i:s",time()),
					"added_by"			=> $this->user_id
				);
				
				$this->db->insert('rooms_accom', $rooms_accom);

				($data_items->ag_booking_status === 'reserved') ?
					$this->db->update("rooms", array("room_status_id" => 5), array("room_number_id" => $data_items->rid, "status" => "active")) :
						$this->db->update("rooms", array("room_status_id" => 3), array("room_number_id" => $data_items->rid, "status" => "active"));
				
			break;
		}
	}
	
	/*
	 * Saves information to the database
	 * based on the given parameter
	 *
	 * @params:
	 * 		$item - String
	 *
	 */
	public function save( $item = null ) {
		#get the data from the backbone.save() and store it in the $data_items variable
		//$data_items = json_decode( file_get_contents("php://input") );
		$data_items = json_decode($this->input->post("model"));
		switch( $item ) {
			case "create-guest":
				# guests
				# guest_accommodation
					# rooms
						# room_type
						# room_status
				# guest_room_tag
				
				$birthdate = "0000-00-00";
				
				if( isset($data_items->ag_dob) && !empty($data_items->ag_dob) ) {
					list( $m, $d, $y ) = explode("/", $data_items->ag_dob, 3);
					$birthdate = "{$y}-{$m}-{$d}";
				}

				$guest = array(
					"booking_agent" => $data_items->ag_booking_type, 
					"fo_staff"		=> $this->user_id,
					"firstname"		=> $data_items->ag_fname,
					"lastname"		=> $data_items->ag_lname,
					"gender"		=> $data_items->ag_gender,
					"country"		=> $data_items->ag_country,
					"civil_status"	=> $data_items->ag_civil_status,
					"dob"			=> $birthdate,
					"email_address"	=> $data_items->ag_email,
					"address"		=> $data_items->ag_address,
					"passport_no"	=> $data_items->ag_passport_no,
					"contact_no"	=> $data_items->ag_contact_no
				);
				
				$accommodations = array(
					"room_number"	=> $data_items->ag_rn,
					"booking_id"	=> $data_items->ag_booking_id,
					"booking_type"	=> $data_items->ag_booking_type,
					"check_in"		=> to_mysql_datetime($data_items->ag_checkin_date),
					"check_out"		=> to_mysql_datetime($data_items->ag_checkout_date),
					"comment"		=> $data_items->ag_cg_comments,
					"status"		=> $data_items->ag_booking_status
				);
				$this->db->insert("guests", $guest);
				$guest_id = $this->db->insert_id();
				$this->db->insert("guest_accommodation", $accommodations);
				$accommodations_id = $this->db->insert_id();
				# update the room status
				($data_items->ag_booking_status === 'reserved') ? 
					$this->db->update("rooms", array("status" => 5), array("number" => $data_items->ag_rn)) :
						$this->db->update("rooms", array("status" => 3), array("number" => $data_items->ag_rn));
				
				if( $guest_id && $accommodations_id ) {
					$guest_room_tags = array(
						"guest_id"			=> $guest_id,
						"guest_accommodation_id"	=> $accommodations_id,
					);
					$this->db->insert("guest_room_tag", $guest_room_tags);
				}
				
			break;
			case "create-student":
				# students_accom
				# students_accommodation
				
				$student_accom = array(
					"lastname"			=> $data_items->ag_lname,
					"firstname"			=> $data_items->ag_fname,
					"english_name"		=> $data_items->ag_engname,
					"gender"			=> $data_items->ag_gender,
					"age"				=> $data_items->ag_age,
					"numberof_weeks"	=> $data_items->ag_numof_weeks,
					"email"				=> $data_items->ag_email_add,
					"mobile_no"			=> $data_items->ag_mobile_no,
					"student_no"		=> $data_items->ag_student_no,
					"course"			=> $data_items->ag_course,
					"dateof_birth"		=> to_mysql_date($data_items->ag_dob),
					"nationality"		=> $data_items->ag_nationality,
					"passport_no"		=> $data_items->ag_passport_no,
					"expiry_date"		=> to_mysql_date($data_items->ag_expiry_date),
					"placeof_issue"		=> $data_items->ag_placeof_issue,
					"dateof_issue"		=> $data_items->ag_dateof_issue,
					"visa_expiry"		=> to_mysql_date($data_items->ag_visa_expiry),
					"ssp_validity"		=> to_mysql_date($data_items->ag_ssp_validity),
					"address_abroad"	=> $data_items->ag_address_abroad,
					"nameof_guardian"	=> $data_items->ag_nameof_guardian	
				);
				
				$accommodation = array(
					"room_no"			=> $data_items->ag_rn,
					"booking_id"		=> $data_items->ag_booking_id,
					"booking_agent"		=> $data_items->ag_booking_type,
					"checkin_date"		=> to_mysql_datetime($data_items->ag_checkin_date),
					"checkout_date"		=> to_mysql_datetime($data_items->ag_checkout_date),
					"sendoff_time"		=> format_timepicker($data_items->ag_sendoff_time),
					"pickup_time"		=> format_timepicker($data_items->ag_pickup_time),
					"comments"			=> $data_items->ag_cg_comments,
					"status"			=> $data_items->ag_booking_status,
					"added_by"			=> $this->user_id,
				);
				
				$this->db->insert("students_accom", $student_accom);
				$stud_accom_id = $this->db->insert_id();
				$this->db->insert("students_accommodation", $accommodation);
				$accommodations_id = $this->db->insert_id();
				
				# update the room status
				($data_items->ag_booking_status === 'reserved') ?
					$this->db->update("rooms", array("status" => 5), array("number" => $data_items->ag_rn)) :
						$this->db->update("rooms", array("status" => 3), array("number" => $data_items->ag_rn));
				
				if( $stud_accom_id && $accommodations_id ) {
					$student_room_tag = array(
						"students_accom_id"			=> $stud_accom_id,
						"students_accommodation_id"	=> $accommodations_id
					);
					$this->db->insert("student_room_tag", $student_room_tag);
				}
				
				#print_r(array_merge($student_accom, $accommodation));
				//make_timestamp($data_items->ag_checkin_date, "mm/dd/yyyy", "@");
				//print to_mysql_datetime($data_items->ag_checkin_date);
				#print_r($data_items);
				
			break;
		}
	}
	/*
	 * updates information in the database
	 * based on the given parameter
	 *
	 * @params:
	 * 		$item - String
	 *
	 */
	public function update( $item = null ) {
		$data_items = json_decode($this->input->post("model"));
		//$data_items = json_decode(file_get_contents("php://input"));
		switch($item) {
			case "guest-list":
			
				$birthdate = "";
				if( isset($data_items->le_dob) && !empty($data_items->le_dob) ) {
					list( $m, $d, $y ) = explode("/", $data_items->le_dob, 3);
					$birthdate = "{$y}-{$m}-{$d}";
				}
				
				list($accommodation_id, $guest_id) = explode(":", $data_items->le_id, 2);

				$guest = array(
					"booking_agent" => $data_items->le_booking_type, 
					"firstname"		=> $data_items->firstname,
					"lastname"		=> $data_items->lastname,
					"gender"		=> $data_items->le_gender,
					"country"		=> $data_items->le_country,
					"civil_status"	=> $data_items->le_civil_status,
					"dob"			=> $birthdate,
					"email_address"	=> $data_items->le_email,
					"address"		=> $data_items->le_address,
					"passport_no"	=> $data_items->le_passport_no,
					"contact_no"	=> $data_items->le_contact_no
				);

				$accommodations = array(
					#"room_number"	=> $data_items->room_number,
					"booking_id"	=> $data_items->booking_id,
					"booking_type"	=> $data_items->le_booking_type,
					"check_in"		=> !empty($data_items->check_in) ? make_timestamp($data_items->check_in, "mm/dd/yyyy", "@") : 0,
					"check_out"		=> !empty($data_items->check_out) ? make_timestamp($data_items->check_out, "mm/dd/yyyy", "@") : 0,
					"pick_up_time"	=> !empty($data_items->le_pickup_time) ? format_timepicker($data_items->le_pickup_time) : "00:00:00",
					"send_off_time"	=> !empty($data_items->le_send_off_time) ? format_timepicker($data_items->le_send_off_time) : "00:00:00",
					"comment"		=> $data_items->cg_comments,
					"status"		=> $data_items->le_status
				);
				
				$this->db->update("guests", $guest, array("id" => $guest_id));
				$this->db->update("accommodations", $accommodations, array("id" => $accommodation_id));
				
				# update the room status
				if( $data_items->le_status === 'checkin' ) {
					$this->db->update("rooms", array("status" => 3), array("number" => $data_items->room_number));
				} else if( $data_items->le_status === 'checkout' ) {
					$this->db->update("rooms", array("status" => 2), array("number" => $data_items->room_number));
				} else if( $data_items->le_status === 'reserved' ) {
					$this->db->update("rooms", array("status" => 5), array("number" => $data_items->room_number));
				}
				//print "<pre>";
				//print_r($data_items);

			break;
		}
	}
   /*
	* Searches data in the database
	*
	* @params:
	* 		$item - String
	*
	*/	
	public function search( $item = null, $search_by = null, $search_item = null, $offset = 0 ) {
		switch( $item ) {
			case "gli":
				if( !empty($search_item) && !empty($search_by) ) {
					$sql_where = "";
					switch($search_by) {
						case "name":
							$sql_where = "WHERE g.firstname LIKE '%{$search_item}%' OR g.lastname LIKE '%{$search_item}%'";
						break;
						case "country":
							$sql_where = "WHERE g.country LIKE '%{$search_item}%'";
						break;
						case "room":
							$sql_where = "WHERE r.number LIKE '%{$search_item}%'";
						break;
						case "booking_id":
							$sql_where = "WHERE a.booking_id LIKE '%{$search_item}%'";
						break;
						case "booking_type":
							$sql_where = "WHERE a.booking_type LIKE '%{$search_item}%'";
						break;
						case "checkin":
							$time = make_timestamp($search_item, "mm/dd/yyyy");
							$sql_where = "WHERE a.check_in LIKE '%{$time}%'";
						break;
						case "checkout":
							$time = make_timestamp($search_item, "mm/dd/yyyy");
							$sql_where = "WHERE a.check_out LIKE '%{$time}%'";
						break;
						case "status":
							$sql_where = "WHERE a.status LIKE '%{$search_item}%'";
						break;
					}
					$per_page  = 15;
					$offset = intval( $offset );
					
					$total_rows = $this->db->query("
						SELECT *
						FROM guest_room_tags AS grt
						LEFT JOIN guests AS g ON g.id = grt.guest_id
						LEFT JOIN accommodations AS a ON a.id = grt.accommodation_id
						LEFT JOIN emp_personal_details AS epd ON epd.emp_id = g.fo_staff
						LEFT JOIN rooms AS r ON r.number = a.room_number
						LEFT JOIN room_type AS rt ON rt.id = r.type
						LEFT JOIN room_status AS rs ON rs.id = r.status
						{$sql_where}
					")->num_rows();
					
					$config = array(
						"total_rows"	=> $total_rows,
						"base_url"		=> base_url()."hotel/accommodation/search/{$item}/{$search_by}/{$search_item}",
						"uri_segment"	=> 6,
						"per_page"		=> $per_page
					);
					
					$this->pagination->initialize($config);
					$pagination = $this->pagination->create_links();
					
					$sql = "
						SELECT a.id AS aid, CONCAT_WS(' - ', r.number, rt.name) AS room_number, a.booking_id, a.booking_type, FROM_UNIXTIME(a.check_in, '%m/%d/%Y @ %h:%i %p') AS check_in,
						FROM_UNIXTIME(a.check_out, '%m/%d/%Y @ %h:%i %p') AS check_out, a.pick_up_time, a.send_off_time,
						a.comment, a.status, FROM_UNIXTIME(a.date_added, '%m/%d/%Y') AS date_added,
						g.id AS gid,
						g.firstname AS guest_firstname, g.lastname AS guest_lastname,g.gender, g.address, g.dob, g.email_address,
						g.country, g.civil_status, CONCAT_WS(' ', epd.firstname, epd.lastname) AS staff, g.contact_no
						FROM guest_room_tags AS grt
						LEFT JOIN guests AS g ON g.id = grt.guest_id
						LEFT JOIN accommodations AS a ON a.id = grt.accommodation_id
						LEFT JOIN emp_personal_details AS epd ON epd.emp_id = g.fo_staff
						LEFT JOIN rooms AS r ON r.number = a.room_number
						LEFT JOIN room_type AS rt ON rt.id = r.type
						LEFT JOIN room_status AS rs ON rs.id = r.status
						{$sql_where}
						ORDER BY a.date_added DESC
						LIMIT {$offset}, {$per_page}
					";
					
					$this->output->set_output(
						json_encode(
							array(
								'result' => $this->db->query($sql)->result_array(),
								'pagination' => $pagination
							)
						)
					);
				}
			break;
		}
	}
	
}
