<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Rooms extends CI_Controller {
	var $user_session, $user_id;
	public function __construct() {
		parent::__construct();
		
		$this->user_session = $this->session->userdata('emp_info');
		$this->user_id =  $this->user_session['id'];
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	/*
	 * Retrieves information from the database based on the passed parameter
	 * 
	 * @access: public
	 * @params: $item - String
	 */
	public function retrieve( $item = null ) {
		switch( $item ) {
			# status list are not not being used.. updated: Wednesday - February 1, 2012
			/*
			case "status-list":
				#returns the list of room status ex('vacant - clean, vacant-dirty, etc..	')
				
				$result = array();
				foreach( $this->db->get('room_status')->result_array() as $val ) {
					#check how many single rooms with a status of ex('vacant - clean')
					$single = "
						SELECT COUNT(*) as single_rooms 
						FROM rooms AS r
						LEFT JOIN room_type AS rt ON rt.id = r.type
						LEFT JOIN room_status AS rs ON rs.id = r.status
						WHERE rt.name = 'single' AND rs.id = {$val['id']}
					";
					
					#check how many double rooms with a status of ex('vacant - clean')
					$double = "
						SELECT COUNT(*) as double_rooms
						FROM rooms AS r
						LEFT JOIN room_type AS rt ON rt.id = r.type
						LEFT JOIN room_status AS rs ON rs.id = r.status
						WHERE rt.name = 'double' AND rs.id = {$val['id']}
					";
					
					#check how many quad rooms with a status of ex('vacant - clean')
					$quad = "
						SELECT COUNT(*) as quad_rooms
						FROM rooms AS r
						LEFT JOIN room_type AS rt ON rt.id = r.type
						LEFT JOIN room_status AS rs ON rs.id = r.status
						WHERE rt.name = 'quad' AND rs.id = {$val['id']}
					";
					#check how many quad rooms with a status of ex('vacant - clean')
					$corp = "
						SELECT COUNT(*) as corporate_rooms
						FROM rooms AS r
						LEFT JOIN room_type AS rt ON rt.id = r.type
						LEFT JOIN room_status AS rs ON rs.id = r.status
						WHERE rt.name = 'corporate' AND rs.id = {$val['id']}
					";
					
					#add the result to the result array
					$result[$val["id"]] = $val + $this->db->query($single)->row_array()
											   + $this->db->query($double)->row_array()
											   + $this->db->query($quad)->row_array()
											   + $this->db->query($corp)->row_array();
				}

				$this->output->set_output(
					json_encode(
						array("result" => $result)
					)
				);
			break;*/
		}
	}
	
	/*
	 * Gets the booking information of the given room
	 */
	
	public function info( $type = null, $number = 0 ) {
		#get the last room from the accommodations table 
		#based on the $type variable
		#function not needed: last updated @ February 3, 2012 - 1:49PM
		$number = intval($number);
		switch( $type ) {
			case "reserved":
			case "occupied":
				#check for the reserve or checkin status
				$sql = "
					SELECT a.id AS aid, g.id AS gid, a.room_number, a.booking_id, a.booking_type, 
					FROM_UNIXTIME(a.check_in, '%m/%d/%Y @ %h:%i %p') AS check_in, FROM_UNIXTIME(a.check_out, '%m/%d/%Y @ %h:%i %p') AS check_out, 
					a.pick_up_time, a.send_off_time, a.comment, FROM_UNIXTIME(a.date_added, '%m/%d/%Y') AS date_added, a.status,
					g.booking_agent, g.fo_staff, g.firstname, g.lastname, g.gender, g.country, g.civil_status,
					g.dob, g.email_address, g.address, g.passport_no, g.contact_no 
					FROM accommodations a
					LEFT JOIN guest_room_tags AS grt ON grt.accommodation_id = a.id
					LEFT JOIN guests AS g ON g.id = grt.guest_id
					WHERE a.room_number = {$number} AND (a.status = 'checkin' OR a.status = 'reserved')
					ORDER BY a.id DESC LIMIT 1
				";
				$this->output->set_output(
					json_encode(
						array('info' => $this->db->query($sql)->row_array())
					)
				);
			break;
		}
		
	}
	
	public function get( $item = null ) {
		switch( $item ) {
			case "rooms-list":
				
				$sql = "
					SELECT rni.room_number_id AS room_nid, rni.room_no, rt.room_type_name, rs.room_status_name, rs.room_status_css, rmrates.room_rate
					FROM rooms AS r LEFT JOIN room_number_ids AS rni ON rni.room_number_id = r.room_number_id 
					LEFT JOIN room_type AS rt ON rt.room_type_id = r.room_type_id 
					LEFT JOIN room_status AS rs ON rs.room_status_id = r.room_status_id 
					LEFT JOIN room_rates AS rmrates ON rmrates.room_rate_id = rt.room_rate_id
					ORDER BY rni.room_no";
				
				/*$sql = "
					SELECT rm.number, rmt.name AS room_type, rmt.rate, rms.name AS room_status, rms.css
					FROM rooms AS rm LEFT JOIN room_type AS rmt ON rmt.id = rm.type
					LEFT JOIN room_status AS rms ON rms.id = rm.status ORDER BY rm.number ASC
				";*/
				$this->output->set_output(
					json_encode(
						array(
							'result' => $this->db->query($sql)->result_array()
						)
					)
				);
			break;
		}
		/*
		$search = array("single-rooms", "double-rooms", "quad-rooms", "corporate-rooms");
		$replace = array("single", "double", "quad", "corporate");
		
		if( in_array($item, $search) ) {
			$room = str_replace($search, $replace, $item);
			# print $rooms;
			
			$sql = "
				SELECT r.number, r.comment, rs.css, rs.name FROM rooms AS r 
				LEFT JOIN room_type AS rt ON rt.id = r.type 
				LEFT JOIN room_status AS rs ON rs.id = r.status 
				WHERE rt.name = '{$room}'
			";
			$this->output->set_output(
				json_encode(
					array("rooms" => $this->db->query($sql)->result_array())
				)
			);
		} else show_404();
		*/
	}
	
	public function update( $item = null ) {
		#function not needed: last updated @ February 3, 2012 - 1:50PM
		$data_items = json_decode( $this->input->post('model') );
		switch( $item ) {
			case "rm-status":
				#rooms
				$this->db->update('rooms', array('status' => $data_items->new_status), array('number' => $data_items->room));
			break;
		}
	}	
}