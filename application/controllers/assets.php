<?php if( !defined('BASEPATH') ) exit('No direct script access allowed');
class Assets extends CI_Controller {
	var $user_session;
	public function __construct() {
		parent::__construct();
		$this->user_session = $this->session->userdata('emp_info');
		
		#tell the user to login if the session is empty
		if( empty( $this->user_session ) ) redirect('login');
	}
	/*
	 * Retrieve information from the database
	 * based on the given parameter
	 *
	 * @params:
	 * 		$items - String
	 *      $offset - int (For pagination)
	 *
	 */
	public function retrieve( $items = null, $offset = 0 ) {
		$user_id =  $this->user_session['id']; #the user_id of the login user
		switch( $items ) {
			case "assigned-assets":
				
			break;
			case "transfered-assets":
				$sql = "
					SELECT a.tag AS asset_tag, a.brand, FROM_UNIXTIME(a_t.date,'%m/%d/%Y' ) AS date_transfered, 
					a_t.received, a_t.fin_appr AS approval, CONCAT_WS(' ', epd.firstname, epd.lastname) AS user_from,
					a_t.tracker_id, a_t.parent_tracker
					FROM assets AS a
					LEFT JOIN asset_tracker AS a_t USING(asset_id)
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = a_t.from_emp_id
					WHERE a_t.to_emp_id =  {$user_id} AND a_t.received = 0 AND a_t.for_log != '1' AND fin_appr = 'approved'
					ORDER BY a_t.date DESC
				";
				$this->output->set_output(
					json_encode(
						array(
							"result" => $this->db->query($sql)->result_array(),
							"pagination" => ""
						)
					)
				);
			break;
			case "transfer":
				
				$per_page = 15; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("
					SELECT *
					FROM assets AS a
					LEFT JOIN asset_tracker AS at USING(asset_id)
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = at.to_emp_id
					WHERE at.from_emp_id = {$user_id} AND asset_status = 'transfered'
				")->num_rows();
				$config = array(
					'base_url'		=> base_url().'assets/retrieve/transfer',
					'total_rows'	=> $total_rows,
					'per_page'		=> $per_page,
					'uri_segment'	=> 4
				);
			
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				
				$sql = "
					SELECT a.asset_id, a.tag AS asset_tag, a.brand, CONCAT_WS(' ', epd.firstname, epd.lastname) AS transfer_to,
					FROM_UNIXTIME(at.date, '%m/%d/%Y') AS date, at.received, at.fin_appr, at.tracker_id, at.parent_tracker
					FROM assets AS a
					LEFT JOIN asset_tracker AS at USING(asset_id)
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = at.to_emp_id
					WHERE at.from_emp_id = {$user_id} AND asset_status = 'transfered'
					ORDER BY at.date DESC
					LIMIT {$offset}, {$per_page} 
				";
				
				$this->output->set_output(json_encode(
					array_merge(
						array("result" => $this->db->query($sql)->result_array()),
						array("pagination" => $pagination )
					)
				));
			break;
			case "returned":
				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("
					SELECT *
					FROM assets AS a
					LEFT JOIN asset_tracker AS at USING(asset_id)
					LEFT JOIN emp_personal_details AS epd ON epd.emp_id = at.to_emp_id
					WHERE at.from_emp_id = {$user_id} AND asset_status = 'returned'
				")->num_rows();
				$config = array(
					'base_url'		=> base_url().'assets/retrieve/returned',
					'total_rows'	=> $total_rows,
					'per_page'		=> $per_page,
					'uri_segment'	=> 4
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();

				$sql = "
					SELECT a.asset_id, a.tag AS asset_tag, a.brand,
					FROM_UNIXTIME(at.date, '%m/%d/%Y') AS date, at.received, at.fin_appr, at.tracker_id, at.parent_tracker
					FROM assets AS a
					LEFT JOIN asset_tracker AS at USING(asset_id)
					WHERE at.from_emp_id = {$user_id} AND asset_status = 'returned'
					ORDER BY at.date DESC
					LIMIT {$offset}, {$per_page}
				";
				$this->output->set_output(json_encode(
					array_merge(
						array("result" => $this->db->query($sql)->result_array()),
						array("pagination" => $pagination )
					)
				));
			break;
			case "asset-list":
				
				$per_page = 10; # number of items to be displayed per page
				$offset = intval($offset);
				
				$total_rows = $this->db->query("
					SELECT * FROM assets AS a
					LEFT JOIN asset_tracker AS at USING(asset_id)
					WHERE at.to_emp_id = {$user_id} AND ( at.received !=0 AND at.fin_appr = 'approved' AND for_log = '0' )
				")->num_rows();
				$config = array(
						'base_url'		=> base_url().'assets/retrieve/asset-list',
						'total_rows'	=> $total_rows,
						'per_page'		=> $per_page,
						'uri_segment'	=> 4
				);
				
				$this->pagination->initialize($config);
				$pagination = $this->pagination->create_links();
				/*
				$sql = "
					SELECT asset_id, tag AS asset_tag, item_code, brand, description, 
					serial_num
					FROM assets WHERE owner = {$user_id} 
					ORDER BY asset_id DESC
					LIMIT {$offset}, {$per_page}
				";*/
				
				$sql = "
					SELECT a.asset_id, a.tag AS asset_tag, a.item_code, a.brand, a.description,
					a.serial_num
					FROM assets AS a
					LEFT JOIN asset_tracker AS at USING(asset_id)
					WHERE at.to_emp_id = {$user_id} AND ( at.received !=0 AND at.fin_appr = 'approved' AND for_log = '0' )
					ORDER BY a.asset_id DESC
					LIMIT {$offset}, {$per_page}
				";
				
				$this->output->set_output(json_encode( 
					array_merge(
						array("asset_list" => $this->db->query($sql)->result_array()),
						array("pagination" => $pagination )
					) 
				));
			break;
			case "all-assets":
				/*
				$sql = "
					SELECT a.asset_id, a.tag AS asset_tag, a.item_code, a.brand, a.description,
					a.serial_num
					FROM assets AS a
					WHERE a.owner = {$user_id}
					ORDER BY a.asset_id DESC
				";*/
				$sql = "
					SELECT a.asset_id, a.tag AS asset_tag, a.item_code, a.brand, a.description,
					a.serial_num, at.tracker_id
					FROM assets AS a 
					LEFT JOIN asset_tracker AS at USING(asset_id)
					WHERE at.to_emp_id = {$user_id} AND ( at.received !=0 AND at.fin_appr = 'approved' AND for_log = '0' )
					ORDER BY a.asset_id DESC
				";
				$this->output->set_output(json_encode(
					array("asset_list" => $this->db->query($sql)->result_array())
				));
			break;
			case "name-to-transfer":
				# select active status
				$name = ( $this->input->post("name") ) ? $this->input->post("name") : "";
				
				$sql = "
					SELECT CONCAT_WS(' ', epd.firstname, epd.lastname) AS name, dd.dept_name, eu.emp_id AS trans_to
					FROM emp_personal_details AS epd
					LEFT JOIN emp_user AS eu ON eu.emp_id = epd.emp_id
					LEFT JOIN department AS d ON d.emp_id = epd.emp_id
					LEFT JOIN department_details AS dd ON d.deptd_id = dd.deptd_id
					WHERE (eu.status = 'active' AND eu.emp_id != {$user_id} ) AND CONCAT_WS(' ', epd.firstname, epd.lastname) like '%{$name}%'
					ORDER BY epd.firstname ASC
				";
				$this->output->set_output(json_encode(array("result" => $this->db->query($sql)->result_array())));
			break;
			default:
				show_404();
			break;
		}
	}
	/*
	 * Retrieve the asset information of a specific asset
	 * based on the asset id
	 *
	 * @params:
 	 * 		$items
	 *
	 */
	public function action( $items = null ) {
		$user_id =  $this->user_session['id']; #the user_id of the login user
		
		#get the data from the backbone.save() and store it in the $data_items variable
		$post_items = json_decode(file_get_contents("php://input"));
		
		switch($items) {
			case "receive-transfer":
				list( $id, $tracker_id ) = explode(":", $post_items->id, 2);
				$this->db->update("asset_tracker", array("received" => time()), array("tracker_id" => $id));
			break;
			case "return":
				#asset_tracker
				#assets
				
				# the loop will not run if there are no ids
				foreach( $post_items->ids as $id ) {
					
					list( $id, $tracker_id ) = explode(":", $id, 2);
					
					$update_tracker = array("for_log" => '1');
					$this->db->update("asset_tracker", $update_tracker, array("to_emp_id" => $user_id, "tracker_id" => $tracker_id));
					
					$asset_tracker = array(
						"parent_tracker"=> $tracker_id,
						"asset_status"	=> "returned",
						"from_emp_id"	=> $user_id,
						"asset_id" 		=> $id,
						"date"			=> time()
					);
					$this->db->affected_rows() ? $this->db->insert("asset_tracker", $asset_tracker) : "";
				}
			break;
			case "transfer":
				foreach( $post_items->ids as $id ) {
					
					list( $id, $tracker_id ) = explode(":", $id, 2);
					
					$update_tracker = array("for_log" => '1');
					$this->db->update("asset_tracker", $update_tracker, array("to_emp_id" => $user_id, "tracker_id" => $tracker_id));
					
					$asset_tracker = array(
						"parent_tracker"=> $tracker_id,
						"asset_status"	=> "transfered",
						"from_emp_id"	=> $user_id,
						"to_emp_id"		=> $post_items->transfer,
						"asset_id" 		=> $id,
						"date"			=> time()
					);
					
					$this->db->affected_rows() ? $this->db->insert("asset_tracker", $asset_tracker) : "";
				}
			break;
			default:
				//redirect('login');
				show_404();
			break;
		}
	}
	/*
	 * Retrieve the asset information of a specific asset 
	 * based on the asset id
	 *
 	 * @params:
 	 * 		$id - the id of the asset
	 *
	 */
	public function asset_info( $id = 0 ) {
		$user_id =  $this->user_session['id']; #the user_id of the login user
		$id = intval($id);
		/*
		$sql = "
			SELECT a.tag AS asset_tag, a.item_code, a.brand, a.serial_num, a.description, 
			astats.status_name AS status, location, CONCAT_WS(' ', epd.firstname, epd.lastname ) AS owner
			FROM assets AS a
			LEFT JOIN asset_stats AS astats ON astats.id = a.status
			LEFT JOIN emp_personal_details AS epd ON epd.emp_id = a.owner
			WHERE owner = {$user_id} AND asset_id = {$id}
		";*/
		$sql = "
			SELECT a.tag AS asset_tag, a.item_code, a.brand, a.description,
			a.serial_num, at.asset_status AS status, a.location, CONCAT_WS(' ', epd.firstname, epd.lastname ) AS owner
			FROM assets AS a
			LEFT JOIN asset_tracker AS at USING(asset_id)
			LEFT JOIN emp_personal_details AS epd ON epd.emp_id = at.to_emp_id
			WHERE at.to_emp_id = {$user_id} AND a.asset_id = {$id}
			LIMIT 1
		";
		$this->output->set_output(
			json_encode($this->db->query($sql)->row_array())
		);
	}
	/*
	 * Cancels all the asset transfer and asset return
	 * 
	 * @params:
	 * 		$items - String ( either return or transfer )
	 * 
	 */
	public function cancel( $items = null ) {
		$user_id =  $this->user_session['id']; #the user_id of the login user
		
		#get the data from the backbone.save() and store it in the $data_items variable
		$post_items = json_decode(file_get_contents("php://input"));
		
		switch( $items ) {
			case "transfer":
				$cancel_transfer = array(
					"fin_appr"	=> "cancelled",
					"for_log"	=> "1" #we will make sure that the for_log is set to 1 so that this asset will not be read in asset list 
				);
				list($id, $parent_tracker) = explode(":", $post_items->id, 2 );
				
				$this->db->update("asset_tracker", $cancel_transfer, array("tracker_id" => $id));
				( $this->db->affected_rows() ) ?
					$this->db->update("asset_tracker", array("for_log"	=> "0"), array("tracker_id" => $parent_tracker)) : "";
				
			break;
			case "return":
				$cancel_return = array(
					"fin_appr"	=> "cancelled",
					"for_log"	=> "1" #we will make sure that the for_log is set to 1 so that this asset will not be read in asset list
				);
				list($id, $parent_tracker) = explode(":", $post_items->id, 2 );
				
				$this->db->update("asset_tracker", $cancel_return, array("tracker_id" => $id));
				( $this->db->affected_rows() ) ?
					$this->db->update("asset_tracker", array("for_log"	=> "0"), array("tracker_id" => $parent_tracker)) : "";
				
			break;
		}
	}
	
	/*
	 * Retrieve asset(list, transfered, returned) and 
	 * displays it in the Asset Master list page
	 * 
	 * @params: 
	 * 		$items, String parameter
	 * 		$offset, int - used for pagination
	 * @access: public
	 * @return: json encoded values
	 */
	
	public function master_list( $items = null, $offset = 0 ) {
		switch( $items ) {
			case "":
				
			break;
		}
	}
}